Imports System.Data
Imports System.Data.SqlClient

Partial Class frmAddUser
    Inherits System.Web.UI.Page

    Dim dsStation As New DataSet
    Dim dsDepartment As New DataSet
    Dim dsEmployee As New DataSet
    Dim dsApplyTo As New DataSet

    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strApplyToList As String
        lblMsg.Text = ""

        If IsPostBack() = False Then

            Session("DA_New_USERLIST") = ""

            If Session("DA_USERLIST") = "" Then
                strApplyToList = ""
                Session("DA_New_USERLIST") = ""
            Else
                strApplyToList = Session("DA_USERLIST").ToString
                Session("DA_New_USERLIST") = Session("DA_USERLIST")
            End If
            BindDropDownList()
            GetApplyToData(strApplyToList)

        End If
    End Sub

    Private Sub BindDropDownList()
        Try

            dsStation = DIMERCO.SDK.Utilities.ReSM.GetStationList()
            Dim dtView As DataView = dsStation.Tables(0).DefaultView
            dtView.Sort = "StationCode ASC"
            ddlStation.DataSource = dtView
            'ddlStation.DataMember = dsStation.Tables(0).TableName
            ddlStation.DataValueField = dtView.Table.Columns("StationID").ToString
            ddlStation.DataTextField = dtView.Table.Columns("StationCode").ToString
            ddlStation.DataBind()
            'ddlStation.Items.Add("")
            ddlStation.SelectedValue = Session("DA_StationID").ToString 'Session("DA_AddUserStationID").ToString


            'Dim dsDepartment As New DataSet
            'dsDepartment = DIMERCO.SDK.Utilities.LSDK.getDepartment()

            'Dim newRow As DataRow = dsDepartment.Tables(0).NewRow()
            'newRow("HQID") = "0"
            'newRow("DepartmentName") = "ALL"

            'dsDepartment.Tables(0).Rows.Add(newRow)

            'ddlDepartment.DataSource = dsDepartment
            'ddlDepartment.DataMember = dsDepartment.Tables(0).TableName
            'ddlDepartment.DataValueField = dsDepartment.Tables(0).Columns("HQID").ToString
            'ddlDepartment.DataTextField = dsDepartment.Tables(0).Columns("DepartmentName").ToString
            'ddlDepartment.DataBind()

            'If Session("DA_DepartmentCode") <> "" Then
            '    ddlDepartment.SelectedValue = Session("DA_DepartmentCode").ToString
            'End If

            'dsDepartment = DIMERCO.SDK.Utilities.LSDK.getDepartment()
            ''Dim i As Integer
            ''For i = 0 To dsDepartment.Tables(0).Columns.Count - 1
            ''    Dim strField As String = dsDepartment.Tables(0).Columns(i).ColumnName
            ''Next

            'ddlDepartment.DataSource = dsDepartment
            'ddlDepartment.DataMember = dsDepartment.Tables(0).TableName
            'ddlDepartment.DataValueField = dsDepartment.Tables(0).Columns("HQID").ToString
            'ddlDepartment.DataTextField = dsDepartment.Tables(0).Columns("DepartmentName").ToString
            'ddlDepartment.DataBind()

            'If Session("DA_DepartmentCode").ToString <> "" Then
            '    ddlDepartment.SelectedValue = Session("DA_DepartmentCode").ToString
            'End If
            ''ddlDepartment.Items.Add("All")
            ''ddlDepartment.SelectedValue = "All"

        Catch ex As Exception
            Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
        End Try
    End Sub

    Private Sub GetApplyToData(ByVal strApplyToList As String)

        Dim strStation As String = ""
        'Dim strDepartmentCode As String = 0

        If ddlStation.Items.Count <> 0 Then
            strStation = ddlStation.SelectedItem.Value.ToString
        End If

        'If ddlStation.Items.Count <> 0 Then
        '    If ddlDepartment.SelectedValue.ToString = "" Then
        '        strDepartmentCode = ""
        '    Else
        '        strDepartmentCode = ddlDepartment.SelectedItem.Value.ToString
        '    End If
        'End If

        dsEmployee = DIMERCO.SDK.Utilities.LSDK.getUserProfilebyFullNameNStation(strStation, txtUserName.Text.ToString)

        Dim dtView As DataView = dsEmployee.Tables(0).DefaultView
        dtView.RowFilter = "UserID NOT IN ('" + strApplyToList.Replace(",", "','") + "')"

        'Dim i As Integer
        'Dim strcolname As String
        'For i = 0 To dtView.Table.Columns.Count
        '    strcolname = dtView.Table.Columns(i).ColumnName.ToString()
        'Next

        gvName.Visible = True
        gvName.DataSource = dtView.Table
        gvName.DataMember = dtView.Table.TableName
        gvName.DataBind()

        'Bind Employee 
        'dsEmployee = DIMERCO.SDK.Utilities.LSDK.getUserDataByStationIDNDeptID(strStation, strDepartmentCode, strApplyToList)
        'gvName.Visible = True
        'gvName.DataSource = dsEmployee.Tables(0)
        'gvName.DataMember = dsEmployee.Tables(0).TableName
        'gvName.DataBind()

        If gvName.Rows.Count > 0 Then
            chkSelectAllCC.Visible = True
            chkSelectAllCC.Checked = False
        Else
            chkSelectAllCC.Visible = True
            chkSelectAllCC.Checked = False
        End If

        dsApplyTo = DIMERCO.SDK.Utilities.LSDK.getUserDataByList(strApplyToList)
        grvList.Dispose()
        If dsApplyTo.Tables(0).Rows.Count > 0 Then
            grvList.Visible = True
            grvList.DataSource = dsApplyTo
            grvList.DataMember = dsApplyTo.Tables(0).TableName
            grvList.DataBind()
        Else
            grvList.Visible = False
        End If

    End Sub


    Protected Sub grvInviteList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        Dim strApplyToList As String
        Dim strNewApplyToList As String = ""
        Dim strRemove As String = ""

        strRemove = grvList.SelectedValue.ToString.ToUpper()

        strApplyToList = Session("DA_New_USERLIST").ToString.ToUpper()

        strNewApplyToList = strApplyToList.Replace(strRemove.ToUpper(), "").ToString
        strNewApplyToList = strNewApplyToList.Replace(",,", ",").ToString
        If strNewApplyToList = "," Then
            strNewApplyToList = ""
        End If

        Session("DA_New_USERLIST") = strNewApplyToList

        GetApplyToData(strNewApplyToList)

    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click

        If Session("DA_New_USERLIST") <> Nothing Then
            GetApplyToData(Session("DA_New_USERLIST").ToString)
        Else
            GetApplyToData("")
        End If
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddToList.Click
        'Dim i As Integer
        Dim strSelectedName As String = ""
        Dim strApplyToList As String = ""

        If Session("DA_New_USERLIST") <> Nothing Then
            strApplyToList = Session("DA_New_USERLIST").ToString
        End If

        If gvName.Rows.Count > 0 Then
            For Each row As GridViewRow In gvName.Rows

                Dim chkCC As CheckBox = row.FindControl("chkCC")
                Dim hfID As HiddenField = row.FindControl("hfID")
                strSelectedName = hfID.Value.ToString()

                If chkCC.Checked = True Then
                    If strApplyToList.Contains(strSelectedName) = False Then
                        strApplyToList += "," + strSelectedName
                    End If
                End If
            Next
        End If

        If strApplyToList.ToString <> "" Then
            strApplyToList += ","
        End If


        Session("DA_New_USERLIST") = ""
        Session("DA_New_USERLIST") = strApplyToList


        If Session("DA_New_USERLIST") <> Nothing Then
            GetApplyToData(Session("DA_New_USERLIST").ToString)
        Else
            GetApplyToData("")
        End If

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Close the window
        Response.Write("<script language=""Javascript"">window.opener =self;window.close();</script>")
        Response.End()
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        Dim ParentFormID As String = ""
        ParentFormID = Request.QueryString("ParentForm").ToString

        Session("DA_USERLIST") = Session("DA_New_USERLIST")
        Session("DA_New_USERLIST") = Nothing

        Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener =self;window.close();</script>")
    End Sub

    Protected Sub chkSelectAllCC_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAllCC.CheckedChanged
        If gvName.Rows.Count > 0 Then
            For Each row As GridViewRow In gvName.Rows
                Dim chkCC As CheckBox = row.FindControl("chkCC")
                If chkSelectAllCC.Checked = True Then
                    If chkCC IsNot Nothing Then
                        chkCC.Checked = True
                    End If
                Else
                    chkCC.Checked = False
                End If
            Next
        End If
    End Sub
End Class
