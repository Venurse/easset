<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmeAsset.aspx.vb" Inherits="frmeAsset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Dimerco eAsset</title>
     <script language="javascript" type="text/javascript">

        function showDate() 
        {
            var d_names = new Array("Sunday", "Monday", "Tuesday",
            "Wednesday", "Thursday", "Friday", "Saturday");

            var m_names = new Array("January", "February", "March", 
            "April", "May", "June", "July", "August", "September", 
            "October", "November", "December");
            
            var month_names = new Array("Jan", "Feb", "Mar", 
            "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
            "Oct", "Nov", "Dec");

            var d = new Date();
            var curr_day = d.getDay();
            var curr_date = d.getDate();
            var hours = d.getHours();
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            
            var curr_hours;
            var curr_minutes;
            var curr_seconds;

            if(hours<10)
            {
                curr_hours = "0" + hours;
            }
            else
            {
                curr_hours = hours;
            }
            
            if(minutes < 10)
            {
                curr_minutes = "0" + minutes;
            }
             else
            {
                curr_minutes = minutes;
            }
            
           if(seconds < 10)
            {
                curr_seconds = "0" + seconds;
            }
             else
            {
                curr_seconds = seconds;
            }


            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
            
            document.getElementById("txtDate").value = d_names[curr_day] + ", " +  m_names[curr_month] +  "  " + curr_date + ", " + curr_year;
            document.getElementById("hdfCurrentDateTime").value = curr_date + " " +  month_names[curr_month] +  " " + curr_year + " " + curr_hours + ":" + curr_minutes + ":" + curr_seconds
            
            
//            var myvar = curr_date + " " +  month_names[curr_month] +  " " + curr_year;  
         }
    </script>
</head>
<body onload="showDate()">
    <form id="eAsset" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%; height: 8px;">
                </td>
                <td style="width: 150px; height: 8px;">
                </td>
                <td align="center" colspan="6" rowspan="2">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Garamond" Font-Size="XX-Large"
                        Text="DIMERCO eASSET SYSTEM" Font-Italic="False"></asp:Label>
                    &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; 
                </td>
                <td style="width: 10%; height: 8px;">
                    <asp:HiddenField ID="hdfCurrentDateTime" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblWelcome" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="#0000C0"></asp:Label></td>
                <td style="width: 15%" align="right">
                    <asp:TextBox ID="txtDate" runat="server" BorderStyle="None" Font-Names="Verdana"
                        Font-Size="X-Small" ForeColor="#0000C0" ReadOnly="True" Width="200px"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="9" style="height: 37px">
                    <hr />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
