<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Copy of Default.aspx.vb" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Flextronic - Data List"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="height: 21px">
                </td>
                <td colspan="7" style="height: 21px">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label>
                    <table>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="ID"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:Label ID="lblID" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Software"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:TextBox ID="txtSoftware" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    MaxLength="200" Width="400px"></asp:TextBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <table style="width: 335px">
                                    <tr>
                                        <td style="width: 150px">
                                            <asp:Button ID="Button2" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Save"
                                                Width="65px" /></td>
                                        <td style="width: 150px">
                                            <asp:Button ID="btnSearch" runat="server" Font-Names="Verdana" Font-Size="Small"
                                                Text="Search" Width="65px" /></td>
                                        <td style="width: 150px">
                                            <asp:Button ID="btnClear" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Clear"
                                                Width="65px" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="height: 21px">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:LinkButton ID="LinkButton1" runat="server" Font-Names="Verdana" Font-Size="Smaller"
                        PostBackUrl="~/frmMenu.aspx">Menu</asp:LinkButton></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="HAWB"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:TextBox ID="txtHAWB" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="120px"></asp:TextBox></td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Fiscal Year"></asp:Label></td>
                <td>
                    :</td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                    <table style="width: 335px">
                        <tr>
                            <td style="width: 150px">
                                <asp:Button ID="btnSave" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Save"
                                    Width="65px" /></td>
                            <td style="width: 150px">
                                <asp:Button ID="Button1" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Clear"
                                    Width="65px" /></td>
                            <td style="width: 150px">
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
