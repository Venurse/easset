
Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class System_frmSubCategory
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If


        If IsPostBack() = False Then

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            btnSave.Enabled = False
            gvData.Columns(0).Visible = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "S0012")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the SubCategory Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnSave.Enabled = True
                        gvData.Columns(0).Visible = True
                    Else
                        Session("blnWrite") = False
                        btnSave.Enabled = False
                        gvData.Columns(0).Visible = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                    'btnSave.Visible = True
                    'gvData.Columns(0).Visible = True
                Else
                    Session("blnModifyOthers") = False
                    'btnSave.Visible = False
                    'gvData.Columns(0).Visible = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                    'gvData.Columns(0).Visible = True
                Else
                    Session("blnDeleteOthers") = False
                    'gvData.Columns(0).Visible = False
                End If
            End If

            BindDropDownList()
            GetSubCategoryData()
        Else

        End If
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByID")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[Group], [Category]"
                    .Parameters.Add("@ID", SqlDbType.BigInt)
                    .Parameters("@ID").Value = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "Group"
                MyData.Tables(1).TableName = "Category"

                ddlGroup.DataSource = MyData.Tables(0)
                ddlGroup.DataValueField = MyData.Tables(0).Columns("GroupID").ToString
                ddlGroup.DataTextField = MyData.Tables(0).Columns("Group_Name").ToString
                ddlGroup.DataBind()

                ddlCategory.DataSource = MyData.Tables(1)
                ddlCategory.DataValueField = MyData.Tables(1).Columns("CategoryID").ToString
                ddlCategory.DataTextField = MyData.Tables(1).Columns("Category").ToString
                ddlCategory.DataBind()



                Dim dtViewCurrency As DataView = clsCF.GetCurrency()

                ddlCurrencyCode.DataSource = dtViewCurrency.Table
                ddlCurrencyCode.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlCurrencyCode.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlCurrencyCode.DataBind()


            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub GetSubCategoryData()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetSubCategoryData")

                With MyCommand
                    .Parameters.Add("@GroupID", SqlDbType.Int)
                    .Parameters("@GroupID").Value = CInt(ddlGroup.SelectedValue.ToString)
                    .Parameters.Add("@ParentCategory", SqlDbType.Int)
                    .Parameters("@ParentCategory").Value = CInt(ddlCategory.SelectedValue.ToString)
                    .Parameters.Add("@SubCategory", SqlDbType.NVarChar, 100)
                    .Parameters("@SubCategory").Value = txtSubCategory.Text.ToString
                    .Parameters.Add("@Description", SqlDbType.NVarChar)
                    .Parameters("@Description").Value = txtDescription.Text.ToString


                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "GetSubCategoryData"


                gvData.DataSource = MyData.Tables(0)
                gvData.DataMember = MyData.Tables(0).TableName
                gvData.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "GetSubCategoryData: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        lblMsg.Text = ""
        lblID.Text = ""
        ddlGroup.SelectedValue = 0
        ddlCategory.SelectedValue = 0
        txtSubCategory.Text = ""
        txtDescription.Text = ""
        txtDepreciationPeriod.Text = ""
        chkSendeReq.Checked = False
        ddlCurrencyCode.SelectedValue = "0"
        txteReqPricePoint.Text = ""
        chkBudgetItem.Checked = False
        chkUsable_Life_Span.Checked = False
        chkUsable_Life_Span.Checked = False
        chkSendeReqPerUnit.Checked = False
        chkLansweeperMapping.Checked = False

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblMsg.Text = ""
        GetSubCategoryData()
    End Sub

    Protected Sub gvData_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvData.SelectedIndexChanged
        lblMsg.Text = ""

        Dim intID As Integer = 0
        intID = CInt(gvData.SelectedDataKey.Value.ToString)
        lblID.Text = intID

        Dim hdfGroupID As New HiddenField
        hdfGroupID = gvData.SelectedRow.FindControl("hdfGroupID")
        ddlGroup.SelectedValue = hdfGroupID.Value.ToString
        ddlGroup_SelectedIndexChanged(sender, e)

        Dim hdParentCategoryID As New HiddenField
        hdParentCategoryID = gvData.SelectedRow.FindControl("hdParentCategoryID")
        ddlCategory.SelectedValue = hdParentCategoryID.Value.ToString

        Dim lbtnSubCategory As New LinkButton
        lbtnSubCategory = gvData.SelectedRow.FindControl("lbtnSubCategory")
        txtSubCategory.Text = lbtnSubCategory.Text.ToString

        Dim lblDescription As New Label
        lblDescription = gvData.SelectedRow.FindControl("lblDescription")
        txtDescription.Text = lblDescription.Text.ToString

        Dim lblDepreciationPeriod As New Label
        lblDepreciationPeriod = gvData.SelectedRow.FindControl("lblDepreciationPeriod")
        txtDepreciationPeriod.Text = lblDepreciationPeriod.Text.ToString

        Dim lblSendeReq As New Label
        lblSendeReq = gvData.SelectedRow.FindControl("lblSendeReq")
        If lblSendeReq.Text.ToString = "Y" Then
            chkSendeReq.Checked = True
            ddlCurrencyCode.Enabled = False
            txteReqPricePoint.Enabled = False
        Else
            chkSendeReq.Checked = False
            'ddlCurrencyCode.Enabled = True
            'txteReqPricePoint.Enabled = True
        End If

        Dim hdfCurrencyCodeID As New HiddenField
        hdfCurrencyCodeID = gvData.SelectedRow.FindControl("hdfCurrencyCodeID")
        If hdfCurrencyCodeID.Value.ToString = "" Then
            ddlCurrencyCode.SelectedValue = "0"
        Else
            ddlCurrencyCode.SelectedValue = hdfCurrencyCodeID.Value.ToString
        End If

        Dim lbleReqPricePointCtrl As New Label
        lbleReqPricePointCtrl = gvData.SelectedRow.FindControl("lbleReqPricePointCtrl")
        txteReqPricePoint.Text = lbleReqPricePointCtrl.Text.ToString

        Dim lblBudgetItem As New Label
        lblBudgetItem = gvData.SelectedRow.FindControl("lblBudgetItem")
        If lblBudgetItem.Text.ToString = "Y" Then
            chkBudgetItem.Checked = True
        Else
            chkBudgetItem.Checked = False
        End If

        Dim lblUsable_Life_Span As New Label
        lblUsable_Life_Span = gvData.SelectedRow.FindControl("lblUsable_Life_Span")
        If lblUsable_Life_Span.Text.ToString = "Y" Then
            chkUsable_Life_Span.Checked = True
        Else
            chkUsable_Life_Span.Checked = False
        End If

        Dim lblLansweeperMapping As New Label
        lblLansweeperMapping = gvData.SelectedRow.FindControl("lblLansweeperMapping")
        If lblLansweeperMapping.Text.ToString = "Y" Then
            chkLansweeperMapping.Checked = True
        Else
            chkLansweeperMapping.Checked = False
        End If


    End Sub

    Protected Sub gvData_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvData.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim intID As Integer = 0
                Dim lbtnDelete As New LinkButton

                lblMsg.Text = ""

                lbtnDelete = gvData.Rows(e.RowIndex).FindControl("lbtnDelete")
                intID = CInt(lbtnDelete.CommandArgument.ToString)


                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteCategory", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@CategoryID", SqlDbType.Int)
                    .Parameters("@CategoryID").Value = intID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    lblID.Text = ""
                    lblMsg.Text = ""

                    btnClear_Click(sender, e)
                    GetSubCategoryData()

                    'lblMsg.Text = "Delete Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvData_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim intID As Integer = 0
                Dim strMsg As String = ""

                lblMsg.Text = ""

                If lblID.Text.ToString = "" Then
                    intID = 0
                Else
                    intID = CInt(lblID.Text.ToString)
                End If

                If ddlGroup.SelectedValue.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Group')</script>")
                    ddlGroup.Focus()
                    Exit Sub
                End If

                If ddlCategory.SelectedValue.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Category')</script>")
                    ddlCategory.Focus()
                    Exit Sub
                End If

                If txtSubCategory.Text.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter SubCategory')</script>")
                    txtSubCategory.Focus()
                    Exit Sub
                End If

                If txtDescription.Text.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter SubCategory Description')</script>")
                    txtDescription.Focus()
                    Exit Sub
                End If

                If txtDepreciationPeriod.Text.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Depreciation Period')</script>")
                    txtDepreciationPeriod.Focus()
                    Exit Sub
                End If

                If txteReqPricePoint.Text.ToString <> "" Then
                    If CDec(txteReqPricePoint.Text.ToString) <= 0 Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid eRequisition Price Point Control')</script>")
                        txteReqPricePoint.Focus()
                        Exit Sub
                    End If

                    If ddlCurrencyCode.SelectedValue.ToString = "0" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Currency Code')</script>")
                        ddlCurrencyCode.Focus()
                        Exit Sub
                    End If
                End If

                If ddlCurrencyCode.SelectedValue.ToString <> "0" Then
                    If txteReqPricePoint.Text.ToString = "" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter eRequisition Price Point')</script>")
                        ddlCurrencyCode.Focus()
                        Exit Sub
                    End If
                End If



                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveSubCategory", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@GroupID", SqlDbType.BigInt)
                    .Parameters("@GroupID").Value = CInt(ddlGroup.SelectedValue.ToString)
                    .Parameters.Add("@ParentCategoryID", SqlDbType.BigInt)
                    .Parameters("@ParentCategoryID").Value = CInt(ddlCategory.SelectedValue.ToString)
                    .Parameters.Add("@CategoryID", SqlDbType.BigInt)
                    .Parameters("@CategoryID").Value = intID
                    .Parameters.Add("@Category", SqlDbType.NVarChar, 100)
                    .Parameters("@Category").Value = txtSubCategory.Text.ToString
                    .Parameters.Add("@Description", SqlDbType.NVarChar)
                    .Parameters("@Description").Value = txtDescription.Text.ToString

                    .Parameters.Add("@Send_eReq", SqlDbType.TinyInt)
                    If chkSendeReq.Checked = True Then
                        .Parameters("@Send_eReq").Value = 1
                    Else
                        .Parameters("@Send_eReq").Value = 0
                    End If

                    .Parameters.Add("@Send_eReq_PerUnit", SqlDbType.TinyInt)
                    If chkSendeReqPerUnit.Checked = True Then
                        .Parameters("@Send_eReq_PerUnit").Value = 1
                    Else
                        .Parameters("@Send_eReq_PerUnit").Value = 0
                    End If

                    .Parameters.Add("@CurrencyCode", SqlDbType.Int)
                    If ddlCurrencyCode.SelectedValue.ToString <> "0" Then
                        .Parameters("@CurrencyCode").Value = ddlCurrencyCode.SelectedValue.ToString
                    Else
                        .Parameters("@CurrencyCode").Value = DBNull.Value
                    End If


                    .Parameters.Add("@eReqPricePointControl", SqlDbType.Decimal, 18, 2)
                    If txteReqPricePoint.Text.ToString = "" Then
                        .Parameters("@eReqPricePointControl").Value = DBNull.Value
                    Else
                        .Parameters("@eReqPricePointControl").Value = CDec(txteReqPricePoint.Text.ToString)
                    End If


                    .Parameters.Add("@Is_Budget_Item", SqlDbType.TinyInt)
                    If chkBudgetItem.Checked = True Then
                        .Parameters("@Is_Budget_Item").Value = 1
                    Else
                        .Parameters("@Is_Budget_Item").Value = 0
                    End If

                    .Parameters.Add("@Usable_Life_Span", SqlDbType.TinyInt)
                    If chkUsable_Life_Span.Checked = True Then
                        .Parameters("@Usable_Life_Span").Value = 1
                    Else
                        .Parameters("@Usable_Life_Span").Value = 0
                    End If

                    .Parameters.Add("@DepreciationPeriod", SqlDbType.Int)
                    .Parameters("@DepreciationPeriod").Value = CInt(txtDepreciationPeriod.Text.ToString)

                    .Parameters.Add("@LansweeperMapping", SqlDbType.TinyInt)
                    If chkLansweeperMapping.Checked = True Then
                        .Parameters("@LansweeperMapping").Value = 1
                    Else
                        .Parameters("@LansweeperMapping").Value = 0
                    End If

                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    btnClear_Click(sender, e)
                    GetSubCategoryData()

                    'lblMsg.Text = "Save Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByID")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[Category],[CategoryItemType]"
                    .Parameters.Add("@ID", SqlDbType.BigInt)
                    .Parameters("@ID").Value = CInt(ddlGroup.SelectedValue.ToString)
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(1).TableName = "Category"


                ddlCategory.DataSource = MyData.Tables(1)
                ddlCategory.DataValueField = MyData.Tables(1).Columns("CategoryID").ToString
                ddlCategory.DataTextField = MyData.Tables(1).Columns("Category").ToString
                ddlCategory.DataBind()

                chkSendeReqPerUnit.Enabled = False
                chkSendeReq.Enabled = False
                ddlCurrencyCode.Enabled = False
                txteReqPricePoint.Enabled = False
                chkLansweeperMapping.Enabled = False

                If MyData.Tables(0).Rows.Count > 0 Then
                    If MyData.Tables(0).Rows(0)("ItemType").ToString = "IT" Then

                        chkSendeReq.Enabled = True
                        chkSendeReqPerUnit.Enabled = True
                        chkLansweeperMapping.Enabled = True

                        If chkSendeReq.Checked = False Then
                            'chkSendeReqPerUnit.Checked = False
                            'chkSendeReqPerUnit.Enabled = False
                            ddlCurrencyCode.Enabled = True
                            txteReqPricePoint.Enabled = True
                        End If

                        'chkSendeReq.Enabled = True
                        'ddlCurrencyCode.Enabled = True
                        'txteReqPricePoint.Enabled = True

                        'If chkSendeReq.Checked = True Then
                        '    chkSendeReqPerUnit.Enabled = True
                        'Else
                        '    chkSendeReqPerUnit.Enabled = False
                        'End If
                    Else
                        chkSendeReqPerUnit.Enabled = False
                        chkSendeReqPerUnit.Checked = False
                        chkSendeReq.Checked = False
                        ddlCurrencyCode.SelectedValue = "0"
                        txteReqPricePoint.Text = ""
                        chkLansweeperMapping.Enabled = False
                        chkLansweeperMapping.Checked = False

                        'chkSendeReq.Checked = False
                        'txteReqPricePoint.Text = ""
                        'ddlCurrencyCode.SelectedValue = "0"
                    End If
                Else
                    chkSendeReqPerUnit.Checked = False
                    chkSendeReq.Checked = False
                    txteReqPricePoint.Text = ""
                    ddlCurrencyCode.SelectedValue = "0"
                    chkLansweeperMapping.Checked = False
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "ddlGroup_SelectedIndexChanged: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        If lblID.Text.ToString = "" Then
            GetParentCategorySetting()
        End If
    End Sub

    Private Sub GetParentCategorySetting()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetParentCategorySetting")

                With MyCommand
                    .Parameters.Add("@ParentCategory", SqlDbType.Int)
                    .Parameters("@ParentCategory").Value = CInt(ddlCategory.SelectedValue.ToString)
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "ParentCategorySetting"

                txteReqPricePoint.Text = ""
                ddlCurrencyCode.SelectedValue = "0"
                txtDepreciationPeriod.Text = ""
                chkSendeReq.Checked = False
                chkBudgetItem.Checked = False
                chkUsable_Life_Span.Checked = False
                chkSendeReqPerUnit.Checked = False
                chkLansweeperMapping.Checked = False

                If MyData.Tables(0).Rows.Count > 0 Then
                    txtDepreciationPeriod.Text = MyData.Tables(0).Rows(0)("DepreciationPeriod").ToString

                    If MyData.Tables(0).Rows(0)("Send_eReq").ToString = "Y" Then
                        chkSendeReq.Checked = True
                        ddlCurrencyCode.Enabled = False
                        txteReqPricePoint.Enabled = False
                        chkSendeReqPerUnit.Enabled = True
                    Else
                        chkSendeReq.Checked = False
                        ddlCurrencyCode.Enabled = True
                        txteReqPricePoint.Enabled = True
                    End If

                    If MyData.Tables(0).Rows(0)("Send_eReq_PerUnit").ToString = "Y" Then
                        chkSendeReqPerUnit.Checked = True
                    Else
                        chkSendeReqPerUnit.Checked = False
                    End If

                    If MyData.Tables(0).Rows(0)("Is_Budget_Item").ToString = "Y" Then
                        chkBudgetItem.Checked = True
                    Else
                        chkBudgetItem.Checked = False
                    End If
                    If MyData.Tables(0).Rows(0)("Usable_Life_Span").ToString = "Y" Then
                        chkUsable_Life_Span.Checked = True
                    Else
                        chkUsable_Life_Span.Checked = False
                    End If

                    If MyData.Tables(0).Rows(0)("LansweeperMapping").ToString = "Y" Then
                        chkLansweeperMapping.Checked = True
                    Else
                        chkLansweeperMapping.Checked = False
                    End If

                    If MyData.Tables(0).Rows(0)("CurrencyCodeID").ToString <> "" Then
                        ddlCurrencyCode.SelectedValue = MyData.Tables(0).Rows(0)("CurrencyCodeID").ToString
                    End If
                    txteReqPricePoint.Text = MyData.Tables(0).Rows(0)("PricePointeRequisition").ToString

                    If MyData.Tables(0).Rows(0)("ItemType").ToString = "IT" Then
                        chkSendeReq.Enabled = True
                        chkSendeReqPerUnit.Enabled = True
                        chkLansweeperMapping.Enabled = True
                        If chkSendeReq.Checked = False Then
                            ddlCurrencyCode.Enabled = True
                            txteReqPricePoint.Enabled = True
                        End If
                    Else
                        chkSendeReqPerUnit.Enabled = False
                        chkSendeReqPerUnit.Checked = False
                        chkSendeReq.Enabled = False
                        chkSendeReq.Checked = False
                        ddlCurrencyCode.SelectedValue = "0"
                        txteReqPricePoint.Text = ""
                        ddlCurrencyCode.Enabled = False
                        txteReqPricePoint.Enabled = False
                        chkLansweeperMapping.Enabled = False
                        chkLansweeperMapping.Checked = False
                    End If

                End If

            Catch ex As Exception
                Me.lblMsg.Text = "GetParentCategorySetting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub chkSendeReq_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSendeReq.CheckedChanged
        If chkSendeReq.Checked = True Then
            ddlCurrencyCode.SelectedValue = "0"
            txteReqPricePoint.Text = ""
            ddlCurrencyCode.Enabled = False
            txteReqPricePoint.Enabled = False
            chkSendeReqPerUnit.Enabled = True
        Else
            ddlCurrencyCode.Enabled = True
            txteReqPricePoint.Enabled = True
        End If

    End Sub

End Class
