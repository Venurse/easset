<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmSubCategory.aspx.vb" Inherits="System_frmSubCategory" %>

<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Sub Category</title>
    <script type="text/javascript">
         function showDate() 
        {
            var month_names = new Array("Jan", "Feb", "Mar", 
            "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
            "Oct", "Nov", "Dec");

            var d = new Date();
            var curr_day = d.getDay();
            var curr_date = d.getDate();
            var hours = d.getHours();
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            
            var curr_hours;
            var curr_minutes;
            var curr_seconds;

            if(hours<10)
            {
                curr_hours = "0" + hours;
            }
            else
            {
                curr_hours = hours;
            }
            
            if(minutes < 10)
            {
                curr_minutes = "0" + minutes;
            }
             else
            {
                curr_minutes = minutes;
            }
            
           if(seconds < 10)
            {
                curr_seconds = "0" + seconds;
            }
             else
            {
                curr_seconds = seconds;
            }

            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
 
            document.getElementById("hdfCurrentDateTime").value = curr_date + " " +  month_names[curr_month] +  " " + curr_year + " " + curr_hours + ":" + curr_minutes + ":" + curr_seconds
         }
     </script>
</head>
<body>
    <form id="frmSubCategory" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Sub Category"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:HiddenField ID="hdfCurrentDateTime" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <table>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="Label10" runat="server" Font-Names="Verdana" Font-Size="X-Small">ID</asp:Label></td>
                            <td style="font-size: 12pt; width: 1px; font-family: Times New Roman">
                                :</td>
                            <td>
                    <asp:Label ID="lblID" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td style="width: 100px">
                                <asp:Label ID="Label9" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Group"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True" Font-Names="Verdana"
                                    Font-Size="X-Small">
                                </asp:DropDownList></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Category"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:DropDownList ID="ddlCategory" runat="server" Font-Names="Verdana" Font-Size="X-Small" AutoPostBack="True">
                    </asp:DropDownList></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Sub Category Code"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:TextBox ID="txtSubCategory" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        MaxLength="100" Width="350px"></asp:TextBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Sub Category Description"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:TextBox ID="txtDescription" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        MaxLength="200" Width="450px"></asp:TextBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                            </td>
                            <td style="width: 1px">
                            </td>
                            <td>
                    <asp:Button ID="btnSearch" runat="server" Font-Names="Verdana" Font-Size="Small"
                        Text="Search" Width="65px" /></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label8" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Depreciation Period"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <ew:numericbox id="txtDepreciationPeriod" runat="server" width="100px" Font-Names="Verdana" Font-Size="X-Small"></ew:numericbox>
                                <asp:Label ID="Label11" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Month"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Send eRequisition"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:CheckBox ID="chkSendeReq" runat="server" AutoPostBack="True" />&nbsp;&nbsp; &nbsp; &nbsp;
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label45" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="eRequisition Price Point Control"
                                    Width="110px"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlCurrencyCode" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList>
                                <ew:NumericBox ID="txteReqPricePoint" runat="server" Font-Names="Verdana" Font-Size="X-Small"></ew:NumericBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label12" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Send eRequisition Per Unit"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:CheckBox ID="chkSendeReqPerUnit" runat="server" Font-Names="Verdana" Font-Size="X-Small" /></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label6" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Budget Item"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:CheckBox ID="chkBudgetItem" runat="server" /></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label7" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Usable Life Span > 1 Year"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:CheckBox ID="chkUsable_Life_Span" runat="server" /></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label46" runat="server" Font-Names="Verdana" Font-Size="X-Small" 
                                    Text="Lansweeper Mapping"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:CheckBox ID="chkLansweeperMapping" runat="server" /></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                            </td>
                            <td style="width: 1px">
                            </td>
                            <td>
                    <table style="width: 335px">
                        <tr>
                            <td style="width: 150px">
                    <asp:Button ID="btnSave" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Save"
                        Width="65px" OnClientClick="showDate();" /></td>
                            <td style="width: 150px">
                    <asp:Button ID="btnClear" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Clear"
                        Width="65px" /></td>
                            <td style="width: 150px">
                    </td>
                        </tr>
                    </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="font-size: 12pt; font-family: Times New Roman">
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        Font-Names="Verdana" Font-Size="X-Small" ForeColor="#333333" GridLines="Vertical" DataKeyNames="CategoryID">
                        <RowStyle BackColor="#EFF3FB" />
                        <Columns>
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CausesValidation="False" CommandArgument='<%# Bind("CategoryID") %>'
                                        CommandName="Delete" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this SubCategory?');"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Group">
                                <ItemTemplate>
                                    <asp:Label ID="lblGroup" runat="server" Text='<%# Bind("Group_Name") %>'></asp:Label>
                                    <asp:HiddenField ID="hdfGroupID" runat="server" Value='<%# Bind("GroupID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category">
                                <ItemTemplate>
                                    <asp:Label ID="lblParentCategory" runat="server" Text='<%# Bind("ParentCategory") %>'></asp:Label>
                                    <asp:HiddenField ID="hdParentCategoryID" runat="server" Value='<%# Bind("ParentCategoryID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sub Category Code">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnSubCategory" runat="server" CausesValidation="False" CommandArgument='<%# Bind("CategoryID") %>'
                                        CommandName="Select" Text='<%# Bind("Category") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sub Category Description">
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Depreciation Period (Month)">
                                <ItemTemplate>
                                    <asp:Label ID="lblDepreciationPeriod" runat="server" Text='<%# bind("DepreciationPeriod") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Send eReq">
                                <ItemTemplate>
                                    <asp:Label ID="lblSendeReq" runat="server" Text='<%# Bind("Send_eReq") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Send eReq Per Unit">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Send_eReq_PerUnit") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblSend_eReq_PerUnit" runat="server" Text='<%# Bind("Send_eReq_PerUnit") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="eRequisition Price Point Control">
                                <ItemTemplate>
                                    <asp:Label ID="lbleReqPricePointCtrl" runat="server" Text='<%# Bind("PricePointeRequisition") %>'></asp:Label><br />
                                    <asp:HiddenField ID="hdfCurrencyCodeID" runat="server" Value='<%# Bind("CurrencyCodeID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Budget Item">
                                <ItemTemplate>
                                    <asp:Label ID="lblBudgetItem" runat="server" Text='<%# Bind("Is_Budget_Item") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Usable Life Span">
                                <ItemTemplate>
                                    <asp:Label ID="lblUsable_Life_Span" runat="server" Text='<%# Bind("Usable_Life_Span") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lansweeper Mapping">
                                <ItemTemplate>
                                    <asp:Label ID="lblLansweeperMapping" runat="server" 
                                        Text='<%# Bind("LansweeperMapping") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" 
                                        Text='<%# Bind("LansweeperMapping") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
