Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class System_frmVendor
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If IsPostBack() = False Then

            Session("DA_CompanyID") = ""
            Session("DA_CompanyCode") = ""
            Session("DA_CompanyName") = ""

            'If Session("DA_EmployeeStatus").ToString.ToUpper <> "ACTIVE" Then
            '    btnRemoveSetting.Visible = True
            'Else
            '    btnRemoveSetting.Visible = False
            'End If

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            btnClear_Click(sender, e)
            btnSave.Enabled = False
            btnSave_D.Enabled = False
            gvData.Columns(0).Visible = False
            gvContactDetail.Columns(0).Visible = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "S0013")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If

            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Vendor page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnSave.Enabled = True
                        btnSave_D.Enabled = True
                        gvData.Columns(0).Visible = True
                        gvContactDetail.Columns(0).Visible = True
                    Else
                        Session("blnWrite") = False
                        btnSave.Enabled = False
                        btnSave_D.Enabled = False
                        gvData.Columns(0).Visible = False
                        gvContactDetail.Columns(0).Visible = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                    'btnSave.Enabled = True
                Else
                    Session("blnModifyOthers") = False
                    'btnSave.Enabled = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            GetVendorSetting()

        Else
            If Session("DA_CompanyID") <> "" Then
                txtVendorCode.Text = Session("DA_CompanyCode").ToString
                hdfCustomerID.Value = Session("DA_CompanyID").ToString
                txtVendorName.Text = Session("DA_CompanyName").ToString

                Session("DA_CompanyID") = ""
                Session("DA_CompanyCode") = ""
                Session("DA_CompanyName") = ""
            End If

        End If
    End Sub

    Private Sub GetVendorSetting()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetVendorSetting")

                With MyCommand
                    .Parameters.Add("@CustomerID", SqlDbType.BigInt)
                    If hdfCustomerID.Value.ToString = "" Then
                        .Parameters("@CustomerID").Value = 0
                    Else
                        .Parameters("@CustomerID").Value = CInt(hdfCustomerID.Value.ToString)
                    End If

                    .Parameters.Add("@CustomerCode", SqlDbType.NVarChar, 50)
                    .Parameters("@CustomerCode").Value = txtVendorCode.Text.ToString
                    .Parameters.Add("@CustomerName", SqlDbType.NVarChar, 500)
                    .Parameters("@CustomerName").Value = txtVendorName.Text.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)


                gvData.DataSource = MyData.Tables(0)
                gvData.DataMember = MyData.Tables(0).TableName
                gvData.DataBind()

                'gvContactDetail.DataSource = MyData.Tables(1)
                'gvContactDetail.DataMember = MyData.Tables(1).TableName
                'gvContactDetail.DataBind()

                'Session("VendorContactDetail") = MyData.Tables(1)

            Catch ex As Exception
                Me.lblMsg.Text = "GetVendorSetting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnVendor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVendor.Click
        Dim strScript As String

        If hdfCustomerID.Value.ToString = "" Then
            Session("DA_CompanyID") = ""
        Else
            Session("DA_CompanyID") = hdfCustomerID.Value.ToString
        End If

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmCompany.aspx?ParentForm=frmVendor','AddVendor','height=550, width=500,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try

                Dim intID As Integer = 0
                Dim strMsg As String = ""

                lblMsg.Text = ""

                If txtVendorCode.Text.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Vendor Code')</script>")
                    'lblMsg.Text = "Please enter Vendor Code."
                    Exit Sub
                End If

                If txtVendorName.Text.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Vendor Name')</script>")
                    'lblMsg.Text = "Please enter Vendor Name."
                    Exit Sub
                End If

                If hdfCustomerID.Value.ToString = "" Or hdfCustomerID.Value.ToString = "0" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Vendor from list')</script>")
                    'lblMsg.Text = "Please enter Vendor Name."
                    Exit Sub
                End If

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveVendor", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString

                    .Parameters.Add("@VendorID", SqlDbType.BigInt)
                    If lblVendorID.Text.ToString = "" Then
                        .Parameters("@VendorID").Value = 0
                    Else
                        .Parameters("@VendorID").Value = CInt(lblVendorID.Text.ToString)
                    End If

                    .Parameters.Add("@VendorCode", SqlDbType.NVarChar, 50)
                    .Parameters("@VendorCode").Value = txtVendorCode.Text.ToString

                    .Parameters.Add("@CustomerID", SqlDbType.BigInt)
                    If hdfCustomerID.Value.ToString = "" Then
                        .Parameters("@CustomerID").Value = 0
                    Else
                        .Parameters("@CustomerID").Value = CInt(hdfCustomerID.Value.ToString)
                    End If

                    .Parameters.Add("@VendorName", SqlDbType.NVarChar, 500)
                    .Parameters("@VendorName").Value = txtVendorName.Text.ToString

                    .Parameters.Add("@ContactNo", SqlDbType.NVarChar, 50)
                    .Parameters("@ContactNo").Value = txtVendorContactNo.Text.ToString

                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    btnClear_Click(sender, e)
                    GetVendorSetting()
                    'lblMsg.Text = "Save Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        GetVendorSetting()
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        lblVendorID.Text = "0"
        txtVendorCode.Text = ""
        hdfCustomerID.Value = ""
        txtVendorName.Text = ""
        txtVendorContactNo.Text = ""
        btnVendor.Enabled = True
        btnClear_D_Click(sender, e)
    End Sub

    Protected Sub btnClear_D_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear_D.Click
        lblContactID.Text = ""
        txtName.Text = ""
        txtEmail.Text = ""
        txtContactNo.Text = ""

    End Sub

    Protected Sub btnSave_D_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave_D.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim intID As Integer = 0
                Dim strMsg As String = ""


                lblMsg.Text = ""

                If lblVendorID.Text.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select vendor')</script>")
                    'lblMsg.Text = "Please select vendor."
                    Exit Sub
                End If

                If txtName.Text.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Contact Name')</script>")
                    'lblMsg.Text = "Please enter Contact Name."
                    Exit Sub
                End If

                If txtEmail.Text.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Email')</script>")
                    'lblMsg.Text = "Please enter Email."
                    Exit Sub
                End If

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveVendorContact", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString

                    .Parameters.Add("@VendorID", SqlDbType.BigInt)
                    If lblVendorID.Text.ToString = "" Then
                        .Parameters("@VendorID").Value = 0
                    Else
                        .Parameters("@VendorID").Value = CInt(lblVendorID.Text.ToString)
                    End If

                    .Parameters.Add("@VendorContactID", SqlDbType.BigInt)
                    If lblContactID.Text.ToString = "" Then
                        .Parameters("@VendorContactID").Value = 0
                    Else
                        .Parameters("@VendorContactID").Value = CInt(lblContactID.Text.ToString)
                    End If


                    .Parameters.Add("@Name", SqlDbType.NVarChar, 100)
                    .Parameters("@Name").Value = txtName.Text.ToString

                    .Parameters.Add("@Email", SqlDbType.NVarChar, 100)
                    .Parameters("@Email").Value = txtEmail.Text.ToString

                    .Parameters.Add("@ContactNo", SqlDbType.NVarChar, 50)
                    .Parameters("@ContactNo").Value = txtContactNo.Text.ToString

                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    btnClear_D_Click(sender, e)
                    GetVendorContactDetail(CInt(lblVendorID.Text.ToString))
                    'lblMsg.Text = "Save Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnSave_D_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub gvData_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvData.SelectedIndexChanged
        btnClear_Click(sender, e)
        btnClear_D_Click(sender, e)

        Dim intID As Integer = 0
        intID = CInt(gvData.SelectedDataKey.Value.ToString)
        lblVendorID.Text = intID

        Dim lbtnVendorCode As New LinkButton
        lbtnVendorCode = gvData.SelectedRow.FindControl("lbtnVendorCode")
        txtVendorCode.Text = lbtnVendorCode.Text.ToString

        Dim hdf_gvCustomerID As New HiddenField
        hdf_gvCustomerID = gvData.SelectedRow.FindControl("hdf_gvCustomerID")
        hdfCustomerID.Value = hdf_gvCustomerID.Value.ToString

        Dim lblVendorName As New Label
        lblVendorName = gvData.SelectedRow.FindControl("lblVendorName")
        txtVendorName.Text = lblVendorName.Text.ToString

        Dim lblContactNo As New Label
        lblContactNo = gvData.SelectedRow.FindControl("lblContactNo")
        txtVendorContactNo.Text = lblContactNo.Text.ToString


        GetVendorContactDetail(CInt(lblVendorID.Text.ToString))
        btnVendor.Enabled = False
    End Sub

    Private Sub GetVendorContactDetail(ByVal intVendorID As Integer)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetVendorContactDetail")

                With MyCommand
                    .Parameters.Add("@VendorID", SqlDbType.BigInt)
                    .Parameters("@VendorID").Value = intVendorID
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)

                gvContactDetail.DataSource = MyData.Tables(0)
                gvContactDetail.DataMember = MyData.Tables(0).TableName
                gvContactDetail.DataBind()


            Catch ex As Exception
                Me.lblMsg.Text = "GetVendorContactDetail: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using

        'Dim dtVendorContactDetail As New DataTable
        'dtVendorContactDetail = Session("VendorContactDetail")

        'dtVendorContactDetail.Select("VendorID = '" + intVendorID.ToString + "'")

        'gvContactDetail.DataSource = dtVendorContactDetail
        'gvContactDetail.DataMember = dtVendorContactDetail.TableName
        'gvContactDetail.DataBind()

    End Sub

    Protected Sub gvData_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvData.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim intID As Integer = 0
                Dim lbtnDelete As New LinkButton

                lblMsg.Text = ""

                lbtnDelete = gvData.Rows(e.RowIndex).FindControl("lbtnDelete")
                intID = CInt(lbtnDelete.CommandArgument.ToString)

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteVendor", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ID", SqlDbType.BigInt)
                    .Parameters("@ID").Value = intID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    btnClear_Click(sender, e)
                    btnClear_D_Click(sender, e)
                    GetVendorSetting()
                    GetVendorContactDetail(CInt(lblVendorID.Text.ToString))
                    'lblMsg.Text = "Delete Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvData_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub gvContactDetail_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvContactDetail.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String

                Dim intID As Integer = 0
                Dim lbtnDelete As New LinkButton

                lblMsg.Text = ""

                lbtnDelete = gvContactDetail.Rows(e.RowIndex).FindControl("lbtnDelete")
                intID = CInt(lbtnDelete.CommandArgument.ToString)

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteVendorContact", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ID", SqlDbType.BigInt)
                    .Parameters("@ID").Value = intID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    btnClear_D_Click(sender, e)
                    If lblVendorID.Text.ToString = "" Then
                        GetVendorSetting()
                    Else
                        GetVendorContactDetail(CInt(lblVendorID.Text.ToString))
                    End If

                    'lblMsg.Text = "Delete Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvContactDetail_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub gvContactDetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvContactDetail.SelectedIndexChanged
        Dim intID As Integer = 0
        intID = CInt(gvContactDetail.SelectedDataKey.Value.ToString)
        lblContactID.Text = intID

        Dim lbtnName As New LinkButton
        lbtnName = gvContactDetail.SelectedRow.FindControl("lbtnName")
        txtName.Text = lbtnName.Text.ToString

        Dim lblEmail As New Label
        lblEmail = gvContactDetail.SelectedRow.FindControl("lblEmail")
        txtEmail.Text = lblEmail.Text.ToString

        Dim lblContactNo As New Label
        lblContactNo = gvContactDetail.SelectedRow.FindControl("lblContactNo")
        txtContactNo.Text = lblContactNo.Text.ToString

    End Sub
End Class
