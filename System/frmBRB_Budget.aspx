<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmBRB_Budget.aspx.vb" Inherits="System_frmBRB_Budget" %>

<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Excel Budget</title>
</head>
<body onunload="window.opener.document.frmBudgetList.submit();">
    <form id="frmBRB_Budget" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Budget List (Excel)"></asp:Label></td>
                <td style="font-weight: bold; font-size: 12pt; font-family: Times New Roman">
                </td>
            </tr>
            <tr style="font-weight: bold; font-size: 12pt; font-family: Times New Roman">
                <td style="height: 21px">
                </td>
                <td colspan="7" style="height: 21px">
                    <asp:HiddenField ID="hdfCurrentDateTime" runat="server" />
                </td>
                <td style="height: 21px">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <table>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td style="width: 100px">
                                <asp:Label ID="Label10" runat="server" Font-Names="Verdana" Font-Size="X-Small">ID</asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:Label ID="lblID" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label>
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td style="width: 100px">
                                <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Station"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlStation" runat="server" AutoPostBack="True" Font-Names="Verdana"
                                    Font-Size="X-Small" Enabled="False">
                                </asp:DropDownList></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Year"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" Font-Names="Verdana"
                                    Font-Size="X-Small" Enabled="False">
                                </asp:DropDownList></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label12" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Description"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:Label ID="lblBRBDesc" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label11" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Type"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True" Font-Names="Verdana"
                                    Font-Size="X-Small" Enabled="False">
                                    <asp:ListItem>Category</asp:ListItem>
                                    <asp:ListItem>Operating Expenses Budget</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Category"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True" Font-Names="Verdana"
                                    Font-Size="X-Small">
                                </asp:DropDownList></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Sub Category"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlSubCategory" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label6" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Quantity"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <ew:numericbox id="txtQuantity" runat="server" font-names="Verdana" font-size="X-Small"
                                    positivenumber="True" Enabled="False"></ew:numericbox>
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label7" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Quantity Balance"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <ew:numericbox id="txtBalance" runat="server" enabled="False" font-names="Verdana"
                                    font-size="X-Small"></ew:numericbox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="lblAmountPerUnit" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Amount Per Unit"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlCurrency" runat="server" Enabled="False" Font-Names="Verdana"
                                    Font-Size="X-Small">
                                </asp:DropDownList>
                                <ew:numericbox id="txtAmount" runat="server" font-names="Verdana" font-size="X-Small" Enabled="False"></ew:numericbox>
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label8" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Amount Balance"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <ew:numericbox id="txtAmtBalance" runat="server" enabled="False" font-names="Verdana"
                                    font-size="X-Small"></ew:numericbox>
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label9" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Remarks"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:TextBox ID="txtRemarks" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Height="50px" TextMode="MultiLine" Width="450px"></asp:TextBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                            </td>
                            <td style="width: 1px">
                            </td>
                            <td>
                                <table style="width: 335px">
                                    <tr>
                                        <td style="width: 150px">
                                            <asp:Button ID="btnSave" runat="server" Font-Names="Verdana" Font-Size="Small" OnClientClick="showDate();"
                                                Text="Save" Width="65px" /></td>
                                        <td style="width: 150px">
                                            <asp:Button ID="btnClear" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Clear"
                                                Width="65px" /></td>
                                        <td style="width: 150px">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="font-size: 12pt; font-family: Times New Roman">
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td colspan="5">
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        DataKeyNames="BRBBudgetID" Font-Names="Verdana" Font-Size="X-Small" ForeColor="#333333"
                        GridLines="Vertical">
                        <RowStyle BackColor="#EFF3FB" />
                        <Columns>
                            <asp:TemplateField HeaderText="Year">
                                <ItemTemplate>
                                    <asp:Label ID="lblYear" runat="server" Text='<%# Bind("Year") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Station">
                                <ItemTemplate>
                                    <asp:Label ID="lblStation" runat="server" Text='<%# Bind("StationCode") %>'></asp:Label>
                                    <asp:HiddenField ID="hdStationID" runat="server" Value='<%# Bind("StationID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnBRBDescription" runat="server" CommandArgument='<%# Bind("BRBBudgetID") %>'
                                        CommandName="Select" Text='<%# Bind("Description") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type" Visible="False">
                                <ItemTemplate>
                                    &nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("ParentCategory") %>'></asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Budget Item">
                                <ItemTemplate>
                                    <asp:Label ID="lblSubCategory" runat="server" Text='<%# Bind("SubCategory") %>'></asp:Label>
                                    <asp:LinkButton ID="lbtnSubCategory" runat="server" CausesValidation="False" CommandArgument='<%# Bind("BRBBudgetID") %>'
                                        CommandName="Select" Text='<%# Bind("SubCategory") %>' Visible="False"></asp:LinkButton>
                                    <asp:HiddenField ID="hdSubCategory" runat="server" Value='<%# Bind("SubCategoryID") %>' />
                                    <asp:HiddenField ID="hdCategory" runat="server" Value='<%# Bind("ParentCategoryID") %>' />
                                    <asp:Label ID="lblType" runat="server" Text='<%# Bind("Type") %>' Visible="False"></asp:Label>
                                    <asp:HiddenField ID="hdfBRBBudgetID" runat="server" Value='<%# Bind("BRBBudgetID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quantity">
                                <ItemTemplate>
                                    <asp:Label ID="lblQuantity" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty Balance">
                                <ItemTemplate>
                                    <asp:Label ID="lblBalance" runat="server" Text='<%# Bind("Qty_Balance") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Currency">
                                <ItemTemplate>
                                    <asp:Label ID="lblCurrency" runat="server" Text='<%# Bind("CurrencyCode") %>'></asp:Label>
                                    <asp:HiddenField ID="hdfCurrencyCodeID" runat="server" Value='<%# Bind("CurrencyCodeID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount">
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("Amount") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount Balance">
                                <ItemTemplate>
                                    <asp:Label ID="lblAmountBalance" runat="server" Text='<%# Bind("Amount_Balance") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remarks">
                                <ItemTemplate>
                                    <asp:Label ID="lblRemark" runat="server" Text='<%# Bind("Remark") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ErrorMsg" HeaderText="Confirm - Error" />
                        </Columns>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Button ID="btnConfirm" runat="server" Font-Names="Verdana" Font-Size="Small"
                        Text="Confirm" /></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
