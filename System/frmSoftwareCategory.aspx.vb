Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class System_frmSoftwareCategory
    Inherits System.Web.UI.Page

    Dim dsStation As New DataSet

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If IsPostBack() = False Then

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            btnSave.Enabled = False
            gvData.Columns(0).Visible = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "S0010")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Software Category Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnSave.Enabled = True
                        gvData.Columns(0).Visible = True
                    Else
                        Session("blnWrite") = False
                        btnSave.Enabled = False
                        gvData.Columns(0).Visible = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                    'btnSave.Visible = True
                    'gvData.Columns(0).Visible = True
                Else
                    Session("blnModifyOthers") = False
                    'btnSave.Visible = False
                    'gvData.Columns(0).Visible = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                    'gvData.Columns(0).Visible = True
                Else
                    Session("blnDeleteOthers") = False
                    'gvData.Columns(0).Visible = False
                End If
            End If

            GetSoftwareCategory()
        Else

        End If

    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        lblMsg.Text = ""
        lblID.Text = ""
        txtSoftwareCategory.Text = ""
    End Sub

    Private Sub GetSoftwareCategory()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetSoftwareCategory")

                With MyCommand
                    .Parameters.Add("@SoftwareCategory", SqlDbType.NVarChar, 200)
                    .Parameters("@SoftwareCategory").Value = txtSoftwareCategory.Text.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "SoftwareCategory"

                gvData.DataSource = MyData
                gvData.DataMember = MyData.Tables(0).TableName
                gvData.DataBind()


            Catch ex As Exception
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('GetSoftwareCategory: " & ex.Message.ToString & "')</script>")
                Exit Sub
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim i As Integer = 0
                Dim intID As Integer = 0
                Dim strMsg As String = ""
                Dim strSoftwareCategory As String = ""

                lblMsg.Text = ""

                If lblID.Text.Equals("") Then
                    intID = 0
                Else
                    intID = CInt(lblID.Text)
                End If

                strSoftwareCategory = txtSoftwareCategory.Text.ToString

                If strSoftwareCategory = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please key in Software Category')</script>")
                    txtSoftwareCategory.Focus()
                    Exit Sub
                End If

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveSoftwareCategory", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@ID", SqlDbType.Int)
                    If lblID.Text.ToString = "" Then
                        .Parameters("@ID").Value = 0
                    Else
                        .Parameters("@ID").Value = CInt(lblID.Text.ToString)
                    End If
                    .Parameters.Add("@SoftwareCategory", SqlDbType.NVarChar, 200)
                    .Parameters("@SoftwareCategory").Value = strSoftwareCategory
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With


                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    btnClear_Click(sender, e)
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                    GetSoftwareCategory()

                End If
            Catch ex As Exception
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('btnSave_Click: " & ex.Message.ToString & "')</script>")
                Exit Sub
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        GetSoftwareCategory()
    End Sub

    Protected Sub gvData_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvData.SelectedIndexChanged
        Dim intID As Integer = 0
        intID = CInt(gvData.SelectedDataKey.Value.ToString)
        lblID.Text = intID

        Dim lbtnSoftwareCategory As New LinkButton
        lbtnSoftwareCategory = gvData.SelectedRow.FindControl("lbtnSoftwareCategory")
        txtSoftwareCategory.Text = lbtnSoftwareCategory.Text.ToString

    End Sub

    Protected Sub gvData_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvData.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim intID As Integer = 0
                Dim lbtnDelete As New LinkButton

                lblMsg.Text = ""

                lbtnDelete = gvData.Rows(e.RowIndex).FindControl("lbtnDelete")
                intID = CInt(lbtnDelete.CommandArgument.ToString)

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteSoftwareCategory", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ID", SqlDbType.BigInt)
                    .Parameters("@ID").Value = intID
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    btnClear_Click(sender, e)
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                    GetSoftwareCategory()
                End If
            Catch ex As Exception
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('gvData_RowDeleting: " & ex.Message.ToString & "')</script>")
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub
End Class
