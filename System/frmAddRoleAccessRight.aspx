<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAddRoleAccessRight.aspx.vb" Inherits="System_frmAddRoleAccessRight" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Access Right</title>
</head>
<body>
    <form id="frmAddRoleAccessRight" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Smaller"
                        Text="Role And Access Right"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                        Font-Underline="True" Text="Role"></asp:Label></td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="Label11" runat="server" Font-Names="Verdana" Font-Size="X-Small">ID</asp:Label></td>
                            <td style="font-size: 12pt; width: 1px; font-family: Times New Roman">
                                :</td>
                            <td style="font-size: 12pt; font-family: Times New Roman">
                    <asp:Label ID="lblID" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="ID"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td style="width: 100px">
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Role"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:TextBox ID="txtRole" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="242px"></asp:TextBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label3" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Role Description"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:TextBox ID="txtRoleDescription" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="365px"></asp:TextBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Active"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:CheckBox ID="chkActive" runat="server" Checked="True" /></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                            </td>
                            <td style="width: 1px">
                            </td>
                            <td>
                                <table style="width: 335px">
                                    <tr>
                                        <td style="width: 150px">
                    <asp:Button ID="btnSave" runat="server" Text="Save" Width="71px" /></td>
                                        <td colspan="2">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="font-size: 12pt; font-family: Times New Roman">
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <hr />
                    &nbsp;</td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                        Font-Underline="True" Text="Access Right"></asp:Label></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:GridView ID="grvRoleAccessRight" runat="server" AutoGenerateColumns="False"
                        BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px"
                        CellPadding="3" Font-Names="Verdana" Font-Size="X-Small" GridLines="Vertical"
                        PageSize="100">
                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                        <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                        <Columns>
                            <asp:TemplateField HeaderText="Module">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("nvchDisplayName") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblModule" runat="server" Text='<%# Bind("nvchDisplayName") %>'></asp:Label>
                                    <asp:HiddenField ID="hdfScreenID" runat="server" Value='<%# Bind("vchScreenID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Access">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    &nbsp;<asp:CheckBox ID="chkbAccess" runat="server" Checked='<%# bind("tiAccess") %>'
                                        OnCheckedChanged="chkbAccess_CheckedChanged" Enabled='<%# Bind("Access") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Read Only">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkbReadOnly" runat="server" Checked='<%# bind("tiReadOnly") %>'
                                        OnCheckedChanged="chkbReadOnly_CheckedChanged" Enabled='<%# Bind("ReadOnly") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Write">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkbWrite" runat="server" Checked='<%# Bind("tiWrite") %>' Enabled='<%# Bind("Write") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Modify Others">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkbModify" runat="server" Checked='<%# bind("tiModifyOthers") %>' Enabled='<%# Bind("ModifyOthers") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Delete Others">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkbDelete" runat="server" Checked='<%# Bind("tiDeleteOthers") %>' Enabled='<%# Bind("DeleteOthers") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="Gainsboro" />
                    </asp:GridView>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                    <asp:Button ID="btnARSave" runat="server" Text="Save" /></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
