Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class System_frmGroup
    Inherits System.Web.UI.Page

    Dim dsStation As New DataSet

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If IsPostBack() = False Then

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            btnSave.Enabled = False
            gvData.Columns(0).Visible = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "S0003")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Group Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnSave.Enabled = True
                        gvData.Columns(0).Visible = True
                    Else
                        Session("blnWrite") = False
                        btnSave.Enabled = False
                        gvData.Columns(0).Visible = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                    'btnSave.Visible = True
                    'gvData.Columns(0).Visible = True
                Else
                    Session("blnModifyOthers") = False
                    'btnSave.Visible = False
                    'gvData.Columns(0).Visible = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                    'gvData.Columns(0).Visible = True
                Else
                    Session("blnDeleteOthers") = False
                    'gvData.Columns(0).Visible = False
                End If
            End If

            GetAssetGroupData()
        Else

        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        lblMsg.Text = ""
        lblID.Text = ""
        txtGroup.Text = ""
        txtDescription.Text = ""
        ddlType.SelectedValue = "Select One"
    End Sub

    Private Sub GetAssetGroupData()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetAssetGroupData")

                With MyCommand
                    '.Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    '.Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AssetGroupData"


                gvData.DataSource = MyData.Tables(0)
                gvData.DataMember = MyData.Tables(0).TableName
                gvData.DataBind()


            Catch ex As Exception
                Me.lblMsg.Text = "GetAssetGroupData: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub gvData_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvData.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim intID As Integer = 0
                Dim lbtnDelete As New LinkButton

                lbtnDelete = gvData.Rows(e.RowIndex).FindControl("lbtnDelete")
                intID = CInt(lbtnDelete.CommandArgument.ToString)

                lblMsg.Text = ""

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteAssetGroup", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ID", SqlDbType.Int)
                    .Parameters("@ID").Value = intID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    lblID.Text = ""

                    btnClear_Click(sender, e)
                    GetAssetGroupData()

                    'lblMsg.Text = "Delete Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvData_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub gvData_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvData.SelectedIndexChanged
        Dim intID As Integer = 0
        intID = CInt(gvData.SelectedDataKey.Value.ToString)
        lblID.Text = intID

        Dim lbtnCategory As New LinkButton
        lbtnCategory = gvData.SelectedRow.FindControl("lbtnCategory")
        txtGroup.Text = lbtnCategory.Text.ToString

        Dim lblDescription As New Label
        lblDescription = gvData.SelectedRow.FindControl("lblDescription")
        txtDescription.Text = lblDescription.Text.ToString

        Dim lblItemType As New Label
        lblItemType = gvData.SelectedRow.FindControl("lblItemType")
        ddlType.SelectedValue = lblItemType.Text.ToString
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim intID As Integer = 0
                Dim strMsg As String = ""

                If lblID.Text.ToString = "" Then
                    intID = 0
                Else
                    intID = CInt(lblID.Text.ToString)
                End If

                If txtGroup.Text.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Group')</script>")
                    'lblMsg.Text = "Please enter Group."
                    Exit Sub
                End If

                If ddlType.SelectedValue.ToString = "Select One" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Type')</script>")
                    'lblMsg.Text = "Please select Type."
                    Exit Sub
                End If

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveAssetGroup", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@ID", SqlDbType.Int)
                    .Parameters("@ID").Value = intID
                    .Parameters.Add("@Group", SqlDbType.NVarChar, 50)
                    .Parameters("@Group").Value = txtGroup.Text.ToString
                    .Parameters.Add("@Group_Description", SqlDbType.NVarChar)
                    .Parameters("@Group_Description").Value = txtDescription.Text.ToString
                    .Parameters.Add("@Type", SqlDbType.NVarChar, 50)
                    .Parameters("@Type").Value = ddlType.SelectedValue.ToString
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    lblID.Text = ""
                    txtGroup.Text = ""
                    txtDescription.Text = ""
                    ddlType.SelectedValue = "Select One"
                    GetAssetGroupData()
                    'lblMsg.Text = "Save Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub
End Class
