Imports System.Data
Imports System.Data.SqlClient

Partial Class System_frmRole
    Inherits System.Web.UI.Page
    Dim dsRole As New DataSet
    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMsg.Text = ""

        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If IsPostBack() = False Then

            'Session("LastContentForm") = "System/frmRole.aspx"

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            lbtnAddRole.Visible = False
            grvRole.Columns(0).Visible = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "S0007")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Role page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        lbtnAddRole.Visible = True
                        grvRole.Columns(0).Visible = True
                    Else
                        Session("blnWrite") = False
                        lbtnAddRole.Visible = False
                        grvRole.Columns(0).Visible = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            GetRoleData()
        End If

    End Sub

    Private Sub GetRoleData()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("Proc_GetRole")

                With MyCommand
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "Role"

                dsRole.Tables.Add(MyData.Tables(0).Copy)
                dsRole.Tables(0).TableName = "Role"

                If dsRole.Tables(0).Rows.Count > 0 Then
                    grvRole.DataSource = dsRole
                    grvRole.DataMember = dsRole.Tables(0).TableName
                    grvRole.DataBind()

                    'If Session("blnModifyOthers") = False Then
                    '    grvRole.Columns(0).Visible = False
                    'Else
                    '    grvRole.Columns(0).Visible = True
                    'End If
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "GetRole: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub grvRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvRole.SelectedIndexChanged
        Dim strRoleID As String = ""
        strRoleID = grvRole.SelectedValue.ToString
        Session("vchRoleID") = strRoleID
        Response.Redirect("~/System/frmAddRoleAccessRight.aspx")
    End Sub

    Protected Sub lbtnAddRole_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnAddRole.Click
        Session("vchRoleID") = "999"
        Response.Redirect("~/System/frmAddRoleAccessRight.aspx")
    End Sub
End Class
