<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmStationSetting.aspx.vb" Inherits="System_frmStationSetting" %>

<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>

<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>

<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Station Setting</title>
     <script type="text/javascript">
         function showDate() 
        {
            var month_names = new Array("Jan", "Feb", "Mar", 
            "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
            "Oct", "Nov", "Dec");

            var d = new Date();
            var curr_day = d.getDay();
            var curr_date = d.getDate();
            var hours = d.getHours();
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            
            var curr_hours;
            var curr_minutes;
            var curr_seconds;

            if(hours<10)
            {
                curr_hours = "0" + hours;
            }
            else
            {
                curr_hours = hours;
            }
            
            if(minutes < 10)
            {
                curr_minutes = "0" + minutes;
            }
             else
            {
                curr_minutes = minutes;
            }
            
           if(seconds < 10)
            {
                curr_seconds = "0" + seconds;
            }
             else
            {
                curr_seconds = seconds;
            }

            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
 
            document.getElementById("hdfCurrentDateTime").value = curr_date + " " +  month_names[curr_month] +  " " + curr_year + " " + curr_hours + ":" + curr_minutes + ":" + curr_seconds
         }
     </script>
</head>
<body>
    <form id="frmStationSetting" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Station Setting"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:HiddenField ID="hdfCurrentDateTime" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table style="width: 500px">
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Station"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlStation" runat="server" Font-Names="Verdana" Font-Size="X-Small" AutoPostBack="True">
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label59" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                        Font-Underline="True" Text="Location :-"></asp:Label>
                    <asp:Label ID="lblMsg_Location" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7"><table style="width: 500px">
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="Label60" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Location"></asp:Label></td>
                        <td style="width: 1px">
                            :</td>
                        <td>
                            <asp:TextBox ID="txtLocation" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                Width="300px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblID" runat="server" Font-Names="Verdana" Font-Size="X-Small" Visible="False"></asp:Label></td>
                        <td style="width: 1px">
                        </td>
                        <td>
                            <asp:Button ID="btnAdd" runat="server" Text="Add" Width="52px" OnClientClick="showDate();" /></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td style="width: 1px">
                        </td>
                        <td>
                            <asp:GridView ID="gvLocation" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                DataKeyNames="LocationID" Font-Names="Verdana" Font-Size="X-Small" ForeColor="#333333"
                                GridLines="Vertical">
                                <RowStyle BackColor="#EFF3FB" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnDelete" runat="server" CausesValidation="False" CommandArgument='<%# Bind("LocationID") %>'
                                                CommandName="Delete" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this Location?');"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Location">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnLocation" runat="server" CausesValidation="False" CommandArgument='<%# Bind("LocationID") %>'
                                                CommandName="Select" Text='<%# Bind("Location") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <EditRowStyle BackColor="#2461BF" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td style="width: 1px">
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label37" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                        Font-Underline="True" Text="Price Point Control :-"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table style="width: 500px">
                        <tr>
                            <td style="width: 150px">
                                <asp:Label ID="Label43" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="* Currency Code"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlCurrencyCode" runat="server" Font-Names="Verdana" Font-Size="X-Small" Enabled="False">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_CurrencyCode" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label44" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="* Quotation"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <ew:NumericBox ID="txtQuoPricePoint" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server"
                                    ControlToValidate="txtQuoPricePoint" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" SetFocusOnError="True" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator>
                                <asp:Label ID="lblMsg_Quotation" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label45" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="* eRequisition"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <ew:NumericBox ID="txteReqPricePoint" runat="server" Font-Names="Verdana" Font-Size="X-Small"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator22" runat="server"
                                    ControlToValidate="txteReqPricePoint" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator>
                                <asp:Label ID="lblMsg_eReq" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label46" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="* Fixed Asset"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <ew:NumericBox ID="txtFAPricePoint" runat="server" Font-Names="Verdana" Font-Size="X-Small"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator23" runat="server"
                                    ControlToValidate="txtFAPricePoint" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator>
                                <asp:Label ID="lblMsg_FA" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label74" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="* Create Asset Record Per Unit"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <ew:NumericBox ID="txtCARPricePoint" runat="server" Font-Names="Verdana" Font-Size="X-Small"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator32" runat="server"
                                    ControlToValidate="txtCARPricePoint" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator>
                                <asp:Label ID="lblMsg_CAR" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label77" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                        Font-Underline="True" Text="Purchase Order Setting :-"></asp:Label></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table style="width: 500px">
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="Label87" runat="server" Font-Bold="False" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Auto Generate PO#"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:CheckBox ID="chkAutoPONo" runat="server" /></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="Label107" runat="server" Font-Bold="False" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="PO RDLC"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:FileUpload ID="fuPO" runat="server" Width="285px" />
                                <asp:Button ID="btnAttach" runat="server" Text="Attach" /></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                            </td>
                            <td style="width: 1px">
                            </td>
                            <td>
                                <asp:GridView ID="gvPORDLC" runat="server" AutoGenerateColumns="False" Font-Names="Verdana"
                                    Font-Size="X-Small">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Remove">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnRemove" runat="server" CommandArgument='<%# Bind("StationID") %>'
                                                    CommandName="Delete">Remove</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PO RDLC File">
                                            <ItemTemplate>
                                                <asp:Label ID="Label108" runat="server" Text='<%# Bind("PO_RDLC") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label51" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                        Font-Underline="True" Text="In Charge Setting :-"></asp:Label>
                    <asp:Label ID="lblMsg_MailNotify" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table border="1">
                        <tr>
                            <td align="center">
                                <asp:Label ID="Label52" runat="server" BackColor="MediumPurple" Font-Bold="True"
                                    Font-Names="Verdana" Font-Size="X-Small" Text="Type" Width="100%"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label56" runat="server" BackColor="MediumPurple" Font-Bold="True"
                                    Font-Names="Verdana" Font-Size="X-Small" Text="1st Party" Width="100%"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label57" runat="server" BackColor="MediumPurple" Font-Bold="True"
                                    Font-Names="Verdana" Font-Size="X-Small" Text="2nd Party" Width="100%"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label58" runat="server" BackColor="MediumPurple" Font-Bold="True"
                                    Font-Names="Verdana" Font-Size="X-Small" Text="3rd Party" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 150px">
                                <asp:Label ID="Label53" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="eRequisition"></asp:Label></td>
                            <td align="center">
                                <asp:DropDownList ID="ddleReq_1st" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                            <td align="center">
                                <asp:DropDownList ID="ddleReq_2nd" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                            <td align="center">
                                <asp:DropDownList ID="ddleReq_3rd" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label54" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Issue Purchase Order"></asp:Label></td>
                            <td align="center">
                                <asp:DropDownList ID="ddlIssuePO_1st" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                            <td align="center">
                                <asp:DropDownList ID="ddlIssuePO_2nd" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                            <td align="center">
                                <asp:DropDownList ID="ddlIssuePO_3rd" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label55" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Issue Payment"></asp:Label></td>
                            <td align="center">
                                <asp:DropDownList ID="ddlIssuePayment_1st" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                            <td align="center">
                                <asp:DropDownList ID="ddlIssuePayment_2nd" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                            <td align="center">
                                <asp:DropDownList ID="ddlIssuePayment_3rd" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label83" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Post eFMS"></asp:Label></td>
                            <td align="center">
                                <asp:DropDownList ID="ddleFMS_1st" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                            <td align="center">
                                <asp:DropDownList ID="ddleFMS_2nd" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                            <td align="center">
                                <asp:DropDownList ID="ddleFMS_3rd" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label36" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                        Font-Underline="True" Text="Approval Setting :-"></asp:Label></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <table border="1" style="width: 700px">
                        <tr>
                            <td align="center" colspan="13">
                                <asp:Label ID="Label9" runat="server" BackColor="#C0C0FF" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Small" Text="Purchase Budgeted" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="1">
                            </td>
                            <td align="center" colspan="4">
                                <asp:Label ID="Label8" runat="server" BackColor="#80FF80" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Small" Text="IT" Width="100%"></asp:Label></td>
                            <td align="center" colspan="4">
                                <asp:Label ID="Label10" runat="server" BackColor="#FFFF80" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Small" Text="Non IT" Width="100%"></asp:Label></td>
                            <td align="center" colspan="4">
                                <asp:Label ID="Label84" runat="server" BackColor="#FF80FF" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Small" Text="Non IT (Major)" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Level" Width="100%"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label11" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Comparision"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label12" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Currency Code"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label13" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Amount"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label14" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Approval Party"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label15" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Comparision"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label16" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Currency Code"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label17" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Amount"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Approval Party"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label85" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Comparision"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label86" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Currency Code"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label88" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Amount"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label89" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Approval Party"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="I"></asp:Label></td>
                            <td align="left" valign="top">
                                <asp:DropDownList ID="ddlComparision_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px" Enabled="False">
                                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_Comparision_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlCurrencyCode_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px" Enabled="False">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_CurrencyCode_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtAmount_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="80px" PositiveNumber="True" Enabled="False"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtAmount_IT_L1"
                                    Display="Dynamic" ErrorMessage="Invalid Amount" Font-Names="Verdana" Font-Size="X-Small"
                                    ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_Amount_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top"><asp:DropDownList ID="ddlApprovalParty_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small" AutoPostBack="True">
                            </asp:DropDownList><br />
                                <asp:DropDownList ID="ddlApprovalParty2_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_ApprovalParty_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlComparision_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px" Enabled="False">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_Comparision_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlCurrencyCode_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px" Enabled="False">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_CurrencyCode_NonIT_L1" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtAmount_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="80px" PositiveNumber="True" Enabled="False"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtAmount_NonIT_L1"
                                    Display="Dynamic" ErrorMessage="Invalid Amount" Font-Names="Verdana" Font-Size="X-Small"
                                    ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_Amount_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top"><asp:DropDownList ID="ddlApprovalParty_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small" AutoPostBack="True">
                            </asp:DropDownList>
                                <br />
                                <asp:DropDownList ID="ddlApprovalParty2_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_ApprovalParty_NonIT_L1" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlComparision_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px" Enabled="False">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_Comparision_NonITMajor_L1" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlCurrencyCode_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px" Enabled="False">
                                </asp:DropDownList><asp:Label ID="lblMsg_CurrencyCode_NonITMajor_L1" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtAmount_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="80px" PositiveNumber="True" Enabled="False">
                                </ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator37" runat="server"
                                    ControlToValidate="txtAmount_NonITMajor_L1" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_Amount_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlApprovalParty_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small" AutoPostBack="True">
                                </asp:DropDownList><br />
                                <asp:DropDownList ID="ddlApprovalParty2_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_ApprovalParty_NonITMajor_L1" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="II"></asp:Label></td>
                            <td align="left" valign="top">
                                <asp:DropDownList ID="ddlComparision_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_Comparision_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlCurrencyCode_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_CurrencyCode_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtAmount_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="80px" PositiveNumber="True"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtAmount_IT_L2"
                                    Display="Dynamic" ErrorMessage="Invalid Amount" Font-Names="Verdana" Font-Size="X-Small"
                                    ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_Amount_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top"><asp:DropDownList ID="ddlApprovalParty_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                            </asp:DropDownList>
                                <asp:Label ID="lblMsg_ApprovalParty_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlComparision_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_Comparision_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlCurrencyCode_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_CurrencyCode_NonIT_L2" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtAmount_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="80px" PositiveNumber="True"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtAmount_NonIT_L2"
                                    Display="Dynamic" ErrorMessage="Invalid Amount" Font-Names="Verdana" Font-Size="X-Small"
                                    ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_Amount_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top"><asp:DropDownList ID="ddlApprovalParty_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                            </asp:DropDownList>
                                <asp:Label ID="lblMsg_ApprovalParty_NonIT_L2" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlComparision_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_Comparision_NonITMajor_L2" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlCurrencyCode_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList><asp:Label ID="lblMsg_CurrencyCode_NonITMajor_L2" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtAmount_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="80px" PositiveNumber="True">
                                </ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator38" runat="server"
                                    ControlToValidate="txtAmount_NonITMajor_L2" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_Amount_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlApprovalParty_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><asp:Label ID="lblMsg_ApprovalParty_NonITMajor_L2" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="III"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlComparision_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_Comparision_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlCurrencyCode_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_CurrencyCode_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtAmount_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="80px" PositiveNumber="True"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtAmount_IT_L3"
                                    Display="Dynamic" ErrorMessage="Invalid Amount" Font-Names="Verdana" Font-Size="X-Small"
                                    ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_Amount_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top"><asp:DropDownList ID="ddlApprovalParty_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                            </asp:DropDownList>
                                <asp:Label ID="lblMsg_ApprovalParty_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlComparision_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_Comparision_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlCurrencyCode_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_CurrencyCode_NonIT_L3" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtAmount_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="80px" PositiveNumber="True"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtAmount_NonIT_L3"
                                    Display="Dynamic" ErrorMessage="Invalid Amount" Font-Names="Verdana" Font-Size="X-Small"
                                    ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_Amount_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top"><asp:DropDownList ID="ddlApprovalParty_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                            </asp:DropDownList>
                                <asp:Label ID="lblMsg_ApprovalParty_NonIT_L3" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlComparision_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_Comparision_NonITMajor_L3" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlCurrencyCode_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList><asp:Label ID="lblMsg_CurrencyCode_NonITMajor_L3" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtAmount_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="80px" PositiveNumber="True">
                                </ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator39" runat="server"
                                    ControlToValidate="txtAmount_NonITMajor_L2" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_Amount_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlApprovalParty_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><asp:Label ID="lblMsg_ApprovalParty_NonITMajor_L3" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="IV"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlComparision_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_Comparision_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlCurrencyCode_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_CurrencyCode_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtAmount_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="80px" PositiveNumber="True"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtAmount_IT_L4"
                                    Display="Dynamic" ErrorMessage="Invalid Amount" Font-Names="Verdana" Font-Size="X-Small"
                                    ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_Amount_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top"><asp:DropDownList ID="ddlApprovalParty_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                            </asp:DropDownList>
                                <asp:Label ID="lblMsg_ApprovalParty_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlComparision_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_Comparision_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlCurrencyCode_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_CurrencyCode_NonIT_L4" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtAmount_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="80px" PositiveNumber="True"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtAmount_NonIT_L4"
                                    Display="Dynamic" ErrorMessage="Invalid Amount" Font-Names="Verdana" Font-Size="X-Small"
                                    ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_Amount_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top"><asp:DropDownList ID="ddlApprovalParty_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                            </asp:DropDownList>
                                <asp:Label ID="lblMsg_ApprovalParty_NonIT_L4" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlComparision_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_Comparision_NonITMajor_L4" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlCurrencyCode_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList><asp:Label ID="lblMsg_CurrencyCode_NonITMajor_L4" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtAmount_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="80px" PositiveNumber="True">
                                </ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator40" runat="server"
                                    ControlToValidate="txtAmount_NonITMajor_L2" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_Amount_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlApprovalParty_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><asp:Label ID="lblMsg_ApprovalParty_NonITMajor_L4" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8" style="height: 21px">
                    &nbsp;<table border="1" style="width: 700px">
                        <tr>
                            <td align="center" colspan="13">
                                <asp:Label ID="Label19" runat="server" BackColor="#C0C0FF" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Small" Text="Purchase UnBudgeted" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="1" style="height: 18px">
                            </td>
                            <td align="center" colspan="4" style="height: 18px">
                                <asp:Label ID="Label20" runat="server" BackColor="#80FF80" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Small" Text="IT" Width="100%"></asp:Label></td>
                            <td align="center" colspan="4" style="height: 18px">
                                <asp:Label ID="Label21" runat="server" BackColor="#FFFF80" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Small" Text="Non IT" Width="100%"></asp:Label></td>
                            <td align="center" colspan="4" style="height: 18px">
                                <asp:Label ID="Label90" runat="server" BackColor="#FF80FF" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Small" Text="Non IT (Major)" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label22" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Level" Width="100%"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label23" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Comparision"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label24" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Currency Code"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label25" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Amount"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label26" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Approval Party"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label27" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Comparision"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label28" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Currency Code"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label29" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Amount"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label30" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Approval Party"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label92" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Comparision"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label93" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Currency Code"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label94" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Amount"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label95" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Approval Party"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="height: 21px">
                                <asp:Label ID="Label31" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="I"></asp:Label></td>
                            <td align="left" style="height: 21px" valign="top">
                                <asp:DropDownList ID="ddlUBComparision_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px" Enabled="False">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBComparision_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <asp:DropDownList ID="ddlUBCurrencyCode_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px" Enabled="False">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBCurrencyCode_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <ew:NumericBox ID="txtUBAmount_IT_L1" runat="server" Enabled="False" Font-Names="Verdana"
                                    Font-Size="X-Small" PositiveNumber="True" Width="80px"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtUBAmount_IT_L1"
                                    Display="Dynamic" ErrorMessage="Invalid Amount" Font-Names="Verdana" Font-Size="X-Small"
                                    ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_UBAmount_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <asp:DropDownList ID="ddlUBApprovalParty_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Enabled="False">
                                </asp:DropDownList>
                                <br />
                                <asp:DropDownList ID="ddlUBApprovalParty2_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Enabled="False">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBApprovalParty_IT_L1" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <asp:DropDownList ID="ddlUBComparision_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px" Enabled="False">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBComparision_NonIT_L1" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <asp:DropDownList ID="ddlUBCurrencyCode_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px" Enabled="False">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBCurrencyCode_NonIT_L1" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <ew:NumericBox ID="txtUBAmount_NonIT_L1" runat="server" Enabled="False" Font-Names="Verdana"
                                    Font-Size="X-Small" PositiveNumber="True" Width="80px"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server"
                                    ControlToValidate="txtUBAmount_NonIT_L1" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_UBAmount_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <asp:DropDownList ID="ddlUBApprovalParty_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Enabled="False">
                                </asp:DropDownList>
                                <br />
                                <asp:DropDownList ID="ddlUBApprovalParty2_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Enabled="False">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBApprovalParty_NonIT_L1" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <asp:DropDownList ID="ddlUBComparision_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px" Enabled="False">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_UBComparision_NonITMajor_L1" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <asp:DropDownList ID="ddlUBCurrencyCode_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px" Enabled="False">
                                </asp:DropDownList><asp:Label ID="lblMsg_UBCurrencyCode_NonITMajor_L1" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <ew:NumericBox ID="txtUBAmount_NonITMajor_L1" runat="server" Enabled="False" Font-Names="Verdana"
                                    Font-Size="X-Small" PositiveNumber="True" Width="80px">
                                </ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator41" runat="server"
                                    ControlToValidate="txtUBAmount_NonITMajor_L1" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_UBAmount_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <asp:DropDownList ID="ddlUBApprovalParty_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Enabled="False">
                                </asp:DropDownList><br />
                                <asp:DropDownList ID="ddlUBApprovalParty2_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Enabled="False">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBApprovalParty_NonITMajor_L1" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label32" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="II"></asp:Label></td>
                            <td align="left" valign="top">
                                <asp:DropDownList ID="ddlUBComparision_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBComparision_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBCurrencyCode_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBCurrencyCode_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtUBAmount_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                                    ControlToValidate="txtUBAmount_IT_L2" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_UBAmount_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBApprovalParty_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBApprovalParty_IT_L2" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBComparision_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBComparision_NonIT_L2" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBCurrencyCode_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBCurrencyCode_NonIT_L2" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtUBAmount_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                                    ControlToValidate="txtUBAmount_NonIT_L2" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_UBAmount_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBApprovalParty_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBApprovalParty_NonIT_L2" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBComparision_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_UBComparision_NonITMajor_L2" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBCurrencyCode_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList><asp:Label ID="lblMsg_UBCurrencyCode_NonITMajor_L2" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtUBAmount_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px">
                                </ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator42" runat="server"
                                    ControlToValidate="txtUBAmount_NonITMajor_L2" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_UBAmount_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBApprovalParty_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><asp:Label ID="lblMsg_UBApprovalParty_NonITMajor_L2" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label33" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="III"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBComparision_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBComparision_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBCurrencyCode_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBCurrencyCode_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtUBAmount_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                                    ControlToValidate="txtUBAmount_IT_L3" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_UBAmount_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBApprovalParty_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBApprovalParty_IT_L3" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBComparision_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBComparision_NonIT_L3" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBCurrencyCode_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBCurrencyCode_NonIT_L3" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtUBAmount_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                                    ControlToValidate="txtUBAmount_NonIT_L3" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_UBAmount_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBApprovalParty_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBApprovalParty_NonIT_L3" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBComparision_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_UBComparision_NonITMajor_L3" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBCurrencyCode_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList><asp:Label ID="lblMsg_UBCurrencyCode_NonITMajor_L3" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtUBAmount_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px">
                                </ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator43" runat="server"
                                    ControlToValidate="txtUBAmount_NonITMajor_L2" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_UBAmount_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBApprovalParty_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><asp:Label ID="lblMsg_UBApprovalParty_NonITMajor_L3" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label34" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="IV"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBComparision_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBComparision_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBCurrencyCode_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBCurrencyCode_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtUBAmount_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                                    ControlToValidate="txtUBAmount_IT_L4" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_UBAmount_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBApprovalParty_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBApprovalParty_IT_L4" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBComparision_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBComparision_NonIT_L4" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBCurrencyCode_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBCurrencyCode_NonIT_L4" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtUBAmount_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="80px" PositiveNumber="True"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server"
                                    ControlToValidate="txtUBAmount_NonIT_L4" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_UBAmount_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top"><asp:DropDownList ID="ddlUBApprovalParty_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                            </asp:DropDownList>
                                <asp:Label ID="lblMsg_UBApprovalParty_NonIT_L4" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBComparision_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_UBComparision_NonITMajor_L4" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBCurrencyCode_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList><asp:Label ID="lblMsg_UBCurrencyCode_NonITMajor_L4" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtUBAmount_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px">
                                </ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator44" runat="server"
                                    ControlToValidate="txtUBAmount_NonITMajor_L2" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_UBAmount_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlUBApprovalParty_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><asp:Label ID="lblMsg_UBApprovalParty_NonITMajor_L4" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <table border="1" style="width: 700px">
                        <tr>
                            <td align="center" colspan="13">
                                <asp:Label ID="Label61" runat="server" BackColor="#C0C0FF" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Small" Text="Purchase Over Budget" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="1">
                            </td>
                            <td align="center" colspan="4">
                                <asp:Label ID="Label62" runat="server" BackColor="#80FF80" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Small" Text="IT" Width="100%"></asp:Label></td>
                            <td align="center" colspan="4">
                                <asp:Label ID="Label63" runat="server" BackColor="#FFFF80" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Small" Text="Non IT" Width="100%"></asp:Label></td>
                            <td align="center" colspan="4">
                                <asp:Label ID="Label96" runat="server" BackColor="#FF80FF" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Small" Text="Non IT (Major)" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label64" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Level" Width="100%"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label65" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Comparision"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label66" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Currency Code"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label67" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Amount"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label68" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Approval Party"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label69" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Comparision"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label70" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Currency Code"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label71" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Amount"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label72" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Approval Party"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label97" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Comparision"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label98" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Currency Code"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label99" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Amount"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label101" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Approval Party"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="height: 21px">
                                <asp:Label ID="Label73" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="I"></asp:Label></td>
                            <td align="left" style="height: 21px" valign="top">
                                <asp:DropDownList ID="ddlOBComparision_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px" Enabled="False">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBComparision_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <asp:DropDownList ID="ddlOBCurrencyCode_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px" Enabled="False">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBCurrencyCode_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <ew:NumericBox ID="txtOBAmount_IT_L1" runat="server" Enabled="False" Font-Names="Verdana"
                                    Font-Size="X-Small" PositiveNumber="True" Width="80px"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator24" runat="server"
                                    ControlToValidate="txtOBAmount_IT_L1" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_OBAmount_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <asp:DropDownList ID="ddlOBApprovalParty_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Enabled="False">
                                </asp:DropDownList>
                                <br />
                                <asp:DropDownList ID="ddlOBApprovalParty2_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Enabled="False">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBApprovalParty_IT_L1" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <asp:DropDownList ID="ddlOBComparision_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px" Enabled="False">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBComparision_NonIT_L1" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <asp:DropDownList ID="ddlOBCurrencyCode_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px" Enabled="False">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBCurrencyCode_NonIT_L1" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <ew:NumericBox ID="txtOBAmount_NonIT_L1" runat="server" Enabled="False" Font-Names="Verdana"
                                    Font-Size="X-Small" PositiveNumber="True" Width="80px"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator25" runat="server"
                                    ControlToValidate="txtOBAmount_NonIT_L1" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_OBAmount_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <asp:DropDownList ID="ddlOBApprovalParty_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Enabled="False">
                                </asp:DropDownList>
                                <br />
                                <asp:DropDownList ID="ddlOBApprovalParty2_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Enabled="False">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBApprovalParty_NonIT_L1" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <asp:DropDownList ID="ddlOBComparision_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px" Enabled="False">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_OBComparision_NonITMajor_L1" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <asp:DropDownList ID="ddlOBCurrencyCode_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px" Enabled="False">
                                </asp:DropDownList><asp:Label ID="lblMsg_OBCurrencyCode_NonITMajor_L1" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <ew:NumericBox ID="txtOBAmount_NonITMajor_L1" runat="server" Enabled="False" Font-Names="Verdana"
                                    Font-Size="X-Small" PositiveNumber="True" Width="80px">
                                </ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator45"
                                    runat="server" ControlToValidate="txtOBAmount_NonITMajor_L1" Display="Dynamic"
                                    ErrorMessage="Invalid Amount" Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_OBAmount_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td style="height: 21px" valign="top">
                                <asp:DropDownList ID="ddlOBApprovalParty_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Enabled="False">
                                </asp:DropDownList><br />
                                <asp:DropDownList ID="ddlOBApprovalParty2_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Enabled="False">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBApprovalParty_NonITMajor_L1" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label82" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="II"></asp:Label></td>
                            <td align="left" valign="top">
                                <asp:DropDownList ID="ddlOBComparision_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBComparision_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBCurrencyCode_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBCurrencyCode_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtOBAmount_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator26" runat="server"
                                    ControlToValidate="txtOBAmount_IT_L2" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_OBAmount_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBApprovalParty_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBApprovalParty_IT_L2" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBComparision_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBComparision_NonIT_L2" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBCurrencyCode_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBCurrencyCode_NonIT_L2" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtOBAmount_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator27" runat="server"
                                    ControlToValidate="txtOBAmount_NonIT_L2" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_OBAmount_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBApprovalParty_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBApprovalParty_NonIT_L2" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBComparision_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_OBComparision_NonITMajor_L2" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBCurrencyCode_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList><asp:Label ID="lblMsg_OBCurrencyCode_NonITMajor_L2" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtOBAmount_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px">
                                </ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator46"
                                    runat="server" ControlToValidate="txtOBAmount_NonITMajor_L2" Display="Dynamic"
                                    ErrorMessage="Invalid Amount" Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_OBAmount_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBApprovalParty_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><asp:Label ID="lblMsg_OBApprovalParty_NonITMajor_L2" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label91" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="III"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBComparision_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBComparision_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBCurrencyCode_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBCurrencyCode_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtOBAmount_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator28" runat="server"
                                    ControlToValidate="txtOBAmount_IT_L3" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_OBAmount_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBApprovalParty_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBApprovalParty_IT_L3" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBComparision_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBComparision_NonIT_L3" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBCurrencyCode_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBCurrencyCode_NonIT_L3" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtOBAmount_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator29" runat="server"
                                    ControlToValidate="txtOBAmount_NonIT_L3" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_OBAmount_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBApprovalParty_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBApprovalParty_NonIT_L3" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBComparision_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_OBComparision_NonITMajor_L3" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBCurrencyCode_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList><asp:Label ID="lblMsg_OBCurrencyCode_NonITMajor_L3" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtOBAmount_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px">
                                </ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator47"
                                    runat="server" ControlToValidate="txtOBAmount_NonITMajor_L2" Display="Dynamic"
                                    ErrorMessage="Invalid Amount" Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_OBAmount_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBApprovalParty_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><asp:Label ID="lblMsg_OBApprovalParty_NonITMajor_L3" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label100" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="IV"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBComparision_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBComparision_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBCurrencyCode_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList><asp:Label ID="lblMsg_OBCurrencyCode_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtOBAmount_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator30" runat="server"
                                    ControlToValidate="txtOBAmount_IT_L4" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator>
                                <asp:Label ID="lblMsg_OBAmount_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBApprovalParty_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBApprovalParty_IT_L4" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBComparision_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBComparision_NonIT_L4" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBCurrencyCode_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBCurrencyCode_NonIT_L4" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtOBAmount_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px"></ew:NumericBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator31" runat="server"
                                    ControlToValidate="txtOBAmount_NonIT_L4" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator>
                                <asp:Label ID="lblMsg_OBAmount_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBApprovalParty_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_OBApprovalParty_NonIT_L4" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBComparision_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_OBComparision_NonITMajor_L4" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBCurrencyCode_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList><asp:Label ID="lblMsg_OBCurrencyCode_NonITMajor_L4" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtOBAmount_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px">
                                </ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator48"
                                    runat="server" ControlToValidate="txtOBAmount_NonITMajor_L2" Display="Dynamic"
                                    ErrorMessage="Invalid Amount" Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_OBAmount_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlOBApprovalParty_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><asp:Label ID="lblMsg_OBApprovalParty_NonITMajor_L4" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <table border="1" style="width: 400px">
                        <tr>
                            <td align="center" colspan="13">
                                <asp:Label ID="Label35" runat="server" BackColor="#C0C0FF" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Small" Text="Obsolete, Disposal & Writeoff" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td align="center" colspan="4">
                                <asp:Label ID="Label75" runat="server" BackColor="#80FF80" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Small" Text="IT" Width="100%"></asp:Label></td>
                            <td align="center" colspan="4">
                                <asp:Label ID="Label76" runat="server" BackColor="#FFFF80" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Small" Text="Non IT" Width="100%"></asp:Label></td>
                            <td align="center" colspan="4">
                                <asp:Label ID="Label102" runat="server" BackColor="#FF80FF" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Small" Text="Non IT (Major)" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label38" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Level" Width="100%"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label39" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Comparision"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label40" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Currency Code"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label41" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Amount"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label42" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Approval Party"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label78" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Comparision"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label79" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Currency Code"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label80" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Amount"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label81" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Approval Party"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label106" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Comparision"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label105" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Currency Code"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label104" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Amount"></asp:Label></td>
                            <td align="center">
                                <asp:Label ID="Label103" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="Approval Party"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label47" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="I"></asp:Label></td>
                            <td align="left" valign="top">
                                <asp:DropDownList ID="ddlDWComparision_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px" Enabled="False">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_DWComparision_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWCurrencyCode_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px" Enabled="False">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWCurrencyCode_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtDWAmount_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="80px" PositiveNumber="True" Enabled="False"></ew:NumericBox><asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server"
                                    ControlToValidate="txtDWAmount_IT_L1" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_DWAmount_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top"><asp:DropDownList ID="ddlDWApprovalParty_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                            </asp:DropDownList><br />
                                <asp:DropDownList ID="ddlDWApprovalParty2_IT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWApprovalParty_IT_L1" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWComparision_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px" Enabled="False">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_DWComparision_NonIT_L1" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWCurrencyCode_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px" Enabled="False">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWCurrencyCode_NonIT_L1" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtDWAmount_NonIT_L1" runat="server" Enabled="False" Font-Names="Verdana"
                                    Font-Size="X-Small" PositiveNumber="True" Width="80px"></ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator33" runat="server"
                                    ControlToValidate="txtDWAmount_NonIT_L1" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_DWAmount_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWApprovalParty_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><br />
                                <asp:DropDownList ID="ddlDWApprovalParty2_NonIT_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList>
                                <asp:Label ID="lblMsg_DWApprovalParty_NonIT_L1" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWComparision_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px" Enabled="False">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_DWComparision_NonITMajor_L1" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWCurrencyCode_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px" Enabled="False">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWCurrencyCode_NonITMajor_L1" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtDWAmount_NonITMajor_L1" runat="server" Enabled="False" Font-Names="Verdana"
                                    Font-Size="X-Small" PositiveNumber="True" Width="80px">
                                </ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator49"
                                    runat="server" ControlToValidate="txtDWAmount_NonITMajor_L1" Display="Dynamic"
                                    ErrorMessage="Invalid Amount" Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_DWAmount_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWApprovalParty_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><br />
                                <asp:DropDownList ID="ddlDWApprovalParty2_NonITMajor_L1" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWApprovalParty_NonITMajor_L1" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label48" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="II"></asp:Label></td>
                            <td align="left" valign="top">
                                <asp:DropDownList ID="ddlDWComparision_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_DWComparision_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWCurrencyCode_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWCurrencyCode_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtDWAmount_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="80px" PositiveNumber="True"></ew:NumericBox><asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server"
                                    ControlToValidate="txtDWAmount_IT_L2" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_DWAmount_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top"><asp:DropDownList ID="ddlDWApprovalParty_IT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                            </asp:DropDownList><asp:Label ID="lblMsg_DWApprovalParty_IT_L2" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWComparision_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_DWComparision_NonIT_L2" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWCurrencyCode_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWCurrencyCode_NonIT_L2" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtDWAmount_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px"></ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator34" runat="server"
                                    ControlToValidate="txtDWAmount_NonIT_L2" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_DWAmount_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWApprovalParty_NonIT_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWApprovalParty_NonIT_L2" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWComparision_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_DWComparision_NonITMajor_L2" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWCurrencyCode_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWCurrencyCode_NonITMajor_L2" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtDWAmount_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px">
                                </ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator50"
                                    runat="server" ControlToValidate="txtDWAmount_NonITMajor_L2" Display="Dynamic"
                                    ErrorMessage="Invalid Amount" Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_DWAmount_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWApprovalParty_NonITMajor_L2" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWApprovalParty_NonITMajor_L2" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label49" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="III"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWComparision_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_DWComparision_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWCurrencyCode_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWCurrencyCode_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtDWAmount_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="80px" PositiveNumber="True"></ew:NumericBox><asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server"
                                    ControlToValidate="txtDWAmount_IT_L3" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_DWAmount_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top"><asp:DropDownList ID="ddlDWApprovalParty_IT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                            </asp:DropDownList><asp:Label ID="lblMsg_DWApprovalParty_IT_L3" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWComparision_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_DWComparision_NonIT_L3" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWCurrencyCode_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWCurrencyCode_NonIT_L3" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtDWAmount_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px"></ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator35" runat="server"
                                    ControlToValidate="txtDWAmount_NonIT_L3" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_DWAmount_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWApprovalParty_NonIT_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWApprovalParty_NonIT_L3" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWComparision_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_DWComparision_NonITMajor_L3" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWCurrencyCode_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWCurrencyCode_NonITMajor_L3" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtDWAmount_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px">
                                </ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator51"
                                    runat="server" ControlToValidate="txtDWAmount_NonITMajor_L2" Display="Dynamic"
                                    ErrorMessage="Invalid Amount" Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_DWAmount_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWApprovalParty_NonITMajor_L3" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWApprovalParty_NonITMajor_L3" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label50" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="IV"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWComparision_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_DWComparision_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWCurrencyCode_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px" Height="17px">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWCurrencyCode_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtDWAmount_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="80px" PositiveNumber="True"></ew:NumericBox><asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server"
                                    ControlToValidate="txtDWAmount_IT_L4" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><br />
                                <asp:Label ID="lblMsg_DWAmount_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top"><asp:DropDownList ID="ddlDWApprovalParty_IT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                            </asp:DropDownList><asp:Label ID="lblMsg_DWApprovalParty_IT_L4" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWComparision_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_DWComparision_NonIT_L4" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWCurrencyCode_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px" Height="17px">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWCurrencyCode_NonIT_L4" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtDWAmount_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px"></ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator36" runat="server"
                                    ControlToValidate="txtDWAmount_NonIT_L4" Display="Dynamic" ErrorMessage="Invalid Amount"
                                    Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_DWAmount_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWApprovalParty_NonIT_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWApprovalParty_NonIT_L4" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWComparision_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="85px">
                                    <asp:ListItem>Select One</asp:ListItem>
                                    <asp:ListItem>&gt;=</asp:ListItem>
                                    <asp:ListItem>&gt;</asp:ListItem>
                                </asp:DropDownList><asp:Label ID="lblMsg_DWComparision_NonITMajor_L4" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWCurrencyCode_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="60px">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWCurrencyCode_NonITMajor_L4" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <ew:NumericBox ID="txtDWAmount_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    PositiveNumber="True" Width="80px">
                                </ew:NumericBox><br />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator52"
                                    runat="server" ControlToValidate="txtDWAmount_NonITMajor_L2" Display="Dynamic"
                                    ErrorMessage="Invalid Amount" Font-Names="Verdana" Font-Size="X-Small" ValidationExpression="(^(0|([1-9][0-9]*))(\.[0-9]{1,2})?$)|(^(0{0,1}|([1-9][0-9]*))(\.[0-9]{1,2})?$)"></asp:RegularExpressionValidator><asp:Label
                                        ID="lblMsg_DWAmount_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        ForeColor="Red" Visible="False"></asp:Label></td>
                            <td valign="top">
                                <asp:DropDownList ID="ddlDWApprovalParty_NonITMajor_L4" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList><asp:Label ID="lblMsg_DWApprovalParty_NonITMajor_L4" runat="server"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    &nbsp;</td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table style="width: 335px">
                        <tr>
                            <td style="width: 150px">
                                <asp:Button ID="btnSave" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Save"
                                    Width="65px" OnClientClick="showDate();" /></td>
                            <td style="width: 150px">
                                <asp:Button ID="btnClear" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Clear"
                                    Width="65px" /></td>
                            <td style="width: 150px">
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
