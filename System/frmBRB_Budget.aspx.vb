Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class System_frmBRB_Budget
    Inherits System.Web.UI.Page

    Dim dsStation As New DataSet

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        'Session("BRB_StationID") = "027"
        'Session("DA_UserID") = "a2144"
        'Session("BRB_Year") = 2015

        If IsPostBack() = False Then

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            btnSave.Enabled = False


            hdfCurrentDateTime.Value = Format(DateTime.Now, "dd MMM yyyy hh:mm:ss").ToString

            If Session("CurrentDateTime") <> Nothing Then
                If Session("CurrentDateTime").ToString <> "" Then
                    hdfCurrentDateTime.Value = Session("CurrentDateTime").ToString
                End If
            End If

            'Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "S0002")

            'If Len(strAccessRight) >= 7 Then
            '    Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            'Else
            '    Session("AccessStation") = ""
            'End If


            'If IsNumeric(Left(strAccessRight, 6)) = True Then
            '    If Mid(strAccessRight, 1, 1) = 1 Then
            '        Session("blnAccess") = True
            '    Else
            '        Session("blnAccess") = False
            '        Session("AccessRightMsg") = "You are no right to access the Budget List Page."
            '        Response.Redirect("~/ListHome.aspx")
            '        Exit Sub
            '    End If

            '    If Mid(strAccessRight, 2, 1) = 1 Then
            '        Session("blnReadOnly") = True
            '    Else
            '        Session("blnReadOnly") = False

            '        If Mid(strAccessRight, 3, 1) = 1 Then
            '            Session("blnWrite") = True
            '            'btnSave.Enabled = True
            '            'gvData.Columns(0).Visible = True
            '        Else
            '            Session("blnWrite") = False
            '            'btnSave.Enabled = False
            '            'gvData.Columns(0).Visible = False
            '        End If
            '    End If

            '    If Mid(strAccessRight, 4, 1) = 1 Then
            '        Session("blnModifyOthers") = True
            '        'btnSave.Visible = True
            '        'gvData.Columns(0).Visible = True
            '    Else
            '        Session("blnModifyOthers") = False
            '        'btnSave.Visible = False
            '        'gvData.Columns(0).Visible = False
            '    End If

            '    If Mid(strAccessRight, 5, 1) = 1 Then
            '        Session("blnDeleteOthers") = True
            '        'gvData.Columns(0).Visible = True
            '    Else
            '        Session("blnDeleteOthers") = False
            '        'gvData.Columns(0).Visible = False
            '    End If
            'End If

            BindDropDownList()
            ddlType_SelectedIndexChanged(sender, e)

            If Session("BRB_StationID") <> Nothing Then
                If Session("BRB_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("BRB_StationID").ToString()
                End If
                Session("BRB_StationID") = Nothing
            End If

            If Session("BRB_Year") <> Nothing Then
                If Session("BRB_Year").ToString <> "" Then
                    ddlYear.SelectedValue = Session("BRB_Year").ToString()
                End If
                Session("BRB_Year") = Nothing
            End If

            GetBRBBudget()
            'gvData.Columns(0).Visible = False
            'gvData.Columns(gvData.Columns.Count - 1).Visible = False
            ddlStation.Enabled = False
            ddlYear.Enabled = False

            'If gvData.Rows.Count = 0 Then
            '    'GetBRBBudgetFromFMS(ddlStation.SelectedValue.ToString(), ddlYear.SelectedValue.ToString())
            'End If

        Else

        End If

    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[AccessStation],[Year],[Budget_Category]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"
                MyData.Tables(1).TableName = "Year"
                MyData.Tables(2).TableName = "Category"

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)
                ddlStation.SelectedValue = Session("BRB_StationID").ToString()

                ddlYear.DataSource = MyData.Tables(1)
                ddlYear.DataValueField = MyData.Tables(1).Columns("Year").ToString
                ddlYear.DataTextField = MyData.Tables(1).Columns("Year").ToString
                ddlYear.DataBind()
                ddlYear.SelectedValue = Now.Year

                ddlCategory.DataSource = MyData.Tables(2)
                ddlCategory.DataValueField = MyData.Tables(2).Columns("CategoryID").ToString
                ddlCategory.DataTextField = MyData.Tables(2).Columns("Category").ToString
                ddlCategory.DataBind()


                Dim dtView As DataView = clsCF.GetCurrency()
                ddlCurrency.DataSource = dtView
                ddlCurrency.DataValueField = dtView.Table.Columns("HQID").ToString
                ddlCurrency.DataTextField = dtView.Table.Columns("CurrencyCode").ToString
                ddlCurrency.DataBind()

                StationCurrency()

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub StationCurrency()
        clsCF.StationCurrencyID(ddlStation.SelectedValue.ToString, ddlCurrency)
    End Sub

    Private Sub GetBRBBudget()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                If ddlYear.SelectedValue.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Year')</script>")
                    Exit Sub
                End If

                If ddlStation.SelectedValue.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Station')</script>")
                    Exit Sub
                End If

                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetBRBBudget")

                With MyCommand
                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@Year", SqlDbType.Int)
                    .Parameters("@Year").Value = CInt(ddlYear.SelectedValue.ToString)

                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "BRBBudget"


                gvData.DataSource = MyData.Tables(0)
                gvData.DataMember = MyData.Tables(0).TableName
                gvData.DataBind()

                

            Catch ex As Exception
                Me.lblMsg.Text = "GetBRBBudget: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub GetBRBBudgetFromFMS(ByVal strStation As String, ByVal intYear As Integer)
        'Call function and return BRB Budget dataset
       
        Dim dtBRB As New DataSet
        'dtBRB = fms.GetBRBBudget(ddlStation.SelectedValue.ToString(), CInt(ddlYear.SelectedValue.ToString))

        If dtBRB.Tables(0).Rows.Count > 0 Then

            Dim column As DataColumn

            column = New DataColumn
            With column
                .ColumnName = "CreatedBy"
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = Session("DA_UserID").ToString
                .Unique = False
            End With
            dtBRB.Tables(0).Columns.Add(column)

            column = New DataColumn
            With column
                .ColumnName = "CreatedOn"
                .DataType = System.Type.GetType("System.DateTime")
                .DefaultValue = hdfCurrentDateTime.Value.ToString
                .Unique = False
            End With
            dtBRB.Tables(0).Columns.Add(column)

            column = New DataColumn
            With column
                .ColumnName = "DataSource"
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = "BRB"
                .Unique = False
            End With
            dtBRB.Tables(0).Columns.Add(column)

            column = New DataColumn
            With column
                .ColumnName = "Year"
                .DataType = System.Type.GetType("System.Int32")
                .DefaultValue = CInt(ddlYear.SelectedValue.ToString())
                .Unique = False
            End With
            dtBRB.Tables(0).Columns.Add(column)

            column = New DataColumn
            With column
                .ColumnName = "StationID"
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = ddlStation.SelectedValue.ToString()
                .Unique = False
            End With
            dtBRB.Tables(0).Columns.Add(column)


            Using bulkCopy As SqlBulkCopy = New SqlBulkCopy(strConnectionString)
                bulkCopy.DestinationTableName = "tbBRB_Budget"

                bulkCopy.ColumnMappings.Add("Description", "Description")
                bulkCopy.ColumnMappings.Add("Quantity", "Quantity")
                bulkCopy.ColumnMappings.Add("Amount", "Amount")
                bulkCopy.ColumnMappings.Add("Remark", "Remark")
                bulkCopy.ColumnMappings.Add("CurrencyCodeID", "CurrencyCodeID")

                'bulkCopy.BulkCopyTimeout = (timeoutvalue)
                'bulkCopy.BatchSize = 10000
                bulkCopy.WriteToServer(dtBRB)
            End Using

            GetBRBBudget()
        Else
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('BRB Budget not found from eFMS.')</script>")
        End If
    End Sub


    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        lblMsg.Text = ""
        lblID.Text = ""
        If ddlCategory.Items.Count > 0 Then
            ddlCategory.SelectedValue = "0"
        End If
        If ddlSubCategory.Items.Count > 0 Then
            ddlSubCategory.SelectedValue = "0"
        End If
        lblBRBDesc.Text = ""
        txtQuantity.Text = ""
        txtBalance.Text = ""
        txtAmount.Text = ""
        txtRemarks.Text = ""
        ddlType.SelectedValue = "Category"
        lblAmountPerUnit.Text = "Amount Per Unit"
        txtAmtBalance.Text = ""
        ddlType_SelectedIndexChanged(sender, e)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try

                Dim strID As String = ""
                Dim strMsg As String = ""

                lblMsg.Text = ""

                If lblID.Text.ToString = "" Then
                    strID = ""
                Else
                    strID = lblID.Text.ToString
                End If

                If ddlType.SelectedValue.ToString = "Category" Then
                    If ddlCategory.SelectedValue.ToString = "0" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Category')</script>")
                        'lblMsg.Text = "Please select Category."
                        Exit Sub
                    End If

                    If ddlSubCategory.Items.Count > 1 Then
                        If ddlSubCategory.SelectedValue.ToString = "0" Then
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select SubCategory')</script>")
                            'lblMsg.Text = "Please select subCategory."
                            Exit Sub
                        End If
                    End If

                    If txtQuantity.Text.ToString = "" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Quantity')</script>")
                        'lblMsg.Text = "Please enter Quantity."
                        Exit Sub
                    Else
                        If CInt(txtQuantity.Text.ToString) <= 0 Then
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Quantity')</script>")
                            'lblMsg.Text = "Invalid Quantity."
                            Exit Sub
                        End If
                    End If
                End If

                If txtAmount.Text.ToString = "" Then
                    If ddlType.SelectedValue.ToString = "Category" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Amount Per Unit')</script>")
                        'lblMsg.Text = "Please enter Amount Per Unit."
                        Exit Sub
                    Else
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Amount')</script>")
                        'lblMsg.Text = "Please enter Amount."
                        Exit Sub
                    End If
                Else
                    If Convert.ToDecimal(txtAmount.Text.ToString) <= 0 Then
                        'If CInt(txtAmount.Text.ToString) <= 0 Then
                        If ddlType.SelectedValue.ToString = "Category" Then
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Amount Per Unit')</script>")
                            'lblMsg.Text = "Invalid Amount Per Unit."
                            Exit Sub
                        Else
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Amount')</script>")
                            'lblMsg.Text = "Invalid Amount."
                            Exit Sub
                        End If
                    End If
                End If

                If ddlCurrency.SelectedValue = "0" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Currency Code')</script>")
                    Exit Sub
                End If

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveBRBBudgetList", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@BRBBudgetID", SqlDbType.BigInt)
                    .Parameters("@BRBBudgetID").Value = Convert.ToInt32(strID)
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@Year", SqlDbType.Int)
                    .Parameters("@Year").Value = CInt(ddlYear.SelectedValue.ToString)
                    .Parameters.Add("@Type", SqlDbType.NVarChar, 50)
                    .Parameters("@Type").Value = ddlType.SelectedValue.ToString
                    .Parameters.Add("@CategoryID", SqlDbType.BigInt)
                    If ddlType.SelectedValue.ToString = "Category" Then
                        If ddlSubCategory.Items.Count > 1 Then
                            .Parameters("@CategoryID").Value = CInt(ddlSubCategory.SelectedValue.ToString)
                        Else
                            .Parameters("@CategoryID").Value = CInt(ddlCategory.SelectedValue.ToString)
                        End If
                    Else
                        .Parameters("@CategoryID").Value = 0
                    End If


                    .Parameters.Add("@Quantity", SqlDbType.Int)
                    If txtQuantity.Text.ToString <> "" Then
                        .Parameters("@Quantity").Value = CInt(txtQuantity.Text.ToString)
                    Else
                        .Parameters("@Quantity").Value = DBNull.Value
                    End If

                    .Parameters.Add("@CurrencyCodeID", SqlDbType.BigInt)
                    .Parameters("@CurrencyCodeID").Value = CInt(ddlCurrency.SelectedValue.ToString)

                    .Parameters.Add("@Amount", SqlDbType.Decimal, 18, 2)
                    If txtAmount.Text.ToString <> "" Then
                        .Parameters("@Amount").Value = CDec(txtAmount.Text.ToString)
                    Else
                        .Parameters("@Amount").Value = DBNull.Value
                    End If

                    .Parameters.Add("@Remarks", SqlDbType.NVarChar)
                    .Parameters("@Remarks").Value = txtRemarks.Text.ToString
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()


                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    btnClear_Click(sender, e)
                    GetBRBBudget()
                    'gvData.Columns(0).Visible = False
                    'gvData.Columns(gvData.Columns.Count - 1).Visible = False
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub gvData_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvData.SelectedIndexChanged



        Dim BRBBudgetID As Integer = 0
        BRBBudgetID = gvData.SelectedDataKey.Value.ToString
        lblID.Text = BRBBudgetID

        Dim lbtnBRBDescription As New LinkButton
        lbtnBRBDescription = gvData.SelectedRow.FindControl("lbtnBRBDescription")
        lblBRBDesc.Text = lbtnBRBDescription.Text.ToString()


        'Dim lblType As New Label
        'lblType = gvData.SelectedRow.FindControl("lblType")
        'ddlType.SelectedValue = lblType.Text.ToString
        'If lblType.Text.ToString = "Category" Then
        '    ddlCategory.Enabled = True
        '    ddlSubCategory.Enabled = True
        '    lblAmountPerUnit.Text = "Amount Per Unit"
        'Else
        '    ddlCategory.SelectedValue = "0"
        '    ddlSubCategory.SelectedValue = "0"
        '    ddlCategory.Enabled = False
        '    ddlSubCategory.Enabled = False
        '    lblAmountPerUnit.Text = "Amount"
        'End If

        If ddlType.SelectedValue.ToString = "Category" Then
            ddlCategory.Enabled = True
            ddlSubCategory.Enabled = True
            lblAmountPerUnit.Text = "Amount Per Unit"

            Dim hdCategory As New HiddenField
            hdCategory = gvData.SelectedRow.FindControl("hdCategory")

            Dim hdSubCategory As New HiddenField
            hdSubCategory = gvData.SelectedRow.FindControl("hdSubCategory")

            If (hdCategory.Value.ToString() <> "") Then
                ddlCategory.SelectedValue = CInt(hdCategory.Value.ToString)

                Dim intCategoryID As Integer
                intCategoryID = CInt(ddlCategory.SelectedValue.ToString)
                BindSubCategoryDropDownList(intCategoryID)
            End If

            If (hdCategory.Value.ToString() <> "") Then
                ddlSubCategory.SelectedValue = CInt(hdSubCategory.Value.ToString)
            End If

        End If

        Dim lblQuantity As New Label
        lblQuantity = gvData.SelectedRow.FindControl("lblQuantity")
        txtQuantity.Text = lblQuantity.Text.ToString

        Dim lblBalance As New Label
        lblBalance = gvData.SelectedRow.FindControl("lblBalance")
        txtBalance.Text = lblBalance.Text.ToString

        'Dim hdfCurrencyCodeID As New HiddenField
        'hdfCurrencyCodeID = gvData.SelectedRow.FindControl("hdfCurrencyCodeID")
        'ddlCurrency.SelectedValue = hdfCurrencyCodeID.Value.ToString

        Dim lblAmount As New Label
        lblAmount = gvData.SelectedRow.FindControl("lblAmount")
        txtAmount.Text = lblAmount.Text.ToString

        Dim lblAmountBalance As New Label
        lblAmountBalance = gvData.SelectedRow.FindControl("lblAmountBalance")
        txtAmtBalance.Text = lblAmountBalance.Text.ToString

        Dim lblRemark As New Label
        lblRemark = gvData.SelectedRow.FindControl("lblRemark")
        txtRemarks.Text = lblRemark.Text.ToString

        'ddlType.Enabled = False
        'ddlCategory.Enabled = False
        'ddlSubCategory.Enabled = False
        lblMsg.Text = ""
        btnSave.Enabled = True

    End Sub

    Private Sub BindSubCategoryDropDownList(ByVal intID As Integer)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByID")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[Budget_SubCategory]"
                    .Parameters.Add("@ID", SqlDbType.BigInt)
                    .Parameters("@ID").Value = intID
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "SubCategory"


                ddlSubCategory.DataSource = MyData.Tables(0)
                ddlSubCategory.DataValueField = MyData.Tables(0).Columns("CategoryID").ToString
                ddlSubCategory.DataTextField = MyData.Tables(0).Columns("Category").ToString
                ddlSubCategory.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "BindSubCategoryDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        If ddlType.SelectedValue = "Category" Then
            ddlCategory.Enabled = True
            ddlSubCategory.Enabled = True
            lblAmountPerUnit.Text = "Amount Per Unit"
        Else
            If ddlCategory.Items.Count > 0 Then
                ddlCategory.SelectedValue = "0"
            End If
            If ddlSubCategory.Items.Count > 0 Then
                ddlSubCategory.SelectedValue = "0"
            End If
            txtBalance.Text = ""
            txtQuantity.Text = ""
            ddlCategory.Enabled = False
            ddlSubCategory.Enabled = False
            lblAmountPerUnit.Text = "Amount"
        End If
    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        Dim intCategoryID As Integer
        intCategoryID = CInt(ddlCategory.SelectedValue.ToString)
        BindSubCategoryDropDownList(intCategoryID)
    End Sub

    Protected Sub btnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try

                Dim strMsg As String = ""

                lblMsg.Text = ""

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_ConfirmBRBBudget", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@Year", SqlDbType.Int)
                    .Parameters("@Year").Value = CInt(ddlYear.SelectedValue.ToString)
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    GetBRBBudget()
                    'gvData.Columns(0).Visible = False
                    'gvData.Columns(gvData.Columns.Count - 1).Visible = True
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    'GetBRBBudget()
                    btnSave.Enabled = False
                    btnConfirm.Enabled = False
                    Session("BRB_StationID") = ddlStation.SelectedValue.ToString()
                    Session("BRB_Year") = ddlYear.SelectedValue.ToString()
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Confirmed and Save into Budget List Successful');window.opener =self;window.close();</script>")
                    'Dim ParentFormID As String = ""
                    'ParentFormID = Request.QueryString("ParentForm").ToString

                    'Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener =self;window.close();alert('Confirmed and Save into Budget List Successful');</script>")

                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnConfirm_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub
End Class
