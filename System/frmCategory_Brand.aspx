<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmCategory_Brand.aspx.vb" Inherits="System_frmCategory_Brand" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Category VS Brand</title>
</head>
<body>
    <form id="frmCategory_Brand" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Category VS Brand"></asp:Label></td>
                <td style="font-weight: bold; font-size: 12pt; font-family: Times New Roman">
                </td>
            </tr>
            <tr style="font-weight: bold; font-size: 12pt; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:HiddenField ID="hdfCurrentDateTime" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="Label11" runat="server" Font-Names="Verdana" Font-Size="X-Small">ID</asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:Label ID="lblID" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Group"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True" Font-Names="Verdana"
                                    Font-Size="X-Small">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Category"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:TextBox ID="txtCategory" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    MaxLength="100" Width="350px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Description"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:TextBox ID="txtDescription" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    MaxLength="200" Width="450px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td style="width: 1px">
                            </td>
                            <td>
                                <table style="width: 335px">
                                    <tr>
                                        <td style="width: 150px">
                                            <asp:Button ID="btnSave" runat="server" Font-Names="Verdana" Font-Size="Small" OnClientClick="showDate();"
                                                Text="Save" Width="65px" /></td>
                                        <td colspan="2">
                                            <asp:Button ID="btnClear" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Clear"
                                                Width="67px" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
