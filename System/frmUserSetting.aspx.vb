Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class System_frmUserSetting
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If IsPostBack() = False Then


            Session("DA_ACCESSSTATION") = ""
            Session("DA_USERLIST") = ""

            If Session("DA_EmployeeUserID") <> "" Then
                txtUserID.Text = Session("DA_EmployeeUserID").ToString
                'txtUserID.Enabled = False
            Else
                txtUserID.Text = ""
                'txtUserID.Enabled = True
            End If

            'If Session("DA_EmployeeStatus").ToString.ToUpper <> "ACTIVE" Then
            '    btnRemoveSetting.Visible = True
            'Else
            '    btnRemoveSetting.Visible = False
            'End If

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            btnSave.Enabled = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "S0004")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If

            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the User Setting page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnSave.Enabled = True
                    Else
                        Session("blnWrite") = False
                        btnSave.Enabled = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                    'btnSave.Enabled = True
                Else
                    Session("blnModifyOthers") = False
                    'btnSave.Enabled = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            GetUserSetting()

        Else
            If Session("DA_New_ACCESSSTATION") <> "" Then
                ShowAccessStation(Session("DA_New_ACCESSSTATION").ToString, "AccessStation")
                hdfStation.Value = Session("DA_New_ACCESSSTATION").ToString
                Session("DA_ACCESSSTATION") = hdfStation.Value.ToString
                Session("DA_New_ACCESSSTATION") = ""
            Else
                If Session("DA_ACCESSSTATION").ToString <> "" Then
                    hdfStation.Value = Session("DA_ACCESSSTATION").ToString
                End If
                ShowAccessStation(hdfStation.Value.ToString, "AccessStation")
            End If

        End If
    End Sub

    Private Sub GetUserSetting()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsAccessStation As New DataSet

                Session("DA_ACCESSSTATION") = ""

                Dim i As Integer
                Dim dsEmployeeDetail As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand


                If txtUserID.Text.ToString = "" Then
                    lblMsg.Text = "Please enter User ID."
                    Exit Sub
                End If

                dsEmployeeDetail = DIMERCO.SDK.Utilities.LSDK.getUserProfilebyUserList(txtUserID.Text.ToString)

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetUserSetting")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.NVarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@UserID", SqlDbType.NVarChar, 50)
                    .Parameters("@UserID").Value = txtUserID.Text.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"
                MyData.Tables(1).TableName = "UserDetail"
                MyData.Tables(2).TableName = "Role"
                MyData.Tables(3).TableName = "UserRole"



                hdfStation.Value = ""
                hdfStation.Value = MyData.Tables(0).Rows(0)("StationID").ToString
                dsAccessStation = DIMERCO.SDK.Utilities.LSDK.getStationDataByWithList(hdfStation.Value)
                gvAccessStation.DataSource = dsAccessStation
                gvAccessStation.DataMember = dsAccessStation.Tables(0).TableName
                gvAccessStation.DataBind()


                If MyData.Tables(1).Rows.Count > 0 Then

                    dsEmployeeDetail = DIMERCO.SDK.Utilities.LSDK.getUserProfilebyUserList(txtUserID.Text.ToString)
                    'Dim strcolumnname As String = ""
                    'For i = 0 To dsEmployeeDetail.Tables(0).Columns.Count - 1
                    '    strcolumnname = dsEmployeeDetail.Tables(0).Columns(i).ColumnName.ToString

                    'Next
                    lblName.Text = dsEmployeeDetail.Tables(0).Rows(0)("FullName").ToString
                    txtEmail.Text = dsEmployeeDetail.Tables(0).Rows(0)("Email").ToString
                    If txtEmail.Text.ToString.Contains("@") = False Then
                        txtEmail.Text = MyData.Tables(1).Rows(0)("Email").ToString
                    End If
                    lblStation.Text = dsEmployeeDetail.Tables(0).Rows(0)("StationCode").ToString
                    Session("DA_EmployeeStationID") = dsEmployeeDetail.Tables(0).Rows(0)("StationID").ToString
                    hdStationID.Value = Session("DA_EmployeeStationID")

                    'If Session("AccessStation").ToString <> "" Then
                    '    If Session("AccessStation").ToString.Contains(hdStationID.Value & ",") = False Then
                    '        btnSave.Enabled = False
                    '    End If
                    'End If



                    If MyData.Tables(1).Rows(0)("AsApprovalParty").ToString = 1 Then
                        chkAsApprovalParty.Checked = True
                    Else
                        chkAsApprovalParty.Checked = False
                    End If

                Else

                    lblName.Text = dsEmployeeDetail.Tables(0).Rows(0)("FullName").ToString
                    txtEmail.Text = dsEmployeeDetail.Tables(0).Rows(0)("Email").ToString
                    lblStation.Text = dsEmployeeDetail.Tables(0).Rows(0)("StationCode").ToString
                    Session("DA_EmployeeStationID") = dsEmployeeDetail.Tables(0).Rows(0)("StationID").ToString
                    hdStationID.Value = Session("DA_EmployeeStationID")
                End If


                hdfRole.Value = ""

                If MyData.Tables(2).Rows.Count > 0 Then
                    ddlRole.DataSource = MyData
                    ddlRole.DataMember = MyData.Tables(2).TableName
                    ddlRole.DataValueField = MyData.Tables(2).Columns(0).ToString
                    ddlRole.DataTextField = MyData.Tables(2).Columns(1).ToString
                    ddlRole.DataBind()
                    ddlRole.SelectedValue = "3"
                End If

                If MyData.Tables(3).Rows.Count > 0 Then
                    hdfRole.Value = MyData.Tables(3).Rows(0)("vchRoleID").ToString
                End If

                ShowData("Role", hdfRole.Value.ToString)


            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub



    Private Sub ShowAccessStation(ByVal strApplyList As String, ByVal strType As String)
        Try
            Dim MyData As New DataSet

            If strType = "AccessStation" Then
                MyData = DIMERCO.SDK.Utilities.LSDK.getStationDataByWithList(strApplyList)
                gvAccessStation.DataSource = MyData
                gvAccessStation.DataMember = MyData.Tables(0).TableName
                gvAccessStation.DataBind()
            End If
        Catch ex As Exception
            Me.lblMsg.Text = "ShowAccessStation: " & ex.Message.ToString
        End Try
    End Sub

    Protected Sub btnAccessStation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccessStation.Click
        Dim strScript As String

        If hdfStation.Value.ToString = "" Then
            Session("DA_ACCESSSTATION") = ""
            Session("DA_New_ACCESSSTATION") = ""
        Else
            Session("DA_ACCESSSTATION") = hdfStation.Value.ToString
        End If

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmAddStation.aspx?ParentForm=frmUserSetting','AddStation','height=550, width=500,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub gvAccessStation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvAccessStation.SelectedIndexChanged
        Dim strStation As String
        Dim strRemove As String = ""


        strRemove = gvAccessStation.SelectedValue.ToString

        strStation = hdfStation.Value.ToString.ToUpper()
        strStation = strStation.Replace(strRemove.ToUpper(), "").ToString
        strStation = strStation.Replace(",,", ",").ToString
        If strStation = "," Then
            strStation = ""
        End If
        hdfStation.Value = strStation

        Session("DA_New_ACCESSSTATION") = strStation
        Session("DA_ACCESSSTATION") = strStation
        ShowAccessStation(strStation, "AccessStation")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("~/System/frmUserList.aspx")
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        GetUserSetting()
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim strRole As String = ""

        If hdfRole.Value.ToString <> "" Then
            strRole = hdfRole.Value.ToString
        End If

        If strRole = "" Then
            strRole = ddlRole.SelectedValue.ToString() + "|"
        Else
            strRole += ddlRole.SelectedValue.ToString() + "|"
        End If

        hdfRole.Value = strRole

        ShowData("Role", strRole)
    End Sub

    Private Sub ShowData(ByVal strDataName As String, ByVal strValue As String)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsData As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetAddData")

                With MyCommand
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@Value", SqlDbType.NVarChar)
                    .Parameters("@Value").Value = strValue
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)

                grvRole.Visible = True
                grvRole.DataSource = MyData.Tables(0)
                grvRole.DataMember = MyData.Tables(0).TableName
                grvRole.DataBind()
            Catch ex As Exception
                Me.lblMsg.Text = "ShowData: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub grvRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvRole.SelectedIndexChanged
        Dim strRole As String
        Dim strRemove As String = ""

        strRemove = grvRole.SelectedValue.ToString.ToUpper

        strRole = hdfRole.Value.ToString.ToUpper
        strRole = strRole.ToUpper.Replace(strRemove + "|", "").ToString
        strRole = strRole.Replace("||", "|").ToString
        hdfRole.Value = strRole
        ShowData("Role", strRole)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim i As Integer = 0
                Dim strRole As String
                Dim strMsg As String = ""
                Dim strStation As String = ""


                lblMsg.Text = ""

                strRole = clsCF.TrimAll(hdfRole.Value.ToString)

                If strRole = "|" Then
                    strRole = ""
                Else
                    If strRole.Contains("1|") = True Or strRole.Contains("2|") = True Then

                    Else
                        If strRole.Contains("99|") = True Or strRole.Contains("4|") = True Or strRole.Contains("7|") = True Then
                            If gvAccessStation.Rows.Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Access Station')</script>")
                                'lblMsg.Text = "Please select Access Station."
                                Exit Sub
                            End If
                        End If
                    End If
                End If

                If strRole.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Role')</script>")
                    'lblMsg.Text = "Please select Role."
                    Exit Sub
                End If

                If txtEmail.Text.Contains("@") = False Then '
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter email address')</script>")
                    'lblMsg.Text = "Please enter email address."
                    Exit Sub
                End If

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveUserSetting", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@DA_UserID", SqlDbType.VarChar, 6)
                    .Parameters("@DA_UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = txtUserID.Text.ToString
                    .Parameters.Add("@Fullname", SqlDbType.NVarChar, 100)
                    .Parameters("@Fullname").Value = lblName.Text.ToString
                    .Parameters.Add("@Email", SqlDbType.NVarChar, 50)
                    .Parameters("@Email").Value = txtEmail.Text.ToString
                    .Parameters.Add("@AsApprovalParty", SqlDbType.TinyInt)
                    If chkAsApprovalParty.Checked = True Then
                        .Parameters("@AsApprovalParty").Value = 1
                    Else
                        .Parameters("@AsApprovalParty").Value = 0
                    End If

                    .Parameters.Add("@AccessStation", SqlDbType.NVarChar)
                    .Parameters("@AccessStation").Value = hdfStation.Value.ToString

                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = Session("DA_EmployeeStationID").ToString

                    .Parameters.Add("@Role", SqlDbType.NVarChar)
                    .Parameters("@Role").Value = strRole
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    If strMsg.Length > 100 Then
                        lblMsg.Text = strMsg
                    Else
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                    End If
                Else
                    GetUserSetting()
                    'lblMsg.Text = "Save Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnRemoveSetting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveSetting.Click

    End Sub
End Class
