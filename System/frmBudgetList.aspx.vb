Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class System_frmBudgetList
    Inherits System.Web.UI.Page

    Dim dsStation As New DataSet

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")
    Dim strExcelExtension As String = ConfigurationSettings.AppSettings("ExcelExtension")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If IsPostBack() = False Then

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            btnSave.Enabled = False
            gvData.Columns(0).Visible = False
            btnGetBRBBudget.Visible = False

            hdfCurrentDateTime.Value = Format(DateTime.Now, "dd MMM yyyy hh:mm:ss").ToString

            If Session("CurrentDateTime") <> Nothing Then
                If Session("CurrentDateTime").ToString <> "" Then
                    hdfCurrentDateTime.Value = Session("CurrentDateTime").ToString
                End If
            End If

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "S0002")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Budget List Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnSave.Enabled = True
                        gvData.Columns(0).Visible = True
                    Else
                        Session("blnWrite") = False
                        btnSave.Enabled = False
                        gvData.Columns(0).Visible = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                    'btnSave.Visible = True
                    'gvData.Columns(0).Visible = True
                Else
                    Session("blnModifyOthers") = False
                    'btnSave.Visible = False
                    'gvData.Columns(0).Visible = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                    'gvData.Columns(0).Visible = True
                Else
                    Session("blnDeleteOthers") = False
                    'gvData.Columns(0).Visible = False
                End If
            End If

            BindDropDownList()
            GetBudgetList()
        Else
            GetBudgetList()
        End If

    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[AccessStation],[Year],[Budget_Category],[ImageDocPath_Temp]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"
                MyData.Tables(1).TableName = "Year"
                MyData.Tables(2).TableName = "Category"

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)


                'If MyData.Tables(0).Rows(0)("AccessStation").ToString <> "" Then
                '    dsStation = DIMERCO.SDK.Utilities.LSDK.getStationDataByWithList(MyData.Tables(0).Rows(0)("AccessStation").ToString)
                'Else
                '    dsStation = DIMERCO.SDK.Utilities.ReSM.GetStationList()
                'End If
                'Dim dtView As DataView = dsStation.Tables(0).DefaultView
                'dtView.Sort = "StationCode ASC"

                'ddlStation.DataSource = dtView
                'ddlStation.DataValueField = dtView.Table.Columns("StationID").ToString
                'ddlStation.DataTextField = dtView.Table.Columns("StationCode").ToString
                'ddlStation.DataBind()

                If Session("DA_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("DA_StationID").ToString
                End If

                If Session("BRB_StationID") <> Nothing Then
                    If Session("BRB_StationID").ToString <> "" Then
                        ddlStation.SelectedValue = Session("BRB_StationID").ToString
                        Session("BRB_StationID") = Nothing
                    End If
                End If

                ddlYear.DataSource = MyData.Tables(1)
                ddlYear.DataValueField = MyData.Tables(1).Columns("Year").ToString
                ddlYear.DataTextField = MyData.Tables(1).Columns("Year").ToString
                ddlYear.DataBind()
                ddlYear.SelectedValue = Now.Year

                If Session("BRB_Year") <> Nothing Then
                    If Session("BRB_Year").ToString <> "" Then
                        ddlYear.SelectedValue = Session("BRB_Year").ToString
                        Session("BRB_Year") = Nothing
                    End If
                End If


                ddlCategory.DataSource = MyData.Tables(2)
                ddlCategory.DataValueField = MyData.Tables(2).Columns("CategoryID").ToString
                ddlCategory.DataTextField = MyData.Tables(2).Columns("Category").ToString
                ddlCategory.DataBind()


                Dim dtView As DataView = clsCF.GetCurrency()
                ddlCurrency.DataSource = dtView
                ddlCurrency.DataValueField = dtView.Table.Columns("HQID").ToString
                ddlCurrency.DataTextField = dtView.Table.Columns("CurrencyCode").ToString
                ddlCurrency.DataBind()

                StationCurrency()

                hdfFilePath.Value = MyData.Tables(3).Rows(0)("ImageDocPath_Temp").ToString + "/" + Session("DA_UserID").ToString

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        Dim intCategoryID As Integer
        intCategoryID = CInt(ddlCategory.SelectedValue.ToString)
        BindDropDownList(intCategoryID)
    End Sub

    Private Sub BindDropDownList(ByVal intID As Integer)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByID")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[Budget_SubCategory]"
                    .Parameters.Add("@ID", SqlDbType.BigInt)
                    .Parameters("@ID").Value = intID
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "SubCategory"


                ddlSubCategory.DataSource = MyData.Tables(0)
                ddlSubCategory.DataValueField = MyData.Tables(0).Columns("CategoryID").ToString
                ddlSubCategory.DataTextField = MyData.Tables(0).Columns("Category").ToString
                ddlSubCategory.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        'btnSearch.Enabled = True
        lblMsg.Text = ""
        lblID.Text = ""
        'If Session("DA_StationID").ToString <> "" Then
        '    ddlStation.SelectedValue = Session("DA_StationID").ToString
        'End If

        'ddlYear.SelectedValue = Now.Year

        If ddlCategory.Items.Count > 0 Then
            ddlCategory.SelectedValue = "0"
        End If
        If ddlSubCategory.Items.Count > 0 Then
            ddlSubCategory.SelectedValue = "0"
        End If
        txtQuantity.Text = ""
        txtBalance.Text = ""
        txtAmount.Text = ""
        txtRemarks.Text = ""
        ddlType.SelectedValue = "Category"
        lblAmountPerUnit.Text = "Amount Per Unit"
        hdfBudgetListItemID.Value = 0
        txtAmtBalance.Text = ""

        ddlStation.Enabled = True
        ddlYear.Enabled = True
        ddlType.Enabled = True
        ddlCategory.Enabled = True
        ddlSubCategory.Enabled = True
        txtQuantity.Enabled = True
        txtAmount.Enabled = True


    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try

                Dim strID As String = ""
                Dim strMsg As String = ""
                'btnSearch.Enabled = True
                lblMsg.Text = ""

                If lblID.Text.ToString = "" Then
                    strID = ""
                Else
                    strID = lblID.Text.ToString
                End If

                If ddlType.SelectedValue.ToString = "Category" Then
                    If ddlCategory.SelectedValue.ToString = "0" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Category')</script>")
                        'lblMsg.Text = "Please select Category."
                        Exit Sub
                    End If

                    If ddlSubCategory.Items.Count > 1 Then
                        If ddlSubCategory.SelectedValue.ToString = "0" Then
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select SubCategory')</script>")
                            'lblMsg.Text = "Please select subCategory."
                            Exit Sub
                        End If
                    End If

                    If txtQuantity.Text.ToString = "" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Quantity')</script>")
                        'lblMsg.Text = "Please enter Quantity."
                        Exit Sub
                    Else
                        If CInt(txtQuantity.Text.ToString) <= 0 Then
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Quantity')</script>")
                            'lblMsg.Text = "Invalid Quantity."
                            Exit Sub
                        End If
                    End If
                End If

                If txtAmount.Text.ToString = "" Then
                    If ddlType.SelectedValue.ToString = "Category" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Amount Per Unit')</script>")
                        'lblMsg.Text = "Please enter Amount Per Unit."
                        Exit Sub
                    Else
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Amount')</script>")
                        'lblMsg.Text = "Please enter Amount."
                        Exit Sub
                    End If
                Else
                    If Convert.ToDecimal(txtAmount.Text.ToString) <= 0 Then
                        'If CInt(txtAmount.Text.ToString) <= 0 Then
                        If ddlType.SelectedValue.ToString = "Category" Then
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Amount Per Unit')</script>")
                            'lblMsg.Text = "Invalid Amount Per Unit."
                            Exit Sub
                        Else
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Amount')</script>")
                            'lblMsg.Text = "Invalid Amount."
                            Exit Sub
                        End If
                    End If
                End If

                If ddlCurrency.SelectedValue = "0" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Currency Code')</script>")
                    Exit Sub
                End If

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveBudgetList", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@BudgetListID", SqlDbType.VarChar, 20)
                    .Parameters("@BudgetListID").Value = strID
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@Year", SqlDbType.Int)
                    .Parameters("@Year").Value = CInt(ddlYear.SelectedValue.ToString)
                    .Parameters.Add("@Type", SqlDbType.NVarChar, 50)
                    .Parameters("@Type").Value = ddlType.SelectedValue.ToString
                    .Parameters.Add("@CategoryID", SqlDbType.BigInt)
                    If ddlType.SelectedValue.ToString = "Category" Then
                        If ddlSubCategory.Items.Count > 1 Then
                            .Parameters("@CategoryID").Value = CInt(ddlSubCategory.SelectedValue.ToString)
                        Else
                            .Parameters("@CategoryID").Value = CInt(ddlCategory.SelectedValue.ToString)
                        End If
                    Else
                        .Parameters("@CategoryID").Value = 0
                    End If


                    .Parameters.Add("@Quantity", SqlDbType.Int)
                    If txtQuantity.Text.ToString <> "" Then
                        .Parameters("@Quantity").Value = CInt(txtQuantity.Text.ToString)
                    Else
                        .Parameters("@Quantity").Value = DBNull.Value
                    End If

                    .Parameters.Add("@CurrencyCodeID", SqlDbType.BigInt)
                    .Parameters("@CurrencyCodeID").Value = CInt(ddlCurrency.SelectedValue.ToString)

                    .Parameters.Add("@Amount", SqlDbType.Decimal, 18, 2)
                    If txtAmount.Text.ToString <> "" Then
                        .Parameters("@Amount").Value = CDec(txtAmount.Text.ToString)
                    Else
                        .Parameters("@Amount").Value = DBNull.Value
                    End If

                    .Parameters.Add("@Remarks", SqlDbType.NVarChar)
                    .Parameters("@Remarks").Value = txtRemarks.Text.ToString
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@BudgetListItemID", SqlDbType.BigInt)
                    If hdfBudgetListItemID.Value.ToString = "" Then
                        .Parameters("@BudgetListItemID").Value = 0
                    Else
                        .Parameters("@BudgetListItemID").Value = CInt(hdfBudgetListItemID.Value.ToString)
                    End If


                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    btnClear_Click(sender, e)
                    GetBudgetList()

                    'lblMsg.Text = "Save Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub gvData_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvData.SelectedIndexChanged
        lblMsg.Text = ""
        'btnSearch.Enabled = False

        Dim strBudgetListItemID As String = 0
        strBudgetListItemID = gvData.SelectedDataKey.Value.ToString
        hdfBudgetListItemID.Value = strBudgetListItemID

        Dim hdfBudgetListID_gv As New HiddenField
        hdfBudgetListID_gv = gvData.SelectedRow.FindControl("hdfBudgetListID")
        lblID.Text = hdfBudgetListID_gv.Value

        Dim lblYear As New Label
        lblYear = gvData.SelectedRow.FindControl("lblYear")
        ddlYear.SelectedValue = lblYear.Text.ToString

        Dim hdStationID As New HiddenField
        hdStationID = gvData.SelectedRow.FindControl("hdStationID")
        ddlStation.SelectedValue = hdStationID.Value.ToString

        Dim lblType As New Label
        lblType = gvData.SelectedRow.FindControl("lblType")
        ddlType.SelectedValue = lblType.Text.ToString
        If lblType.Text.ToString = "Category" Then
            ddlCategory.Enabled = True
            ddlSubCategory.Enabled = True
            txtQuantity.Enabled = True
            lblAmountPerUnit.Text = "Amount Per Unit"
        Else
            ddlCategory.SelectedValue = "0"
            ddlSubCategory.SelectedValue = "0"
            txtQuantity.Text = ""
            ddlCategory.Enabled = False
            ddlSubCategory.Enabled = False
            txtQuantity.Enabled = False
            lblAmountPerUnit.Text = "Amount"
        End If

        If lblType.Text.ToString = "Category" Then
            Dim hdCategory As New HiddenField
            hdCategory = gvData.SelectedRow.FindControl("hdCategory")
            ddlCategory.SelectedValue = CInt(hdCategory.Value.ToString)

            Dim intCategoryID As Integer
            intCategoryID = CInt(ddlCategory.SelectedValue.ToString)
            BindDropDownList(intCategoryID)

            Dim hdSubCategory As New HiddenField
            hdSubCategory = gvData.SelectedRow.FindControl("hdSubCategory")
            ddlSubCategory.SelectedValue = CInt(hdSubCategory.Value.ToString)
        End If

        Dim lblQuantity As New Label
        lblQuantity = gvData.SelectedRow.FindControl("lblQuantity")
        txtQuantity.Text = lblQuantity.Text.ToString

        Dim lblBalance As New Label
        lblBalance = gvData.SelectedRow.FindControl("lblBalance")
        txtBalance.Text = lblBalance.Text.ToString

        Dim hdfCurrencyCodeID As New HiddenField
        hdfCurrencyCodeID = gvData.SelectedRow.FindControl("hdfCurrencyCodeID")
        ddlCurrency.SelectedValue = hdfCurrencyCodeID.Value.ToString

        Dim lblAmount As New Label
        lblAmount = gvData.SelectedRow.FindControl("lblAmount")
        txtAmount.Text = lblAmount.Text.ToString

        Dim lblAmountBalance As New Label
        lblAmountBalance = gvData.SelectedRow.FindControl("lblAmountBalance")
        txtAmtBalance.Text = lblAmountBalance.Text.ToString

        Dim lblRemark As New Label
        lblRemark = gvData.SelectedRow.FindControl("lblRemark")
        txtRemarks.Text = lblRemark.Text.ToString

        Dim lblDataSource As New Label
        lblDataSource = gvData.SelectedRow.FindControl("lblDataSource")

        If lblDataSource.Text.ToString <> "Manual" Then
            ddlStation.Enabled = False
            ddlYear.Enabled = False
            fuFile.Enabled = False
            btnUpload.Enabled = False
            ddlType.Enabled = False
            ddlCategory.Enabled = False
            ddlSubCategory.Enabled = False
            txtQuantity.Enabled = False
            txtBalance.Enabled = False
            ddlCurrency.Enabled = False
            txtAmount.Enabled = False
            txtAmtBalance.Enabled = False
            txtRemarks.Enabled = True
        Else
            ddlStation.Enabled = False
            ddlYear.Enabled = False
            fuFile.Enabled = False
            btnUpload.Enabled = False
            ddlType.Enabled = False
            ddlCategory.Enabled = False
            ddlSubCategory.Enabled = False
            txtQuantity.Enabled = True
            txtBalance.Enabled = False
            ddlCurrency.Enabled = False
            txtAmount.Enabled = True
            txtAmtBalance.Enabled = False
            txtRemarks.Enabled = True
        End If


    End Sub

    Protected Sub gvData_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvData.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim strBudgetListItemID As String = ""
                Dim lbtnDelete As New LinkButton

                lblMsg.Text = ""
                'btnSearch.Enabled = True


                lbtnDelete = gvData.Rows(e.RowIndex).FindControl("lbtnDelete")
                strBudgetListItemID = lbtnDelete.CommandArgument.ToString

                'Dim hdSubCategory As New HiddenField
                'hdSubCategory = gvData.Rows(e.RowIndex).FindControl("hdSubCategory")

                'Dim lblType As New Label
                'lblType = gvData.Rows(e.RowIndex).FindControl("lblType")

                'Dim hdCategory As New HiddenField
                'hdCategory = gvData.Rows(e.RowIndex).FindControl("hdCategory")

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteBudgetList", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@BudgetListItemID", SqlDbType.BigInt)
                    .Parameters("@BudgetListItemID").Value = strBudgetListItemID

                    '.Parameters.Add("@BudgetListID", SqlDbType.VarChar, 20)
                    '.Parameters("@BudgetListID").Value = strBudgetListID
                    '.Parameters.Add("@CategoryID", SqlDbType.BigInt)

                    'If lblType.Text.ToString = "Category" Then
                    '    If hdSubCategory.Value.ToString <> "0" Then
                    '        .Parameters("@CategoryID").Value = CInt(hdSubCategory.Value.ToString)
                    '    Else
                    '        .Parameters("@CategoryID").Value = CInt(hdCategory.Value.ToString)
                    '    End If
                    'Else
                    '    .Parameters("@CategoryID").Value = 0
                    'End If

                    '.Parameters("@CategoryID").Value = CInt(hdSubCategory.Value.ToString)
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    lblID.Text = ""

                    btnClear_Click(sender, e)
                    GetBudgetList()

                    'lblMsg.Text = "Delete Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvData_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        GetBudgetList()
    End Sub

    Private Sub GetBudgetList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                If ddlYear.SelectedValue.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Year')</script>")
                    Exit Sub
                End If

                If ddlStation.SelectedValue.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Station')</script>")
                    Exit Sub
                End If

                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetBudgetList")

                With MyCommand
                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@Year", SqlDbType.Int)
                    .Parameters("@Year").Value = CInt(ddlYear.SelectedValue.ToString)

                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "BudgetList"


                gvData.DataSource = MyData.Tables(0)
                gvData.DataMember = MyData.Tables(0).TableName
                gvData.DataBind()

                btnGetBRBBudget.Visible = False

                fuFile.Enabled = False
                btnUpload.Enabled = False
                btnUpload.Visible = True

                lblTemplate.Text = MyData.Tables(3).Rows(0)("DocPicLink").ToString()

                btnUpload.OnClientClick = "showDate();return confirm('Excel Budget List Uploaded. Do you want replace it?');"

                If MyData.Tables(1).Rows(0)("BRB_CNT").ToString = "0" Then
                    If MyData.Tables(2).Rows(0)("BRBTemp_CNT").ToString <> "0" Then
                        btnGetBRBBudget.Visible = True
                        fuFile.Enabled = True
                        btnUpload.Enabled = True
                        btnUpload.OnClientClick = "showDate();return confirm('Excel Budget List Uploaded. Do you want replace it?');"
                    Else
                        fuFile.Enabled = True
                        btnUpload.Enabled = True
                        btnUpload.OnClientClick = ""
                    End If
                    If CInt(ddlYear.SelectedValue.ToString()) > 2014 Then
                        If CInt(ddlYear.SelectedValue.ToString()) >= CDate(hdfCurrentDateTime.Value.ToString()).Year Then
                            fuFile.Enabled = True
                            btnUpload.Enabled = True

                        End If
                    Else
                        fuFile.Enabled = False
                        btnUpload.Enabled = False
                    End If
                    'Else
                    '    If CInt(ddlYear.SelectedValue.ToString()) > 2014 Then
                    '        If CInt(ddlYear.SelectedValue.ToString()) >= CDate(hdfCurrentDateTime.Value.ToString()).Year Then
                    '            btnGetBRBBudget.Visible = False
                    '            btnUpload.OnClientClick = ""
                    '        End If
                    '    End If
                End If

                    'If MyData.Tables(1).Rows(0)("CreatedBy").ToString <> Session("DA_UserID").ToString Then
                    '    If Session("blnModifyOthers") = False Then
                    '        btnSave.Visible = False
                    '        gvData.Columns(0).Visible = False
                    '    Else
                    '        btnSave.Visible = True
                    '        gvData.Columns(0).Visible = True
                    '    End If

                    '    If Session("blnDeleteOthers") = True Then
                    '        gvData.Columns(0).Visible = True
                    '    Else
                    '        gvData.Columns(0).Visible = False
                    '    End If
                    'End If


            Catch ex As Exception
                Me.lblMsg.Text = "GetBudgetList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        If ddlType.SelectedValue = "Category" Then
            ddlCategory.Enabled = True
            ddlSubCategory.Enabled = True
            txtQuantity.Enabled = True
            lblAmountPerUnit.Text = "Amount Per Unit"
        Else
            If ddlCategory.Items.Count > 0 Then
                ddlCategory.SelectedValue = "0"
            End If
            If ddlSubCategory.Items.Count > 0 Then
                ddlSubCategory.SelectedValue = "0"
            End If
            txtBalance.Text = ""
            txtQuantity.Text = ""
            ddlCategory.Enabled = False
            ddlSubCategory.Enabled = False
            txtQuantity.Enabled = False
            lblAmountPerUnit.Text = "Amount"
            txtAmount.Focus()
        End If
    End Sub

    Protected Sub ddlStation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStation.SelectedIndexChanged
        Session("DA_StationID") = ddlStation.SelectedValue.ToString
        StationCurrency()
        GetBudgetList()
    End Sub

    Private Sub StationCurrency()
        clsCF.StationCurrencyID(ddlStation.SelectedValue.ToString, ddlCurrency)
    End Sub

    Protected Sub btnGetBRBBudget_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetBRBBudget.Click
        Dim strScript As String

        Session("BRB_StationID") = ""
        Session("BRB_Year") = ""
        Session("BRB_StationID") = ddlStation.SelectedValue.ToString()
        Session("BRB_Year") = ddlYear.SelectedValue.ToString()

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('frmBRB_Budget.aspx?ParentForm=frmBudgetList','AddBRBBudget','height=650, width=1000,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        GetBudgetList()
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click

        UploadBudget()

        btnGetBRBBudget.Visible = True
        btnGetBRBBudget_Click(sender, e)
    End Sub

    Private Function SqlBulkCopy(ByVal strPath As String, ByVal strFile As String) As Boolean

        Try

            ' Connection String to Excel Workbook
            Dim MyConnection As System.Data.OleDb.OleDbConnection
            If strFile.Contains(".xls") = True Then
                MyConnection = New System.Data.OleDb.OleDbConnection( _
                "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & strPath & "; Extended Properties='Excel 8.0;HDR=yes;IMEX=1'")
            ElseIf strFile.Contains(".xlsx") = True Then
                MyConnection = New System.Data.OleDb.OleDbConnection( _
                "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & strPath & "; Extended Properties='Excel 12.0;HDR=yes;IMEX=1'")
            End If

            Dim sheetTable As New DataTable
            Dim rowSheetName As DataRow
            Dim sheetName As String

            MyConnection.Open()

            sheetTable = MyConnection.GetSchema("Tables")
            rowSheetName = sheetTable.Rows(0)
            sheetName = rowSheetName(2).ToString()


            ' Create Connection to Excel Workbook
            Dim command As New System.Data.OleDb.OleDbCommand("Select * FROM [" & sheetName & "] WHERE Amount IS NOT NULL ", MyConnection)
            'MyConnection.Open()

            ' Create DbDataReader to Data Worksheet
            Using dr As Common.DbDataReader = command.ExecuteReader()

                Dim dt As DataTable = New DataTable()

                dt.Columns.Add("CreatedBy")
                dt.Columns(0).DefaultValue = Session("DA_UserID").ToString()

                dt.Columns.Add("CreatedOn")
                dt.Columns(1).DefaultValue = hdfCurrentDateTime.Value.ToString

                dt.Columns.Add("Year")
                dt.Columns(2).DefaultValue = ddlYear.SelectedValue.ToString()

                dt.Columns.Add("StationID")
                dt.Columns(3).DefaultValue = ddlStation.SelectedValue.ToString()

                dt.Columns.Add("DataSource")
                dt.Columns(4).DefaultValue = "Excel"

                dt.Columns.Add("Qty_Balance")

                Dim i As Integer = dr.FieldCount
                Dim j As Integer
                For j = 0 To i - 1
                    dt.Columns.Add(dr.GetName(j).ToString)
                Next
                dt.Load(dr)

                Dim drNew As Common.DbDataReader
                drNew = dt.CreateDataReader

                ' SQL Server Connection String. Bulk Copy to SQL Server
                Using bulkCopy As New SqlBulkCopy(strConnectionString)
                    bulkCopy.ColumnMappings.Add("CreatedBy", "CreatedBy")
                    bulkCopy.ColumnMappings.Add("CreatedOn", "CreatedOn")
                    bulkCopy.ColumnMappings.Add("Year", "Year")
                    bulkCopy.ColumnMappings.Add("StationID", "StationID")
                    bulkCopy.ColumnMappings.Add("DataSource", "DataSource")
                    bulkCopy.ColumnMappings.Add("Quantity", "Qty_Balance")

                    bulkCopy.ColumnMappings.Add("Description", "Description")
                    bulkCopy.ColumnMappings.Add("Quantity", "Quantity")
                    bulkCopy.ColumnMappings.Add("Amount", "Amount")
                    bulkCopy.ColumnMappings.Add("TotalAmount", "TotalAmount")
                    bulkCopy.ColumnMappings.Add("Remark", "Remark")

                    bulkCopy.DestinationTableName = "tbBRB_Budget"
                    bulkCopy.WriteToServer(drNew)
                End Using
            End Using
            Return True
        Catch ex As Exception
            Me.lblMsg.Text = "SqlBulkCopy: " & ex.Message.ToString
            Return False
            Exit Function
        End Try
    End Function

    Private Sub UploadBudget()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                lblMsg.Text = ""

                If fuFile.FileName.ToString = "" Then
                    lblMsg.Text = "Please select a file."
                    Exit Sub
                End If

                'Dim fn As String = "E:\EASSET_UPLOAD\DIMPEN_AA.xls"

                'If SqlBulkCopy(fn, "DIMPEN_AA.xls") = False Then
                '    Exit Sub
                'End If

                If (Regex.IsMatch(fuFile.FileName, strExcelExtension, RegexOptions.IgnoreCase)) = False Then
                    lblMsg.Text = "Invalid file format."
                    Exit Sub
                End If

                If FileIO.FileSystem.DirectoryExists(hdfFilePath.Value.ToString + "/") = False Then
                    FileIO.FileSystem.CreateDirectory(hdfFilePath.Value.ToString + "/")
                End If

                Dim strFileName As String = ""
                Dim strFileExt As String = ""
                strFileName = System.IO.Path.GetFileName(fuFile.FileName)
                If strFileName.EndsWith(".xls") Then
                    strFileName = strFileName.Replace(".xls", "")
                    strFileExt = ".xls"
                ElseIf strFileName.EndsWith(".xlsx") Then
                    strFileName = strFileName.Replace(".xlsx", "")
                    strFileExt = ".xlsx"
                End If
                strFileName = strFileName + "_" + Format(DateTime.Now, "dd MMM yyyy hh:mm:ss").ToString.Replace(" ", "").Replace(":", "") + strFileExt


                Dim fn As String = hdfFilePath.Value.ToString + "/" + strFileName

                fuFile.SaveAs(hdfFilePath.Value.ToString + "/" + strFileName)

                'Dim fn As String = hdfFilePath.Value.ToString + "/" + System.IO.Path.GetFileName(fuFile.FileName)
                'fuFile.SaveAs(hdfFilePath.Value.ToString + "/" + System.IO.Path.GetFileName(fuFile.FileName))

                If FileIO.FileSystem.FileExists(fn) = False Then
                    fn = ""
                    lblMsg.Text = "Unable to upload. Please contact System Admin."
                    Exit Sub
                End If

                DeleteExcelBudgetList()

                If SqlBulkCopy(fn, strFileName) = False Then
                    Exit Sub
                End If

                GetBudgetList()

            Catch ex As Exception
                Me.lblMsg.Text = "UploadBudget: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub DeleteExcelBudgetList()
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim strBudgetListItemID As String = ""
                Dim lbtnDelete As New LinkButton

                lblMsg.Text = ""

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteExcelBudgetList", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@Year", SqlDbType.Int)
                    .Parameters("@Year").Value = CInt(ddlYear.SelectedValue.ToString())
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 6)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString()
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "DeleteExcelBudgetList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

End Class
