Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class System_frmUserList
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If


        If IsPostBack() = False Then
            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False


            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "S0005")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If

            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the User List page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                    Else
                        Session("blnWrite") = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            BindDropDownList()
            GetEmployeeList(ddlStation.SelectedValue.ToString, txtName.Text.ToString, ddlStatus.SelectedValue.ToString)
        Else
        End If

    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[AccessStation]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"
                'MyData.Tables(1).TableName = "Year"

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)


                'If MyData.Tables(0).Rows(0)("AccessStation").ToString <> "" Then
                '    dsStation = DIMERCO.SDK.Utilities.LSDK.getStationDataByWithList(MyData.Tables(0).Rows(0)("AccessStation").ToString)
                'Else
                '    dsStation = DIMERCO.SDK.Utilities.ReSM.GetStationList()
                'End If
                'Dim dtView As DataView = dsStation.Tables(0).DefaultView
                'dtView.Sort = "StationCode ASC"

                'ddlStation.DataSource = dtView
                ''ddlStation.DataMember = dtView.Tables(0).TableName
                'ddlStation.DataValueField = dtView.Table.Columns("StationID").ToString
                'ddlStation.DataTextField = dtView.Table.Columns("StationCode").ToString
                'ddlStation.DataBind()

                If Session("DA_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("DA_StationID").ToString
                End If


                Dim dsEmpStatus As New DataSet
                dsEmpStatus = DIMERCO.SDK.Utilities.LSDK.getEmpStatus
                ddlStatus.DataSource = dsEmpStatus
                'Dim i As Integer
                'For i = 0 To dsEmpStatus.Tables(0).Columns.Count - 1
                '    Dim strcolumn As String = dsEmpStatus.Tables(0).Columns(i).ColumnName.ToString()
                'Next
                ddlStatus.DataMember = dsEmpStatus.Tables(0).TableName
                ddlStatus.DataValueField = dsEmpStatus.Tables(0).Columns(0).ToString
                ddlStatus.DataTextField = dsEmpStatus.Tables(0).Columns(0).ToString
                ddlStatus.DataBind()
                ddlStatus.SelectedValue = DIMERCO.SDK.Utilities.LSDK.EmpStatus.ACTIVE

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    'Private Sub BindDropDownList()
    '    Try
    '        Dim dsStation As New DataSet
    '        dsStation = DIMERCO.SDK.Utilities.ReSM.GetStationList()
    '        Dim dtView As DataView = dsStation.Tables(0).DefaultView
    '        dtView.Sort = "StationCode ASC"
    '        ddlStation.DataSource = dtView
    '        'ddlStation.DataMember = dsStation.Tables(0).TableName
    '        ddlStation.DataValueField = dtView.Table.Columns("StationID").ToString
    '        ddlStation.DataTextField = dtView.Table.Columns("StationCode").ToString
    '        ddlStation.DataBind()
    '        If Session("DA_StationID").ToString <> "" Then
    '            ddlStation.SelectedValue = Session("DA_StationID").ToString
    '        End If

    '        Dim dsEmpStatus As New DataSet
    '        dsEmpStatus = DIMERCO.SDK.Utilities.LSDK.getEmpStatus
    '        ddlStatus.DataSource = dsEmpStatus
    '        'Dim i As Integer
    '        'For i = 0 To dsEmpStatus.Tables(0).Columns.Count - 1
    '        '    Dim strcolumn As String = dsEmpStatus.Tables(0).Columns(i).ColumnName.ToString()
    '        'Next
    '        ddlStatus.DataMember = dsEmpStatus.Tables(0).TableName
    '        ddlStatus.DataValueField = dsEmpStatus.Tables(0).Columns(0).ToString
    '        ddlStatus.DataTextField = dsEmpStatus.Tables(0).Columns(0).ToString
    '        ddlStatus.DataBind()
    '        ddlStatus.SelectedValue = DIMERCO.SDK.Utilities.LSDK.EmpStatus.ACTIVE

    '    Catch ex As Exception
    '        Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
    '    End Try
    'End Sub

    Private Sub GetEmployeeList(ByVal strStationID As String, ByVal strName As String, ByVal strStatus As String)

        Dim dsEmployeeList As New DataSet

        dsEmployeeList = DIMERCO.SDK.Utilities.LSDK.getUserProfilebyFullNameNStation(strStationID, strName, strStatus)

        Dim dtView As DataView = dsEmployeeList.Tables(0).DefaultView
        dtView.Sort = "Fullname ASC"

        grvList.DataSource = dtView
        'grvList.DataMember = dsEmployeeList.Tables(0).TableName
        grvList.DataBind()

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Session("DA_StationID") = ddlStation.SelectedValue.ToString
        GetEmployeeList(ddlStation.SelectedValue.ToString, txtName.Text.ToString, ddlStatus.SelectedValue.ToString)
    End Sub

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        Dim strUserID As String = ""
        strUserID = grvList.SelectedValue.ToString()
        Session("DA_EmployeeUserID") = strUserID
        Session("DA_EmployeeStatus") = ddlStatus.SelectedValue.ToString
        Response.Redirect("frmUserSetting.aspx")

    End Sub
End Class
