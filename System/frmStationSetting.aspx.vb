Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class System_frmStationSetting
    Inherits System.Web.UI.Page

    Dim dsStation As New DataSet

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Page.MaintainScrollPositionOnPostBack = True

        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If IsPostBack() = False Then

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            btnClear.Enabled = False
            btnSave.Enabled = False
            btnAdd.Enabled = False
            gvLocation.Columns(0).Visible = False

            lblMsg.Text = ""

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "S0001")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Station Setting Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnClear.Enabled = True
                        btnSave.Enabled = True
                        btnAdd.Enabled = True
                        gvLocation.Columns(0).Visible = True
                    Else
                        Session("blnWrite") = False
                        btnClear.Enabled = False
                        btnSave.Enabled = False
                        btnAdd.Enabled = False
                        gvLocation.Columns(0).Visible = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            BindDropDownList()
        Else

        End If
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)
                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[AccessStation],[CurrencyCode]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"
                MyData.Tables(1).TableName = "CurrencyCode"

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)

                Dim dtViewCurrency As DataView = clsCF.GetCurrency()

                ddlCurrencyCode_IT_L1.DataSource = dtViewCurrency.Table
                ddlCurrencyCode_IT_L1.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlCurrencyCode_IT_L1.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlCurrencyCode_IT_L1.DataBind()

                ddlCurrencyCode_IT_L2.DataSource = dtViewCurrency.Table
                ddlCurrencyCode_IT_L2.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlCurrencyCode_IT_L2.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlCurrencyCode_IT_L2.DataBind()

                ddlCurrencyCode_IT_L3.DataSource = dtViewCurrency.Table
                ddlCurrencyCode_IT_L3.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlCurrencyCode_IT_L3.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlCurrencyCode_IT_L3.DataBind()

                ddlCurrencyCode_IT_L4.DataSource = dtViewCurrency.Table
                ddlCurrencyCode_IT_L4.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlCurrencyCode_IT_L4.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlCurrencyCode_IT_L4.DataBind()

                ddlCurrencyCode_NonIT_L1.DataSource = dtViewCurrency.Table
                ddlCurrencyCode_NonIT_L1.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlCurrencyCode_NonIT_L1.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlCurrencyCode_NonIT_L1.DataBind()

                ddlCurrencyCode_NonIT_L2.DataSource = dtViewCurrency.Table
                ddlCurrencyCode_NonIT_L2.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlCurrencyCode_NonIT_L2.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlCurrencyCode_NonIT_L2.DataBind()

                ddlCurrencyCode_NonIT_L3.DataSource = dtViewCurrency.Table
                ddlCurrencyCode_NonIT_L3.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlCurrencyCode_NonIT_L3.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlCurrencyCode_NonIT_L3.DataBind()

                ddlCurrencyCode_NonIT_L4.DataSource = dtViewCurrency.Table
                ddlCurrencyCode_NonIT_L4.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlCurrencyCode_NonIT_L4.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlCurrencyCode_NonIT_L4.DataBind()

                'NON IT (MAJOR)
                ddlCurrencyCode_NonITMajor_L1.DataSource = dtViewCurrency.Table
                ddlCurrencyCode_NonITMajor_L1.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlCurrencyCode_NonITMajor_L1.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlCurrencyCode_NonITMajor_L1.DataBind()

                ddlCurrencyCode_NonITMajor_L2.DataSource = dtViewCurrency.Table
                ddlCurrencyCode_NonITMajor_L2.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlCurrencyCode_NonITMajor_L2.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlCurrencyCode_NonITMajor_L2.DataBind()

                ddlCurrencyCode_NonITMajor_L3.DataSource = dtViewCurrency.Table
                ddlCurrencyCode_NonITMajor_L3.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlCurrencyCode_NonITMajor_L3.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlCurrencyCode_NonITMajor_L3.DataBind()

                ddlCurrencyCode_NonITMajor_L4.DataSource = dtViewCurrency.Table
                ddlCurrencyCode_NonITMajor_L4.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlCurrencyCode_NonITMajor_L4.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlCurrencyCode_NonITMajor_L4.DataBind()




                ddlUBCurrencyCode_IT_L1.DataSource = dtViewCurrency.Table
                ddlUBCurrencyCode_IT_L1.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlUBCurrencyCode_IT_L1.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlUBCurrencyCode_IT_L1.DataBind()

                ddlUBCurrencyCode_IT_L2.DataSource = dtViewCurrency.Table
                ddlUBCurrencyCode_IT_L2.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlUBCurrencyCode_IT_L2.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlUBCurrencyCode_IT_L2.DataBind()

                ddlUBCurrencyCode_IT_L3.DataSource = dtViewCurrency.Table
                ddlUBCurrencyCode_IT_L3.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlUBCurrencyCode_IT_L3.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlUBCurrencyCode_IT_L3.DataBind()

                ddlUBCurrencyCode_IT_L4.DataSource = dtViewCurrency.Table
                ddlUBCurrencyCode_IT_L4.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlUBCurrencyCode_IT_L4.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlUBCurrencyCode_IT_L4.DataBind()

                ddlUBCurrencyCode_NonIT_L1.DataSource = dtViewCurrency.Table
                ddlUBCurrencyCode_NonIT_L1.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlUBCurrencyCode_NonIT_L1.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlUBCurrencyCode_NonIT_L1.DataBind()

                ddlUBCurrencyCode_NonIT_L2.DataSource = dtViewCurrency.Table
                ddlUBCurrencyCode_NonIT_L2.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlUBCurrencyCode_NonIT_L2.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlUBCurrencyCode_NonIT_L2.DataBind()

                ddlUBCurrencyCode_NonIT_L3.DataSource = dtViewCurrency.Table
                ddlUBCurrencyCode_NonIT_L3.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlUBCurrencyCode_NonIT_L3.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlUBCurrencyCode_NonIT_L3.DataBind()

                ddlUBCurrencyCode_NonIT_L4.DataSource = dtViewCurrency.Table
                ddlUBCurrencyCode_NonIT_L4.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlUBCurrencyCode_NonIT_L4.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlUBCurrencyCode_NonIT_L4.DataBind()

                'NON IT (MAJOR)
                ddlUBCurrencyCode_NonITMajor_L1.DataSource = dtViewCurrency.Table
                ddlUBCurrencyCode_NonITMajor_L1.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlUBCurrencyCode_NonITMajor_L1.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlUBCurrencyCode_NonITMajor_L1.DataBind()

                ddlUBCurrencyCode_NonITMajor_L2.DataSource = dtViewCurrency.Table
                ddlUBCurrencyCode_NonITMajor_L2.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlUBCurrencyCode_NonITMajor_L2.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlUBCurrencyCode_NonITMajor_L2.DataBind()

                ddlUBCurrencyCode_NonITMajor_L3.DataSource = dtViewCurrency.Table
                ddlUBCurrencyCode_NonITMajor_L3.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlUBCurrencyCode_NonITMajor_L3.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlUBCurrencyCode_NonITMajor_L3.DataBind()

                ddlUBCurrencyCode_NonITMajor_L4.DataSource = dtViewCurrency.Table
                ddlUBCurrencyCode_NonITMajor_L4.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlUBCurrencyCode_NonITMajor_L4.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlUBCurrencyCode_NonITMajor_L4.DataBind()


                ddlOBCurrencyCode_IT_L1.DataSource = dtViewCurrency.Table
                ddlOBCurrencyCode_IT_L1.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlOBCurrencyCode_IT_L1.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlOBCurrencyCode_IT_L1.DataBind()

                ddlOBCurrencyCode_IT_L2.DataSource = dtViewCurrency.Table
                ddlOBCurrencyCode_IT_L2.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlOBCurrencyCode_IT_L2.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlOBCurrencyCode_IT_L2.DataBind()

                ddlOBCurrencyCode_IT_L3.DataSource = dtViewCurrency.Table
                ddlOBCurrencyCode_IT_L3.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlOBCurrencyCode_IT_L3.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlOBCurrencyCode_IT_L3.DataBind()

                ddlOBCurrencyCode_IT_L4.DataSource = dtViewCurrency.Table
                ddlOBCurrencyCode_IT_L4.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlOBCurrencyCode_IT_L4.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlOBCurrencyCode_IT_L4.DataBind()

                ddlOBCurrencyCode_NonIT_L1.DataSource = dtViewCurrency.Table
                ddlOBCurrencyCode_NonIT_L1.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlOBCurrencyCode_NonIT_L1.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlOBCurrencyCode_NonIT_L1.DataBind()

                ddlOBCurrencyCode_NonIT_L2.DataSource = dtViewCurrency.Table
                ddlOBCurrencyCode_NonIT_L2.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlOBCurrencyCode_NonIT_L2.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlOBCurrencyCode_NonIT_L2.DataBind()

                ddlOBCurrencyCode_NonIT_L3.DataSource = dtViewCurrency.Table
                ddlOBCurrencyCode_NonIT_L3.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlOBCurrencyCode_NonIT_L3.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlOBCurrencyCode_NonIT_L3.DataBind()

                ddlOBCurrencyCode_NonIT_L4.DataSource = dtViewCurrency.Table
                ddlOBCurrencyCode_NonIT_L4.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlOBCurrencyCode_NonIT_L4.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlOBCurrencyCode_NonIT_L4.DataBind()

                'NON IT (MAJOR)
                ddlOBCurrencyCode_NonITMajor_L1.DataSource = dtViewCurrency.Table
                ddlOBCurrencyCode_NonITMajor_L1.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlOBCurrencyCode_NonITMajor_L1.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlOBCurrencyCode_NonITMajor_L1.DataBind()

                ddlOBCurrencyCode_NonITMajor_L2.DataSource = dtViewCurrency.Table
                ddlOBCurrencyCode_NonITMajor_L2.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlOBCurrencyCode_NonITMajor_L2.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlOBCurrencyCode_NonITMajor_L2.DataBind()

                ddlOBCurrencyCode_NonITMajor_L3.DataSource = dtViewCurrency.Table
                ddlOBCurrencyCode_NonITMajor_L3.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlOBCurrencyCode_NonITMajor_L3.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlOBCurrencyCode_NonITMajor_L3.DataBind()

                ddlOBCurrencyCode_NonITMajor_L4.DataSource = dtViewCurrency.Table
                ddlOBCurrencyCode_NonITMajor_L4.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlOBCurrencyCode_NonITMajor_L4.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlOBCurrencyCode_NonITMajor_L4.DataBind()


                ddlDWCurrencyCode_IT_L1.DataSource = dtViewCurrency.Table
                ddlDWCurrencyCode_IT_L1.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlDWCurrencyCode_IT_L1.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlDWCurrencyCode_IT_L1.DataBind()

                ddlDWCurrencyCode_IT_L2.DataSource = dtViewCurrency.Table
                ddlDWCurrencyCode_IT_L2.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlDWCurrencyCode_IT_L2.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlDWCurrencyCode_IT_L2.DataBind()

                ddlDWCurrencyCode_IT_L3.DataSource = dtViewCurrency.Table
                ddlDWCurrencyCode_IT_L3.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlDWCurrencyCode_IT_L3.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlDWCurrencyCode_IT_L3.DataBind()

                ddlDWCurrencyCode_IT_L4.DataSource = dtViewCurrency.Table
                ddlDWCurrencyCode_IT_L4.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlDWCurrencyCode_IT_L4.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlDWCurrencyCode_IT_L4.DataBind()

                ddlDWCurrencyCode_NonIT_L1.DataSource = dtViewCurrency.Table
                ddlDWCurrencyCode_NonIT_L1.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlDWCurrencyCode_NonIT_L1.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlDWCurrencyCode_NonIT_L1.DataBind()

                ddlDWCurrencyCode_NonIT_L2.DataSource = dtViewCurrency.Table
                ddlDWCurrencyCode_NonIT_L2.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlDWCurrencyCode_NonIT_L2.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlDWCurrencyCode_NonIT_L2.DataBind()

                ddlDWCurrencyCode_NonIT_L3.DataSource = dtViewCurrency.Table
                ddlDWCurrencyCode_NonIT_L3.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlDWCurrencyCode_NonIT_L3.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlDWCurrencyCode_NonIT_L3.DataBind()

                ddlDWCurrencyCode_NonIT_L4.DataSource = dtViewCurrency.Table
                ddlDWCurrencyCode_NonIT_L4.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlDWCurrencyCode_NonIT_L4.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlDWCurrencyCode_NonIT_L4.DataBind()

                'NON IT (MAJOR)
                ddlDWCurrencyCode_NonITMajor_L1.DataSource = dtViewCurrency.Table
                ddlDWCurrencyCode_NonITMajor_L1.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlDWCurrencyCode_NonITMajor_L1.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlDWCurrencyCode_NonITMajor_L1.DataBind()

                ddlDWCurrencyCode_NonITMajor_L2.DataSource = dtViewCurrency.Table
                ddlDWCurrencyCode_NonITMajor_L2.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlDWCurrencyCode_NonITMajor_L2.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlDWCurrencyCode_NonITMajor_L2.DataBind()

                ddlDWCurrencyCode_NonITMajor_L3.DataSource = dtViewCurrency.Table
                ddlDWCurrencyCode_NonITMajor_L3.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlDWCurrencyCode_NonITMajor_L3.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlDWCurrencyCode_NonITMajor_L3.DataBind()

                ddlDWCurrencyCode_NonITMajor_L4.DataSource = dtViewCurrency.Table
                ddlDWCurrencyCode_NonITMajor_L4.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlDWCurrencyCode_NonITMajor_L4.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlDWCurrencyCode_NonITMajor_L4.DataBind()

                ddlCurrencyCode.DataSource = dtViewCurrency.Table
                ddlCurrencyCode.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlCurrencyCode.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlCurrencyCode.DataBind()

                If Session("DA_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("DA_StationID").ToString
                    BindDropDownList_ByStation()
                    ClearData()
                    GetStationSetting()
                    GetStationPORDLC()
                    StationCurrency()
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using

    End Sub

    Private Sub ClearData()
        ddlComparision_IT_L1.SelectedValue = "Select One"
        ddlComparision_IT_L2.SelectedValue = "Select One"
        ddlComparision_IT_L3.SelectedValue = "Select One"
        ddlComparision_IT_L4.SelectedValue = "Select One"

        ddlCurrencyCode_IT_L1.SelectedValue = "0"
        ddlCurrencyCode_IT_L2.SelectedValue = "0"
        ddlCurrencyCode_IT_L3.SelectedValue = "0"
        ddlCurrencyCode_IT_L4.SelectedValue = "0"

        txtAmount_IT_L1.Text = ""
        txtAmount_IT_L2.Text = ""
        txtAmount_IT_L3.Text = ""
        txtAmount_IT_L4.Text = ""

        ddlApprovalParty_IT_L1.Text = ""
        ddlApprovalParty_IT_L2.Text = ""
        ddlApprovalParty_IT_L3.Text = ""
        ddlApprovalParty_IT_L4.Text = ""

        ddlComparision_NonIT_L1.SelectedValue = "Select One"
        ddlComparision_NonIT_L2.SelectedValue = "Select One"
        ddlComparision_NonIT_L3.SelectedValue = "Select One"
        ddlComparision_NonIT_L4.SelectedValue = "Select One"

        ddlCurrencyCode_NonIT_L1.SelectedValue = "0"
        ddlCurrencyCode_NonIT_L2.SelectedValue = "0"
        ddlCurrencyCode_NonIT_L3.SelectedValue = "0"
        ddlCurrencyCode_NonIT_L4.SelectedValue = "0"

        txtAmount_NonIT_L1.Text = ""
        txtAmount_NonIT_L2.Text = ""
        txtAmount_NonIT_L3.Text = ""
        txtAmount_NonIT_L4.Text = ""

        ddlApprovalParty_NonIT_L1.Text = ""
        ddlApprovalParty2_NonIT_L1.Text = ""
        ddlApprovalParty_NonIT_L2.Text = ""
        ddlApprovalParty_NonIT_L3.Text = ""
        ddlApprovalParty_NonIT_L4.Text = ""

        'NON IT (MAJOR)
        ddlComparision_NonITMajor_L1.SelectedValue = "Select One"
        ddlComparision_NonITMajor_L2.SelectedValue = "Select One"
        ddlComparision_NonITMajor_L3.SelectedValue = "Select One"
        ddlComparision_NonITMajor_L4.SelectedValue = "Select One"

        ddlCurrencyCode_NonITMajor_L1.SelectedValue = "0"
        ddlCurrencyCode_NonITMajor_L2.SelectedValue = "0"
        ddlCurrencyCode_NonITMajor_L3.SelectedValue = "0"
        ddlCurrencyCode_NonITMajor_L4.SelectedValue = "0"

        txtAmount_NonITMajor_L1.Text = ""
        txtAmount_NonITMajor_L2.Text = ""
        txtAmount_NonITMajor_L3.Text = ""
        txtAmount_NonITMajor_L4.Text = ""

        ddlApprovalParty_NonITMajor_L1.Text = ""
        ddlApprovalParty2_NonITMajor_L1.Text = ""
        ddlApprovalParty_NonITMajor_L2.Text = ""
        ddlApprovalParty_NonITMajor_L3.Text = ""
        ddlApprovalParty_NonITMajor_L4.Text = ""


        ddlUBComparision_IT_L1.SelectedValue = "Select One"
        ddlUBComparision_IT_L2.SelectedValue = "Select One"
        ddlUBComparision_IT_L3.SelectedValue = "Select One"
        ddlUBComparision_IT_L4.SelectedValue = "Select One"

        ddlUBCurrencyCode_IT_L1.SelectedValue = "0"
        ddlUBCurrencyCode_IT_L2.SelectedValue = "0"
        ddlUBCurrencyCode_IT_L3.SelectedValue = "0"
        ddlUBCurrencyCode_IT_L4.SelectedValue = "0"

        txtUBAmount_IT_L1.Text = ""
        txtUBAmount_IT_L2.Text = ""
        txtUBAmount_IT_L3.Text = ""
        txtUBAmount_IT_L4.Text = ""

        ddlUBApprovalParty_IT_L1.Text = ""
        ddlUBApprovalParty2_IT_L1.Text = ""
        ddlUBApprovalParty_IT_L2.Text = ""
        ddlUBApprovalParty_IT_L3.Text = ""
        ddlUBApprovalParty_IT_L4.Text = ""

        ddlUBComparision_NonIT_L1.SelectedValue = "Select One"
        ddlUBComparision_NonIT_L2.SelectedValue = "Select One"
        ddlUBComparision_NonIT_L3.SelectedValue = "Select One"
        ddlUBComparision_NonIT_L4.SelectedValue = "Select One"

        ddlUBCurrencyCode_NonIT_L1.SelectedValue = "0"
        ddlUBCurrencyCode_NonIT_L2.SelectedValue = "0"
        ddlUBCurrencyCode_NonIT_L3.SelectedValue = "0"
        ddlUBCurrencyCode_NonIT_L4.SelectedValue = "0"

        txtUBAmount_NonIT_L1.Text = ""
        txtUBAmount_NonIT_L2.Text = ""
        txtUBAmount_NonIT_L3.Text = ""
        txtUBAmount_NonIT_L4.Text = ""

        ddlUBApprovalParty_NonIT_L1.Text = ""
        ddlUBApprovalParty2_NonIT_L1.Text = ""
        ddlUBApprovalParty_NonIT_L2.Text = ""
        ddlUBApprovalParty_NonIT_L3.Text = ""
        ddlUBApprovalParty_NonIT_L4.Text = ""

        'NON IT (MAJOR)
        ddlUBComparision_NonITMajor_L1.SelectedValue = "Select One"
        ddlUBComparision_NonITMajor_L2.SelectedValue = "Select One"
        ddlUBComparision_NonITMajor_L3.SelectedValue = "Select One"
        ddlUBComparision_NonITMajor_L4.SelectedValue = "Select One"

        ddlUBCurrencyCode_NonITMajor_L1.SelectedValue = "0"
        ddlUBCurrencyCode_NonITMajor_L2.SelectedValue = "0"
        ddlUBCurrencyCode_NonITMajor_L3.SelectedValue = "0"
        ddlUBCurrencyCode_NonITMajor_L4.SelectedValue = "0"

        txtUBAmount_NonITMajor_L1.Text = ""
        txtUBAmount_NonITMajor_L2.Text = ""
        txtUBAmount_NonITMajor_L3.Text = ""
        txtUBAmount_NonITMajor_L4.Text = ""

        ddlUBApprovalParty_NonITMajor_L1.Text = ""
        ddlUBApprovalParty2_NonITMajor_L1.Text = ""
        ddlUBApprovalParty_NonITMajor_L2.Text = ""
        ddlUBApprovalParty_NonITMajor_L3.Text = ""
        ddlUBApprovalParty_NonITMajor_L4.Text = ""


        ddlOBComparision_IT_L1.SelectedValue = "Select One"
        ddlOBComparision_IT_L2.SelectedValue = "Select One"
        ddlOBComparision_IT_L3.SelectedValue = "Select One"
        ddlOBComparision_IT_L4.SelectedValue = "Select One"

        ddlOBCurrencyCode_IT_L1.SelectedValue = "0"
        ddlOBCurrencyCode_IT_L2.SelectedValue = "0"
        ddlOBCurrencyCode_IT_L3.SelectedValue = "0"
        ddlOBCurrencyCode_IT_L4.SelectedValue = "0"

        txtOBAmount_IT_L1.Text = ""
        txtOBAmount_IT_L2.Text = ""
        txtOBAmount_IT_L3.Text = ""
        txtOBAmount_IT_L4.Text = ""

        ddlOBApprovalParty_IT_L1.Text = ""
        ddlOBApprovalParty2_IT_L1.Text = ""
        ddlOBApprovalParty_IT_L2.Text = ""
        ddlOBApprovalParty_IT_L3.Text = ""
        ddlOBApprovalParty_IT_L4.Text = ""

        ddlOBComparision_NonIT_L1.SelectedValue = "Select One"
        ddlOBComparision_NonIT_L2.SelectedValue = "Select One"
        ddlOBComparision_NonIT_L3.SelectedValue = "Select One"
        ddlOBComparision_NonIT_L4.SelectedValue = "Select One"

        ddlOBCurrencyCode_NonIT_L1.SelectedValue = "0"
        ddlOBCurrencyCode_NonIT_L2.SelectedValue = "0"
        ddlOBCurrencyCode_NonIT_L3.SelectedValue = "0"
        ddlOBCurrencyCode_NonIT_L4.SelectedValue = "0"

        txtOBAmount_NonIT_L1.Text = ""
        txtOBAmount_NonIT_L2.Text = ""
        txtOBAmount_NonIT_L3.Text = ""
        txtOBAmount_NonIT_L4.Text = ""

        ddlOBApprovalParty_NonIT_L1.Text = ""
        ddlOBApprovalParty2_NonIT_L1.Text = ""
        ddlOBApprovalParty_NonIT_L2.Text = ""
        ddlOBApprovalParty_NonIT_L3.Text = ""
        ddlOBApprovalParty_NonIT_L4.Text = ""

        'NON IT (MAJOR)
        ddlOBComparision_NonITMajor_L1.SelectedValue = "Select One"
        ddlOBComparision_NonITMajor_L2.SelectedValue = "Select One"
        ddlOBComparision_NonITMajor_L3.SelectedValue = "Select One"
        ddlOBComparision_NonITMajor_L4.SelectedValue = "Select One"

        ddlOBCurrencyCode_NonITMajor_L1.SelectedValue = "0"
        ddlOBCurrencyCode_NonITMajor_L2.SelectedValue = "0"
        ddlOBCurrencyCode_NonITMajor_L3.SelectedValue = "0"
        ddlOBCurrencyCode_NonITMajor_L4.SelectedValue = "0"

        txtOBAmount_NonITMajor_L1.Text = ""
        txtOBAmount_NonITMajor_L2.Text = ""
        txtOBAmount_NonITMajor_L3.Text = ""
        txtOBAmount_NonITMajor_L4.Text = ""

        ddlOBApprovalParty_NonITMajor_L1.Text = ""
        ddlOBApprovalParty2_NonITMajor_L1.Text = ""
        ddlOBApprovalParty_NonITMajor_L2.Text = ""
        ddlOBApprovalParty_NonITMajor_L3.Text = ""
        ddlOBApprovalParty_NonITMajor_L4.Text = ""


        ddlDWComparision_IT_L1.SelectedValue = "Select One"
        ddlDWComparision_IT_L2.SelectedValue = "Select One"
        ddlDWComparision_IT_L3.SelectedValue = "Select One"
        ddlDWComparision_IT_L4.SelectedValue = "Select One"

        ddlDWCurrencyCode_IT_L1.SelectedValue = "0"
        ddlDWCurrencyCode_IT_L2.SelectedValue = "0"
        ddlDWCurrencyCode_IT_L3.SelectedValue = "0"
        ddlDWCurrencyCode_IT_L4.SelectedValue = "0"

        txtDWAmount_IT_L1.Text = ""
        txtDWAmount_IT_L2.Text = ""
        txtDWAmount_IT_L3.Text = ""
        txtDWAmount_IT_L4.Text = ""

        ddlDWApprovalParty_IT_L1.Text = ""
        ddlDWApprovalParty2_IT_L1.Text = ""
        ddlDWApprovalParty_IT_L2.Text = ""
        ddlDWApprovalParty_IT_L3.Text = ""
        ddlDWApprovalParty_IT_L4.Text = ""


        ddlDWComparision_NonIT_L1.SelectedValue = "Select One"
        ddlDWComparision_NonIT_L2.SelectedValue = "Select One"
        ddlDWComparision_NonIT_L3.SelectedValue = "Select One"
        ddlDWComparision_NonIT_L4.SelectedValue = "Select One"

        ddlDWCurrencyCode_NonIT_L1.SelectedValue = "0"
        ddlDWCurrencyCode_NonIT_L2.SelectedValue = "0"
        ddlDWCurrencyCode_NonIT_L3.SelectedValue = "0"
        ddlDWCurrencyCode_NonIT_L4.SelectedValue = "0"

        txtDWAmount_NonIT_L1.Text = ""
        txtDWAmount_NonIT_L2.Text = ""
        txtDWAmount_NonIT_L3.Text = ""
        txtDWAmount_NonIT_L4.Text = ""

        ddlDWApprovalParty_NonIT_L1.Text = ""
        ddlDWApprovalParty2_NonIT_L1.Text = ""
        ddlDWApprovalParty_NonIT_L2.Text = ""
        ddlDWApprovalParty_NonIT_L3.Text = ""
        ddlDWApprovalParty_NonIT_L4.Text = ""

        'NON IT (MAJOR)
        ddlDWComparision_NonITMajor_L1.SelectedValue = "Select One"
        ddlDWComparision_NonITMajor_L2.SelectedValue = "Select One"
        ddlDWComparision_NonITMajor_L3.SelectedValue = "Select One"
        ddlDWComparision_NonITMajor_L4.SelectedValue = "Select One"

        ddlDWCurrencyCode_NonITMajor_L1.SelectedValue = "0"
        ddlDWCurrencyCode_NonITMajor_L2.SelectedValue = "0"
        ddlDWCurrencyCode_NonITMajor_L3.SelectedValue = "0"
        ddlDWCurrencyCode_NonITMajor_L4.SelectedValue = "0"

        txtDWAmount_NonITMajor_L1.Text = ""
        txtDWAmount_NonITMajor_L2.Text = ""
        txtDWAmount_NonITMajor_L3.Text = ""
        txtDWAmount_NonITMajor_L4.Text = ""

        ddlDWApprovalParty_NonITMajor_L1.Text = ""
        ddlDWApprovalParty2_NonITMajor_L1.Text = ""
        ddlDWApprovalParty_NonITMajor_L2.Text = ""
        ddlDWApprovalParty_NonITMajor_L3.Text = ""
        ddlDWApprovalParty_NonITMajor_L4.Text = ""


        txtLocation.Text = ""
        lblID.Text = ""
        ddlCurrencyCode.SelectedValue = "0"
        txtQuoPricePoint.Text = ""
        txteReqPricePoint.Text = ""
        txtFAPricePoint.Text = ""
        txtCARPricePoint.Text = ""

        chkAutoPONo.Checked = False
        fuPO.Enabled = True
        btnAttach.Enabled = True
        gvPORDLC.Dispose()
        gvPORDLC.Visible = False

        ddleReq_1st.SelectedValue = ""
        ddleReq_2nd.SelectedValue = ""
        ddleReq_3rd.SelectedValue = ""
        ddlIssuePO_1st.SelectedValue = ""
        ddlIssuePO_2nd.SelectedValue = ""
        ddlIssuePO_3rd.SelectedValue = ""
        ddlIssuePayment_1st.SelectedValue = ""
        ddlIssuePayment_2nd.SelectedValue = ""
        ddlIssuePayment_3rd.SelectedValue = ""
        ddleFMS_1st.SelectedValue = ""
        ddleFMS_2nd.SelectedValue = ""
        ddleFMS_3rd.SelectedValue = ""


        lblMsg_Location.Text = ""
        lblMsg_CurrencyCode.Text = ""
        lblMsg_Quotation.Text = ""
        lblMsg_eReq.Text = ""
        lblMsg_FA.Text = ""
        lblMsg_CAR.Text = ""
        lblMsg_MailNotify.Text = ""
        lblMsg_Comparision_IT_L1.Text = ""
        lblMsg_Comparision_IT_L2.Text = ""
        lblMsg_Comparision_IT_L3.Text = ""
        lblMsg_Comparision_IT_L4.Text = ""
        lblMsg_CurrencyCode_IT_L1.Text = ""
        lblMsg_CurrencyCode_IT_L2.Text = ""
        lblMsg_CurrencyCode_IT_L3.Text = ""
        lblMsg_CurrencyCode_IT_L4.Text = ""
        lblMsg_Amount_IT_L1.Text = ""
        lblMsg_Amount_IT_L2.Text = ""
        lblMsg_Amount_IT_L3.Text = ""
        lblMsg_Amount_IT_L4.Text = ""
        lblMsg_ApprovalParty_IT_L1.Text = ""
        lblMsg_ApprovalParty_IT_L2.Text = ""
        lblMsg_ApprovalParty_IT_L3.Text = ""
        lblMsg_ApprovalParty_IT_L4.Text = ""

        lblMsg_Comparision_NonIT_L1.Text = ""
        lblMsg_Comparision_NonIT_L2.Text = ""
        lblMsg_Comparision_NonIT_L3.Text = ""
        lblMsg_Comparision_NonIT_L4.Text = ""
        lblMsg_CurrencyCode_NonIT_L1.Text = ""
        lblMsg_CurrencyCode_NonIT_L2.Text = ""
        lblMsg_CurrencyCode_NonIT_L3.Text = ""
        lblMsg_CurrencyCode_NonIT_L4.Text = ""
        lblMsg_Amount_NonIT_L1.Text = ""
        lblMsg_Amount_NonIT_L2.Text = ""
        lblMsg_Amount_NonIT_L3.Text = ""
        lblMsg_Amount_NonIT_L4.Text = ""
        lblMsg_ApprovalParty_NonIT_L1.Text = ""
        lblMsg_ApprovalParty_NonIT_L2.Text = ""
        lblMsg_ApprovalParty_NonIT_L3.Text = ""
        lblMsg_ApprovalParty_NonIT_L4.Text = ""

        'NON IT (MAJOR)
        lblMsg_Comparision_NonITMajor_L1.Text = ""
        lblMsg_Comparision_NonITMajor_L2.Text = ""
        lblMsg_Comparision_NonITMajor_L3.Text = ""
        lblMsg_Comparision_NonITMajor_L4.Text = ""
        lblMsg_CurrencyCode_NonITMajor_L1.Text = ""
        lblMsg_CurrencyCode_NonITMajor_L2.Text = ""
        lblMsg_CurrencyCode_NonITMajor_L3.Text = ""
        lblMsg_CurrencyCode_NonITMajor_L4.Text = ""
        lblMsg_Amount_NonITMajor_L1.Text = ""
        lblMsg_Amount_NonITMajor_L2.Text = ""
        lblMsg_Amount_NonITMajor_L3.Text = ""
        lblMsg_Amount_NonITMajor_L4.Text = ""
        lblMsg_ApprovalParty_NonITMajor_L1.Text = ""
        lblMsg_ApprovalParty_NonITMajor_L2.Text = ""
        lblMsg_ApprovalParty_NonITMajor_L3.Text = ""
        lblMsg_ApprovalParty_NonITMajor_L4.Text = ""

        lblMsg_UBComparision_IT_L1.Text = ""
        lblMsg_UBComparision_IT_L2.Text = ""
        lblMsg_UBComparision_IT_L3.Text = ""
        lblMsg_UBComparision_IT_L4.Text = ""
        lblMsg_UBCurrencyCode_IT_L1.Text = ""
        lblMsg_UBCurrencyCode_IT_L2.Text = ""
        lblMsg_UBCurrencyCode_IT_L3.Text = ""
        lblMsg_UBCurrencyCode_IT_L4.Text = ""
        lblMsg_UBAmount_IT_L1.Text = ""
        lblMsg_UBAmount_IT_L2.Text = ""
        lblMsg_UBAmount_IT_L3.Text = ""
        lblMsg_UBAmount_IT_L4.Text = ""
        lblMsg_UBApprovalParty_IT_L1.Text = ""
        lblMsg_UBApprovalParty_IT_L2.Text = ""
        lblMsg_UBApprovalParty_IT_L3.Text = ""
        lblMsg_UBApprovalParty_IT_L4.Text = ""

        lblMsg_UBComparision_NonIT_L1.Text = ""
        lblMsg_UBComparision_NonIT_L2.Text = ""
        lblMsg_UBComparision_NonIT_L3.Text = ""
        lblMsg_UBComparision_NonIT_L4.Text = ""
        lblMsg_UBCurrencyCode_NonIT_L1.Text = ""
        lblMsg_UBCurrencyCode_NonIT_L2.Text = ""
        lblMsg_UBCurrencyCode_NonIT_L3.Text = ""
        lblMsg_UBCurrencyCode_NonIT_L4.Text = ""
        lblMsg_UBAmount_NonIT_L1.Text = ""
        lblMsg_UBAmount_NonIT_L2.Text = ""
        lblMsg_UBAmount_NonIT_L3.Text = ""
        lblMsg_UBAmount_NonIT_L4.Text = ""
        lblMsg_UBApprovalParty_NonIT_L1.Text = ""
        lblMsg_UBApprovalParty_NonIT_L2.Text = ""
        lblMsg_UBApprovalParty_NonIT_L3.Text = ""
        lblMsg_UBApprovalParty_NonIT_L4.Text = ""

        'NON IT (MAJOR)
        lblMsg_UBComparision_NonITMajor_L1.Text = ""
        lblMsg_UBComparision_NonITMajor_L2.Text = ""
        lblMsg_UBComparision_NonITMajor_L3.Text = ""
        lblMsg_UBComparision_NonITMajor_L4.Text = ""
        lblMsg_UBCurrencyCode_NonITMajor_L1.Text = ""
        lblMsg_UBCurrencyCode_NonITMajor_L2.Text = ""
        lblMsg_UBCurrencyCode_NonITMajor_L3.Text = ""
        lblMsg_UBCurrencyCode_NonITMajor_L4.Text = ""
        lblMsg_UBAmount_NonITMajor_L1.Text = ""
        lblMsg_UBAmount_NonITMajor_L2.Text = ""
        lblMsg_UBAmount_NonITMajor_L3.Text = ""
        lblMsg_UBAmount_NonITMajor_L4.Text = ""
        lblMsg_UBApprovalParty_NonITMajor_L1.Text = ""
        lblMsg_UBApprovalParty_NonITMajor_L2.Text = ""
        lblMsg_UBApprovalParty_NonITMajor_L3.Text = ""
        lblMsg_UBApprovalParty_NonITMajor_L4.Text = ""


        lblMsg_OBComparision_IT_L1.Text = ""
        lblMsg_OBComparision_IT_L2.Text = ""
        lblMsg_OBComparision_IT_L3.Text = ""
        lblMsg_OBComparision_IT_L4.Text = ""
        lblMsg_OBCurrencyCode_IT_L1.Text = ""
        lblMsg_OBCurrencyCode_IT_L2.Text = ""
        lblMsg_OBCurrencyCode_IT_L3.Text = ""
        lblMsg_OBCurrencyCode_IT_L4.Text = ""
        lblMsg_OBAmount_IT_L1.Text = ""
        lblMsg_OBAmount_IT_L2.Text = ""
        lblMsg_OBAmount_IT_L3.Text = ""
        lblMsg_OBAmount_IT_L4.Text = ""
        lblMsg_OBApprovalParty_IT_L1.Text = ""
        lblMsg_OBApprovalParty_IT_L2.Text = ""
        lblMsg_OBApprovalParty_IT_L3.Text = ""
        lblMsg_OBApprovalParty_IT_L4.Text = ""

        lblMsg_OBComparision_NonIT_L1.Text = ""
        lblMsg_OBComparision_NonIT_L2.Text = ""
        lblMsg_OBComparision_NonIT_L3.Text = ""
        lblMsg_OBComparision_NonIT_L4.Text = ""
        lblMsg_OBCurrencyCode_NonIT_L1.Text = ""
        lblMsg_OBCurrencyCode_NonIT_L2.Text = ""
        lblMsg_OBCurrencyCode_NonIT_L3.Text = ""
        lblMsg_OBCurrencyCode_NonIT_L4.Text = ""
        lblMsg_OBAmount_NonIT_L1.Text = ""
        lblMsg_OBAmount_NonIT_L2.Text = ""
        lblMsg_OBAmount_NonIT_L3.Text = ""
        lblMsg_OBAmount_NonIT_L4.Text = ""
        lblMsg_OBApprovalParty_NonIT_L1.Text = ""
        lblMsg_OBApprovalParty_NonIT_L2.Text = ""
        lblMsg_OBApprovalParty_NonIT_L3.Text = ""
        lblMsg_OBApprovalParty_NonIT_L4.Text = ""

        'NON IT (MAJOR)
        lblMsg_OBComparision_NonITMajor_L1.Text = ""
        lblMsg_OBComparision_NonITMajor_L2.Text = ""
        lblMsg_OBComparision_NonITMajor_L3.Text = ""
        lblMsg_OBComparision_NonITMajor_L4.Text = ""
        lblMsg_OBCurrencyCode_NonITMajor_L1.Text = ""
        lblMsg_OBCurrencyCode_NonITMajor_L2.Text = ""
        lblMsg_OBCurrencyCode_NonITMajor_L3.Text = ""
        lblMsg_OBCurrencyCode_NonITMajor_L4.Text = ""
        lblMsg_OBAmount_NonITMajor_L1.Text = ""
        lblMsg_OBAmount_NonITMajor_L2.Text = ""
        lblMsg_OBAmount_NonITMajor_L3.Text = ""
        lblMsg_OBAmount_NonITMajor_L4.Text = ""
        lblMsg_OBApprovalParty_NonITMajor_L1.Text = ""
        lblMsg_OBApprovalParty_NonITMajor_L2.Text = ""
        lblMsg_OBApprovalParty_NonITMajor_L3.Text = ""
        lblMsg_OBApprovalParty_NonITMajor_L4.Text = ""

        lblMsg_DWComparision_IT_L1.Text = ""
        lblMsg_DWComparision_IT_L2.Text = ""
        lblMsg_DWComparision_IT_L3.Text = ""
        lblMsg_DWComparision_IT_L4.Text = ""
        lblMsg_DWCurrencyCode_IT_L1.Text = ""
        lblMsg_DWCurrencyCode_IT_L2.Text = ""
        lblMsg_DWCurrencyCode_IT_L3.Text = ""
        lblMsg_DWCurrencyCode_IT_L4.Text = ""
        lblMsg_DWAmount_IT_L1.Text = ""
        lblMsg_DWAmount_IT_L2.Text = ""
        lblMsg_DWAmount_IT_L3.Text = ""
        lblMsg_DWAmount_IT_L4.Text = ""
        lblMsg_DWApprovalParty_IT_L1.Text = ""
        lblMsg_DWApprovalParty_IT_L2.Text = ""
        lblMsg_DWApprovalParty_IT_L3.Text = ""
        lblMsg_DWApprovalParty_IT_L4.Text = ""


        lblMsg_DWComparision_NonIT_L1.Text = ""
        lblMsg_DWComparision_NonIT_L2.Text = ""
        lblMsg_DWComparision_NonIT_L3.Text = ""
        lblMsg_DWComparision_NonIT_L4.Text = ""
        lblMsg_DWCurrencyCode_NonIT_L1.Text = ""
        lblMsg_DWCurrencyCode_NonIT_L2.Text = ""
        lblMsg_DWCurrencyCode_NonIT_L3.Text = ""
        lblMsg_DWCurrencyCode_NonIT_L4.Text = ""
        lblMsg_DWAmount_NonIT_L1.Text = ""
        lblMsg_DWAmount_NonIT_L2.Text = ""
        lblMsg_DWAmount_NonIT_L3.Text = ""
        lblMsg_DWAmount_NonIT_L4.Text = ""
        lblMsg_DWApprovalParty_NonIT_L1.Text = ""
        lblMsg_DWApprovalParty_NonIT_L2.Text = ""
        lblMsg_DWApprovalParty_NonIT_L3.Text = ""
        lblMsg_DWApprovalParty_NonIT_L4.Text = ""

        'NON IT (MAJOR)
        lblMsg_DWComparision_NonITMajor_L1.Text = ""
        lblMsg_DWComparision_NonITMajor_L2.Text = ""
        lblMsg_DWComparision_NonITMajor_L3.Text = ""
        lblMsg_DWComparision_NonITMajor_L4.Text = ""
        lblMsg_DWCurrencyCode_NonITMajor_L1.Text = ""
        lblMsg_DWCurrencyCode_NonITMajor_L2.Text = ""
        lblMsg_DWCurrencyCode_NonITMajor_L3.Text = ""
        lblMsg_DWCurrencyCode_NonITMajor_L4.Text = ""
        lblMsg_DWAmount_NonITMajor_L1.Text = ""
        lblMsg_DWAmount_NonITMajor_L2.Text = ""
        lblMsg_DWAmount_NonITMajor_L3.Text = ""
        lblMsg_DWAmount_NonITMajor_L4.Text = ""
        lblMsg_DWApprovalParty_NonITMajor_L1.Text = ""
        lblMsg_DWApprovalParty_NonITMajor_L2.Text = ""
        lblMsg_DWApprovalParty_NonITMajor_L3.Text = ""
        lblMsg_DWApprovalParty_NonITMajor_L4.Text = ""


    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click

        ClearData()

        If Session("DA_StationID").ToString <> "" Then
            ddlStation.SelectedValue = Session("DA_StationID").ToString
            BindDropDownList_ByStation()
        End If
    End Sub

    Protected Sub ddlStation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStation.SelectedIndexChanged
        lblMsg.Text = ""
        If ddlStation.SelectedValue.ToString = "" Then
            lblMsg.Text = "Please select Station."
            Exit Sub
        End If
        Session("DA_StationID") = ddlStation.SelectedValue.ToString

        BindDropDownList_ByStation()
        ClearData()
        GetStationSetting()
        GetStationPORDLC()
        StationCurrency()
    End Sub


    Private Sub BindDropDownList_ByStation()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByStation")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[ApprovalParty],[User]"
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "ApprovalParty"

                ddlApprovalParty_IT_L1.DataSource = MyData.Tables(0)
                ddlApprovalParty_IT_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlApprovalParty_IT_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlApprovalParty_IT_L1.DataBind()

                ddlApprovalParty2_IT_L1.DataSource = MyData.Tables(0)
                ddlApprovalParty2_IT_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlApprovalParty2_IT_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlApprovalParty2_IT_L1.DataBind()

                ddlApprovalParty_IT_L2.DataSource = MyData.Tables(0)
                ddlApprovalParty_IT_L2.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlApprovalParty_IT_L2.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlApprovalParty_IT_L2.DataBind()

                ddlApprovalParty_IT_L3.DataSource = MyData.Tables(0)
                ddlApprovalParty_IT_L3.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlApprovalParty_IT_L3.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlApprovalParty_IT_L3.DataBind()

                ddlApprovalParty_IT_L4.DataSource = MyData.Tables(0)
                ddlApprovalParty_IT_L4.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlApprovalParty_IT_L4.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlApprovalParty_IT_L4.DataBind()


                ddlApprovalParty_NonIT_L1.DataSource = MyData.Tables(0)
                ddlApprovalParty_NonIT_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlApprovalParty_NonIT_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlApprovalParty_NonIT_L1.DataBind()

                ddlApprovalParty2_NonIT_L1.DataSource = MyData.Tables(0)
                ddlApprovalParty2_NonIT_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlApprovalParty2_NonIT_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlApprovalParty2_NonIT_L1.DataBind()

                ddlApprovalParty_NonIT_L2.DataSource = MyData.Tables(0)
                ddlApprovalParty_NonIT_L2.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlApprovalParty_NonIT_L2.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlApprovalParty_NonIT_L2.DataBind()

                ddlApprovalParty_NonIT_L3.DataSource = MyData.Tables(0)
                ddlApprovalParty_NonIT_L3.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlApprovalParty_NonIT_L3.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlApprovalParty_NonIT_L3.DataBind()

                ddlApprovalParty_NonIT_L4.DataSource = MyData.Tables(0)
                ddlApprovalParty_NonIT_L4.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlApprovalParty_NonIT_L4.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlApprovalParty_NonIT_L4.DataBind()

                'NON IT (MAJOR)
                ddlApprovalParty_NonITMajor_L1.DataSource = MyData.Tables(0)
                ddlApprovalParty_NonITMajor_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlApprovalParty_NonITMajor_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlApprovalParty_NonITMajor_L1.DataBind()

                ddlApprovalParty2_NonITMajor_L1.DataSource = MyData.Tables(0)
                ddlApprovalParty2_NonITMajor_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlApprovalParty2_NonITMajor_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlApprovalParty2_NonITMajor_L1.DataBind()

                ddlApprovalParty_NonITMajor_L2.DataSource = MyData.Tables(0)
                ddlApprovalParty_NonITMajor_L2.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlApprovalParty_NonITMajor_L2.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlApprovalParty_NonITMajor_L2.DataBind()

                ddlApprovalParty_NonITMajor_L3.DataSource = MyData.Tables(0)
                ddlApprovalParty_NonITMajor_L3.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlApprovalParty_NonITMajor_L3.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlApprovalParty_NonITMajor_L3.DataBind()

                ddlApprovalParty_NonITMajor_L4.DataSource = MyData.Tables(0)
                ddlApprovalParty_NonITMajor_L4.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlApprovalParty_NonITMajor_L4.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlApprovalParty_NonITMajor_L4.DataBind()


                ddlUBApprovalParty_IT_L1.DataSource = MyData.Tables(0)
                ddlUBApprovalParty_IT_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlUBApprovalParty_IT_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlUBApprovalParty_IT_L1.DataBind()

                ddlUBApprovalParty2_IT_L1.DataSource = MyData.Tables(0)
                ddlUBApprovalParty2_IT_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlUBApprovalParty2_IT_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlUBApprovalParty2_IT_L1.DataBind()

                ddlUBApprovalParty_IT_L2.DataSource = MyData.Tables(0)
                ddlUBApprovalParty_IT_L2.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlUBApprovalParty_IT_L2.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlUBApprovalParty_IT_L2.DataBind()

                ddlUBApprovalParty_IT_L3.DataSource = MyData.Tables(0)
                ddlUBApprovalParty_IT_L3.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlUBApprovalParty_IT_L3.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlUBApprovalParty_IT_L3.DataBind()

                ddlUBApprovalParty_IT_L4.DataSource = MyData.Tables(0)
                ddlUBApprovalParty_IT_L4.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlUBApprovalParty_IT_L4.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlUBApprovalParty_IT_L4.DataBind()


                ddlUBApprovalParty_NonIT_L1.DataSource = MyData.Tables(0)
                ddlUBApprovalParty_NonIT_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlUBApprovalParty_NonIT_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlUBApprovalParty_NonIT_L1.DataBind()

                ddlUBApprovalParty2_NonIT_L1.DataSource = MyData.Tables(0)
                ddlUBApprovalParty2_NonIT_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlUBApprovalParty2_NonIT_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlUBApprovalParty2_NonIT_L1.DataBind()

                ddlUBApprovalParty_NonIT_L2.DataSource = MyData.Tables(0)
                ddlUBApprovalParty_NonIT_L2.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlUBApprovalParty_NonIT_L2.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlUBApprovalParty_NonIT_L2.DataBind()

                ddlUBApprovalParty_NonIT_L3.DataSource = MyData.Tables(0)
                ddlUBApprovalParty_NonIT_L3.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlUBApprovalParty_NonIT_L3.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlUBApprovalParty_NonIT_L3.DataBind()

                ddlUBApprovalParty_NonIT_L4.DataSource = MyData.Tables(0)
                ddlUBApprovalParty_NonIT_L4.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlUBApprovalParty_NonIT_L4.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlUBApprovalParty_NonIT_L4.DataBind()

                'NON IT (MAJOR)
                ddlUBApprovalParty_NonITMajor_L1.DataSource = MyData.Tables(0)
                ddlUBApprovalParty_NonITMajor_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlUBApprovalParty_NonITMajor_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlUBApprovalParty_NonITMajor_L1.DataBind()

                ddlUBApprovalParty2_NonITMajor_L1.DataSource = MyData.Tables(0)
                ddlUBApprovalParty2_NonITMajor_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlUBApprovalParty2_NonITMajor_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlUBApprovalParty2_NonITMajor_L1.DataBind()

                ddlUBApprovalParty_NonITMajor_L2.DataSource = MyData.Tables(0)
                ddlUBApprovalParty_NonITMajor_L2.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlUBApprovalParty_NonITMajor_L2.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlUBApprovalParty_NonITMajor_L2.DataBind()

                ddlUBApprovalParty_NonITMajor_L3.DataSource = MyData.Tables(0)
                ddlUBApprovalParty_NonITMajor_L3.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlUBApprovalParty_NonITMajor_L3.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlUBApprovalParty_NonITMajor_L3.DataBind()

                ddlUBApprovalParty_NonITMajor_L4.DataSource = MyData.Tables(0)
                ddlUBApprovalParty_NonITMajor_L4.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlUBApprovalParty_NonITMajor_L4.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlUBApprovalParty_NonITMajor_L4.DataBind()

                ''''
                ddlOBApprovalParty_IT_L1.DataSource = MyData.Tables(0)
                ddlOBApprovalParty_IT_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlOBApprovalParty_IT_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlOBApprovalParty_IT_L1.DataBind()

                ddlOBApprovalParty2_IT_L1.DataSource = MyData.Tables(0)
                ddlOBApprovalParty2_IT_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlOBApprovalParty2_IT_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlOBApprovalParty2_IT_L1.DataBind()

                ddlOBApprovalParty_IT_L2.DataSource = MyData.Tables(0)
                ddlOBApprovalParty_IT_L2.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlOBApprovalParty_IT_L2.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlOBApprovalParty_IT_L2.DataBind()

                ddlOBApprovalParty_IT_L3.DataSource = MyData.Tables(0)
                ddlOBApprovalParty_IT_L3.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlOBApprovalParty_IT_L3.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlOBApprovalParty_IT_L3.DataBind()

                ddlOBApprovalParty_IT_L4.DataSource = MyData.Tables(0)
                ddlOBApprovalParty_IT_L4.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlOBApprovalParty_IT_L4.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlOBApprovalParty_IT_L4.DataBind()


                ddlOBApprovalParty_NonIT_L1.DataSource = MyData.Tables(0)
                ddlOBApprovalParty_NonIT_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlOBApprovalParty_NonIT_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlOBApprovalParty_NonIT_L1.DataBind()


                ddlOBApprovalParty2_NonIT_L1.DataSource = MyData.Tables(0)
                ddlOBApprovalParty2_NonIT_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlOBApprovalParty2_NonIT_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlOBApprovalParty2_NonIT_L1.DataBind()


                ddlOBApprovalParty_NonIT_L2.DataSource = MyData.Tables(0)
                ddlOBApprovalParty_NonIT_L2.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlOBApprovalParty_NonIT_L2.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlOBApprovalParty_NonIT_L2.DataBind()

                ddlOBApprovalParty_NonIT_L3.DataSource = MyData.Tables(0)
                ddlOBApprovalParty_NonIT_L3.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlOBApprovalParty_NonIT_L3.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlOBApprovalParty_NonIT_L3.DataBind()

                ddlOBApprovalParty_NonIT_L4.DataSource = MyData.Tables(0)
                ddlOBApprovalParty_NonIT_L4.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlOBApprovalParty_NonIT_L4.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlOBApprovalParty_NonIT_L4.DataBind()
                ''''

                'NON IT (MAJOR)
                ddlOBApprovalParty_NonITMajor_L1.DataSource = MyData.Tables(0)
                ddlOBApprovalParty_NonITMajor_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlOBApprovalParty_NonITMajor_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlOBApprovalParty_NonITMajor_L1.DataBind()

                ddlOBApprovalParty2_NonITMajor_L1.DataSource = MyData.Tables(0)
                ddlOBApprovalParty2_NonITMajor_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlOBApprovalParty2_NonITMajor_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlOBApprovalParty2_NonITMajor_L1.DataBind()

                ddlOBApprovalParty_NonITMajor_L2.DataSource = MyData.Tables(0)
                ddlOBApprovalParty_NonITMajor_L2.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlOBApprovalParty_NonITMajor_L2.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlOBApprovalParty_NonITMajor_L2.DataBind()

                ddlOBApprovalParty_NonITMajor_L3.DataSource = MyData.Tables(0)
                ddlOBApprovalParty_NonITMajor_L3.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlOBApprovalParty_NonITMajor_L3.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlOBApprovalParty_NonITMajor_L3.DataBind()

                ddlOBApprovalParty_NonITMajor_L4.DataSource = MyData.Tables(0)
                ddlOBApprovalParty_NonITMajor_L4.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlOBApprovalParty_NonITMajor_L4.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlOBApprovalParty_NonITMajor_L4.DataBind()


                ddlDWApprovalParty_IT_L1.DataSource = MyData.Tables(0)
                ddlDWApprovalParty_IT_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlDWApprovalParty_IT_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlDWApprovalParty_IT_L1.DataBind()

                ddlDWApprovalParty2_IT_L1.DataSource = MyData.Tables(0)
                ddlDWApprovalParty2_IT_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlDWApprovalParty2_IT_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlDWApprovalParty2_IT_L1.DataBind()

                ddlDWApprovalParty_IT_L2.DataSource = MyData.Tables(0)
                ddlDWApprovalParty_IT_L2.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlDWApprovalParty_IT_L2.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlDWApprovalParty_IT_L2.DataBind()

                ddlDWApprovalParty_IT_L3.DataSource = MyData.Tables(0)
                ddlDWApprovalParty_IT_L3.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlDWApprovalParty_IT_L3.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlDWApprovalParty_IT_L3.DataBind()

                ddlDWApprovalParty_IT_L4.DataSource = MyData.Tables(0)
                ddlDWApprovalParty_IT_L4.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlDWApprovalParty_IT_L4.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlDWApprovalParty_IT_L4.DataBind()

                ''''
                ddlDWApprovalParty_NonIT_L1.DataSource = MyData.Tables(0)
                ddlDWApprovalParty_NonIT_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlDWApprovalParty_NonIT_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlDWApprovalParty_NonIT_L1.DataBind()

                ddlDWApprovalParty2_NonIT_L1.DataSource = MyData.Tables(0)
                ddlDWApprovalParty2_NonIT_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlDWApprovalParty2_NonIT_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlDWApprovalParty2_NonIT_L1.DataBind()

                ddlDWApprovalParty_NonIT_L2.DataSource = MyData.Tables(0)
                ddlDWApprovalParty_NonIT_L2.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlDWApprovalParty_NonIT_L2.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlDWApprovalParty_NonIT_L2.DataBind()

                ddlDWApprovalParty_NonIT_L3.DataSource = MyData.Tables(0)
                ddlDWApprovalParty_NonIT_L3.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlDWApprovalParty_NonIT_L3.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlDWApprovalParty_NonIT_L3.DataBind()

                ddlDWApprovalParty_NonIT_L4.DataSource = MyData.Tables(0)
                ddlDWApprovalParty_NonIT_L4.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlDWApprovalParty_NonIT_L4.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlDWApprovalParty_NonIT_L4.DataBind()

                'NON IT (MAJOR)
                ddlDWApprovalParty_NonITMajor_L1.DataSource = MyData.Tables(0)
                ddlDWApprovalParty_NonITMajor_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlDWApprovalParty_NonITMajor_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlDWApprovalParty_NonITMajor_L1.DataBind()

                ddlDWApprovalParty2_NonITMajor_L1.DataSource = MyData.Tables(0)
                ddlDWApprovalParty2_NonITMajor_L1.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlDWApprovalParty2_NonITMajor_L1.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlDWApprovalParty2_NonITMajor_L1.DataBind()

                ddlDWApprovalParty_NonITMajor_L2.DataSource = MyData.Tables(0)
                ddlDWApprovalParty_NonITMajor_L2.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlDWApprovalParty_NonITMajor_L2.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlDWApprovalParty_NonITMajor_L2.DataBind()

                ddlDWApprovalParty_NonITMajor_L3.DataSource = MyData.Tables(0)
                ddlDWApprovalParty_NonITMajor_L3.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlDWApprovalParty_NonITMajor_L3.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlDWApprovalParty_NonITMajor_L3.DataBind()

                ddlDWApprovalParty_NonITMajor_L4.DataSource = MyData.Tables(0)
                ddlDWApprovalParty_NonITMajor_L4.DataValueField = MyData.Tables(0).Columns("USERID").ToString
                ddlDWApprovalParty_NonITMajor_L4.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlDWApprovalParty_NonITMajor_L4.DataBind()


                ddleReq_1st.DataSource = MyData.Tables(1)
                ddleReq_1st.DataValueField = MyData.Tables(1).Columns("USERID").ToString
                ddleReq_1st.DataTextField = MyData.Tables(1).Columns("nvchFullname").ToString
                ddleReq_1st.DataBind()

                ddleReq_2nd.DataSource = MyData.Tables(1)
                ddleReq_2nd.DataValueField = MyData.Tables(1).Columns("USERID").ToString
                ddleReq_2nd.DataTextField = MyData.Tables(1).Columns("nvchFullname").ToString
                ddleReq_2nd.DataBind()

                ddleReq_3rd.DataSource = MyData.Tables(1)
                ddleReq_3rd.DataValueField = MyData.Tables(1).Columns("USERID").ToString
                ddleReq_3rd.DataTextField = MyData.Tables(1).Columns("nvchFullname").ToString
                ddleReq_3rd.DataBind()

                ddlIssuePO_1st.DataSource = MyData.Tables(1)
                ddlIssuePO_1st.DataValueField = MyData.Tables(1).Columns("USERID").ToString
                ddlIssuePO_1st.DataTextField = MyData.Tables(1).Columns("nvchFullname").ToString
                ddlIssuePO_1st.DataBind()


                ddlIssuePO_2nd.DataSource = MyData.Tables(1)
                ddlIssuePO_2nd.DataValueField = MyData.Tables(1).Columns("USERID").ToString
                ddlIssuePO_2nd.DataTextField = MyData.Tables(1).Columns("nvchFullname").ToString
                ddlIssuePO_2nd.DataBind()

                ddlIssuePO_3rd.DataSource = MyData.Tables(1)
                ddlIssuePO_3rd.DataValueField = MyData.Tables(1).Columns("USERID").ToString
                ddlIssuePO_3rd.DataTextField = MyData.Tables(1).Columns("nvchFullname").ToString
                ddlIssuePO_3rd.DataBind()

                ddlIssuePayment_1st.DataSource = MyData.Tables(1)
                ddlIssuePayment_1st.DataValueField = MyData.Tables(1).Columns("USERID").ToString
                ddlIssuePayment_1st.DataTextField = MyData.Tables(1).Columns("nvchFullname").ToString
                ddlIssuePayment_1st.DataBind()

                ddlIssuePayment_2nd.DataSource = MyData.Tables(1)
                ddlIssuePayment_2nd.DataValueField = MyData.Tables(1).Columns("USERID").ToString
                ddlIssuePayment_2nd.DataTextField = MyData.Tables(1).Columns("nvchFullname").ToString
                ddlIssuePayment_2nd.DataBind()

                ddlIssuePayment_3rd.DataSource = MyData.Tables(1)
                ddlIssuePayment_3rd.DataValueField = MyData.Tables(1).Columns("USERID").ToString
                ddlIssuePayment_3rd.DataTextField = MyData.Tables(1).Columns("nvchFullname").ToString
                ddlIssuePayment_3rd.DataBind()

                ddleFMS_1st.DataSource = MyData.Tables(1)
                ddleFMS_1st.DataValueField = MyData.Tables(1).Columns("USERID").ToString
                ddleFMS_1st.DataTextField = MyData.Tables(1).Columns("nvchFullname").ToString
                ddleFMS_1st.DataBind()

                ddleFMS_2nd.DataSource = MyData.Tables(1)
                ddleFMS_2nd.DataValueField = MyData.Tables(1).Columns("USERID").ToString
                ddleFMS_2nd.DataTextField = MyData.Tables(1).Columns("nvchFullname").ToString
                ddleFMS_2nd.DataBind()

                ddleFMS_3rd.DataSource = MyData.Tables(1)
                ddleFMS_3rd.DataValueField = MyData.Tables(1).Columns("USERID").ToString
                ddleFMS_3rd.DataTextField = MyData.Tables(1).Columns("nvchFullname").ToString
                ddleFMS_3rd.DataBind()

                'StationCurrency()

                'Dim strStationCurrency As String
                'strStationCurrency = DIMERCO.SDK.Utilities.ReSM.GetStationCurrencyByID(ddlStation.SelectedValue.ToString).ToString()

                'If ddlCurrencyCode.Items.Count > 0 Then
                '    ddlCurrencyCode.SelectedValue = DIMERCO.SDK.Utilities.ReSM.GetCurrencyID(strStationCurrency)
                'End If

            Catch ex As Exception
                Me.lblMsg.Text = "ddlStation_SelectedIndexChanged: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub StationCurrency()
        clsCF.StationCurrencyID(ddlStation.SelectedValue.ToString, ddlCurrencyCode)
    End Sub

    Private Sub GetStationSetting()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetStationSetting")

                With MyCommand
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "PurchaseBudgeted_IT"

                If MyData.Tables(0).Rows.Count > 0 Then
                    ddlComparision_IT_L1.SelectedValue = MyData.Tables(0).Rows(0)("L1_Comparison").ToString
                    ddlCurrencyCode_IT_L1.SelectedValue = MyData.Tables(0).Rows(0)("L1_CurrenyCode").ToString
                    txtAmount_IT_L1.Text = MyData.Tables(0).Rows(0)("L1_Amount").ToString
                    If Not ddlApprovalParty_IT_L1.Items.FindByValue(MyData.Tables(0).Rows(0)("L1_ApprovalParty").ToString) Is Nothing Then
                        ddlApprovalParty_IT_L1.SelectedValue = MyData.Tables(0).Rows(0)("L1_ApprovalParty").ToString
                    Else
                        lblMsg_ApprovalParty_IT_L1.Visible = True
                        lblMsg_ApprovalParty_IT_L1.Text = "Please reset Approval Party 1"
                    End If

                    If Not ddlApprovalParty2_IT_L1.Items.FindByValue(MyData.Tables(0).Rows(0)("L1_ApprovalParty2").ToString) Is Nothing Then
                        ddlApprovalParty2_IT_L1.SelectedValue = MyData.Tables(0).Rows(0)("L1_ApprovalParty2").ToString
                    Else
                        lblMsg_ApprovalParty_IT_L1.Visible = True
                        lblMsg_ApprovalParty_IT_L1.Text = "Please reset Approval Party 2"
                    End If


                    ddlComparision_IT_L2.SelectedValue = MyData.Tables(0).Rows(0)("L2_Comparison").ToString
                    ddlCurrencyCode_IT_L2.SelectedValue = MyData.Tables(0).Rows(0)("L2_CurrenyCode").ToString
                    txtAmount_IT_L2.Text = MyData.Tables(0).Rows(0)("L2_Amount").ToString
                    If Not ddlApprovalParty_IT_L2.Items.FindByValue(MyData.Tables(0).Rows(0)("L2_ApprovalParty").ToString) Is Nothing Then
                        ddlApprovalParty_IT_L2.SelectedValue = MyData.Tables(0).Rows(0)("L2_ApprovalParty").ToString
                    Else
                        lblMsg_ApprovalParty_IT_L2.Visible = True
                        lblMsg_ApprovalParty_IT_L2.Text = "Please reset Approval Party"
                    End If


                    ddlComparision_IT_L3.SelectedValue = MyData.Tables(0).Rows(0)("L3_Comparison").ToString
                    ddlCurrencyCode_IT_L3.SelectedValue = MyData.Tables(0).Rows(0)("L3_CurrenyCode").ToString
                    txtAmount_IT_L3.Text = MyData.Tables(0).Rows(0)("L3_Amount").ToString
                    If Not ddlApprovalParty_IT_L3.Items.FindByValue(MyData.Tables(0).Rows(0)("L3_ApprovalParty").ToString) Is Nothing Then
                        ddlApprovalParty_IT_L3.SelectedValue = MyData.Tables(0).Rows(0)("L3_ApprovalParty").ToString
                    Else
                        lblMsg_ApprovalParty_IT_L3.Visible = True
                        lblMsg_ApprovalParty_IT_L3.Text = "Please reset Approval Party"
                    End If
                    
                    ddlComparision_IT_L4.SelectedValue = MyData.Tables(0).Rows(0)("L4_Comparison").ToString
                    ddlCurrencyCode_IT_L4.SelectedValue = MyData.Tables(0).Rows(0)("L4_CurrenyCode").ToString
                    txtAmount_IT_L4.Text = MyData.Tables(0).Rows(0)("L4_Amount").ToString
                    If Not ddlApprovalParty_IT_L4.Items.FindByValue(MyData.Tables(0).Rows(0)("L4_ApprovalParty").ToString) Is Nothing Then
                        ddlApprovalParty_IT_L4.SelectedValue = MyData.Tables(0).Rows(0)("L4_ApprovalParty").ToString
                    Else
                        lblMsg_ApprovalParty_IT_L4.Visible = True
                        lblMsg_ApprovalParty_IT_L4.Text = "Please reset Approval Party"
                    End If

                End If

                If MyData.Tables(1).Rows.Count > 0 Then

                    ddlComparision_NonIT_L1.SelectedValue = MyData.Tables(1).Rows(0)("L1_Comparison").ToString
                    ddlCurrencyCode_NonIT_L1.SelectedValue = MyData.Tables(1).Rows(0)("L1_CurrenyCode").ToString
                    txtAmount_NonIT_L1.Text = MyData.Tables(1).Rows(0)("L1_Amount").ToString
                    If Not ddlApprovalParty_NonIT_L1.Items.FindByValue(MyData.Tables(1).Rows(0)("L1_ApprovalParty").ToString) Is Nothing Then
                        ddlApprovalParty_NonIT_L1.SelectedValue = MyData.Tables(1).Rows(0)("L1_ApprovalParty").ToString
                    Else
                        lblMsg_ApprovalParty_NonIT_L1.Visible = True
                        lblMsg_ApprovalParty_NonIT_L1.Text = "Please reset Approval Party 1"
                    End If

                    If Not ddlApprovalParty2_NonIT_L1.Items.FindByValue(MyData.Tables(1).Rows(0)("L1_ApprovalParty2").ToString) Is Nothing Then
                        ddlApprovalParty2_NonIT_L1.SelectedValue = MyData.Tables(1).Rows(0)("L1_ApprovalParty2").ToString
                    Else
                        lblMsg_ApprovalParty_NonIT_L1.Visible = True
                        lblMsg_ApprovalParty_NonIT_L1.Text = "Please reset Approval Party 2"
                    End If


                    ddlComparision_NonIT_L2.SelectedValue = MyData.Tables(1).Rows(0)("L2_Comparison").ToString
                    ddlCurrencyCode_NonIT_L2.SelectedValue = MyData.Tables(1).Rows(0)("L2_CurrenyCode").ToString
                    txtAmount_NonIT_L2.Text = MyData.Tables(1).Rows(0)("L2_Amount").ToString
                    If Not ddlApprovalParty_NonIT_L2.Items.FindByValue(MyData.Tables(1).Rows(0)("L2_ApprovalParty").ToString) Is Nothing Then
                        ddlApprovalParty_NonIT_L2.SelectedValue = MyData.Tables(1).Rows(0)("L2_ApprovalParty").ToString
                    Else
                        lblMsg_ApprovalParty_NonIT_L2.Visible = True
                        lblMsg_ApprovalParty_NonIT_L2.Text = "Please reset Approval Party"
                    End If


                    ddlComparision_NonIT_L3.SelectedValue = MyData.Tables(1).Rows(0)("L3_Comparison").ToString
                    ddlCurrencyCode_NonIT_L3.SelectedValue = MyData.Tables(1).Rows(0)("L3_CurrenyCode").ToString
                    txtAmount_NonIT_L3.Text = MyData.Tables(1).Rows(0)("L3_Amount").ToString
                    If Not ddlApprovalParty_NonIT_L3.Items.FindByValue(MyData.Tables(1).Rows(0)("L3_ApprovalParty").ToString) Is Nothing Then
                        ddlApprovalParty_NonIT_L3.SelectedValue = MyData.Tables(1).Rows(0)("L3_ApprovalParty").ToString
                    Else
                        lblMsg_ApprovalParty_NonIT_L3.Visible = True
                        lblMsg_ApprovalParty_NonIT_L3.Text = "Please reset Approval Party"
                    End If


                    ddlComparision_NonIT_L4.SelectedValue = MyData.Tables(1).Rows(0)("L4_Comparison").ToString
                    ddlCurrencyCode_NonIT_L4.SelectedValue = MyData.Tables(1).Rows(0)("L4_CurrenyCode").ToString
                    txtAmount_NonIT_L4.Text = MyData.Tables(1).Rows(0)("L4_Amount").ToString
                    If Not ddlApprovalParty_NonIT_L4.Items.FindByValue(MyData.Tables(1).Rows(0)("L4_ApprovalParty").ToString) Is Nothing Then
                        ddlApprovalParty_NonIT_L4.SelectedValue = MyData.Tables(1).Rows(0)("L4_ApprovalParty").ToString
                    Else
                        lblMsg_ApprovalParty_NonIT_L4.Visible = True
                        lblMsg_ApprovalParty_NonIT_L4.Text = "Please reset Approval Party"
                    End If

                End If

                If MyData.Tables(2).Rows.Count > 0 Then

                    ddlComparision_NonITMajor_L1.SelectedValue = MyData.Tables(2).Rows(0)("L1_Comparison").ToString
                    ddlCurrencyCode_NonITMajor_L1.SelectedValue = MyData.Tables(2).Rows(0)("L1_CurrenyCode").ToString
                    txtAmount_NonITMajor_L1.Text = MyData.Tables(2).Rows(0)("L1_Amount").ToString
                    If Not ddlApprovalParty_NonITMajor_L1.Items.FindByValue(MyData.Tables(2).Rows(0)("L1_ApprovalParty").ToString) Is Nothing Then
                        ddlApprovalParty_NonITMajor_L1.SelectedValue = MyData.Tables(2).Rows(0)("L1_ApprovalParty").ToString
                    Else
                        lblMsg_ApprovalParty_NonITMajor_L1.Visible = True
                        lblMsg_ApprovalParty_NonITMajor_L1.Text = "Please reset Approval Party 1"
                    End If

                    If Not ddlApprovalParty2_NonITMajor_L1.Items.FindByValue(MyData.Tables(2).Rows(0)("L1_ApprovalParty2").ToString) Is Nothing Then
                        ddlApprovalParty2_NonITMajor_L1.SelectedValue = MyData.Tables(2).Rows(0)("L1_ApprovalParty2").ToString
                    Else
                        lblMsg_ApprovalParty_NonITMajor_L1.Visible = True
                        lblMsg_ApprovalParty_NonITMajor_L1.Text = "Please reset Approval Party 2"
                    End If


                    ddlComparision_NonITMajor_L2.SelectedValue = MyData.Tables(2).Rows(0)("L2_Comparison").ToString
                    ddlCurrencyCode_NonITMajor_L2.SelectedValue = MyData.Tables(2).Rows(0)("L2_CurrenyCode").ToString
                    txtAmount_NonITMajor_L2.Text = MyData.Tables(2).Rows(0)("L2_Amount").ToString
                    If Not ddlApprovalParty_NonITMajor_L2.Items.FindByValue(MyData.Tables(2).Rows(0)("L2_ApprovalParty").ToString) Is Nothing Then
                        ddlApprovalParty_NonITMajor_L2.SelectedValue = MyData.Tables(2).Rows(0)("L2_ApprovalParty").ToString
                    Else
                        lblMsg_ApprovalParty_NonITMajor_L2.Visible = True
                        lblMsg_ApprovalParty_NonITMajor_L2.Text = "Please reset Approval Party"
                    End If


                    ddlComparision_NonITMajor_L3.SelectedValue = MyData.Tables(2).Rows(0)("L3_Comparison").ToString
                    ddlCurrencyCode_NonITMajor_L3.SelectedValue = MyData.Tables(2).Rows(0)("L3_CurrenyCode").ToString
                    txtAmount_NonITMajor_L3.Text = MyData.Tables(2).Rows(0)("L3_Amount").ToString
                    If Not ddlApprovalParty_NonITMajor_L3.Items.FindByValue(MyData.Tables(2).Rows(0)("L3_ApprovalParty").ToString) Is Nothing Then
                        ddlApprovalParty_NonITMajor_L3.SelectedValue = MyData.Tables(2).Rows(0)("L3_ApprovalParty").ToString
                    Else
                        lblMsg_ApprovalParty_NonITMajor_L3.Visible = True
                        lblMsg_ApprovalParty_NonITMajor_L3.Text = "Please reset Approval Party"
                    End If


                    ddlComparision_NonITMajor_L4.SelectedValue = MyData.Tables(2).Rows(0)("L4_Comparison").ToString
                    ddlCurrencyCode_NonITMajor_L4.SelectedValue = MyData.Tables(2).Rows(0)("L4_CurrenyCode").ToString
                    txtAmount_NonITMajor_L4.Text = MyData.Tables(2).Rows(0)("L4_Amount").ToString
                    If Not ddlApprovalParty_NonITMajor_L4.Items.FindByValue(MyData.Tables(2).Rows(0)("L4_ApprovalParty").ToString) Is Nothing Then
                        ddlApprovalParty_NonITMajor_L4.SelectedValue = MyData.Tables(2).Rows(0)("L4_ApprovalParty").ToString
                    Else
                        lblMsg_ApprovalParty_NonITMajor_L4.Visible = True
                        lblMsg_ApprovalParty_NonITMajor_L4.Text = "Please reset Approval Party"
                    End If

                End If

                If MyData.Tables(3).Rows.Count > 0 Then
                    ddlUBComparision_IT_L1.SelectedValue = MyData.Tables(3).Rows(0)("L1_Comparison").ToString
                    ddlUBCurrencyCode_IT_L1.SelectedValue = MyData.Tables(3).Rows(0)("L1_CurrenyCode").ToString
                    txtUBAmount_IT_L1.Text = MyData.Tables(3).Rows(0)("L1_Amount").ToString
                    If Not ddlUBApprovalParty_IT_L1.Items.FindByValue(MyData.Tables(3).Rows(0)("L1_ApprovalParty").ToString) Is Nothing Then
                        ddlUBApprovalParty_IT_L1.SelectedValue = MyData.Tables(3).Rows(0)("L1_ApprovalParty").ToString
                    Else
                        lblMsg_UBApprovalParty_IT_L1.Visible = True
                        lblMsg_UBApprovalParty_IT_L1.Text = "Please reset Approval Party 1"
                    End If

                    If Not ddlUBApprovalParty2_IT_L1.Items.FindByValue(MyData.Tables(3).Rows(0)("L1_ApprovalParty2").ToString) Is Nothing Then
                        ddlUBApprovalParty2_IT_L1.SelectedValue = MyData.Tables(3).Rows(0)("L1_ApprovalParty2").ToString
                    Else
                        lblMsg_UBApprovalParty_IT_L1.Visible = True
                        lblMsg_UBApprovalParty_IT_L1.Text = "Please reset Approval Party 2"
                    End If
                    
                    ddlUBComparision_IT_L2.SelectedValue = MyData.Tables(3).Rows(0)("L2_Comparison").ToString
                    ddlUBCurrencyCode_IT_L2.SelectedValue = MyData.Tables(3).Rows(0)("L2_CurrenyCode").ToString
                    txtUBAmount_IT_L2.Text = MyData.Tables(3).Rows(0)("L2_Amount").ToString

                    If Not ddlUBApprovalParty_IT_L2.Items.FindByValue(MyData.Tables(3).Rows(0)("L2_ApprovalParty").ToString) Is Nothing Then
                        ddlUBApprovalParty_IT_L2.SelectedValue = MyData.Tables(3).Rows(0)("L2_ApprovalParty").ToString
                    Else
                        lblMsg_UBApprovalParty_IT_L2.Visible = True
                        lblMsg_UBApprovalParty_IT_L2.Text = "Please reset Approval Party"
                    End If


                    ddlUBComparision_IT_L3.SelectedValue = MyData.Tables(3).Rows(0)("L3_Comparison").ToString
                    ddlUBCurrencyCode_IT_L3.SelectedValue = MyData.Tables(3).Rows(0)("L3_CurrenyCode").ToString
                    txtUBAmount_IT_L3.Text = MyData.Tables(3).Rows(0)("L3_Amount").ToString

                    If Not ddlUBApprovalParty_IT_L3.Items.FindByValue(MyData.Tables(3).Rows(0)("L3_ApprovalParty").ToString) Is Nothing Then
                        ddlUBApprovalParty_IT_L3.SelectedValue = MyData.Tables(3).Rows(0)("L3_ApprovalParty").ToString
                    Else
                        lblMsg_UBApprovalParty_IT_L3.Visible = True
                        lblMsg_UBApprovalParty_IT_L3.Text = "Please reset Approval Party"
                    End If


                    ddlUBComparision_IT_L4.SelectedValue = MyData.Tables(3).Rows(0)("L4_Comparison").ToString
                    ddlUBCurrencyCode_IT_L4.SelectedValue = MyData.Tables(3).Rows(0)("L4_CurrenyCode").ToString
                    txtUBAmount_IT_L4.Text = MyData.Tables(3).Rows(0)("L4_Amount").ToString

                    If Not ddlUBApprovalParty_IT_L4.Items.FindByValue(MyData.Tables(3).Rows(0)("L4_ApprovalParty").ToString) Is Nothing Then
                        ddlUBApprovalParty_IT_L4.SelectedValue = MyData.Tables(3).Rows(0)("L4_ApprovalParty").ToString
                    Else
                        lblMsg_UBApprovalParty_IT_L4.Visible = True
                        lblMsg_UBApprovalParty_IT_L4.Text = "Please reset Approval Party"
                    End If

                End If

                If MyData.Tables(4).Rows.Count > 0 Then
                    ddlUBComparision_NonIT_L1.SelectedValue = MyData.Tables(4).Rows(0)("L1_Comparison").ToString
                    ddlUBCurrencyCode_NonIT_L1.SelectedValue = MyData.Tables(4).Rows(0)("L1_CurrenyCode").ToString
                    txtUBAmount_NonIT_L1.Text = MyData.Tables(4).Rows(0)("L1_Amount").ToString

                    If Not ddlUBApprovalParty_NonIT_L1.Items.FindByValue(MyData.Tables(4).Rows(0)("L1_ApprovalParty").ToString) Is Nothing Then
                        ddlUBApprovalParty_NonIT_L1.SelectedValue = MyData.Tables(4).Rows(0)("L1_ApprovalParty").ToString
                    Else
                        lblMsg_UBApprovalParty_NonIT_L1.Visible = True
                        lblMsg_UBApprovalParty_NonIT_L1.Text = "Please reset Approval Party 1"
                    End If

                    If Not ddlUBApprovalParty2_NonIT_L1.Items.FindByValue(MyData.Tables(4).Rows(0)("L1_ApprovalParty2").ToString) Is Nothing Then
                        ddlUBApprovalParty2_NonIT_L1.SelectedValue = MyData.Tables(4).Rows(0)("L1_ApprovalParty2").ToString
                    Else
                        lblMsg_UBApprovalParty_NonIT_L1.Visible = True
                        lblMsg_UBApprovalParty_NonIT_L1.Text = "Please reset Approval Party 2"
                    End If


                    ddlUBComparision_NonIT_L2.SelectedValue = MyData.Tables(4).Rows(0)("L2_Comparison").ToString
                    ddlUBCurrencyCode_NonIT_L2.SelectedValue = MyData.Tables(4).Rows(0)("L2_CurrenyCode").ToString
                    txtUBAmount_NonIT_L2.Text = MyData.Tables(4).Rows(0)("L2_Amount").ToString

                    If Not ddlUBApprovalParty_NonIT_L2.Items.FindByValue(MyData.Tables(4).Rows(0)("L2_ApprovalParty").ToString) Is Nothing Then
                        ddlUBApprovalParty_NonIT_L2.SelectedValue = MyData.Tables(4).Rows(0)("L2_ApprovalParty").ToString
                    Else
                        lblMsg_UBApprovalParty_NonIT_L2.Visible = True
                        lblMsg_UBApprovalParty_NonIT_L2.Text = "Please reset Approval Party"
                    End If


                    ddlUBComparision_NonIT_L3.SelectedValue = MyData.Tables(4).Rows(0)("L3_Comparison").ToString
                    ddlUBCurrencyCode_NonIT_L3.SelectedValue = MyData.Tables(4).Rows(0)("L3_CurrenyCode").ToString
                    txtUBAmount_NonIT_L3.Text = MyData.Tables(4).Rows(0)("L3_Amount").ToString

                    If Not ddlUBApprovalParty_NonIT_L3.Items.FindByValue(MyData.Tables(4).Rows(0)("L3_ApprovalParty").ToString) Is Nothing Then
                        ddlUBApprovalParty_NonIT_L3.SelectedValue = MyData.Tables(4).Rows(0)("L3_ApprovalParty").ToString
                    Else
                        lblMsg_UBApprovalParty_NonIT_L3.Visible = True
                        lblMsg_UBApprovalParty_NonIT_L3.Text = "Please reset Approval Party"
                    End If


                    ddlUBComparision_NonIT_L4.SelectedValue = MyData.Tables(4).Rows(0)("L4_Comparison").ToString
                    ddlUBCurrencyCode_NonIT_L4.SelectedValue = MyData.Tables(4).Rows(0)("L4_CurrenyCode").ToString
                    txtUBAmount_NonIT_L4.Text = MyData.Tables(4).Rows(0)("L4_Amount").ToString
                    If Not ddlUBApprovalParty_NonIT_L4.Items.FindByValue(MyData.Tables(4).Rows(0)("L4_ApprovalParty").ToString) Is Nothing Then
                        ddlUBApprovalParty_NonIT_L4.SelectedValue = MyData.Tables(4).Rows(0)("L4_ApprovalParty").ToString
                    Else
                        lblMsg_UBApprovalParty_NonIT_L4.Visible = True
                        lblMsg_UBApprovalParty_NonIT_L4.Text = "Please reset Approval Party"
                    End If

                End If

                If MyData.Tables(5).Rows.Count > 0 Then
                    ddlUBComparision_NonITMajor_L1.SelectedValue = MyData.Tables(5).Rows(0)("L1_Comparison").ToString
                    ddlUBCurrencyCode_NonITMajor_L1.SelectedValue = MyData.Tables(5).Rows(0)("L1_CurrenyCode").ToString
                    txtUBAmount_NonITMajor_L1.Text = MyData.Tables(5).Rows(0)("L1_Amount").ToString

                    If Not ddlUBApprovalParty_NonITMajor_L1.Items.FindByValue(MyData.Tables(5).Rows(0)("L1_ApprovalParty").ToString) Is Nothing Then
                        ddlUBApprovalParty_NonITMajor_L1.SelectedValue = MyData.Tables(5).Rows(0)("L1_ApprovalParty").ToString
                    Else
                        lblMsg_UBApprovalParty_NonITMajor_L1.Visible = True
                        lblMsg_UBApprovalParty_NonITMajor_L1.Text = "Please reset Approval Party 1"
                    End If

                    If Not ddlUBApprovalParty2_NonITMajor_L1.Items.FindByValue(MyData.Tables(5).Rows(0)("L1_ApprovalParty2").ToString) Is Nothing Then
                        ddlUBApprovalParty2_NonITMajor_L1.SelectedValue = MyData.Tables(5).Rows(0)("L1_ApprovalParty2").ToString
                    Else
                        lblMsg_UBApprovalParty_NonITMajor_L1.Visible = True
                        lblMsg_UBApprovalParty_NonITMajor_L1.Text = "Please reset Approval Party 2"
                    End If


                    ddlUBComparision_NonITMajor_L2.SelectedValue = MyData.Tables(5).Rows(0)("L2_Comparison").ToString
                    ddlUBCurrencyCode_NonITMajor_L2.SelectedValue = MyData.Tables(5).Rows(0)("L2_CurrenyCode").ToString
                    txtUBAmount_NonITMajor_L2.Text = MyData.Tables(5).Rows(0)("L2_Amount").ToString

                    If Not ddlUBApprovalParty_NonITMajor_L2.Items.FindByValue(MyData.Tables(5).Rows(0)("L2_ApprovalParty").ToString) Is Nothing Then
                        ddlUBApprovalParty_NonITMajor_L2.SelectedValue = MyData.Tables(5).Rows(0)("L2_ApprovalParty").ToString
                    Else
                        lblMsg_UBApprovalParty_NonITMajor_L2.Visible = True
                        lblMsg_UBApprovalParty_NonITMajor_L2.Text = "Please reset Approval Party"
                    End If


                    ddlUBComparision_NonITMajor_L3.SelectedValue = MyData.Tables(5).Rows(0)("L3_Comparison").ToString
                    ddlUBCurrencyCode_NonITMajor_L3.SelectedValue = MyData.Tables(5).Rows(0)("L3_CurrenyCode").ToString
                    txtUBAmount_NonITMajor_L3.Text = MyData.Tables(5).Rows(0)("L3_Amount").ToString
                    If Not ddlUBApprovalParty_NonITMajor_L3.Items.FindByValue(MyData.Tables(5).Rows(0)("L3_ApprovalParty").ToString) Is Nothing Then
                        ddlUBApprovalParty_NonITMajor_L3.SelectedValue = MyData.Tables(5).Rows(0)("L3_ApprovalParty").ToString
                    Else
                        lblMsg_UBApprovalParty_NonITMajor_L3.Visible = True
                        lblMsg_UBApprovalParty_NonITMajor_L3.Text = "Please reset Approval Party"
                    End If


                    ddlUBComparision_NonITMajor_L4.SelectedValue = MyData.Tables(5).Rows(0)("L4_Comparison").ToString
                    ddlUBCurrencyCode_NonITMajor_L4.SelectedValue = MyData.Tables(5).Rows(0)("L4_CurrenyCode").ToString
                    txtUBAmount_NonITMajor_L4.Text = MyData.Tables(5).Rows(0)("L4_Amount").ToString
                    If Not ddlUBApprovalParty_NonITMajor_L4.Items.FindByValue(MyData.Tables(5).Rows(0)("L4_ApprovalParty").ToString) Is Nothing Then
                        ddlUBApprovalParty_NonITMajor_L4.SelectedValue = MyData.Tables(5).Rows(0)("L4_ApprovalParty").ToString
                    Else
                        lblMsg_UBApprovalParty_NonITMajor_L4.Visible = True
                        lblMsg_UBApprovalParty_NonITMajor_L4.Text = "Please reset Approval Party"
                    End If

                End If

                If MyData.Tables(6).Rows.Count > 0 Then

                    ddlOBComparision_IT_L1.SelectedValue = MyData.Tables(6).Rows(0)("L1_Comparison").ToString
                    ddlOBCurrencyCode_IT_L1.SelectedValue = MyData.Tables(6).Rows(0)("L1_CurrenyCode").ToString
                    txtOBAmount_IT_L1.Text = MyData.Tables(6).Rows(0)("L1_Amount").ToString

                    If Not ddlOBApprovalParty_IT_L1.Items.FindByValue(MyData.Tables(6).Rows(0)("L1_ApprovalParty").ToString) Is Nothing Then
                        ddlOBApprovalParty_IT_L1.SelectedValue = MyData.Tables(6).Rows(0)("L1_ApprovalParty").ToString
                    Else
                        lblMsg_OBApprovalParty_IT_L1.Visible = True
                        lblMsg_OBApprovalParty_IT_L1.Text = "Please reset Approval Party 1"
                    End If

                    If Not ddlOBApprovalParty2_IT_L1.Items.FindByValue(MyData.Tables(6).Rows(0)("L1_ApprovalParty2").ToString) Is Nothing Then
                        ddlOBApprovalParty2_IT_L1.SelectedValue = MyData.Tables(6).Rows(0)("L1_ApprovalParty2").ToString
                    Else
                        lblMsg_OBApprovalParty_IT_L1.Visible = True
                        lblMsg_OBApprovalParty_IT_L1.Text = "Please reset Approval Party 2"
                    End If


                    ddlOBComparision_IT_L2.SelectedValue = MyData.Tables(6).Rows(0)("L2_Comparison").ToString
                    ddlOBCurrencyCode_IT_L2.SelectedValue = MyData.Tables(6).Rows(0)("L2_CurrenyCode").ToString
                    txtOBAmount_IT_L2.Text = MyData.Tables(6).Rows(0)("L2_Amount").ToString
                    If Not ddlOBApprovalParty_IT_L2.Items.FindByValue(MyData.Tables(6).Rows(0)("L2_ApprovalParty").ToString) Is Nothing Then
                        ddlOBApprovalParty_IT_L2.SelectedValue = MyData.Tables(6).Rows(0)("L2_ApprovalParty").ToString
                    Else
                        lblMsg_OBApprovalParty_IT_L2.Visible = True
                        lblMsg_OBApprovalParty_IT_L2.Text = "Please reset Approval Party"
                    End If


                    ddlOBComparision_IT_L3.SelectedValue = MyData.Tables(6).Rows(0)("L3_Comparison").ToString
                    ddlOBCurrencyCode_IT_L3.SelectedValue = MyData.Tables(6).Rows(0)("L3_CurrenyCode").ToString
                    txtOBAmount_IT_L3.Text = MyData.Tables(6).Rows(0)("L3_Amount").ToString
                    If Not ddlOBApprovalParty_IT_L3.Items.FindByValue(MyData.Tables(6).Rows(0)("L3_ApprovalParty").ToString) Is Nothing Then
                        ddlOBApprovalParty_IT_L3.SelectedValue = MyData.Tables(6).Rows(0)("L3_ApprovalParty").ToString
                    Else
                        lblMsg_OBApprovalParty_IT_L3.Visible = True
                        lblMsg_OBApprovalParty_IT_L3.Text = "Please reset Approval Party"
                    End If


                    ddlOBComparision_IT_L4.SelectedValue = MyData.Tables(6).Rows(0)("L4_Comparison").ToString
                    ddlOBCurrencyCode_IT_L4.SelectedValue = MyData.Tables(6).Rows(0)("L4_CurrenyCode").ToString
                    txtOBAmount_IT_L4.Text = MyData.Tables(6).Rows(0)("L4_Amount").ToString
                    If Not ddlOBApprovalParty_IT_L4.Items.FindByValue(MyData.Tables(6).Rows(0)("L4_ApprovalParty").ToString) Is Nothing Then
                        ddlOBApprovalParty_IT_L4.SelectedValue = MyData.Tables(6).Rows(0)("L4_ApprovalParty").ToString
                    Else
                        lblMsg_OBApprovalParty_IT_L4.Visible = True
                        lblMsg_OBApprovalParty_IT_L4.Text = "Please reset Approval Party"
                    End If

                End If

                If MyData.Tables(7).Rows.Count > 0 Then
                    ddlOBComparision_NonIT_L1.SelectedValue = MyData.Tables(7).Rows(0)("L1_Comparison").ToString
                    ddlOBCurrencyCode_NonIT_L1.SelectedValue = MyData.Tables(7).Rows(0)("L1_CurrenyCode").ToString
                    txtOBAmount_NonIT_L1.Text = MyData.Tables(7).Rows(0)("L1_Amount").ToString

                    If Not ddlOBApprovalParty_NonIT_L1.Items.FindByValue(MyData.Tables(7).Rows(0)("L1_ApprovalParty").ToString) Is Nothing Then
                        ddlOBApprovalParty_NonIT_L1.SelectedValue = MyData.Tables(7).Rows(0)("L1_ApprovalParty").ToString
                    Else
                        lblMsg_OBApprovalParty_NonIT_L1.Visible = True
                        lblMsg_OBApprovalParty_NonIT_L1.Text = "Please reset Approval Party 1"
                    End If

                    If Not ddlOBApprovalParty2_NonIT_L1.Items.FindByValue(MyData.Tables(7).Rows(0)("L1_ApprovalParty2").ToString) Is Nothing Then
                        ddlOBApprovalParty2_NonIT_L1.SelectedValue = MyData.Tables(7).Rows(0)("L1_ApprovalParty2").ToString
                    Else
                        lblMsg_OBApprovalParty_NonIT_L1.Visible = True
                        lblMsg_OBApprovalParty_NonIT_L1.Text = "Please reset Approval Party 2"
                    End If


                    ddlOBComparision_NonIT_L2.SelectedValue = MyData.Tables(7).Rows(0)("L2_Comparison").ToString
                    ddlOBCurrencyCode_NonIT_L2.SelectedValue = MyData.Tables(7).Rows(0)("L2_CurrenyCode").ToString
                    txtOBAmount_NonIT_L2.Text = MyData.Tables(7).Rows(0)("L2_Amount").ToString

                    If Not ddlOBApprovalParty_NonIT_L2.Items.FindByValue(MyData.Tables(7).Rows(0)("L2_ApprovalParty").ToString) Is Nothing Then
                        ddlOBApprovalParty_NonIT_L2.SelectedValue = MyData.Tables(7).Rows(0)("L2_ApprovalParty").ToString
                    Else
                        lblMsg_OBApprovalParty_NonIT_L2.Visible = True
                        lblMsg_OBApprovalParty_NonIT_L2.Text = "Please reset Approval Party"
                    End If


                    ddlOBComparision_NonIT_L3.SelectedValue = MyData.Tables(7).Rows(0)("L3_Comparison").ToString
                    ddlOBCurrencyCode_NonIT_L3.SelectedValue = MyData.Tables(7).Rows(0)("L3_CurrenyCode").ToString
                    txtOBAmount_NonIT_L3.Text = MyData.Tables(7).Rows(0)("L3_Amount").ToString
                    If Not ddlOBApprovalParty_NonIT_L3.Items.FindByValue(MyData.Tables(7).Rows(0)("L3_ApprovalParty").ToString) Is Nothing Then
                        ddlOBApprovalParty_NonIT_L3.SelectedValue = MyData.Tables(7).Rows(0)("L3_ApprovalParty").ToString
                    Else
                        lblMsg_OBApprovalParty_NonIT_L3.Visible = True
                        lblMsg_OBApprovalParty_NonIT_L3.Text = "Please reset Approval Party"
                    End If


                    ddlOBComparision_NonIT_L4.SelectedValue = MyData.Tables(7).Rows(0)("L4_Comparison").ToString
                    ddlOBCurrencyCode_NonIT_L4.SelectedValue = MyData.Tables(7).Rows(0)("L4_CurrenyCode").ToString
                    txtOBAmount_NonIT_L4.Text = MyData.Tables(7).Rows(0)("L4_Amount").ToString
                    If Not ddlOBApprovalParty_NonIT_L4.Items.FindByValue(MyData.Tables(7).Rows(0)("L4_ApprovalParty").ToString) Is Nothing Then
                        ddlOBApprovalParty_NonIT_L4.SelectedValue = MyData.Tables(7).Rows(0)("L4_ApprovalParty").ToString
                    Else
                        lblMsg_OBApprovalParty_NonIT_L4.Visible = True
                        lblMsg_OBApprovalParty_NonIT_L4.Text = "Please reset Approval Party"
                    End If

                End If

                If MyData.Tables(8).Rows.Count > 0 Then
                    ddlOBComparision_NonITMajor_L1.SelectedValue = MyData.Tables(8).Rows(0)("L1_Comparison").ToString
                    ddlOBCurrencyCode_NonITMajor_L1.SelectedValue = MyData.Tables(8).Rows(0)("L1_CurrenyCode").ToString
                    txtOBAmount_NonITMajor_L1.Text = MyData.Tables(8).Rows(0)("L1_Amount").ToString

                    If Not ddlOBApprovalParty_NonITMajor_L1.Items.FindByValue(MyData.Tables(8).Rows(0)("L1_ApprovalParty").ToString) Is Nothing Then
                        ddlOBApprovalParty_NonITMajor_L1.SelectedValue = MyData.Tables(8).Rows(0)("L1_ApprovalParty").ToString
                    Else
                        lblMsg_OBApprovalParty_NonITMajor_L1.Visible = True
                        lblMsg_OBApprovalParty_NonITMajor_L1.Text = "Please reset Approval Party 1"
                    End If

                    If Not ddlOBApprovalParty2_NonITMajor_L1.Items.FindByValue(MyData.Tables(8).Rows(0)("L1_ApprovalParty2").ToString) Is Nothing Then
                        ddlOBApprovalParty2_NonITMajor_L1.SelectedValue = MyData.Tables(8).Rows(0)("L1_ApprovalParty2").ToString
                    Else
                        lblMsg_OBApprovalParty_NonITMajor_L1.Visible = True
                        lblMsg_OBApprovalParty_NonITMajor_L1.Text = "Please reset Approval Party 2"
                    End If


                    ddlOBComparision_NonITMajor_L2.SelectedValue = MyData.Tables(8).Rows(0)("L2_Comparison").ToString
                    ddlOBCurrencyCode_NonITMajor_L2.SelectedValue = MyData.Tables(8).Rows(0)("L2_CurrenyCode").ToString
                    txtOBAmount_NonITMajor_L2.Text = MyData.Tables(8).Rows(0)("L2_Amount").ToString
                    If Not ddlOBApprovalParty_NonITMajor_L2.Items.FindByValue(MyData.Tables(8).Rows(0)("L2_ApprovalParty").ToString) Is Nothing Then
                        ddlOBApprovalParty_NonITMajor_L2.SelectedValue = MyData.Tables(8).Rows(0)("L2_ApprovalParty").ToString
                    Else
                        lblMsg_OBApprovalParty_NonITMajor_L2.Visible = True
                        lblMsg_OBApprovalParty_NonITMajor_L2.Text = "Please reset Approval Party"
                    End If


                    ddlOBComparision_NonITMajor_L3.SelectedValue = MyData.Tables(8).Rows(0)("L3_Comparison").ToString
                    ddlOBCurrencyCode_NonITMajor_L3.SelectedValue = MyData.Tables(8).Rows(0)("L3_CurrenyCode").ToString
                    txtOBAmount_NonITMajor_L3.Text = MyData.Tables(8).Rows(0)("L3_Amount").ToString
                    If Not ddlOBApprovalParty_NonITMajor_L3.Items.FindByValue(MyData.Tables(8).Rows(0)("L3_ApprovalParty").ToString) Is Nothing Then
                        ddlOBApprovalParty_NonITMajor_L3.SelectedValue = MyData.Tables(8).Rows(0)("L3_ApprovalParty").ToString
                    Else
                        lblMsg_OBApprovalParty_NonITMajor_L3.Visible = True
                        lblMsg_OBApprovalParty_NonITMajor_L3.Text = "Please reset Approval Party"
                    End If


                    ddlOBComparision_NonITMajor_L4.SelectedValue = MyData.Tables(8).Rows(0)("L4_Comparison").ToString
                    ddlOBCurrencyCode_NonITMajor_L4.SelectedValue = MyData.Tables(8).Rows(0)("L4_CurrenyCode").ToString
                    txtOBAmount_NonITMajor_L4.Text = MyData.Tables(8).Rows(0)("L4_Amount").ToString
                    If Not ddlOBApprovalParty_NonITMajor_L4.Items.FindByValue(MyData.Tables(8).Rows(0)("L4_ApprovalParty").ToString) Is Nothing Then
                        ddlOBApprovalParty_NonITMajor_L4.SelectedValue = MyData.Tables(8).Rows(0)("L4_ApprovalParty").ToString
                    Else
                        lblMsg_OBApprovalParty_NonITMajor_L4.Visible = True
                        lblMsg_OBApprovalParty_NonITMajor_L4.Text = "Please reset Approval Party"
                    End If

                End If

                If MyData.Tables(9).Rows.Count > 0 Then
                    ddlDWComparision_IT_L1.SelectedValue = MyData.Tables(9).Rows(0)("L1_Comparison").ToString
                    ddlDWCurrencyCode_IT_L1.SelectedValue = MyData.Tables(9).Rows(0)("L1_CurrenyCode").ToString
                    txtDWAmount_IT_L1.Text = MyData.Tables(9).Rows(0)("L1_Amount").ToString

                    If Not ddlDWApprovalParty_IT_L1.Items.FindByValue(MyData.Tables(9).Rows(0)("L1_ApprovalParty").ToString) Is Nothing Then
                        ddlDWApprovalParty_IT_L1.SelectedValue = MyData.Tables(9).Rows(0)("L1_ApprovalParty").ToString
                    Else
                        lblMsg_DWApprovalParty_IT_L1.Visible = True
                        lblMsg_DWApprovalParty_IT_L1.Text = "Please reset Approval Party 1."
                    End If

                    If Not ddlDWApprovalParty2_IT_L1.Items.FindByValue(MyData.Tables(9).Rows(0)("L1_ApprovalParty2").ToString) Is Nothing Then
                        ddlDWApprovalParty2_IT_L1.SelectedValue = MyData.Tables(9).Rows(0)("L1_ApprovalParty2").ToString
                    Else
                        lblMsg_DWApprovalParty_IT_L1.Visible = True
                        lblMsg_DWApprovalParty_IT_L1.Text = "Please reset Approval Party 2."
                    End If


                    ddlDWComparision_IT_L2.SelectedValue = MyData.Tables(9).Rows(0)("L2_Comparison").ToString
                    ddlDWCurrencyCode_IT_L2.SelectedValue = MyData.Tables(9).Rows(0)("L2_CurrenyCode").ToString
                    txtDWAmount_IT_L2.Text = MyData.Tables(9).Rows(0)("L2_Amount").ToString

                    If Not ddlDWApprovalParty_IT_L2.Items.FindByValue(MyData.Tables(9).Rows(0)("L2_ApprovalParty").ToString) Is Nothing Then
                        ddlDWApprovalParty_IT_L2.SelectedValue = MyData.Tables(9).Rows(0)("L2_ApprovalParty").ToString
                    Else
                        lblMsg_DWApprovalParty_IT_L2.Visible = True
                        lblMsg_DWApprovalParty_IT_L2.Text = "Please reset Approval Party"
                    End If


                    ddlDWComparision_IT_L3.SelectedValue = MyData.Tables(9).Rows(0)("L3_Comparison").ToString
                    ddlDWCurrencyCode_IT_L3.SelectedValue = MyData.Tables(9).Rows(0)("L3_CurrenyCode").ToString
                    txtDWAmount_IT_L3.Text = MyData.Tables(9).Rows(0)("L3_Amount").ToString
                    If Not ddlDWApprovalParty_IT_L3.Items.FindByValue(MyData.Tables(9).Rows(0)("L3_ApprovalParty").ToString) Is Nothing Then
                        ddlDWApprovalParty_IT_L3.SelectedValue = MyData.Tables(9).Rows(0)("L3_ApprovalParty").ToString
                    Else
                        lblMsg_DWApprovalParty_IT_L3.Visible = True
                        lblMsg_DWApprovalParty_IT_L3.Text = "Please reset Approval Party"
                    End If


                    ddlDWComparision_IT_L4.SelectedValue = MyData.Tables(9).Rows(0)("L4_Comparison").ToString
                    ddlDWCurrencyCode_IT_L4.SelectedValue = MyData.Tables(9).Rows(0)("L4_CurrenyCode").ToString
                    txtDWAmount_IT_L4.Text = MyData.Tables(9).Rows(0)("L4_Amount").ToString
                    If Not ddlDWApprovalParty_IT_L4.Items.FindByValue(MyData.Tables(9).Rows(0)("L4_ApprovalParty").ToString) Is Nothing Then
                        ddlDWApprovalParty_IT_L4.SelectedValue = MyData.Tables(9).Rows(0)("L4_ApprovalParty").ToString
                    Else
                        lblMsg_DWApprovalParty_IT_L4.Visible = True
                        lblMsg_DWApprovalParty_IT_L4.Text = "Please reset Approval Party"
                    End If

                End If

                If MyData.Tables(10).Rows.Count > 0 Then
                    ddlDWComparision_NonIT_L1.SelectedValue = MyData.Tables(10).Rows(0)("L1_Comparison").ToString
                    ddlDWCurrencyCode_NonIT_L1.SelectedValue = MyData.Tables(10).Rows(0)("L1_CurrenyCode").ToString
                    txtDWAmount_NonIT_L1.Text = MyData.Tables(10).Rows(0)("L1_Amount").ToString
                    If Not ddlDWApprovalParty_NonIT_L1.Items.FindByValue(MyData.Tables(10).Rows(0)("L1_ApprovalParty").ToString) Is Nothing Then
                        ddlDWApprovalParty_NonIT_L1.SelectedValue = MyData.Tables(10).Rows(0)("L1_ApprovalParty").ToString
                    Else
                        lblMsg_DWApprovalParty_NonIT_L1.Visible = True
                        lblMsg_DWApprovalParty_NonIT_L1.Text = "Please reset Approval Party 1"
                    End If

                    If Not ddlDWApprovalParty2_NonIT_L1.Items.FindByValue(MyData.Tables(10).Rows(0)("L1_ApprovalParty2").ToString) Is Nothing Then
                        ddlDWApprovalParty2_NonIT_L1.SelectedValue = MyData.Tables(10).Rows(0)("L1_ApprovalParty2").ToString
                    Else
                        lblMsg_DWApprovalParty_NonIT_L1.Visible = True
                        lblMsg_DWApprovalParty_NonIT_L1.Text = "Please reset Approval Party 2"
                    End If


                    ddlDWComparision_NonIT_L2.SelectedValue = MyData.Tables(10).Rows(0)("L2_Comparison").ToString
                    ddlDWCurrencyCode_NonIT_L2.SelectedValue = MyData.Tables(10).Rows(0)("L2_CurrenyCode").ToString
                    txtDWAmount_NonIT_L2.Text = MyData.Tables(10).Rows(0)("L2_Amount").ToString

                    If Not ddlDWApprovalParty_NonIT_L2.Items.FindByValue(MyData.Tables(10).Rows(0)("L2_ApprovalParty").ToString) Is Nothing Then
                        ddlDWApprovalParty_NonIT_L2.SelectedValue = MyData.Tables(10).Rows(0)("L2_ApprovalParty").ToString
                    Else
                        lblMsg_DWApprovalParty_NonIT_L2.Visible = True
                        lblMsg_DWApprovalParty_NonIT_L2.Text = "Please reset Approval Party"
                    End If


                    ddlDWComparision_NonIT_L3.SelectedValue = MyData.Tables(10).Rows(0)("L3_Comparison").ToString
                    ddlDWCurrencyCode_NonIT_L3.SelectedValue = MyData.Tables(10).Rows(0)("L3_CurrenyCode").ToString
                    txtDWAmount_NonIT_L3.Text = MyData.Tables(10).Rows(0)("L3_Amount").ToString
                    If Not ddlDWApprovalParty_NonIT_L3.Items.FindByValue(MyData.Tables(10).Rows(0)("L3_ApprovalParty").ToString) Is Nothing Then
                        ddlDWApprovalParty_NonIT_L3.SelectedValue = MyData.Tables(10).Rows(0)("L3_ApprovalParty").ToString
                    Else
                        lblMsg_DWApprovalParty_NonIT_L3.Visible = True
                        lblMsg_DWApprovalParty_NonIT_L3.Text = "Please reset Approval Party"
                    End If


                    ddlDWComparision_NonIT_L4.SelectedValue = MyData.Tables(10).Rows(0)("L4_Comparison").ToString
                    ddlDWCurrencyCode_NonIT_L4.SelectedValue = MyData.Tables(10).Rows(0)("L4_CurrenyCode").ToString
                    txtDWAmount_NonIT_L4.Text = MyData.Tables(10).Rows(0)("L4_Amount").ToString
                    If Not ddlDWApprovalParty_NonIT_L4.Items.FindByValue(MyData.Tables(10).Rows(0)("L4_ApprovalParty").ToString) Is Nothing Then
                        ddlDWApprovalParty_NonIT_L4.SelectedValue = MyData.Tables(10).Rows(0)("L4_ApprovalParty").ToString
                    Else
                        lblMsg_DWApprovalParty_NonIT_L4.Visible = True
                        lblMsg_DWApprovalParty_NonIT_L4.Text = "Please reset Approval Party"
                    End If

                End If

                If MyData.Tables(11).Rows.Count > 0 Then
                    ddlDWComparision_NonITMajor_L1.SelectedValue = MyData.Tables(11).Rows(0)("L1_Comparison").ToString
                    ddlDWCurrencyCode_NonITMajor_L1.SelectedValue = MyData.Tables(11).Rows(0)("L1_CurrenyCode").ToString
                    txtDWAmount_NonITMajor_L1.Text = MyData.Tables(11).Rows(0)("L1_Amount").ToString
                    If Not ddlDWApprovalParty_NonITMajor_L1.Items.FindByValue(MyData.Tables(11).Rows(0)("L1_ApprovalParty").ToString) Is Nothing Then
                        ddlDWApprovalParty_NonITMajor_L1.SelectedValue = MyData.Tables(11).Rows(0)("L1_ApprovalParty").ToString
                    Else
                        lblMsg_DWApprovalParty_NonITMajor_L1.Visible = True
                        lblMsg_DWApprovalParty_NonITMajor_L1.Text = "Please reset Approval Party 1"
                    End If

                    If Not ddlDWApprovalParty2_NonITMajor_L1.Items.FindByValue(MyData.Tables(11).Rows(0)("L1_ApprovalParty2").ToString) Is Nothing Then
                        ddlDWApprovalParty2_NonITMajor_L1.SelectedValue = MyData.Tables(11).Rows(0)("L1_ApprovalParty2").ToString
                    Else
                        lblMsg_DWApprovalParty_NonITMajor_L1.Visible = True
                        lblMsg_DWApprovalParty_NonITMajor_L1.Text = "Please reset Approval Party 2"
                    End If


                    ddlDWComparision_NonITMajor_L2.SelectedValue = MyData.Tables(11).Rows(0)("L2_Comparison").ToString
                    ddlDWCurrencyCode_NonITMajor_L2.SelectedValue = MyData.Tables(11).Rows(0)("L2_CurrenyCode").ToString
                    txtDWAmount_NonITMajor_L2.Text = MyData.Tables(11).Rows(0)("L2_Amount").ToString
                    If Not ddlDWApprovalParty_NonITMajor_L2.Items.FindByValue(MyData.Tables(11).Rows(0)("L2_ApprovalParty").ToString) Is Nothing Then
                        ddlDWApprovalParty_NonITMajor_L2.SelectedValue = MyData.Tables(11).Rows(0)("L2_ApprovalParty").ToString
                    Else
                        lblMsg_DWApprovalParty_NonITMajor_L2.Visible = True
                        lblMsg_DWApprovalParty_NonITMajor_L2.Text = "Please reset Approval Party"
                    End If
                    
                    ddlDWComparision_NonITMajor_L3.SelectedValue = MyData.Tables(11).Rows(0)("L3_Comparison").ToString
                    ddlDWCurrencyCode_NonITMajor_L3.SelectedValue = MyData.Tables(11).Rows(0)("L3_CurrenyCode").ToString
                    txtDWAmount_NonITMajor_L3.Text = MyData.Tables(11).Rows(0)("L3_Amount").ToString
                    If Not ddlDWApprovalParty_NonITMajor_L3.Items.FindByValue(MyData.Tables(11).Rows(0)("L3_ApprovalParty").ToString) Is Nothing Then
                        ddlDWApprovalParty_NonITMajor_L3.SelectedValue = MyData.Tables(11).Rows(0)("L3_ApprovalParty").ToString
                    Else
                        lblMsg_DWApprovalParty_NonITMajor_L3.Visible = True
                        lblMsg_DWApprovalParty_NonITMajor_L3.Text = "Please reset Approval Party"
                    End If


                    ddlDWComparision_NonITMajor_L4.SelectedValue = MyData.Tables(11).Rows(0)("L4_Comparison").ToString
                    ddlDWCurrencyCode_NonITMajor_L4.SelectedValue = MyData.Tables(11).Rows(0)("L4_CurrenyCode").ToString
                    txtDWAmount_NonITMajor_L4.Text = MyData.Tables(11).Rows(0)("L4_Amount").ToString
                    If Not ddlDWApprovalParty_NonITMajor_L4.Items.FindByValue(MyData.Tables(11).Rows(0)("L4_ApprovalParty").ToString) Is Nothing Then
                        ddlDWApprovalParty_NonITMajor_L4.SelectedValue = MyData.Tables(11).Rows(0)("L4_ApprovalParty").ToString
                    Else
                        lblMsg_DWApprovalParty_NonITMajor_L4.Visible = True
                        lblMsg_DWApprovalParty_NonITMajor_L4.Text = "Please reset Approval Party"
                    End If

                End If

                gvLocation.DataSource = MyData
                gvLocation.DataMember = MyData.Tables(12).TableName
                gvLocation.DataBind()

                If MyData.Tables(13).Rows.Count > 0 Then

                    ddlCurrencyCode.SelectedValue = MyData.Tables(13).Rows(0)("CurrencyCodeID").ToString
                    txtQuoPricePoint.Text = MyData.Tables(13).Rows(0)("PricePointQuotation").ToString
                    txteReqPricePoint.Text = MyData.Tables(13).Rows(0)("PricePointeRequisition").ToString
                    txtFAPricePoint.Text = MyData.Tables(13).Rows(0)("PricePointFixedAsset").ToString
                    txtCARPricePoint.Text = MyData.Tables(13).Rows(0)("PricePointCreateAssetRecord").ToString

                    If MyData.Tables(13).Rows(0)("AutoGeneratePO").ToString = "1" Then
                        chkAutoPONo.Checked = True
                    Else
                        chkAutoPONo.Checked = False
                    End If
                End If

                If MyData.Tables(14).Rows.Count > 0 Then
                    ddleReq_1st.SelectedValue = MyData.Tables(14).Rows(0)("eReq_1stParty").ToString
                    ddleReq_2nd.SelectedValue = MyData.Tables(14).Rows(0)("eReq_2ndParty").ToString
                    ddleReq_3rd.SelectedValue = MyData.Tables(14).Rows(0)("eReq_3rdParty").ToString
                    ddlIssuePO_1st.SelectedValue = MyData.Tables(14).Rows(0)("IssuePO_1stParty").ToString
                    ddlIssuePO_2nd.SelectedValue = MyData.Tables(14).Rows(0)("IssuePO_2ndParty").ToString
                    ddlIssuePO_3rd.SelectedValue = MyData.Tables(14).Rows(0)("IssuePO_3rdParty").ToString
                    ddlIssuePayment_1st.SelectedValue = MyData.Tables(14).Rows(0)("IssuePayment_1stParty").ToString
                    ddlIssuePayment_2nd.SelectedValue = MyData.Tables(14).Rows(0)("IssuePayment_2ndParty").ToString
                    ddlIssuePayment_3rd.SelectedValue = MyData.Tables(14).Rows(0)("IssuePayment_3rdParty").ToString
                    ddleFMS_1st.SelectedValue = MyData.Tables(14).Rows(0)("eFMS_1stParty").ToString
                    ddleFMS_2nd.SelectedValue = MyData.Tables(14).Rows(0)("eFMS_2ndParty").ToString
                    ddleFMS_3rd.SelectedValue = MyData.Tables(14).Rows(0)("eFMS_3rdParty").ToString
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "GetStationSetting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String = ""
                Dim strStation As String = ""


                If ValidateData() = False Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Error - Please check data')</script>")
                    Exit Sub
                End If

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveStationSetting", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString


                    .Parameters.Add("@Comparision_IT_L1", SqlDbType.NVarChar, 50)
                    If ddlComparision_IT_L1.SelectedValue.ToString = "Select One" Then
                        .Parameters("@Comparision_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@Comparision_IT_L1").Value = ddlComparision_IT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@CurrencyCode_IT_L1", SqlDbType.BigInt)
                    If ddlCurrencyCode_IT_L1.SelectedValue.ToString = "0" Then
                        .Parameters("@CurrencyCode_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@CurrencyCode_IT_L1").Value = ddlCurrencyCode_IT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@Amount_IT_L1", SqlDbType.Decimal, 18, 2)
                    If txtAmount_IT_L1.Text = "" Then
                        .Parameters("@Amount_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@Amount_IT_L1").Value = CDec(txtAmount_IT_L1.Text.ToString)
                    End If

                    .Parameters.Add("@ApprovalParty_IT_L1", SqlDbType.VarChar, 6)
                    If ddlApprovalParty_IT_L1.SelectedValue.ToString = "" Then
                        .Parameters("@ApprovalParty_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@ApprovalParty_IT_L1").Value = ddlApprovalParty_IT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@ApprovalParty2_IT_L1", SqlDbType.VarChar, 6)
                    If ddlApprovalParty2_IT_L1.SelectedValue.ToString = "" Then
                        .Parameters("@ApprovalParty2_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@ApprovalParty2_IT_L1").Value = ddlApprovalParty2_IT_L1.SelectedValue.ToString
                    End If



                    .Parameters.Add("@Comparision_IT_L2", SqlDbType.NVarChar, 50)
                    If ddlComparision_IT_L2.SelectedValue.ToString = "Select One" Then
                        .Parameters("@Comparision_IT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@Comparision_IT_L2").Value = ddlComparision_IT_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@CurrencyCode_IT_L2", SqlDbType.BigInt)
                    If ddlCurrencyCode_IT_L2.SelectedValue.ToString = "0" Then
                        .Parameters("@CurrencyCode_IT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@CurrencyCode_IT_L2").Value = ddlCurrencyCode_IT_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@Amount_IT_L2", SqlDbType.Decimal, 18, 2)
                    If txtAmount_IT_L2.Text = "" Then
                        .Parameters("@Amount_IT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@Amount_IT_L2").Value = CDec(txtAmount_IT_L2.Text.ToString)
                    End If


                    .Parameters.Add("@ApprovalParty_IT_L2", SqlDbType.VarChar, 6)
                    If ddlApprovalParty_IT_L2.SelectedValue.ToString = "" Then
                        .Parameters("@ApprovalParty_IT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@ApprovalParty_IT_L2").Value = ddlApprovalParty_IT_L2.SelectedValue.ToString
                    End If


                    .Parameters.Add("@Comparision_IT_L3", SqlDbType.NVarChar, 50)
                    If ddlComparision_IT_L3.SelectedValue.ToString = "Select One" Then
                        .Parameters("@Comparision_IT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@Comparision_IT_L3").Value = ddlComparision_IT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@CurrencyCode_IT_L3", SqlDbType.BigInt)
                    If ddlCurrencyCode_IT_L3.SelectedValue.ToString = "0" Then
                        .Parameters("@CurrencyCode_IT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@CurrencyCode_IT_L3").Value = ddlCurrencyCode_IT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@Amount_IT_L3", SqlDbType.Decimal, 18, 2)
                    If txtAmount_IT_L3.Text = "" Then
                        .Parameters("@Amount_IT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@Amount_IT_L3").Value = CDec(txtAmount_IT_L3.Text.ToString)
                    End If

                    .Parameters.Add("@ApprovalParty_IT_L3", SqlDbType.VarChar, 6)
                    If ddlApprovalParty_IT_L3.SelectedValue.ToString = "" Then
                        .Parameters("@ApprovalParty_IT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@ApprovalParty_IT_L3").Value = ddlApprovalParty_IT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@Comparision_IT_L4", SqlDbType.NVarChar, 50)
                    If ddlComparision_IT_L4.SelectedValue.ToString = "Select One" Then
                        .Parameters("@Comparision_IT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@Comparision_IT_L4").Value = ddlComparision_IT_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@CurrencyCode_IT_L4", SqlDbType.BigInt)
                    If ddlCurrencyCode_IT_L4.SelectedValue.ToString = "0" Then
                        .Parameters("@CurrencyCode_IT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@CurrencyCode_IT_L4").Value = ddlCurrencyCode_IT_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@Amount_IT_L4", SqlDbType.Decimal, 18, 2)
                    If txtAmount_IT_L4.Text = "" Then
                        .Parameters("@Amount_IT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@Amount_IT_L4").Value = CDec(txtAmount_IT_L4.Text.ToString)
                    End If

                    .Parameters.Add("@ApprovalParty_IT_L4", SqlDbType.VarChar, 6)
                    If ddlApprovalParty_IT_L4.SelectedValue.ToString = "" Then
                        .Parameters("@ApprovalParty_IT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@ApprovalParty_IT_L4").Value = ddlApprovalParty_IT_L4.SelectedValue.ToString
                    End If


                    .Parameters.Add("@Comparision_NonIT_L1", SqlDbType.NVarChar, 50)
                    If ddlComparision_NonIT_L1.SelectedValue.ToString = "Select One" Then
                        .Parameters("@Comparision_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@Comparision_NonIT_L1").Value = ddlComparision_NonIT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@CurrencyCode_NonIT_L1", SqlDbType.BigInt)
                    If ddlCurrencyCode_NonIT_L1.SelectedValue.ToString = "0" Then
                        .Parameters("@CurrencyCode_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@CurrencyCode_NonIT_L1").Value = ddlCurrencyCode_NonIT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@Amount_NonIT_L1", SqlDbType.Decimal, 18, 2)
                    If txtAmount_NonIT_L1.Text = "" Then
                        .Parameters("@Amount_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@Amount_NonIT_L1").Value = CDec(txtAmount_NonIT_L1.Text.ToString)
                    End If

                    .Parameters.Add("@ApprovalParty_NonIT_L1", SqlDbType.VarChar, 6)
                    If ddlApprovalParty_NonIT_L1.SelectedValue.ToString = "" Then
                        .Parameters("@ApprovalParty_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@ApprovalParty_NonIT_L1").Value = ddlApprovalParty_NonIT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@ApprovalParty2_NonIT_L1", SqlDbType.VarChar, 6)
                    If ddlApprovalParty2_NonIT_L1.SelectedValue.ToString = "" Then
                        .Parameters("@ApprovalParty2_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@ApprovalParty2_NonIT_L1").Value = ddlApprovalParty2_NonIT_L1.SelectedValue.ToString
                    End If


                    .Parameters.Add("@Comparision_NonIT_L2", SqlDbType.NVarChar, 50)
                    If ddlComparision_NonIT_L2.SelectedValue.ToString = "Select One" Then
                        .Parameters("@Comparision_NonIT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@Comparision_NonIT_L2").Value = ddlComparision_NonIT_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@CurrencyCode_NonIT_L2", SqlDbType.BigInt)
                    If ddlCurrencyCode_NonIT_L2.SelectedValue.ToString = "0" Then
                        .Parameters("@CurrencyCode_NonIT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@CurrencyCode_NonIT_L2").Value = ddlCurrencyCode_NonIT_L2.SelectedValue.ToString
                    End If
                    .Parameters.Add("@Amount_NonIT_L2", SqlDbType.Decimal, 18, 2)
                    If txtAmount_NonIT_L2.Text = "" Then
                        .Parameters("@Amount_NonIT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@Amount_NonIT_L2").Value = CDec(txtAmount_NonIT_L2.Text.ToString)
                    End If

                    .Parameters.Add("@ApprovalParty_NonIT_L2", SqlDbType.VarChar, 6)
                    If ddlApprovalParty_NonIT_L2.SelectedValue.ToString = "" Then
                        .Parameters("@ApprovalParty_NonIT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@ApprovalParty_NonIT_L2").Value = ddlApprovalParty_NonIT_L2.SelectedValue.ToString
                    End If


                    .Parameters.Add("@Comparision_NonIT_L3", SqlDbType.NVarChar, 50)
                    If ddlComparision_NonIT_L3.SelectedValue.ToString = "Select One" Then
                        .Parameters("@Comparision_NonIT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@Comparision_NonIT_L3").Value = ddlComparision_NonIT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@CurrencyCode_NonIT_L3", SqlDbType.BigInt)
                    If ddlCurrencyCode_NonIT_L3.SelectedValue.ToString = "0" Then
                        .Parameters("@CurrencyCode_NonIT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@CurrencyCode_NonIT_L3").Value = ddlCurrencyCode_NonIT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@Amount_NonIT_L3", SqlDbType.Decimal, 18, 2)
                    If txtAmount_NonIT_L3.Text = "" Then
                        .Parameters("@Amount_NonIT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@Amount_NonIT_L3").Value = CDec(txtAmount_NonIT_L3.Text.ToString)
                    End If

                    .Parameters.Add("@ApprovalParty_NonIT_L3", SqlDbType.VarChar, 6)
                    If ddlApprovalParty_NonIT_L3.SelectedValue.ToString = "" Then
                        .Parameters("@ApprovalParty_NonIT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@ApprovalParty_NonIT_L3").Value = ddlApprovalParty_NonIT_L3.SelectedValue.ToString
                    End If


                    .Parameters.Add("@Comparision_NonIT_L4", SqlDbType.NVarChar, 50)
                    If ddlComparision_NonIT_L4.SelectedValue.ToString = "Select One" Then
                        .Parameters("@Comparision_NonIT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@Comparision_NonIT_L4").Value = ddlComparision_NonIT_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@CurrencyCode_NonIT_L4", SqlDbType.BigInt)
                    If ddlCurrencyCode_NonIT_L4.SelectedValue.ToString = "0" Then
                        .Parameters("@CurrencyCode_NonIT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@CurrencyCode_NonIT_L4").Value = ddlCurrencyCode_NonIT_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@Amount_NonIT_L4", SqlDbType.Decimal, 18, 2)
                    If txtAmount_NonIT_L4.Text = "" Then
                        .Parameters("@Amount_NonIT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@Amount_NonIT_L4").Value = CDec(txtAmount_NonIT_L4.Text.ToString)
                    End If

                    .Parameters.Add("@ApprovalParty_NonIT_L4", SqlDbType.VarChar, 6)
                    If ddlApprovalParty_NonIT_L4.SelectedValue.ToString = "" Then
                        .Parameters("@ApprovalParty_NonIT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@ApprovalParty_NonIT_L4").Value = ddlApprovalParty_NonIT_L4.SelectedValue.ToString
                    End If

                    'NON IT (MAJOR)
                    .Parameters.Add("@Comparision_NonITMajor_L1", SqlDbType.NVarChar, 50)
                    If ddlComparision_NonITMajor_L1.SelectedValue.ToString = "Select One" Then
                        .Parameters("@Comparision_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@Comparision_NonITMajor_L1").Value = ddlComparision_NonITMajor_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@CurrencyCode_NonITMajor_L1", SqlDbType.BigInt)
                    If ddlCurrencyCode_NonITMajor_L1.SelectedValue.ToString = "0" Then
                        .Parameters("@CurrencyCode_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@CurrencyCode_NonITMajor_L1").Value = ddlCurrencyCode_NonITMajor_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@Amount_NonITMajor_L1", SqlDbType.Decimal, 18, 2)
                    If txtAmount_NonITMajor_L1.Text = "" Then
                        .Parameters("@Amount_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@Amount_NonITMajor_L1").Value = CDec(txtAmount_NonITMajor_L1.Text.ToString)
                    End If

                    .Parameters.Add("@ApprovalParty_NonITMajor_L1", SqlDbType.VarChar, 6)
                    If ddlApprovalParty_NonITMajor_L1.SelectedValue.ToString = "" Then
                        .Parameters("@ApprovalParty_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@ApprovalParty_NonITMajor_L1").Value = ddlApprovalParty_NonITMajor_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@ApprovalParty2_NonITMajor_L1", SqlDbType.VarChar, 6)
                    If ddlApprovalParty2_NonITMajor_L1.SelectedValue.ToString = "" Then
                        .Parameters("@ApprovalParty2_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@ApprovalParty2_NonITMajor_L1").Value = ddlApprovalParty2_NonITMajor_L1.SelectedValue.ToString
                    End If


                    .Parameters.Add("@Comparision_NonITMajor_L2", SqlDbType.NVarChar, 50)
                    If ddlComparision_NonITMajor_L2.SelectedValue.ToString = "Select One" Then
                        .Parameters("@Comparision_NonITMajor_L2").Value = DBNull.Value
                    Else
                        .Parameters("@Comparision_NonITMajor_L2").Value = ddlComparision_NonITMajor_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@CurrencyCode_NonITMajor_L2", SqlDbType.BigInt)
                    If ddlCurrencyCode_NonITMajor_L2.SelectedValue.ToString = "0" Then
                        .Parameters("@CurrencyCode_NonITMajor_L2").Value = DBNull.Value
                    Else
                        .Parameters("@CurrencyCode_NonITMajor_L2").Value = ddlCurrencyCode_NonITMajor_L2.SelectedValue.ToString
                    End If
                    .Parameters.Add("@Amount_NonITMajor_L2", SqlDbType.Decimal, 18, 2)
                    If txtAmount_NonITMajor_L2.Text = "" Then
                        .Parameters("@Amount_NonITMajor_L2").Value = DBNull.Value
                    Else
                        .Parameters("@Amount_NonITMajor_L2").Value = CDec(txtAmount_NonITMajor_L2.Text.ToString)
                    End If

                    .Parameters.Add("@ApprovalParty_NonITMajor_L2", SqlDbType.VarChar, 6)
                    If ddlApprovalParty_NonITMajor_L2.SelectedValue.ToString = "" Then
                        .Parameters("@ApprovalParty_NonITMajor_L2").Value = DBNull.Value
                    Else
                        .Parameters("@ApprovalParty_NonITMajor_L2").Value = ddlApprovalParty_NonITMajor_L2.SelectedValue.ToString
                    End If


                    .Parameters.Add("@Comparision_NonITMajor_L3", SqlDbType.NVarChar, 50)
                    If ddlComparision_NonITMajor_L3.SelectedValue.ToString = "Select One" Then
                        .Parameters("@Comparision_NonITMajor_L3").Value = DBNull.Value
                    Else
                        .Parameters("@Comparision_NonITMajor_L3").Value = ddlComparision_NonITMajor_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@CurrencyCode_NonITMajor_L3", SqlDbType.BigInt)
                    If ddlCurrencyCode_NonITMajor_L3.SelectedValue.ToString = "0" Then
                        .Parameters("@CurrencyCode_NonITMajor_L3").Value = DBNull.Value
                    Else
                        .Parameters("@CurrencyCode_NonITMajor_L3").Value = ddlCurrencyCode_NonITMajor_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@Amount_NonITMajor_L3", SqlDbType.Decimal, 18, 2)
                    If txtAmount_NonITMajor_L3.Text = "" Then
                        .Parameters("@Amount_NonITMajor_L3").Value = DBNull.Value
                    Else
                        .Parameters("@Amount_NonITMajor_L3").Value = CDec(txtAmount_NonITMajor_L3.Text.ToString)
                    End If

                    .Parameters.Add("@ApprovalParty_NonITMajor_L3", SqlDbType.VarChar, 6)
                    If ddlApprovalParty_NonITMajor_L3.SelectedValue.ToString = "" Then
                        .Parameters("@ApprovalParty_NonITMajor_L3").Value = DBNull.Value
                    Else
                        .Parameters("@ApprovalParty_NonITMajor_L3").Value = ddlApprovalParty_NonITMajor_L3.SelectedValue.ToString
                    End If


                    .Parameters.Add("@Comparision_NonITMajor_L4", SqlDbType.NVarChar, 50)
                    If ddlComparision_NonITMajor_L4.SelectedValue.ToString = "Select One" Then
                        .Parameters("@Comparision_NonITMajor_L4").Value = DBNull.Value
                    Else
                        .Parameters("@Comparision_NonITMajor_L4").Value = ddlComparision_NonITMajor_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@CurrencyCode_NonITMajor_L4", SqlDbType.BigInt)
                    If ddlCurrencyCode_NonITMajor_L4.SelectedValue.ToString = "0" Then
                        .Parameters("@CurrencyCode_NonITMajor_L4").Value = DBNull.Value
                    Else
                        .Parameters("@CurrencyCode_NonITMajor_L4").Value = ddlCurrencyCode_NonITMajor_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@Amount_NonITMajor_L4", SqlDbType.Decimal, 18, 2)
                    If txtAmount_NonITMajor_L4.Text = "" Then
                        .Parameters("@Amount_NonITMajor_L4").Value = DBNull.Value
                    Else
                        .Parameters("@Amount_NonITMajor_L4").Value = CDec(txtAmount_NonITMajor_L4.Text.ToString)
                    End If

                    .Parameters.Add("@ApprovalParty_NonITMajor_L4", SqlDbType.VarChar, 6)
                    If ddlApprovalParty_NonITMajor_L4.SelectedValue.ToString = "" Then
                        .Parameters("@ApprovalParty_NonITMajor_L4").Value = DBNull.Value
                    Else
                        .Parameters("@ApprovalParty_NonITMajor_L4").Value = ddlApprovalParty_NonITMajor_L4.SelectedValue.ToString
                    End If



                    ''


                    .Parameters.Add("@UBComparision_IT_L1", SqlDbType.NVarChar, 50)
                    If ddlUBComparision_IT_L1.SelectedValue.ToString = "Select One" Then
                        .Parameters("@UBComparision_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@UBComparision_IT_L1").Value = ddlUBComparision_IT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBCurrencyCode_IT_L1", SqlDbType.BigInt)
                    If ddlUBCurrencyCode_IT_L1.SelectedValue.ToString = "0" Then
                        .Parameters("@UBCurrencyCode_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@UBCurrencyCode_IT_L1").Value = ddlUBCurrencyCode_IT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBAmount_IT_L1", SqlDbType.Decimal, 18, 2)
                    If txtUBAmount_IT_L1.Text = "" Then
                        .Parameters("@UBAmount_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@UBAmount_IT_L1").Value = CDec(txtUBAmount_IT_L1.Text.ToString)
                    End If

                    .Parameters.Add("@UBApprovalParty_IT_L1", SqlDbType.VarChar, 6)
                    If ddlUBApprovalParty_IT_L1.SelectedValue.ToString = "" Then
                        .Parameters("@UBApprovalParty_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@UBApprovalParty_IT_L1").Value = ddlUBApprovalParty_IT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBApprovalParty2_IT_L1", SqlDbType.VarChar, 6)
                    If ddlUBApprovalParty2_IT_L1.SelectedValue.ToString = "" Then
                        .Parameters("@UBApprovalParty2_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@UBApprovalParty2_IT_L1").Value = ddlUBApprovalParty2_IT_L1.SelectedValue.ToString
                    End If


                    .Parameters.Add("@UBComparision_IT_L2", SqlDbType.NVarChar, 50)
                    If ddlUBComparision_IT_L2.SelectedValue.ToString = "Select One" Then
                        .Parameters("@UBComparision_IT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@UBComparision_IT_L2").Value = ddlUBComparision_IT_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBCurrencyCode_IT_L2", SqlDbType.BigInt)
                    If ddlUBCurrencyCode_IT_L2.SelectedValue.ToString = "0" Then
                        .Parameters("@UBCurrencyCode_IT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@UBCurrencyCode_IT_L2").Value = ddlUBCurrencyCode_IT_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBAmount_IT_L2", SqlDbType.Decimal, 18, 2)
                    If txtUBAmount_IT_L2.Text = "" Then
                        .Parameters("@UBAmount_IT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@UBAmount_IT_L2").Value = CDec(txtUBAmount_IT_L2.Text.ToString)
                    End If

                    .Parameters.Add("@UBApprovalParty_IT_L2", SqlDbType.VarChar, 6)
                    If ddlUBApprovalParty_IT_L2.SelectedValue.ToString = "" Then
                        .Parameters("@UBApprovalParty_IT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@UBApprovalParty_IT_L2").Value = ddlUBApprovalParty_IT_L2.SelectedValue.ToString
                    End If


                    .Parameters.Add("@UBComparision_IT_L3", SqlDbType.NVarChar, 50)
                    If ddlUBComparision_IT_L3.SelectedValue.ToString = "Select One" Then
                        .Parameters("@UBComparision_IT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@UBComparision_IT_L3").Value = ddlUBComparision_IT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBCurrencyCode_IT_L3", SqlDbType.BigInt)
                    If ddlUBCurrencyCode_IT_L3.SelectedValue.ToString = "0" Then
                        .Parameters("@UBCurrencyCode_IT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@UBCurrencyCode_IT_L3").Value = ddlUBCurrencyCode_IT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBAmount_IT_L3", SqlDbType.Decimal, 18, 2)
                    If txtUBAmount_IT_L3.Text = "" Then
                        .Parameters("@UBAmount_IT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@UBAmount_IT_L3").Value = CDec(txtUBAmount_IT_L3.Text.ToString)
                    End If

                    .Parameters.Add("@UBApprovalParty_IT_L3", SqlDbType.VarChar, 6)
                    If ddlUBApprovalParty_IT_L3.SelectedValue.ToString = "" Then
                        .Parameters("@UBApprovalParty_IT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@UBApprovalParty_IT_L3").Value = ddlUBApprovalParty_IT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBComparision_IT_L4", SqlDbType.NVarChar, 50)
                    If ddlUBComparision_IT_L4.SelectedValue.ToString = "Select One" Then
                        .Parameters("@UBComparision_IT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@UBComparision_IT_L4").Value = ddlUBComparision_IT_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBCurrencyCode_IT_L4", SqlDbType.BigInt)
                    If ddlUBCurrencyCode_IT_L4.SelectedValue.ToString = "0" Then
                        .Parameters("@UBCurrencyCode_IT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@UBCurrencyCode_IT_L4").Value = ddlUBCurrencyCode_IT_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBAmount_IT_L4", SqlDbType.Decimal, 18, 2)
                    If txtUBAmount_IT_L4.Text = "" Then
                        .Parameters("@UBAmount_IT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@UBAmount_IT_L4").Value = CDec(txtUBAmount_IT_L4.Text.ToString)
                    End If

                    .Parameters.Add("@UBApprovalParty_IT_L4", SqlDbType.VarChar, 6)
                    If ddlUBApprovalParty_IT_L4.SelectedValue.ToString = "" Then
                        .Parameters("@UBApprovalParty_IT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@UBApprovalParty_IT_L4").Value = ddlUBApprovalParty_IT_L4.SelectedValue.ToString
                    End If


                    .Parameters.Add("@UBComparision_NonIT_L1", SqlDbType.NVarChar, 50)
                    If ddlUBComparision_NonIT_L1.SelectedValue.ToString = "Select One" Then
                        .Parameters("@UBComparision_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@UBComparision_NonIT_L1").Value = ddlUBComparision_NonIT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBCurrencyCode_NonIT_L1", SqlDbType.BigInt)
                    If ddlUBCurrencyCode_NonIT_L1.SelectedValue.ToString = "0" Then
                        .Parameters("@UBCurrencyCode_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@UBCurrencyCode_NonIT_L1").Value = ddlUBCurrencyCode_NonIT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBAmount_NonIT_L1", SqlDbType.Decimal, 18, 2)
                    If txtUBAmount_NonIT_L1.Text = "" Then
                        .Parameters("@UBAmount_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@UBAmount_NonIT_L1").Value = CDec(txtUBAmount_NonIT_L1.Text.ToString)
                    End If

                    .Parameters.Add("@UBApprovalParty_NonIT_L1", SqlDbType.VarChar, 6)
                    If ddlUBApprovalParty_NonIT_L1.SelectedValue.ToString = "" Then
                        .Parameters("@UBApprovalParty_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@UBApprovalParty_NonIT_L1").Value = ddlUBApprovalParty_NonIT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBApprovalParty2_NonIT_L1", SqlDbType.VarChar, 6)
                    If ddlUBApprovalParty2_NonIT_L1.SelectedValue.ToString = "" Then
                        .Parameters("@UBApprovalParty2_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@UBApprovalParty2_NonIT_L1").Value = ddlUBApprovalParty2_NonIT_L1.SelectedValue.ToString
                    End If


                    .Parameters.Add("@UBComparision_NonIT_L2", SqlDbType.NVarChar, 50)
                    If ddlUBComparision_NonIT_L2.SelectedValue.ToString = "Select One" Then
                        .Parameters("@UBComparision_NonIT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@UBComparision_NonIT_L2").Value = ddlUBComparision_NonIT_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBCurrencyCode_NonIT_L2", SqlDbType.BigInt)
                    If ddlUBCurrencyCode_NonIT_L2.SelectedValue.ToString = "0" Then
                        .Parameters("@UBCurrencyCode_NonIT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@UBCurrencyCode_NonIT_L2").Value = ddlUBCurrencyCode_NonIT_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBAmount_NonIT_L2", SqlDbType.Decimal, 18, 2)
                    If txtUBAmount_NonIT_L2.Text = "" Then
                        .Parameters("@UBAmount_NonIT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@UBAmount_NonIT_L2").Value = CDec(txtUBAmount_NonIT_L2.Text.ToString)
                    End If

                    .Parameters.Add("@UBApprovalParty_NonIT_L2", SqlDbType.VarChar, 6)
                    If ddlUBApprovalParty_NonIT_L2.SelectedValue.ToString = "" Then
                        .Parameters("@UBApprovalParty_NonIT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@UBApprovalParty_NonIT_L2").Value = ddlUBApprovalParty_NonIT_L2.SelectedValue.ToString
                    End If


                    .Parameters.Add("@UBComparision_NonIT_L3", SqlDbType.NVarChar, 50)
                    If ddlUBComparision_NonIT_L3.SelectedValue.ToString = "Select One" Then
                        .Parameters("@UBComparision_NonIT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@UBComparision_NonIT_L3").Value = ddlUBComparision_NonIT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBCurrencyCode_NonIT_L3", SqlDbType.BigInt)
                    If ddlUBCurrencyCode_NonIT_L3.SelectedValue.ToString = "0" Then
                        .Parameters("@UBCurrencyCode_NonIT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@UBCurrencyCode_NonIT_L3").Value = ddlUBCurrencyCode_NonIT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBAmount_NonIT_L3", SqlDbType.Decimal, 18, 2)
                    If txtUBAmount_NonIT_L3.Text = "" Then
                        .Parameters("@UBAmount_NonIT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@UBAmount_NonIT_L3").Value = CDec(txtUBAmount_NonIT_L3.Text.ToString)
                    End If

                    .Parameters.Add("@UBApprovalParty_NonIT_L3", SqlDbType.VarChar, 6)
                    If ddlUBApprovalParty_NonIT_L3.SelectedValue.ToString = "" Then
                        .Parameters("@UBApprovalParty_NonIT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@UBApprovalParty_NonIT_L3").Value = ddlUBApprovalParty_NonIT_L3.SelectedValue.ToString
                    End If


                    .Parameters.Add("@UBComparision_NonIT_L4", SqlDbType.NVarChar, 50)
                    If ddlUBComparision_NonIT_L4.SelectedValue.ToString = "Select One" Then
                        .Parameters("@UBComparision_NonIT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@UBComparision_NonIT_L4").Value = ddlUBComparision_NonIT_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBCurrencyCode_NonIT_L4", SqlDbType.BigInt)
                    If ddlUBCurrencyCode_NonIT_L4.SelectedValue.ToString = "0" Then
                        .Parameters("@UBCurrencyCode_NonIT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@UBCurrencyCode_NonIT_L4").Value = ddlUBCurrencyCode_NonIT_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBAmount_NonIT_L4", SqlDbType.Decimal, 18, 2)
                    If txtUBAmount_NonIT_L4.Text = "" Then
                        .Parameters("@UBAmount_NonIT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@UBAmount_NonIT_L4").Value = CDec(txtUBAmount_NonIT_L4.Text.ToString)
                    End If

                    .Parameters.Add("@UBApprovalParty_NonIT_L4", SqlDbType.VarChar, 6)
                    If ddlUBApprovalParty_NonIT_L4.SelectedValue.ToString = "" Then
                        .Parameters("@UBApprovalParty_NonIT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@UBApprovalParty_NonIT_L4").Value = ddlUBApprovalParty_NonIT_L4.SelectedValue.ToString
                    End If

                    'NON IT (MAJOR)
                    .Parameters.Add("@UBComparision_NonITMajor_L1", SqlDbType.NVarChar, 50)
                    If ddlUBComparision_NonITMajor_L1.SelectedValue.ToString = "Select One" Then
                        .Parameters("@UBComparision_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@UBComparision_NonITMajor_L1").Value = ddlUBComparision_NonITMajor_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBCurrencyCode_NonITMajor_L1", SqlDbType.BigInt)
                    If ddlUBCurrencyCode_NonITMajor_L1.SelectedValue.ToString = "0" Then
                        .Parameters("@UBCurrencyCode_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@UBCurrencyCode_NonITMajor_L1").Value = ddlUBCurrencyCode_NonITMajor_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBAmount_NonITMajor_L1", SqlDbType.Decimal, 18, 2)
                    If txtUBAmount_NonITMajor_L1.Text = "" Then
                        .Parameters("@UBAmount_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@UBAmount_NonITMajor_L1").Value = CDec(txtUBAmount_NonITMajor_L1.Text.ToString)
                    End If

                    .Parameters.Add("@UBApprovalParty_NonITMajor_L1", SqlDbType.VarChar, 6)
                    If ddlUBApprovalParty_NonITMajor_L1.SelectedValue.ToString = "" Then
                        .Parameters("@UBApprovalParty_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@UBApprovalParty_NonITMajor_L1").Value = ddlUBApprovalParty_NonITMajor_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBApprovalParty2_NonITMajor_L1", SqlDbType.VarChar, 6)
                    If ddlUBApprovalParty2_NonITMajor_L1.SelectedValue.ToString = "" Then
                        .Parameters("@UBApprovalParty2_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@UBApprovalParty2_NonITMajor_L1").Value = ddlUBApprovalParty2_NonITMajor_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBComparision_NonITMajor_L2", SqlDbType.NVarChar, 50)
                    If ddlUBComparision_NonITMajor_L2.SelectedValue.ToString = "Select One" Then
                        .Parameters("@UBComparision_NonITMajor_L2").Value = DBNull.Value
                    Else
                        .Parameters("@UBComparision_NonITMajor_L2").Value = ddlUBComparision_NonITMajor_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBCurrencyCode_NonITMajor_L2", SqlDbType.BigInt)
                    If ddlUBCurrencyCode_NonITMajor_L2.SelectedValue.ToString = "0" Then
                        .Parameters("@UBCurrencyCode_NonITMajor_L2").Value = DBNull.Value
                    Else
                        .Parameters("@UBCurrencyCode_NonITMajor_L2").Value = ddlUBCurrencyCode_NonITMajor_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBAmount_NonITMajor_L2", SqlDbType.Decimal, 18, 2)
                    If txtUBAmount_NonITMajor_L2.Text = "" Then
                        .Parameters("@UBAmount_NonITMajor_L2").Value = DBNull.Value
                    Else
                        .Parameters("@UBAmount_NonITMajor_L2").Value = CDec(txtUBAmount_NonITMajor_L2.Text.ToString)
                    End If

                    .Parameters.Add("@UBApprovalParty_NonITMajor_L2", SqlDbType.VarChar, 6)
                    If ddlUBApprovalParty_NonITMajor_L2.SelectedValue.ToString = "" Then
                        .Parameters("@UBApprovalParty_NonITMajor_L2").Value = DBNull.Value
                    Else
                        .Parameters("@UBApprovalParty_NonITMajor_L2").Value = ddlUBApprovalParty_NonITMajor_L2.SelectedValue.ToString
                    End If


                    .Parameters.Add("@UBComparision_NonITMajor_L3", SqlDbType.NVarChar, 50)
                    If ddlUBComparision_NonITMajor_L3.SelectedValue.ToString = "Select One" Then
                        .Parameters("@UBComparision_NonITMajor_L3").Value = DBNull.Value
                    Else
                        .Parameters("@UBComparision_NonITMajor_L3").Value = ddlUBComparision_NonITMajor_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBCurrencyCode_NonITMajor_L3", SqlDbType.BigInt)
                    If ddlUBCurrencyCode_NonITMajor_L3.SelectedValue.ToString = "0" Then
                        .Parameters("@UBCurrencyCode_NonITMajor_L3").Value = DBNull.Value
                    Else
                        .Parameters("@UBCurrencyCode_NonITMajor_L3").Value = ddlUBCurrencyCode_NonITMajor_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBAmount_NonITMajor_L3", SqlDbType.Decimal, 18, 2)
                    If txtUBAmount_NonITMajor_L3.Text = "" Then
                        .Parameters("@UBAmount_NonITMajor_L3").Value = DBNull.Value
                    Else
                        .Parameters("@UBAmount_NonITMajor_L3").Value = CDec(txtUBAmount_NonITMajor_L3.Text.ToString)
                    End If

                    .Parameters.Add("@UBApprovalParty_NonITMajor_L3", SqlDbType.VarChar, 6)
                    If ddlUBApprovalParty_NonITMajor_L3.SelectedValue.ToString = "" Then
                        .Parameters("@UBApprovalParty_NonITMajor_L3").Value = DBNull.Value
                    Else
                        .Parameters("@UBApprovalParty_NonITMajor_L3").Value = ddlUBApprovalParty_NonITMajor_L3.SelectedValue.ToString
                    End If


                    .Parameters.Add("@UBComparision_NonITMajor_L4", SqlDbType.NVarChar, 50)
                    If ddlUBComparision_NonITMajor_L4.SelectedValue.ToString = "Select One" Then
                        .Parameters("@UBComparision_NonITMajor_L4").Value = DBNull.Value
                    Else
                        .Parameters("@UBComparision_NonITMajor_L4").Value = ddlUBComparision_NonITMajor_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBCurrencyCode_NonITMajor_L4", SqlDbType.BigInt)
                    If ddlUBCurrencyCode_NonITMajor_L4.SelectedValue.ToString = "0" Then
                        .Parameters("@UBCurrencyCode_NonITMajor_L4").Value = DBNull.Value
                    Else
                        .Parameters("@UBCurrencyCode_NonITMajor_L4").Value = ddlUBCurrencyCode_NonITMajor_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@UBAmount_NonITMajor_L4", SqlDbType.Decimal, 18, 2)
                    If txtUBAmount_NonITMajor_L4.Text = "" Then
                        .Parameters("@UBAmount_NonITMajor_L4").Value = DBNull.Value
                    Else
                        .Parameters("@UBAmount_NonITMajor_L4").Value = CDec(txtUBAmount_NonITMajor_L4.Text.ToString)
                    End If

                    .Parameters.Add("@UBApprovalParty_NonITMajor_L4", SqlDbType.VarChar, 6)
                    If ddlUBApprovalParty_NonITMajor_L4.SelectedValue.ToString = "" Then
                        .Parameters("@UBApprovalParty_NonITMajor_L4").Value = DBNull.Value
                    Else
                        .Parameters("@UBApprovalParty_NonITMajor_L4").Value = ddlUBApprovalParty_NonITMajor_L4.SelectedValue.ToString
                    End If

                    ''''


                    .Parameters.Add("@OBComparision_IT_L1", SqlDbType.NVarChar, 50)
                    If ddlOBComparision_IT_L1.SelectedValue.ToString = "Select One" Then
                        .Parameters("@OBComparision_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@OBComparision_IT_L1").Value = ddlOBComparision_IT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBCurrencyCode_IT_L1", SqlDbType.BigInt)
                    If ddlOBCurrencyCode_IT_L1.SelectedValue.ToString = "0" Then
                        .Parameters("@OBCurrencyCode_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@OBCurrencyCode_IT_L1").Value = ddlOBCurrencyCode_IT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBAmount_IT_L1", SqlDbType.Decimal, 18, 2)
                    If txtOBAmount_IT_L1.Text = "" Then
                        .Parameters("@OBAmount_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@OBAmount_IT_L1").Value = CDec(txtOBAmount_IT_L1.Text.ToString)
                    End If

                    .Parameters.Add("@OBApprovalParty_IT_L1", SqlDbType.VarChar, 6)
                    If ddlOBApprovalParty_IT_L1.SelectedValue.ToString = "" Then
                        .Parameters("@OBApprovalParty_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@OBApprovalParty_IT_L1").Value = ddlOBApprovalParty_IT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBApprovalParty2_IT_L1", SqlDbType.VarChar, 6)
                    If ddlOBApprovalParty2_IT_L1.SelectedValue.ToString = "" Then
                        .Parameters("@OBApprovalParty2_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@OBApprovalParty2_IT_L1").Value = ddlOBApprovalParty2_IT_L1.SelectedValue.ToString
                    End If


                    .Parameters.Add("@OBComparision_IT_L2", SqlDbType.NVarChar, 50)
                    If ddlOBComparision_IT_L2.SelectedValue.ToString = "Select One" Then
                        .Parameters("@OBComparision_IT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@OBComparision_IT_L2").Value = ddlOBComparision_IT_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBCurrencyCode_IT_L2", SqlDbType.BigInt)
                    If ddlOBCurrencyCode_IT_L2.SelectedValue.ToString = "0" Then
                        .Parameters("@OBCurrencyCode_IT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@OBCurrencyCode_IT_L2").Value = ddlOBCurrencyCode_IT_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBAmount_IT_L2", SqlDbType.Decimal, 18, 2)
                    If txtOBAmount_IT_L2.Text = "" Then
                        .Parameters("@OBAmount_IT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@OBAmount_IT_L2").Value = CDec(txtOBAmount_IT_L2.Text.ToString)
                    End If

                    .Parameters.Add("@OBApprovalParty_IT_L2", SqlDbType.VarChar, 6)
                    If ddlOBApprovalParty_IT_L2.SelectedValue.ToString = "" Then
                        .Parameters("@OBApprovalParty_IT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@OBApprovalParty_IT_L2").Value = ddlOBApprovalParty_IT_L2.SelectedValue.ToString
                    End If


                    .Parameters.Add("@OBComparision_IT_L3", SqlDbType.NVarChar, 50)
                    If ddlOBComparision_IT_L3.SelectedValue.ToString = "Select One" Then
                        .Parameters("@OBComparision_IT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@OBComparision_IT_L3").Value = ddlOBComparision_IT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBCurrencyCode_IT_L3", SqlDbType.BigInt)
                    If ddlOBCurrencyCode_IT_L3.SelectedValue.ToString = "0" Then
                        .Parameters("@OBCurrencyCode_IT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@OBCurrencyCode_IT_L3").Value = ddlOBCurrencyCode_IT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBAmount_IT_L3", SqlDbType.Decimal, 18, 2)
                    If txtOBAmount_IT_L3.Text = "" Then
                        .Parameters("@OBAmount_IT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@OBAmount_IT_L3").Value = CDec(txtOBAmount_IT_L3.Text.ToString)
                    End If

                    .Parameters.Add("@OBApprovalParty_IT_L3", SqlDbType.VarChar, 6)
                    If ddlOBApprovalParty_IT_L3.SelectedValue.ToString = "" Then
                        .Parameters("@OBApprovalParty_IT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@OBApprovalParty_IT_L3").Value = ddlOBApprovalParty_IT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBComparision_IT_L4", SqlDbType.NVarChar, 50)
                    If ddlOBComparision_IT_L4.SelectedValue.ToString = "Select One" Then
                        .Parameters("@OBComparision_IT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@OBComparision_IT_L4").Value = ddlOBComparision_IT_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBCurrencyCode_IT_L4", SqlDbType.BigInt)
                    If ddlOBCurrencyCode_IT_L4.SelectedValue.ToString = "0" Then
                        .Parameters("@OBCurrencyCode_IT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@OBCurrencyCode_IT_L4").Value = ddlOBCurrencyCode_IT_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBAmount_IT_L4", SqlDbType.Decimal, 18, 2)
                    If txtOBAmount_IT_L4.Text = "" Then
                        .Parameters("@OBAmount_IT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@OBAmount_IT_L4").Value = CDec(txtOBAmount_IT_L4.Text.ToString)
                    End If

                    .Parameters.Add("@OBApprovalParty_IT_L4", SqlDbType.VarChar, 6)
                    If ddlOBApprovalParty_IT_L4.SelectedValue.ToString = "" Then
                        .Parameters("@OBApprovalParty_IT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@OBApprovalParty_IT_L4").Value = ddlOBApprovalParty_IT_L4.SelectedValue.ToString
                    End If


                    .Parameters.Add("@OBComparision_NonIT_L1", SqlDbType.NVarChar, 50)
                    If ddlOBComparision_NonIT_L1.SelectedValue.ToString = "Select One" Then
                        .Parameters("@OBComparision_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@OBComparision_NonIT_L1").Value = ddlOBComparision_NonIT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBCurrencyCode_NonIT_L1", SqlDbType.BigInt)
                    If ddlOBCurrencyCode_NonIT_L1.SelectedValue.ToString = "0" Then
                        .Parameters("@OBCurrencyCode_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@OBCurrencyCode_NonIT_L1").Value = ddlOBCurrencyCode_NonIT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBAmount_NonIT_L1", SqlDbType.Decimal, 18, 2)
                    If txtOBAmount_NonIT_L1.Text = "" Then
                        .Parameters("@OBAmount_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@OBAmount_NonIT_L1").Value = CDec(txtOBAmount_NonIT_L1.Text.ToString)
                    End If

                    .Parameters.Add("@OBApprovalParty_NonIT_L1", SqlDbType.VarChar, 6)
                    If ddlOBApprovalParty_NonIT_L1.SelectedValue.ToString = "" Then
                        .Parameters("@OBApprovalParty_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@OBApprovalParty_NonIT_L1").Value = ddlOBApprovalParty_NonIT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBApprovalParty2_NonIT_L1", SqlDbType.VarChar, 6)
                    If ddlOBApprovalParty2_NonIT_L1.SelectedValue.ToString = "" Then
                        .Parameters("@OBApprovalParty2_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@OBApprovalParty2_NonIT_L1").Value = ddlOBApprovalParty2_NonIT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBComparision_NonIT_L2", SqlDbType.NVarChar, 50)
                    If ddlOBComparision_NonIT_L2.SelectedValue.ToString = "Select One" Then
                        .Parameters("@OBComparision_NonIT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@OBComparision_NonIT_L2").Value = ddlOBComparision_NonIT_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBCurrencyCode_NonIT_L2", SqlDbType.BigInt)
                    If ddlOBCurrencyCode_NonIT_L2.SelectedValue.ToString = "0" Then
                        .Parameters("@OBCurrencyCode_NonIT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@OBCurrencyCode_NonIT_L2").Value = ddlOBCurrencyCode_NonIT_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBAmount_NonIT_L2", SqlDbType.Decimal, 18, 2)
                    If txtOBAmount_NonIT_L2.Text = "" Then
                        .Parameters("@OBAmount_NonIT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@OBAmount_NonIT_L2").Value = CDec(txtOBAmount_NonIT_L2.Text.ToString)
                    End If

                    .Parameters.Add("@OBApprovalParty_NonIT_L2", SqlDbType.VarChar, 6)
                    If ddlOBApprovalParty_NonIT_L2.SelectedValue.ToString = "" Then
                        .Parameters("@OBApprovalParty_NonIT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@OBApprovalParty_NonIT_L2").Value = ddlOBApprovalParty_NonIT_L2.SelectedValue.ToString
                    End If


                    .Parameters.Add("@OBComparision_NonIT_L3", SqlDbType.NVarChar, 50)
                    If ddlOBComparision_NonIT_L3.SelectedValue.ToString = "Select One" Then
                        .Parameters("@OBComparision_NonIT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@OBComparision_NonIT_L3").Value = ddlOBComparision_NonIT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBCurrencyCode_NonIT_L3", SqlDbType.BigInt)
                    If ddlOBCurrencyCode_NonIT_L3.SelectedValue.ToString = "0" Then
                        .Parameters("@OBCurrencyCode_NonIT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@OBCurrencyCode_NonIT_L3").Value = ddlOBCurrencyCode_NonIT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBAmount_NonIT_L3", SqlDbType.Decimal, 18, 2)
                    If txtOBAmount_NonIT_L3.Text = "" Then
                        .Parameters("@OBAmount_NonIT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@OBAmount_NonIT_L3").Value = CDec(txtOBAmount_NonIT_L3.Text.ToString)
                    End If

                    .Parameters.Add("@OBApprovalParty_NonIT_L3", SqlDbType.VarChar, 6)
                    If ddlOBApprovalParty_NonIT_L3.SelectedValue.ToString = "" Then
                        .Parameters("@OBApprovalParty_NonIT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@OBApprovalParty_NonIT_L3").Value = ddlOBApprovalParty_NonIT_L3.SelectedValue.ToString
                    End If


                    .Parameters.Add("@OBComparision_NonIT_L4", SqlDbType.NVarChar, 50)
                    If ddlOBComparision_NonIT_L4.SelectedValue.ToString = "Select One" Then
                        .Parameters("@OBComparision_NonIT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@OBComparision_NonIT_L4").Value = ddlOBComparision_NonIT_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBCurrencyCode_NonIT_L4", SqlDbType.BigInt)
                    If ddlOBCurrencyCode_NonIT_L4.SelectedValue.ToString = "0" Then
                        .Parameters("@OBCurrencyCode_NonIT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@OBCurrencyCode_NonIT_L4").Value = ddlOBCurrencyCode_NonIT_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBAmount_NonIT_L4", SqlDbType.Decimal, 18, 2)
                    If txtOBAmount_NonIT_L4.Text = "" Then
                        .Parameters("@OBAmount_NonIT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@OBAmount_NonIT_L4").Value = CDec(txtOBAmount_NonIT_L4.Text.ToString)
                    End If

                    .Parameters.Add("@OBApprovalParty_NonIT_L4", SqlDbType.VarChar, 6)
                    If ddlOBApprovalParty_NonIT_L4.SelectedValue.ToString = "" Then
                        .Parameters("@OBApprovalParty_NonIT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@OBApprovalParty_NonIT_L4").Value = ddlOBApprovalParty_NonIT_L4.SelectedValue.ToString
                    End If

                    'NON IT (MAJOR)
                    .Parameters.Add("@OBComparision_NonITMajor_L1", SqlDbType.NVarChar, 50)
                    If ddlOBComparision_NonITMajor_L1.SelectedValue.ToString = "Select One" Then
                        .Parameters("@OBComparision_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@OBComparision_NonITMajor_L1").Value = ddlOBComparision_NonITMajor_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBCurrencyCode_NonITMajor_L1", SqlDbType.BigInt)
                    If ddlOBCurrencyCode_NonITMajor_L1.SelectedValue.ToString = "0" Then
                        .Parameters("@OBCurrencyCode_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@OBCurrencyCode_NonITMajor_L1").Value = ddlOBCurrencyCode_NonITMajor_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBAmount_NonITMajor_L1", SqlDbType.Decimal, 18, 2)
                    If txtOBAmount_NonITMajor_L1.Text = "" Then
                        .Parameters("@OBAmount_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@OBAmount_NonITMajor_L1").Value = CDec(txtOBAmount_NonITMajor_L1.Text.ToString)
                    End If

                    .Parameters.Add("@OBApprovalParty_NonITMajor_L1", SqlDbType.VarChar, 6)
                    If ddlOBApprovalParty_NonITMajor_L1.SelectedValue.ToString = "" Then
                        .Parameters("@OBApprovalParty_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@OBApprovalParty_NonITMajor_L1").Value = ddlOBApprovalParty_NonITMajor_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBApprovalParty2_NonITMajor_L1", SqlDbType.VarChar, 6)
                    If ddlOBApprovalParty2_NonITMajor_L1.SelectedValue.ToString = "" Then
                        .Parameters("@OBApprovalParty2_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@OBApprovalParty2_NonITMajor_L1").Value = ddlOBApprovalParty2_NonITMajor_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBComparision_NonITMajor_L2", SqlDbType.NVarChar, 50)
                    If ddlOBComparision_NonITMajor_L2.SelectedValue.ToString = "Select One" Then
                        .Parameters("@OBComparision_NonITMajor_L2").Value = DBNull.Value
                    Else
                        .Parameters("@OBComparision_NonITMajor_L2").Value = ddlOBComparision_NonITMajor_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBCurrencyCode_NonITMajor_L2", SqlDbType.BigInt)
                    If ddlOBCurrencyCode_NonITMajor_L2.SelectedValue.ToString = "0" Then
                        .Parameters("@OBCurrencyCode_NonITMajor_L2").Value = DBNull.Value
                    Else
                        .Parameters("@OBCurrencyCode_NonITMajor_L2").Value = ddlOBCurrencyCode_NonITMajor_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBAmount_NonITMajor_L2", SqlDbType.Decimal, 18, 2)
                    If txtOBAmount_NonITMajor_L2.Text = "" Then
                        .Parameters("@OBAmount_NonITMajor_L2").Value = DBNull.Value
                    Else
                        .Parameters("@OBAmount_NonITMajor_L2").Value = CDec(txtOBAmount_NonITMajor_L2.Text.ToString)
                    End If

                    .Parameters.Add("@OBApprovalParty_NonITMajor_L2", SqlDbType.VarChar, 6)
                    If ddlOBApprovalParty_NonITMajor_L2.SelectedValue.ToString = "" Then
                        .Parameters("@OBApprovalParty_NonITMajor_L2").Value = DBNull.Value
                    Else
                        .Parameters("@OBApprovalParty_NonITMajor_L2").Value = ddlOBApprovalParty_NonITMajor_L2.SelectedValue.ToString
                    End If


                    .Parameters.Add("@OBComparision_NonITMajor_L3", SqlDbType.NVarChar, 50)
                    If ddlOBComparision_NonITMajor_L3.SelectedValue.ToString = "Select One" Then
                        .Parameters("@OBComparision_NonITMajor_L3").Value = DBNull.Value
                    Else
                        .Parameters("@OBComparision_NonITMajor_L3").Value = ddlOBComparision_NonITMajor_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBCurrencyCode_NonITMajor_L3", SqlDbType.BigInt)
                    If ddlOBCurrencyCode_NonITMajor_L3.SelectedValue.ToString = "0" Then
                        .Parameters("@OBCurrencyCode_NonITMajor_L3").Value = DBNull.Value
                    Else
                        .Parameters("@OBCurrencyCode_NonITMajor_L3").Value = ddlOBCurrencyCode_NonITMajor_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBAmount_NonITMajor_L3", SqlDbType.Decimal, 18, 2)
                    If txtOBAmount_NonITMajor_L3.Text = "" Then
                        .Parameters("@OBAmount_NonITMajor_L3").Value = DBNull.Value
                    Else
                        .Parameters("@OBAmount_NonITMajor_L3").Value = CDec(txtOBAmount_NonITMajor_L3.Text.ToString)
                    End If

                    .Parameters.Add("@OBApprovalParty_NonITMajor_L3", SqlDbType.VarChar, 6)
                    If ddlOBApprovalParty_NonITMajor_L3.SelectedValue.ToString = "" Then
                        .Parameters("@OBApprovalParty_NonITMajor_L3").Value = DBNull.Value
                    Else
                        .Parameters("@OBApprovalParty_NonITMajor_L3").Value = ddlOBApprovalParty_NonITMajor_L3.SelectedValue.ToString
                    End If


                    .Parameters.Add("@OBComparision_NonITMajor_L4", SqlDbType.NVarChar, 50)
                    If ddlOBComparision_NonITMajor_L4.SelectedValue.ToString = "Select One" Then
                        .Parameters("@OBComparision_NonITMajor_L4").Value = DBNull.Value
                    Else
                        .Parameters("@OBComparision_NonITMajor_L4").Value = ddlOBComparision_NonITMajor_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBCurrencyCode_NonITMajor_L4", SqlDbType.BigInt)
                    If ddlOBCurrencyCode_NonITMajor_L4.SelectedValue.ToString = "0" Then
                        .Parameters("@OBCurrencyCode_NonITMajor_L4").Value = DBNull.Value
                    Else
                        .Parameters("@OBCurrencyCode_NonITMajor_L4").Value = ddlOBCurrencyCode_NonITMajor_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@OBAmount_NonITMajor_L4", SqlDbType.Decimal, 18, 2)
                    If txtOBAmount_NonITMajor_L4.Text = "" Then
                        .Parameters("@OBAmount_NonITMajor_L4").Value = DBNull.Value
                    Else
                        .Parameters("@OBAmount_NonITMajor_L4").Value = CDec(txtOBAmount_NonITMajor_L4.Text.ToString)
                    End If

                    .Parameters.Add("@OBApprovalParty_NonITMajor_L4", SqlDbType.VarChar, 6)
                    If ddlOBApprovalParty_NonITMajor_L4.SelectedValue.ToString = "" Then
                        .Parameters("@OBApprovalParty_NonITMajor_L4").Value = DBNull.Value
                    Else
                        .Parameters("@OBApprovalParty_NonITMajor_L4").Value = ddlOBApprovalParty_NonITMajor_L4.SelectedValue.ToString
                    End If


                    ''''


                    .Parameters.Add("@DWComparision_IT_L1", SqlDbType.NVarChar, 50)
                    If ddlDWComparision_IT_L1.SelectedValue.ToString = "Select One" Then
                        .Parameters("@DWComparision_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@DWComparision_IT_L1").Value = ddlDWComparision_IT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWCurrencyCode_IT_L1", SqlDbType.BigInt)
                    If ddlDWCurrencyCode_IT_L1.SelectedValue.ToString = "0" Then
                        .Parameters("@DWCurrencyCode_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@DWCurrencyCode_IT_L1").Value = ddlDWCurrencyCode_IT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWAmount_IT_L1", SqlDbType.Decimal, 18, 2)
                    If txtDWAmount_IT_L1.Text = "" Then
                        .Parameters("@DWAmount_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@DWAmount_IT_L1").Value = CDec(txtDWAmount_IT_L1.Text.ToString)
                    End If

                    .Parameters.Add("@DWApprovalParty_IT_L1", SqlDbType.VarChar, 6)
                    If ddlDWApprovalParty_IT_L1.SelectedValue.ToString = "" Then
                        .Parameters("@DWApprovalParty_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@DWApprovalParty_IT_L1").Value = ddlDWApprovalParty_IT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWApprovalParty2_IT_L1", SqlDbType.VarChar, 6)
                    If ddlDWApprovalParty2_IT_L1.SelectedValue.ToString = "" Then
                        .Parameters("@DWApprovalParty2_IT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@DWApprovalParty2_IT_L1").Value = ddlDWApprovalParty2_IT_L1.SelectedValue.ToString
                    End If


                    .Parameters.Add("@DWComparision_IT_L2", SqlDbType.NVarChar, 50)
                    If ddlDWComparision_IT_L2.SelectedValue.ToString = "Select One" Then
                        .Parameters("@DWComparision_IT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@DWComparision_IT_L2").Value = ddlDWComparision_IT_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWCurrencyCode_IT_L2", SqlDbType.BigInt)
                    If ddlDWCurrencyCode_IT_L2.SelectedValue.ToString = "0" Then
                        .Parameters("@DWCurrencyCode_IT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@DWCurrencyCode_IT_L2").Value = ddlDWCurrencyCode_IT_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWAmount_IT_L2", SqlDbType.Decimal, 18, 2)
                    If txtDWAmount_IT_L2.Text = "" Then
                        .Parameters("@DWAmount_IT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@DWAmount_IT_L2").Value = CDec(txtDWAmount_IT_L2.Text.ToString)
                    End If

                    .Parameters.Add("@DWApprovalParty_IT_L2", SqlDbType.VarChar, 6)
                    If ddlDWApprovalParty_IT_L2.SelectedValue.ToString = "" Then
                        .Parameters("@DWApprovalParty_IT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@DWApprovalParty_IT_L2").Value = ddlDWApprovalParty_IT_L2.SelectedValue.ToString
                    End If


                    .Parameters.Add("@DWComparision_IT_L3", SqlDbType.NVarChar, 50)
                    If ddlDWComparision_IT_L3.SelectedValue.ToString = "Select One" Then
                        .Parameters("@DWComparision_IT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@DWComparision_IT_L3").Value = ddlDWComparision_IT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWCurrencyCode_IT_L3", SqlDbType.BigInt)
                    If ddlDWCurrencyCode_IT_L3.SelectedValue.ToString = "0" Then
                        .Parameters("@DWCurrencyCode_IT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@DWCurrencyCode_IT_L3").Value = ddlDWCurrencyCode_IT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWAmount_IT_L3", SqlDbType.Decimal, 18, 2)
                    If txtDWAmount_IT_L3.Text = "" Then
                        .Parameters("@DWAmount_IT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@DWAmount_IT_L3").Value = CDec(txtDWAmount_IT_L3.Text.ToString)
                    End If

                    .Parameters.Add("@DWApprovalParty_IT_L3", SqlDbType.VarChar, 6)
                    If ddlDWApprovalParty_IT_L3.SelectedValue.ToString = "" Then
                        .Parameters("@DWApprovalParty_IT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@DWApprovalParty_IT_L3").Value = ddlDWApprovalParty_IT_L3.SelectedValue.ToString
                    End If


                    .Parameters.Add("@DWComparision_IT_L4", SqlDbType.NVarChar, 50)
                    If ddlDWComparision_IT_L4.SelectedValue.ToString = "Select One" Then
                        .Parameters("@DWComparision_IT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@DWComparision_IT_L4").Value = ddlDWComparision_IT_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWCurrencyCode_IT_L4", SqlDbType.BigInt)
                    If ddlDWCurrencyCode_IT_L4.SelectedValue.ToString = "0" Then
                        .Parameters("@DWCurrencyCode_IT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@DWCurrencyCode_IT_L4").Value = ddlDWCurrencyCode_IT_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWAmount_IT_L4", SqlDbType.Decimal, 18, 2)
                    If txtDWAmount_IT_L4.Text = "" Then
                        .Parameters("@DWAmount_IT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@DWAmount_IT_L4").Value = CDec(txtDWAmount_IT_L4.Text.ToString)
                    End If

                    .Parameters.Add("@DWApprovalParty_IT_L4", SqlDbType.VarChar, 6)
                    If ddlDWApprovalParty_IT_L4.SelectedValue.ToString = "" Then
                        .Parameters("@DWApprovalParty_IT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@DWApprovalParty_IT_L4").Value = ddlDWApprovalParty_IT_L4.SelectedValue.ToString
                    End If

                    ''
                    .Parameters.Add("@DWComparision_NonIT_L1", SqlDbType.NVarChar, 50)
                    If ddlDWComparision_NonIT_L1.SelectedValue.ToString = "Select One" Then
                        .Parameters("@DWComparision_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@DWComparision_NonIT_L1").Value = ddlDWComparision_NonIT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWCurrencyCode_NonIT_L1", SqlDbType.BigInt)
                    If ddlDWCurrencyCode_NonIT_L1.SelectedValue.ToString = "0" Then
                        .Parameters("@DWCurrencyCode_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@DWCurrencyCode_NonIT_L1").Value = ddlDWCurrencyCode_NonIT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWAmount_NonIT_L1", SqlDbType.Decimal, 18, 2)
                    If txtDWAmount_NonIT_L1.Text = "" Then
                        .Parameters("@DWAmount_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@DWAmount_NonIT_L1").Value = CDec(txtDWAmount_NonIT_L1.Text.ToString)
                    End If

                    .Parameters.Add("@DWApprovalParty_NonIT_L1", SqlDbType.VarChar, 6)
                    If ddlDWApprovalParty_NonIT_L1.SelectedValue.ToString = "" Then
                        .Parameters("@DWApprovalParty_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@DWApprovalParty_NonIT_L1").Value = ddlDWApprovalParty_NonIT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWApprovalParty2_NonIT_L1", SqlDbType.VarChar, 6)
                    If ddlDWApprovalParty2_NonIT_L1.SelectedValue.ToString = "" Then
                        .Parameters("@DWApprovalParty2_NonIT_L1").Value = DBNull.Value
                    Else
                        .Parameters("@DWApprovalParty2_NonIT_L1").Value = ddlDWApprovalParty2_NonIT_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWComparision_NonIT_L2", SqlDbType.NVarChar, 50)
                    If ddlDWComparision_NonIT_L2.SelectedValue.ToString = "Select One" Then
                        .Parameters("@DWComparision_NonIT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@DWComparision_NonIT_L2").Value = ddlDWComparision_NonIT_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWCurrencyCode_NonIT_L2", SqlDbType.BigInt)
                    If ddlDWCurrencyCode_NonIT_L2.SelectedValue.ToString = "0" Then
                        .Parameters("@DWCurrencyCode_NonIT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@DWCurrencyCode_NonIT_L2").Value = ddlDWCurrencyCode_NonIT_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWAmount_NonIT_L2", SqlDbType.Decimal, 18, 2)
                    If txtDWAmount_NonIT_L2.Text = "" Then
                        .Parameters("@DWAmount_NonIT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@DWAmount_NonIT_L2").Value = CDec(txtDWAmount_NonIT_L2.Text.ToString)
                    End If

                    .Parameters.Add("@DWApprovalParty_NonIT_L2", SqlDbType.VarChar, 6)
                    If ddlDWApprovalParty_NonIT_L2.SelectedValue.ToString = "" Then
                        .Parameters("@DWApprovalParty_NonIT_L2").Value = DBNull.Value
                    Else
                        .Parameters("@DWApprovalParty_NonIT_L2").Value = ddlDWApprovalParty_NonIT_L2.SelectedValue.ToString
                    End If


                    .Parameters.Add("@DWComparision_NonIT_L3", SqlDbType.NVarChar, 50)
                    If ddlDWComparision_NonIT_L3.SelectedValue.ToString = "Select One" Then
                        .Parameters("@DWComparision_NonIT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@DWComparision_NonIT_L3").Value = ddlDWComparision_NonIT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWCurrencyCode_NonIT_L3", SqlDbType.BigInt)
                    If ddlDWCurrencyCode_NonIT_L3.SelectedValue.ToString = "0" Then
                        .Parameters("@DWCurrencyCode_NonIT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@DWCurrencyCode_NonIT_L3").Value = ddlDWCurrencyCode_NonIT_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWAmount_NonIT_L3", SqlDbType.Decimal, 18, 2)
                    If txtDWAmount_NonIT_L3.Text = "" Then
                        .Parameters("@DWAmount_NonIT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@DWAmount_NonIT_L3").Value = CDec(txtDWAmount_NonIT_L3.Text.ToString)
                    End If

                    .Parameters.Add("@DWApprovalParty_NonIT_L3", SqlDbType.VarChar, 6)
                    If ddlDWApprovalParty_NonIT_L3.SelectedValue.ToString = "" Then
                        .Parameters("@DWApprovalParty_NonIT_L3").Value = DBNull.Value
                    Else
                        .Parameters("@DWApprovalParty_NonIT_L3").Value = ddlDWApprovalParty_NonIT_L3.SelectedValue.ToString
                    End If


                    .Parameters.Add("@DWComparision_NonIT_L4", SqlDbType.NVarChar, 50)
                    If ddlDWComparision_NonIT_L4.SelectedValue.ToString = "Select One" Then
                        .Parameters("@DWComparision_NonIT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@DWComparision_NonIT_L4").Value = ddlDWComparision_NonIT_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWCurrencyCode_NonIT_L4", SqlDbType.BigInt)
                    If ddlDWCurrencyCode_NonIT_L4.SelectedValue.ToString = "0" Then
                        .Parameters("@DWCurrencyCode_NonIT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@DWCurrencyCode_NonIT_L4").Value = ddlDWCurrencyCode_NonIT_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWAmount_NonIT_L4", SqlDbType.Decimal, 18, 2)
                    If txtDWAmount_NonIT_L4.Text = "" Then
                        .Parameters("@DWAmount_NonIT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@DWAmount_NonIT_L4").Value = CDec(txtDWAmount_NonIT_L4.Text.ToString)
                    End If

                    .Parameters.Add("@DWApprovalParty_NonIT_L4", SqlDbType.VarChar, 6)
                    If ddlDWApprovalParty_NonIT_L4.SelectedValue.ToString = "" Then
                        .Parameters("@DWApprovalParty_NonIT_L4").Value = DBNull.Value
                    Else
                        .Parameters("@DWApprovalParty_NonIT_L4").Value = ddlDWApprovalParty_NonIT_L4.SelectedValue.ToString
                    End If
                    ''

                    ''NON IT (MAJOR)
                    .Parameters.Add("@DWComparision_NonITMajor_L1", SqlDbType.NVarChar, 50)
                    If ddlDWComparision_NonITMajor_L1.SelectedValue.ToString = "Select One" Then
                        .Parameters("@DWComparision_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@DWComparision_NonITMajor_L1").Value = ddlDWComparision_NonITMajor_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWCurrencyCode_NonITMajor_L1", SqlDbType.BigInt)
                    If ddlDWCurrencyCode_NonITMajor_L1.SelectedValue.ToString = "0" Then
                        .Parameters("@DWCurrencyCode_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@DWCurrencyCode_NonITMajor_L1").Value = ddlDWCurrencyCode_NonITMajor_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWAmount_NonITMajor_L1", SqlDbType.Decimal, 18, 2)
                    If txtDWAmount_NonITMajor_L1.Text = "" Then
                        .Parameters("@DWAmount_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@DWAmount_NonITMajor_L1").Value = CDec(txtDWAmount_NonITMajor_L1.Text.ToString)
                    End If

                    .Parameters.Add("@DWApprovalParty_NonITMajor_L1", SqlDbType.VarChar, 6)
                    If ddlDWApprovalParty_NonITMajor_L1.SelectedValue.ToString = "" Then
                        .Parameters("@DWApprovalParty_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@DWApprovalParty_NonITMajor_L1").Value = ddlDWApprovalParty_NonITMajor_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWApprovalParty2_NonITMajor_L1", SqlDbType.VarChar, 6)
                    If ddlDWApprovalParty2_NonITMajor_L1.SelectedValue.ToString = "" Then
                        .Parameters("@DWApprovalParty2_NonITMajor_L1").Value = DBNull.Value
                    Else
                        .Parameters("@DWApprovalParty2_NonITMajor_L1").Value = ddlDWApprovalParty2_NonITMajor_L1.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWComparision_NonITMajor_L2", SqlDbType.NVarChar, 50)
                    If ddlDWComparision_NonITMajor_L2.SelectedValue.ToString = "Select One" Then
                        .Parameters("@DWComparision_NonITMajor_L2").Value = DBNull.Value
                    Else
                        .Parameters("@DWComparision_NonITMajor_L2").Value = ddlDWComparision_NonITMajor_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWCurrencyCode_NonITMajor_L2", SqlDbType.BigInt)
                    If ddlDWCurrencyCode_NonITMajor_L2.SelectedValue.ToString = "0" Then
                        .Parameters("@DWCurrencyCode_NonITMajor_L2").Value = DBNull.Value
                    Else
                        .Parameters("@DWCurrencyCode_NonITMajor_L2").Value = ddlDWCurrencyCode_NonITMajor_L2.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWAmount_NonITMajor_L2", SqlDbType.Decimal, 18, 2)
                    If txtDWAmount_NonITMajor_L2.Text = "" Then
                        .Parameters("@DWAmount_NonITMajor_L2").Value = DBNull.Value
                    Else
                        .Parameters("@DWAmount_NonITMajor_L2").Value = CDec(txtDWAmount_NonITMajor_L2.Text.ToString)
                    End If

                    .Parameters.Add("@DWApprovalParty_NonITMajor_L2", SqlDbType.VarChar, 6)
                    If ddlDWApprovalParty_NonITMajor_L2.SelectedValue.ToString = "" Then
                        .Parameters("@DWApprovalParty_NonITMajor_L2").Value = DBNull.Value
                    Else
                        .Parameters("@DWApprovalParty_NonITMajor_L2").Value = ddlDWApprovalParty_NonITMajor_L2.SelectedValue.ToString
                    End If


                    .Parameters.Add("@DWComparision_NonITMajor_L3", SqlDbType.NVarChar, 50)
                    If ddlDWComparision_NonITMajor_L3.SelectedValue.ToString = "Select One" Then
                        .Parameters("@DWComparision_NonITMajor_L3").Value = DBNull.Value
                    Else
                        .Parameters("@DWComparision_NonITMajor_L3").Value = ddlDWComparision_NonITMajor_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWCurrencyCode_NonITMajor_L3", SqlDbType.BigInt)
                    If ddlDWCurrencyCode_NonITMajor_L3.SelectedValue.ToString = "0" Then
                        .Parameters("@DWCurrencyCode_NonITMajor_L3").Value = DBNull.Value
                    Else
                        .Parameters("@DWCurrencyCode_NonITMajor_L3").Value = ddlDWCurrencyCode_NonITMajor_L3.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWAmount_NonITMajor_L3", SqlDbType.Decimal, 18, 2)
                    If txtDWAmount_NonITMajor_L3.Text = "" Then
                        .Parameters("@DWAmount_NonITMajor_L3").Value = DBNull.Value
                    Else
                        .Parameters("@DWAmount_NonITMajor_L3").Value = CDec(txtDWAmount_NonITMajor_L3.Text.ToString)
                    End If

                    .Parameters.Add("@DWApprovalParty_NonITMajor_L3", SqlDbType.VarChar, 6)
                    If ddlDWApprovalParty_NonITMajor_L3.SelectedValue.ToString = "" Then
                        .Parameters("@DWApprovalParty_NonITMajor_L3").Value = DBNull.Value
                    Else
                        .Parameters("@DWApprovalParty_NonITMajor_L3").Value = ddlDWApprovalParty_NonITMajor_L3.SelectedValue.ToString
                    End If


                    .Parameters.Add("@DWComparision_NonITMajor_L4", SqlDbType.NVarChar, 50)
                    If ddlDWComparision_NonITMajor_L4.SelectedValue.ToString = "Select One" Then
                        .Parameters("@DWComparision_NonITMajor_L4").Value = DBNull.Value
                    Else
                        .Parameters("@DWComparision_NonITMajor_L4").Value = ddlDWComparision_NonITMajor_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWCurrencyCode_NonITMajor_L4", SqlDbType.BigInt)
                    If ddlDWCurrencyCode_NonITMajor_L4.SelectedValue.ToString = "0" Then
                        .Parameters("@DWCurrencyCode_NonITMajor_L4").Value = DBNull.Value
                    Else
                        .Parameters("@DWCurrencyCode_NonITMajor_L4").Value = ddlDWCurrencyCode_NonITMajor_L4.SelectedValue.ToString
                    End If

                    .Parameters.Add("@DWAmount_NonITMajor_L4", SqlDbType.Decimal, 18, 2)
                    If txtDWAmount_NonITMajor_L4.Text = "" Then
                        .Parameters("@DWAmount_NonITMajor_L4").Value = DBNull.Value
                    Else
                        .Parameters("@DWAmount_NonITMajor_L4").Value = CDec(txtDWAmount_NonITMajor_L4.Text.ToString)
                    End If

                    .Parameters.Add("@DWApprovalParty_NonITMajor_L4", SqlDbType.VarChar, 6)
                    If ddlDWApprovalParty_NonITMajor_L4.SelectedValue.ToString = "" Then
                        .Parameters("@DWApprovalParty_NonITMajor_L4").Value = DBNull.Value
                    Else
                        .Parameters("@DWApprovalParty_NonITMajor_L4").Value = ddlDWApprovalParty_NonITMajor_L4.SelectedValue.ToString
                    End If



                    .Parameters.Add("@CurrencyCode", SqlDbType.BigInt)
                    If ddlCurrencyCode.SelectedValue.ToString = "0" Then
                        .Parameters("@CurrencyCode").Value = DBNull.Value
                    Else
                        .Parameters("@CurrencyCode").Value = CInt(ddlCurrencyCode.SelectedValue.ToString)
                    End If

                    .Parameters.Add("@QuoPricePoint", SqlDbType.Decimal, 18, 2)
                    .Parameters("@QuoPricePoint").Value = CDec(txtQuoPricePoint.Text.ToString)
                    .Parameters.Add("@eReqPricePoint", SqlDbType.Decimal, 18, 2)
                    .Parameters("@eReqPricePoint").Value = CDec(txteReqPricePoint.Text.ToString)
                    .Parameters.Add("@FAPricePoint", SqlDbType.Decimal, 18, 2)
                    .Parameters("@FAPricePoint").Value = CDec(txtFAPricePoint.Text.ToString)
                    .Parameters.Add("@CARPricePoint", SqlDbType.Decimal, 18, 2)
                    .Parameters("@CARPricePoint").Value = CDec(txtCARPricePoint.Text.ToString)

                    .Parameters.Add("@AutoGeneratePO", SqlDbType.TinyInt)
                    If chkAutoPONo.Checked = True Then
                        .Parameters("@AutoGeneratePO").Value = 1
                    Else
                        .Parameters("@AutoGeneratePO").Value = 0
                    End If


                    .Parameters.Add("@eReq_1st", SqlDbType.VarChar, 6)
                    If ddleReq_1st.SelectedValue.ToString = "" Then
                        .Parameters("@eReq_1st").Value = DBNull.Value
                    Else
                        .Parameters("@eReq_1st").Value = ddleReq_1st.SelectedValue.ToString
                    End If

                    .Parameters.Add("@eReq_2nd", SqlDbType.VarChar, 6)
                    If ddleReq_2nd.SelectedValue.ToString = "" Then
                        .Parameters("@eReq_2nd").Value = DBNull.Value
                    Else
                        .Parameters("@eReq_2nd").Value = ddleReq_2nd.SelectedValue.ToString
                    End If

                    .Parameters.Add("@eReq_3rd", SqlDbType.VarChar, 6)
                    If ddleReq_3rd.SelectedValue.ToString = "" Then
                        .Parameters("@eReq_3rd").Value = DBNull.Value
                    Else
                        .Parameters("@eReq_3rd").Value = ddleReq_3rd.SelectedValue.ToString
                    End If

                    .Parameters.Add("@IssuePO_1st", SqlDbType.VarChar, 6)
                    If ddlIssuePO_1st.SelectedValue.ToString = "" Then
                        .Parameters("@IssuePO_1st").Value = DBNull.Value
                    Else
                        .Parameters("@IssuePO_1st").Value = ddlIssuePO_1st.SelectedValue.ToString
                    End If

                    .Parameters.Add("@IssuePO_2nd", SqlDbType.VarChar, 6)
                    If ddlIssuePO_2nd.SelectedValue.ToString = "" Then
                        .Parameters("@IssuePO_2nd").Value = DBNull.Value
                    Else
                        .Parameters("@IssuePO_2nd").Value = ddlIssuePO_2nd.SelectedValue.ToString
                    End If

                    .Parameters.Add("@IssuePO_3rd", SqlDbType.VarChar, 6)
                    If ddlIssuePO_3rd.SelectedValue.ToString = "" Then
                        .Parameters("@IssuePO_3rd").Value = DBNull.Value
                    Else
                        .Parameters("@IssuePO_3rd").Value = ddlIssuePO_3rd.SelectedValue.ToString
                    End If

                    .Parameters.Add("@IssuePayment_1st", SqlDbType.VarChar, 6)
                    If ddlIssuePayment_1st.SelectedValue.ToString = "" Then
                        .Parameters("@IssuePayment_1st").Value = DBNull.Value
                    Else
                        .Parameters("@IssuePayment_1st").Value = ddlIssuePayment_1st.SelectedValue.ToString
                    End If

                    .Parameters.Add("@IssuePayment_2nd", SqlDbType.VarChar, 6)
                    If ddlIssuePayment_2nd.SelectedValue.ToString = "" Then
                        .Parameters("@IssuePayment_2nd").Value = DBNull.Value
                    Else
                        .Parameters("@IssuePayment_2nd").Value = ddlIssuePayment_2nd.SelectedValue.ToString
                    End If

                    .Parameters.Add("@IssuePayment_3rd", SqlDbType.VarChar, 6)
                    If ddlIssuePayment_3rd.SelectedValue.ToString = "" Then
                        .Parameters("@IssuePayment_3rd").Value = DBNull.Value
                    Else
                        .Parameters("@IssuePayment_3rd").Value = ddlIssuePayment_3rd.SelectedValue.ToString
                    End If

                    'post efms
                    .Parameters.Add("@eFMS_1st", SqlDbType.VarChar, 6)
                    If ddleFMS_1st.SelectedValue.ToString = "" Then
                        .Parameters("@eFMS_1st").Value = DBNull.Value
                    Else
                        .Parameters("@eFMS_1st").Value = ddleFMS_1st.SelectedValue.ToString
                    End If

                    .Parameters.Add("@eFMS_2nd", SqlDbType.VarChar, 6)
                    If ddleFMS_2nd.SelectedValue.ToString = "" Then
                        .Parameters("@eFMS_2nd").Value = DBNull.Value
                    Else
                        .Parameters("@eFMS_2nd").Value = ddleFMS_2nd.SelectedValue.ToString
                    End If

                    .Parameters.Add("@eFMS_3rd", SqlDbType.VarChar, 6)
                    If ddleFMS_3rd.SelectedValue.ToString = "" Then
                        .Parameters("@eFMS_3rd").Value = DBNull.Value
                    Else
                        .Parameters("@eFMS_3rd").Value = ddleFMS_3rd.SelectedValue.ToString
                    End If

                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    'lblMsg.Text = "Save Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Public Function ValidateData() As Boolean
        Dim blnResult As Boolean = True

        lblMsg_Location.Text = ""
        lblMsg_CurrencyCode.Text = ""
        lblMsg_Quotation.Text = ""
        lblMsg_eReq.Text = ""
        lblMsg_FA.Text = ""
        lblMsg_MailNotify.Text = ""

        lblMsg_Comparision_IT_L1.Visible = False
        lblMsg_Comparision_IT_L1.Text = ""
        lblMsg_CurrencyCode_IT_L1.Visible = False
        lblMsg_CurrencyCode_IT_L1.Text = ""
        lblMsg_Amount_IT_L1.Visible = False
        lblMsg_Amount_IT_L1.Text = ""
        lblMsg_ApprovalParty_IT_L1.Visible = False
        lblMsg_ApprovalParty_IT_L1.Text = ""

        lblMsg_Comparision_IT_L2.Visible = False
        lblMsg_Comparision_IT_L2.Text = ""
        lblMsg_CurrencyCode_IT_L2.Visible = False
        lblMsg_CurrencyCode_IT_L2.Text = ""
        lblMsg_Amount_IT_L2.Visible = False
        lblMsg_Amount_IT_L2.Text = ""
        lblMsg_ApprovalParty_IT_L2.Visible = False
        lblMsg_ApprovalParty_IT_L2.Text = ""

        lblMsg_Comparision_IT_L3.Visible = False
        lblMsg_Comparision_IT_L3.Text = ""
        lblMsg_CurrencyCode_IT_L3.Visible = False
        lblMsg_CurrencyCode_IT_L3.Text = ""
        lblMsg_Amount_IT_L3.Visible = False
        lblMsg_Amount_IT_L3.Text = ""
        lblMsg_ApprovalParty_IT_L3.Visible = False
        lblMsg_ApprovalParty_IT_L3.Text = ""

        lblMsg_Comparision_IT_L4.Visible = False
        lblMsg_Comparision_IT_L4.Text = ""
        lblMsg_CurrencyCode_IT_L4.Visible = False
        lblMsg_CurrencyCode_IT_L4.Text = ""
        lblMsg_Amount_IT_L4.Visible = False
        lblMsg_Amount_IT_L4.Text = ""
        lblMsg_ApprovalParty_IT_L4.Visible = False
        lblMsg_ApprovalParty_IT_L4.Text = ""

        lblMsg_Comparision_NonIT_L1.Visible = False
        lblMsg_Comparision_NonIT_L1.Text = ""
        lblMsg_CurrencyCode_NonIT_L1.Visible = False
        lblMsg_CurrencyCode_NonIT_L1.Text = ""
        lblMsg_Amount_NonIT_L1.Visible = False
        lblMsg_Amount_NonIT_L1.Text = ""
        lblMsg_ApprovalParty_NonIT_L1.Visible = False
        lblMsg_ApprovalParty_NonIT_L1.Text = ""

        lblMsg_Comparision_NonIT_L2.Visible = False
        lblMsg_Comparision_NonIT_L2.Text = ""
        lblMsg_CurrencyCode_NonIT_L2.Visible = False
        lblMsg_CurrencyCode_NonIT_L2.Text = ""
        lblMsg_Amount_NonIT_L2.Visible = False
        lblMsg_Amount_NonIT_L2.Text = ""
        lblMsg_ApprovalParty_NonIT_L2.Visible = False
        lblMsg_ApprovalParty_NonIT_L2.Text = ""

        lblMsg_Comparision_NonIT_L3.Visible = False
        lblMsg_Comparision_NonIT_L3.Text = ""
        lblMsg_CurrencyCode_NonIT_L3.Visible = False
        lblMsg_CurrencyCode_NonIT_L3.Text = ""
        lblMsg_Amount_NonIT_L3.Visible = False
        lblMsg_Amount_NonIT_L3.Text = ""
        lblMsg_ApprovalParty_NonIT_L3.Visible = False
        lblMsg_ApprovalParty_NonIT_L3.Text = ""

        lblMsg_Comparision_NonIT_L4.Visible = False
        lblMsg_Comparision_NonIT_L4.Text = ""
        lblMsg_CurrencyCode_NonIT_L4.Visible = False
        lblMsg_CurrencyCode_NonIT_L4.Text = ""
        lblMsg_Amount_NonIT_L4.Visible = False
        lblMsg_Amount_NonIT_L4.Text = ""
        lblMsg_ApprovalParty_NonIT_L4.Visible = False
        lblMsg_ApprovalParty_NonIT_L4.Text = ""

        'NON IT (MAJOR)
        lblMsg_Comparision_NonITMajor_L1.Visible = False
        lblMsg_Comparision_NonITMajor_L1.Text = ""
        lblMsg_CurrencyCode_NonITMajor_L1.Visible = False
        lblMsg_CurrencyCode_NonITMajor_L1.Text = ""
        lblMsg_Amount_NonITMajor_L1.Visible = False
        lblMsg_Amount_NonITMajor_L1.Text = ""
        lblMsg_ApprovalParty_NonITMajor_L1.Visible = False
        lblMsg_ApprovalParty_NonITMajor_L1.Text = ""

        lblMsg_Comparision_NonITMajor_L2.Visible = False
        lblMsg_Comparision_NonITMajor_L2.Text = ""
        lblMsg_CurrencyCode_NonITMajor_L2.Visible = False
        lblMsg_CurrencyCode_NonITMajor_L2.Text = ""
        lblMsg_Amount_NonITMajor_L2.Visible = False
        lblMsg_Amount_NonITMajor_L2.Text = ""
        lblMsg_ApprovalParty_NonITMajor_L2.Visible = False
        lblMsg_ApprovalParty_NonITMajor_L2.Text = ""

        lblMsg_Comparision_NonITMajor_L3.Visible = False
        lblMsg_Comparision_NonITMajor_L3.Text = ""
        lblMsg_CurrencyCode_NonITMajor_L3.Visible = False
        lblMsg_CurrencyCode_NonITMajor_L3.Text = ""
        lblMsg_Amount_NonITMajor_L3.Visible = False
        lblMsg_Amount_NonITMajor_L3.Text = ""
        lblMsg_ApprovalParty_NonITMajor_L3.Visible = False
        lblMsg_ApprovalParty_NonITMajor_L3.Text = ""

        lblMsg_Comparision_NonITMajor_L4.Visible = False
        lblMsg_Comparision_NonITMajor_L4.Text = ""
        lblMsg_CurrencyCode_NonITMajor_L4.Visible = False
        lblMsg_CurrencyCode_NonITMajor_L4.Text = ""
        lblMsg_Amount_NonITMajor_L4.Visible = False
        lblMsg_Amount_NonITMajor_L4.Text = ""
        lblMsg_ApprovalParty_NonITMajor_L4.Visible = False
        lblMsg_ApprovalParty_NonITMajor_L4.Text = ""


        lblMsg_UBComparision_IT_L1.Visible = False
        lblMsg_UBComparision_IT_L1.Text = ""
        lblMsg_UBCurrencyCode_IT_L1.Visible = False
        lblMsg_UBCurrencyCode_IT_L1.Text = ""
        lblMsg_UBAmount_IT_L1.Visible = False
        lblMsg_UBAmount_IT_L1.Text = ""
        lblMsg_UBApprovalParty_IT_L1.Visible = False
        lblMsg_UBApprovalParty_IT_L1.Text = ""

        lblMsg_UBComparision_IT_L2.Visible = False
        lblMsg_UBComparision_IT_L2.Text = ""
        lblMsg_UBCurrencyCode_IT_L2.Visible = False
        lblMsg_UBCurrencyCode_IT_L2.Text = ""
        lblMsg_UBAmount_IT_L2.Visible = False
        lblMsg_UBAmount_IT_L2.Text = ""
        lblMsg_UBApprovalParty_IT_L2.Visible = False
        lblMsg_UBApprovalParty_IT_L2.Text = ""

        lblMsg_UBComparision_IT_L3.Visible = False
        lblMsg_UBComparision_IT_L3.Text = ""
        lblMsg_UBCurrencyCode_IT_L3.Visible = False
        lblMsg_UBCurrencyCode_IT_L3.Text = ""
        lblMsg_UBAmount_IT_L3.Visible = False
        lblMsg_UBAmount_IT_L3.Text = ""
        lblMsg_UBApprovalParty_IT_L3.Visible = False
        lblMsg_UBApprovalParty_IT_L3.Text = ""

        lblMsg_UBComparision_IT_L4.Visible = False
        lblMsg_UBComparision_IT_L4.Text = ""
        lblMsg_UBCurrencyCode_IT_L4.Visible = False
        lblMsg_UBCurrencyCode_IT_L4.Text = ""
        lblMsg_UBAmount_IT_L4.Visible = False
        lblMsg_UBAmount_IT_L4.Text = ""
        lblMsg_UBApprovalParty_IT_L4.Visible = False
        lblMsg_UBApprovalParty_IT_L4.Text = ""

        lblMsg_UBComparision_NonIT_L1.Visible = False
        lblMsg_UBComparision_NonIT_L1.Text = ""
        lblMsg_UBCurrencyCode_NonIT_L1.Visible = False
        lblMsg_UBCurrencyCode_NonIT_L1.Text = ""
        lblMsg_UBAmount_NonIT_L1.Visible = False
        lblMsg_UBAmount_NonIT_L1.Text = ""
        lblMsg_UBApprovalParty_NonIT_L1.Visible = False
        lblMsg_UBApprovalParty_NonIT_L1.Text = ""

        lblMsg_UBComparision_NonIT_L2.Visible = False
        lblMsg_UBComparision_NonIT_L2.Text = ""
        lblMsg_UBCurrencyCode_NonIT_L2.Visible = False
        lblMsg_UBCurrencyCode_NonIT_L2.Text = ""
        lblMsg_UBAmount_NonIT_L2.Visible = False
        lblMsg_UBAmount_NonIT_L2.Text = ""
        lblMsg_UBApprovalParty_NonIT_L2.Visible = False
        lblMsg_UBApprovalParty_NonIT_L2.Text = ""

        lblMsg_UBComparision_NonIT_L3.Visible = False
        lblMsg_UBComparision_NonIT_L3.Text = ""
        lblMsg_UBCurrencyCode_NonIT_L3.Visible = False
        lblMsg_UBCurrencyCode_NonIT_L3.Text = ""
        lblMsg_UBAmount_NonIT_L3.Visible = False
        lblMsg_UBAmount_NonIT_L3.Text = ""
        lblMsg_UBApprovalParty_NonIT_L3.Visible = False
        lblMsg_UBApprovalParty_NonIT_L3.Text = ""

        lblMsg_UBComparision_NonIT_L4.Visible = False
        lblMsg_UBComparision_NonIT_L4.Text = ""
        lblMsg_UBCurrencyCode_NonIT_L4.Visible = False
        lblMsg_UBCurrencyCode_NonIT_L4.Text = ""
        lblMsg_UBAmount_NonIT_L4.Visible = False
        lblMsg_UBAmount_NonIT_L4.Text = ""
        lblMsg_UBApprovalParty_NonIT_L4.Visible = False
        lblMsg_UBApprovalParty_NonIT_L4.Text = ""


        'NON IT (MAJOR)
        lblMsg_UBComparision_NonITMajor_L1.Visible = False
        lblMsg_UBComparision_NonITMajor_L1.Text = ""
        lblMsg_UBCurrencyCode_NonITMajor_L1.Visible = False
        lblMsg_UBCurrencyCode_NonITMajor_L1.Text = ""
        lblMsg_UBAmount_NonITMajor_L1.Visible = False
        lblMsg_UBAmount_NonITMajor_L1.Text = ""
        lblMsg_UBApprovalParty_NonITMajor_L1.Visible = False
        lblMsg_UBApprovalParty_NonITMajor_L1.Text = ""

        lblMsg_UBComparision_NonITMajor_L2.Visible = False
        lblMsg_UBComparision_NonITMajor_L2.Text = ""
        lblMsg_UBCurrencyCode_NonITMajor_L2.Visible = False
        lblMsg_UBCurrencyCode_NonITMajor_L2.Text = ""
        lblMsg_UBAmount_NonITMajor_L2.Visible = False
        lblMsg_UBAmount_NonITMajor_L2.Text = ""
        lblMsg_UBApprovalParty_NonITMajor_L2.Visible = False
        lblMsg_UBApprovalParty_NonITMajor_L2.Text = ""

        lblMsg_UBComparision_NonITMajor_L3.Visible = False
        lblMsg_UBComparision_NonITMajor_L3.Text = ""
        lblMsg_UBCurrencyCode_NonITMajor_L3.Visible = False
        lblMsg_UBCurrencyCode_NonITMajor_L3.Text = ""
        lblMsg_UBAmount_NonITMajor_L3.Visible = False
        lblMsg_UBAmount_NonITMajor_L3.Text = ""
        lblMsg_UBApprovalParty_NonITMajor_L3.Visible = False
        lblMsg_UBApprovalParty_NonITMajor_L3.Text = ""

        lblMsg_UBComparision_NonITMajor_L4.Visible = False
        lblMsg_UBComparision_NonITMajor_L4.Text = ""
        lblMsg_UBCurrencyCode_NonITMajor_L4.Visible = False
        lblMsg_UBCurrencyCode_NonITMajor_L4.Text = ""
        lblMsg_UBAmount_NonITMajor_L4.Visible = False
        lblMsg_UBAmount_NonITMajor_L4.Text = ""
        lblMsg_UBApprovalParty_NonITMajor_L4.Visible = False
        lblMsg_UBApprovalParty_NonITMajor_L4.Text = ""

        lblMsg_OBComparision_IT_L1.Visible = False
        lblMsg_OBComparision_IT_L1.Text = ""
        lblMsg_OBCurrencyCode_IT_L1.Visible = False
        lblMsg_OBCurrencyCode_IT_L1.Text = ""
        lblMsg_OBAmount_IT_L1.Visible = False
        lblMsg_OBAmount_IT_L1.Text = ""
        lblMsg_OBApprovalParty_IT_L1.Visible = False
        lblMsg_OBApprovalParty_IT_L1.Text = ""

        lblMsg_OBComparision_IT_L2.Visible = False
        lblMsg_OBComparision_IT_L2.Text = ""
        lblMsg_OBCurrencyCode_IT_L2.Visible = False
        lblMsg_OBCurrencyCode_IT_L2.Text = ""
        lblMsg_OBAmount_IT_L2.Visible = False
        lblMsg_OBAmount_IT_L2.Text = ""
        lblMsg_OBApprovalParty_IT_L2.Visible = False
        lblMsg_OBApprovalParty_IT_L2.Text = ""

        lblMsg_OBComparision_IT_L3.Visible = False
        lblMsg_OBComparision_IT_L3.Text = ""
        lblMsg_OBCurrencyCode_IT_L3.Visible = False
        lblMsg_OBCurrencyCode_IT_L3.Text = ""
        lblMsg_OBAmount_IT_L3.Visible = False
        lblMsg_OBAmount_IT_L3.Text = ""
        lblMsg_OBApprovalParty_IT_L3.Visible = False
        lblMsg_OBApprovalParty_IT_L3.Text = ""

        lblMsg_OBComparision_IT_L4.Visible = False
        lblMsg_OBComparision_IT_L4.Text = ""
        lblMsg_OBCurrencyCode_IT_L4.Visible = False
        lblMsg_OBCurrencyCode_IT_L4.Text = ""
        lblMsg_OBAmount_IT_L4.Visible = False
        lblMsg_OBAmount_IT_L4.Text = ""
        lblMsg_OBApprovalParty_IT_L4.Visible = False
        lblMsg_OBApprovalParty_IT_L4.Text = ""

        lblMsg_OBComparision_NonIT_L1.Visible = False
        lblMsg_OBComparision_NonIT_L1.Text = ""
        lblMsg_OBCurrencyCode_NonIT_L1.Visible = False
        lblMsg_OBCurrencyCode_NonIT_L1.Text = ""
        lblMsg_OBAmount_NonIT_L1.Visible = False
        lblMsg_OBAmount_NonIT_L1.Text = ""
        lblMsg_OBApprovalParty_NonIT_L1.Visible = False
        lblMsg_OBApprovalParty_NonIT_L1.Text = ""

        lblMsg_OBComparision_NonIT_L2.Visible = False
        lblMsg_OBComparision_NonIT_L2.Text = ""
        lblMsg_OBCurrencyCode_NonIT_L2.Visible = False
        lblMsg_OBCurrencyCode_NonIT_L2.Text = ""
        lblMsg_OBAmount_NonIT_L2.Visible = False
        lblMsg_OBAmount_NonIT_L2.Text = ""
        lblMsg_OBApprovalParty_NonIT_L2.Visible = False
        lblMsg_OBApprovalParty_NonIT_L2.Text = ""

        lblMsg_OBComparision_NonIT_L3.Visible = False
        lblMsg_OBComparision_NonIT_L3.Text = ""
        lblMsg_OBCurrencyCode_NonIT_L3.Visible = False
        lblMsg_OBCurrencyCode_NonIT_L3.Text = ""
        lblMsg_OBAmount_NonIT_L3.Visible = False
        lblMsg_OBAmount_NonIT_L3.Text = ""
        lblMsg_OBApprovalParty_NonIT_L3.Visible = False
        lblMsg_OBApprovalParty_NonIT_L3.Text = ""

        lblMsg_OBComparision_NonIT_L4.Visible = False
        lblMsg_OBComparision_NonIT_L4.Text = ""
        lblMsg_OBCurrencyCode_NonIT_L4.Visible = False
        lblMsg_OBCurrencyCode_NonIT_L4.Text = ""
        lblMsg_OBAmount_NonIT_L4.Visible = False
        lblMsg_OBAmount_NonIT_L4.Text = ""
        lblMsg_OBApprovalParty_NonIT_L4.Visible = False
        lblMsg_OBApprovalParty_NonIT_L4.Text = ""

        'NON IT (MAJOR)
        lblMsg_OBComparision_NonITMajor_L1.Visible = False
        lblMsg_OBComparision_NonITMajor_L1.Text = ""
        lblMsg_OBCurrencyCode_NonITMajor_L1.Visible = False
        lblMsg_OBCurrencyCode_NonITMajor_L1.Text = ""
        lblMsg_OBAmount_NonITMajor_L1.Visible = False
        lblMsg_OBAmount_NonITMajor_L1.Text = ""
        lblMsg_OBApprovalParty_NonITMajor_L1.Visible = False
        lblMsg_OBApprovalParty_NonITMajor_L1.Text = ""

        lblMsg_OBComparision_NonITMajor_L2.Visible = False
        lblMsg_OBComparision_NonITMajor_L2.Text = ""
        lblMsg_OBCurrencyCode_NonITMajor_L2.Visible = False
        lblMsg_OBCurrencyCode_NonITMajor_L2.Text = ""
        lblMsg_OBAmount_NonITMajor_L2.Visible = False
        lblMsg_OBAmount_NonITMajor_L2.Text = ""
        lblMsg_OBApprovalParty_NonITMajor_L2.Visible = False
        lblMsg_OBApprovalParty_NonITMajor_L2.Text = ""

        lblMsg_OBComparision_NonITMajor_L3.Visible = False
        lblMsg_OBComparision_NonITMajor_L3.Text = ""
        lblMsg_OBCurrencyCode_NonITMajor_L3.Visible = False
        lblMsg_OBCurrencyCode_NonITMajor_L3.Text = ""
        lblMsg_OBAmount_NonITMajor_L3.Visible = False
        lblMsg_OBAmount_NonITMajor_L3.Text = ""
        lblMsg_OBApprovalParty_NonITMajor_L3.Visible = False
        lblMsg_OBApprovalParty_NonITMajor_L3.Text = ""

        lblMsg_OBComparision_NonITMajor_L4.Visible = False
        lblMsg_OBComparision_NonITMajor_L4.Text = ""
        lblMsg_OBCurrencyCode_NonITMajor_L4.Visible = False
        lblMsg_OBCurrencyCode_NonITMajor_L4.Text = ""
        lblMsg_OBAmount_NonITMajor_L4.Visible = False
        lblMsg_OBAmount_NonITMajor_L4.Text = ""
        lblMsg_OBApprovalParty_NonITMajor_L4.Visible = False
        lblMsg_OBApprovalParty_NonITMajor_L4.Text = ""


        lblMsg_DWComparision_IT_L1.Visible = False
        lblMsg_DWComparision_IT_L1.Text = ""
        lblMsg_DWCurrencyCode_IT_L1.Visible = False
        lblMsg_DWCurrencyCode_IT_L1.Text = ""
        lblMsg_DWAmount_IT_L1.Visible = False
        lblMsg_DWAmount_IT_L1.Text = ""
        lblMsg_DWApprovalParty_IT_L1.Visible = False
        lblMsg_DWApprovalParty_IT_L1.Text = ""

        lblMsg_DWComparision_IT_L2.Visible = False
        lblMsg_DWComparision_IT_L2.Text = ""
        lblMsg_DWCurrencyCode_IT_L2.Visible = False
        lblMsg_DWCurrencyCode_IT_L2.Text = ""
        lblMsg_DWAmount_IT_L2.Visible = False
        lblMsg_DWAmount_IT_L2.Text = ""
        lblMsg_DWApprovalParty_IT_L2.Visible = False
        lblMsg_DWApprovalParty_IT_L2.Text = ""

        lblMsg_DWComparision_IT_L3.Visible = False
        lblMsg_DWComparision_IT_L3.Text = ""
        lblMsg_DWCurrencyCode_IT_L3.Visible = False
        lblMsg_DWCurrencyCode_IT_L3.Text = ""
        lblMsg_DWAmount_IT_L3.Visible = False
        lblMsg_DWAmount_IT_L3.Text = ""
        lblMsg_DWApprovalParty_IT_L3.Visible = False
        lblMsg_DWApprovalParty_IT_L3.Text = ""

        lblMsg_DWComparision_IT_L4.Visible = False
        lblMsg_DWComparision_IT_L4.Text = ""
        lblMsg_DWCurrencyCode_IT_L4.Visible = False
        lblMsg_DWCurrencyCode_IT_L4.Text = ""
        lblMsg_DWAmount_IT_L4.Visible = False
        lblMsg_DWAmount_IT_L4.Text = ""
        lblMsg_DWApprovalParty_IT_L4.Visible = False
        lblMsg_DWApprovalParty_IT_L4.Text = ""


        '' 
        lblMsg_DWComparision_NonIT_L1.Visible = False
        lblMsg_DWComparision_NonIT_L1.Text = ""
        lblMsg_DWCurrencyCode_NonIT_L1.Visible = False
        lblMsg_DWCurrencyCode_NonIT_L1.Text = ""
        lblMsg_DWAmount_NonIT_L1.Visible = False
        lblMsg_DWAmount_NonIT_L1.Text = ""
        lblMsg_DWApprovalParty_NonIT_L1.Visible = False
        lblMsg_DWApprovalParty_NonIT_L1.Text = ""

        lblMsg_DWComparision_NonIT_L2.Visible = False
        lblMsg_DWComparision_NonIT_L2.Text = ""
        lblMsg_DWCurrencyCode_NonIT_L2.Visible = False
        lblMsg_DWCurrencyCode_NonIT_L2.Text = ""
        lblMsg_DWAmount_NonIT_L2.Visible = False
        lblMsg_DWAmount_NonIT_L2.Text = ""
        lblMsg_DWApprovalParty_NonIT_L2.Visible = False
        lblMsg_DWApprovalParty_NonIT_L2.Text = ""

        lblMsg_DWComparision_NonIT_L3.Visible = False
        lblMsg_DWComparision_NonIT_L3.Text = ""
        lblMsg_DWCurrencyCode_NonIT_L3.Visible = False
        lblMsg_DWCurrencyCode_NonIT_L3.Text = ""
        lblMsg_DWAmount_NonIT_L3.Visible = False
        lblMsg_DWAmount_NonIT_L3.Text = ""
        lblMsg_DWApprovalParty_NonIT_L3.Visible = False
        lblMsg_DWApprovalParty_NonIT_L3.Text = ""

        lblMsg_DWComparision_NonIT_L4.Visible = False
        lblMsg_DWComparision_NonIT_L4.Text = ""
        lblMsg_DWCurrencyCode_NonIT_L4.Visible = False
        lblMsg_DWCurrencyCode_NonIT_L4.Text = ""
        lblMsg_DWAmount_NonIT_L4.Visible = False
        lblMsg_DWAmount_NonIT_L4.Text = ""
        lblMsg_DWApprovalParty_NonIT_L4.Visible = False
        lblMsg_DWApprovalParty_NonIT_L4.Text = ""
        ''

        'NON IT (MAJOR)
        lblMsg_DWComparision_NonITMajor_L1.Visible = False
        lblMsg_DWComparision_NonITMajor_L1.Text = ""
        lblMsg_DWCurrencyCode_NonITMajor_L1.Visible = False
        lblMsg_DWCurrencyCode_NonITMajor_L1.Text = ""
        lblMsg_DWAmount_NonITMajor_L1.Visible = False
        lblMsg_DWAmount_NonITMajor_L1.Text = ""
        lblMsg_DWApprovalParty_NonITMajor_L1.Visible = False
        lblMsg_DWApprovalParty_NonITMajor_L1.Text = ""

        lblMsg_DWComparision_NonITMajor_L2.Visible = False
        lblMsg_DWComparision_NonITMajor_L2.Text = ""
        lblMsg_DWCurrencyCode_NonITMajor_L2.Visible = False
        lblMsg_DWCurrencyCode_NonITMajor_L2.Text = ""
        lblMsg_DWAmount_NonITMajor_L2.Visible = False
        lblMsg_DWAmount_NonITMajor_L2.Text = ""
        lblMsg_DWApprovalParty_NonITMajor_L2.Visible = False
        lblMsg_DWApprovalParty_NonITMajor_L2.Text = ""

        lblMsg_DWComparision_NonITMajor_L3.Visible = False
        lblMsg_DWComparision_NonITMajor_L3.Text = ""
        lblMsg_DWCurrencyCode_NonITMajor_L3.Visible = False
        lblMsg_DWCurrencyCode_NonITMajor_L3.Text = ""
        lblMsg_DWAmount_NonITMajor_L3.Visible = False
        lblMsg_DWAmount_NonITMajor_L3.Text = ""
        lblMsg_DWApprovalParty_NonITMajor_L3.Visible = False
        lblMsg_DWApprovalParty_NonITMajor_L3.Text = ""

        lblMsg_DWComparision_NonITMajor_L4.Visible = False
        lblMsg_DWComparision_NonITMajor_L4.Text = ""
        lblMsg_DWCurrencyCode_NonITMajor_L4.Visible = False
        lblMsg_DWCurrencyCode_NonITMajor_L4.Text = ""
        lblMsg_DWAmount_NonITMajor_L4.Visible = False
        lblMsg_DWAmount_NonITMajor_L4.Text = ""
        lblMsg_DWApprovalParty_NonITMajor_L4.Visible = False
        lblMsg_DWApprovalParty_NonITMajor_L4.Text = ""
        ''


        If gvLocation.Rows.Count = 0 Then
            lblMsg_Location.Text = "Please enter Station Location."
            blnResult = False
        End If

        If ddlCurrencyCode.SelectedValue.ToString = "0" Then
            lblMsg_CurrencyCode.Text = "Please select Price Point Currency Code."
            blnResult = False
        End If

        If txtQuoPricePoint.Text = "" Then
            lblMsg_Quotation.Text = "Please enter Quotation Price Point."
            blnResult = False
        ElseIf CDec(txtQuoPricePoint.Text) = 0 Then
            lblMsg_Quotation.Text = "Invalid Quotation Price Point."
            blnResult = False
        End If

        If txteReqPricePoint.Text = "" Then
            lblMsg_eReq.Text = "Please enter eRequisition Price Point."
            blnResult = False
        ElseIf CDec(txteReqPricePoint.Text) = 0 Then
            lblMsg_Quotation.Text = "Invalid Quotation Price Point."
            blnResult = False
        End If
        If txtFAPricePoint.Text = "" Then
            lblMsg_FA.Text = "Please enter Fixed Asset Price Point."
            blnResult = False
        ElseIf CDec(txtFAPricePoint.Text) = 0 Then
            lblMsg_Quotation.Text = "Invalid Quotation Price Point."
            blnResult = False
        End If

        If ddleReq_1st.SelectedValue.ToString = "" Then
            lblMsg_MailNotify.Text = "Please select 1st Party of eRequisition."
            blnResult = False
        End If

        If ddleReq_1st.SelectedValue.ToString <> "" Then
            If ddleReq_1st.SelectedValue.ToString = ddleReq_2nd.SelectedValue.ToString Then
                lblMsg_MailNotify.Text = "Error - eRequisition Party (1st and 2nd) cannot same."
                blnResult = False
            End If
            If ddleReq_1st.SelectedValue.ToString = ddleReq_3rd.SelectedValue.ToString Then
                lblMsg_MailNotify.Text = "Error - eRequisition Party (1st and 3rd) cannot same."
                blnResult = False
            End If
        End If

        If ddleReq_2nd.SelectedValue.ToString <> "" Then
            If ddleReq_2nd.SelectedValue.ToString = ddleReq_3rd.SelectedValue.ToString Then
                lblMsg_MailNotify.Text = "Error - eRequisition Party (2nd and 3rd) cannot same."
                blnResult = False
            End If
        End If


        If ddlIssuePO_1st.SelectedValue.ToString = "" Then
            lblMsg_MailNotify.Text = "Please select 1st Party of Issue PO."
            blnResult = False
        End If

        If ddlIssuePO_1st.SelectedValue.ToString <> "" Then
            If ddlIssuePO_1st.SelectedValue.ToString = ddlIssuePO_2nd.SelectedValue.ToString Then
                lblMsg_MailNotify.Text = "Error - Issue PO Party (1st and 2nd) cannot same."
                blnResult = False
            End If
            If ddlIssuePO_1st.SelectedValue.ToString = ddlIssuePO_3rd.SelectedValue.ToString Then
                lblMsg_MailNotify.Text = "Error - Issue PO Party (1st and 3rd) cannot same."
                blnResult = False
            End If
        End If

        If ddlIssuePO_2nd.SelectedValue.ToString <> "" Then
            If ddlIssuePO_2nd.SelectedValue.ToString = ddlIssuePO_3rd.SelectedValue.ToString Then
                lblMsg_MailNotify.Text = "Error - Issue PO Party (2nd and 3rd) cannot same."
                blnResult = False
            End If
        End If

        If ddlIssuePayment_1st.SelectedValue.ToString = "" Then
            lblMsg_MailNotify.Text = "Please select 1st Party of Issue Payment."
            blnResult = False
        End If

        If ddlIssuePayment_1st.SelectedValue.ToString <> "" Then
            If ddlIssuePayment_1st.SelectedValue.ToString = ddlIssuePayment_2nd.SelectedValue.ToString Then
                lblMsg_MailNotify.Text = "Error - Issue Payment Party (1st and 2nd) cannot same."
                blnResult = False
            End If
            If ddlIssuePayment_1st.SelectedValue.ToString = ddlIssuePayment_3rd.SelectedValue.ToString Then
                lblMsg_MailNotify.Text = "Error - Issue Payment Party (1st and 3rd) cannot same."
                blnResult = False
            End If
        End If

        If ddlIssuePayment_2nd.SelectedValue.ToString <> "" Then
            If ddlIssuePayment_2nd.SelectedValue.ToString = ddlIssuePayment_3rd.SelectedValue.ToString Then
                lblMsg_MailNotify.Text = "Error - Issue Payment Party (2nd and 3rd) cannot same."
                blnResult = False
            End If
        End If


        If ddleFMS_1st.SelectedValue.ToString = "" Then
            lblMsg_MailNotify.Text = "Please select 1st Party of Post eFMS."
            blnResult = False
        End If

        If ddleFMS_1st.SelectedValue.ToString <> "" Then
            If ddleFMS_1st.SelectedValue.ToString = ddleFMS_2nd.SelectedValue.ToString Then
                lblMsg_MailNotify.Text = "Error - Post eFMS Party (1st and 2nd) cannot same."
                blnResult = False
            End If
            If ddleFMS_1st.SelectedValue.ToString = ddleFMS_3rd.SelectedValue.ToString Then
                lblMsg_MailNotify.Text = "Error - Post eFMS Party (1st and 3rd) cannot same."
                blnResult = False
            End If
        End If
        If ddleFMS_2nd.SelectedValue.ToString <> "" Then
            If ddleFMS_2nd.SelectedValue.ToString = ddleFMS_3rd.SelectedValue.ToString Then
                lblMsg_MailNotify.Text = "Error - Post eFMS Party (2nd and 3rd) cannot same."
                blnResult = False
            End If
        End If

        'Purchase, IT, Budgeted
        'Level 1 
        If txtAmount_IT_L1.Text <> "" Then
            If ddlComparision_IT_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_IT_L1.Visible = True
                lblMsg_Comparision_IT_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If ddlCurrencyCode_IT_L1.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_IT_L1.Visible = True
                lblMsg_CurrencyCode_IT_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlComparision_IT_L1.SelectedValue.ToString <> "Select One" Then
            If txtAmount_IT_L1.Text = "" Then
                lblMsg_Amount_IT_L1.Visible = True
                lblMsg_Amount_IT_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
            If ddlCurrencyCode_IT_L1.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_IT_L1.Visible = True
                lblMsg_CurrencyCode_IT_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlCurrencyCode_IT_L1.SelectedValue.ToString <> "0" Then
            If ddlComparision_IT_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_IT_L1.Visible = True
                lblMsg_Comparision_IT_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If txtAmount_IT_L1.Text = "" Then
                lblMsg_Amount_IT_L1.Visible = True
                lblMsg_Amount_IT_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
        End If

        If ddlApprovalParty_IT_L1.SelectedValue.ToString = "" Then
            lblMsg_ApprovalParty_IT_L1.Visible = True
            lblMsg_ApprovalParty_IT_L1.Text = "Please select L1 Approval Party."
            blnResult = False
        Else
            If ddlApprovalParty2_IT_L1.SelectedValue.ToString <> "" Then
                If ddlApprovalParty_IT_L1.SelectedValue.ToString = ddlApprovalParty2_IT_L1.SelectedValue.ToString Then
                    lblMsg_ApprovalParty_IT_L1.Visible = True
                    lblMsg_ApprovalParty_IT_L1.Text = "Error - L1 Approval Party (1st and 2nd) cannot same."
                    blnResult = False
                End If
            End If
        End If



        If ddlApprovalParty_IT_L1.SelectedValue.ToString <> "" Then
            If ddlApprovalParty_IT_L1.SelectedValue.ToString = ddlApprovalParty_IT_L2.SelectedValue.ToString Then
                lblMsg_ApprovalParty_IT_L2.Visible = True
                lblMsg_ApprovalParty_IT_L2.Text = "Error - Approval Party (L1 [1st] & L2) cannot be same."
                blnResult = False
            End If
            If ddlApprovalParty_IT_L1.SelectedValue.ToString = ddlApprovalParty_IT_L3.SelectedValue.ToString Then
                lblMsg_ApprovalParty_IT_L3.Visible = True
                lblMsg_ApprovalParty_IT_L3.Text = "Error - Approval Party (L1 [1st] & L3) cannot be same."
                blnResult = False
            End If
            If ddlApprovalParty_IT_L1.SelectedValue.ToString = ddlApprovalParty_IT_L4.SelectedValue.ToString Then
                lblMsg_ApprovalParty_IT_L4.Visible = True
                lblMsg_ApprovalParty_IT_L4.Text = "Error - Approval Party (L1 [1st] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlApprovalParty2_IT_L1.SelectedValue.ToString <> "" Then
            If ddlApprovalParty2_IT_L1.SelectedValue.ToString = ddlApprovalParty_IT_L2.SelectedValue.ToString Then
                lblMsg_ApprovalParty_IT_L2.Visible = True
                lblMsg_ApprovalParty_IT_L2.Text = "Error - Approval Party (L1 [2nd] & L2) cannot be same."
                blnResult = False
            End If
            If ddlApprovalParty2_IT_L1.SelectedValue.ToString = ddlApprovalParty_IT_L3.SelectedValue.ToString Then
                lblMsg_ApprovalParty_IT_L3.Visible = True
                lblMsg_ApprovalParty_IT_L3.Text = "Error - Approval Party (L1 [2nd] & L3) cannot be same."
                blnResult = False
            End If
            If ddlApprovalParty2_IT_L1.SelectedValue.ToString = ddlApprovalParty_IT_L4.SelectedValue.ToString Then
                lblMsg_ApprovalParty_IT_L4.Visible = True
                lblMsg_ApprovalParty_IT_L4.Text = "Error - Approval Party (L1 [2nd] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlApprovalParty_IT_L2.SelectedValue.ToString <> "" Then
            If ddlApprovalParty_IT_L2.SelectedValue.ToString = ddlApprovalParty_IT_L3.SelectedValue.ToString Then
                lblMsg_ApprovalParty_IT_L3.Visible = True
                lblMsg_ApprovalParty_IT_L3.Text = "Error - Approval Party (L2 & L3) cannot be same."
                blnResult = False
            End If
            If ddlApprovalParty_IT_L2.SelectedValue.ToString = ddlApprovalParty_IT_L4.SelectedValue.ToString Then
                lblMsg_ApprovalParty_IT_L4.Visible = True
                lblMsg_ApprovalParty_IT_L4.Text = "Error - Approval Party (L2 & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlApprovalParty_IT_L3.SelectedValue.ToString <> "" Then
            If ddlApprovalParty_IT_L3.SelectedValue.ToString = ddlApprovalParty_IT_L4.SelectedValue.ToString Then
                lblMsg_ApprovalParty_IT_L4.Visible = True
                lblMsg_ApprovalParty_IT_L4.Text = "Error - Approval Party (L3 & L4) cannot be same."
                blnResult = False
            End If
        End If


        'Level 2
        If txtAmount_IT_L2.Text <> "" Then
            If ddlComparision_IT_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_IT_L2.Visible = True
                lblMsg_Comparision_IT_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If ddlCurrencyCode_IT_L2.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_IT_L2.Visible = True
                lblMsg_CurrencyCode_IT_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlApprovalParty_IT_L2.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_IT_L2.Visible = True
                lblMsg_ApprovalParty_IT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlComparision_IT_L2.SelectedValue.ToString <> "Select One" Then
            If txtAmount_IT_L2.Text = "" Then
                lblMsg_Amount_IT_L2.Visible = True
                lblMsg_Amount_IT_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If
            If ddlCurrencyCode_IT_L2.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_IT_L2.Visible = True
                lblMsg_CurrencyCode_IT_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlApprovalParty_IT_L2.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_IT_L2.Visible = True
                lblMsg_ApprovalParty_IT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlCurrencyCode_IT_L2.SelectedValue.ToString <> "0" Then
            If ddlComparision_IT_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_IT_L2.Visible = True
                lblMsg_Comparision_IT_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If txtAmount_IT_L2.Text = "" Then
                lblMsg_Amount_IT_L2.Visible = True
                lblMsg_Amount_IT_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If

            If ddlApprovalParty_IT_L2.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_IT_L2.Visible = True
                lblMsg_ApprovalParty_IT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If


        'Level 3
        If txtAmount_IT_L3.Text <> "" Then
            If ddlComparision_IT_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_IT_L3.Visible = True
                lblMsg_Comparision_IT_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If ddlCurrencyCode_IT_L3.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_IT_L3.Visible = True
                lblMsg_CurrencyCode_IT_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlApprovalParty_IT_L3.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_IT_L3.Visible = True
                lblMsg_ApprovalParty_IT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlComparision_IT_L3.SelectedValue.ToString <> "Select One" Then
            If txtAmount_IT_L3.Text = "" Then
                lblMsg_Amount_IT_L3.Visible = True
                lblMsg_Amount_IT_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If
            If ddlCurrencyCode_IT_L3.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_IT_L3.Visible = True
                lblMsg_CurrencyCode_IT_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlApprovalParty_IT_L3.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_IT_L3.Visible = True
                lblMsg_ApprovalParty_IT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlCurrencyCode_IT_L3.SelectedValue.ToString <> "0" Then
            If ddlComparision_IT_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_IT_L3.Visible = True
                lblMsg_Comparision_IT_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If txtAmount_IT_L3.Text = "" Then
                lblMsg_Amount_IT_L3.Visible = True
                lblMsg_Amount_IT_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If

            If ddlApprovalParty_IT_L3.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_IT_L3.Visible = True
                lblMsg_ApprovalParty_IT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        'Level 4
        If txtAmount_IT_L4.Text <> "" Then
            If ddlComparision_IT_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_IT_L4.Visible = True
                lblMsg_Comparision_IT_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If ddlCurrencyCode_IT_L4.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_IT_L4.Visible = True
                lblMsg_CurrencyCode_IT_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlApprovalParty_IT_L4.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_IT_L4.Visible = True
                lblMsg_ApprovalParty_IT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlComparision_IT_L4.SelectedValue.ToString <> "Select One" Then
            If txtAmount_IT_L4.Text = "" Then
                lblMsg_Amount_IT_L4.Visible = True
                lblMsg_Amount_IT_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If
            If ddlCurrencyCode_IT_L4.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_IT_L4.Visible = True
                lblMsg_CurrencyCode_IT_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlApprovalParty_IT_L4.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_IT_L4.Visible = True
                lblMsg_ApprovalParty_IT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlCurrencyCode_IT_L4.SelectedValue.ToString <> "0" Then
            If ddlComparision_IT_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_IT_L4.Visible = True
                lblMsg_Comparision_IT_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If txtAmount_IT_L4.Text = "" Then
                lblMsg_Amount_IT_L4.Visible = True
                lblMsg_Amount_IT_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If

            If ddlApprovalParty_IT_L4.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_IT_L4.Visible = True
                lblMsg_ApprovalParty_IT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If


        'Purchase, NON IT, Budgeted
        'Level 1
        If txtAmount_NonIT_L1.Text <> "" Then
            If ddlComparision_NonIT_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_NonIT_L1.Visible = True
                lblMsg_Comparision_NonIT_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If ddlCurrencyCode_NonIT_L1.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_NonIT_L1.Visible = True
                lblMsg_CurrencyCode_NonIT_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlComparision_NonIT_L1.SelectedValue.ToString <> "Select One" Then
            If txtAmount_NonIT_L1.Text = "" Then
                lblMsg_Amount_NonIT_L1.Visible = True
                lblMsg_Amount_NonIT_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
            If ddlCurrencyCode_NonIT_L1.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_NonIT_L1.Visible = True
                lblMsg_CurrencyCode_NonIT_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlCurrencyCode_NonIT_L1.SelectedValue.ToString <> "0" Then
            If ddlComparision_NonIT_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_NonIT_L1.Visible = True
                lblMsg_Comparision_NonIT_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If txtAmount_NonIT_L1.Text = "" Then
                lblMsg_Amount_NonIT_L1.Visible = True
                lblMsg_Amount_NonIT_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
        End If

        If ddlApprovalParty_NonIT_L1.SelectedValue.ToString = "" Then
            lblMsg_ApprovalParty_NonIT_L1.Visible = True
            lblMsg_ApprovalParty_NonIT_L1.Text = "Please select L1 Approval Party."
            blnResult = False
        Else
            If ddlApprovalParty2_NonIT_L1.SelectedValue.ToString <> "" Then
                If ddlApprovalParty_NonIT_L1.SelectedValue.ToString = ddlApprovalParty2_NonIT_L1.SelectedValue.ToString Then
                    lblMsg_ApprovalParty_NonIT_L1.Visible = True
                    lblMsg_ApprovalParty_NonIT_L1.Text = "Error - L1 Approval Party (1st and 2nd) cannot same."
                    blnResult = False
                End If
            End If
        End If

        If ddlApprovalParty_NonIT_L1.SelectedValue.ToString <> "" Then
            If ddlApprovalParty_NonIT_L1.SelectedValue.ToString = ddlApprovalParty_NonIT_L2.SelectedValue.ToString Then
                lblMsg_ApprovalParty_NonIT_L2.Visible = True
                lblMsg_ApprovalParty_NonIT_L2.Text = "Error - Approval Party (L1 [1st] & L2) cannot be same."
                blnResult = False
            End If
            If ddlApprovalParty_NonIT_L1.SelectedValue.ToString = ddlApprovalParty_NonIT_L3.SelectedValue.ToString Then
                lblMsg_ApprovalParty_NonIT_L3.Visible = True
                lblMsg_ApprovalParty_NonIT_L3.Text = "Error - Approval Party (L1 [1st] & L3) cannot be same."
                blnResult = False
            End If
            If ddlApprovalParty_NonIT_L1.SelectedValue.ToString = ddlApprovalParty_NonIT_L4.SelectedValue.ToString Then
                lblMsg_ApprovalParty_NonIT_L4.Visible = True
                lblMsg_ApprovalParty_NonIT_L4.Text = "Error - Approval Party (L1 [1st] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlApprovalParty2_NonIT_L1.SelectedValue.ToString <> "" Then
            If ddlApprovalParty2_NonIT_L1.SelectedValue.ToString = ddlApprovalParty_NonIT_L2.SelectedValue.ToString Then
                lblMsg_ApprovalParty_NonIT_L2.Visible = True
                lblMsg_ApprovalParty_NonIT_L2.Text = "Error - Approval Party (L1 [2nd] & L2) cannot be same."
                blnResult = False
            End If
            If ddlApprovalParty2_NonIT_L1.SelectedValue.ToString = ddlApprovalParty_NonIT_L3.SelectedValue.ToString Then
                lblMsg_ApprovalParty_NonIT_L3.Visible = True
                lblMsg_ApprovalParty_NonIT_L3.Text = "Error - Approval Party (L1 [2nd] & L3) cannot be same."
                blnResult = False
            End If
            If ddlApprovalParty2_NonIT_L1.SelectedValue.ToString = ddlApprovalParty_NonIT_L4.SelectedValue.ToString Then
                lblMsg_ApprovalParty_NonIT_L4.Visible = True
                lblMsg_ApprovalParty_NonIT_L4.Text = "Error - Approval Party (L1 [2nd] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlApprovalParty_NonIT_L2.SelectedValue.ToString <> "" Then
            If ddlApprovalParty_NonIT_L2.SelectedValue.ToString = ddlApprovalParty_NonIT_L3.SelectedValue.ToString Then
                lblMsg_ApprovalParty_NonIT_L3.Visible = True
                lblMsg_ApprovalParty_NonIT_L3.Text = "Error - Approval Party (L2 & L3) cannot be same."
                blnResult = False
            End If
            If ddlApprovalParty_NonIT_L2.SelectedValue.ToString = ddlApprovalParty_NonIT_L4.SelectedValue.ToString Then
                lblMsg_ApprovalParty_NonIT_L4.Visible = True
                lblMsg_ApprovalParty_NonIT_L4.Text = "Error - Approval Party (L2 & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlApprovalParty_NonIT_L3.SelectedValue.ToString <> "" Then
            If ddlApprovalParty_NonIT_L3.SelectedValue.ToString = ddlApprovalParty_NonIT_L4.SelectedValue.ToString Then
                lblMsg_ApprovalParty_NonIT_L4.Visible = True
                lblMsg_ApprovalParty_NonIT_L4.Text = "Error - Approval Party (L3 & L4) cannot be same."
                blnResult = False
            End If
        End If

        'Level 2
        If txtAmount_NonIT_L2.Text <> "" Then
            If ddlComparision_NonIT_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_NonIT_L2.Visible = True
                lblMsg_Comparision_NonIT_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If ddlCurrencyCode_NonIT_L2.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_NonIT_L2.Visible = True
                lblMsg_CurrencyCode_NonIT_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlApprovalParty_NonIT_L2.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_NonIT_L2.Visible = True
                lblMsg_ApprovalParty_NonIT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlComparision_NonIT_L2.SelectedValue.ToString <> "Select One" Then
            If txtAmount_NonIT_L2.Text = "" Then
                lblMsg_Amount_NonIT_L2.Visible = True
                lblMsg_Amount_NonIT_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If
            If ddlCurrencyCode_NonIT_L2.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_NonIT_L2.Visible = True
                lblMsg_CurrencyCode_NonIT_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlApprovalParty_NonIT_L2.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_NonIT_L2.Visible = True
                lblMsg_ApprovalParty_NonIT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlCurrencyCode_NonIT_L2.SelectedValue.ToString <> "0" Then
            If ddlComparision_NonIT_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_NonIT_L2.Visible = True
                lblMsg_Comparision_NonIT_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If txtAmount_NonIT_L2.Text = "" Then
                lblMsg_Amount_NonIT_L2.Visible = True
                lblMsg_Amount_NonIT_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If

            If ddlApprovalParty_NonIT_L2.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_NonIT_L2.Visible = True
                lblMsg_ApprovalParty_NonIT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        'Level 3
        If txtAmount_NonIT_L3.Text <> "" Then
            If ddlComparision_NonIT_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_NonIT_L3.Visible = True
                lblMsg_Comparision_NonIT_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If ddlCurrencyCode_NonIT_L3.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_NonIT_L3.Visible = True
                lblMsg_CurrencyCode_NonIT_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlApprovalParty_NonIT_L3.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_NonIT_L3.Visible = True
                lblMsg_ApprovalParty_NonIT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlComparision_NonIT_L3.SelectedValue.ToString <> "Select One" Then
            If txtAmount_NonIT_L3.Text = "" Then
                lblMsg_Amount_NonIT_L3.Visible = True
                lblMsg_Amount_NonIT_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If
            If ddlCurrencyCode_NonIT_L3.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_NonIT_L3.Visible = True
                lblMsg_CurrencyCode_NonIT_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlApprovalParty_NonIT_L3.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_NonIT_L3.Visible = True
                lblMsg_ApprovalParty_NonIT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlCurrencyCode_NonIT_L3.SelectedValue.ToString <> "0" Then
            If ddlComparision_NonIT_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_NonIT_L3.Visible = True
                lblMsg_Comparision_NonIT_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If txtAmount_NonIT_L3.Text = "" Then
                lblMsg_Amount_NonIT_L3.Visible = True
                lblMsg_Amount_NonIT_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If

            If ddlApprovalParty_NonIT_L3.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_NonIT_L3.Visible = True
                lblMsg_ApprovalParty_NonIT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        'Level 4
        If txtAmount_NonIT_L4.Text <> "" Then
            If ddlComparision_NonIT_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_NonIT_L4.Visible = True
                lblMsg_Comparision_NonIT_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If ddlCurrencyCode_NonIT_L4.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_NonIT_L4.Visible = True
                lblMsg_CurrencyCode_NonIT_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlApprovalParty_NonIT_L4.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_NonIT_L4.Visible = True
                lblMsg_ApprovalParty_NonIT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlComparision_NonIT_L4.SelectedValue.ToString <> "Select One" Then
            If txtAmount_NonIT_L4.Text = "" Then
                lblMsg_Amount_NonIT_L4.Visible = True
                lblMsg_Amount_NonIT_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If
            If ddlCurrencyCode_NonIT_L4.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_NonIT_L4.Visible = True
                lblMsg_CurrencyCode_NonIT_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlApprovalParty_NonIT_L4.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_NonIT_L4.Visible = True
                lblMsg_ApprovalParty_NonIT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlCurrencyCode_NonIT_L4.SelectedValue.ToString <> "0" Then
            If ddlComparision_NonIT_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_NonIT_L4.Visible = True
                lblMsg_Comparision_NonIT_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If txtAmount_NonIT_L4.Text = "" Then
                lblMsg_Amount_NonIT_L4.Visible = True
                lblMsg_Amount_NonIT_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If

            If ddlApprovalParty_NonIT_L4.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_NonIT_L4.Visible = True
                lblMsg_ApprovalParty_NonIT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        '''''''''''
        'Purchase, NON IT (MAJOR), Budgeted
        'Level 1
        If txtAmount_NonITMajor_L1.Text <> "" Then
            If ddlComparision_NonITMajor_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_NonITMajor_L1.Visible = True
                lblMsg_Comparision_NonITMajor_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If ddlCurrencyCode_NonITMajor_L1.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_NonITMajor_L1.Visible = True
                lblMsg_CurrencyCode_NonITMajor_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlComparision_NonITMajor_L1.SelectedValue.ToString <> "Select One" Then
            If txtAmount_NonITMajor_L1.Text = "" Then
                lblMsg_Amount_NonITMajor_L1.Visible = True
                lblMsg_Amount_NonITMajor_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
            If ddlCurrencyCode_NonITMajor_L1.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_NonITMajor_L1.Visible = True
                lblMsg_CurrencyCode_NonITMajor_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlCurrencyCode_NonITMajor_L1.SelectedValue.ToString <> "0" Then
            If ddlComparision_NonITMajor_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_NonITMajor_L1.Visible = True
                lblMsg_Comparision_NonITMajor_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If txtAmount_NonITMajor_L1.Text = "" Then
                lblMsg_Amount_NonITMajor_L1.Visible = True
                lblMsg_Amount_NonITMajor_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
        End If

        If ddlApprovalParty_NonITMajor_L1.SelectedValue.ToString = "" Then
            lblMsg_ApprovalParty_NonITMajor_L1.Visible = True
            lblMsg_ApprovalParty_NonITMajor_L1.Text = "Please select L1 Approval Party."
            blnResult = False
        Else
            If ddlApprovalParty2_NonITMajor_L1.SelectedValue.ToString <> "" Then
                If ddlApprovalParty_NonITMajor_L1.SelectedValue.ToString = ddlApprovalParty2_NonITMajor_L1.SelectedValue.ToString Then
                    lblMsg_ApprovalParty_NonITMajor_L1.Visible = True
                    lblMsg_ApprovalParty_NonITMajor_L1.Text = "Error - L1 Approval Party (1st and 2nd) cannot same."
                    blnResult = False
                End If
            End If
        End If

        If ddlApprovalParty_NonITMajor_L1.SelectedValue.ToString <> "" Then
            If ddlApprovalParty_NonITMajor_L1.SelectedValue.ToString = ddlApprovalParty_NonITMajor_L2.SelectedValue.ToString Then
                lblMsg_ApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_ApprovalParty_NonITMajor_L2.Text = "Error - Approval Party (L1 [1st] & L2) cannot be same."
                blnResult = False
            End If
            If ddlApprovalParty_NonITMajor_L1.SelectedValue.ToString = ddlApprovalParty_NonITMajor_L3.SelectedValue.ToString Then
                lblMsg_ApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_ApprovalParty_NonITMajor_L3.Text = "Error - Approval Party (L1 [1st] & L3) cannot be same."
                blnResult = False
            End If
            If ddlApprovalParty_NonITMajor_L1.SelectedValue.ToString = ddlApprovalParty_NonITMajor_L4.SelectedValue.ToString Then
                lblMsg_ApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_ApprovalParty_NonITMajor_L4.Text = "Error - Approval Party (L1 [1st] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlApprovalParty2_NonITMajor_L1.SelectedValue.ToString <> "" Then
            If ddlApprovalParty2_NonITMajor_L1.SelectedValue.ToString = ddlApprovalParty_NonITMajor_L2.SelectedValue.ToString Then
                lblMsg_ApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_ApprovalParty_NonITMajor_L2.Text = "Error - Approval Party (L1 [2nd] & L2) cannot be same."
                blnResult = False
            End If
            If ddlApprovalParty2_NonITMajor_L1.SelectedValue.ToString = ddlApprovalParty_NonITMajor_L3.SelectedValue.ToString Then
                lblMsg_ApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_ApprovalParty_NonITMajor_L3.Text = "Error - Approval Party (L1 [2nd] & L3) cannot be same."
                blnResult = False
            End If
            If ddlApprovalParty2_NonITMajor_L1.SelectedValue.ToString = ddlApprovalParty_NonITMajor_L4.SelectedValue.ToString Then
                lblMsg_ApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_ApprovalParty_NonITMajor_L4.Text = "Error - Approval Party (L1 [2nd] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlApprovalParty_NonITMajor_L2.SelectedValue.ToString <> "" Then
            If ddlApprovalParty_NonITMajor_L2.SelectedValue.ToString = ddlApprovalParty_NonITMajor_L3.SelectedValue.ToString Then
                lblMsg_ApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_ApprovalParty_NonITMajor_L3.Text = "Error - Approval Party (L2 & L3) cannot be same."
                blnResult = False
            End If
            If ddlApprovalParty_NonITMajor_L2.SelectedValue.ToString = ddlApprovalParty_NonITMajor_L4.SelectedValue.ToString Then
                lblMsg_ApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_ApprovalParty_NonITMajor_L4.Text = "Error - Approval Party (L2 & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlApprovalParty_NonITMajor_L3.SelectedValue.ToString <> "" Then
            If ddlApprovalParty_NonITMajor_L3.SelectedValue.ToString = ddlApprovalParty_NonITMajor_L4.SelectedValue.ToString Then
                lblMsg_ApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_ApprovalParty_NonITMajor_L4.Text = "Error - Approval Party (L3 & L4) cannot be same."
                blnResult = False
            End If
        End If

        'Level 2
        If txtAmount_NonITMajor_L2.Text <> "" Then
            If ddlComparision_NonITMajor_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_NonITMajor_L2.Visible = True
                lblMsg_Comparision_NonITMajor_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If ddlCurrencyCode_NonITMajor_L2.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_NonITMajor_L2.Visible = True
                lblMsg_CurrencyCode_NonITMajor_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlApprovalParty_NonITMajor_L2.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_ApprovalParty_NonITMajor_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlComparision_NonITMajor_L2.SelectedValue.ToString <> "Select One" Then
            If txtAmount_NonITMajor_L2.Text = "" Then
                lblMsg_Amount_NonITMajor_L2.Visible = True
                lblMsg_Amount_NonITMajor_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If
            If ddlCurrencyCode_NonITMajor_L2.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_NonITMajor_L2.Visible = True
                lblMsg_CurrencyCode_NonITMajor_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlApprovalParty_NonITMajor_L2.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_ApprovalParty_NonITMajor_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlCurrencyCode_NonITMajor_L2.SelectedValue.ToString <> "0" Then
            If ddlComparision_NonITMajor_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_NonITMajor_L2.Visible = True
                lblMsg_Comparision_NonITMajor_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If txtAmount_NonITMajor_L2.Text = "" Then
                lblMsg_Amount_NonITMajor_L2.Visible = True
                lblMsg_Amount_NonITMajor_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If

            If ddlApprovalParty_NonITMajor_L2.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_ApprovalParty_NonITMajor_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        'Level 3
        If txtAmount_NonITMajor_L3.Text <> "" Then
            If ddlComparision_NonITMajor_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_NonITMajor_L3.Visible = True
                lblMsg_Comparision_NonITMajor_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If ddlCurrencyCode_NonITMajor_L3.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_NonITMajor_L3.Visible = True
                lblMsg_CurrencyCode_NonITMajor_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlApprovalParty_NonITMajor_L3.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_ApprovalParty_NonITMajor_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlComparision_NonITMajor_L3.SelectedValue.ToString <> "Select One" Then
            If txtAmount_NonITMajor_L3.Text = "" Then
                lblMsg_Amount_NonITMajor_L3.Visible = True
                lblMsg_Amount_NonITMajor_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If
            If ddlCurrencyCode_NonITMajor_L3.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_NonITMajor_L3.Visible = True
                lblMsg_CurrencyCode_NonITMajor_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlApprovalParty_NonITMajor_L3.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_ApprovalParty_NonITMajor_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlCurrencyCode_NonITMajor_L3.SelectedValue.ToString <> "0" Then
            If ddlComparision_NonITMajor_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_NonITMajor_L3.Visible = True
                lblMsg_Comparision_NonITMajor_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If txtAmount_NonITMajor_L3.Text = "" Then
                lblMsg_Amount_NonITMajor_L3.Visible = True
                lblMsg_Amount_NonITMajor_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If

            If ddlApprovalParty_NonITMajor_L3.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_ApprovalParty_NonITMajor_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        'Level 4
        If txtAmount_NonITMajor_L4.Text <> "" Then
            If ddlComparision_NonITMajor_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_NonITMajor_L4.Visible = True
                lblMsg_Comparision_NonITMajor_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If ddlCurrencyCode_NonITMajor_L4.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_NonITMajor_L4.Visible = True
                lblMsg_CurrencyCode_NonITMajor_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlApprovalParty_NonITMajor_L4.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_ApprovalParty_NonITMajor_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlComparision_NonITMajor_L4.SelectedValue.ToString <> "Select One" Then
            If txtAmount_NonITMajor_L4.Text = "" Then
                lblMsg_Amount_NonITMajor_L4.Visible = True
                lblMsg_Amount_NonITMajor_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If
            If ddlCurrencyCode_NonITMajor_L4.SelectedValue.ToString = "0" Then
                lblMsg_CurrencyCode_NonITMajor_L4.Visible = True
                lblMsg_CurrencyCode_NonITMajor_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlApprovalParty_NonITMajor_L4.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_ApprovalParty_NonITMajor_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlCurrencyCode_NonITMajor_L4.SelectedValue.ToString <> "0" Then
            If ddlComparision_NonITMajor_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_Comparision_NonITMajor_L4.Visible = True
                lblMsg_Comparision_NonITMajor_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If txtAmount_NonITMajor_L4.Text = "" Then
                lblMsg_Amount_NonITMajor_L4.Visible = True
                lblMsg_Amount_NonITMajor_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If

            If ddlApprovalParty_NonITMajor_L4.SelectedValue.ToString = "" Then
                lblMsg_ApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_ApprovalParty_NonITMajor_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If


        ''''''''''''''''''''


        'Purchase, IT, UnBudgeted
        'Level 1 
        If txtUBAmount_IT_L1.Text <> "" Then
            If ddlUBComparision_IT_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_IT_L1.Visible = True
                lblMsg_UBComparision_IT_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If ddlUBCurrencyCode_IT_L1.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_IT_L1.Visible = True
                lblMsg_UBCurrencyCode_IT_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlUBComparision_IT_L1.SelectedValue.ToString <> "Select One" Then
            If txtUBAmount_IT_L1.Text = "" Then
                lblMsg_UBAmount_IT_L1.Visible = True
                lblMsg_UBAmount_IT_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
            If ddlUBCurrencyCode_IT_L1.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_IT_L1.Visible = True
                lblMsg_UBCurrencyCode_IT_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlUBCurrencyCode_IT_L1.SelectedValue.ToString <> "0" Then
            If ddlUBComparision_IT_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_IT_L1.Visible = True
                lblMsg_UBComparision_IT_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If txtUBAmount_IT_L1.Text = "" Then
                lblMsg_UBAmount_IT_L1.Visible = True
                lblMsg_UBAmount_IT_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
        End If

        If ddlUBApprovalParty_IT_L1.SelectedValue.ToString = "" Then
            lblMsg_UBApprovalParty_IT_L1.Visible = True
            lblMsg_UBApprovalParty_IT_L1.Text = "Please select L1 Approval Party."
            blnResult = False
        Else
            If ddlUBApprovalParty2_IT_L1.SelectedValue.ToString <> "" Then
                If ddlUBApprovalParty_IT_L1.SelectedValue.ToString = ddlUBApprovalParty2_IT_L1.SelectedValue.ToString Then
                    lblMsg_UBApprovalParty_IT_L1.Visible = True
                    lblMsg_UBApprovalParty_IT_L1.Text = "Error - L1 Approval Party (1st and 2nd) cannot same."
                    blnResult = False
                End If
            End If
        End If

        If ddlUBApprovalParty_IT_L1.SelectedValue.ToString <> "" Then
            If ddlUBApprovalParty_IT_L1.SelectedValue.ToString = ddlUBApprovalParty_IT_L2.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_IT_L2.Visible = True
                lblMsg_UBApprovalParty_IT_L2.Text = "Error - Approval Party (L1 [1st] & L2) cannot be same."
                blnResult = False
            End If
            If ddlUBApprovalParty_IT_L1.SelectedValue.ToString = ddlUBApprovalParty_IT_L3.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_IT_L3.Visible = True
                lblMsg_UBApprovalParty_IT_L3.Text = "Error - Approval Party (L1 [1st] & L3) cannot be same."
                blnResult = False
            End If
            If ddlUBApprovalParty_IT_L1.SelectedValue.ToString = ddlUBApprovalParty_IT_L4.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_IT_L4.Visible = True
                lblMsg_UBApprovalParty_IT_L4.Text = "Error - Approval Party (L1 [1st] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlUBApprovalParty2_IT_L1.SelectedValue.ToString <> "" Then
            If ddlUBApprovalParty2_IT_L1.SelectedValue.ToString = ddlUBApprovalParty_IT_L2.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_IT_L2.Visible = True
                lblMsg_UBApprovalParty_IT_L2.Text = "Error - Approval Party (L1 [2nd] & L2) cannot be same."
                blnResult = False
            End If
            If ddlUBApprovalParty2_IT_L1.SelectedValue.ToString = ddlUBApprovalParty_IT_L3.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_IT_L3.Visible = True
                lblMsg_UBApprovalParty_IT_L3.Text = "Error - Approval Party (L1 [2nd] & L3) cannot be same."
                blnResult = False
            End If
            If ddlUBApprovalParty2_IT_L1.SelectedValue.ToString = ddlUBApprovalParty_IT_L4.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_IT_L4.Visible = True
                lblMsg_UBApprovalParty_IT_L4.Text = "Error - Approval Party (L1 [2nd] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlUBApprovalParty_IT_L2.SelectedValue.ToString <> "" Then
            If ddlUBApprovalParty_IT_L2.SelectedValue.ToString = ddlUBApprovalParty_IT_L3.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_IT_L3.Visible = True
                lblMsg_UBApprovalParty_IT_L3.Text = "Error - Approval Party (L2 & L3) cannot be same."
                blnResult = False
            End If
            If ddlUBApprovalParty_IT_L2.SelectedValue.ToString = ddlUBApprovalParty_IT_L4.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_IT_L4.Visible = True
                lblMsg_UBApprovalParty_IT_L4.Text = "Error - Approval Party (L2 & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlUBApprovalParty_IT_L3.SelectedValue.ToString <> "" Then
            If ddlUBApprovalParty_IT_L3.SelectedValue.ToString = ddlUBApprovalParty_IT_L4.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_IT_L4.Visible = True
                lblMsg_UBApprovalParty_IT_L4.Text = "Error - Approval Party (L3 & L4) cannot be same."
                blnResult = False
            End If
        End If

        'Level 2
        If txtUBAmount_IT_L2.Text <> "" Then
            If ddlUBComparision_IT_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_IT_L2.Visible = True
                lblMsg_UBComparision_IT_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If ddlUBCurrencyCode_IT_L2.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_IT_L2.Visible = True
                lblMsg_UBCurrencyCode_IT_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlUBApprovalParty_IT_L2.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_IT_L2.Visible = True
                lblMsg_UBApprovalParty_IT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlUBComparision_IT_L2.SelectedValue.ToString <> "Select One" Then
            If txtUBAmount_IT_L2.Text = "" Then
                lblMsg_UBAmount_IT_L2.Visible = True
                lblMsg_UBAmount_IT_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If
            If ddlUBCurrencyCode_IT_L2.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_IT_L2.Visible = True
                lblMsg_UBCurrencyCode_IT_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlUBApprovalParty_IT_L2.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_IT_L2.Visible = True
                lblMsg_UBApprovalParty_IT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlUBCurrencyCode_IT_L2.SelectedValue.ToString <> "0" Then
            If ddlUBComparision_IT_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_IT_L2.Visible = True
                lblMsg_UBComparision_IT_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If txtUBAmount_IT_L2.Text = "" Then
                lblMsg_UBAmount_IT_L2.Visible = True
                lblMsg_UBAmount_IT_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If

            If ddlUBApprovalParty_IT_L2.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_IT_L2.Visible = True
                lblMsg_UBApprovalParty_IT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        'Level 3
        If txtUBAmount_IT_L3.Text <> "" Then
            If ddlUBComparision_IT_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_IT_L3.Visible = True
                lblMsg_UBComparision_IT_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If ddlUBCurrencyCode_IT_L3.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_IT_L3.Visible = True
                lblMsg_UBCurrencyCode_IT_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlUBApprovalParty_IT_L3.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_IT_L3.Visible = True
                lblMsg_UBApprovalParty_IT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlUBComparision_IT_L3.SelectedValue.ToString <> "Select One" Then
            If txtUBAmount_IT_L3.Text = "" Then
                lblMsg_UBAmount_IT_L3.Visible = True
                lblMsg_UBAmount_IT_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If
            If ddlUBCurrencyCode_IT_L3.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_IT_L3.Visible = True
                lblMsg_UBCurrencyCode_IT_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlUBApprovalParty_IT_L3.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_IT_L3.Visible = True
                lblMsg_UBApprovalParty_IT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlUBCurrencyCode_IT_L3.SelectedValue.ToString <> "0" Then
            If ddlUBComparision_IT_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_IT_L3.Visible = True
                lblMsg_UBComparision_IT_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If txtUBAmount_IT_L3.Text = "" Then
                lblMsg_UBAmount_IT_L3.Visible = True
                lblMsg_UBAmount_IT_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If

            If ddlUBApprovalParty_IT_L3.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_IT_L3.Visible = True
                lblMsg_UBApprovalParty_IT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        'Level 4
        If txtUBAmount_IT_L4.Text <> "" Then
            If ddlUBComparision_IT_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_IT_L4.Visible = True
                lblMsg_UBComparision_IT_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If ddlUBCurrencyCode_IT_L4.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_IT_L4.Visible = True
                lblMsg_UBCurrencyCode_IT_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlUBApprovalParty_IT_L4.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_IT_L4.Visible = True
                lblMsg_UBApprovalParty_IT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlUBComparision_IT_L4.SelectedValue.ToString <> "Select One" Then
            If txtUBAmount_IT_L4.Text = "" Then
                lblMsg_UBAmount_IT_L4.Visible = True
                lblMsg_UBAmount_IT_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If
            If ddlUBCurrencyCode_IT_L4.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_IT_L4.Visible = True
                lblMsg_UBCurrencyCode_IT_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlUBApprovalParty_IT_L4.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_IT_L4.Visible = True
                lblMsg_UBApprovalParty_IT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlUBCurrencyCode_IT_L4.SelectedValue.ToString <> "0" Then
            If ddlUBComparision_IT_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_IT_L4.Visible = True
                lblMsg_UBComparision_IT_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If txtUBAmount_IT_L4.Text = "" Then
                lblMsg_UBAmount_IT_L4.Visible = True
                lblMsg_UBAmount_IT_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If

            If ddlUBApprovalParty_IT_L4.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_IT_L4.Visible = True
                lblMsg_UBApprovalParty_IT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If


        'Purchase, NON IT, UnBudgeted
        'Level 1
        If txtUBAmount_NonIT_L1.Text <> "" Then
            If ddlUBComparision_NonIT_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_NonIT_L1.Visible = True
                lblMsg_UBComparision_NonIT_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If ddlUBCurrencyCode_NonIT_L1.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_NonIT_L1.Visible = True
                lblMsg_UBCurrencyCode_NonIT_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlUBComparision_NonIT_L1.SelectedValue.ToString <> "Select One" Then
            If txtUBAmount_NonIT_L1.Text = "" Then
                lblMsg_UBAmount_NonIT_L1.Visible = True
                lblMsg_UBAmount_NonIT_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
            If ddlUBCurrencyCode_NonIT_L1.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_NonIT_L1.Visible = True
                lblMsg_UBCurrencyCode_NonIT_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlUBCurrencyCode_NonIT_L1.SelectedValue.ToString <> "0" Then
            If ddlUBComparision_NonIT_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_NonIT_L1.Visible = True
                lblMsg_UBComparision_NonIT_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If txtUBAmount_NonIT_L1.Text = "" Then
                lblMsg_UBAmount_NonIT_L1.Visible = True
                lblMsg_UBAmount_NonIT_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
        End If

        If ddlUBApprovalParty_NonIT_L1.SelectedValue.ToString = "" Then
            lblMsg_UBApprovalParty_NonIT_L1.Visible = True
            lblMsg_UBApprovalParty_NonIT_L1.Text = "Please select L1 Approval Party."
            blnResult = False
        Else
            If ddlUBApprovalParty2_NonIT_L1.SelectedValue.ToString <> "" Then
                If ddlUBApprovalParty_NonIT_L1.SelectedValue.ToString = ddlUBApprovalParty2_NonIT_L1.SelectedValue.ToString Then
                    lblMsg_UBApprovalParty_NonIT_L1.Visible = True
                    lblMsg_UBApprovalParty_NonIT_L1.Text = "Error - L1 Approval Party (1st and 2nd) cannot same."
                    blnResult = False
                End If
            End If
        End If

        If ddlUBApprovalParty_NonIT_L1.SelectedValue.ToString <> "" Then
            If ddlUBApprovalParty_NonIT_L1.SelectedValue.ToString = ddlUBApprovalParty_NonIT_L2.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_NonIT_L2.Visible = True
                lblMsg_UBApprovalParty_NonIT_L2.Text = "Error - Approval Party (L1 [1st] & L2) cannot be same."
                blnResult = False
            End If
            If ddlUBApprovalParty_NonIT_L1.SelectedValue.ToString = ddlUBApprovalParty_NonIT_L3.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_NonIT_L3.Visible = True
                lblMsg_UBApprovalParty_NonIT_L3.Text = "Error - Approval Party (L1 [1st] & L3) cannot be same."
                blnResult = False
            End If
            If ddlUBApprovalParty_NonIT_L1.SelectedValue.ToString = ddlUBApprovalParty_NonIT_L4.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_NonIT_L4.Visible = True
                lblMsg_UBApprovalParty_NonIT_L4.Text = "Error - Approval Party (L1 [1st] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlUBApprovalParty2_NonIT_L1.SelectedValue.ToString <> "" Then
            If ddlUBApprovalParty2_NonIT_L1.SelectedValue.ToString = ddlUBApprovalParty_NonIT_L2.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_NonIT_L2.Visible = True
                lblMsg_UBApprovalParty_NonIT_L2.Text = "Error - Approval Party (L1 [2nd] & L2) cannot be same."
                blnResult = False
            End If
            If ddlUBApprovalParty2_NonIT_L1.SelectedValue.ToString = ddlUBApprovalParty_NonIT_L3.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_NonIT_L3.Visible = True
                lblMsg_UBApprovalParty_NonIT_L3.Text = "Error - Approval Party (L1 [2nd] & L3) cannot be same."
                blnResult = False
            End If
            If ddlUBApprovalParty2_NonIT_L1.SelectedValue.ToString = ddlUBApprovalParty_NonIT_L4.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_NonIT_L4.Visible = True
                lblMsg_UBApprovalParty_NonIT_L4.Text = "Error - Approval Party (L1 [2nd] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlUBApprovalParty_NonIT_L2.SelectedValue.ToString <> "" Then
            If ddlUBApprovalParty_NonIT_L2.SelectedValue.ToString = ddlUBApprovalParty_NonIT_L3.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_NonIT_L3.Visible = True
                lblMsg_UBApprovalParty_NonIT_L3.Text = "Error - Approval Party (L2 & L3) cannot be same."
                blnResult = False
            End If
            If ddlUBApprovalParty_NonIT_L2.SelectedValue.ToString = ddlUBApprovalParty_NonIT_L4.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_NonIT_L4.Visible = True
                lblMsg_UBApprovalParty_NonIT_L4.Text = "Error - Approval Party (L2 & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlUBApprovalParty_NonIT_L3.SelectedValue.ToString <> "" Then
            If ddlUBApprovalParty_NonIT_L3.SelectedValue.ToString = ddlUBApprovalParty_NonIT_L4.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_NonIT_L4.Visible = True
                lblMsg_UBApprovalParty_NonIT_L4.Text = "Error - Approval Party (L3 & L4) cannot be same."
                blnResult = False
            End If
        End If

        'Level 2
        If txtUBAmount_NonIT_L2.Text <> "" Then
            If ddlUBComparision_NonIT_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_NonIT_L2.Visible = True
                lblMsg_UBComparision_NonIT_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If ddlUBCurrencyCode_NonIT_L2.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_NonIT_L2.Visible = True
                lblMsg_UBCurrencyCode_NonIT_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlUBApprovalParty_NonIT_L2.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_NonIT_L2.Visible = True
                lblMsg_UBApprovalParty_NonIT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlUBComparision_NonIT_L2.SelectedValue.ToString <> "Select One" Then
            If txtUBAmount_NonIT_L2.Text = "" Then
                lblMsg_UBAmount_NonIT_L2.Visible = True
                lblMsg_UBAmount_NonIT_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If
            If ddlUBCurrencyCode_NonIT_L2.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_NonIT_L2.Visible = True
                lblMsg_UBCurrencyCode_NonIT_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlUBApprovalParty_NonIT_L2.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_NonIT_L2.Visible = True
                lblMsg_UBApprovalParty_NonIT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlUBCurrencyCode_NonIT_L2.SelectedValue.ToString <> "0" Then
            If ddlUBComparision_NonIT_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_NonIT_L2.Visible = True
                lblMsg_UBComparision_NonIT_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If txtUBAmount_NonIT_L2.Text = "" Then
                lblMsg_UBAmount_NonIT_L2.Visible = True
                lblMsg_UBAmount_NonIT_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If

            If ddlUBApprovalParty_NonIT_L2.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_NonIT_L2.Visible = True
                lblMsg_UBApprovalParty_NonIT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        'Level 3
        If txtUBAmount_NonIT_L3.Text <> "" Then
            If ddlUBComparision_NonIT_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_NonIT_L3.Visible = True
                lblMsg_UBComparision_NonIT_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If ddlUBCurrencyCode_NonIT_L3.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_NonIT_L3.Visible = True
                lblMsg_UBCurrencyCode_NonIT_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlUBApprovalParty_NonIT_L3.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_NonIT_L3.Visible = True
                lblMsg_UBApprovalParty_NonIT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlUBComparision_NonIT_L3.SelectedValue.ToString <> "Select One" Then
            If txtUBAmount_NonIT_L3.Text = "" Then
                lblMsg_UBAmount_NonIT_L3.Visible = True
                lblMsg_UBAmount_NonIT_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If
            If ddlUBCurrencyCode_NonIT_L3.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_NonIT_L3.Visible = True
                lblMsg_UBCurrencyCode_NonIT_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlUBApprovalParty_NonIT_L3.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_NonIT_L3.Visible = True
                lblMsg_UBApprovalParty_NonIT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlUBCurrencyCode_NonIT_L3.SelectedValue.ToString <> "0" Then
            If ddlUBComparision_NonIT_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_NonIT_L3.Visible = True
                lblMsg_UBComparision_NonIT_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If txtUBAmount_NonIT_L3.Text = "" Then
                lblMsg_UBAmount_NonIT_L3.Visible = True
                lblMsg_UBAmount_NonIT_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If

            If ddlUBApprovalParty_NonIT_L3.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_NonIT_L3.Visible = True
                lblMsg_UBApprovalParty_NonIT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        'Level 4
        If txtUBAmount_NonIT_L4.Text <> "" Then
            If ddlUBComparision_NonIT_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_NonIT_L4.Visible = True
                lblMsg_UBComparision_NonIT_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If ddlUBCurrencyCode_NonIT_L4.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_NonIT_L4.Visible = True
                lblMsg_UBCurrencyCode_NonIT_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlUBApprovalParty_NonIT_L4.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_NonIT_L4.Visible = True
                lblMsg_UBApprovalParty_NonIT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlUBComparision_NonIT_L4.SelectedValue.ToString <> "Select One" Then
            If txtUBAmount_NonIT_L4.Text = "" Then
                lblMsg_UBAmount_NonIT_L4.Visible = True
                lblMsg_UBAmount_NonIT_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If
            If ddlUBCurrencyCode_NonIT_L4.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_NonIT_L4.Visible = True
                lblMsg_UBCurrencyCode_NonIT_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlUBApprovalParty_NonIT_L4.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_NonIT_L4.Visible = True
                lblMsg_UBApprovalParty_NonIT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlUBCurrencyCode_NonIT_L4.SelectedValue.ToString <> "0" Then
            If ddlUBComparision_NonIT_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_NonIT_L4.Visible = True
                lblMsg_UBComparision_NonIT_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If txtUBAmount_NonIT_L4.Text = "" Then
                lblMsg_UBAmount_NonIT_L4.Visible = True
                lblMsg_UBAmount_NonIT_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If

            If ddlUBApprovalParty_NonIT_L4.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_NonIT_L4.Visible = True
                lblMsg_UBApprovalParty_NonIT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        '''''''''''''''
        'Purchase, NON IT (MAJOR), UnBudgeted
        'Level 1
        If txtUBAmount_NonITMajor_L1.Text <> "" Then
            If ddlUBComparision_NonITMajor_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_NonITMajor_L1.Visible = True
                lblMsg_UBComparision_NonITMajor_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If ddlUBCurrencyCode_NonITMajor_L1.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_NonITMajor_L1.Visible = True
                lblMsg_UBCurrencyCode_NonITMajor_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlUBComparision_NonITMajor_L1.SelectedValue.ToString <> "Select One" Then
            If txtUBAmount_NonITMajor_L1.Text = "" Then
                lblMsg_UBAmount_NonITMajor_L1.Visible = True
                lblMsg_UBAmount_NonITMajor_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
            If ddlUBCurrencyCode_NonITMajor_L1.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_NonITMajor_L1.Visible = True
                lblMsg_UBCurrencyCode_NonITMajor_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlUBCurrencyCode_NonITMajor_L1.SelectedValue.ToString <> "0" Then
            If ddlUBComparision_NonITMajor_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_NonITMajor_L1.Visible = True
                lblMsg_UBComparision_NonITMajor_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If txtUBAmount_NonITMajor_L1.Text = "" Then
                lblMsg_UBAmount_NonITMajor_L1.Visible = True
                lblMsg_UBAmount_NonITMajor_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
        End If

        If ddlUBApprovalParty_NonITMajor_L1.SelectedValue.ToString = "" Then
            lblMsg_UBApprovalParty_NonITMajor_L1.Visible = True
            lblMsg_UBApprovalParty_NonITMajor_L1.Text = "Please select L1 Approval Party."
            blnResult = False
        Else
            If ddlUBApprovalParty2_NonITMajor_L1.SelectedValue.ToString <> "" Then
                If ddlUBApprovalParty_NonITMajor_L1.SelectedValue.ToString = ddlUBApprovalParty2_NonITMajor_L1.SelectedValue.ToString Then
                    lblMsg_UBApprovalParty_NonITMajor_L1.Visible = True
                    lblMsg_UBApprovalParty_NonITMajor_L1.Text = "Error - L1 Approval Party (1st and 2nd) cannot same."
                    blnResult = False
                End If
            End If
        End If

        If ddlUBApprovalParty_NonITMajor_L1.SelectedValue.ToString <> "" Then
            If ddlUBApprovalParty_NonITMajor_L1.SelectedValue.ToString = ddlUBApprovalParty_NonITMajor_L2.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_UBApprovalParty_NonITMajor_L2.Text = "Error - Approval Party (L1 [1st] & L2) cannot be same."
                blnResult = False
            End If
            If ddlUBApprovalParty_NonITMajor_L1.SelectedValue.ToString = ddlUBApprovalParty_NonITMajor_L3.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_UBApprovalParty_NonITMajor_L3.Text = "Error - Approval Party (L1 [1st] & L3) cannot be same."
                blnResult = False
            End If
            If ddlUBApprovalParty_NonITMajor_L1.SelectedValue.ToString = ddlUBApprovalParty_NonITMajor_L4.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_UBApprovalParty_NonITMajor_L4.Text = "Error - Approval Party (L1 [1st] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlUBApprovalParty2_NonITMajor_L1.SelectedValue.ToString <> "" Then
            If ddlUBApprovalParty2_NonITMajor_L1.SelectedValue.ToString = ddlUBApprovalParty_NonITMajor_L2.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_UBApprovalParty_NonITMajor_L2.Text = "Error - Approval Party (L1 [2nd] & L2) cannot be same."
                blnResult = False
            End If
            If ddlUBApprovalParty2_NonITMajor_L1.SelectedValue.ToString = ddlUBApprovalParty_NonITMajor_L3.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_UBApprovalParty_NonITMajor_L3.Text = "Error - Approval Party (L1 [2nd] & L3) cannot be same."
                blnResult = False
            End If
            If ddlUBApprovalParty2_NonITMajor_L1.SelectedValue.ToString = ddlUBApprovalParty_NonITMajor_L4.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_UBApprovalParty_NonITMajor_L4.Text = "Error - Approval Party (L1 [2nd] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlUBApprovalParty_NonITMajor_L2.SelectedValue.ToString <> "" Then
            If ddlUBApprovalParty_NonITMajor_L2.SelectedValue.ToString = ddlUBApprovalParty_NonITMajor_L3.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_UBApprovalParty_NonITMajor_L3.Text = "Error - Approval Party (L2 & L3) cannot be same."
                blnResult = False
            End If
            If ddlUBApprovalParty_NonITMajor_L2.SelectedValue.ToString = ddlUBApprovalParty_NonITMajor_L4.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_UBApprovalParty_NonITMajor_L4.Text = "Error - Approval Party (L2 & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlUBApprovalParty_NonITMajor_L3.SelectedValue.ToString <> "" Then
            If ddlUBApprovalParty_NonITMajor_L3.SelectedValue.ToString = ddlUBApprovalParty_NonITMajor_L4.SelectedValue.ToString Then
                lblMsg_UBApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_UBApprovalParty_NonITMajor_L4.Text = "Error - Approval Party (L3 & L4) cannot be same."
                blnResult = False
            End If
        End If

        'Level 2
        If txtUBAmount_NonITMajor_L2.Text <> "" Then
            If ddlUBComparision_NonITMajor_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_NonITMajor_L2.Visible = True
                lblMsg_UBComparision_NonITMajor_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If ddlUBCurrencyCode_NonITMajor_L2.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_NonITMajor_L2.Visible = True
                lblMsg_UBCurrencyCode_NonITMajor_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlUBApprovalParty_NonITMajor_L2.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_UBApprovalParty_NonITMajor_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlUBComparision_NonITMajor_L2.SelectedValue.ToString <> "Select One" Then
            If txtUBAmount_NonITMajor_L2.Text = "" Then
                lblMsg_UBAmount_NonITMajor_L2.Visible = True
                lblMsg_UBAmount_NonITMajor_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If
            If ddlUBCurrencyCode_NonITMajor_L2.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_NonITMajor_L2.Visible = True
                lblMsg_UBCurrencyCode_NonITMajor_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlUBApprovalParty_NonITMajor_L2.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_UBApprovalParty_NonITMajor_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlUBCurrencyCode_NonITMajor_L2.SelectedValue.ToString <> "0" Then
            If ddlUBComparision_NonITMajor_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_NonITMajor_L2.Visible = True
                lblMsg_UBComparision_NonITMajor_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If txtUBAmount_NonITMajor_L2.Text = "" Then
                lblMsg_UBAmount_NonITMajor_L2.Visible = True
                lblMsg_UBAmount_NonITMajor_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If

            If ddlUBApprovalParty_NonITMajor_L2.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_UBApprovalParty_NonITMajor_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        'Level 3
        If txtUBAmount_NonITMajor_L3.Text <> "" Then
            If ddlUBComparision_NonITMajor_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_NonITMajor_L3.Visible = True
                lblMsg_UBComparision_NonITMajor_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If ddlUBCurrencyCode_NonITMajor_L3.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_NonITMajor_L3.Visible = True
                lblMsg_UBCurrencyCode_NonITMajor_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlUBApprovalParty_NonITMajor_L3.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_UBApprovalParty_NonITMajor_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlUBComparision_NonITMajor_L3.SelectedValue.ToString <> "Select One" Then
            If txtUBAmount_NonITMajor_L3.Text = "" Then
                lblMsg_UBAmount_NonITMajor_L3.Visible = True
                lblMsg_UBAmount_NonITMajor_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If
            If ddlUBCurrencyCode_NonITMajor_L3.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_NonITMajor_L3.Visible = True
                lblMsg_UBCurrencyCode_NonITMajor_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlUBApprovalParty_NonITMajor_L3.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_UBApprovalParty_NonITMajor_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlUBCurrencyCode_NonITMajor_L3.SelectedValue.ToString <> "0" Then
            If ddlUBComparision_NonITMajor_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_NonITMajor_L3.Visible = True
                lblMsg_UBComparision_NonITMajor_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If txtUBAmount_NonITMajor_L3.Text = "" Then
                lblMsg_UBAmount_NonITMajor_L3.Visible = True
                lblMsg_UBAmount_NonITMajor_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If

            If ddlUBApprovalParty_NonITMajor_L3.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_UBApprovalParty_NonITMajor_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        'Level 4
        If txtUBAmount_NonITMajor_L4.Text <> "" Then
            If ddlUBComparision_NonITMajor_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_NonITMajor_L4.Visible = True
                lblMsg_UBComparision_NonITMajor_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If ddlUBCurrencyCode_NonITMajor_L4.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_NonITMajor_L4.Visible = True
                lblMsg_UBCurrencyCode_NonITMajor_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlUBApprovalParty_NonITMajor_L4.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_UBApprovalParty_NonITMajor_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlUBComparision_NonITMajor_L4.SelectedValue.ToString <> "Select One" Then
            If txtUBAmount_NonITMajor_L4.Text = "" Then
                lblMsg_UBAmount_NonITMajor_L4.Visible = True
                lblMsg_UBAmount_NonITMajor_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If
            If ddlUBCurrencyCode_NonITMajor_L4.SelectedValue.ToString = "0" Then
                lblMsg_UBCurrencyCode_NonITMajor_L4.Visible = True
                lblMsg_UBCurrencyCode_NonITMajor_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlUBApprovalParty_NonITMajor_L4.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_UBApprovalParty_NonITMajor_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlUBCurrencyCode_NonITMajor_L4.SelectedValue.ToString <> "0" Then
            If ddlUBComparision_NonITMajor_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_UBComparision_NonITMajor_L4.Visible = True
                lblMsg_UBComparision_NonITMajor_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If txtUBAmount_NonITMajor_L4.Text = "" Then
                lblMsg_UBAmount_NonITMajor_L4.Visible = True
                lblMsg_UBAmount_NonITMajor_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If

            If ddlUBApprovalParty_NonITMajor_L4.SelectedValue.ToString = "" Then
                lblMsg_UBApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_UBApprovalParty_NonITMajor_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If


        '''''''''''''''

        ''''''
        'Purchase, IT, Over Budget
        'Level 1 
        If txtOBAmount_IT_L1.Text <> "" Then
            If ddlOBComparision_IT_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_IT_L1.Visible = True
                lblMsg_OBComparision_IT_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If ddlOBCurrencyCode_IT_L1.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_IT_L1.Visible = True
                lblMsg_OBCurrencyCode_IT_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlOBComparision_IT_L1.SelectedValue.ToString <> "Select One" Then
            If txtOBAmount_IT_L1.Text = "" Then
                lblMsg_OBAmount_IT_L1.Visible = True
                lblMsg_OBAmount_IT_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
            If ddlOBCurrencyCode_IT_L1.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_IT_L1.Visible = True
                lblMsg_OBCurrencyCode_IT_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlOBCurrencyCode_IT_L1.SelectedValue.ToString <> "0" Then
            If ddlOBComparision_IT_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_IT_L1.Visible = True
                lblMsg_OBComparision_IT_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If txtOBAmount_IT_L1.Text = "" Then
                lblMsg_OBAmount_IT_L1.Visible = True
                lblMsg_OBAmount_IT_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
        End If

        If ddlOBApprovalParty_IT_L1.SelectedValue.ToString = "" Then
            lblMsg_OBApprovalParty_IT_L1.Visible = True
            lblMsg_OBApprovalParty_IT_L1.Text = "Please select L1 Approval Party."
            blnResult = False
        Else
            If ddlOBApprovalParty2_IT_L1.SelectedValue.ToString <> "" Then
                If ddlOBApprovalParty_IT_L1.SelectedValue.ToString = ddlOBApprovalParty2_IT_L1.SelectedValue.ToString Then
                    lblMsg_OBApprovalParty_IT_L1.Visible = True
                    lblMsg_OBApprovalParty_IT_L1.Text = "Error - L1 Approval Party (1st and 2nd) cannot same."
                    blnResult = False
                End If
            End If
        End If

        If ddlOBApprovalParty_IT_L1.SelectedValue.ToString <> "" Then
            If ddlOBApprovalParty_IT_L1.SelectedValue.ToString = ddlOBApprovalParty_IT_L2.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_IT_L2.Visible = True
                lblMsg_OBApprovalParty_IT_L2.Text = "Error - Approval Party (L1 [1st] & L2) cannot be same."
                blnResult = False
            End If
            If ddlOBApprovalParty_IT_L1.SelectedValue.ToString = ddlOBApprovalParty_IT_L3.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_IT_L3.Visible = True
                lblMsg_OBApprovalParty_IT_L3.Text = "Error - Approval Party (L1 [1st] & L3) cannot be same."
                blnResult = False
            End If
            If ddlOBApprovalParty_IT_L1.SelectedValue.ToString = ddlOBApprovalParty_IT_L4.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_IT_L4.Visible = True
                lblMsg_OBApprovalParty_IT_L4.Text = "Error - Approval Party (L1 [1st] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlOBApprovalParty2_IT_L1.SelectedValue.ToString <> "" Then
            If ddlOBApprovalParty2_IT_L1.SelectedValue.ToString = ddlOBApprovalParty_IT_L2.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_IT_L2.Visible = True
                lblMsg_OBApprovalParty_IT_L2.Text = "Error - Approval Party (L1 [2nd] & L2) cannot be same."
                blnResult = False
            End If
            If ddlOBApprovalParty2_IT_L1.SelectedValue.ToString = ddlOBApprovalParty_IT_L3.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_IT_L3.Visible = True
                lblMsg_OBApprovalParty_IT_L3.Text = "Error - Approval Party (L1 [2nd] & L3) cannot be same."
                blnResult = False
            End If
            If ddlOBApprovalParty2_IT_L1.SelectedValue.ToString = ddlOBApprovalParty_IT_L4.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_IT_L4.Visible = True
                lblMsg_OBApprovalParty_IT_L4.Text = "Error - Approval Party (L1 [2nd] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlOBApprovalParty_IT_L2.SelectedValue.ToString <> "" Then
            If ddlOBApprovalParty_IT_L2.SelectedValue.ToString = ddlOBApprovalParty_IT_L3.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_IT_L3.Visible = True
                lblMsg_OBApprovalParty_IT_L3.Text = "Error - Approval Party (L2 & L3) cannot be same."
                blnResult = False
            End If
            If ddlOBApprovalParty_IT_L2.SelectedValue.ToString = ddlOBApprovalParty_IT_L4.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_IT_L4.Visible = True
                lblMsg_OBApprovalParty_IT_L4.Text = "Error - Approval Party (L2 & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlOBApprovalParty_IT_L3.SelectedValue.ToString <> "" Then
            If ddlOBApprovalParty_IT_L3.SelectedValue.ToString = ddlOBApprovalParty_IT_L4.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_IT_L4.Visible = True
                lblMsg_OBApprovalParty_IT_L4.Text = "Error - Approval Party (L3 & L4) cannot be same."
                blnResult = False
            End If
        End If

        'Level 2
        If txtOBAmount_IT_L2.Text <> "" Then
            If ddlOBComparision_IT_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_IT_L2.Visible = True
                lblMsg_OBComparision_IT_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If ddlOBCurrencyCode_IT_L2.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_IT_L2.Visible = True
                lblMsg_OBCurrencyCode_IT_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlOBComparision_NonIT_L2.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_IT_L2.Visible = True
                lblMsg_OBApprovalParty_IT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlOBComparision_IT_L2.SelectedValue.ToString <> "Select One" Then
            If txtOBAmount_IT_L2.Text = "" Then
                lblMsg_OBAmount_IT_L2.Visible = True
                lblMsg_OBAmount_IT_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If
            If ddlOBCurrencyCode_IT_L2.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_IT_L2.Visible = True
                lblMsg_OBCurrencyCode_IT_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlOBComparision_NonIT_L2.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_IT_L2.Visible = True
                lblMsg_OBApprovalParty_IT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlOBCurrencyCode_IT_L2.SelectedValue.ToString <> "0" Then
            If ddlOBComparision_IT_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_IT_L2.Visible = True
                lblMsg_OBComparision_IT_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If txtOBAmount_IT_L2.Text = "" Then
                lblMsg_OBAmount_IT_L2.Visible = True
                lblMsg_OBAmount_IT_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If

            If ddlOBComparision_NonIT_L2.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_IT_L2.Visible = True
                lblMsg_OBApprovalParty_IT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        'Level 3
        If txtOBAmount_IT_L3.Text <> "" Then
            If ddlOBComparision_IT_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_IT_L3.Visible = True
                lblMsg_OBComparision_IT_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If ddlOBCurrencyCode_IT_L3.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_IT_L3.Visible = True
                lblMsg_OBCurrencyCode_IT_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlOBComparision_NonIT_L3.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_IT_L3.Visible = True
                lblMsg_OBApprovalParty_IT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlOBComparision_IT_L3.SelectedValue.ToString <> "Select One" Then
            If txtOBAmount_IT_L3.Text = "" Then
                lblMsg_OBAmount_IT_L3.Visible = True
                lblMsg_OBAmount_IT_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If
            If ddlOBCurrencyCode_IT_L3.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_IT_L3.Visible = True
                lblMsg_OBCurrencyCode_IT_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlOBComparision_NonIT_L3.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_IT_L3.Visible = True
                lblMsg_OBApprovalParty_IT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlOBCurrencyCode_IT_L3.SelectedValue.ToString <> "0" Then
            If ddlOBComparision_IT_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_IT_L3.Visible = True
                lblMsg_OBComparision_IT_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If txtOBAmount_IT_L3.Text = "" Then
                lblMsg_OBAmount_IT_L3.Visible = True
                lblMsg_OBAmount_IT_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If

            If ddlOBComparision_NonIT_L3.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_IT_L3.Visible = True
                lblMsg_OBApprovalParty_IT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        'Level 4
        If txtOBAmount_IT_L4.Text <> "" Then
            If ddlOBComparision_IT_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_IT_L4.Visible = True
                lblMsg_OBComparision_IT_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If ddlOBCurrencyCode_IT_L4.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_IT_L4.Visible = True
                lblMsg_OBCurrencyCode_IT_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlOBComparision_NonIT_L4.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_IT_L4.Visible = True
                lblMsg_OBApprovalParty_IT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlOBComparision_IT_L4.SelectedValue.ToString <> "Select One" Then
            If txtOBAmount_IT_L4.Text = "" Then
                lblMsg_OBAmount_IT_L4.Visible = True
                lblMsg_OBAmount_IT_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If
            If ddlOBCurrencyCode_IT_L4.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_IT_L4.Visible = True
                lblMsg_OBCurrencyCode_IT_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlOBComparision_NonIT_L4.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_IT_L4.Visible = True
                lblMsg_OBApprovalParty_IT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlOBCurrencyCode_IT_L4.SelectedValue.ToString <> "0" Then
            If ddlOBComparision_IT_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_IT_L4.Visible = True
                lblMsg_OBComparision_IT_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If txtOBAmount_IT_L4.Text = "" Then
                lblMsg_OBAmount_IT_L4.Visible = True
                lblMsg_OBAmount_IT_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If

            If ddlOBComparision_NonIT_L4.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_IT_L4.Visible = True
                lblMsg_OBApprovalParty_IT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If


        'Purchase, NON IT, Over Budget
        'Level 1
        If txtOBAmount_NonIT_L1.Text <> "" Then
            If ddlOBComparision_NonIT_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_NonIT_L1.Visible = True
                lblMsg_OBComparision_NonIT_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If ddlOBCurrencyCode_NonIT_L1.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_NonIT_L1.Visible = True
                lblMsg_OBCurrencyCode_NonIT_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlOBComparision_NonIT_L1.SelectedValue.ToString <> "Select One" Then
            If txtOBAmount_NonIT_L1.Text = "" Then
                lblMsg_OBAmount_NonIT_L1.Visible = True
                lblMsg_OBAmount_NonIT_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
            If ddlOBCurrencyCode_NonIT_L1.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_NonIT_L1.Visible = True
                lblMsg_OBCurrencyCode_NonIT_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlOBCurrencyCode_NonIT_L1.SelectedValue.ToString <> "0" Then
            If ddlOBComparision_NonIT_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_NonIT_L1.Visible = True
                lblMsg_OBComparision_NonIT_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If txtOBAmount_NonIT_L1.Text = "" Then
                lblMsg_OBAmount_NonIT_L1.Visible = True
                lblMsg_OBAmount_NonIT_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
        End If

        If ddlOBApprovalParty_NonIT_L1.SelectedValue.ToString = "" Then
            lblMsg_OBApprovalParty_NonIT_L1.Visible = True
            lblMsg_OBApprovalParty_NonIT_L1.Text = "Please select L1 Approval Party."
            blnResult = False
        Else
            If ddlOBApprovalParty2_NonIT_L1.SelectedValue.ToString <> "" Then
                If ddlOBApprovalParty_NonIT_L1.SelectedValue.ToString = ddlOBApprovalParty2_NonIT_L1.SelectedValue.ToString Then
                    lblMsg_OBApprovalParty_NonIT_L1.Visible = True
                    lblMsg_OBApprovalParty_NonIT_L1.Text = "Error - L1 Approval Party (1st and 2nd) cannot same."
                    blnResult = False
                End If
            End If
        End If

        If ddlOBApprovalParty_NonIT_L1.SelectedValue.ToString <> "" Then
            If ddlOBApprovalParty_NonIT_L1.SelectedValue.ToString = ddlOBApprovalParty_NonIT_L2.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_NonIT_L2.Visible = True
                lblMsg_OBApprovalParty_NonIT_L2.Text = "Error - Approval Party (L1 [1st] & L2) cannot be same."
                blnResult = False
            End If
            If ddlOBApprovalParty_NonIT_L1.SelectedValue.ToString = ddlOBApprovalParty_NonIT_L3.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_NonIT_L3.Visible = True
                lblMsg_OBApprovalParty_NonIT_L3.Text = "Error - Approval Party (L1 [1st] & L3) cannot be same."
                blnResult = False
            End If
            If ddlOBApprovalParty_NonIT_L1.SelectedValue.ToString = ddlOBApprovalParty_NonIT_L4.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_NonIT_L4.Visible = True
                lblMsg_OBApprovalParty_NonIT_L4.Text = "Error - Approval Party (L1 [1st] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlOBApprovalParty2_NonIT_L1.SelectedValue.ToString <> "" Then
            If ddlOBApprovalParty2_NonIT_L1.SelectedValue.ToString = ddlOBApprovalParty_NonIT_L2.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_NonIT_L2.Visible = True
                lblMsg_OBApprovalParty_NonIT_L2.Text = "Error - Approval Party (L1 [2nd] & L2) cannot be same."
                blnResult = False
            End If
            If ddlOBApprovalParty2_NonIT_L1.SelectedValue.ToString = ddlOBApprovalParty_NonIT_L3.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_NonIT_L3.Visible = True
                lblMsg_OBApprovalParty_NonIT_L3.Text = "Error - Approval Party (L1 [2nd] & L3) cannot be same."
                blnResult = False
            End If
            If ddlOBApprovalParty2_NonIT_L1.SelectedValue.ToString = ddlOBApprovalParty_NonIT_L4.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_NonIT_L4.Visible = True
                lblMsg_OBApprovalParty_NonIT_L4.Text = "Error - Approval Party (L1 [2nd] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlOBApprovalParty_NonIT_L2.SelectedValue.ToString <> "" Then
            If ddlOBApprovalParty_NonIT_L2.SelectedValue.ToString = ddlOBApprovalParty_NonIT_L3.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_NonIT_L3.Visible = True
                lblMsg_OBApprovalParty_NonIT_L3.Text = "Error - Approval Party (L2 & L3) cannot be same."
                blnResult = False
            End If
            If ddlOBApprovalParty_NonIT_L2.SelectedValue.ToString = ddlOBApprovalParty_NonIT_L4.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_NonIT_L4.Visible = True
                lblMsg_OBApprovalParty_NonIT_L4.Text = "Error - Approval Party (L2 & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlOBApprovalParty_NonIT_L3.SelectedValue.ToString <> "" Then
            If ddlOBApprovalParty_NonIT_L3.SelectedValue.ToString = ddlOBApprovalParty_NonIT_L4.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_NonIT_L4.Visible = True
                lblMsg_OBApprovalParty_NonIT_L4.Text = "Error - Approval Party (L3 & L4) cannot be same."
                blnResult = False
            End If
        End If

        'Level 2
        If txtOBAmount_NonIT_L2.Text <> "" Then
            If ddlOBComparision_NonIT_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_NonIT_L2.Visible = True
                lblMsg_OBComparision_NonIT_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If ddlOBCurrencyCode_NonIT_L2.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_NonIT_L2.Visible = True
                lblMsg_OBCurrencyCode_NonIT_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlOBApprovalParty_NonIT_L2.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_NonIT_L2.Visible = True
                lblMsg_OBApprovalParty_NonIT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlOBComparision_NonIT_L2.SelectedValue.ToString <> "Select One" Then
            If txtOBAmount_NonIT_L2.Text = "" Then
                lblMsg_OBAmount_NonIT_L2.Visible = True
                lblMsg_OBAmount_NonIT_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If
            If ddlOBCurrencyCode_NonIT_L2.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_NonIT_L2.Visible = True
                lblMsg_OBCurrencyCode_NonIT_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlOBApprovalParty_NonIT_L2.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_NonIT_L2.Visible = True
                lblMsg_OBApprovalParty_NonIT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlOBCurrencyCode_NonIT_L2.SelectedValue.ToString <> "0" Then
            If ddlOBComparision_NonIT_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_NonIT_L2.Visible = True
                lblMsg_OBComparision_NonIT_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If txtOBAmount_NonIT_L2.Text = "" Then
                lblMsg_OBAmount_NonIT_L2.Visible = True
                lblMsg_OBAmount_NonIT_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If

            If ddlOBApprovalParty_NonIT_L2.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_NonIT_L2.Visible = True
                lblMsg_OBApprovalParty_NonIT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        'Level 3
        If txtOBAmount_NonIT_L3.Text <> "" Then
            If ddlOBComparision_NonIT_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_NonIT_L3.Visible = True
                lblMsg_OBComparision_NonIT_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If ddlOBCurrencyCode_NonIT_L3.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_NonIT_L3.Visible = True
                lblMsg_OBCurrencyCode_NonIT_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlOBApprovalParty_NonIT_L3.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_NonIT_L3.Visible = True
                lblMsg_OBApprovalParty_NonIT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlOBComparision_NonIT_L3.SelectedValue.ToString <> "Select One" Then
            If txtOBAmount_NonIT_L3.Text = "" Then
                lblMsg_OBAmount_NonIT_L3.Visible = True
                lblMsg_OBAmount_NonIT_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If
            If ddlOBCurrencyCode_NonIT_L3.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_NonIT_L3.Visible = True
                lblMsg_OBCurrencyCode_NonIT_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlOBApprovalParty_NonIT_L3.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_NonIT_L3.Visible = True
                lblMsg_OBApprovalParty_NonIT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlOBCurrencyCode_NonIT_L3.SelectedValue.ToString <> "0" Then
            If ddlOBComparision_NonIT_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_NonIT_L3.Visible = True
                lblMsg_OBComparision_NonIT_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If txtOBAmount_NonIT_L3.Text = "" Then
                lblMsg_OBAmount_NonIT_L3.Visible = True
                lblMsg_OBAmount_NonIT_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If

            If ddlOBApprovalParty_NonIT_L3.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_NonIT_L3.Visible = True
                lblMsg_OBApprovalParty_NonIT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        'Level 4
        If txtOBAmount_NonIT_L4.Text <> "" Then
            If ddlOBComparision_NonIT_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_NonIT_L4.Visible = True
                lblMsg_OBComparision_NonIT_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If ddlOBCurrencyCode_NonIT_L4.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_NonIT_L4.Visible = True
                lblMsg_OBCurrencyCode_NonIT_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlOBApprovalParty_NonIT_L4.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_NonIT_L4.Visible = True
                lblMsg_OBApprovalParty_NonIT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlOBComparision_NonIT_L4.SelectedValue.ToString <> "Select One" Then
            If txtOBAmount_NonIT_L4.Text = "" Then
                lblMsg_OBAmount_NonIT_L4.Visible = True
                lblMsg_OBAmount_NonIT_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If
            If ddlOBCurrencyCode_NonIT_L4.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_NonIT_L4.Visible = True
                lblMsg_OBCurrencyCode_NonIT_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlOBApprovalParty_NonIT_L4.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_NonIT_L4.Visible = True
                lblMsg_OBApprovalParty_NonIT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlOBCurrencyCode_NonIT_L4.SelectedValue.ToString <> "0" Then
            If ddlOBComparision_NonIT_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_NonIT_L4.Visible = True
                lblMsg_OBComparision_NonIT_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If txtOBAmount_NonIT_L4.Text = "" Then
                lblMsg_OBAmount_NonIT_L4.Visible = True
                lblMsg_OBAmount_NonIT_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If

            If ddlOBApprovalParty_NonIT_L4.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_NonIT_L4.Visible = True
                lblMsg_OBApprovalParty_NonIT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        '''''''''''''''
        'Purchase, NON IT (MAJOR), Over Budget
        'Level 1
        If txtOBAmount_NonITMajor_L1.Text <> "" Then
            If ddlOBComparision_NonITMajor_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_NonITMajor_L1.Visible = True
                lblMsg_OBComparision_NonITMajor_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If ddlOBCurrencyCode_NonITMajor_L1.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_NonITMajor_L1.Visible = True
                lblMsg_OBCurrencyCode_NonITMajor_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlOBComparision_NonITMajor_L1.SelectedValue.ToString <> "Select One" Then
            If txtOBAmount_NonITMajor_L1.Text = "" Then
                lblMsg_OBAmount_NonITMajor_L1.Visible = True
                lblMsg_OBAmount_NonITMajor_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
            If ddlOBCurrencyCode_NonITMajor_L1.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_NonITMajor_L1.Visible = True
                lblMsg_OBCurrencyCode_NonITMajor_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlOBCurrencyCode_NonITMajor_L1.SelectedValue.ToString <> "0" Then
            If ddlOBComparision_NonITMajor_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_NonITMajor_L1.Visible = True
                lblMsg_OBComparision_NonITMajor_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If txtOBAmount_NonITMajor_L1.Text = "" Then
                lblMsg_OBAmount_NonITMajor_L1.Visible = True
                lblMsg_OBAmount_NonITMajor_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
        End If

        If ddlOBApprovalParty_NonITMajor_L1.SelectedValue.ToString = "" Then
            lblMsg_OBApprovalParty_NonITMajor_L1.Visible = True
            lblMsg_OBApprovalParty_NonITMajor_L1.Text = "Please select L1 Approval Party."
            blnResult = False
        Else
            If ddlOBApprovalParty2_NonITMajor_L1.SelectedValue.ToString <> "" Then
                If ddlOBApprovalParty_NonITMajor_L1.SelectedValue.ToString = ddlOBApprovalParty2_NonITMajor_L1.SelectedValue.ToString Then
                    lblMsg_OBApprovalParty_NonITMajor_L1.Visible = True
                    lblMsg_OBApprovalParty_NonITMajor_L1.Text = "Error - L1 Approval Party (1st and 2nd) cannot same."
                    blnResult = False
                End If
            End If
        End If

        If ddlOBApprovalParty_NonITMajor_L1.SelectedValue.ToString <> "" Then
            If ddlOBApprovalParty_NonITMajor_L1.SelectedValue.ToString = ddlOBApprovalParty_NonITMajor_L2.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_OBApprovalParty_NonITMajor_L2.Text = "Error - Approval Party (L1 [1st] & L2) cannot be same."
                blnResult = False
            End If
            If ddlOBApprovalParty_NonITMajor_L1.SelectedValue.ToString = ddlOBApprovalParty_NonITMajor_L3.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_OBApprovalParty_NonITMajor_L3.Text = "Error - Approval Party (L1 [1st] & L3) cannot be same."
                blnResult = False
            End If
            If ddlOBApprovalParty_NonITMajor_L1.SelectedValue.ToString = ddlOBApprovalParty_NonITMajor_L4.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_OBApprovalParty_NonITMajor_L4.Text = "Error - Approval Party (L1 [1st] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlOBApprovalParty2_NonITMajor_L1.SelectedValue.ToString <> "" Then
            If ddlOBApprovalParty2_NonITMajor_L1.SelectedValue.ToString = ddlOBApprovalParty_NonITMajor_L2.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_OBApprovalParty_NonITMajor_L2.Text = "Error - Approval Party (L1 [2nd] & L2) cannot be same."
                blnResult = False
            End If
            If ddlOBApprovalParty2_NonITMajor_L1.SelectedValue.ToString = ddlOBApprovalParty_NonITMajor_L3.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_OBApprovalParty_NonITMajor_L3.Text = "Error - Approval Party (L1 [2nd] & L3) cannot be same."
                blnResult = False
            End If
            If ddlOBApprovalParty2_NonITMajor_L1.SelectedValue.ToString = ddlOBApprovalParty_NonITMajor_L4.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_OBApprovalParty_NonITMajor_L4.Text = "Error - Approval Party (L1 [2nd] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlOBApprovalParty_NonITMajor_L2.SelectedValue.ToString <> "" Then
            If ddlOBApprovalParty_NonITMajor_L2.SelectedValue.ToString = ddlOBApprovalParty_NonITMajor_L3.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_OBApprovalParty_NonITMajor_L3.Text = "Error - Approval Party (L2 & L3) cannot be same."
                blnResult = False
            End If
            If ddlOBApprovalParty_NonITMajor_L2.SelectedValue.ToString = ddlOBApprovalParty_NonITMajor_L4.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_OBApprovalParty_NonITMajor_L4.Text = "Error - Approval Party (L2 & L4) cannot be same."
                blnResult = False
            End If
        End If
        If ddlOBApprovalParty_NonITMajor_L3.SelectedValue.ToString <> "" Then
            If ddlOBApprovalParty_NonITMajor_L3.SelectedValue.ToString = ddlOBApprovalParty_NonITMajor_L4.SelectedValue.ToString Then
                lblMsg_OBApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_OBApprovalParty_NonITMajor_L4.Text = "Error - Approval Party (L3 & L4) cannot be same."
                blnResult = False
            End If
        End If



        'Level 2
        If txtOBAmount_NonITMajor_L2.Text <> "" Then
            If ddlOBComparision_NonITMajor_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_NonITMajor_L2.Visible = True
                lblMsg_OBComparision_NonITMajor_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If ddlOBCurrencyCode_NonITMajor_L2.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_NonITMajor_L2.Visible = True
                lblMsg_OBCurrencyCode_NonITMajor_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlOBApprovalParty_NonITMajor_L2.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_OBApprovalParty_NonITMajor_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlOBComparision_NonITMajor_L2.SelectedValue.ToString <> "Select One" Then
            If txtOBAmount_NonITMajor_L2.Text = "" Then
                lblMsg_OBAmount_NonITMajor_L2.Visible = True
                lblMsg_OBAmount_NonITMajor_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If
            If ddlOBCurrencyCode_NonITMajor_L2.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_NonITMajor_L2.Visible = True
                lblMsg_OBCurrencyCode_NonITMajor_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlOBApprovalParty_NonITMajor_L2.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_OBApprovalParty_NonITMajor_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlOBCurrencyCode_NonITMajor_L2.SelectedValue.ToString <> "0" Then
            If ddlOBComparision_NonITMajor_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_NonITMajor_L2.Visible = True
                lblMsg_OBComparision_NonITMajor_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If txtOBAmount_NonITMajor_L2.Text = "" Then
                lblMsg_OBAmount_NonITMajor_L2.Visible = True
                lblMsg_OBAmount_NonITMajor_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If

            If ddlOBApprovalParty_NonITMajor_L2.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_OBApprovalParty_NonITMajor_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        'Level 3
        If txtOBAmount_NonITMajor_L3.Text <> "" Then
            If ddlOBComparision_NonITMajor_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_NonITMajor_L3.Visible = True
                lblMsg_OBComparision_NonITMajor_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If ddlOBCurrencyCode_NonITMajor_L3.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_NonITMajor_L3.Visible = True
                lblMsg_OBCurrencyCode_NonITMajor_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlOBApprovalParty_NonITMajor_L3.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_OBApprovalParty_NonITMajor_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlOBComparision_NonITMajor_L3.SelectedValue.ToString <> "Select One" Then
            If txtOBAmount_NonITMajor_L3.Text = "" Then
                lblMsg_OBAmount_NonITMajor_L3.Visible = True
                lblMsg_OBAmount_NonITMajor_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If
            If ddlOBCurrencyCode_NonITMajor_L3.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_NonITMajor_L3.Visible = True
                lblMsg_OBCurrencyCode_NonITMajor_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlOBApprovalParty_NonITMajor_L3.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_OBApprovalParty_NonITMajor_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlOBCurrencyCode_NonITMajor_L3.SelectedValue.ToString <> "0" Then
            If ddlOBComparision_NonITMajor_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_NonITMajor_L3.Visible = True
                lblMsg_OBComparision_NonITMajor_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If txtOBAmount_NonITMajor_L3.Text = "" Then
                lblMsg_OBAmount_NonITMajor_L3.Visible = True
                lblMsg_OBAmount_NonITMajor_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If

            If ddlOBApprovalParty_NonITMajor_L3.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_OBApprovalParty_NonITMajor_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        'Level 4
        If txtOBAmount_NonITMajor_L4.Text <> "" Then
            If ddlOBComparision_NonITMajor_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_NonITMajor_L4.Visible = True
                lblMsg_OBComparision_NonITMajor_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If ddlOBCurrencyCode_NonITMajor_L4.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_NonITMajor_L4.Visible = True
                lblMsg_OBCurrencyCode_NonITMajor_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlOBApprovalParty_NonITMajor_L4.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_OBApprovalParty_NonITMajor_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlOBComparision_NonITMajor_L4.SelectedValue.ToString <> "Select One" Then
            If txtOBAmount_NonITMajor_L4.Text = "" Then
                lblMsg_OBAmount_NonITMajor_L4.Visible = True
                lblMsg_OBAmount_NonITMajor_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If
            If ddlOBCurrencyCode_NonITMajor_L4.SelectedValue.ToString = "0" Then
                lblMsg_OBCurrencyCode_NonITMajor_L4.Visible = True
                lblMsg_OBCurrencyCode_NonITMajor_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlOBApprovalParty_NonITMajor_L4.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_OBApprovalParty_NonITMajor_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlOBCurrencyCode_NonITMajor_L4.SelectedValue.ToString <> "0" Then
            If ddlOBComparision_NonITMajor_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_OBComparision_NonITMajor_L4.Visible = True
                lblMsg_OBComparision_NonITMajor_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If txtOBAmount_NonITMajor_L4.Text = "" Then
                lblMsg_OBAmount_NonITMajor_L4.Visible = True
                lblMsg_OBAmount_NonITMajor_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If

            If ddlOBApprovalParty_NonITMajor_L4.SelectedValue.ToString = "" Then
                lblMsg_OBApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_OBApprovalParty_NonITMajor_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If




        ''''''''''''''


        'WDO - IT
        'Level 1 
        If txtDWAmount_IT_L1.Text <> "" Then
            If ddlDWComparision_IT_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_IT_L1.Visible = True
                lblMsg_DWComparision_IT_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If ddlDWCurrencyCode_IT_L1.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_IT_L1.Visible = True
                lblMsg_DWCurrencyCode_IT_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlDWComparision_IT_L1.SelectedValue.ToString <> "Select One" Then
            If txtDWAmount_IT_L1.Text = "" Then
                lblMsg_DWAmount_IT_L1.Visible = True
                lblMsg_DWAmount_IT_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
            If ddlDWCurrencyCode_IT_L1.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_IT_L1.Visible = True
                lblMsg_DWCurrencyCode_IT_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlDWCurrencyCode_IT_L1.SelectedValue.ToString <> "0" Then
            If ddlDWComparision_IT_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_IT_L1.Visible = True
                lblMsg_DWComparision_IT_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If txtDWAmount_IT_L1.Text = "" Then
                lblMsg_DWAmount_IT_L1.Visible = True
                lblMsg_DWAmount_IT_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
        End If

        If ddlDWApprovalParty_IT_L1.SelectedValue.ToString = "" Then
            lblMsg_DWApprovalParty_IT_L1.Visible = True
            lblMsg_DWApprovalParty_IT_L1.Text = "Please select L1 Approval Party."
            blnResult = False
        Else
            If ddlDWApprovalParty2_IT_L1.SelectedValue.ToString <> "" Then
                If ddlDWApprovalParty_IT_L1.SelectedValue.ToString = ddlDWApprovalParty2_IT_L1.SelectedValue.ToString Then
                    lblMsg_DWApprovalParty_IT_L1.Visible = True
                    lblMsg_DWApprovalParty_IT_L1.Text = "Error - L1 Approval Party (1st and 2nd) cannot same."
                    blnResult = False
                End If
            End If
        End If

        If ddlDWApprovalParty_IT_L1.SelectedValue.ToString <> "" Then
            If ddlDWApprovalParty_IT_L1.SelectedValue.ToString = ddlDWApprovalParty_IT_L2.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_IT_L2.Visible = True
                lblMsg_DWApprovalParty_IT_L2.Text = "Error - Approval Party (L1 [1st] & L2) cannot be same."
                blnResult = False
            End If
            If ddlDWApprovalParty_IT_L1.SelectedValue.ToString = ddlDWApprovalParty_IT_L3.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_IT_L3.Visible = True
                lblMsg_DWApprovalParty_IT_L3.Text = "Error - Approval Party (L1 [1st] & L3) cannot be same."
                blnResult = False
            End If
            If ddlDWApprovalParty_IT_L1.SelectedValue.ToString = ddlDWApprovalParty_IT_L4.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_IT_L4.Visible = True
                lblMsg_DWApprovalParty_IT_L4.Text = "Error - Approval Party (L1 [1st] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlDWApprovalParty2_IT_L1.SelectedValue.ToString <> "" Then
            If ddlDWApprovalParty2_IT_L1.SelectedValue.ToString = ddlDWApprovalParty_IT_L2.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_IT_L2.Visible = True
                lblMsg_DWApprovalParty_IT_L2.Text = "Error - Approval Party (L1 [2nd] & L2) cannot be same."
                blnResult = False
            End If
            If ddlDWApprovalParty2_IT_L1.SelectedValue.ToString = ddlDWApprovalParty_IT_L3.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_IT_L3.Visible = True
                lblMsg_DWApprovalParty_IT_L3.Text = "Error - Approval Party (L1 [2nd] & L3) cannot be same."
                blnResult = False
            End If
            If ddlDWApprovalParty2_IT_L1.SelectedValue.ToString = ddlDWApprovalParty_IT_L4.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_IT_L4.Visible = True
                lblMsg_DWApprovalParty_IT_L4.Text = "Error - Approval Party (L1 [2nd] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlDWApprovalParty_IT_L2.SelectedValue.ToString <> "" Then
            If ddlDWApprovalParty_IT_L2.SelectedValue.ToString = ddlDWApprovalParty_IT_L3.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_IT_L3.Visible = True
                lblMsg_DWApprovalParty_IT_L3.Text = "Error - Approval Party (L2 & L3) cannot be same."
                blnResult = False
            End If
            If ddlDWApprovalParty_IT_L2.SelectedValue.ToString = ddlDWApprovalParty_IT_L4.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_IT_L4.Visible = True
                lblMsg_DWApprovalParty_IT_L4.Text = "Error - Approval Party (L2 & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlDWApprovalParty_IT_L3.SelectedValue.ToString <> "" Then
            If ddlDWApprovalParty_IT_L3.SelectedValue.ToString = ddlDWApprovalParty_IT_L4.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_IT_L4.Visible = True
                lblMsg_DWApprovalParty_IT_L4.Text = "Error - Approval Party (L3 & L4) cannot be same."
                blnResult = False
            End If
        End If


        'Level 2
        If txtDWAmount_IT_L2.Text <> "" Then
            If ddlDWComparision_IT_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_IT_L2.Visible = True
                lblMsg_DWComparision_IT_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If ddlDWCurrencyCode_IT_L2.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_IT_L2.Visible = True
                lblMsg_DWCurrencyCode_IT_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlDWApprovalParty_IT_L2.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_IT_L2.Visible = True
                lblMsg_DWApprovalParty_IT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlDWComparision_IT_L2.SelectedValue.ToString <> "Select One" Then
            If txtDWAmount_IT_L2.Text = "" Then
                lblMsg_DWAmount_IT_L2.Visible = True
                lblMsg_DWAmount_IT_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If
            If ddlDWCurrencyCode_IT_L2.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_IT_L2.Visible = True
                lblMsg_DWCurrencyCode_IT_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlDWApprovalParty_IT_L2.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_IT_L2.Visible = True
                lblMsg_DWApprovalParty_IT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlDWCurrencyCode_IT_L2.SelectedValue.ToString <> "0" Then
            If ddlDWComparision_IT_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_IT_L2.Visible = True
                lblMsg_DWComparision_IT_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If txtDWAmount_IT_L2.Text = "" Then
                lblMsg_DWAmount_IT_L2.Visible = True
                lblMsg_DWAmount_IT_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If

            If ddlDWApprovalParty_IT_L2.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_IT_L2.Visible = True
                lblMsg_DWApprovalParty_IT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        'Level 3
        If txtDWAmount_IT_L3.Text <> "" Then
            If ddlDWComparision_IT_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_IT_L3.Visible = True
                lblMsg_DWComparision_IT_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If ddlDWCurrencyCode_IT_L3.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_IT_L3.Visible = True
                lblMsg_DWCurrencyCode_IT_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlDWApprovalParty_IT_L3.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_IT_L3.Visible = True
                lblMsg_DWApprovalParty_IT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlDWComparision_IT_L3.SelectedValue.ToString <> "Select One" Then
            If txtDWAmount_IT_L3.Text = "" Then
                lblMsg_DWAmount_IT_L3.Visible = True
                lblMsg_DWAmount_IT_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If
            If ddlDWCurrencyCode_IT_L3.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_IT_L3.Visible = True
                lblMsg_DWCurrencyCode_IT_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlDWApprovalParty_IT_L3.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_IT_L3.Visible = True
                lblMsg_DWApprovalParty_IT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlDWCurrencyCode_IT_L3.SelectedValue.ToString <> "0" Then
            If ddlDWComparision_IT_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_IT_L3.Visible = True
                lblMsg_DWComparision_IT_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If txtDWAmount_IT_L3.Text = "" Then
                lblMsg_DWAmount_IT_L3.Visible = True
                lblMsg_DWAmount_IT_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If

            If ddlDWApprovalParty_IT_L3.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_IT_L3.Visible = True
                lblMsg_DWApprovalParty_IT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        'Level 4
        If txtDWAmount_IT_L4.Text <> "" Then
            If ddlDWComparision_IT_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_IT_L4.Visible = True
                lblMsg_DWComparision_IT_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If ddlDWCurrencyCode_IT_L4.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_IT_L4.Visible = True
                lblMsg_DWCurrencyCode_IT_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlDWApprovalParty_IT_L4.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_IT_L4.Visible = True
                lblMsg_DWApprovalParty_IT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlDWComparision_IT_L4.SelectedValue.ToString <> "Select One" Then
            If txtDWAmount_IT_L4.Text = "" Then
                lblMsg_DWAmount_IT_L4.Visible = True
                lblMsg_DWAmount_IT_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If
            If ddlDWCurrencyCode_IT_L4.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_IT_L4.Visible = True
                lblMsg_DWCurrencyCode_IT_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlDWApprovalParty_IT_L4.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_IT_L4.Visible = True
                lblMsg_DWApprovalParty_IT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlDWCurrencyCode_IT_L4.SelectedValue.ToString <> "0" Then
            If ddlDWComparision_IT_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_IT_L4.Visible = True
                lblMsg_DWComparision_IT_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If txtDWAmount_IT_L4.Text = "" Then
                lblMsg_DWAmount_IT_L4.Visible = True
                lblMsg_DWAmount_IT_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If

            If ddlDWApprovalParty_IT_L4.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_IT_L4.Visible = True
                lblMsg_DWApprovalParty_IT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        'WDO - Non IT
        'Level 1 
        If txtDWAmount_NonIT_L1.Text <> "" Then
            If ddlDWComparision_NonIT_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_NonIT_L1.Visible = True
                lblMsg_DWComparision_NonIT_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If ddlDWCurrencyCode_NonIT_L1.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_NonIT_L1.Visible = True
                lblMsg_DWCurrencyCode_NonIT_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlDWComparision_NonIT_L1.SelectedValue.ToString <> "Select One" Then
            If txtDWAmount_NonIT_L1.Text = "" Then
                lblMsg_DWAmount_NonIT_L1.Visible = True
                lblMsg_DWAmount_NonIT_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
            If ddlDWCurrencyCode_NonIT_L1.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_NonIT_L1.Visible = True
                lblMsg_DWCurrencyCode_NonIT_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlDWCurrencyCode_NonIT_L1.SelectedValue.ToString <> "0" Then
            If ddlDWComparision_NonIT_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_NonIT_L1.Visible = True
                lblMsg_DWComparision_NonIT_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If txtDWAmount_NonIT_L1.Text = "" Then
                lblMsg_DWAmount_NonIT_L1.Visible = True
                lblMsg_DWAmount_NonIT_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
        End If

        If ddlDWApprovalParty_NonIT_L1.SelectedValue.ToString = "" Then
            lblMsg_DWApprovalParty_NonIT_L1.Visible = True
            lblMsg_DWApprovalParty_NonIT_L1.Text = "Please select L1 Approval Party."
            blnResult = False
        Else
            If ddlDWApprovalParty2_NonIT_L1.SelectedValue.ToString <> "" Then
                If ddlDWApprovalParty_NonIT_L1.SelectedValue.ToString = ddlDWApprovalParty2_NonIT_L1.SelectedValue.ToString Then
                    lblMsg_DWApprovalParty_NonIT_L1.Visible = True
                    lblMsg_DWApprovalParty_NonIT_L1.Text = "Error - L1 Approval Party (1st and 2nd) cannot same."
                    blnResult = False
                End If
            End If
        End If

        If ddlDWApprovalParty_NonIT_L1.SelectedValue.ToString <> "" Then
            If ddlDWApprovalParty_NonIT_L1.SelectedValue.ToString = ddlDWApprovalParty_NonIT_L2.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_NonIT_L2.Visible = True
                lblMsg_DWApprovalParty_NonIT_L2.Text = "Error - Approval Party (L1 [1st] & L2) cannot be same."
                blnResult = False
            End If
            If ddlDWApprovalParty_NonIT_L1.SelectedValue.ToString = ddlDWApprovalParty_NonIT_L3.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_NonIT_L3.Visible = True
                lblMsg_DWApprovalParty_NonIT_L3.Text = "Error - Approval Party (L1 [1st] & L3) cannot be same."
                blnResult = False
            End If
            If ddlDWApprovalParty_NonIT_L1.SelectedValue.ToString = ddlDWApprovalParty_NonIT_L4.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_NonIT_L4.Visible = True
                lblMsg_DWApprovalParty_NonIT_L4.Text = "Error - Approval Party (L1 [1st] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlDWApprovalParty2_NonIT_L1.SelectedValue.ToString <> "" Then
            If ddlDWApprovalParty2_NonIT_L1.SelectedValue.ToString = ddlDWApprovalParty_NonIT_L2.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_NonIT_L2.Visible = True
                lblMsg_DWApprovalParty_NonIT_L2.Text = "Error - Approval Party (L1 [2nd] & L2) cannot be same."
                blnResult = False
            End If
            If ddlDWApprovalParty2_NonIT_L1.SelectedValue.ToString = ddlDWApprovalParty_NonIT_L3.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_NonIT_L3.Visible = True
                lblMsg_DWApprovalParty_NonIT_L3.Text = "Error - Approval Party (L1 [2nd] & L3) cannot be same."
                blnResult = False
            End If
            If ddlDWApprovalParty2_NonIT_L1.SelectedValue.ToString = ddlDWApprovalParty_NonIT_L4.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_NonIT_L4.Visible = True
                lblMsg_DWApprovalParty_NonIT_L4.Text = "Error - Approval Party (L1 [2nd] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlDWApprovalParty_NonIT_L2.SelectedValue.ToString <> "" Then
            If ddlDWApprovalParty_NonIT_L2.SelectedValue.ToString = ddlDWApprovalParty_NonIT_L3.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_NonIT_L3.Visible = True
                lblMsg_DWApprovalParty_NonIT_L3.Text = "Error - Approval Party (L2 & L3) cannot be same."
                blnResult = False
            End If
            If ddlDWApprovalParty_NonIT_L2.SelectedValue.ToString = ddlDWApprovalParty_NonIT_L4.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_NonIT_L4.Visible = True
                lblMsg_DWApprovalParty_NonIT_L4.Text = "Error - Approval Party (L2 & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlDWApprovalParty_NonIT_L3.SelectedValue.ToString <> "" Then
            If ddlDWApprovalParty_NonIT_L3.SelectedValue.ToString = ddlDWApprovalParty_NonIT_L4.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_NonIT_L4.Visible = True
                lblMsg_DWApprovalParty_NonIT_L4.Text = "Error - Approval Party (L3 & L4) cannot be same."
                blnResult = False
            End If
        End If

        'Level 2
        If txtDWAmount_NonIT_L2.Text <> "" Then
            If ddlDWComparision_NonIT_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_NonIT_L2.Visible = True
                lblMsg_DWComparision_NonIT_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If ddlDWCurrencyCode_NonIT_L2.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_NonIT_L2.Visible = True
                lblMsg_DWCurrencyCode_NonIT_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlDWApprovalParty_NonIT_L2.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_NonIT_L2.Visible = True
                lblMsg_DWApprovalParty_NonIT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlDWComparision_NonIT_L2.SelectedValue.ToString <> "Select One" Then
            If txtDWAmount_NonIT_L2.Text = "" Then
                lblMsg_DWAmount_NonIT_L2.Visible = True
                lblMsg_DWAmount_NonIT_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If
            If ddlDWCurrencyCode_NonIT_L2.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_NonIT_L2.Visible = True
                lblMsg_DWCurrencyCode_NonIT_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlDWApprovalParty_NonIT_L2.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_NonIT_L2.Visible = True
                lblMsg_DWApprovalParty_NonIT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlDWCurrencyCode_NonIT_L2.SelectedValue.ToString <> "0" Then
            If ddlDWComparision_NonIT_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_NonIT_L2.Visible = True
                lblMsg_DWComparision_NonIT_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If txtDWAmount_NonIT_L2.Text = "" Then
                lblMsg_DWAmount_NonIT_L2.Visible = True
                lblMsg_DWAmount_NonIT_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If

            If ddlDWApprovalParty_NonIT_L2.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_NonIT_L2.Visible = True
                lblMsg_DWApprovalParty_NonIT_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        'Level 3
        If txtDWAmount_NonIT_L3.Text <> "" Then
            If ddlDWComparision_NonIT_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_NonIT_L3.Visible = True
                lblMsg_DWComparision_NonIT_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If ddlDWCurrencyCode_NonIT_L3.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_NonIT_L3.Visible = True
                lblMsg_DWCurrencyCode_NonIT_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlDWApprovalParty_NonIT_L3.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_NonIT_L3.Visible = True
                lblMsg_DWApprovalParty_NonIT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlDWComparision_NonIT_L3.SelectedValue.ToString <> "Select One" Then
            If txtDWAmount_NonIT_L3.Text = "" Then
                lblMsg_DWAmount_NonIT_L3.Visible = True
                lblMsg_DWAmount_NonIT_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If
            If ddlDWCurrencyCode_NonIT_L3.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_NonIT_L3.Visible = True
                lblMsg_DWCurrencyCode_NonIT_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlDWApprovalParty_NonIT_L3.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_NonIT_L3.Visible = True
                lblMsg_DWApprovalParty_NonIT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlDWCurrencyCode_NonIT_L3.SelectedValue.ToString <> "0" Then
            If ddlDWComparision_NonIT_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_NonIT_L3.Visible = True
                lblMsg_DWComparision_NonIT_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If txtDWAmount_NonIT_L3.Text = "" Then
                lblMsg_DWAmount_NonIT_L3.Visible = True
                lblMsg_DWAmount_NonIT_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If

            If ddlDWApprovalParty_NonIT_L3.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_NonIT_L3.Visible = True
                lblMsg_DWApprovalParty_NonIT_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        'Level 4
        If txtDWAmount_NonIT_L4.Text <> "" Then
            If ddlDWComparision_NonIT_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_NonIT_L4.Visible = True
                lblMsg_DWComparision_NonIT_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If ddlDWCurrencyCode_NonIT_L4.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_NonIT_L4.Visible = True
                lblMsg_DWCurrencyCode_NonIT_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlDWApprovalParty_NonIT_L4.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_NonIT_L4.Visible = True
                lblMsg_DWApprovalParty_NonIT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlDWComparision_NonIT_L4.SelectedValue.ToString <> "Select One" Then
            If txtDWAmount_NonIT_L4.Text = "" Then
                lblMsg_DWAmount_NonIT_L4.Visible = True
                lblMsg_DWAmount_NonIT_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If
            If ddlDWCurrencyCode_NonIT_L4.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_NonIT_L4.Visible = True
                lblMsg_DWCurrencyCode_NonIT_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlDWApprovalParty_NonIT_L4.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_NonIT_L4.Visible = True
                lblMsg_DWApprovalParty_NonIT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlDWCurrencyCode_NonIT_L4.SelectedValue.ToString <> "0" Then
            If ddlDWComparision_NonIT_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_NonIT_L4.Visible = True
                lblMsg_DWComparision_NonIT_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If txtDWAmount_NonIT_L4.Text = "" Then
                lblMsg_DWAmount_NonIT_L4.Visible = True
                lblMsg_DWAmount_NonIT_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If

            If ddlDWApprovalParty_NonIT_L4.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_NonIT_L4.Visible = True
                lblMsg_DWApprovalParty_NonIT_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        '''''''''''''''
        'WDO - Non IT (MAJOR)
        'Level 1 
        If txtDWAmount_NonITMajor_L1.Text <> "" Then
            If ddlDWComparision_NonITMajor_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_NonITMajor_L1.Visible = True
                lblMsg_DWComparision_NonITMajor_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If ddlDWCurrencyCode_NonITMajor_L1.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_NonITMajor_L1.Visible = True
                lblMsg_DWCurrencyCode_NonITMajor_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlDWComparision_NonITMajor_L1.SelectedValue.ToString <> "Select One" Then
            If txtDWAmount_NonITMajor_L1.Text = "" Then
                lblMsg_DWAmount_NonITMajor_L1.Visible = True
                lblMsg_DWAmount_NonITMajor_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
            If ddlDWCurrencyCode_NonITMajor_L1.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_NonITMajor_L1.Visible = True
                lblMsg_DWCurrencyCode_NonITMajor_L1.Text = "Please select L1 Currency Code."
                blnResult = False
            End If
        End If

        If ddlDWCurrencyCode_NonITMajor_L1.SelectedValue.ToString <> "0" Then
            If ddlDWComparision_NonITMajor_L1.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_NonITMajor_L1.Visible = True
                lblMsg_DWComparision_NonITMajor_L1.Text = "Please select L1 Comparision."
                blnResult = False
            End If

            If txtDWAmount_NonITMajor_L1.Text = "" Then
                lblMsg_DWAmount_NonITMajor_L1.Visible = True
                lblMsg_DWAmount_NonITMajor_L1.Text = "Please enter L1 Amount."
                blnResult = False
            End If
        End If

        If ddlDWApprovalParty_NonITMajor_L1.SelectedValue.ToString = "" Then
            lblMsg_DWApprovalParty_NonITMajor_L1.Visible = True
            lblMsg_DWApprovalParty_NonITMajor_L1.Text = "Please select L1 Approval Party."
            blnResult = False
        Else
            If ddlDWApprovalParty2_NonITMajor_L1.SelectedValue.ToString <> "" Then
                If ddlDWApprovalParty_NonITMajor_L1.SelectedValue.ToString = ddlDWApprovalParty2_NonITMajor_L1.SelectedValue.ToString Then
                    lblMsg_DWApprovalParty_NonITMajor_L1.Visible = True
                    lblMsg_DWApprovalParty_NonITMajor_L1.Text = "Error - L1 Approval Party (1st and 2nd) cannot same."
                    blnResult = False
                End If
            End If
        End If

        If ddlDWApprovalParty_NonITMajor_L1.SelectedValue.ToString <> "" Then
            If ddlDWApprovalParty_NonITMajor_L1.SelectedValue.ToString = ddlDWApprovalParty_NonITMajor_L2.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_DWApprovalParty_NonITMajor_L2.Text = "Error - Approval Party (L1 [1st] & L2) cannot be same."
                blnResult = False
            End If
            If ddlDWApprovalParty_NonITMajor_L1.SelectedValue.ToString = ddlDWApprovalParty_NonITMajor_L3.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_DWApprovalParty_NonITMajor_L3.Text = "Error - Approval Party (L1 [1st] & L3) cannot be same."
                blnResult = False
            End If
            If ddlDWApprovalParty_NonITMajor_L1.SelectedValue.ToString = ddlDWApprovalParty_NonITMajor_L4.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_DWApprovalParty_NonITMajor_L4.Text = "Error - Approval Party (L1 [1st] & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlDWApprovalParty2_NonITMajor_L1.SelectedValue.ToString <> "" Then
            If ddlDWApprovalParty2_NonITMajor_L1.SelectedValue.ToString = ddlDWApprovalParty_NonITMajor_L2.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_DWApprovalParty_NonITMajor_L2.Text = "Error - Approval Party (L1 [2nd] & L2) cannot be same."
                blnResult = False
            End If
            If ddlDWApprovalParty2_NonITMajor_L1.SelectedValue.ToString = ddlDWApprovalParty_NonITMajor_L3.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_DWApprovalParty_NonITMajor_L3.Text = "Error - Approval Party (L1 [2nd] & L3) cannot be same."
                blnResult = False
            End If
            If ddlDWApprovalParty2_NonITMajor_L1.SelectedValue.ToString = ddlDWApprovalParty_NonITMajor_L4.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_DWApprovalParty_NonITMajor_L4.Text = "Error - Approval Party (L1 [2nd] & L4) cannot be same."
                blnResult = False
            End If
        End If


        If ddlDWApprovalParty_NonITMajor_L2.SelectedValue.ToString <> "" Then
            If ddlDWApprovalParty_NonITMajor_L2.SelectedValue.ToString = ddlDWApprovalParty_NonITMajor_L3.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_DWApprovalParty_NonITMajor_L3.Text = "Error - Approval Party (L2 & L3) cannot be same."
                blnResult = False
            End If
            If ddlDWApprovalParty_NonITMajor_L2.SelectedValue.ToString = ddlDWApprovalParty_NonITMajor_L4.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_DWApprovalParty_NonITMajor_L4.Text = "Error - Approval Party (L2 & L4) cannot be same."
                blnResult = False
            End If
        End If

        If ddlDWApprovalParty_NonITMajor_L3.SelectedValue.ToString <> "" Then
            If ddlDWApprovalParty_NonITMajor_L3.SelectedValue.ToString = ddlDWApprovalParty_NonITMajor_L4.SelectedValue.ToString Then
                lblMsg_DWApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_DWApprovalParty_NonITMajor_L4.Text = "Error - Approval Party (L3 & L4) cannot be same."
                blnResult = False
            End If
        End If

        'Level 2
        If txtDWAmount_NonITMajor_L2.Text <> "" Then
            If ddlDWComparision_NonITMajor_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_NonITMajor_L2.Visible = True
                lblMsg_DWComparision_NonITMajor_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If ddlDWCurrencyCode_NonITMajor_L2.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_NonITMajor_L2.Visible = True
                lblMsg_DWCurrencyCode_NonITMajor_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlDWApprovalParty_NonITMajor_L2.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_DWApprovalParty_NonITMajor_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlDWComparision_NonITMajor_L2.SelectedValue.ToString <> "Select One" Then
            If txtDWAmount_NonITMajor_L2.Text = "" Then
                lblMsg_DWAmount_NonITMajor_L2.Visible = True
                lblMsg_DWAmount_NonITMajor_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If
            If ddlDWCurrencyCode_NonITMajor_L2.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_NonITMajor_L2.Visible = True
                lblMsg_DWCurrencyCode_NonITMajor_L2.Text = "Please select L2 Currency Code."
                blnResult = False
            End If

            If ddlDWApprovalParty_NonITMajor_L2.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_DWApprovalParty_NonITMajor_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        If ddlDWCurrencyCode_NonITMajor_L2.SelectedValue.ToString <> "0" Then
            If ddlDWComparision_NonITMajor_L2.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_NonITMajor_L2.Visible = True
                lblMsg_DWComparision_NonITMajor_L2.Text = "Please select L2 Comparision."
                blnResult = False
            End If

            If txtDWAmount_NonITMajor_L2.Text = "" Then
                lblMsg_DWAmount_NonITMajor_L2.Visible = True
                lblMsg_DWAmount_NonITMajor_L2.Text = "Please enter L2 Amount."
                blnResult = False
            End If

            If ddlDWApprovalParty_NonITMajor_L2.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_NonITMajor_L2.Visible = True
                lblMsg_DWApprovalParty_NonITMajor_L2.Text = "Please select L2 Approval Party."
                blnResult = False
            End If
        End If

        'Level 3
        If txtDWAmount_NonITMajor_L3.Text <> "" Then
            If ddlDWComparision_NonITMajor_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_NonITMajor_L3.Visible = True
                lblMsg_DWComparision_NonITMajor_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If ddlDWCurrencyCode_NonITMajor_L3.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_NonITMajor_L3.Visible = True
                lblMsg_DWCurrencyCode_NonITMajor_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlDWApprovalParty_NonITMajor_L3.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_DWApprovalParty_NonITMajor_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlDWComparision_NonITMajor_L3.SelectedValue.ToString <> "Select One" Then
            If txtDWAmount_NonITMajor_L3.Text = "" Then
                lblMsg_DWAmount_NonITMajor_L3.Visible = True
                lblMsg_DWAmount_NonITMajor_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If
            If ddlDWCurrencyCode_NonITMajor_L3.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_NonITMajor_L3.Visible = True
                lblMsg_DWCurrencyCode_NonITMajor_L3.Text = "Please select L3 Currency Code."
                blnResult = False
            End If

            If ddlDWApprovalParty_NonITMajor_L3.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_DWApprovalParty_NonITMajor_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        If ddlDWCurrencyCode_NonITMajor_L3.SelectedValue.ToString <> "0" Then
            If ddlDWComparision_NonITMajor_L3.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_NonITMajor_L3.Visible = True
                lblMsg_DWComparision_NonITMajor_L3.Text = "Please select L3 Comparision."
                blnResult = False
            End If

            If txtDWAmount_NonITMajor_L3.Text = "" Then
                lblMsg_DWAmount_NonITMajor_L3.Visible = True
                lblMsg_DWAmount_NonITMajor_L3.Text = "Please enter L3 Amount."
                blnResult = False
            End If

            If ddlDWApprovalParty_NonITMajor_L3.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_NonITMajor_L3.Visible = True
                lblMsg_DWApprovalParty_NonITMajor_L3.Text = "Please select L3 Approval Party."
                blnResult = False
            End If
        End If

        'Level 4
        If txtDWAmount_NonITMajor_L4.Text <> "" Then
            If ddlDWComparision_NonITMajor_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_NonITMajor_L4.Visible = True
                lblMsg_DWComparision_NonITMajor_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If ddlDWCurrencyCode_NonITMajor_L4.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_NonITMajor_L4.Visible = True
                lblMsg_DWCurrencyCode_NonITMajor_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlDWApprovalParty_NonITMajor_L4.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_DWApprovalParty_NonITMajor_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlDWComparision_NonITMajor_L4.SelectedValue.ToString <> "Select One" Then
            If txtDWAmount_NonITMajor_L4.Text = "" Then
                lblMsg_DWAmount_NonITMajor_L4.Visible = True
                lblMsg_DWAmount_NonITMajor_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If
            If ddlDWCurrencyCode_NonITMajor_L4.SelectedValue.ToString = "0" Then
                lblMsg_DWCurrencyCode_NonITMajor_L4.Visible = True
                lblMsg_DWCurrencyCode_NonITMajor_L4.Text = "Please select L4 Currency Code."
                blnResult = False
            End If

            If ddlDWApprovalParty_NonITMajor_L4.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_DWApprovalParty_NonITMajor_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        If ddlDWCurrencyCode_NonITMajor_L4.SelectedValue.ToString <> "0" Then
            If ddlDWComparision_NonITMajor_L4.SelectedValue.ToString = "Select One" Then
                lblMsg_DWComparision_NonITMajor_L4.Visible = True
                lblMsg_DWComparision_NonITMajor_L4.Text = "Please select L4 Comparision."
                blnResult = False
            End If

            If txtDWAmount_NonITMajor_L4.Text = "" Then
                lblMsg_DWAmount_NonITMajor_L4.Visible = True
                lblMsg_DWAmount_NonITMajor_L4.Text = "Please enter L4 Amount."
                blnResult = False
            End If

            If ddlDWApprovalParty_NonITMajor_L4.SelectedValue.ToString = "" Then
                lblMsg_DWApprovalParty_NonITMajor_L4.Visible = True
                lblMsg_DWApprovalParty_NonITMajor_L4.Text = "Please select L4 Approval Party."
                blnResult = False
            End If
        End If

        Return blnResult

    End Function

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim i As Integer = 0
                Dim intID As Integer = 0
                Dim strMsg As String = ""
                Dim strStation As String = ""
                Dim strLocation As String = ""


                lblMsg.Text = ""

                If lblID.Text.Equals("") Then
                    intID = 0
                Else
                    intID = CInt(lblID.Text)
                End If

                strStation = ddlStation.SelectedValue.ToString

                If strStation = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Station')</script>")
                    'lblMsg.Text = "Please select Station."
                    Exit Sub
                End If

                strLocation = txtLocation.Text.ToString

                If strLocation = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please key in Location')</script>")
                    'lblMsg.Text = "Please key in Location."
                    Exit Sub
                End If

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveLocation", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@ID", SqlDbType.Int)
                    If lblID.Text.ToString = "" Then
                        .Parameters("@ID").Value = 0
                    Else
                        .Parameters("@ID").Value = CInt(lblID.Text.ToString)
                    End If
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = strStation
                    .Parameters.Add("@Location", SqlDbType.NVarChar, 50)
                    .Parameters("@Location").Value = clsCF.TrimAll(strLocation)
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    lblID.Text = ""

                    txtLocation.Text = ""
                    If ddlStation.SelectedValue.ToString <> "" Then
                        GetStationLocation(ddlStation.SelectedValue.ToString)
                    Else
                        If Session("DA_StationID").ToString <> "" Then
                            GetStationLocation(Session("DA_StationID").ToString)
                        End If
                    End If
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")

                    'lblMsg.Text = "Save Successful"
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Private Sub GetStationLocation(ByVal strStation As String)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                If strStation = "" Then
                    lblMsg.Text = "Please select Station"
                    Exit Sub
                End If

                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetStationLocation")

                With MyCommand
                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = strStation
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "Location"

                gvLocation.DataSource = MyData
                gvLocation.DataMember = MyData.Tables(0).TableName
                gvLocation.DataBind()


            Catch ex As Exception
                Me.lblMsg.Text = "GetStationLocation: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub gvLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvLocation.SelectedIndexChanged
        Dim intID As Integer = 0
        intID = CInt(gvLocation.SelectedDataKey.Value.ToString)
        lblID.Text = intID

        'Dim hdStationID As New HiddenField
        'hdStationID = gvLocation.SelectedRow.FindControl("hdStationID")
        'ddlStation.SelectedValue = hdStationID.Value.ToString

        Dim lbtnLocation As New LinkButton
        lbtnLocation = gvLocation.SelectedRow.FindControl("lbtnLocation")
        txtLocation.Text = lbtnLocation.Text.ToString
    End Sub

    Protected Sub gvLocation_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvLocation.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim intID As Integer = 0
                Dim lbtnDelete As New LinkButton

                lblMsg.Text = ""

                lbtnDelete = gvLocation.Rows(e.RowIndex).FindControl("lbtnDelete")
                intID = CInt(lbtnDelete.CommandArgument.ToString)

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteStationLocation", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ID", SqlDbType.Int)
                    .Parameters("@ID").Value = intID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    lblID.Text = ""

                    txtLocation.Text = ""
                    If ddlStation.SelectedValue.ToString <> "" Then
                        GetStationLocation(ddlStation.SelectedValue.ToString)
                    Else
                        If Session("DA_StationID").ToString <> "" Then
                            GetStationLocation(Session("DA_StationID").ToString)
                        End If
                    End If

                    'lblMsg.Text = "Delete Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvLocation_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub ddlApprovalParty_IT_L1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlApprovalParty_IT_L1.SelectedIndexChanged
        ddlUBApprovalParty_IT_L1.SelectedValue = ddlApprovalParty_IT_L1.SelectedValue.ToString
        ddlOBApprovalParty_IT_L1.SelectedValue = ddlApprovalParty_IT_L1.SelectedValue.ToString
    End Sub

    Protected Sub ddlApprovalParty_NonIT_L1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlApprovalParty_NonIT_L1.SelectedIndexChanged
        ddlUBApprovalParty_NonIT_L1.SelectedValue = ddlApprovalParty_NonIT_L1.SelectedValue.ToString
        ddlOBApprovalParty_NonIT_L1.SelectedValue = ddlApprovalParty_NonIT_L1.SelectedValue.ToString
    End Sub

    Protected Sub ddlApprovalParty_NonITMajor_L1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlApprovalParty_NonITMajor_L1.SelectedIndexChanged
        ddlUBApprovalParty_NonITMajor_L1.SelectedValue = ddlApprovalParty_NonITMajor_L1.SelectedValue.ToString
        ddlOBApprovalParty_NonITMajor_L1.SelectedValue = ddlApprovalParty_NonITMajor_L1.SelectedValue.ToString
    End Sub

    Protected Sub btnAttach_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAttach.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim i As Integer = 0
                Dim strFileName As String = ""
                Dim strFilePath As String = ""
                Dim strMsg As String = ""


                If fuPO.FileName <> "" Then
                    clsCF.CheckConnectionState(sqlConn)

                    Dim cmd As New SqlCommand("SP_SaveStationPO_RDLC", sqlConn)
                    With cmd
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                        .Parameters("@UserID").Value = Session("DA_UserID").ToString
                        .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                        .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                        .Parameters.Add("@FileName", SqlDbType.NVarChar, 500)
                        .Parameters("@FileName").Value = clsCF.RenameFileName(fuPO.FileName.ToString)
                        .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                        .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                        .Parameters.Add("@FilePath", SqlDbType.NVarChar, 500)
                        .Parameters("@FilePath").Direction = ParameterDirection.Output
                        .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                        .Parameters("@Msg").Direction = ParameterDirection.Output
                        .CommandTimeout = 0
                        .ExecuteNonQuery()

                        strMsg = .Parameters("@Msg").Value.ToString()
                        strFilePath = .Parameters("@FilePath").Value.ToString()
                    End With

                    If strMsg <> "" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                    Else
                        If strFilePath <> "" Then

                            'strAlbumPath = hfServerPath.Value
                            Dim fn As String = ""
                            fn = System.IO.Path.GetFileName(fuPO.PostedFile.FileName)

                            If fn <> "" Then
                                If FileIO.FileSystem.DirectoryExists(strFilePath) = True Then
                                    FileIO.FileSystem.DeleteDirectory(strFilePath, FileIO.DeleteDirectoryOption.DeleteAllContents)
                                End If

                                FileIO.FileSystem.CreateDirectory(strFilePath)

                                If FileIO.FileSystem.FileExists(strFilePath + "/" + fn) = False Then
                                    fuPO.PostedFile.SaveAs(strFilePath + "/" + fn)
                                End If
                            End If
                        End If

                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Add Successful')</script>")
                        GetStationPORDLC()
                    End If

                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnAttach_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using

    End Sub

    Private Sub GetStationPORDLC()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetStationPORDLC")

                With MyCommand

                    .Parameters.Add("@StationID", SqlDbType.VarChar, 6)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AssetQuoReceiptFile"

                gvPORDLC.Visible = True
                gvPORDLC.DataSource = MyData
                gvPORDLC.DataMember = MyData.Tables(0).TableName
                gvPORDLC.DataBind()

                If gvPORDLC.Rows.Count > 0 Then
                    fuPO.Enabled = False
                    btnAttach.Enabled = False
                Else
                    fuPO.Enabled = True
                    btnAttach.Enabled = True
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "GetAssetQuoReceiptFile: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub


    Protected Sub gvPORDLC_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvPORDLC.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim strStationID As String
                Dim lbtnRemove As New LinkButton

                lblMsg.Text = ""

                lbtnRemove = gvPORDLC.Rows(e.RowIndex).FindControl("lbtnRemove")
                strStationID = lbtnRemove.CommandArgument.ToString

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteStationPORDLC", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 6)
                    .Parameters("@StationID").Value = strStationID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    GetStationPORDLC()
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvPORDLC_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub ddlApprovalParty2_IT_L1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlApprovalParty2_IT_L1.SelectedIndexChanged
        ddlUBApprovalParty2_IT_L1.SelectedValue = ddlApprovalParty2_IT_L1.SelectedValue.ToString
        ddlOBApprovalParty2_IT_L1.SelectedValue = ddlApprovalParty2_IT_L1.SelectedValue.ToString
    End Sub

    Protected Sub ddlApprovalParty2_NonIT_L1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlApprovalParty2_NonIT_L1.SelectedIndexChanged
        ddlUBApprovalParty2_NonIT_L1.SelectedValue = ddlApprovalParty2_NonIT_L1.SelectedValue.ToString
        ddlOBApprovalParty2_NonIT_L1.SelectedValue = ddlApprovalParty2_NonIT_L1.SelectedValue.ToString
    End Sub

    Protected Sub ddlApprovalParty2_NonITMajor_L1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlApprovalParty2_NonITMajor_L1.SelectedIndexChanged
        ddlUBApprovalParty2_NonITMajor_L1.SelectedValue = ddlApprovalParty2_NonITMajor_L1.SelectedValue.ToString
        ddlOBApprovalParty2_NonITMajor_L1.SelectedValue = ddlApprovalParty2_NonITMajor_L1.SelectedValue.ToString
    End Sub
End Class
