<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmUserSetting.aspx.vb" Inherits="System_frmUserSetting" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>User Setting</title>
    <script type="text/javascript">
         function showDate() 
        {
            var month_names = new Array("Jan", "Feb", "Mar", 
            "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
            "Oct", "Nov", "Dec");

            var d = new Date();
            var curr_day = d.getDay();
            var curr_date = d.getDate();
            var hours = d.getHours();
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            
            var curr_hours;
            var curr_minutes;
            var curr_seconds;

            if(hours<10)
            {
                curr_hours = "0" + hours;
            }
            else
            {
                curr_hours = hours;
            }
            
            if(minutes < 10)
            {
                curr_minutes = "0" + minutes;
            }
             else
            {
                curr_minutes = minutes;
            }
            
           if(seconds < 10)
            {
                curr_seconds = "0" + seconds;
            }
             else
            {
                curr_seconds = seconds;
            }

            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
 
            document.getElementById("hdfCurrentDateTime").value = curr_date + " " +  month_names[curr_month] +  " " + curr_year + " " + curr_hours + ":" + curr_minutes + ":" + curr_seconds
         }
     </script>
</head>
<body>
    <form id="frmUserSetting" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="User Setting"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:HiddenField ID="hdfCurrentDateTime" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="User ID"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:TextBox ID="txtUserID" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="120px"></asp:TextBox>&nbsp;<asp:Button ID="btnView" runat="server" Font-Names="Verdana"
                            Font-Size="Small" Text="View" Width="65px" /></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Name"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:Label ID="lblName" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Email Address"></asp:Label></td>
                <td>
                    :</td>
                <td colspan="5">
                    <asp:TextBox ID="txtEmail" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="454px"></asp:TextBox></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label6" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Station"></asp:Label></td>
                <td>
                    :</td>
                <td colspan="5">
                    <asp:Label ID="lblStation" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label><asp:HiddenField
                        ID="hdStationID" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label42" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Role"></asp:Label></td>
                <td>
                    :</td>
                <td colspan="5">
                    <asp:DropDownList ID="ddlRole" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="249px">
                    </asp:DropDownList><asp:Button ID="btnAdd" runat="server" Text="Add" /></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td colspan="5">
                    <asp:GridView ID="grvRole" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px"
                        CellPadding="3" DataKeyNames="vchRoleID" Font-Names="Verdana" Font-Size="X-Small"
                        GridLines="Vertical">
                        <PagerSettings Position="TopAndBottom" />
                        <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                        <Columns>
                            <asp:TemplateField HeaderText="Remove">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnRemove" runat="server" CommandName="Select">Remove</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="nvchRoleName" HeaderText="Role" />
                        </Columns>
                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="Gainsboro" />
                    </asp:GridView>
                    <asp:HiddenField ID="hdfRole" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label7" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Access Station"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:Button ID="btnAccessStation" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Text="...." /></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td colspan="5">
                    <asp:GridView ID="gvAccessStation" runat="server" AutoGenerateColumns="False" BackColor="White"
                        BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="StationID"
                        EmptyDataText="Record Not Found" Font-Names="Verdana" Font-Size="X-Small">
                        <RowStyle BackColor="White" ForeColor="#003399" />
                        <Columns>
                            <asp:TemplateField HeaderText="Remove">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnRemove" runat="server" CommandArgument='<%# Bind("StationID") %>'
                                        CommandName="Select">Remove</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Station">
                                <ItemTemplate>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Bind("StationCode") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                        <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                    </asp:GridView>
                    <asp:HiddenField ID="hdfStation" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="As Approval Party"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:CheckBox ID="chkAsApprovalParty" runat="server" /></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td colspan="5">
                    <table style="width: 450px">
                        <tr>
                            <td style="width: 150px">
                                <asp:Button ID="btnSave" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Save"
                                    Width="65px" OnClientClick="showDate();" /></td>
                            <td style="width: 150px">
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="64px" /></td>
                            <td style="width: 150px">
                                <asp:Button ID="btnRemoveSetting" runat="server" Text="Remove Setting" ToolTip="Remove : As Approval Party, Leave Approval Party, Leave Notify, On Behalf"
                                    Width="114px" Visible="False" /></td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
