Imports System.Data
Imports System.Data.SqlClient

Partial Class System_frmAddRoleAccessRight
    Inherits System.Web.UI.Page
    Dim dsRoleAccessRight As New DataSet
    Dim dsRole As New DataSet
    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMsg.Text = ""
        Dim strRoleID As String = ""

        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If IsPostBack() = False Then

            If Session("vchRoleID") <> Nothing Then
                strRoleID = Session("vchRoleID")
                lblID.Text = strRoleID
                If strRoleID = "999" Then
                    btnARSave.Enabled = False
                Else
                    btnARSave.Enabled = True
                End If
            Else
                Response.Redirect("~/ListHome.aspx")
                Exit Sub
            End If

            'Session("LastContentForm") = "System/frmAddRoleAccessRight.aspx"

            If strRoleID <> "" Then
                Session("vchRoleID") = strRoleID
            End If

            If Session("vchRoleID") = Nothing Then
                Response.Redirect("~/System/frmRole.aspx")
                Exit Sub
            Else

                Session("blnAccess") = False
                Session("blnReadOnly") = False
                Session("blnWrite") = False
                Session("blnModifyOthers") = False
                Session("blnDeleteOthers") = False

                btnSave.Enabled = False
                btnARSave.Enabled = False

                Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "S0006")

                If Len(strAccessRight) >= 7 Then
                    Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
                Else
                    Session("AccessStation") = ""
                End If


                If IsNumeric(Left(strAccessRight, 6)) = True Then
                    If Mid(strAccessRight, 1, 1) = 1 Then
                        Session("blnAccess") = True
                    Else
                        Session("blnAccess") = False
                        Session("AccessRightMsg") = "You are no right to access the Role Access Right Page."
                        Response.Redirect("~/System/frmRole.aspx")
                        Exit Sub
                    End If

                    If Mid(strAccessRight, 2, 1) = 1 Then
                        Session("blnReadOnly") = True
                    Else
                        Session("blnReadOnly") = False

                        If Mid(strAccessRight, 3, 1) = 1 Then
                            Session("blnWrite") = True
                            btnSave.Enabled = True
                            btnARSave.Enabled = True
                        Else
                            Session("blnWrite") = False
                            btnSave.Enabled = False
                            btnARSave.Enabled = False
                        End If
                    End If

                    If Mid(strAccessRight, 4, 1) = 1 Then
                        Session("blnModifyOthers") = True
                        'btnSave.Visible = True
                        'btnARSave.Visible = True
                    Else
                        Session("blnModifyOthers") = False
                        'btnSave.Visible = False
                        'btnARSave.Visible = False
                    End If

                    If Mid(strAccessRight, 5, 1) = 1 Then
                        Session("blnDeleteOthers") = True
                    Else
                        Session("blnDeleteOthers") = False
                    End If
                End If

                GetRoleAccessRightData()
            End If
        End If
    End Sub

    Private Sub GetRoleAccessRightData()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("Proc_GetRoleAccessRight")

                With MyCommand
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@RoleID", SqlDbType.NVarChar, 50)
                    .Parameters("@RoleID").Value = Session("vchRoleID").ToString
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "RoleAccessRight"

                dsRoleAccessRight.Tables.Add(MyData.Tables(0).Copy)
                dsRoleAccessRight.Tables(0).TableName = "RoleAccessRight"

                dsRole.Tables.Add(MyData.Tables(1).Copy)
                dsRole.Tables(0).TableName = "Role"

                If dsRoleAccessRight.Tables(0).Rows.Count > 0 Then
                    grvRoleAccessRight.DataSource = dsRoleAccessRight
                    grvRoleAccessRight.DataMember = dsRoleAccessRight.Tables(0).TableName
                    grvRoleAccessRight.DataBind()
                End If

                If dsRole.Tables(0).Rows.Count > 0 Then
                    txtRole.Text = dsRole.Tables(0).Rows(0)("nvchRoleName").ToString
                    txtRoleDescription.Text = dsRole.Tables(0).Rows(0)("nvchRoleDescription").ToString
                    If dsRole.Tables(0).Rows(0)("iActive").ToString = "1" Then
                        chkActive.Checked = True
                    Else
                        chkActive.Checked = False
                    End If
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "GetRoleAccessRightData: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnARSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnARSave.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim intTotalRow As Integer
                Dim intRow As Integer
                Dim strScreenID As String
                Dim intAccess As Integer
                Dim intReadOnly As Integer
                Dim intWrite As Integer
                Dim intModify As Integer
                Dim intDelete As Integer
                Dim hdfScreenID As HiddenField
                Dim chkAccess As CheckBox
                Dim chkReadOnly As CheckBox
                Dim chkWrite As CheckBox
                Dim chkModify As CheckBox
                Dim chkDelete As CheckBox

                intTotalRow = grvRoleAccessRight.Rows.Count - 1

                For intRow = 0 To intTotalRow
                    hdfScreenID = grvRoleAccessRight.Rows(intRow).FindControl("hdfScreenID")
                    chkAccess = grvRoleAccessRight.Rows(intRow).FindControl("chkbAccess")
                    chkReadOnly = grvRoleAccessRight.Rows(intRow).FindControl("chkbReadOnly")
                    chkWrite = grvRoleAccessRight.Rows(intRow).FindControl("chkbWrite")
                    chkModify = grvRoleAccessRight.Rows(intRow).FindControl("chkbModify")
                    chkDelete = grvRoleAccessRight.Rows(intRow).FindControl("chkbDelete")

                    strScreenID = hdfScreenID.Value.ToString
                    If chkAccess.Checked = True Then
                        intAccess = 1
                    Else
                        intAccess = 0
                    End If
                    If chkReadOnly.Checked = True Then
                        intReadOnly = 1
                    Else
                        intReadOnly = 0
                    End If
                    If chkWrite.Checked = True Then
                        intWrite = 1
                    Else
                        intWrite = 0
                    End If
                    If chkModify.Checked = True Then
                        intModify = 1
                    Else
                        intModify = 0
                    End If
                    If chkDelete.Checked = True Then
                        intDelete = 1
                    Else
                        intDelete = 0
                    End If



                    clsCF.CheckConnectionState(sqlConn)

                    Dim cmd As New SqlCommand("Proc_SaveRoleAccessRight", sqlConn)
                    With cmd
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@UserID", SqlDbType.NVarChar, 50)
                        .Parameters("@UserID").Value = Session("DA_UserID")
                        .Parameters.Add("@RoleID", SqlDbType.NVarChar, 50)
                        .Parameters("@RoleID").Value = Session("vchRoleID")
                        .Parameters.Add("@ScreenID", SqlDbType.VarChar, 10)
                        .Parameters("@ScreenID").Value = strScreenID
                        .Parameters.Add("@Access", SqlDbType.Int)
                        .Parameters("@Access").Value = intAccess
                        .Parameters.Add("@ReadOnly", SqlDbType.Int)
                        .Parameters("@ReadOnly").Value = intReadOnly
                        .Parameters.Add("@Write", SqlDbType.Int)
                        .Parameters("@Write").Value = intWrite
                        .Parameters.Add("@Modify", SqlDbType.Int)
                        .Parameters("@Modify").Value = intModify
                        .Parameters.Add("@Delete", SqlDbType.Int)
                        .Parameters("@Delete").Value = intDelete
                        .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                        .Parameters("@Msg").Direction = ParameterDirection.Output
                        .CommandTimeout = 0
                        .ExecuteNonQuery()

                        strMsg = .Parameters("@Msg").Value.ToString()
                    End With

                    If strMsg <> "" Then
                        lblMsg.Text = strMsg
                    Else

                    End If
                Next
            Catch ex As Exception
                Me.lblMsg.Text = "btnARSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try

        End Using
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim strNewRoleID As String = "999"
                Dim strRoleID As String
                Dim intActive As Integer

                If Session("vchRoleID") <> Nothing Then
                    If Session("vchRoleID").ToString <> "999" Then
                        strRoleID = Session("vchRoleID").ToString
                    Else
                        strRoleID = "999"
                    End If
                Else
                    strRoleID = "999"
                End If

                If chkActive.Checked = True Then
                    intActive = 1
                Else
                    intActive = 0
                End If


                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("Proc_SaveRole", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@UserID", SqlDbType.NVarChar, 50)
                    .Parameters("@UserID").Value = Session("DA_UserID")
                    .Parameters.Add("@RoleID", SqlDbType.VarChar, 10)
                    .Parameters("@RoleID").Value = strRoleID
                    .Parameters.Add("@Role", SqlDbType.NVarChar, 50)
                    .Parameters("@Role").Value = txtRole.Text.ToString
                    .Parameters.Add("@RoleDesc", SqlDbType.NVarChar, 50)
                    .Parameters("@RoleDesc").Value = txtRoleDescription.Text.ToString
                    .Parameters.Add("@Active", SqlDbType.Int)
                    .Parameters("@Active").Value = intActive
                    .Parameters.Add("@NewRoleID", SqlDbType.VarChar, 10)
                    .Parameters("@NewRoleID").Direction = ParameterDirection.Output
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                    If strMsg = "" Then
                        strNewRoleID = .Parameters("@NewRoleID").Value.ToString()
                    End If
                End With

                If strMsg <> "" Then
                    lblMsg.Text = strMsg
                Else
                    If strRoleID = "999" Then
                        btnARSave.Enabled = True
                    End If
                    Session("vchRoleID") = strNewRoleID.ToString
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub chkbAccess_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub chkbReadOnly_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
End Class
