﻿<%@ WebHandler Language="C#" Class="SearchUser" %>

using System;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Data;

public class SearchUser : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        string prefixText = context.Request.QueryString["q"];

        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = ConfigurationManager
                    .ConnectionStrings["dbConnectionString"].ConnectionString;
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT A.Fullname + ' (' + A.UserID + ') - ' + B.StationCode  AS 'Fullname' " +
                     //"FROM tbEmployee A " +
                     "FROM RESM..SMUSER A " +
                     "LEFT JOIN RESM..SMStation B ON B.StationID = A.StationID " +
                     "WHERE A.Status = 'Active' " +
                     "AND A.Fullname + ' (' + A.UserID + ') - ' + B.StationCode like + '%' + @SearchText + '%' " +
                     "ORDER BY A.USERID ";

                cmd.Parameters.AddWithValue("@SearchText", prefixText);
                cmd.Connection = conn;
                StringBuilder sb = new StringBuilder();
                conn.Open();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        sb.Append(sdr["Fullname"])
                            .Append(Environment.NewLine);
                    }
                }
                conn.Close();
                context.Response.Write(sb.ToString());
            }
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}