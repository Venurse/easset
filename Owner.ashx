<%@ WebHandler Language="VB" Class="Owner" %>

Imports System
Imports System.Web
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Text
Imports System.Data
Imports System.Configuration.ConfigurationSettings

Public Class Owner : Implements IHttpHandler
    
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        
        Dim prefixText As String = context.Request.QueryString("q")
        Using MyConnection As New SqlConnection(strConnectionString)
            
            Using cmd As New SqlCommand()
                cmd.CommandText = "SELECT A.Fullname + ' (' + A.UserID + ') - ' + B.StationCode  AS 'Fullname' " & _
                     "FROM RESM..SMUSER A " & _
                     "LEFT JOIN RESM..SMStation B ON B.StationID = A.StationID " & _
                     "WHERE A.Status = 'Active' " & _
                     "AND A.Fullname + ' (' + A.UserID + ') - ' + B.StationCode like + '%' + @SearchText + '%' " & _
                     "ORDER BY A.USERID "
                cmd.Parameters.AddWithValue("@SearchText", prefixText)
                cmd.Connection = MyConnection
                Dim sb As New StringBuilder()
                MyConnection.Open()
                
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read
                        sb.Append(sdr("Fullname")).Append(Environment.NewLine)
                    End While
                End Using
                MyConnection.Close()
                context.Response.Write(sb.ToString)
            End Using
        End Using
        
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class