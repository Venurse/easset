Imports System.Configuration.ConfigurationSettings
Imports System.Data
Imports System.Data.SqlClient

Partial Class ListHome
    Inherits System.Web.UI.Page

    Dim strExtLink As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("CurrentDateTime") <> Nothing Then
            If Session("CurrentDateTime").ToString <> "" Then
                hdfCurrentDateTime.Value = Session("CurrentDateTime").ToString
            End If
        Else
            If hdfCurrentDateTime.Value.ToString <> "" Then
                Session("CurrentDateTime") = hdfCurrentDateTime.Value.ToString
            Else
                Session("CurrentDateTime") = Format(DateTime.Now, "dd MMM yyyy hh:mm:ss").ToString
            End If
        End If

        If Session("DA_LOG_IN_OUT") = Nothing Then
            Dim strUserID As String = ""
            Dim strUserName As String = Request.ServerVariables("LOGON_USER")
            Dim hasDomain As Integer = strUserName.IndexOf("\")
            If hasDomain > 0 Then
                strUserID = strUserName.Remove(0, hasDomain + 1).ToUpper()
            Else
                strUserID = strUserName.ToUpper()
            End If

            Dim dsUserDetail As New DataSet
            dsUserDetail = DIMERCO.SDK.Utilities.LSDK.getUserProfilebyUserList(strUserID)
            If dsUserDetail.Tables(0).Rows.Count > 0 Then

                Session("DA_UserID") = dsUserDetail.Tables(0).Rows(0)("UserID").ToString
                Session("DA_UserName") = dsUserDetail.Tables(0).Rows(0)("FullName").ToString
                Session("DA_StationID") = dsUserDetail.Tables(0).Rows(0)("StationID").ToString
                Session("DA_StationCode") = dsUserDetail.Tables(0).Rows(0)("StationCode").ToString
                Session("DA_DepartmentCode") = dsUserDetail.Tables(0).Rows(0)("DepartmentID").ToString
                Session("DA_LOG_IN_OUT") = "IN"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "RefreshParent", "<script language='javascript'>RefreshParent()</script>")
                Exit Sub
            End If

        End If

        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
        End If

        If Session("AccessRightMsg") <> "" Then
            lblMsg.Visible = True
            lblMsg.Text = Session("AccessRightMsg")
            Session("AccessRightMsg") = ""
        End If

        If Session("LastContentForm") <> Nothing Then
            Dim strLastContentForm As String = Session("LastContentForm").ToString
            Session("LastContentForm") = ""
            If strLastContentForm <> "" Then
                Response.Redirect("~/" + strLastContentForm)
            End If
        End If

        If hdfCurrentDateTime.Value.ToString <> "" Then
            Session("CurrentDateTime") = hdfCurrentDateTime.Value.ToString
        End If

    End Sub
End Class
