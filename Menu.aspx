<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Menu.aspx.vb" Inherits="Menu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TreeView ID="TreeView1" runat="server" NodeIndent="10" NodeWrap="True" Width="164px">
            <ParentNodeStyle Font-Bold="False" />
            <HoverNodeStyle BackColor="#C0FFC0" Font-Underline="True" ForeColor="#5555DD" />
            <SelectedNodeStyle BackColor="#C0C0FF" Font-Underline="True" ForeColor="#5555DD"
                HorizontalPadding="0px" VerticalPadding="0px" />
            <Nodes>
                <asp:TreeNode Text="Purchase" Value="Purchase" Expanded="True">
                    <asp:TreeNode NavigateUrl="~/Purchase/frmApplyNew.aspx?ANT=N&amp;ID=0" Target="ListHome" Text="Add New Purchase"
                        Value="Add New Purchase"></asp:TreeNode>
                    <asp:TreeNode NavigateUrl="~/Purchase/frmPurchaseList.aspx" Target="ListHome" Text="Purchase List"
                        Value="Purchase List"></asp:TreeNode>
                    <asp:TreeNode NavigateUrl="~/Purchase/frmPayment.aspx" Target="ListHome" Text="Add Payment"
                        Value="Add Payment"></asp:TreeNode>
                    <asp:TreeNode NavigateUrl="~/Purchase/frmPaymentList.aspx" Target="ListHome" Text="Payment List"
                        Value="Payment List"></asp:TreeNode>
                    <asp:TreeNode NavigateUrl="~/Purchase/frmPurchaseApproval.aspx" Target="ListHome"
                        Text="Purchase Approval" Value="Purchase Approval"></asp:TreeNode>
                </asp:TreeNode>
                <asp:TreeNode Text="Asset" Value="Asset" Expanded="True">
                    <asp:TreeNode NavigateUrl="~/Asset/frmAssetList.aspx" Target="ListHome" Text="Asset List"
                        Value="New Node"></asp:TreeNode>
                    <asp:TreeNode NavigateUrl="~/Asset/frmAssetDetail.aspx" Target="ListHome" Text="Add New Asset"
                        Value="Add New Asset"></asp:TreeNode>
                </asp:TreeNode>
                <asp:TreeNode Target="ListHome" Text="Disposal &amp; Writeoff" Value="Disposal &amp; Writeoff" Expanded="False">
                    <asp:TreeNode NavigateUrl="~/DisposalWriteOff/frmDWList.aspx" Target="ListHome" Text="DW List"
                        Value="New Node"></asp:TreeNode>
                    <asp:TreeNode NavigateUrl="~/DisposalWriteOff/frmApplyDW.aspx" Target="ListHome"
                        Text="Apply DW" Value="Apply DW"></asp:TreeNode>
                    <asp:TreeNode NavigateUrl="~/DisposalWriteOff/frmDWApproval.aspx" Target="ListHome"
                        Text="DW Approval" Value="DW Approval"></asp:TreeNode>
                </asp:TreeNode>
                <asp:TreeNode Text="System" Value="System" Expanded="False">
                    <asp:TreeNode NavigateUrl="~/System/frmUserList.aspx" Target="ListHome" Text="User List"
                        Value="User List"></asp:TreeNode>
                    <asp:TreeNode NavigateUrl="~/System/frmStationSetting.aspx" Target="ListHome" Text="Station Setting"
                        Value="Approval Flow"></asp:TreeNode>
                    <asp:TreeNode NavigateUrl="~/System/frmBudgetList.aspx" Target="ListHome" Text="Budget List"
                        Value="Budget List"></asp:TreeNode>
                    <asp:TreeNode NavigateUrl="~/System/frmSoftware.aspx" Target="ListHome" Text="Software"
                        Value="Software"></asp:TreeNode>
                    <asp:TreeNode NavigateUrl="~/System/frmSoftwareCategory.aspx" Target="ListHome" Text="Software Category"
                        Value="Software Category"></asp:TreeNode>
                    <asp:TreeNode NavigateUrl="~/System/frmVendor.aspx" Target="ListHome" Text="Vendor"
                        Value="Vendor"></asp:TreeNode>
                    <asp:TreeNode NavigateUrl="~/System/frmRole.aspx" Target="ListHome"
                        Text="Role And Access Right" Value="Role And Access Right"></asp:TreeNode>
                    <asp:TreeNode Text="Asset Category" Value="Asset Category">
                        <asp:TreeNode NavigateUrl="~/System/frmGroup.aspx" Target="ListHome" Text="Group"
                            Value="Group"></asp:TreeNode>
                        <asp:TreeNode NavigateUrl="~/System/frmCategory.aspx" Target="ListHome" Text="Category"
                            Value="Category"></asp:TreeNode>
                        <asp:TreeNode NavigateUrl="~/System/frmSubCategory.aspx" Target="ListHome" Text="Sub Category"
                            Value="Sub Category"></asp:TreeNode>
                    </asp:TreeNode>
                </asp:TreeNode>
                <asp:TreeNode Text="Report" Value="Report" Expanded="True">
                    <asp:TreeNode Target="ListHome" Text="User Asset" Value="User Asset"></asp:TreeNode>
                </asp:TreeNode>
                <asp:TreeNode NavigateUrl="~/frmUserManual.aspx" Target="ListHome" Text="User Manual"
                    Value="User Manual"></asp:TreeNode>
                <asp:TreeNode NavigateUrl="http://10.161.254.9/eSM/Forms/ChangePassword.aspx" Target="Blank"
                    Text="Change Password" Value="Change Password" PopulateOnDemand="True"></asp:TreeNode>
                <asp:TreeNode NavigateUrl="~/frmLogin.aspx" Target="ListHome" Text="Logout" Value="Logout">
                </asp:TreeNode>
            </Nodes>
            <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" HorizontalPadding="0px"
                NodeSpacing="0px" VerticalPadding="0px" />
        </asp:TreeView>
    
    </div>
    </form>
</body>
</html>
