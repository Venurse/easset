Imports System.Configuration.ConfigurationSettings
Imports System.Data
Imports System.Data.SqlClient

Partial Class Menu2
    Inherits System.Web.UI.Page
    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If IsPostBack() = False Then
        If Session("DA_UserID") <> Nothing Then
            TreeView1.Dispose()
            TreeView1.Nodes.Clear()
            GetTreeViewSetting()
        End If
        'End If
    End Sub

    Private Sub GetTreeViewSetting()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsChild As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetTreeViewParent")

                With MyCommand
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@ParentID", SqlDbType.VarChar, 6)
                    .Parameters("@ParentID").Value = ""
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)

                Dim i As Integer
                For i = 0 To MyData.Tables(0).Rows.Count - 1
                    Dim root As TreeNode

                    root = New TreeNode(MyData.Tables(0).Rows(i)("TreeNodeName").ToString, MyData.Tables(0).Rows(i)("vchScreenID").ToString, "", MyData.Tables(0).Rows(i)("NavigateUrl").ToString, MyData.Tables(0).Rows(i)("Target").ToString)
                    If MyData.Tables(0).Rows(i)("Expanded").ToString = "True" Then
                        root.Expand()
                    End If
                    CreateNode(root)
                    TreeView1.Nodes.Add(root)
                Next

            Catch ex As Exception
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + ex.Message.ToString + "')</script>")
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub CreateNode(ByVal node As TreeNode)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim i As Integer
                Dim dsChild As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetTreeViewSetting")

                With MyCommand
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@ParentID", SqlDbType.VarChar, 6)
                    .Parameters("@ParentID").Value = node.Value
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)

                If MyData.Tables(0).Rows.Count = 0 Then
                    Return
                Else
                    For i = 0 To MyData.Tables(0).Rows.Count - 1
                        Dim tnode As TreeNode

                        tnode = New TreeNode(MyData.Tables(0).Rows(i)("TreeNodeName").ToString, MyData.Tables(0).Rows(i)("vchScreenID").ToString, "", MyData.Tables(0).Rows(i)("NavigateUrl").ToString, MyData.Tables(0).Rows(i)("Target").ToString)
                        node.ChildNodes.Add(tnode)
                        CreateNode(tnode)
                    Next
                End If

            Catch ex As Exception
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + ex.Message.ToString + "')</script>")
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

End Class
