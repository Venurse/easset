Imports System.Data
Imports System.Data.SqlClient

Partial Class frmItem
    Inherits System.Web.UI.Page
    Dim clsCF As New clsComFunction
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack() = False Then
            BindDropDownList()
        End If
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByID")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[Group]"
                    .Parameters.Add("@ID", SqlDbType.BigInt)
                    .Parameters("@ID").Value = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "Group"


                ddlCategoryGroup.DataSource = MyData.Tables(0)
                ddlCategoryGroup.DataValueField = MyData.Tables(0).Columns("GroupID").ToString
                ddlCategoryGroup.DataTextField = MyData.Tables(0).Columns("Group_Name").ToString
                ddlCategoryGroup.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ddlCategoryGroup.SelectedValue = 0
        txtDescription.Text = ""
        'Close the window
        Response.Write("<script language=""Javascript"">window.opener =self;window.close();</script>")
        Response.End()
    End Sub


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If ddlCategoryGroup.SelectedValue.ToString = "0" Then
            lblMsg.Text = "Please select Group."
            Exit Sub
        End If

        GetItem()
    End Sub

    Private Sub GetItem()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetItem")

                With MyCommand
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@GroupID", SqlDbType.Int)
                    .Parameters("@GroupID").Value = CInt(ddlCategoryGroup.SelectedValue.ToString)
                    .Parameters.Add("@Category", SqlDbType.NVarChar, 100)
                    .Parameters("@Category").Value = txtCategory.Text.ToString()
                    .Parameters.Add("@Description", SqlDbType.NVarChar, 500)
                    .Parameters("@Description").Value = txtDescription.Text.ToString()
                    .CommandTimeout = 0
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "Item"

                gvItem.Visible = True
                gvItem.DataSource = MyData
                gvItem.DataMember = MyData.Tables(0).TableName
                gvItem.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "GetItem: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
        Dim intCategoryID As Integer
        Dim lblCategory As Label
        Dim hdfParentCategoryID As HiddenField
        Dim hdfParentCategory As HiddenField
        Dim hdfDepreciationPeriod As HiddenField

        intCategoryID = CInt(gvItem.SelectedValue.ToString)
        lblCategory = gvItem.SelectedRow.FindControl("lblCategory")
        hdfParentCategoryID = gvItem.SelectedRow.FindControl("hdfParentCategoryID")
        hdfParentCategory = gvItem.SelectedRow.FindControl("hdfParentCategory")
        hdfDepreciationPeriod = gvItem.SelectedRow.FindControl("hdfDepreciationPeriod")

        Session("DA_CategoryDepreciation_Period") = hdfDepreciationPeriod.Value.ToString
        Session("DA_ParentCategory") = hdfParentCategory.Value.ToString
        Session("DA_ParentCategoryID") = hdfParentCategoryID.Value.ToString
        Session("DA_CategoryID") = intCategoryID
        Session("DA_Category") = lblCategory.Text.ToString

        Dim ParentFormID As String = ""
        ParentFormID = Request.QueryString("ParentForm").ToString
        Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener =self;window.close();</script>")

    End Sub
End Class
