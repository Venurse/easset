<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAddUser2.aspx.vb" Inherits="frmAddUser2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Add User</title>
</head>
<body>
    <form id="frmAddUser2" method="post" runat="server">
    <div>
        <table style="width: 453px">
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Smaller"
                        Text="Add User"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 130px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="Small" ForeColor="Red"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 67px">
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 67px">
                    <asp:Label ID="lblStation" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Text="Station"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:DropDownList ID="ddlStation" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="323px">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td style="width: 130px">
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="User Name:"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:TextBox ID="txtUserName" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="316px"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 130px">
                </td>
                <td>
                </td>
                <td align="left">
                    <asp:Button ID="btnView" runat="server" Text="View" /></td>
            </tr>
            <tr>
                <td style="width: 67px">
                </td>
                <td>
                </td>
                <td>
                    <asp:GridView ID="gvUser" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        DataKeyNames="UserID" Font-Names="Verdana" Font-Size="X-Small" ForeColor="#333333"
                        Width="80%">
                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                        <Columns>
                            <asp:TemplateField HeaderText="Select">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnSelect" runat="server" CommandArgument='<%# Bind("UserID") %>'
                                        CommandName="Select" Text="Select"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
