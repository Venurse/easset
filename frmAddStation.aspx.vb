Imports System.Data
Imports System.Data.SqlClient

Partial Class frmAddStation
    Inherits System.Web.UI.Page
    Dim dsApplyTo As New DataSet
    Dim dsStation As New DataSet
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strApplyToList As String
        lblMsg.Text = ""

        If IsPostBack() = False Then

            Session("DA_New_ACCESSSTATION") = ""

            If Session("DA_ACCESSSTATION") = "" Then
                strApplyToList = ""
                Session("DA_New_ACCESSSTATION") = ""
            Else
                strApplyToList = Session("DA_ACCESSSTATION").ToString
                Session("DA_New_ACCESSSTATION") = Session("DA_ACCESSSTATION")
            End If

            GetApplyToStation(strApplyToList)

        End If
    End Sub

    Private Sub GetApplyToStation(ByVal strApplyToList As String)

        dsStation = DIMERCO.SDK.Utilities.LSDK.getStationDataByWithExcludeList(strApplyToList)
        dsStation.Tables(0).DefaultView.Sort = "StationCode"

        'Bind Employee 
        gvName.Visible = True
        gvName.DataSource = dsStation.Tables(0)
        gvName.DataMember = dsStation.Tables(0).TableName
        gvName.DataBind()


        If gvName.Rows.Count > 0 Then
            chkSelectAllCC.Visible = True
            chkSelectAllCC.Checked = False
        Else
            chkSelectAllCC.Visible = True
            chkSelectAllCC.Checked = False
        End If

        dsApplyTo = DIMERCO.SDK.Utilities.LSDK.getStationDataByWithList(strApplyToList)
        grvList.Dispose()
        If dsApplyTo.Tables(0).Rows.Count > 0 Then
            grvList.Visible = True
            grvList.DataSource = dsApplyTo
            grvList.DataMember = dsApplyTo.Tables(0).TableName
            grvList.DataBind()
        Else
            grvList.Visible = False
        End If

        'Try
        '    Dim MyData As New DataSet
        '    Dim MyAdapter As SqlDataAdapter
        '    Dim MyCommand As SqlCommand
        '    Dim MyConnection As New SqlConnection(strConnectionString)

        '    MyCommand = New SqlCommand("SP_GetAddApplyToStation")

        '    With MyCommand
        '        .CommandType = CommandType.StoredProcedure
        '        .Parameters.Add("@ReSM", SqlDbType.NVarChar, 50)
        '        .Parameters("@ReSM").Value = strReSM
        '        .Parameters.Add("@List", SqlDbType.NVarChar)
        '        .Parameters("@List").Value = strApplyToList
        '        .Connection = MyConnection
        '    End With

        '    MyAdapter = New SqlDataAdapter(MyCommand)
        '    MyAdapter.Fill(MyData)
        '    MyData.Tables(0).TableName = "Station"
        '    MyData.Tables(1).TableName = "ApplyTo"

        '    dsStation.Tables.Add(MyData.Tables(0).Copy)
        '    dsStation.Tables(0).TableName = "Station"

        '    dsApplyTo.Tables.Add(MyData.Tables(1).Copy)
        '    dsApplyTo.Tables(0).TableName = "ApplyTo"


        '    'Bind Employee 
        '    gvName.Visible = True
        '    gvName.DataSource = dsStation.Tables(0)
        '    gvName.DataMember = dsStation.Tables(0).TableName
        '    gvName.DataBind()


        '    If gvName.Rows.Count > 0 Then
        '        chkSelectAllCC.Visible = True
        '        chkSelectAllCC.Checked = False
        '    Else
        '        chkSelectAllCC.Visible = True
        '        chkSelectAllCC.Checked = False
        '    End If


        '    grvList.Dispose()
        '    If dsApplyTo.Tables(0).Rows.Count > 0 Then
        '        grvList.Visible = True
        '        grvList.DataSource = dsApplyTo
        '        grvList.DataMember = dsApplyTo.Tables(0).TableName
        '        grvList.DataBind()
        '    Else
        '        grvList.Visible = False
        '    End If

        'Catch ex As Exception
        '    Me.lblMsg.Text = "ShowApplyList: " & ex.Message.ToString
        'Finally
        '    'sqlConn.Close()
        'End Try
    End Sub


    Protected Sub grvInviteList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        Dim strApplyToList As String
        Dim strNewApplyToList As String = ""
        Dim strRemove As String = ""

        strRemove = grvList.SelectedValue.ToString.ToUpper()

        strApplyToList = Session("DA_New_ACCESSSTATION").ToString.ToUpper()

        strNewApplyToList = strApplyToList.Replace(strRemove.ToUpper(), "").ToString
        strNewApplyToList = strNewApplyToList.Replace(",,", ",").ToString
        If strNewApplyToList = "," Then
            strNewApplyToList = ""
        End If

        Session("DA_New_ACCESSSTATION") = strNewApplyToList

        GetApplyToStation(strNewApplyToList)

    End Sub


    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddToList.Click
        'Dim i As Integer
        Dim strSelectedName As String = ""
        Dim strApplyToList As String = ""

        If Session("DA_New_ACCESSSTATION") <> Nothing Then
            strApplyToList = Session("DA_New_ACCESSSTATION").ToString
        End If

        If gvName.Rows.Count > 0 Then
            For Each row As GridViewRow In gvName.Rows

                Dim chkCC As CheckBox = row.FindControl("chkCC")
                Dim hfID As HiddenField = row.FindControl("hfID")
                strSelectedName = hfID.Value.ToString()

                If chkCC.Checked = True Then
                    If strApplyToList.Contains(strSelectedName) = False Then
                        strApplyToList += "," + strSelectedName
                    End If
                End If
            Next
        End If

        If strApplyToList.ToString <> "" Then
            strApplyToList += ","
        End If


        Session("DA_New_ACCESSSTATION") = ""
        Session("DA_New_ACCESSSTATION") = strApplyToList


        If Session("DA_New_ACCESSSTATION") <> Nothing Then
            GetApplyToStation(Session("DA_New_ACCESSSTATION").ToString)
        Else
            GetApplyToStation("")
        End If

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Close the window
        Response.Write("<script language=""Javascript"">window.opener =self;window.close();</script>")
        Response.End()
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        Dim ParentFormID As String = ""
        ParentFormID = Request.QueryString("ParentForm").ToString

        Session("DA_ACCESSSTATION") = Session("DA_New_ACCESSSTATION")
        'Session("DA_New_ACCESSSTATION") = Nothing

        Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener =self;window.close();</script>")
    End Sub

    Protected Sub chkSelectAllCC_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAllCC.CheckedChanged
        If gvName.Rows.Count > 0 Then
            For Each row As GridViewRow In gvName.Rows
                Dim chkCC As CheckBox = row.FindControl("chkCC")
                If chkSelectAllCC.Checked = True Then
                    If chkCC IsNot Nothing Then
                        chkCC.Checked = True
                    End If
                Else
                    chkCC.Checked = False
                End If
            Next
        End If
    End Sub
End Class
