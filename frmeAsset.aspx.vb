
Partial Class frmeAsset
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'lblDate.Text = Now.ToLongDateString.ToString()
        If Session("DA_UserName") <> Nothing Then
            lblWelcome.Text = "Welcome, " & Session("DA_UserName").ToString
        End If

        If hdfCurrentDateTime.Value.ToString <> "" Then
            Session("CurrentDateTime") = hdfCurrentDateTime.Value.ToString
        Else
            Session("CurrentDateTime") = Format(DateTime.Now, "dd MMM yyyy hh:mm:ss").ToString
        End If


    End Sub
End Class
