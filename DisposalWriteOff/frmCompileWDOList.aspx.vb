Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class DisposalWriteOff_frmCompileWDOList
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")
    Dim dsSubCategory As New DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If IsPostBack() = False Then
            BindDropDownList()

            hdfWDOID.Value = ""
            If Session("WDOID") <> Nothing Then
                hdfWDOID.Value = Session("WDOID").ToString
                Session("WDOID") = ""
            End If

            If Session("WDOTypeName") <> Nothing Then
                If Session("WDOTypeName").ToString <> "Select One" Then
                    lblTitle.Text = "Compile " + Session("WDOTypeName").ToString + " List"
                    lblList.Text = Session("WDOTypeName").ToString + " List :- "
                End If
            End If
            BindAssetList()
        End If
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByID")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[WDOReason]" '[Group_Category], [SubCategory_ParentCategoryID], 
                    .Parameters.Add("@ID", SqlDbType.BigInt)
                    .Parameters("@ID").Value = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "WDOReason"

                'ddlCategory.DataSource = MyData.Tables(0)
                'ddlCategory.DataValueField = MyData.Tables(0).Columns("CategoryID").ToString
                'ddlCategory.DataTextField = MyData.Tables(0).Columns("Category").ToString
                'ddlCategory.DataBind()

                'dsSubCategory.Tables.Add(MyData.Tables(1).Copy)
                'dsSubCategory.Tables(0).TableName = "SubCategory"
                'ViewState("dsSubCategory") = dsSubCategory

                ddlReason.DataSource = MyData.Tables(0)
                ddlReason.DataValueField = MyData.Tables(0).Columns("ReasonID").ToString
                ddlReason.DataTextField = MyData.Tables(0).Columns("Reason").ToString
                ddlReason.DataBind()



            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    'Protected Sub ddlSubCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubCategory.SelectedIndexChanged
    '    BindAssetList()
    'End Sub

    Private Sub BindAssetList()
        Dim MyConnection As New SqlConnection(strConnectionString)
        Try
            'Dim intCategoryID As Integer = 0
            'If ddlSubCategory.Items.Count > 0 Then
            '    intCategoryID = CInt(ddlSubCategory.SelectedValue.ToString)
            'Else
            '    intCategoryID = 0
            'End If

            Dim MyData As New DataSet
            Dim MyAdapter As SqlDataAdapter
            Dim MyCommand As SqlCommand

            clsCF.CheckConnectionState(MyConnection)

            MyCommand = New SqlCommand("SP_GetDW_AssetList")

            With MyCommand

                .Parameters.Add("@WDOID", SqlDbType.VarChar, 20)
                .Parameters("@WDOID").Value = hdfWDOID.Value.ToString
                .Parameters.Add("@Category", SqlDbType.NVarChar, 100)
                .Parameters("@Category").Value = txtCategory.Text.ToString
                .Parameters.Add("@AssetNo", SqlDbType.NVarChar, 100)
                .Parameters("@AssetNo").Value = txtAssetNo.Text.ToString
                .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                .Parameters("@StationID").Value = Session("WDOStationID").ToString
                .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                .CommandType = CommandType.StoredProcedure
                .Connection = MyConnection
            End With

            MyAdapter = New SqlDataAdapter(MyCommand)
            MyAdapter.Fill(MyData)


            gvItem.Visible = True
            gvItem.DataSource = MyData
            gvItem.DataMember = MyData.Tables(0).TableName
            gvItem.DataBind()

            If gvItem.Rows.Count > 0 Then
                btnAdd.Enabled = True
            Else
                btnAdd.Enabled = False
            End If


            gvDWList.Visible = True
            gvDWList.DataSource = MyData
            gvDWList.DataMember = MyData.Tables(1).TableName
            gvDWList.DataBind()

            If MyData.Tables(1).Rows.Count > 0 Then
                hdfWDOID.Value = MyData.Tables(1).Rows(0)("WDOID").ToString
                Session("WDOID") = hdfWDOID.Value.ToString
            End If

        Catch ex As Exception
            Me.lblMsg.Text = "BindAssetList: " & ex.Message.ToString
        Finally
            clsCF.CloseConnection(MyConnection)
        End Try
    End Sub

    'Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
    '    'Dim dsSubCategory As DataSet
    '    dsSubCategory = ViewState("dsSubCategory")

    '    Dim objDataSet2 As New DataSet
    '    objDataSet2.Merge(dsSubCategory.Tables(0).Select("ParentCategoryID=" & CInt(ddlCategory.SelectedValue.ToString) & " OR ParentCategoryID=0"))

    '    If objDataSet2.Tables.Count = 0 Then
    '        objDataSet2 = dsSubCategory.Clone()
    '    End If

    '    ddlSubCategory.DataSource = objDataSet2.Tables(0)
    '    ddlSubCategory.DataValueField = objDataSet2.Tables(0).Columns("CategoryID").ToString
    '    ddlSubCategory.DataTextField = objDataSet2.Tables(0).Columns("Category").ToString
    '    ddlSubCategory.DataBind()

    'End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim hdfAssetID As HiddenField
        Dim blnSelected As Boolean = False

        If ddlReason.SelectedValue.ToString = "0" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Reason')</script>")
            Exit Sub
        End If

        hdfSelectedAssetID.Value = ""

        For i = 0 To gvItem.Rows.Count - 1
            chkSelect = gvItem.Rows(i).FindControl("chkSelect")
            hdfAssetID = gvItem.Rows(i).FindControl("hdfAssetID")
            If chkSelect.Checked = True Then
                If hdfSelectedAssetID.Value.ToString = "" Then
                    hdfSelectedAssetID.Value = hdfAssetID.Value.ToString
                Else
                    hdfSelectedAssetID.Value = hdfSelectedAssetID.Value.ToString + "<" + hdfAssetID.Value.ToString
                End If
                blnSelected = True
            End If
        Next
        If blnSelected = False Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Asset')</script>")
        Else
            SaveDWOAssetList()
        End If
    End Sub

    Private Sub SaveDWOAssetList()
        Using sqlConn As New SqlConnection(strConnectionString)
            Try

                Dim strDWOID As String = ""
                Dim strMsg As String = ""

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveWDO_AssetList", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@WDOID", SqlDbType.VarChar, 20)
                    If hdfWDOID.Value.ToString = "" Then
                        .Parameters("@WDOID").Value = ""
                    Else
                        .Parameters("@WDOID").Value = hdfWDOID.Value.ToString
                    End If

                    .Parameters.Add("@To", SqlDbType.VarChar, 6)
                    If Session("WDOTo") <> Nothing Then
                        .Parameters("@To").Value = Session("WDOTo").ToString
                    Else
                        .Parameters("@To").Value = ""
                    End If

                    .Parameters.Add("@CC", SqlDbType.NVarChar)
                    If Session("WDOCC") <> Nothing Then
                        .Parameters("@CC").Value = Session("WDOCC").ToString
                    Else
                        .Parameters("@CC").Value = ""
                    End If

                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    If Session("WDOStationID") <> Nothing Then
                        .Parameters("@StationID").Value = Session("WDOStationID").ToString
                    Else
                        .Parameters("@StationID").Value = ""
                    End If

                    .Parameters.Add("@Remarks", SqlDbType.NVarChar)
                    If Session("WDORemarks") <> Nothing Then
                        .Parameters("@Remarks").Value = Session("WDORemarks").ToString
                    Else
                        .Parameters("@Remarks").Value = ""
                    End If

                    .Parameters.Add("@WDOType", SqlDbType.VarChar, 50)
                    If Session("WDOType") <> Nothing Then
                        .Parameters("@WDOType").Value = Session("WDOType").ToString
                    Else
                        .Parameters("@WDOType").Value = ""
                    End If

                    .Parameters.Add("@SelectedWDOAssetID", SqlDbType.NVarChar)
                    .Parameters("@SelectedWDOAssetID").Value = hdfSelectedAssetID.Value.ToString

                    .Parameters.Add("@ReasonID", SqlDbType.BigInt)
                    .Parameters("@ReasonID").Value = CInt(ddlReason.SelectedValue.ToString)

                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString

                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@Status", SqlDbType.VarChar, 1)
                    .Parameters("@Status").Value = "D"
                    .Parameters.Add("@WDO_ID", SqlDbType.VarChar, 20)
                    .Parameters("@WDO_ID").Direction = ParameterDirection.Output

                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                    strDWOID = .Parameters("@WDO_ID").Value.ToString()
                End With

                If strMsg <> "" Then
                    lblMsg.Text = strMsg
                Else
                    lblMsg.Text = ""
                    hdfWDOID.Value = strDWOID

                    If hdfWDOID.Value.ToString <> "" Then
                        Session("WDOID") = hdfWDOID.Value.ToString
                    End If

                    BindAssetList()
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "SaveDWOAssetList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ParentFormID As String = ""
        ParentFormID = Request.QueryString("ParentForm").ToString
        Session("WDOID") = hdfWDOID.Value.ToString
        Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener =self;window.close();</script>")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        hdfSelectedAssetID.Value = ""
        Dim i As Integer
        Dim hdfAssetID As HiddenField
        Dim chkDelete As CheckBox
        Dim blnSelected As Boolean = False
        Dim strSelectedAssetList As String = ""

        For i = 0 To gvDWList.Rows.Count - 1
            hdfAssetID = gvDWList.Rows(i).FindControl("hdfAssetID")
            chkDelete = gvDWList.Rows(i).FindControl("chkDelete")
            If chkDelete.Enabled = True Then
                If strSelectedAssetList = "" Then
                    strSelectedAssetList = hdfAssetID.Value.ToString
                Else
                    strSelectedAssetList = strSelectedAssetList + "<" + hdfAssetID.Value.ToString
                End If
            End If
        Next
        DeleteDWOSelectedAssetList(strSelectedAssetList)
    End Sub

    Protected Sub gvDWList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDWList.RowCommand
        If e.CommandName = "DeleteSelected" Then
            Dim i As Integer
            Dim chkDelete As CheckBox
            Dim hdfAssetID As HiddenField
            Dim blnSelected As Boolean = False
            Dim strSelectedAssetList As String = ""

            For i = 0 To gvDWList.Rows.Count - 1
                chkDelete = gvDWList.Rows(i).FindControl("chkDelete")
                hdfAssetID = gvDWList.Rows(i).FindControl("hdfAssetID")
                If chkDelete.Checked = True Then
                    If strSelectedAssetList = "" Then
                        strSelectedAssetList = hdfAssetID.Value.ToString
                    Else
                        strSelectedAssetList = strSelectedAssetList + "<" + hdfAssetID.Value.ToString
                    End If
                End If
            Next
            DeleteDWOSelectedAssetList(strSelectedAssetList)
        End If

    End Sub

    Private Sub DeleteDWOSelectedAssetList(ByVal strSelectedAssetList As String)
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strDWOID As String = ""
                Dim strMsg As String = ""

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteWDO_AssetList", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@WDOID", SqlDbType.VarChar, 20)
                    If hdfWDOID.Value.ToString = "" Then
                        .Parameters("@WDOID").Value = ""
                    Else
                        .Parameters("@WDOID").Value = hdfWDOID.Value.ToString
                    End If

                    '.Parameters.Add("@To", SqlDbType.VarChar, 6)
                    'If Session("WDOTo") <> Nothing Then
                    '    .Parameters("@To").Value = Session("WDOTo").ToString
                    'Else
                    '    .Parameters("@To").Value = ""
                    'End If

                    .Parameters.Add("@CC", SqlDbType.NVarChar)
                    If Session("WDOCC") <> Nothing Then
                        .Parameters("@CC").Value = Session("WDOCC").ToString
                    Else
                        .Parameters("@CC").Value = ""
                    End If

                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    If Session("WDOStationID") <> Nothing Then
                        .Parameters("@StationID").Value = Session("WDOStationID").ToString
                    Else
                        .Parameters("@StationID").Value = ""
                    End If

                    .Parameters.Add("@Remarks", SqlDbType.NVarChar)
                    If Session("WDORemarks") <> Nothing Then
                        .Parameters("@Remarks").Value = Session("WDORemarks").ToString
                    Else
                        .Parameters("@Remarks").Value = ""
                    End If

                    .Parameters.Add("@WDOType", SqlDbType.VarChar, 50)
                    If Session("WDOType") <> Nothing Then
                        .Parameters("@WDOType").Value = Session("WDOType").ToString
                    Else
                        .Parameters("@WDOType").Value = ""
                    End If

                    .Parameters.Add("@SelectedWDOAssetID", SqlDbType.NVarChar)
                    .Parameters("@SelectedWDOAssetID").Value = strSelectedAssetList

                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString

                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@Status", SqlDbType.VarChar, 1)
                    .Parameters("@Status").Value = "D"

                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    lblMsg.Text = strMsg
                Else
                    lblMsg.Text = ""

                    BindAssetList()
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "DeleteDWOSelectedAssetList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub chkDeleteAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim i As Int32
        Dim chkDelete As CheckBox
        Dim chkDeleteAll As CheckBox
        'Dim blnChecked As Boolean
        chkDeleteAll = CType(gvDWList.HeaderRow.FindControl("chkDeleteAll"), CheckBox)
        'blnChecked = chkDeleteAll.Checked
        For i = 0 To gvDWList.Rows.Count - 1
            chkDelete = CType(gvDWList.Rows(i).FindControl("chkDelete"), CheckBox)
            If chkDelete.Enabled = True Then
                chkDelete.Checked = chkDeleteAll.Checked.ToString
            End If
        Next
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindAssetList()
    End Sub
End Class
