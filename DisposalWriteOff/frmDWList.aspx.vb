Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class DisposalWriteOff_frmDWList
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If


        If IsPostBack() = False Then

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            btnAddWD.Enabled = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "D0001")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Write Off/Disposal/Obsolete List Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnAddWD.Enabled = True
                    Else
                        Session("blnWrite") = False
                       btnAddWD.Enabled = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            BindDropDownList()
            GetDWList()
        Else

        End If
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand
                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[AccessStation],[Year],[DWStatus]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)

                'If MyData.Tables(0).Rows(0)("AccessStation").ToString <> "" Then
                '    dsStation = DIMERCO.SDK.Utilities.LSDK.getStationDataByWithList(MyData.Tables(0).Rows(0)("AccessStation").ToString)
                'Else
                '    dsStation = DIMERCO.SDK.Utilities.ReSM.GetStationList()
                'End If
                'Dim dtView As DataView = dsStation.Tables(0).DefaultView
                'dtView.Sort = "StationCode ASC"

                'ddlStation.DataSource = dtView
                'ddlStation.DataValueField = dtView.Table.Columns("StationID").ToString
                'ddlStation.DataTextField = dtView.Table.Columns("StationCode").ToString
                'ddlStation.DataBind()

                If Session("DA_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("DA_StationID").ToString
                End If


                ddlYear.DataSource = MyData.Tables(1)
                ddlYear.DataValueField = MyData.Tables(1).Columns("Year").ToString
                ddlYear.DataTextField = MyData.Tables(1).Columns("Year").ToString
                ddlYear.DataBind()
                ddlYear.SelectedValue = Now.Year

                ddlStatus.DataSource = MyData.Tables(2)
                ddlStatus.DataValueField = MyData.Tables(2).Columns("StatusID").ToString
                ddlStatus.DataTextField = MyData.Tables(2).Columns("Status").ToString
                ddlStatus.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnAddWD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddWD.Click
        Session("WDOID") = ""
        Response.Redirect("~/DisposalWriteOff/frmApplyDW.aspx")
    End Sub

    
    Protected Sub btnQuery_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQuery.Click
        Session("DA_StationID") = ddlStation.SelectedValue.ToString
        GetDWList()
    End Sub

    Private Sub GetDWList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetWDOList")

                With MyCommand

                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@Status", SqlDbType.VarChar, 1)
                    .Parameters("@Status").Value = ddlStatus.SelectedValue.ToString
                    .Parameters.Add("@Year", SqlDbType.Int)
                    If ddlYear.SelectedValue.ToString = "" Then
                        .Parameters("@Year").Value = DBNull.Value
                    Else
                        .Parameters("@Year").Value = CInt(ddlYear.SelectedValue.ToString)
                    End If

                    .Parameters.Add("@WDOType", SqlDbType.NVarChar, 10)
                    If ddlType.SelectedValue.ToString = "All" Then
                        .Parameters("@WDOType").Value = ""
                    Else
                        .Parameters("@WDOType").Value = ddlType.SelectedValue.ToString
                    End If

                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "DWList"

                gvDWList.DataSource = MyData
                gvDWList.DataMember = MyData.Tables(0).TableName
                gvDWList.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "GetDWList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub gvDWList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDWList.SelectedIndexChanged
        Dim lbtnDW As New LinkButton
        Dim lblStatus As New Label
        lbtnDW = gvDWList.SelectedRow.FindControl("lbtnDW")
        lblStatus = gvDWList.SelectedRow.FindControl("lblStatus")

        Session("WDOID") = ""
        Session("WDOID") = lbtnDW.CommandArgument.ToString

        Dim hdfStationID As New HiddenField
        hdfStationID = gvDWList.SelectedRow.FindControl("hdfStationID")

        Session("WDO_StationID") = ""
        Session("WDO_StationID") = hdfStationID.Value.ToString

        Dim hdfPage As New HiddenField
        hdfPage = gvDWList.SelectedRow.FindControl("hdfPage")
        Response.Redirect("~/" & hdfPage.Value.ToString & "")


        'If lblStatus.Text = "Approved" Then
        '    Response.Redirect("~/DisposalWriteOff/frmDWDetail.aspx")
        'ElseIf lblStatus.Text = "Rejected" Then
        '    Response.Redirect("~/DisposalWriteOff/frmDWDetail.aspx")
        'ElseIf lblStatus.Text = "Void" Then
        '    Response.Redirect("~/DisposalWriteOff/frmDWDetail.aspx")
        'Else
        '    Response.Redirect("~/DisposalWriteOff/frmApplyDW.aspx")
        'End If

    End Sub
End Class
