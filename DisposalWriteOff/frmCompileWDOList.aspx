<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmCompileWDOList.aspx.vb" Inherits="DisposalWriteOff_frmCompileWDOList" ValidateRequest="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Compile List</title>
    <script type="text/javascript">
         function showDate() 
        {
            var month_names = new Array("Jan", "Feb", "Mar", 
            "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
            "Oct", "Nov", "Dec");

            var d = new Date();
            var curr_day = d.getDay();
            var curr_date = d.getDate();
            var hours = d.getHours();
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            
            var curr_hours;
            var curr_minutes;
            var curr_seconds;

            if(hours<10)
            {
                curr_hours = "0" + hours;
            }
            else
            {
                curr_hours = hours;
            }
            
            if(minutes < 10)
            {
                curr_minutes = "0" + minutes;
            }
             else
            {
                curr_minutes = minutes;
            }
            
           if(seconds < 10)
            {
                curr_seconds = "0" + seconds;
            }
             else
            {
                curr_seconds = seconds;
            }

            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
 
            document.getElementById("hdfCurrentDateTime").value = curr_date + " " +  month_names[curr_month] +  " " + curr_year + " " + curr_hours + ":" + curr_minutes + ":" + curr_seconds
         }
     </script>
</head>
<body onunload="window.opener.document.frmApplyDW.submit();">
    <form id="frmCompileWDOList" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Compile List"></asp:Label></td>
                <td style="font-size: 12pt; font-family: Times New Roman">
                </td>
            </tr>
            <tr style="font-size: 12pt; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:HiddenField ID="hdfCurrentDateTime" runat="server" />
                    <asp:HiddenField ID="hdfWDOID" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                </td>
            </tr>
            <tr style="font-size: 12pt; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8" style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                    <asp:Label ID="Label2" runat="server" BackColor="GradientActiveCaption" Font-Bold="True"
                        Font-Names="Verdana" Font-Size="X-Small" ForeColor="Blue" Width="100%">Asset List :-</asp:Label></td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table width="100%">
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td style="width: 150px">
                                <asp:Label ID="Label21" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Category"></asp:Label></td>
                            <td style="width: 5px">
                                :</td>
                            <td>
                                <asp:TextBox ID="txtCategory" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="203px"></asp:TextBox>
                                </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label10" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Asset No"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:TextBox ID="txtAssetNo" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="203px"></asp:TextBox>
                                <asp:Button ID="btnSearch" runat="server" Text="Search" /></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Reason"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlReason" runat="server" AutoPostBack="True" Font-Names="Verdana"
                                    Font-Size="X-Small">
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                </td>
                <td style="font-size: 12pt; font-family: Times New Roman">
                </td>
            </tr>
            <tr style="font-size: 12pt; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8"><table width="100%">
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td colspan="3">
                                <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" BackColor="White"
                                    BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" DataKeyNames="AssetID"
                                    Font-Names="Verdana" Font-Size="X-Small" EmptyDataText="Record Not Found">
                                    <RowStyle BackColor="White" ForeColor="#330099" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Select">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                                <asp:HiddenField ID="hdfAssetID" runat="server" Value='<%# Bind("AssetID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="AssetNo" HeaderText="Asset No" />
                                        <asp:BoundField DataField="Category" HeaderText="Category" />
                                        <asp:BoundField DataField="BrandModel" HeaderText="Brand / Model" />
                                        <asp:BoundField DataField="OwnerName" HeaderText="Owner" />
                                        <asp:BoundField DataField="Location" HeaderText="Location" />
                                        <asp:BoundField DataField="Vendor_Warranty" HeaderText="Vendor Warranty" />
                                        <asp:BoundField DataField="PurchaseDate" HeaderText="Purchase Date" />
                                        <asp:BoundField DataField="PurchaseCost" HeaderText="Purchase Cost" />
                                        <asp:BoundField DataField="NetBookValue" HeaderText="NetBook Value" />
                                        <asp:BoundField DataField="Accumulated_Depreciation" HeaderText="Accumulated Depreciation" />
                                    </Columns>
                                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                                </asp:GridView>
                            </td>
                        </tr>
                    <tr style="font-size: 12pt; font-family: Times New Roman">
                        <td colspan="3">
                            &nbsp;</td>
                    </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td colspan="3">
                                <asp:Button ID="btnAdd" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Add To List (Save)"
                                    Width="141px" OnClientClick="showDate();" />
                                <asp:HiddenField ID="hdfSelectedAssetID" runat="server" />
                            </td>
                        </tr>
                    <tr style="font-size: 12pt; font-family: Times New Roman">
                        <td colspan="3">
                            <asp:Label ID="lblList" runat="server" BackColor="GradientActiveCaption" Font-Bold="True"
                                Font-Names="Verdana" Font-Size="X-Small" ForeColor="Blue" Width="100%"></asp:Label></td>
                    </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td colspan="3">
                                <asp:GridView ID="gvDWList" runat="server" AutoGenerateColumns="False" BackColor="White"
                                    BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" DataKeyNames="AssetID"
                                    Font-Names="Verdana" Font-Size="X-Small" EmptyDataText="Record Not Found">
                                    <RowStyle BackColor="White" ForeColor="#330099" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Remove">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnRemove" runat="server" CommandArgument='<%# Bind("AssetID") %>'
                                                    CommandName="Delete" Visible="False">Remove</asp:LinkButton>
                                                <asp:CheckBox ID="chkDelete" runat="server" Enabled='<%# Bind("AllowEditDelete") %>' />
                                                <asp:HiddenField ID="hdfAssetID" runat="server" Value='<%# Bind("AssetID") %>' />
                                            </ItemTemplate>
                                            <HeaderTemplate><asp:CheckBox ID="chkDeleteAll" runat="server" AutoPostBack="True" OnCheckedChanged="chkDeleteAll_CheckedChanged" /><asp:Button ID="btnDelete_gv" runat="server" CommandName="DeleteSelected" Font-Names="Verdana"
                                                    Font-Size="X-Small" Text="Delete" OnClientClick="showDate(); return confirm('Are you sure you want to remove this Asset?');" Height="25px" />
                                            </HeaderTemplate>
                                            <HeaderStyle Wrap="False" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="AssetNo" HeaderText="Asset No" />
                                        <asp:BoundField DataField="Category" HeaderText="Category" />
                                        <asp:BoundField DataField="BrandModel" HeaderText="Brand / Model" />
                                        <asp:BoundField DataField="OwnerName" HeaderText="Owner" />
                                        <asp:BoundField DataField="Location" HeaderText="Location" />
                                        <asp:BoundField DataField="Vendor_Warranty" HeaderText="Vendor Warranty" />
                                        <asp:BoundField DataField="PurchaseDate" HeaderText="Purchase Date" />
                                        <asp:BoundField DataField="PurchaseCost" HeaderText="Purchase Cost" />
                                        <asp:BoundField DataField="NetBookValue" HeaderText="NetBook Value" />
                                        <asp:BoundField DataField="Accumulated_Depreciation" HeaderText="Accumulated Depreciation" />
                                        <asp:BoundField DataField="Reason" HeaderText="Reason" />
                                        <asp:BoundField DataField="ApprovalStatus" HeaderText="Approval Status" />
                                    </Columns>
                                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td colspan="3">
                                <table style="width: 550px">
                                    <tr>
                                        <td style="width: 150px">
                                            <asp:Button ID="btnSave" runat="server" Font-Names="Verdana" Font-Size="Small" OnClientClick="showDate();"
                                                Text="Return To Page" Width="133px" /></td>
                                        <td style="width: 150px">
                                            <asp:Button ID="btnClear" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Delete All"
                                                Width="85px" OnClientClick="showDate();" Visible="False" /></td>
                                        <td style="width: 150px">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
