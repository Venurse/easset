Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class DisposalWriteOff_frmDWDetail
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If IsPostBack() = False Then
            hdfWDOID.Value = ""

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "D0003")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Write Off/Disposal/Obsolete Detail Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                    Else
                        Session("blnWrite") = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            If Request.QueryString("WDOID") <> Nothing Then
                Session("WDOID") = Request.QueryString("WDOID").ToString
            End If

            If Session("WDOID") <> Nothing Then
                If Session("WDOID").ToString <> "" Then
                    hdfWDOID.Value = Session("WDOID").ToString
                    GetDWDetail()
                    Session("WDOID") = ""
                End If
            End If
        End If
    End Sub

    Private Sub GetDWDetail()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetDWData")

                With MyCommand
                    .Parameters.Add("@WDOID", SqlDbType.VarChar, 20)
                    .Parameters("@WDOID").Value = hdfWDOID.Value.ToString
                    '.Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    '.Parameters("@StationID").Value = Session("WDO_StationID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "DWDetail"
                MyData.Tables(1).TableName = "DWList"

                lblStation.Text = MyData.Tables(0).Rows(0)("StationCode")
                lblYear.Text = MyData.Tables(0).Rows(0)("Year")
                lblStatus.Text = MyData.Tables(0).Rows(0)("Status")
                lblType.Text = MyData.Tables(0).Rows(0)("Type")
                lblDate.Text = MyData.Tables(0).Rows(0)("CreatedOn")
                lblApplyBy.Text = MyData.Tables(0).Rows(0)("CreatedBy")

                lblTitle.Text = lblType.Text.ToString + " Detail"

                gvDWList.DataSource = MyData
                gvDWList.DataMember = MyData.Tables(1).TableName
                gvDWList.DataBind()

                gvApprovalHistory.DataSource = MyData
                gvApprovalHistory.DataMember = MyData.Tables(2).TableName
                gvApprovalHistory.DataBind()



            Catch ex As Exception
                Me.lblMsg.Text = "GetDWDetail: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub


    Protected Sub gvDWList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDWList.SelectedIndexChanged
        Dim lbtnAsset As New LinkButton
        lbtnAsset = gvDWList.SelectedRow.FindControl("lbtnAsset")

        Session("AssetID") = ""
        Session("AssetID") = lbtnAsset.CommandArgument.ToString
        Response.Redirect("~/Asset/frmAssetDetail.aspx")
    End Sub
End Class
