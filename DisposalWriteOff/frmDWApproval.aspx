<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmDWApproval.aspx.vb" Inherits="DisposalWriteOff_frmDWApproval" ValidateRequest="false"%>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Disposal & Writeoff Approval</title>
     <script type="text/javascript">
         function showDate() 
        {
            var month_names = new Array("Jan", "Feb", "Mar", 
            "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
            "Oct", "Nov", "Dec");

            var d = new Date();
            var curr_day = d.getDay();
            var curr_date = d.getDate();
            var hours = d.getHours();
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            
            var curr_hours;
            var curr_minutes;
            var curr_seconds;

            if(hours<10)
            {
                curr_hours = "0" + hours;
            }
            else
            {
                curr_hours = hours;
            }
            
            if(minutes < 10)
            {
                curr_minutes = "0" + minutes;
            }
             else
            {
                curr_minutes = minutes;
            }
            
           if(seconds < 10)
            {
                curr_seconds = "0" + seconds;
            }
             else
            {
                curr_seconds = seconds;
            }

            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
 
            document.getElementById("hdfCurrentDateTime").value = curr_date + " " +  month_names[curr_month] +  " " + curr_year + " " + curr_hours + ":" + curr_minutes + ":" + curr_seconds
         }
     </script>
</head>
<body>
    <form id="frmDWApproval" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Write Off, Disposal & Obsolete Approval"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:HiddenField ID="hdfCurrentDateTime" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <asp:GridView ID="gvDWList" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        Font-Names="Verdana" Font-Size="X-Small" ForeColor="#333333" GridLines="None" DataKeyNames="WDOID" EmptyDataText="Record Not Found" BorderColor="#E0E0E0" BorderStyle="Solid" BorderWidth="1px">
                        <RowStyle BackColor="#E3EAEB" />
                        <Columns>
                            <asp:TemplateField HeaderText="Select">
                                <ItemTemplate>
                                    <asp:Button ID="btnSelect" runat="server" CommandArgument='<%# Bind("WDOID") %>' Text="Select" CommandName="Select" />
                                    <asp:HiddenField ID="hdfStationID" runat="server" Value='<%# Bind("StationID") %>' />
                                    <asp:HiddenField ID="hdfApprovalLevel" runat="server" Value='<%# Bind("Approval_Level") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="WDOID" HeaderText="DW No" />
                            <asp:BoundField DataField="StationCode" HeaderText="Station" />
                            <asp:BoundField DataField="Year" HeaderText="Year" />
                            <asp:BoundField DataField="WDOType" HeaderText="Type" />
                            <asp:BoundField DataField="CreatedOn" HeaderText="Apply Date" />
                            <asp:BoundField DataField="CreatedBy" HeaderText="Apply By" />
                        </Columns>
                        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="#7C6F57" />
                        <AlternatingRowStyle BackColor="White" />
                        <EmptyDataRowStyle BorderColor="#E0E0E0" ForeColor="Red" />
                    </asp:GridView>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="lblDetail" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                        Font-Underline="True" ForeColor="Blue" Text="Detail :-"></asp:Label></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table width="700">
                        <tr>
                            <td style="width: 100px">
                    <asp:Label ID="lblWDONo" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Obsolete, Disposal & Writeoff No"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:Label ID="lblWDOID" runat="server" Font-Names="Verdana" Font-Size="Small" Font-Bold="True" ForeColor="Blue"></asp:Label>
                                <asp:HiddenField ID="hdfLevel" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px" valign="top">
                                <asp:Label ID="Label6" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Remarks"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td>
                                <asp:Label ID="lblRemarks" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8"><table>
                    <tr>
                        <td style="width: 100px" valign="top">
                            <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Asset List"></asp:Label></td>
                        <td style="width: 1px" valign="top">
                            :</td>
                        <td>
                    <asp:GridView ID="gvDetail" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        Font-Names="Verdana" Font-Size="X-Small" ForeColor="#333333" GridLines="Vertical">
                        <RowStyle BackColor="#E3EAEB" />
                        <Columns>
                            <asp:TemplateField HeaderText="Select">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True" OnCheckedChanged="chkSelectAll_CheckedChanged"
                                        Text="Select" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Asset No">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnAsset" runat="server" CommandArgument='<%# Bind("AssetID") %>'
                                        Text='<%# Bind("AssetNo") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Category" HeaderText="Category" />
                            <asp:BoundField DataField="BrandModel" HeaderText="Brand / Model" />
                            <asp:BoundField DataField="PurchaseDate" HeaderText="Purchase Date" />
                            <asp:BoundField DataField="PurchaseCost" HeaderText="Purchase Cost" />
                            <asp:BoundField DataField="NetBookValue" HeaderText="NetBook Value" />
                            <asp:BoundField DataField="Accumulated_Depreciation" HeaderText="Accumulated Depreciation" />
                            <asp:BoundField DataField="Reason" HeaderText="Reason" />
                        </Columns>
                        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="#7C6F57" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <table width="700">
                        <tr>
                            <td style="width: 100px" valign="top">
                                &nbsp;</td>
                            <td style="width: 1px" valign="top">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px" valign="top">
                    <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Remarks"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td>
                                <ftb:freetextbox id="txtRemarks" runat="server" allowhtmlmode="False" autogeneratetoolbarsfromstring="True"
                                    autoparsestyles="False" breakmode="Paragraph" buttondownimage="False" enablehtmlmode="False"
                                    enableviewstate="False" height="150px" pastemode="Disabled" toolbarlayout="Bold,Italic,Underline,Strikethrough;Superscript,Subscript|BulletedList,NumberedList,Indent,Outdent|FontFacesMenu,FontSizesMenu"
                                    toolbarstyleconfiguration="Office2003" width="540px"></ftb:freetextbox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px" valign="top">
                                &nbsp;</td>
                            <td style="width: 1px" valign="top">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px" valign="top">
                            </td>
                            <td style="width: 1px" valign="top">
                            </td>
                            <td>
                    <table style="width: 500px">
                        <tr>
                            <td style="width: 150px; height: 26px;">
                                <asp:Button ID="btnApprove" runat="server" Font-Names="Verdana" Font-Size="Small"
                                    Text="Approve" Width="85px" OnClientClick="showDate();" /></td>
                            <td style="width: 150px; height: 26px;">
                                <asp:Button ID="btnReject" runat="server" Font-Names="Verdana" Font-Size="Small"
                                    Text="Reject All" Width="85px" OnClientClick="showDate();" /></td>
                            <td style="width: 150px; height: 26px;"><asp:Button ID="btnModificationRequest" runat="server" Font-Names="Verdana" Font-Size="Small"
                                    Text="Modification Request" Width="162px" OnClientClick="showDate();" /></td>
                        </tr>
                    </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px" valign="top">
                                &nbsp;</td>
                            <td style="width: 1px" valign="top">
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table width="700">
                        <tr>
                            <td style="width: 100px;">
                                <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Approval History"></asp:Label></td>
                            <td style="width: 1px;">
                                :</td>
                            <td>
                                <asp:GridView ID="gvApprovalHistory" runat="server" AutoGenerateColumns="False" Font-Names="Verdana"
                                    Font-Size="X-Small">
                                    <Columns>
                                        <asp:BoundField DataField="Approval_Level" HeaderText="Level" />
                                        <asp:BoundField DataField="ApprovedBy" HeaderText="Approval Party" />
                                        <asp:BoundField DataField="ApprovedOn" HeaderText="Date" />
                                        <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                        <asp:BoundField DataField="Status" HeaderText="Status" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
