Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class DisposalWriteOff_frmApplyDW
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        Me.Page.MaintainScrollPositionOnPostBack = True

        If IsPostBack() = False Then
            hdfDWID.Value = ""

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            btnSave.Enabled = False
            btnSend.Enabled = False
            btnVoid.Enabled = False
            btnCompileList.Enabled = False
            gvDWList.Columns(0).Visible = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "D0002")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Apply Write Off/Disposal/Obsolete Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnSave.Enabled = True
                        btnSend.Enabled = True
                        btnVoid.Enabled = True
                        btnCompileList.Enabled = True
                        gvDWList.Columns(0).Visible = True
                    Else
                        Session("blnWrite") = False
                        btnSave.Enabled = False
                        btnSend.Enabled = False
                        btnVoid.Enabled = False
                        btnCompileList.Enabled = False
                        gvDWList.Columns(0).Visible = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            BindStation()
            Session("DA_USERLIST") = ""
            If Session("WDOID") <> Nothing Then
                If Session("WDOID").ToString <> "" Then
                    hdfDWID.Value = Session("WDOID").ToString
                    GetApplyDWData()
                    Session("WDOID") = ""
                End If
            End If
        Else
            If Session("DA_USERLIST") <> "" Then
                If Session("DA_USERLIST_TYPE") = "To" Then
                    'If Session("DA_USERLIST").ToString <> "" Then
                    '    hdfTo.Value = Session("DA_USERLIST").ToString
                    'End If
                    'clsCF.ShowApplyTo(Session("DA_USERLIST").ToString, "To", txtTo, gvCC)
                    ''ShowApplyTo(Session("DA_USERLIST").ToString, "To")
                ElseIf Session("DA_USERLIST_TYPE") = "CC" Then
                    If Session("DA_USERLIST").ToString <> "" Then
                        hdfCC.Value = Session("DA_USERLIST").ToString
                    End If
                    clsCF.ShowApplyTo(Session("DA_USERLIST").ToString, "CC", txtTo, gvCC)
                    'ShowApplyTo(Session("DA_USERLIST").ToString, "CC")
                End If
                Session("DA_USERLIST_TYPE") = ""
                Session("DA_USERLIST") = ""
            Else
                If Session("DA_USERLIST_TYPE") = "To" Then
                    'If Session("DA_USERLIST").ToString <> "" Then
                    '    hdfTo.Value = Session("DA_USERLIST").ToString
                    'End If
                    'clsCF.ShowApplyTo(hdfTo.Value.ToString, "To", txtTo, gvCC)
                    ''ShowApplyTo(hdfTo.Value.ToString, "To")
                ElseIf Session("DA_USERLIST_TYPE") = "CC" Then
                    If Session("DA_USERLIST").ToString <> "" Then
                        hdfCC.Value = Session("DA_USERLIST").ToString
                    End If
                    clsCF.ShowApplyTo(hdfCC.Value.ToString, "CC", txtTo, gvCC)
                    'ShowApplyTo(hdfCC.Value, "CC")
                End If
                Session("DA_USERLIST_TYPE") = ""
                Session("DA_USERLIST") = ""
            End If

            If Session("WDOID") <> Nothing Then
                If Session("WDOID").ToString <> "" Then
                    hdfDWID.Value = Session("WDOID").ToString
                    Session("WDOID") = ""
                    GetApplyDWData()
                End If
            End If
        End If
    End Sub

    Private Sub BindSelectedWDOAssetList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try

                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetDW_AssetList")

                With MyCommand

                    .Parameters.Add("@CategoryID", SqlDbType.BigInt)
                    .Parameters("@CategoryID").Value = 0
                    .Parameters.Add("@ExcludeAssetID", SqlDbType.NVarChar)
                    .Parameters("@ExcludeAssetID").Value = hdfSelectedAssetID.Value.ToString
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)

                gvDWList.Visible = True
                gvDWList.DataSource = MyData
                gvDWList.DataMember = MyData.Tables(1).TableName
                gvDWList.DataBind()


            Catch ex As Exception
                Me.lblMsg.Text = "BindSelectedWDOAssetList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub BindStation()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[AccessStation]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)

                'If MyData.Tables(0).Rows(0)("AccessStation").ToString <> "" Then
                '    dsStation = DIMERCO.SDK.Utilities.LSDK.getStationDataByWithList(MyData.Tables(0).Rows(0)("AccessStation").ToString)
                'Else
                '    dsStation = DIMERCO.SDK.Utilities.ReSM.GetStationList()
                'End If
                'Dim dtView As DataView = dsStation.Tables(0).DefaultView
                'dtView.Sort = "StationCode ASC"

                'ddlStation.DataSource = dtView
                'ddlStation.DataValueField = dtView.Table.Columns("StationID").ToString
                'ddlStation.DataTextField = dtView.Table.Columns("StationCode").ToString
                'ddlStation.DataBind()

                If Session("DA_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("DA_StationID").ToString
                    GetApprovalParty_ByStation()
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub GetApprovalParty_ByStation()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetApprovalParty_ByStation")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[ApprovalParty]"
                    .Parameters.Add("@ApplyNewType", SqlDbType.VarChar, 50)
                    .Parameters("@ApplyNewType").Value = "WDO"

                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "ApprovalParty"

                txtTo.Text = ""

                ddlApprovalPartyIT.DataSource = MyData.Tables(0)
                ddlApprovalPartyIT.DataValueField = MyData.Tables(0).Columns("L1_ApprovalParty").ToString
                ddlApprovalPartyIT.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                ddlApprovalPartyIT.DataBind()

                ddlApprovalPartyNonIT.DataSource = MyData.Tables(1)
                ddlApprovalPartyNonIT.DataValueField = MyData.Tables(1).Columns("L1_ApprovalParty").ToString
                ddlApprovalPartyNonIT.DataTextField = MyData.Tables(1).Columns("nvchFullname").ToString
                ddlApprovalPartyNonIT.DataBind()

                ddlApprovalPartyNonITMajor.DataSource = MyData.Tables(2)
                ddlApprovalPartyNonITMajor.DataValueField = MyData.Tables(2).Columns("L1_ApprovalParty").ToString
                ddlApprovalPartyNonITMajor.DataTextField = MyData.Tables(2).Columns("nvchFullname").ToString
                ddlApprovalPartyNonITMajor.DataBind()

                If MyData.Tables(0).Rows.Count = 0 Then
                    lblMsg.Text = "No Configure Approval Party. Please contact system admin."
                    Exit Sub
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "ddlStation_SelectedIndexChanged: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub GetApplyDWData()
        If hdfDWID.Value.ToString = "" Then
            Exit Sub
        End If

        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetApplyDWData")

                With MyCommand
                    .Parameters.Add("@WDOID", SqlDbType.VarChar, 20)
                    .Parameters("@WDOID").Value = hdfDWID.Value.ToString
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "DWData"

                ddlStation.SelectedValue = MyData.Tables(0).Rows(0)("StationID").ToString
                hdfCC.Value = MyData.Tables(0).Rows(0)("CC").ToString
                clsCF.ShowApplyTo(hdfCC.Value.ToString, "CC", txtTo, gvCC)

                If MyData.Tables(0).Rows(0)("L1_ApprovalParty_IT").ToString <> "" Then
                    ddlApprovalPartyIT.SelectedValue = MyData.Tables(0).Rows(0)("L1_ApprovalParty_IT").ToString
                End If

                If MyData.Tables(0).Rows(0)("L1_ApprovalParty_NonIT").ToString <> "" Then
                    ddlApprovalPartyNonIT.SelectedValue = MyData.Tables(0).Rows(0)("L1_ApprovalParty_NonIT").ToString
                End If

                If MyData.Tables(0).Rows(0)("L1_ApprovalParty_NonITMajor").ToString <> "" Then
                    ddlApprovalPartyNonITMajor.SelectedValue = MyData.Tables(0).Rows(0)("L1_ApprovalParty_NonITMajor").ToString
                End If

                ddlType.SelectedValue = MyData.Tables(0).Rows(0)("WDOType").ToString

                If ddlType.SelectedValue.ToString <> "Select One" Then
                    lblList.Text = ddlType.SelectedItem.Text.ToString + " List :-"
                End If

                txtRemarks.Text = MyData.Tables(0).Rows(0)("Remarks").ToString

                gvDWList.DataSource = MyData
                gvDWList.DataMember = MyData.Tables(1).TableName
                gvDWList.DataBind()

                If hdfDWID.Value.ToString <> "" Then
                    ddlStation.Enabled = False
                Else
                    ddlStation.Enabled = True
                End If

                If MyData.Tables(0).Rows(0)("Status").ToString = "Cancel" Or MyData.Tables(0).Rows(0)("Status").ToString = "Pending" Then
                    btnSave.Enabled = False
                    btnSend.Enabled = False
                    btnVoid.Enabled = False
                    btnClear.Enabled = False
                    btnCompileList.Enabled = False
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "GetApplyDWData: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    'Private Sub ShowApplyTo(ByVal strApplyList As String, ByVal strType As String)
    '    Try
    '        Dim MyData As New DataSet

    '        If strType = "To" Then
    '            If strApplyList = "" Then
    '                txtTo.Text = ""
    '                hdfTo.Value = ""
    '            Else
    '                MyData = DIMERCO.SDK.Utilities.LSDK.getUserDataByList(strApplyList)
    '                txtTo.Text = MyData.Tables(0).Rows(0)("Fullname").ToString
    '            End If

    '        End If

    '        If strType = "CC" Then
    '            MyData = DIMERCO.SDK.Utilities.LSDK.getUserDataByList(strApplyList)
    '            gvCC.DataSource = MyData
    '            gvCC.DataMember = MyData.Tables(0).TableName
    '            gvCC.DataBind()
    '        End If
    '    Catch ex As Exception
    '        Me.lblMsg.Text = "ShowApplyTo: " & ex.Message.ToString
    '    End Try
    'End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveDW("Save")
    End Sub

    Protected Sub lbtnCC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnCC.Click
        Dim strScript As String

        If hdfCC.Value.ToString = "" Then
            Session("DA_USERLIST_TYPE") = "CC"
            Session("DA_USERLIST") = ""
            Session("DA_New_USERLIST") = ""
        Else
            Session("DA_USERLIST_TYPE") = "CC"
            Session("DA_USERLIST") = hdfCC.Value.ToString
        End If

        Session("DA_AddUserStationID") = ""
        Session("DA_AddUserStationID") = Session("DA_StationID").ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmAddUser.aspx?ParentForm=frmApplyDW','AddUser','height=550, width=500,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub gvCC_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvCC.SelectedIndexChanged
        Dim strCC As String
        Dim strRemove As String = ""

        strRemove = gvCC.SelectedValue.ToString

        strCC = hdfCC.Value.ToString.ToUpper()
        strCC = strCC.Replace(strRemove.ToUpper(), "").ToString
        strCC = strCC.Replace(",,", ",").ToString
        hdfCC.Value = strCC
        Session("DA_USERLIST_TYPE") = "CC"
        Session("DA_New_USERLIST") = strCC
        Session("DA_USERLIST") = strCC
        clsCF.ShowApplyTo(strCC, "CC", txtTo, gvCC)
        'ShowApplyTo(strCC, "CC")
    End Sub

    Protected Sub btnVoid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVoid.Click
        SaveDW("Void")
    End Sub

    Private Sub BindSelectedAsset(ByVal strSelectedAssetID As String)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetSelectedAssetDetail")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@SelectedAssetID", SqlDbType.NVarChar)
                    .Parameters("@SelectedAssetID").Value = strSelectedAssetID.ToString
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)

                gvDWList.DataSource = MyData
                gvDWList.DataMember = MyData.Tables(0).TableName
                gvDWList.DataBind()


            Catch ex As Exception
                Me.lblMsg.Text = "BindSelectedAsset: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub gvDWList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDWList.RowDeleting
        Try
            Dim lbtnRemove As New LinkButton
            Dim strAssetID As String = ""
            Dim strSelectedAssetID As String = ""
            lbtnRemove = gvDWList.SelectedRow.FindControl("lbtnRemove")
            strAssetID = lbtnRemove.CommandArgument.ToString
            strSelectedAssetID = hdfSelectedAssetID.Value.Replace(strAssetID, "")
            hdfSelectedAssetID.Value = strSelectedAssetID.Replace("<<", "")

            BindSelectedAsset(hdfSelectedAssetID.Value.ToString)
        Catch ex As Exception
            Me.lblMsg.Text = "gvDWList_RowDeleting: " & ex.Message.ToString
        End Try
    End Sub

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        SaveDW("Send")
    End Sub

    Private Sub SaveDW(ByVal strType As String)
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strWDOID As String = ""
                Dim strMailID As String = ""
                Dim strMsg As String = ""

                If ddlStation.SelectedValue.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Station')</script>")
                    Exit Sub
                End If

                If ddlApprovalPartyIT.Items.Count = 0 Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('No Configure IT Approval Party. Please contact system admin')</script>")
                    Exit Sub
                End If

                If ddlApprovalPartyNonIT.Items.Count = 0 Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('No Configure Non IT Approval Party. Please contact system admin')</script>")
                    Exit Sub
                End If

                If ddlApprovalPartyNonITMajor.Items.Count = 0 Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('No Configure Non IT Major Approval Party. Please contact system admin')</script>")
                    Exit Sub
                End If
                'If gvTo.Rows.Count = 0 Then
                '    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('No Configure Approval Party. Please contact system admin')</script>")
                '    Exit Sub
                'End If

                If ddlType.SelectedValue.ToString = "Select One" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Type')</script>")
                    ddlType.Focus()
                    Exit Sub
                End If

                If gvDWList.Rows.Count = 0 Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Asset to " + ddlType.SelectedItem.Text.ToString + ".')</script>")
                    btnCompileList.Focus()
                    Exit Sub
                End If

                Dim strRemarks As String = ""

                strRemarks = txtRemarks.Text.Replace("<p>", "")
                strRemarks = strRemarks.Replace("</p>", "</br>")

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveWDO", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@WDOID", SqlDbType.NVarChar, 50)
                    .Parameters("@WDOID").Value = hdfDWID.Value.ToString

                    .Parameters.Add("@To_IT", SqlDbType.VarChar, 6)
                    .Parameters("@To_IT").Value = ddlApprovalPartyIT.SelectedValue.ToString
                    .Parameters.Add("@To_NonIT", SqlDbType.VarChar, 6)
                    .Parameters("@To_NonIT").Value = ddlApprovalPartyNonIT.SelectedValue.ToString
                    .Parameters.Add("@To_NonITMajor", SqlDbType.VarChar, 6)
                    .Parameters("@To_NonITMajor").Value = ddlApprovalPartyNonITMajor.SelectedValue.ToString
                    .Parameters.Add("@CC", SqlDbType.NVarChar)
                    .Parameters("@CC").Value = hdfCC.Value.ToString
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@Remarks", SqlDbType.NVarChar)
                    .Parameters("@Remarks").Value = strRemarks
                    .Parameters.Add("@WDOType", SqlDbType.VarChar, 1)
                    .Parameters("@WDOType").Value = ddlType.SelectedValue.ToString
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@Status", SqlDbType.VarChar, 10)
                    If strType = "Save" Then
                        .Parameters("@Status").Value = "D"
                    ElseIf strType = "Send" Then
                        .Parameters("@Status").Value = "P"
                    ElseIf strType = "Void" Then
                        .Parameters("@Status").Value = "C"
                    End If

                    .Parameters.Add("@WDO_ID", SqlDbType.VarChar, 20)
                    .Parameters("@WDO_ID").Direction = ParameterDirection.Output
                    .Parameters.Add("@biMailID", SqlDbType.NVarChar, 500)
                    .Parameters("@biMailID").Direction = ParameterDirection.Output
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()
                    strWDOID = .Parameters("@WDO_ID").Value.ToString()
                    strMailID = .Parameters("@biMailID").Value.ToString()
                    strMsg = .Parameters("@Msg").Value.ToString()

                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    If strType = "Save" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                    Else
                        If strMailID <> "" Then
                            Dim MailID As String() = strMailID.Split(New Char() {","c})
                            Dim intMailID As Integer = 0
                            Dim strStatusMsg As String = ""
                            Dim strResult As String = ""

                            For Each intMailID In MailID

                                strResult = clsCF.SendMail(intMailID)

                                If strResult = "" Then
                                    clsCF.UpdateMailQueue(intMailID, 1, strType + " Successful", hdfCurrentDateTime.Value.ToString)
                                    GetApplyDWData()
                                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strType + " Successful')</script>")

                                    'Response.Redirect("~/DisposalWriteOff/frmDWDetail.aspx")
                                Else
                                    clsCF.UpdateMailQueue(intMailID, 2, "Send mail Fail - " & strResult, hdfCurrentDateTime.Value.ToString)
                                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Send Fail - " & strResult & "')</script>")
                                End If
                            Next
                        Else
                            GetApplyDWData()
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strType + " Successful')</script>")
                            'Response.Redirect("~/DisposalWriteOff/frmDWList.aspx")
                        End If
                    End If
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "SaveDW: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        ResetData()
    End Sub

    Private Sub ResetData()
        Try
            hdfCC.Value = ""
            txtRemarks.Text = ""
            ddlType.SelectedValue = "Select One"
            hdfSelectedAssetID.Value = ""

            'clsCF.ShowApplyTo(hdfCC.Value.ToString, "CC", txtTo, gvCC)
            'ShowApplyTo(hdfCC.Value, "CC")
            BindSelectedAsset(hdfSelectedAssetID.Value.ToString)
        Catch ex As Exception
            Me.lblMsg.Text = "ResetData: " & ex.Message.ToString
        End Try
    End Sub

   
    Protected Sub ddlStation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStation.SelectedIndexChanged
        Session("DA_StationID") = ddlStation.SelectedValue.ToString
        GetApprovalParty_ByStation()
    End Sub

    Protected Sub btnCompileList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCompileList.Click
        Dim strScript As String

        If ddlStation.SelectedValue.ToString = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Station')</script>")
            Exit Sub
        End If

        'If txtTo.Text.ToString = "" Then
        '    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('No Configure Approval Party. Please contact system admin')</script>")
        '    Exit Sub
        'End If

        If ddlType.SelectedValue.ToString = "Select One" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Type')</script>")
            Exit Sub
        End If

        'Session("WDOTo") = hdfTo.Value.ToString
        Session("WDOCC") = hdfCC.Value.ToString
        Session("WDORemarks") = txtRemarks.Text.ToString

        Session("WDOType") = ""
        Session("WDOType") = ddlType.SelectedValue.ToString
        Session("WDOTypeName") = ddlType.SelectedItem.Text.ToString

        Session("WDOID") = ""
        Session("WDOID") = hdfDWID.Value.ToString

        Session("WDOStationID") = ""
        Session("WDOStationID") = ddlStation.SelectedValue.ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('frmCompileWDOList.aspx?ParentForm=frmApplyDW','CompileList','height=700, width=950,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub gvDWList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDWList.RowCommand
        If e.CommandName = "DeleteSelected" Then
            Dim i As Integer
            Dim chkDelete As CheckBox
            Dim hdfAssetID As HiddenField
            Dim blnSelected As Boolean = False
            Dim strSelectedAssetList As String = ""

            For i = 0 To gvDWList.Rows.Count - 1
                chkDelete = gvDWList.Rows(i).FindControl("chkDelete")
                hdfAssetID = gvDWList.Rows(i).FindControl("hdfAssetID")
                If chkDelete.Checked = True Then
                    If strSelectedAssetList = "" Then
                        strSelectedAssetList = hdfAssetID.Value.ToString
                    Else
                        strSelectedAssetList = strSelectedAssetList + "<" + hdfAssetID.Value.ToString
                    End If
                End If
            Next
            If strSelectedAssetList = "" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Asset to delete')</script>")
                Exit Sub
            End If

            DeleteDWOSelectedAssetList(strSelectedAssetList)
        End If
    End Sub

    Private Sub DeleteDWOSelectedAssetList(ByVal strSelectedAssetList As String)
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strDWOID As String = ""
                Dim strMsg As String = ""

                lblMsg.Text = ""

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteWDO_AssetList", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@WDOID", SqlDbType.VarChar, 20)
                    .Parameters("@WDOID").Value = hdfDWID.Value.ToString
                    .Parameters.Add("@To_IT", SqlDbType.VarChar, 6)
                    .Parameters("@To_IT").Value = ddlApprovalPartyIT.SelectedValue.ToString
                    .Parameters.Add("@To_NonIT", SqlDbType.VarChar, 6)
                    .Parameters("@To_NonIT").Value = ddlApprovalPartyNonIT.SelectedValue.ToString
                    .Parameters.Add("@To_NonITMajor", SqlDbType.VarChar, 6)
                    .Parameters("@To_NonITMajor").Value = ddlApprovalPartyNonITMajor.SelectedValue.ToString
                    .Parameters.Add("@CC", SqlDbType.NVarChar)
                    .Parameters("@CC").Value = hdfCC.Value.ToString
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@Remarks", SqlDbType.NVarChar)
                    .Parameters("@Remarks").Value = txtRemarks.Text.ToString
                    .Parameters.Add("@WDOType", SqlDbType.VarChar, 50)
                    .Parameters("@WDOType").Value = ddlType.SelectedValue.ToString

                    .Parameters.Add("@SelectedWDOAssetID", SqlDbType.NVarChar)
                    .Parameters("@SelectedWDOAssetID").Value = strSelectedAssetList

                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString

                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@Status", SqlDbType.VarChar, 1)
                    .Parameters("@Status").Value = "D"

                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    GetApplyDWData()
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")

                End If
            Catch ex As Exception
                Me.lblMsg.Text = "DeleteDWOSelectedAssetList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub chkDeleteAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim i As Int32
        Dim chkDelete As CheckBox
        Dim chkDeleteAll As CheckBox
        chkDeleteAll = CType(gvDWList.HeaderRow.FindControl("chkDeleteAll"), CheckBox)
        For i = 0 To gvDWList.Rows.Count - 1
            chkDelete = CType(gvDWList.Rows(i).FindControl("chkDelete"), CheckBox)
            If chkDelete.Enabled = True Then
                chkDelete.Checked = chkDeleteAll.Checked.ToString
            End If
        Next
    End Sub
End Class
