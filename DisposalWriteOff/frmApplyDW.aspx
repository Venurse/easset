<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmApplyDW.aspx.vb" Inherits="DisposalWriteOff_frmApplyDW" ValidateRequest="false"%>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Apply Disposal, Write Off And Obsolete</title>
     <script type="text/javascript">
         function showDate() 
        {
            var month_names = new Array("Jan", "Feb", "Mar", 
            "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
            "Oct", "Nov", "Dec");

            var d = new Date();
            var curr_day = d.getDay();
            var curr_date = d.getDate();
            var hours = d.getHours();
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            
            var curr_hours;
            var curr_minutes;
            var curr_seconds;

            if(hours<10)
            {
                curr_hours = "0" + hours;
            }
            else
            {
                curr_hours = hours;
            }
            
            if(minutes < 10)
            {
                curr_minutes = "0" + minutes;
            }
             else
            {
                curr_minutes = minutes;
            }
            
           if(seconds < 10)
            {
                curr_seconds = "0" + seconds;
            }
             else
            {
                curr_seconds = seconds;
            }

            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
 
            document.getElementById("hdfCurrentDateTime").value = curr_date + " " +  month_names[curr_month] +  " " + curr_year + " " + curr_hours + ":" + curr_minutes + ":" + curr_seconds
         }
     </script>
</head>
<body>
    <form id="frmApplyDW" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Obsolete, Dispose And Write Off"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="height: 21px">
                </td>
                <td colspan="7" style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <table>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Station"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlStation" runat="server" Font-Names="Verdana" Font-Size="X-Small" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdfDWID" runat="server" />
                                <asp:HiddenField ID="hdfCurrentDateTime" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="To"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:GridView ID="gvTo" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:GridView>
                                <table style="border-right: thin groove; border-top: thin groove; border-left: thin groove;
                                    width: 259px; border-bottom: thin groove">
                                    <tr>
                                        <td style="border-right: thin groove; border-top: thin groove; font-weight: bold;
                                            font-size: x-small; border-left: thin groove; width: 100px; border-bottom: thin groove;
                                            font-family: Verdana">
                                            Item Type</td>
                                        <td style="border-right: thin groove; border-top: thin groove; font-weight: bold;
                                            font-size: x-small; border-left: thin groove; border-bottom: thin groove; font-family: Verdana">
                                            Approval Party</td>
                                    </tr>
                                    <tr>
                                        <td style="border-right: thin groove; border-top: thin groove; font-weight: bold;
                                            font-size: x-small; border-left: thin groove; border-bottom: thin groove; font-family: Verdana">
                                            IT</td>
                                        <td style="border-right: thin groove; border-top: thin groove; font-weight: bold;
                                            font-size: x-small; border-left: thin groove; border-bottom: thin groove; font-family: Verdana">
                                            <asp:DropDownList ID="ddlApprovalPartyIT" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td style="border-right: thin groove; border-top: thin groove; font-weight: bold;
                                            font-size: x-small; border-left: thin groove; border-bottom: thin groove; font-family: Verdana">
                                            Non IT</td>
                                        <td style="border-right: thin groove; border-top: thin groove; font-weight: bold;
                                            font-size: x-small; border-left: thin groove; border-bottom: thin groove; font-family: Verdana">
                                            <asp:DropDownList ID="ddlApprovalPartyNonIT" runat="server" Font-Names="Verdana"
                                                Font-Size="X-Small">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td style="border-right: thin groove; border-top: thin groove; font-weight: bold;
                                            font-size: x-small; border-left: thin groove; border-bottom: thin groove; font-family: Verdana">
                                            Non IT Major</td>
                                        <td style="border-right: thin groove; border-top: thin groove; font-weight: bold;
                                            font-size: x-small; border-left: thin groove; border-bottom: thin groove; font-family: Verdana">
                                            <asp:DropDownList ID="ddlApprovalPartyNonITMajor" runat="server" Font-Names="Verdana"
                                                Font-Size="X-Small">
                                            </asp:DropDownList></td>
                                    </tr>
                                </table>
                                <asp:TextBox ID="txtTo" runat="server" Enabled="False" Font-Names="Verdana" Font-Size="X-Small"
                                    Visible="False" Width="200px"></asp:TextBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:LinkButton ID="lbtnCC" runat="server" Font-Names="Verdana" Font-Size="X-Small">CC</asp:LinkButton></td>
                            <td>
                                :</td>
                            <td>
                                <asp:GridView ID="gvCC" runat="server" AutoGenerateColumns="False" DataKeyNames="UserID"
                                    Font-Names="Verdana" Font-Size="X-Small">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Remove">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnRemove" runat="server" CommandArgument='<%# Bind("UserID") %>'
                                                    CommandName="Select">Remove</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>
                                                <asp:Label ID="Label8" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:HiddenField ID="hdfCC" runat="server" />
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Type"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <asp:DropDownList ID="ddlType" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                        <asp:ListItem>Select One</asp:ListItem>
                        <asp:ListItem Value="O">Obsolete</asp:ListItem>
                        <asp:ListItem Value="D">Disposal</asp:ListItem>
                        <asp:ListItem Value="W">Write Off</asp:ListItem>
                    </asp:DropDownList></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label7" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Remarks"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <ftb:freetextbox id="txtRemarks" runat="server" allowhtmlmode="False" autogeneratetoolbarsfromstring="True"
                                    autoparsestyles="False" breakmode="Paragraph" buttondownimage="False" enablehtmlmode="False"
                                    enableviewstate="False" height="150px" pastemode="Disabled" toolbarlayout="Bold,Italic,Underline,Strikethrough;Superscript,Subscript|BulletedList,NumberedList,Indent,Outdent|FontFacesMenu,FontSizesMenu"
                                    toolbarstyleconfiguration="Office2003" width="600px"></ftb:freetextbox>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Button ID="btnCompileList" runat="server" Font-Names="Verdana" Font-Size="Small"
                        Text="Compile List" />
                    <asp:HiddenField ID="hdfSelectedAssetID" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblList" runat="server" BackColor="GradientActiveCaption" Font-Bold="True"
                        Font-Names="Verdana" Font-Size="X-Small" ForeColor="Blue" Width="100%">Asset List :-</asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:GridView ID="gvDWList" runat="server" AutoGenerateColumns="False" BackColor="White"
                        BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" DataKeyNames="AssetID"
                        EmptyDataText="Record Not Found" Font-Names="Verdana" Font-Size="X-Small">
                        <RowStyle BackColor="White" ForeColor="#330099" />
                        <Columns>
                            <asp:TemplateField HeaderText="Remove">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkDeleteAll" runat="server" AutoPostBack="True" OnCheckedChanged="chkDeleteAll_CheckedChanged" /><asp:Button
                                        ID="btnDelete_gv" runat="server" CommandName="DeleteSelected" Font-Names="Verdana"
                                        Font-Size="X-Small" Height="25px" OnClientClick="showDate(); return confirm('Are you sure you want to remove this Asset?');"
                                        Text="Delete" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnRemove" runat="server" CommandArgument='<%# Bind("AssetID") %>'
                                        CommandName="Delete" Visible="False">Remove</asp:LinkButton>
                                    <asp:CheckBox ID="chkDelete" runat="server" Enabled='<%# Bind("AllowEditDelete") %>' />
                                    <asp:HiddenField ID="hdfAssetID" runat="server" Value='<%# Bind("AssetID") %>' />
                                </ItemTemplate>
                                <HeaderStyle Wrap="False" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="AssetNo" HeaderText="Asset No" />
                            <asp:BoundField DataField="Category" HeaderText="Category" />
                            <asp:BoundField DataField="BrandModel" HeaderText="Brand / Model" />
                            <asp:BoundField DataField="OwnerName" HeaderText="Owner" />
                            <asp:BoundField DataField="Location" HeaderText="Location" />
                            <asp:BoundField DataField="Vendor_Warranty" HeaderText="Vendor Warranty" />
                            <asp:BoundField DataField="PurchaseDate" HeaderText="Purchase Date" />
                            <asp:BoundField DataField="PurchaseCost" HeaderText="Purchase Cost" />
                            <asp:BoundField DataField="NetBookValue" HeaderText="NetBook Value" />
                            <asp:BoundField DataField="Accumulated_Depreciation" HeaderText="Accumulated Depreciation" />
                            <asp:BoundField DataField="Reason" HeaderText="Reason" />
                            <asp:BoundField DataField="ApprovalStatus" HeaderText="Approval Status" />
                        </Columns>
                        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                    </asp:GridView>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table>
                        <tr>
                            <td colspan="3">
                    <table style="width: 500px">
                        <tr>
                            <td style="width: 150px">
                                <asp:Button ID="btnSave" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Save"
                                    Width="85px" OnClientClick="showDate();" /></td>
                            <td style="width: 150px">
                                <asp:Button ID="btnSend" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Send"
                                    Width="85px" OnClientClick="showDate();" /></td>
                            <td style="width: 150px">
                                <asp:Button ID="btnVoid" runat="server" Font-Names="Verdana" Font-Size="Small"
                                    Text="Void" Width="85px" OnClientClick="showDate();" /></td>
                            <td style="width: 150px">
                                <asp:Button ID="btnClear" runat="server" Text="Clear" Width="85px" Visible="False" /></td>
                        </tr>
                    </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 150px">
                            </td>
                            <td style="width: 1px">
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td colspan="5">
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
