<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmDWDetail.aspx.vb" Inherits="DisposalWriteOff_frmDWDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Disposal & Writeoff Detail</title>
</head>
<body>
    <form id="frmDWDetail" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Dispose And Writeoff Detail"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:HiddenField ID="hdfWDOID" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <table>
                        <tr>
                            <td style="width: 100px">
                    <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Station"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:Label ID="lblStation" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Year"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <asp:Label ID="lblYear" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Status"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <asp:Label ID="lblStatus" runat="server" Font-Names="Verdana" Font-Size="X-Small" Font-Bold="True" ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Type"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <asp:Label ID="lblType" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label6" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Apply Date"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <asp:Label ID="lblDate" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label3" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Apply By"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="lblApplyBy" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td valign="top">
                    <asp:Label ID="Label7" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Approval History"></asp:Label></td>
                            <td valign="top">
                                :</td>
                            <td>
                                <asp:GridView ID="gvApprovalHistory" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small">
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <asp:GridView ID="gvDWList" runat="server" AutoGenerateColumns="False" BackColor="White"
                        BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" DataKeyNames="AssetID"
                        EmptyDataText="Record Not Found" Font-Names="Verdana" Font-Size="X-Small">
                        <RowStyle BackColor="White" ForeColor="#330099" />
                        <Columns>
                            <asp:TemplateField HeaderText="Asset No">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("AssetNo") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnAsset" runat="server" CommandArgument='<%# Bind("AssetID") %>'
                                        Text='<%# Bind("AssetNo") %>' CommandName="Select"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Category" HeaderText="Category" />
                            <asp:BoundField DataField="BrandModel" HeaderText="Brand / Model" />
                            <asp:BoundField DataField="OwnerName" HeaderText="Owner" />
                            <asp:BoundField DataField="Location" HeaderText="Location" />
                            <asp:BoundField DataField="Vendor_Warranty" HeaderText="Vendor Warranty" />
                            <asp:BoundField DataField="PurchaseDate" HeaderText="Purchase Date" />
                            <asp:BoundField DataField="PurchaseCost" HeaderText="Purchase Cost" />
                            <asp:BoundField DataField="NetBookValue" HeaderText="NetBook Value" />
                            <asp:BoundField DataField="Accumulated_Depreciation" HeaderText="Accumulated Depreciation" />
                            <asp:BoundField DataField="Reason" HeaderText="Reason" />
                            <asp:BoundField DataField="ApprovalStatus" HeaderText="Approval Status" />
                            <asp:BoundField DataField="ApprovalBy" HeaderText="Approval Party" />
                        </Columns>
                        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                    </asp:GridView>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
