Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class DisposalWriteOff_frmDWApproval
    Inherits System.Web.UI.Page
    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If
        Me.Page.MaintainScrollPositionOnPostBack = True

        If IsPostBack() = False Then

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            btnApprove.Enabled = False
            btnReject.Enabled = False
            btnModificationRequest.Enabled = False


            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "D0004")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Write Off/Disposal/Obsolete Approval Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnApprove.Enabled = True
                        btnReject.Enabled = True
                        btnModificationRequest.Enabled = True

                    Else
                        Session("blnWrite") = False
                        btnApprove.Enabled = False
                        btnReject.Enabled = False
                        btnModificationRequest.Enabled = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            'BindDropDownList()
            btnApprove.Enabled = False
            btnReject.Enabled = False
            btnModificationRequest.Enabled = False
            GetDWApprovalList()
        Else

        End If
    End Sub

    'Private Sub BindDropDownList()
    '    Try
    '        Dim dsStation As New DataSet
    '        Dim MyData As New DataSet
    '        Dim MyAdapter As SqlDataAdapter
    '        Dim MyCommand As SqlCommand
    '        Dim MyConnection As New SqlConnection(strConnectionString)

    '        MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

    '        With MyCommand

    '            .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
    '            .Parameters("@ReSM").Value = strReSM
    '            .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
    '            .Parameters("@ReferenceType").Value = "[AccessStation]"
    '            .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
    '            .Parameters("@UserID").Value = Session("DA_UserID").ToString
    '            .CommandType = CommandType.StoredProcedure
    '            .Connection = MyConnection
    '        End With

    '        MyAdapter = New SqlDataAdapter(MyCommand)
    '        MyAdapter.Fill(MyData)
    '        MyData.Tables(0).TableName = "AccessStation"

    '        If MyData.Tables(0).Rows(0)("AccessStation").ToString <> "" Then
    '            dsStation = DIMERCO.SDK.Utilities.LSDK.getStationDataByWithList(MyData.Tables(0).Rows(0)("AccessStation").ToString)
    '        Else
    '            dsStation = DIMERCO.SDK.Utilities.ReSM.GetStationList()
    '        End If
    '        Dim dtView As DataView = dsStation.Tables(0).DefaultView
    '        dtView.Sort = "StationCode ASC"

    '        ddlStation.DataSource = dtView
    '        ddlStation.DataValueField = dtView.Table.Columns("StationID").ToString
    '        ddlStation.DataTextField = dtView.Table.Columns("StationCode").ToString
    '        ddlStation.DataBind()

    '        If Session("DA_StationID").ToString <> "" Then
    '            ddlStation.SelectedValue = Session("DA_StationID").ToString
    '        End If

    '    Catch ex As Exception
    '        Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
    '    Finally
    '        'sqlConn.Close()
    '    End Try
    'End Sub

    Private Sub GetDWApprovalList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetWDOApprovalList")

                With MyCommand
                    .Parameters.Add("@ApprovalParty", SqlDbType.VarChar, 6)
                    .Parameters("@ApprovalParty").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "DWApprovalList"

                gvDWList.DataSource = MyData
                gvDWList.DataMember = MyData.Tables(0).TableName
                gvDWList.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "GetDWApprovalList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    

    Protected Sub gvDWList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDWList.SelectedIndexChanged
        Dim strWDOID As String = gvDWList.SelectedValue.ToString()
        lblWDOID.Text = strWDOID
        Dim hdfApprovalLevel As New HiddenField
        hdfApprovalLevel = gvDWList.SelectedRow.FindControl("hdfApprovalLevel")

        hdfLevel.Value = hdfApprovalLevel.Value.ToString

        GetDWDetail(strWDOID)
    End Sub

    Private Sub GetDWDetail(ByVal strWDOID As String)
        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetDWData_Approval")

                With MyCommand
                    .Parameters.Add("@WDOID", SqlDbType.VarChar, 20)
                    .Parameters("@WDOID").Value = strWDOID
                    .Parameters.Add("@ApprovalLevel", SqlDbType.VarChar, 5)
                    .Parameters("@ApprovalLevel").Value = hdfLevel.Value.ToString
                    .Parameters.Add("@ApprovalParty", SqlDbType.VarChar, 6)
                    .Parameters("@ApprovalParty").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "DWDetail"
                MyData.Tables(1).TableName = "DWList"

                lblRemarks.Text = MyData.Tables(0).Rows(0)("Remarks").ToString
                lblDetail.Text = MyData.Tables(0).Rows(0)("Type").ToString + " Detail :-"
                lblWDONo.Text = MyData.Tables(0).Rows(0)("Type").ToString + " No :-"

                gvDetail.Visible = True
                gvDetail.DataSource = MyData
                gvDetail.DataMember = MyData.Tables(1).TableName
                gvDetail.DataBind()

                gvApprovalHistory.Visible = True
                gvApprovalHistory.DataSource = MyData
                gvApprovalHistory.DataMember = MyData.Tables(2).TableName
                gvApprovalHistory.DataBind()

                btnApprove.Enabled = True
                btnReject.Enabled = True
                btnModificationRequest.Enabled = True

            Catch ex As Exception
                Me.lblMsg.Text = "GetDWDetail: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub


    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim i As Int32
        Dim chkSelect As CheckBox
        Dim chkSelectAll As CheckBox
        chkSelectAll = CType(gvDetail.HeaderRow.FindControl("chkSelectAll"), CheckBox)
        For i = 0 To gvDetail.Rows.Count - 1
            chkSelect = CType(gvDetail.Rows(i).FindControl("chkSelect"), CheckBox)
            chkSelect.Checked = chkSelectAll.Checked.ToString
        Next
    End Sub

    Protected Sub gvDetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDetail.SelectedIndexChanged
        Dim lbtnAsset As New LinkButton
        lbtnAsset = gvDetail.SelectedRow.FindControl("lbtnAsset")

        Session("AssetID") = ""
        Session("AssetID") = lbtnAsset.CommandArgument.ToString
        Response.Redirect("~/Asset/frmAssetDetail.aspx")
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        WDOApproval("A")
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        WDOApproval("R")
    End Sub

    Protected Sub btnModificationRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificationRequest.Click
        WDOApproval("W")
    End Sub

    Private Sub WDOApproval(ByVal strStatus As String)
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String = ""
                Dim strMailID As String = ""
                Dim i As Integer
                Dim chkSelect As CheckBox
                Dim lbtnAsset As LinkButton
                Dim blnSelected As Boolean = False
                Dim strSelectedAssetID As String = ""

                If strStatus = "A" Then
                    For i = 0 To gvDetail.Rows.Count - 1
                        chkSelect = gvDetail.Rows(i).FindControl("chkSelect")
                        lbtnAsset = gvDetail.Rows(i).FindControl("lbtnAsset")
                        If chkSelect.Checked = True Then
                            If strSelectedAssetID = "" Then
                                strSelectedAssetID = lbtnAsset.CommandArgument.ToString
                            Else
                                strSelectedAssetID = strSelectedAssetID + "<" + lbtnAsset.CommandArgument.ToString
                            End If
                            blnSelected = True
                        End If
                    Next
                    If blnSelected = False Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Asset to Approve')</script>")
                        Exit Sub
                    End If
                Else
                    For i = 0 To gvDetail.Rows.Count - 1
                        lbtnAsset = gvDetail.Rows(i).FindControl("lbtnAsset")
                        If strSelectedAssetID = "" Then
                            strSelectedAssetID = lbtnAsset.CommandArgument.ToString
                        Else
                            strSelectedAssetID = strSelectedAssetID + "<" + lbtnAsset.CommandArgument.ToString
                        End If
                    Next
                End If


                If strStatus = "R" Then
                    If txtRemarks.Text.ToString = "" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please provide Reject Reason')</script>")
                        Exit Sub
                    End If
                End If

                If strStatus = "W" Then
                    If txtRemarks.Text.ToString = "" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please provide the Reason of Modification Request')</script>")
                        Exit Sub
                    End If
                End If


                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_WDOApproval", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@WDOID", SqlDbType.VarChar, 20)
                    .Parameters("@WDOID").Value = lblWDOID.Text.ToString
                    .Parameters.Add("@SelectedAssetID", SqlDbType.NVarChar)
                    .Parameters("@SelectedAssetID").Value = strSelectedAssetID
                    .Parameters.Add("@Remarks", SqlDbType.NVarChar)
                    .Parameters("@Remarks").Value = txtRemarks.Text.ToString

                    .Parameters.Add("@Status", SqlDbType.VarChar, 1)
                    .Parameters("@Status").Value = strStatus
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@ApprovalLevel", SqlDbType.VarChar, 5)
                    .Parameters("@ApprovalLevel").Value = hdfLevel.Value.ToString()

                    .Parameters.Add("@biMailID", SqlDbType.NVarChar, 500)
                    .Parameters("@biMailID").Direction = ParameterDirection.Output
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                    strMailID = .Parameters("@biMailID").Value.ToString()
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    Dim strResult As String = ""
                    Dim MailID As String() = strMailID.Split(New Char() {","c})
                    Dim intMailID As Integer
                    Dim strStatusMsg As String = ""

                    For Each intMailID In MailID
                        strResult = clsCF.SendMail(intMailID)

                        If strResult = "" Then
                            If strStatus = "A" Then
                                strStatusMsg = "Approved."
                            ElseIf strStatus = "R" Then
                                strStatusMsg = "Rejected."
                            Else
                                strStatusMsg = "Sent for Request Modification."
                            End If
                            strStatusMsg = strStatusMsg + " Successful"
                            clsCF.UpdateMailQueue(intMailID, 1, strStatusMsg, hdfCurrentDateTime.Value.ToString)

                        Else
                            strStatusMsg = "Send Mail Fail - " & strResult
                            clsCF.UpdateMailQueue(intMailID, 2, strStatusMsg, hdfCurrentDateTime.Value.ToString)
                        End If
                    Next

                    ClearField()
                    GetDWApprovalList()

                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strStatusMsg + "')</script>")

                End If
            Catch ex As Exception
                Me.lblMsg.Text = "WDOApproval: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Private Sub ClearField()
        txtRemarks.Text = ""
        lblWDOID.Text = ""
        lblRemarks.Text = ""
        hdfLevel.Value = ""
        lblMsg.Text = ""
        lblDetail.Text = "Detail :-"
        lblWDONo.Text = "Obsolete, Disposal & Writeoff No"
        gvApprovalHistory.Dispose()
        gvApprovalHistory.Visible = False
        gvDetail.Dispose()
        gvDetail.Visible = False
        btnApprove.Enabled = False
        btnReject.Enabled = False
        btnModificationRequest.Enabled = False
    End Sub


End Class
