Imports System.Configuration.ConfigurationSettings
Imports System.Data
Imports System.Data.SqlClient


Partial Class frmLogin
    Inherits System.Web.UI.Page

    Dim strExtLink As String
    'Dim clsComF As New clsCommonFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Msg") <> Nothing Then
            If Session("Msg").ToString() <> "" Then
                lblMsg.Text = Session("Msg").ToString()
                Session("Msg") = ""
                lblReLogin.Text = "Re-Login"
            End If
        End If

        If IsPostBack() = False Then

            'strExtLink = ""

            Session("DA_UserName") = ""
            Session("DA_UserID") = ""
            Session("DA_StationID") = ""
            Session("DA_StationCode") = ""
            Session("DA_DepartmentCode") = ""

            BindDropDownList()
            Dim strUserName As String = Request.ServerVariables("LOGON_USER")
            Dim hasDomain As Integer = strUserName.IndexOf("\")
            If hasDomain > 0 Then
                txtUserID.Text = strUserName.Remove(0, hasDomain + 1).ToUpper()
            Else
                txtUserID.Text = strUserName.ToUpper()
            End If

            'Dim dsUserDetail As New DataSet
            'dsUserDetail = DIMERCO.SDK.Utilities.LSDK.getUserProfilebyUserList(txtUserID.Text.ToString)
            'If dsUserDetail.Tables(0).Rows.Count > 0 Then
            '    ddlStation.SelectedValue = dsUserDetail.Tables(0).Rows(0)("StationID").ToString
            'End If


            'If Request.QueryString("ExtLink") <> Nothing Then
            '    If Request.QueryString("ExtLink").ToString().Equals("") = False Then
            '        strExtLink = Request.QueryString("ExtLink").ToString()
            '        Session("LastContentForm") = strExtLink
            '    End If
            'End If
        End If
    End Sub

    Private Sub BindDropDownList()
        Try

            Dim dsStation As New DataSet
            dsStation = DIMERCO.SDK.Utilities.ReSM.GetStationList()
            Dim dtView As DataView = dsStation.Tables(0).DefaultView
            dtView.Sort = "StationCode ASC"

            ddlStation.DataSource = dtView
            ddlStation.DataValueField = dtView.Table.Columns("StationID").ToString
            ddlStation.DataTextField = dtView.Table.Columns("StationCode").ToString
            ddlStation.DataBind()


        Catch ex As Exception
            Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
        End Try

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        txtUserID.Text = ""
        txtPwd.Text = ""
        txtUserID.Focus()
    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim strLoginID As String = ""
        Dim strPassword As String = ""
        Dim strStation As String = ""
        Dim strMsg As String = ""

        lblMsg.Text = ""

        If hdfCurrentDateTime.Value.ToString <> "" Then
            Session("CurrentDateTime") = hdfCurrentDateTime.Value.ToString
        Else
            Session("CurrentDateTime") = Format(DateTime.Now, "dd MMM yyyy hh:mm:ss").ToString
        End If



        'Get Login ID and Password
        strLoginID = Trim(txtUserID.Text.ToString())
        strPassword = Trim(txtPwd.Text.ToString())
        'strStation = ddlStation.SelectedValue.ToString()
        If Len(strLoginID) = 0 Then
            lblMsg.Text = "Please enter User ID."
            Exit Sub
        End If

        If Len(strPassword) = 0 Then
            lblMsg.Text = "Please enter Password."
            Exit Sub
        End If

        'If Len(strStation) = 0 Then
        '    lblMsg.Text = "Please select Station."
        '    Exit Sub
        'End If


        'Check User Login ID And Password
        strMsg = CheckLoginUser(strLoginID, strPassword, strStation)
        If strMsg = "" Then
            Session("AccessRightMsg") = ""
            If lblReLogin.Text = "Re-Login" Then
                lblReLogin.Text = ""
                'Page.RegisterClientScriptBlock("Re-Login", "<script>window.close()</script>")
                Response.Redirect("~/Default.aspx")
            Else
                Page.ClientScript.RegisterStartupScript(GetType(Page), "RefreshParent", "<script language='javascript'>RefreshParent()</script>")
                'Response.Redirect("~/ListHome.aspx")
            End If
        Else
            lblMsg.Text = strMsg.ToString()
            Exit Sub
        End If
    End Sub

    Private Function CheckLoginUser(ByVal strUserID As String, ByVal strPassword As String, ByVal strStation As String) As String
        Dim strMsg As String = ""
        Dim strUserStation As String = ""

        Session("DA_UserID") = ""
        Session("DA_StationID") = ""
        Session("DA_StationCode") = ""

        Try
            Dim lds As New DataSet()
            If DIMERCO.SDK.Utilities.ReSM.CheckUserInfo(strUserID, strPassword, lds) = True Then
                'strUserStation = lds.Tables(0).Rows(0)("StationID").ToString()
                'If strStation <> strUserStation Then
                '    strMsg = "Invalid Station"
                'Else

                'Dim i As Integer
                'For i = 0 To lds.Tables(0).Columns.Count - 1
                '    Dim strcolname As String = lds.Tables(0).Columns(i).ColumnName.ToString
                '    Dim strValue As String = lds.Tables(0).Rows(0)(i).ToString
                'Next

                lds.Tables(0).Rows(0)(0).ToString()
                Session("DA_UserName") = lds.Tables(0).Rows(0)("Fullname").ToString
                Session("DA_UserID") = strUserID.ToUpper.ToString
                Session("DA_StationID") = lds.Tables(0).Rows(0)("StationID").ToString()
                ddlStation.SelectedValue = Session("DA_StationID").ToString
                Session("DA_StationCode") = ddlStation.SelectedItem.Text.ToUpper.ToString
                Session("DA_DepartmentCode") = lds.Tables(0).Rows(0)("DepartmentID").ToString
                'End If

                'Session("DA_UserName") = "	Iris Wang"
                'Session("DA_UserID") = "A0514"
                'Session("DA_StationID") = "012"
                'ddlStation.SelectedValue = Session("DA_StationID").ToString
                'Session("DA_StationCode") = "DFSTPE"
            End If

            Return strMsg
        Catch ex As Exception
            Me.lblMsg.Text = "CheckLoginUser: " & ex.Message.ToString
            strMsg = Me.lblMsg.Text
            Return strMsg
        End Try

    End Function
End Class
