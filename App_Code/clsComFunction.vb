Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Configuration.ConfigurationSettings
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Mail
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System
Imports DIMERCO.SDK



Public Class clsComFunction

    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")
    Dim strRightCode As String = System.Configuration.ConfigurationManager.AppSettings("RightCode")
    Dim strConnectionString As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionString")

    Public Function CheckUserAccessRight(ByVal strUserID As String, ByVal strScreenID As String) As String
        Dim blnAccess As String = 0
        Dim blnReadOnly As String = 0
        Dim blnWrite As String = 0
        Dim blnModifyOthers As String = 0
        Dim blnDeleteOthers As String = 0
        Dim blnApproval As String = 0
        Dim strAccessStation As String = ""

        Dim AccessRight As String = ""

        Dim strMsg As String = ""

        Dim MyData As New DataSet
        Dim MyAdapter As SqlDataAdapter
        Dim MyCommand As SqlCommand
        Dim MyConnection As New SqlConnection(strConnectionString)


        Try

            MyCommand = New SqlCommand("Proc_RoleAccessRight")

            With MyCommand

                .Parameters.Add("@UserID", SqlDbType.VarChar, 50)
                .Parameters("@UserID").Value = strUserID
                .Parameters.Add("@ScreenID", SqlDbType.VarChar, 50)
                .Parameters("@ScreenID").Value = strScreenID
                .CommandType = CommandType.StoredProcedure
                .Connection = MyConnection
            End With

            MyAdapter = New SqlDataAdapter(MyCommand)
            MyAdapter.Fill(MyData)

            blnAccess = MyData.Tables(0).Rows(0)("Access").ToString()
            blnReadOnly = MyData.Tables(0).Rows(0)("ReadOnly").ToString()
            blnWrite = MyData.Tables(0).Rows(0)("Write").ToString()
            blnModifyOthers = MyData.Tables(0).Rows(0)("ModifyOthers").ToString()
            blnDeleteOthers = MyData.Tables(0).Rows(0)("DeleteOthers").ToString()
            blnApproval = MyData.Tables(0).Rows(0)("Approval").ToString()
            strAccessStation = MyData.Tables(0).Rows(0)("AccessStation").ToString()


            strMsg = blnAccess + "" + blnReadOnly + "" + blnWrite + "" + blnModifyOthers + "" + blnDeleteOthers + "" + blnApproval + "" + strAccessStation


            'Dim sqlConn As New SqlConnection(strConnectionString)

            'Try
            '    sqlConn.Open()
            '    Dim cmd As New SqlCommand("[Proc_RoleAccessRight_testing]", sqlConn)
            '    With cmd
            '        .CommandType = CommandType.StoredProcedure
            '        .Parameters.Add("@UserID", SqlDbType.VarChar, 50)
            '        .Parameters("@UserID").Value = strUserID
            '        .Parameters.Add("@ScreenID", SqlDbType.VarChar, 50)
            '        .Parameters("@ScreenID").Value = strScreenID
            '        '.Parameters.Add("@ErrorMsg", SqlDbType.VarChar, 255)
            '        '.Parameters("@ErrorMsg").Direction = ParameterDirection.Output
            '        .Parameters.Add("@Access", SqlDbType.TinyInt, 1)
            '        .Parameters("@Access").Direction = ParameterDirection.Output
            '        .Parameters.Add("@ReadOnly", SqlDbType.TinyInt, 1)
            '        .Parameters("@ReadOnly").Direction = ParameterDirection.Output
            '        .Parameters.Add("@Write", SqlDbType.TinyInt, 1)
            '        .Parameters("@Write").Direction = ParameterDirection.Output
            '        .Parameters.Add("@ModifyOthers", SqlDbType.TinyInt, 1)
            '        .Parameters("@ModifyOthers").Direction = ParameterDirection.Output
            '        .Parameters.Add("@DeleteOthers", SqlDbType.TinyInt, 1)
            '        .Parameters("@DeleteOthers").Direction = ParameterDirection.Output
            '        .Parameters.Add("@Approval", SqlDbType.TinyInt, 1)
            '        .Parameters("@Approval").Direction = ParameterDirection.Output
            '        .Parameters.Add("@AccessStation", SqlDbType.NVarChar)
            '        .Parameters("@AccessStation").Direction = ParameterDirection.Output
            '        .CommandTimeout = 0
            '        .ExecuteNonQuery()

            '        'strMsg = .Parameters("@ErrorMsg").Value.ToString()

            '        'If strMsg = "" Then
            '        blnAccess = .Parameters("@Access").Value.ToString
            '        blnReadOnly = .Parameters("@ReadOnly").Value.ToString
            '        blnWrite = .Parameters("@Write").Value.ToString()
            '        blnModifyOthers = .Parameters("@ModifyOthers").Value.ToString()
            '        blnDeleteOthers = .Parameters("@DeleteOthers").Value.ToString()
            '        blnApproval = .Parameters("@Approval").Value.ToString
            '        strAccessStation = .Parameters("@AccessStation").Value.ToString

            '        strMsg = blnAccess + "" + blnReadOnly + "" + blnWrite + "" + blnModifyOthers + "" + blnDeleteOthers + "" + blnApproval + "" + strAccessStation
            '        'End If
            '    End With

            Return strMsg

        Catch ex As Exception
            Return strMsg
        Finally
            'sqlConn.Close()
        End Try
    End Function

    ''' <summary>
    ''' Trim string value, change double space into single space
    ''' </summary>
    ''' <param name="strValue">A System.String reference.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' 

    Public Function TrimAll(ByRef strValue As String) As String

        ''# Convert Null to Empty #
        If strValue = Nothing Then
            strValue = ""
        End If

        ''# Trim the double space into single space #
        While strValue.Contains("  ") = True
            strValue = strValue.Replace("  ", " ")
        End While

        Return Trim(strValue)

    End Function

    Public Function SendMail(ByVal intMailID As Integer) As String
        Dim strMsg As String = ""
        Dim strMailTo As String = ""
        Dim strMailCC As String = ""
        Dim strMailBCC As String = ""
        Dim strSubject As String = ""
        Dim strMessage As String = ""
        Dim strMailServerIP As String = ""
        Dim strFilePath As String = ""
        Dim strMailContentHeader As String = ""
        Dim strMailContentFooter As String = ""
        Dim strMailTitle As String = ""

        Try
            Dim MyData As New DataSet
            Dim MyAdapter As SqlDataAdapter
            Dim MyCommand As SqlCommand
            Dim MyConnection As New SqlConnection(strConnectionString)

            MyCommand = New SqlCommand("SP_GetMailQueueData")

            With MyCommand
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@biMailID", SqlDbType.BigInt)
                .Parameters("@biMailID").Value = intMailID
                .Connection = MyConnection
            End With

            MyAdapter = New SqlDataAdapter(MyCommand)
            MyAdapter.Fill(MyData)


            If MyData.Tables(0).Rows.Count > 0 Then
                strMailTo = MyData.Tables(0).Rows(0)("nvchMailTo").ToString
                strMailCC = MyData.Tables(0).Rows(0)("nvchMailCC").ToString
                strMailBCC = MyData.Tables(0).Rows(0)("nvchMailBCC").ToString
                strSubject = MyData.Tables(0).Rows(0)("MsgTitle").ToString
                strMessage = MyData.Tables(0).Rows(0)("MsgContent").ToString
                strMailServerIP = MyData.Tables(0).Rows(0)("MailServerIP").ToString
                strMailContentHeader = MyData.Tables(0).Rows(0)("nvchMailContentHeader").ToString
                strMailContentFooter = MyData.Tables(0).Rows(0)("nvchMailContentFooter").ToString
                strMailTitle = MyData.Tables(0).Rows(0)("nvchMailTitle").ToString

            End If

            'If MyData.Tables(1).Rows.Count > 0 Then
            '    strFilePath = MyData.Tables(1).Rows(0)("FilePath")
            'End If

            Dim Subject As String = ""

            If strSubject <> "" Then
                Subject = strSubject
            Else
                Subject = strMailTitle
            End If

            Dim mgr As DIMERCO.SDK.Communication = New DIMERCO.SDK.Communication()

            mgr.SendToMailBox(Subject, strMailContentHeader + strMessage + strMailContentFooter, """Dimerco eAsset"" <Dimerco_eAsset@Dimerco.com>", """Dimerco eAsset"" <Dimerco_eAsset@Dimerco.com>", strMailTo, strMailCC, strMailBCC, "", "", True)



            'Dim mail As New MailMessage()
            'mail.To = strMailTo
            'mail.Cc = strMailCC
            'mail.Bcc = strMailBCC
            'mail.From = """Dimerco eAsset"" <Dimerco_eAsset@Dimerco.com>"
            'If strSubject <> "" Then
            '    mail.Subject = strSubject
            'Else
            '    mail.Subject = strMailTitle
            'End If

            'mail.BodyFormat = MailFormat.Html
            'mail.Body = strMailContentHeader + strMessage + strMailContentFooter
            'mail.BodyEncoding = System.Text.Encoding.GetEncoding("GB2312")

            ''If MyData.Tables(1).Rows.Count > 0 Then
            ''    For i = 0 To MyData.Tables(1).Rows.Count - 1
            ''        strFilePath = MyData.Tables(1).Rows(i)("FilePath")
            ''        Dim attachment As New MailAttachment(strFilePath) 'create the attachment
            ''        mail.Attachments.Add(attachment) 'add the attachment
            ''    Next
            ''End If

            'SmtpMail.SmtpServer = strMailServerIP
            'SmtpMail.Send(mail)

            Return strMsg

        Catch err As Exception
            strMsg = err.Message.ToString
            Return strMsg
        End Try

    End Function

    Public Function SendMail_WithAttach(ByVal intMailID As Integer) As String
        Dim strMsg As String = ""
        Dim i As Integer
        Dim strMailTo As String = ""
        Dim strMailCC As String = ""
        Dim strMailBCC As String = ""
        Dim strSubject As String = ""
        Dim strMessage As String = ""
        Dim strMailServerIP As String = ""
        Dim strFilePath As String = ""
        Dim strMailContentHeader As String = ""
        Dim strMailContentFooter As String = ""
        Dim strMailTitle As String = ""

        Try
            Dim MyData As New DataSet
            Dim MyAdapter As SqlDataAdapter
            Dim MyCommand As SqlCommand
            Dim MyConnection As New SqlConnection(strConnectionString)

            MyCommand = New SqlCommand("SP_GetMailQueueData")

            With MyCommand
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@biMailID", SqlDbType.BigInt)
                .Parameters("@biMailID").Value = intMailID
                .Connection = MyConnection
            End With

            MyAdapter = New SqlDataAdapter(MyCommand)
            MyAdapter.Fill(MyData)


            If MyData.Tables(0).Rows.Count > 0 Then
                strMailTo = MyData.Tables(0).Rows(0)("nvchMailTo").ToString
                strMailCC = MyData.Tables(0).Rows(0)("nvchMailCC").ToString
                strMailBCC = MyData.Tables(0).Rows(0)("nvchMailBCC").ToString
                strSubject = MyData.Tables(0).Rows(0)("MsgTitle").ToString
                strMessage = MyData.Tables(0).Rows(0)("MsgContent").ToString
                strMailServerIP = MyData.Tables(0).Rows(0)("MailServerIP").ToString
                strMailContentHeader = MyData.Tables(0).Rows(0)("nvchMailContentHeader").ToString
                strMailContentFooter = MyData.Tables(0).Rows(0)("nvchMailContentFooter").ToString
                strMailTitle = MyData.Tables(0).Rows(0)("nvchMailTitle").ToString

            End If

            'If MyData.Tables(1).Rows.Count > 0 Then
            '    strFilePath = MyData.Tables(1).Rows(0)("FilePath")
            'End If

            Dim Subject As String = ""
            Dim Body As String = ""
            Dim Attachments As String = ""

            If strSubject <> "" Then
                Subject = strSubject
            Else
                Subject = strMailTitle
            End If

            Body = strMailContentHeader + strMessage + strMailContentFooter

            If MyData.Tables(1).Rows.Count > 0 Then
                For i = 0 To MyData.Tables(1).Rows.Count - 1
                    strFilePath = MyData.Tables(1).Rows(i)("FilePath")
                    If Attachments = "" Then
                        Attachments = strFilePath
                    Else
                        Attachments = Attachments + "," + strFilePath
                    End If
                Next
            End If


            Dim mgr As DIMERCO.SDK.Communication = New DIMERCO.SDK.Communication()

            mgr.SendToMailBox(Subject, Body, """Dimerco eAsset"" <Dimerco_eAsset@Dimerco.com>", """Dimerco eAsset"" <Dimerco_eAsset@Dimerco.com>", strMailTo, strMailCC, strMailBCC, "", Attachments, True)




            'Dim mail As New MailMessage()
            'mail.To = strMailTo
            'mail.Cc = strMailCC
            'mail.Bcc = strMailBCC
            'mail.From = """Dimerco eAsset"" <Dimerco_eAsset@Dimerco.com>"
            'If strSubject <> "" Then
            '    mail.Subject = strSubject
            'Else
            '    mail.Subject = strMailTitle
            'End If

            'mail.BodyFormat = MailFormat.Html
            'mail.Body = strMailContentHeader + strMessage + strMailContentFooter
            'mail.BodyEncoding = System.Text.Encoding.GetEncoding("GB2312")

            'If MyData.Tables(1).Rows.Count > 0 Then
            '    For i = 0 To MyData.Tables(1).Rows.Count - 1
            '        strFilePath = MyData.Tables(1).Rows(i)("FilePath")
            '        Dim attachment As New MailAttachment(strFilePath) 'create the attachment
            '        mail.Attachments.Add(attachment) 'add the attachment
            '    Next
            'End If

            'SmtpMail.SmtpServer = strMailServerIP
            'SmtpMail.Send(mail)

            Return strMsg

        Catch err As Exception
            strMsg = err.Message.ToString
            Return strMsg
        End Try

    End Function

    Public Function SendMail_ApplyNew(ByVal intMailID As Integer, ByVal strApplyNewID As String) As String
        Dim strMsg As String = ""
        Dim i As Integer = 0
        Dim strMailTo As String = ""
        Dim strMailCC As String = ""
        Dim strMailBCC As String = ""
        Dim strSubject As String = ""
        Dim strMessage As String = ""
        Dim strMailServerIP As String = ""
        Dim strFilePath As String = ""
        Dim strMailContentHeader As String = ""
        Dim strMailContentFooter As String = ""
        Dim strMailTitle As String = ""

        Try
            Dim MyData As New DataSet
            Dim MyAdapter As SqlDataAdapter
            Dim MyCommand As SqlCommand
            Dim MyConnection As New SqlConnection(strConnectionString)

            MyCommand = New SqlCommand("SP_GetMailQueueData_ApplyNew")

            With MyCommand
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@biMailID", SqlDbType.BigInt)
                .Parameters("@biMailID").Value = intMailID
                '.Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                '.Parameters("@ApplyNewID").Value = strApplyNewID

                .Connection = MyConnection
            End With

            MyAdapter = New SqlDataAdapter(MyCommand)
            MyAdapter.Fill(MyData)


            If MyData.Tables(0).Rows.Count > 0 Then
                strMailTo = MyData.Tables(0).Rows(0)("nvchMailTo").ToString
                strMailCC = MyData.Tables(0).Rows(0)("nvchMailCC").ToString
                strMailBCC = MyData.Tables(0).Rows(0)("nvchMailBCC").ToString
                strSubject = MyData.Tables(0).Rows(0)("MsgTitle").ToString
                strMessage = MyData.Tables(0).Rows(0)("MsgContent").ToString
                strMailServerIP = MyData.Tables(0).Rows(0)("MailServerIP").ToString
                strMailContentHeader = MyData.Tables(0).Rows(0)("nvchMailContentHeader").ToString
                strMailContentFooter = MyData.Tables(0).Rows(0)("nvchMailContentFooter").ToString
                strMailTitle = MyData.Tables(0).Rows(0)("nvchMailTitle").ToString

            End If

            'If MyData.Tables(1).Rows.Count > 0 Then
            '    strFilePath = MyData.Tables(1).Rows(0)("FilePath")
            'End If

            Dim Subject As String = ""

            If strSubject <> "" Then
                Subject = strSubject
            Else
                Subject = strMailTitle
            End If

            Dim mgr As DIMERCO.SDK.Communication = New DIMERCO.SDK.Communication()

            mgr.SendToMailBox(Subject, strMailContentHeader + strMessage + strMailContentFooter, """Dimerco eAsset"" <Dimerco_eAsset@Dimerco.com>", """Dimerco eAsset"" <Dimerco_eAsset@Dimerco.com>", strMailTo, strMailCC, strMailBCC, "", "", True)




            'Dim mail As New MailMessage()
            'mail.To = strMailTo
            'mail.Cc = strMailCC
            'mail.Bcc = strMailBCC
            'mail.From = """Dimerco eAsset"" <Dimerco_eAsset@Dimerco.com>"
            'If strSubject <> "" Then
            '    mail.Subject = strSubject
            'Else
            '    mail.Subject = strMailTitle
            'End If


            'mail.BodyFormat = MailFormat.Html
            'mail.Body = strMailContentHeader + strMessage + strMailContentFooter
            'mail.BodyEncoding = System.Text.Encoding.GetEncoding("GB2312")

            ''If MyData.Tables(1).Rows.Count > 0 Then
            ''    For i = 0 To MyData.Tables(1).Rows.Count - 1
            ''        strFilePath = MyData.Tables(1).Rows(i)("FilePath")
            ''        Dim attachment As New MailAttachment(strFilePath) 'create the attachment
            ''        mail.Attachments.Add(attachment) 'add the attachment
            ''    Next
            ''End If

            'SmtpMail.SmtpServer = strMailServerIP
            'SmtpMail.Send(mail)

            Return strMsg



        Catch err As Exception
            strMsg = err.Message.ToString
            Return strMsg
        End Try

    End Function

    Public Function SendMail_WithoutAttachment(ByVal intMailID As Integer) As String
        Dim strMsg As String = ""
        Dim i As Integer
        Dim strMailTo As String = ""
        Dim strMailCC As String = ""
        Dim strMailBCC As String = ""
        Dim strSubject As String = ""
        Dim strMessage As String = ""
        Dim strMailServerIP As String = ""
        Dim strFilePath As String = ""
        Dim strMailContentHeader As String = ""
        Dim strMailContentFooter As String = ""
        Dim strMailTitle As String = ""

        Try
            Dim MyData As New DataSet
            Dim MyAdapter As SqlDataAdapter
            Dim MyCommand As SqlCommand
            Dim MyConnection As New SqlConnection(strConnectionString)

            MyCommand = New SqlCommand("SP_GetMailQueueData")

            With MyCommand
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@biMailID", SqlDbType.BigInt)
                .Parameters("@biMailID").Value = intMailID
                .Connection = MyConnection
            End With

            MyAdapter = New SqlDataAdapter(MyCommand)
            MyAdapter.Fill(MyData)


            If MyData.Tables(0).Rows.Count > 0 Then
                strMailTo = MyData.Tables(0).Rows(0)("nvchMailTo").ToString
                strMailCC = MyData.Tables(0).Rows(0)("nvchMailCC").ToString
                strMailBCC = MyData.Tables(0).Rows(0)("nvchMailBCC").ToString
                strSubject = MyData.Tables(0).Rows(0)("nvchMsgTitle").ToString
                strMessage = MyData.Tables(0).Rows(0)("nvchMsgContent").ToString
                strMailServerIP = MyData.Tables(0).Rows(0)("MailServerIP").ToString
                strMailContentHeader = MyData.Tables(0).Rows(0)("nvchMailContentHeader").ToString
                strMailContentFooter = MyData.Tables(0).Rows(0)("nvchMailContentFooter").ToString
                strMailTitle = MyData.Tables(0).Rows(0)("nvchMailTitle").ToString

            End If

            'If MyData.Tables(1).Rows.Count > 0 Then
            '    strFilePath = MyData.Tables(1).Rows(0)("FilePath")
            'End If

            Dim Subject As String = ""

            If strSubject <> "" Then
                Subject = strSubject
            Else
                Subject = strMailTitle
            End If

            Dim mgr As DIMERCO.SDK.Communication = New DIMERCO.SDK.Communication()

            mgr.SendToMailBox(Subject, strMailContentHeader + strMessage + strMailContentFooter, """Dimerco eAsset"" <Dimerco_eAsset@Dimerco.com>", """Dimerco eAsset"" <Dimerco_eAsset@Dimerco.com>", strMailTo, strMailCC, strMailBCC, "", "", True)



            'Dim mail As New MailMessage()
            'mail.To = strMailTo
            'mail.Cc = strMailCC
            'mail.Bcc = strMailBCC
            'mail.From = """Dimerco eAsset"" <Dimerco_eAsset@Dimerco.com>"
            'If strSubject <> "" Then
            '    mail.Subject = strSubject
            'Else
            '    mail.Subject = strMailTitle
            'End If

            'mail.BodyFormat = MailFormat.Html
            'mail.Body = strMailContentHeader + strMessage + strMailContentFooter

            ''If MyData.Tables(1).Rows.Count > 0 Then
            ''    For i = 0 To MyData.Tables(1).Rows.Count - 1
            ''        strFilePath = MyData.Tables(1).Rows(i)("FilePath")
            ''        Dim attachment As New MailAttachment(strFilePath) 'create the attachment
            ''        mail.Attachments.Add(attachment) 'add the attachment
            ''    Next
            ''End If

            'SmtpMail.SmtpServer = strMailServerIP
            'SmtpMail.Send(mail)

            Return strMsg

        Catch err As Exception
            strMsg = err.Message.ToString
            Return strMsg
        End Try

    End Function

    'Public Function SendMail_2(ByVal intMailID As Integer) As String
    '    ''Send mail - Without Contents

    '    Dim strMsg As String = ""
    '    Dim i As Integer
    '    Dim strMailTo As String = ""
    '    Dim strMailCC As String = ""
    '    Dim strMailBCC As String = ""
    '    Dim strSubject As String = ""
    '    Dim strMessage As String = ""
    '    Dim strMailServerIP As String = ""
    '    Dim strFilePath As String = ""
    '    Dim strMailContentHeader As String = ""
    '    Dim strMailContentFooter As String = ""
    '    Dim strMailTitle As String = ""

    '    Try
    '        Dim MyData As New DataSet
    '        Dim MyAdapter As SqlDataAdapter
    '        Dim MyCommand As SqlCommand
    '        Dim MyConnection As New SqlConnection(strConnectionString)

    '        MyCommand = New SqlCommand("Proc_GetMailQueueData")

    '        With MyCommand
    '            .CommandType = CommandType.StoredProcedure
    '            .Parameters.Add("@biMailID", SqlDbType.BigInt)
    '            .Parameters("@biMailID").Value = intMailID
    '            .Connection = MyConnection
    '        End With

    '        MyAdapter = New SqlDataAdapter(MyCommand)
    '        MyAdapter.Fill(MyData)


    '        If MyData.Tables(0).Rows.Count > 0 Then
    '            strMailTo = MyData.Tables(0).Rows(0)("nvchMailTo").ToString
    '            strMailCC = MyData.Tables(0).Rows(0)("nvchMailCC").ToString
    '            strMailBCC = MyData.Tables(0).Rows(0)("nvchMailBCC").ToString
    '            strSubject = MyData.Tables(0).Rows(0)("nvchMsgTitle").ToString
    '            'strMessage = MyData.Tables(0).Rows(0)("nvchMsgContent").ToString
    '            strMailServerIP = MyData.Tables(0).Rows(0)("MailServerIP").ToString
    '            strMailContentHeader = MyData.Tables(0).Rows(0)("nvchMailContentHeader").ToString
    '            strMailContentFooter = MyData.Tables(0).Rows(0)("nvchMailContentFooter").ToString
    '            strMailTitle = MyData.Tables(0).Rows(0)("nvchMailTitle").ToString

    '        End If

    '        If MyData.Tables(1).Rows.Count > 0 Then
    '            strFilePath = MyData.Tables(1).Rows(0)("FilePath")
    '        End If


    '        Dim mail As New MailMessage()
    '        mail.To = strMailTo
    '        mail.Cc = strMailCC
    '        mail.Bcc = strMailBCC
    '        mail.From = """MyHome"" <MyHome@Dimerco.com>"
    '        If strSubject <> "" Then
    '            mail.Subject = strSubject
    '        Else
    '            mail.Subject = strMailTitle
    '        End If

    '        mail.BodyFormat = MailFormat.Html
    '        mail.Body = strMailContentHeader + strMessage + strMailContentFooter
    '        mail.BodyEncoding = System.Text.Encoding.GetEncoding("GB2312")

    '        If MyData.Tables(1).Rows.Count > 0 Then
    '            For i = 0 To MyData.Tables(1).Rows.Count - 1
    '                strFilePath = MyData.Tables(1).Rows(i)("FilePath")
    '                Dim attachment As New MailAttachment(strFilePath) 'create the attachment
    '                mail.Attachments.Add(attachment) 'add the attachment
    '            Next
    '        End If

    '        SmtpMail.SmtpServer = strMailServerIP
    '        SmtpMail.Send(mail)

    '        Return strMsg

    '    Catch err As Exception
    '        strMsg = err.Message.ToString
    '        Return strMsg
    '    End Try

    'End Function

    'Public Function SendMail_MySupport(ByVal intMailID As Integer, ByVal strgidMySupportID As String) As String
    '    Dim strMsg As String = ""
    '    Dim i As Integer
    '    Dim strMailTo As String = ""
    '    Dim strMailCC As String = ""
    '    Dim strMailBCC As String = ""
    '    Dim strSubject As String = ""
    '    Dim strMessage As String = ""
    '    Dim strMailServerIP As String = ""
    '    Dim strFilePath As String = ""
    '    Dim strMailContentHeader As String = ""
    '    Dim strMailContentFooter As String = ""
    '    Dim strMailTitle As String = ""

    '    Try
    '        Dim MyData As New DataSet
    '        Dim MyAdapter As SqlDataAdapter
    '        Dim MyCommand As SqlCommand
    '        Dim MyConnection As New SqlConnection(strConnectionString)

    '        MyCommand = New SqlCommand("Proc_GetMailQueueData_MySupport")

    '        With MyCommand
    '            .CommandType = CommandType.StoredProcedure
    '            .Parameters.Add("@biMailID", SqlDbType.BigInt)
    '            .Parameters("@biMailID").Value = intMailID
    '            .Parameters.Add("@gidMySupportID", SqlDbType.NVarChar, 50)
    '            .Parameters("@gidMySupportID").Value = strgidMySupportID

    '            .Connection = MyConnection
    '        End With

    '        MyAdapter = New SqlDataAdapter(MyCommand)
    '        MyAdapter.Fill(MyData)


    '        If MyData.Tables(0).Rows.Count > 0 Then
    '            strMailTo = MyData.Tables(0).Rows(0)("nvchMailTo").ToString
    '            strMailCC = MyData.Tables(0).Rows(0)("nvchMailCC").ToString
    '            strMailBCC = MyData.Tables(0).Rows(0)("nvchMailBCC").ToString
    '            strSubject = MyData.Tables(0).Rows(0)("nvchMsgTitle").ToString
    '            strMessage = MyData.Tables(0).Rows(0)("nvchMsgContent").ToString
    '            strMailServerIP = MyData.Tables(0).Rows(0)("MailServerIP").ToString
    '            strMailContentHeader = MyData.Tables(0).Rows(0)("nvchMailContentHeader").ToString
    '            strMailContentFooter = MyData.Tables(0).Rows(0)("nvchMailContentFooter").ToString
    '            strMailTitle = MyData.Tables(0).Rows(0)("nvchMailTitle").ToString

    '        End If

    '        If MyData.Tables(1).Rows.Count > 0 Then
    '            strFilePath = MyData.Tables(1).Rows(0)("FilePath")
    '        End If


    '        Dim mail As New MailMessage()
    '        mail.To = strMailTo
    '        mail.Cc = strMailCC
    '        mail.Bcc = strMailBCC
    '        mail.From = """MySupport"" <MySupport@Dimerco.com>"
    '        If strSubject <> "" Then
    '            mail.Subject = strSubject
    '        Else
    '            mail.Subject = strMailTitle
    '        End If

    '        mail.BodyFormat = MailFormat.Html
    '        mail.Body = strMailContentHeader + strMessage + strMailContentFooter

    '        If MyData.Tables(1).Rows.Count > 0 Then
    '            For i = 0 To MyData.Tables(1).Rows.Count - 1
    '                strFilePath = MyData.Tables(1).Rows(i)("FilePath")
    '                Dim attachment As New MailAttachment(strFilePath) 'create the attachment
    '                mail.Attachments.Add(attachment) 'add the attachment
    '            Next
    '        End If

    '        SmtpMail.SmtpServer = strMailServerIP
    '        SmtpMail.Send(mail)

    '        Return strMsg

    '    Catch err As Exception
    '        strMsg = err.Message.ToString
    '        Return strMsg
    '    End Try

    'End Function

    'Public Function SendMail_MyTask(ByVal intMailID As Integer, ByVal strgidMyTaskID As String) As String
    '    Dim strMsg As String = ""
    '    Dim i As Integer
    '    Dim strMailTo As String = ""
    '    Dim strMailCC As String = ""
    '    Dim strMailBCC As String = ""
    '    Dim strSubject As String = ""
    '    Dim strMessage As String = ""
    '    Dim strMailServerIP As String = ""
    '    Dim strFilePath As String = ""
    '    Dim strMailContentHeader As String = ""
    '    Dim strMailContentFooter As String = ""
    '    Dim strMailTitle As String = ""

    '    Try
    '        Dim MyData As New DataSet
    '        Dim MyAdapter As SqlDataAdapter
    '        Dim MyCommand As SqlCommand
    '        Dim MyConnection As New SqlConnection(strConnectionString)

    '        MyCommand = New SqlCommand("Proc_GetMailQueueData_MyTask")

    '        With MyCommand
    '            .CommandType = CommandType.StoredProcedure
    '            .Parameters.Add("@biMailID", SqlDbType.BigInt)
    '            .Parameters("@biMailID").Value = intMailID
    '            .Parameters.Add("@gidMyTaskID", SqlDbType.NVarChar, 50)
    '            .Parameters("@gidMyTaskID").Value = strgidMyTaskID

    '            .Connection = MyConnection
    '        End With

    '        MyAdapter = New SqlDataAdapter(MyCommand)
    '        MyAdapter.Fill(MyData)


    '        If MyData.Tables(0).Rows.Count > 0 Then
    '            strMailTo = MyData.Tables(0).Rows(0)("nvchMailTo").ToString
    '            strMailCC = MyData.Tables(0).Rows(0)("nvchMailCC").ToString
    '            strMailBCC = MyData.Tables(0).Rows(0)("nvchMailBCC").ToString
    '            strSubject = MyData.Tables(0).Rows(0)("nvchMsgTitle").ToString
    '            strMessage = MyData.Tables(0).Rows(0)("nvchMsgContent").ToString
    '            strMailServerIP = MyData.Tables(0).Rows(0)("MailServerIP").ToString
    '            strMailContentHeader = MyData.Tables(0).Rows(0)("nvchMailContentHeader").ToString
    '            strMailContentFooter = MyData.Tables(0).Rows(0)("nvchMailContentFooter").ToString
    '            strMailTitle = MyData.Tables(0).Rows(0)("nvchMailTitle").ToString

    '        End If

    '        'If MyData.Tables(1).Rows.Count > 0 Then
    '        '    strFilePath = MyData.Tables(1).Rows(0)("FilePath").ToString()
    '        'End If


    '        Dim mail As New MailMessage()
    '        mail.To = strMailTo
    '        mail.Cc = strMailCC
    '        mail.Bcc = strMailBCC
    '        mail.From = """MyTask"" <MyTask@Dimerco.com>"
    '        If strSubject <> "" Then
    '            mail.Subject = strSubject
    '        Else
    '            mail.Subject = strMailTitle
    '        End If

    '        mail.BodyFormat = MailFormat.Html
    '        mail.Body = strMailContentHeader + strMessage + strMailContentFooter

    '        If MyData.Tables(1).Rows.Count > 0 Then
    '            For i = 0 To MyData.Tables(1).Rows.Count - 1
    '                strFilePath = MyData.Tables(1).Rows(i)("FilePath").ToString()
    '                If strFilePath <> "" Then
    '                    Dim attachment As New MailAttachment(strFilePath) 'create the attachment
    '                    mail.Attachments.Add(attachment) 'add the attachment
    '                End If
    '            Next
    '        End If

    '        SmtpMail.SmtpServer = strMailServerIP
    '        SmtpMail.Send(mail)

    '        Return strMsg

    '    Catch err As Exception
    '        strMsg = err.Message.ToString
    '        Return strMsg
    '    End Try

    'End Function

    'Public Function SendMail_MyTaskRecipient(ByVal intMailID As Integer, ByVal strgidMyTaskID As String) As String
    '    Dim strMsg As String = ""
    '    Dim i As Integer
    '    Dim strMailTo As String = ""
    '    Dim strMailCC As String = ""
    '    Dim strMailBCC As String = ""
    '    Dim strSubject As String = ""
    '    Dim strMessage As String = ""
    '    Dim strMailServerIP As String = ""
    '    Dim strFilePath As String = ""
    '    Dim strMailContentHeader As String = ""
    '    Dim strMailContentFooter As String = ""
    '    Dim strMailTitle As String = ""

    '    Try
    '        Dim MyData As New DataSet
    '        Dim MyAdapter As SqlDataAdapter
    '        Dim MyCommand As SqlCommand
    '        Dim MyConnection As New SqlConnection(strConnectionString)

    '        MyCommand = New SqlCommand("Proc_GetMailQueueData_MyTaskRecipient")

    '        With MyCommand
    '            .CommandType = CommandType.StoredProcedure
    '            .Parameters.Add("@biMailID", SqlDbType.BigInt)
    '            .Parameters("@biMailID").Value = intMailID
    '            .Parameters.Add("@gidMyTaskID", SqlDbType.NVarChar, 50)
    '            .Parameters("@gidMyTaskID").Value = strgidMyTaskID

    '            .Connection = MyConnection
    '        End With

    '        MyAdapter = New SqlDataAdapter(MyCommand)
    '        MyAdapter.Fill(MyData)


    '        If MyData.Tables(0).Rows.Count > 0 Then
    '            strMailTo = MyData.Tables(0).Rows(0)("nvchMailTo").ToString
    '            strMailCC = MyData.Tables(0).Rows(0)("nvchMailCC").ToString
    '            strMailBCC = MyData.Tables(0).Rows(0)("nvchMailBCC").ToString
    '            strSubject = MyData.Tables(0).Rows(0)("nvchMsgTitle").ToString
    '            strMessage = MyData.Tables(0).Rows(0)("nvchMsgContent").ToString
    '            strMailServerIP = MyData.Tables(0).Rows(0)("MailServerIP").ToString
    '            strMailContentHeader = MyData.Tables(0).Rows(0)("nvchMailContentHeader").ToString
    '            strMailContentFooter = MyData.Tables(0).Rows(0)("nvchMailContentFooter").ToString
    '            strMailTitle = MyData.Tables(0).Rows(0)("nvchMailTitle").ToString

    '        End If

    '        'If MyData.Tables(1).Rows.Count > 0 Then
    '        '    strFilePath = MyData.Tables(1).Rows(0)("FilePath").ToString()
    '        'End If


    '        Dim mail As New MailMessage()
    '        mail.To = strMailTo
    '        mail.Cc = strMailCC
    '        mail.Bcc = strMailBCC
    '        mail.From = """MyTask"" <MyTask@Dimerco.com>"
    '        If strSubject <> "" Then
    '            mail.Subject = strSubject
    '        Else
    '            mail.Subject = strMailTitle
    '        End If

    '        mail.BodyFormat = MailFormat.Html
    '        mail.Body = strMailContentHeader + strMessage + strMailContentFooter

    '        If MyData.Tables(1).Rows.Count > 0 Then
    '            For i = 0 To MyData.Tables(1).Rows.Count - 1
    '                strFilePath = MyData.Tables(1).Rows(i)("FilePath").ToString()
    '                If strFilePath <> "" Then
    '                    Dim attachment As New MailAttachment(strFilePath) 'create the attachment
    '                    mail.Attachments.Add(attachment) 'add the attachment
    '                End If
    '            Next
    '        End If

    '        SmtpMail.SmtpServer = strMailServerIP
    '        SmtpMail.Send(mail)

    '        Return strMsg

    '    Catch err As Exception
    '        strMsg = err.Message.ToString
    '        Return strMsg
    '    End Try

    'End Function

    Public Function UpdateMailQueue(ByVal intMailID As Integer, ByVal intSendStatus As Integer, ByVal strSendMailMsg As String, ByVal strCurrentDate As String) As String
        Dim strMsg As String = ""

        Try
            Dim MyData As New DataSet
            Dim MyAdapter As SqlDataAdapter
            Dim MyCommand As SqlCommand
            Dim MyConnection As New SqlConnection(strConnectionString)

            MyCommand = New SqlCommand("SP_UpdateMailQueue")

            With MyCommand
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@biMailID", SqlDbType.BigInt)
                .Parameters("@biMailID").Value = intMailID
                .Parameters.Add("@SendStatus", SqlDbType.TinyInt)
                .Parameters("@SendStatus").Value = intSendStatus
                .Parameters.Add("@SendMailMsg", SqlDbType.NVarChar, 500)
                .Parameters("@SendMailMsg").Value = strSendMailMsg
                .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                .Parameters("@CurrentDate").Value = strCurrentDate

                .Connection = MyConnection
            End With

            MyAdapter = New SqlDataAdapter(MyCommand)
            MyAdapter.Fill(MyData)

            Return strMsg

        Catch err As Exception
            strMsg = err.Message.ToString
            Return strMsg
        End Try

    End Function

    'Public Function SendMail_MyLog(ByVal intMailID As Integer, ByVal strgidMylogID As String) As String
    '    Dim strMsg As String = ""
    '    Dim i As Integer
    '    Dim strMailTo As String = ""
    '    Dim strMailCC As String = ""
    '    Dim strMailBCC As String = ""
    '    Dim strSubject As String = ""
    '    Dim strMessage As String = ""
    '    Dim strMailServerIP As String = ""
    '    Dim strFilePath As String = ""
    '    Dim strMailContentHeader As String = ""
    '    Dim strMailContentFooter As String = ""
    '    Dim strMailTitle As String = ""

    '    Try
    '        Dim MyData As New DataSet
    '        Dim MyAdapter As SqlDataAdapter
    '        Dim MyCommand As SqlCommand
    '        Dim MyConnection As New SqlConnection(strConnectionString)

    '        MyCommand = New SqlCommand("Proc_GetMailQueueData_MyLog")

    '        With MyCommand
    '            .CommandType = CommandType.StoredProcedure
    '            .Parameters.Add("@biMailID", SqlDbType.BigInt)
    '            .Parameters("@biMailID").Value = intMailID
    '            .Parameters.Add("@gidMyLogID", SqlDbType.NVarChar, 50)
    '            .Parameters("@gidMyLogID").Value = strgidMylogID

    '            .Connection = MyConnection
    '        End With

    '        MyAdapter = New SqlDataAdapter(MyCommand)
    '        MyAdapter.Fill(MyData)


    '        If MyData.Tables(0).Rows.Count > 0 Then
    '            strMailTo = MyData.Tables(0).Rows(0)("nvchMailTo").ToString
    '            strMailCC = MyData.Tables(0).Rows(0)("nvchMailCC").ToString
    '            strMailBCC = MyData.Tables(0).Rows(0)("nvchMailBCC").ToString
    '            strSubject = MyData.Tables(0).Rows(0)("nvchMsgTitle").ToString
    '            strMessage = MyData.Tables(0).Rows(0)("nvchMsgContent").ToString
    '            strMailServerIP = MyData.Tables(0).Rows(0)("MailServerIP").ToString
    '            strMailContentHeader = MyData.Tables(0).Rows(0)("nvchMailContentHeader").ToString
    '            strMailContentFooter = MyData.Tables(0).Rows(0)("nvchMailContentFooter").ToString
    '            strMailTitle = MyData.Tables(0).Rows(0)("nvchMailTitle").ToString

    '        End If

    '        If MyData.Tables(1).Rows.Count > 0 Then
    '            strFilePath = MyData.Tables(1).Rows(0)("FilePath")
    '        End If


    '        Dim mail As New MailMessage()
    '        mail.To = strMailTo
    '        mail.Cc = strMailCC
    '        mail.Bcc = strMailBCC
    '        mail.From = """MyLog"" <MyLog@Dimerco.com>"
    '        If strSubject <> "" Then
    '            mail.Subject = strSubject
    '        Else
    '            mail.Subject = strMailTitle
    '        End If

    '        mail.BodyFormat = MailFormat.Html
    '        mail.Body = strMailContentHeader + strMessage + strMailContentFooter

    '        If MyData.Tables(1).Rows.Count > 0 Then
    '            For i = 0 To MyData.Tables(1).Rows.Count - 1
    '                strFilePath = MyData.Tables(1).Rows(i)("FilePath")
    '                Dim attachment As New MailAttachment(strFilePath) 'create the attachment
    '                mail.Attachments.Add(attachment) 'add the attachment
    '            Next
    '        End If

    '        SmtpMail.SmtpServer = strMailServerIP
    '        SmtpMail.Send(mail)

    '        Return strMsg

    '    Catch err As Exception
    '        strMsg = err.Message.ToString
    '        Return strMsg
    '    End Try

    'End Function

    Public Function ColumnLetter(ByVal iColCount As Integer) As String
        If iColCount > 26 Then

            ' 1st character:  Subtract 1 to map the characters to 0-25,
            '                 but you don't have to remap back to 1-26
            '                 after the 'Int' operation since columns
            '                 1-26 have no prefix letter

            ' 2nd character:  Subtract 1 to map the characters to 0-25,
            '                 but then must remap back to 1-26 after
            '                 the 'Mod' operation by adding 1 back in
            '                 (included in the '65')

            ColumnLetter = Chr(Int((iColCount - 1) / 26) + 64) & _
                           Chr(((iColCount - 1) Mod 26) + 65)
        Else
            ' Columns A-Z
            ColumnLetter = Chr(iColCount + 64)
        End If
    End Function

    Public Function RenameFileName(ByVal strFileName As String) As String

        strFileName = strFileName.Replace(" ", "_")
        strFileName = strFileName.Replace("&", "_")
        strFileName = strFileName.Replace("'", "_")
        strFileName = strFileName.Replace("-", "_")

        RenameFileName = strFileName
    End Function

    Public Function GetCurrency() As DataView
        Dim dsCurrency As New DataSet

        dsCurrency = DIMERCO.SDK.Utilities.ReSM.GetCurrency()
        Dim newRow As DataRow = dsCurrency.Tables(0).NewRow()
        newRow("HQID") = "0"
        newRow("CurrencyCode") = ""

        dsCurrency.Tables(0).Rows.Add(newRow)

        Dim dtView As DataView = dsCurrency.Tables(0).DefaultView
        dtView.Sort = "CurrencyCode ASC"

        Return dtView
    End Function

    Public Sub Bind_dllStation(ByVal strIncludeStationList As String, ByVal ddlStation As DropDownList)

        Dim dsStation As DataSet
        If strIncludeStationList <> "" Then
            dsStation = DIMERCO.SDK.Utilities.LSDK.getStationDataByWithList(strIncludeStationList)
        Else
            dsStation = DIMERCO.SDK.Utilities.ReSM.GetStationList()
        End If
        Dim dtView As DataView = dsStation.Tables(0).DefaultView
        dtView.Sort = "StationCode ASC"

        ddlStation.DataSource = dtView
        ddlStation.DataValueField = dtView.Table.Columns("StationID").ToString
        ddlStation.DataTextField = dtView.Table.Columns("StationCode").ToString
        ddlStation.DataBind()

    End Sub

    Public Function ShowApplyTo(ByVal strApplyList As String, ByVal strType As String, ByRef txtTo As TextBox, ByRef gvCC As GridView) As String
        Dim strErrorMsg As String = ""
        Try
            Dim MyData As New DataSet

            If strType = "To" Then
                MyData = DIMERCO.SDK.Utilities.LSDK.getUserDataByList(strApplyList)
                txtTo.Text = MyData.Tables(0).Rows(0)("Fullname").ToString
            ElseIf strType = "CC" Then
                MyData = DIMERCO.SDK.Utilities.LSDK.getUserDataByList(strApplyList)
                gvCC.DataSource = MyData
                gvCC.DataMember = MyData.Tables(0).TableName
                gvCC.DataBind()
                'ElseIf strType = "Owner" Then
                '    txtTextBox.Text = ""
                '    MyData = DIMERCO.SDK.Utilities.LSDK.getUserDataByList(strApplyList)
                '    txtTextBox.Text = MyData.Tables(0).Rows(0)("Fullname").ToString
                'ElseIf strType = "LocationApprovedBy" Then
                '    lblLocationApprovedBy.Text = ""
                '    MyData = DIMERCO.SDK.Utilities.LSDK.getUserDataByList(strApplyList)
                '    For i = 0 To MyData.Tables(0).Rows.Count - 1
                '        If lblLocationApprovedBy.Text.ToString = "" Then
                '            lblLocationApprovedBy.Text = MyData.Tables(0).Rows(i)("Fullname").ToString()
                '        Else
                '            lblLocationApprovedBy.Text = lblLocationApprovedBy.Text.ToString + ", " + MyData.Tables(0).Rows(i)("Fullname").ToString
                '        End If
                '    Next
                'ElseIf strType = "CCParty" Then
                '    lblCCParty.Text = ""
                '    MyData = DIMERCO.SDK.Utilities.LSDK.getUserDataByList(strApplyList)

                '    hdfCCParty.Value = strApplyList
                '    For i = 0 To MyData.Tables(0).Rows.Count - 1
                '        If lblCCParty.Text.ToString = "" Then
                '            lblCCParty.Text = MyData.Tables(0).Rows(i)("Fullname").ToString()
                '        Else
                '            lblCCParty.Text = lblCCParty.Text.ToString + ", " + MyData.Tables(0).Rows(i)("Fullname").ToString
                '        End If
                '    Next
            End If


            Return strErrorMsg

        Catch ex As Exception
            strErrorMsg = "ShowApplyTo: " & ex.Message.ToString
            Return strErrorMsg
        End Try
    End Function

    Public Function SplitString(ByVal strString As String, ByVal strStartString As String, ByVal strEndString As String) As String

        Dim startIndex As Integer = 0
        Dim endIndex As Integer = 0

        SplitString = ""

        startIndex = strString.IndexOf(strStartString)
        endIndex = strString.IndexOf(strEndString, startIndex)
        If endIndex - startIndex - strEndString.Length <= 0 Then
            SplitString = ""
        Else
            SplitString = strString.Substring(startIndex + strStartString.Length, endIndex - startIndex + 1 - strEndString.Length).Trim
        End If

        Return SplitString

    End Function

    Public Function GetCOAList(ByVal strCOAType As String) As DataView
        Dim dsCOA As New DataSet
        Dim i As Integer
        dsCOA = DIMERCO.SDK.Utilities.LSDK.getCOAList(strCOAType)
        dsCOA.Tables(0).Columns.Add("COADesc")
        For i = 0 To dsCOA.Tables(0).Rows.Count - 1
            dsCOA.Tables(0).Rows(i)("COADesc") = dsCOA.Tables(0).Rows(i)("COA") + "-" + dsCOA.Tables(0).Rows(i)("COADescription")
        Next

        Dim newRow As DataRow = dsCOA.Tables(0).NewRow()
        newRow("COA") = ""
        newRow("COADescription") = ""
        newRow("COADesc") = ""
        dsCOA.Tables(0).Rows.Add(newRow)


        Dim dtView As DataView
        dtView = New DataView(dsCOA.Tables(0), "COA NOT lIKE '%0000'", "COA", DataViewRowState.CurrentRows)

        'Dim dtView As DataView = dsCOA.Tables(0).DefaultView
        dtView.Sort = "COA ASC"
        Return dtView

    End Function

    Public Function DisableTheButton(ByVal pge As Control, ByVal btn As Control, ByVal msg As String, ByVal disableOthbtn1 As String, ByVal disableOthbtn2 As String, ByVal disableOthbtn3 As String, ByVal disableOthbtn4 As String, ByVal disableOthbtn5 As String) As String
        Dim sb As New System.Text.StringBuilder()
        'sb.Append("if (typeof(Page_ClientValidate) == 'function') {")
        'sb.Append("if (Page_ClientValidate() == false) { return false; }} ")

        If msg <> "" Then
            sb.Append("if (confirm('" + msg + "?') == false) { return false; } ")

        End If


        If btn.GetType().Name.ToString = "Button" Then
            sb.Append("this.value = 'Please wait...';")
        ElseIf btn.GetType().Name.ToString = "LinkButton" Then
            sb.Append("this.innerHTML = 'Please wait..';")
        Else
            sb.Append("this.innerHTML = 'Please wait..';")
        End If

        sb.Append("this.disabled = true;")
        If disableOthbtn1 <> "" Then
            sb.Append("document.getElementById('" + disableOthbtn1 + "').disabled=true;")
        End If
        If disableOthbtn2 <> "" Then
            sb.Append("document.getElementById('" + disableOthbtn2 + "').disabled=true;")
        End If
        If disableOthbtn3 <> "" Then
            sb.Append("document.getElementById('" + disableOthbtn3 + "').disabled=true;")
        End If
        If disableOthbtn4 <> "" Then
            sb.Append("document.getElementById('" + disableOthbtn4 + "').disabled=true;")
        End If
        If disableOthbtn5 <> "" Then
            sb.Append("document.getElementById('" + disableOthbtn5 + "').disabled=true;")
        End If

        'sb.Append(ClientScript.GetPostBackEventReference(btn, btn.ID.ToString))
        sb.Append(pge.Page.GetPostBackEventReference(btn))
        sb.Append(";")
        Return sb.ToString()
    End Function

    Public Function GetOwnerInformation(ByVal strUser As String) As DataSet
        Dim dsOwnerInfo As New DataSet
        Dim c As New com.dimerco.home.LeaveApplication
        dsOwnerInfo = c.GetUserInformation(strRightCode, strUser)

        Return dsOwnerInfo
    End Function

    Public Function GeteRequistionJobFunction() As DataView
        Dim dsJobFunction As New DataSet
        Dim c As New com.dimerco.home.LeaveApplication
        dsJobFunction = c.GeteRequistionJobFunction(strRightCode)

        Dim newRow As DataRow = dsJobFunction.Tables(0).NewRow()
        newRow("Job_Function") = ""

        dsJobFunction.Tables(0).Rows.Add(newRow)

        Dim dtView As DataView = dsJobFunction.Tables(0).DefaultView
        dtView.Sort = "Job_Function ASC"

        Return dtView

    End Function

    Public Function GeteRequistionWorkStationType(ByVal UserID As String, ByVal JobFunction As String, ByVal isAudit As Boolean, ByVal isSeedMember As Boolean) As DataSet
        Dim dsWorkStationType As New DataSet
        Dim c As New com.dimerco.home.LeaveApplication
        dsWorkStationType = c.GeteRequistionWorkStationType(strRightCode, UserID, JobFunction, isAudit, isSeedMember)
        Return dsWorkStationType

    End Function

    Public Function GetStationLocation(ByVal strStation As String) As DataSet
        Dim dsLocation As New DataSet
        Dim MyAdapter As SqlDataAdapter
        Dim MyCommand As SqlCommand
        Dim MyConnection As New SqlConnection(strConnectionString)

        MyCommand = New SqlCommand("SP_GetReferenceData_ByStation")

        With MyCommand
            .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
            .Parameters("@ReSM").Value = strReSM
            .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
            .Parameters("@ReferenceType").Value = "[Location]"
            .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
            .Parameters("@StationID").Value = strStation
            .CommandType = CommandType.StoredProcedure
            .Connection = MyConnection
        End With

        MyAdapter = New SqlDataAdapter(MyCommand)
        MyAdapter.Fill(dsLocation)
        dsLocation.Tables(0).TableName = "Location"

        Return dsLocation

    End Function

    'Public Function SendeRequisition(ByVal strAction As String, ByVal streRequisitionNo As String, ByVal strCondition As String _
    '           , ByVal strOwnerUserID As String, ByVal blnInternal_Audit As Boolean, ByVal strJobFunction As String _
    '           , ByVal blnMIS_Seed_Member As Boolean, ByVal strParentCategory As String, ByVal strCategory As String _
    '           , ByVal strAppToBeUsed As String, ByVal strVendorDetail As String _
    '           , ByVal strBrand As String, ByVal strModel As String, ByVal intAvailInInventory As Integer, ByVal strRequiredDate As String _
    '           , ByVal intQuantity As Integer, ByVal strCurrencyCode As String, ByVal strCurrencyRate As String _
    '           , ByVal strPurchaseBudgeted As String, ByVal strPurchaseCost As String, ByVal strRemarks As String, ByVal strLocationApprovedBy As String _
    '           , ByVal strCCParty As String, ByVal strCreatedBy As String, ByVal gvSpec As GridView, ByVal strUserID As String _
    '           , ByVal strCurrentDateTime As String, ByVal strAssetID As String) As String


    '    Dim strEReqNo As String = ""
    '    'Dim streRequisitionNo As String = ""
    '    Dim strEReq_ErrMessage As String = ""
    '    Dim sqlConn As New SqlConnection(strConnectionString)
    '    Dim strMsg As String = ""
    '    Dim strReturnMsg As String = ""

    '    Try
    '        'eRequisition Header
    '        Dim entity As New eAsset.EntityRequisition
    '        entity = New eAsset.EntityRequisition()
    '        entity.ControlID = streRequisitionNo
    '        entity.Action = strAction
    '        entity.Condition = strCondition

    '        entity.UserID = strOwnerUserID
    '        entity.Internal_Audit = blnInternal_Audit
    '        entity.JobFunction = strJobFunction
    '        entity.MIS_Seed_Member = blnMIS_Seed_Member
    '        entity.Category = strParentCategory
    '        entity.Type = strCategory
    '        entity.Applications_to_be_Used = strAppToBeUsed
    '        entity.Vendor = strVendorDetail
    '        entity.Brand_Name = strBrand
    '        entity.Module = strModel
    '        entity.Availability_in_Inventory = intAvailInInventory
    '        entity.Require_Date = Convert.ToDateTime(strRequiredDate)
    '        entity.Number_of_Units = intQuantity
    '        entity.Currency = strCurrencyCode
    '        entity.Rate = CDec(strCurrencyRate)
    '        entity.Budget = CDec(strPurchaseBudgeted)
    '        entity.Quote = CDec(strPurchaseCost)
    '        entity.Remark = strRemarks
    '        entity.LocalApprovedBy = strLocationApprovedBy
    '        entity.eMailCCParty = Replace(strCCParty, ",", ";")
    '        entity.CreatedBy = strCreatedBy


    '        '' ''eRequisition Item Detail (Specification)

    '        Dim items As New System.Collections.Generic.List(Of eAsset.EntityItem)
    '        Dim i As Integer
    '        Dim lblSpec As New Label
    '        Dim lblQty As New Label
    '        Dim lbtnRemove As New LinkButton

    '        For i = 0 To gvSpec.Rows.Count - 1
    '            lblSpec = gvSpec.Rows(i).FindControl("lblSpec")
    '            lblQty = gvSpec.Rows(i).FindControl("lblQty")
    '            lbtnRemove = gvSpec.Rows(i).FindControl("lbtnRemove")

    '            Dim entityItem As New eAsset.EntityItem

    '            entityItem.CategoryCode = strCategory
    '            entityItem.ItemLineNo = lbtnRemove.CommandArgument.ToString
    '            entityItem.ItemDesc = lblSpec.Text.ToString
    '            entityItem.ItemQty = CInt(lblQty.Text.ToString)
    '            items.Add(entityItem)
    '        Next

    '        'eRequisition Return
    '        Dim c As New eAsset.LeaveApplication
    '        Dim ret As New eAsset.eRequisitionReturn
    '        ret = c.Send2eRequisition(strRightCode, entity, items.ToArray())

    '        '// ret.Result is a token of return value, and need saved in your DB, so that you can do the delete function before eRequisition Approved. 
    '        If ret.Result.ToString <> "" Then
    '            strEReqNo = ret.Result.ToString
    '        End If

    '        If ret.Message.ToString <> "" Then
    '            'strEReqNo = ""
    '            strEReq_ErrMessage = ret.Message.ToString
    '            If strEReq_ErrMessage.Contains("Delele Successfully.") = False Then
    '                Return strReturnMsg = strEReq_ErrMessage
    '                Exit Function
    '            End If
    '        End If


    '        Dim strMailID As String = ""
    '        Dim strResult As String = ""
    '        'UPDATE DATA AFTER SEND EREQ SUCCESSFUL
    '        sqlConn.Open()
    '        Dim cmd As New SqlCommand("SP_UpdateSendeReq", sqlConn)
    '        With cmd
    '            .CommandType = CommandType.StoredProcedure
    '            .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
    '            .Parameters("@UserID").Value = strUserID
    '            .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
    '            .Parameters("@CurrentDate").Value = strCurrentDateTime

    '            .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
    '            .Parameters("@AssetID").Value = strAssetID
    '            .Parameters.Add("@eRequisitionNo", SqlDbType.VarChar, 20)
    '            If strAction = "Delete" Then
    '                .Parameters("@eRequisitionNo").Value = streRequisitionNo
    '            Else
    '                .Parameters("@eRequisitionNo").Value = strEReqNo
    '            End If

    '            .Parameters.Add("@Action", SqlDbType.NVarChar, 50)
    '            .Parameters("@Action").Value = strAction

    '            .Parameters.Add("@eReq_Status", SqlDbType.VarChar, 1)
    '            If strAction = "Delete" Then
    '                .Parameters("@eReq_Status").Value = "C"
    '            Else
    '                .Parameters("@eReq_Status").Value = "P"
    '            End If

    '            .Parameters.Add("@eReq_SendStatusMsg", SqlDbType.NVarChar)
    '            .Parameters("@eReq_SendStatusMsg").Value = strEReq_ErrMessage

    '            .Parameters.Add("@biMailID", SqlDbType.NVarChar, 500)
    '            .Parameters("@biMailID").Direction = ParameterDirection.Output

    '            .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
    '            .Parameters("@Msg").Direction = ParameterDirection.Output

    '            .CommandTimeout = 0
    '            .ExecuteNonQuery()
    '            strMailID = .Parameters("@biMailID").Value.ToString()
    '            strMsg = .Parameters("@Msg").Value.ToString()
    '        End With

    '        If strMsg <> "" Then
    '            strReturnMsg = strMsg
    '        Else
    '            strReturnMsg = ""
    '            If strMailID <> "" Then
    '                Dim MailID As String() = strMailID.Split(New Char() {","c})
    '                Dim intMailID As Integer
    '                For Each intMailID In MailID
    '                    If intMailID <> 0 Then
    '                        strResult = SendMail(intMailID)
    '                        If strResult = "" Then
    '                            If strAction = "Delete" Then
    '                                UpdateMailQueue(intMailID, 1, "Cancel eRequisition Successful", strCurrentDateTime)
    '                            Else
    '                                UpdateMailQueue(intMailID, 1, "Send eRequisition Successful", strCurrentDateTime)
    '                            End If
    '                        Else
    '                            UpdateMailQueue(intMailID, 2, "Send Fail - " & strResult, strCurrentDateTime)
    '                        End If
    '                    End If
    '                Next
    '            End If
    '        End If

    '        Return strReturnMsg

    '    Catch ex As Exception
    '        Return ex.Message.ToString
    '        Exit Function
    '    Finally
    '        sqlConn.Close()
    '    End Try

    'End Function

    Public Function SendeRequisition(ByVal strAssetID As String, ByVal strAction As String, ByVal strUserID As String, ByVal strCurrentDateTime As String) As String
        Dim strEReqNo As String = ""
        Dim strEReq_ErrMessage As String = ""
        Dim sqlConn As New SqlConnection(strConnectionString)
        Dim strMsg As String = ""
        Dim strReturnMsg As String = ""

        Dim MyData As New DataSet
        Dim MyAdapter As SqlDataAdapter
        Dim MyCommand As SqlCommand
        Dim MyConnection As New SqlConnection(strConnectionString)

        Try
            MyCommand = New SqlCommand("SP_GetAssetDetailToSendeReq")

            With MyCommand
                .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                .Parameters("@AssetID").Value = strAssetID
                .CommandType = CommandType.StoredProcedure
                .Connection = MyConnection
            End With

            MyAdapter = New SqlDataAdapter(MyCommand)
            MyAdapter.Fill(MyData)
            MyData.Tables(0).TableName = "AssetData"
            MyData.Tables(1).TableName = "Specification"

            ''**** Disable eRequisition - 04 Sep 2020
            ''eRequisition Header

            'Dim entity As New com.dimerco.home.EntityRequisition
            'entity = New com.dimerco.home.EntityRequisition()
            'entity.ApplyStationCode = MyData.Tables(0).Rows(0)("StationCode").ToString
            'entity.ControlID = MyData.Tables(0).Rows(0)("eRequisitionNo").ToString
            'entity.Action = strAction
            'entity.Condition = MyData.Tables(0).Rows(0)("Condition").ToString

            'entity.UserID = MyData.Tables(0).Rows(0)("OwnerID").ToString
            'If MyData.Tables(0).Rows(0)("Internal_Audit").ToString = "1" Then
            '    entity.Internal_Audit = True
            'Else
            '    entity.Internal_Audit = False
            'End If

            'entity.JobFunction = MyData.Tables(0).Rows(0)("Job_Function").ToString
            'If MyData.Tables(0).Rows(0)("MIS_Seed_Member").ToString = "1" Then
            '    entity.MIS_Seed_Member = True
            'Else
            '    entity.MIS_Seed_Member = False
            'End If

            'entity.Category = MyData.Tables(0).Rows(0)("ParentCategory").ToString
            'entity.Type = MyData.Tables(0).Rows(0)("Category").ToString
            'entity.Applications_to_be_Used = MyData.Tables(0).Rows(0)("Application_ToBeUsed").ToString
            'entity.Vendor = MyData.Tables(0).Rows(0)("VendorPayeeDetail").ToString
            'entity.Brand_Name = MyData.Tables(0).Rows(0)("BRANDNAME").ToString
            'entity.Module = MyData.Tables(0).Rows(0)("Model").ToString
            'entity.Availability_in_Inventory = CInt(MyData.Tables(0).Rows(0)("Availability_In_Inventory").ToString)
            'entity.Require_Date = Convert.ToDateTime(MyData.Tables(0).Rows(0)("RequiredDate").ToString)
            'entity.Number_of_Units = CInt(MyData.Tables(0).Rows(0)("No_Of_Units").ToString)
            'entity.Currency = MyData.Tables(0).Rows(0)("PurchaseCurrencyCode").ToString
            'entity.Rate = CDec(MyData.Tables(0).Rows(0)("PurchaseCurrencyRate").ToString)
            'entity.Budget = CDec(MyData.Tables(0).Rows(0)("BudgetAmount").ToString)
            'entity.Quote = CDec(MyData.Tables(0).Rows(0)("PurchaseCost").ToString)
            'entity.Remark = MyData.Tables(0).Rows(0)("Remarks").ToString
            'entity.LocalApprovedBy = MyData.Tables(0).Rows(0)("LocationApprovedBy").ToString
            'entity.eMailCCParty = Replace(MyData.Tables(0).Rows(0)("eMailCCParty").ToString, ",", ";")
            'entity.CreatedBy = MyData.Tables(0).Rows(0)("CreatedBy").ToString
            'entity.SpecialCategory = MyData.Tables(0).Rows(0)("ActualCategory").ToString

            ' '' ''eRequisition Item Detail (Specification)
            'Dim items As New System.Collections.Generic.List(Of com.dimerco.home.EntityItem)
            'Dim i As Integer
            'Dim strSpec As String = ""
            'Dim intQty As Integer = 0
            'Dim strSpecID As String = ""

            'For i = 0 To MyData.Tables(1).Rows.Count - 1
            '    strSpec = MyData.Tables(1).Rows(i)("Specification").ToString
            '    intQty = CInt(MyData.Tables(1).Rows(i)("Quantity").ToString)
            '    strSpecID = MyData.Tables(1).Rows(i)("SpecificationID").ToString

            '    Dim entityItem As New com.dimerco.home.EntityItem

            '    entityItem.CategoryCode = MyData.Tables(0).Rows(0)("Category").ToString
            '    entityItem.ItemLineNo = strSpecID
            '    entityItem.ItemDesc = strSpec
            '    entityItem.ItemQty = intQty
            '    items.Add(entityItem)
            'Next

            ''eRequisition Return
            'Dim c As New com.dimerco.home.LeaveApplication
            'Dim ret As New com.dimerco.home.eRequisitionReturn
            'ret = c.Send2eRequisition(strRightCode, entity, items.ToArray())

            ''// ret.Result is a token of return value, and need saved in your DB, so that you can do the delete function before eRequisition Approved. 
            'If ret.Result.ToString <> "" Then
            '    strEReqNo = ret.Result.ToString
            'End If

            'If ret.Message.ToString <> "" Then
            '    'strEReqNo = ""
            '    strEReq_ErrMessage = ret.Message.ToString
            '    If strEReq_ErrMessage.Contains("Delele Successfully.") = False Then
            '        Return strReturnMsg = strEReq_ErrMessage
            '        Exit Function
            '    End If
            'End If
            ''**** Disable eRequisition - 04 Sep 2020


            Dim strMailID As String = ""
            Dim strResult As String = ""
            'UPDATE DATA AFTER SEND EREQ SUCCESSFUL
            sqlConn.Open()
            Dim cmd As New SqlCommand("SP_UpdateSendeReq", sqlConn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                .Parameters("@UserID").Value = strUserID
                .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                If strCurrentDateTime <> "" Then
                    .Parameters("@CurrentDate").Value = strCurrentDateTime
                Else
                    .Parameters("@CurrentDate").Value = MyData.Tables(0).Rows(0)("CreatedOn").ToString
                End If


                .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                .Parameters("@AssetID").Value = strAssetID
                .Parameters.Add("@eRequisitionNo", SqlDbType.VarChar, 20)
                .Parameters("@eRequisitionNo").Value = MyData.Tables(0).Rows(0)("eRequisitionNo").ToString

                'If strAction = "Delete" Then
                '    .Parameters("@eRequisitionNo").Value = MyData.Tables(0).Rows(0)("eRequisitionNo").ToString
                'Else
                '    .Parameters("@eRequisitionNo").Value = MyData.Tables(0).Rows(0)("eRequisitionNo").ToString
                'End If

                .Parameters.Add("@Action", SqlDbType.NVarChar, 50)
                .Parameters("@Action").Value = strAction

                .Parameters.Add("@eReq_Status", SqlDbType.VarChar, 1)
                If strAction = "Delete" Then
                    .Parameters("@eReq_Status").Value = "C"
                Else
                    .Parameters("@eReq_Status").Value = "P"
                End If

                .Parameters.Add("@eReq_SendStatusMsg", SqlDbType.NVarChar)
                .Parameters("@eReq_SendStatusMsg").Value = "Send Successfully." ''strEReq_ErrMessage

                .Parameters.Add("@biMailID", SqlDbType.NVarChar, 500)
                .Parameters("@biMailID").Direction = ParameterDirection.Output

                .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                .Parameters("@Msg").Direction = ParameterDirection.Output

                .CommandTimeout = 0
                .ExecuteNonQuery()
                strMailID = .Parameters("@biMailID").Value.ToString()
                strMsg = .Parameters("@Msg").Value.ToString()
            End With

            If strMsg <> "" Then
                strReturnMsg = strMsg
            Else
                strReturnMsg = ""
                If strMailID <> "" Then
                    Dim MailID As String() = strMailID.Split(New Char() {","c})
                    Dim intMailID As Integer
                    For Each intMailID In MailID
                        If intMailID <> 0 Then
                            strResult = SendMail(intMailID)
                            If strResult = "" Then
                                If strAction = "Delete" Then
                                    UpdateMailQueue(intMailID, 1, "Cancel eRequisition Successful", strCurrentDateTime)
                                Else
                                    UpdateMailQueue(intMailID, 1, "Send eRequisition Successful", strCurrentDateTime)
                                End If
                            Else
                                UpdateMailQueue(intMailID, 2, "Send Fail - " & strResult, strCurrentDateTime)
                            End If
                        End If
                    Next
                End If
            End If

            Return strReturnMsg

        Catch ex As Exception
            Return ex.Message.ToString
            Exit Function
        Finally
            sqlConn.Close()
        End Try
    End Function

    Public Function Update_eReqACK(ByVal strAssetID As String, ByVal streRequisitionNo As String) As String
        Dim strResult As String = ""
        Dim strErrMessage As String = ""
        Dim strApprovedBy As String = ""
        Dim strApprovedOn As String = ""
        Dim strApprovalRemarks As String = ""
        Dim strReturnMsg As String = ""
        Dim IntMailID As Integer = 0
        Dim strMailID As String = ""
        Dim strMsg As String = ""
        Dim sqlConn As New SqlConnection(strConnectionString)

        Try
            'eRequisition Return
            Dim c As New com.dimerco.home.LeaveApplication
            Dim ret As New com.dimerco.home.eRequisitionReturn
            ret = c.CheckeRequisitionStatus(strRightCode, streRequisitionNo)


            If ret.Result.ToString <> "" Then
                strResult = ret.Result.ToString
            End If

            If ret.Message.ToString <> "" Then
                strErrMessage = ret.Message.ToString
                strApprovedBy = SplitString(strErrMessage, "<By>", "</By>")
                strApprovedOn = SplitString(strErrMessage, "<On>", "</On>")
                strApprovalRemarks = SplitString(strErrMessage, "<Remark>", "</Remark>")
            End If

            Dim streReq_Status As String = ""
            If strResult = "Approved" Then
                streReq_Status = "A"
            ElseIf strResult = "Reject" Then
                streReq_Status = "R"
            ElseIf strResult = "Modify" Then
                streReq_Status = "W"
            ElseIf strResult = "Pending" Then
                streReq_Status = "P"
            End If

            ''TESTING
            'streReq_Status = "A"
            ''TESTING

            If streReq_Status <> "P" Then
                sqlConn.Open()
                Dim cmd As New SqlCommand("SP_UpdateeReqACK", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@eRequisitionNo", SqlDbType.VarChar, 50)
                    .Parameters("@eRequisitionNo").Value = streRequisitionNo
                    .Parameters.Add("@eReq_Status", SqlDbType.VarChar, 1)
                    .Parameters("@eReq_Status").Value = streReq_Status

                    .Parameters.Add("@eReq_ApprovedBy", SqlDbType.VarChar, 6)
                    .Parameters("@eReq_ApprovedBy").Value = strApprovedBy

                    .Parameters.Add("@eReq_ApprovedOn", SqlDbType.NVarChar, 50)
                    .Parameters("@eReq_ApprovedOn").Value = strApprovedOn

                    .Parameters.Add("@eReq_Comments", SqlDbType.NVarChar)
                    .Parameters("@eReq_Comments").Value = strApprovalRemarks

                    .Parameters.Add("@biMailID", SqlDbType.NVarChar, 500)
                    .Parameters("@biMailID").Direction = ParameterDirection.Output
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output

                    .CommandTimeout = 0
                    .ExecuteNonQuery()
                    strMailID = .Parameters("@biMailID").Value.ToString()
                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    strReturnMsg = "Update_eReqACK :" & strMsg
                    'UpdateSendStatus(strAssetID, "Update_eReqACK:" & strMsg)
                Else
                    strReturnMsg = ""
                    'UpdateSendStatus(strAssetID, "Update eReqACK Successful")

                    If strMailID <> "" Then
                        Dim MailID As String() = strMailID.Split(New Char() {","c})
                        For Each IntMailID In MailID
                            If IntMailID <> 0 Then
                                strResult = SendMail(IntMailID)
                                If strResult = "" Then
                                    UpdateMailQueue(IntMailID, 1, "Send Successful", Now.Date.ToString)
                                Else
                                    UpdateMailQueue(IntMailID, 2, "Send Fail - " & strResult, Now.Date.ToString)
                                End If
                            End If
                        Next
                    End If
                End If
            End If


            Return strReturnMsg

        Catch ex As Exception
            Return ex.Message.ToString
            'UpdateSendStatus(strAssetID, "Update_eReqACK: " & ex.Message.ToString)
        Finally
            sqlConn.Close()
        End Try
    End Function

    Public Function GetCheckIs_InChargeParty(ByVal strUserID As String, ByVal strStationID As String) As DataSet
        Dim MyData As New DataSet
        Dim MyAdapter As SqlDataAdapter
        Dim MyCommand As SqlCommand
        Dim MyConnection As New SqlConnection(strConnectionString)

        Try
            MyCommand = New SqlCommand("SP_GetCheckIs_InChargeParty")

            With MyCommand

                .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                .Parameters("@UserID").Value = strUserID
                .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                .Parameters("@StationID").Value = strStationID
                .CommandType = CommandType.StoredProcedure
                .Connection = MyConnection
            End With

            MyAdapter = New SqlDataAdapter(MyCommand)
            MyAdapter.Fill(MyData)
            MyData.Tables(0).TableName = "IS_InChargeParty"

            

            Return MyData

        Catch ex As Exception
            Return MyData
            'Me.lblMsg.Text = "GetCheckIs_InChargeParty: " & ex.Message.ToString
        Finally
            'sqlConn.Close()
        End Try
    End Function

    Public Sub StationCurrencyID(ByVal strStationID As String, ByVal ddlCurrency As DropDownList)
        Dim strStationCurrency As String
        strStationCurrency = DIMERCO.SDK.Utilities.ReSM.GetStationCurrencyByID(strStationID).ToString()
        ddlCurrency.SelectedValue = DIMERCO.SDK.Utilities.ReSM.GetCurrencyID(strStationCurrency).ToString
        'Return StationCurrencyID

    End Sub

    Public Sub CheckConnectionState(ByVal strConnectionString As SqlConnection)
        If strConnectionString.State <> ConnectionState.Open Then
            strConnectionString.Open()
        End If
    End Sub

    Public Sub CloseConnection(ByVal strConnectionString As SqlConnection)
        If strConnectionString.State <> ConnectionState.Closed Then
            strConnectionString.Close()
            strConnectionString.Dispose()
            'SqlConnection.ClearAllPools()
            'sqlConn.Close()
        End If
    End Sub

    Public Function CancelRequisition(ByVal strAssetID As String, ByVal streRequisitionNo As String, ByVal strUserID As String, ByVal strCurrentDateTime As String) As String
        Dim strEReqNo As String = ""
        Dim strEReq_ErrMessage As String = ""
        Dim sqlConn As New SqlConnection(strConnectionString)
        Dim strMsg As String = ""
        Dim strReturnMsg As String = ""

        Try
            ''eRequisition Return
            'Dim c As New com.dimerco.home.LeaveApplication
            'Dim ret As New com.dimerco.home.eRequisitionReturn
            'ret = c.CanceleRequisition(strRightCode, streRequisitionNo, strUserID)

            'If ret.Result.ToUpper() = "TRUE" Then
            '    strEReq_ErrMessage = "Cancel Successfully."
            'ElseIf ret.Result.ToUpper() = "FALSE" Then
            '    strEReq_ErrMessage = "Cancel Fail."
            '    Return strReturnMsg = strEReq_ErrMessage
            '    Exit Function
            'End If

            ''If ret.Message.ToString <> "" Then
            ''    strEReq_ErrMessage = ret.Message.ToString
            ''    If strEReq_ErrMessage.Contains("Cancel Successfully.") = False Then
            ''        Return strReturnMsg = strEReq_ErrMessage
            ''        Exit Function
            ''    End If
            ''End If


            Dim strMailID As String = ""
            Dim strResult As String = ""
            'UPDATE DATA AFTER SEND EREQ SUCCESSFUL
            sqlConn.Open()
            Dim cmd As New SqlCommand("SP_UpdateCanceleReq", sqlConn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                .Parameters("@UserID").Value = strUserID
                .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                .Parameters("@CurrentDate").Value = strCurrentDateTime
                .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                .Parameters("@AssetID").Value = strAssetID
                .Parameters.Add("@eRequisitionNo", SqlDbType.VarChar, 20)
                .Parameters("@eRequisitionNo").Value = streRequisitionNo
                .Parameters.Add("@eReq_SendStatusMsg", SqlDbType.NVarChar)
                .Parameters("@eReq_SendStatusMsg").Value = "Cancel Successfully." ''strEReq_ErrMessage

                .Parameters.Add("@biMailID", SqlDbType.NVarChar, 500)
                .Parameters("@biMailID").Direction = ParameterDirection.Output

                .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                .Parameters("@Msg").Direction = ParameterDirection.Output

                .CommandTimeout = 0
                .ExecuteNonQuery()
                strMailID = .Parameters("@biMailID").Value.ToString()
                strMsg = .Parameters("@Msg").Value.ToString()
            End With

            If strMsg <> "" Then
                strReturnMsg = strMsg
            Else
                strReturnMsg = ""
                If strMailID <> "" Then
                    Dim MailID As String() = strMailID.Split(New Char() {","c})
                    Dim intMailID As Integer
                    For Each intMailID In MailID
                        If intMailID <> 0 Then
                            strResult = SendMail(intMailID)
                            If strResult = "" Then
                                UpdateMailQueue(intMailID, 1, "Cancel eRequisition Successful", strCurrentDateTime)

                            Else
                                UpdateMailQueue(intMailID, 2, "Send Fail - " & strResult, strCurrentDateTime)
                            End If
                        End If
                    Next
                End If
            End If

            Return strReturnMsg

        Catch ex As Exception
            Return ex.Message.ToString
            Exit Function
        Finally
            sqlConn.Close()
        End Try
    End Function

End Class
