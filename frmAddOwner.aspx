<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAddOwner.aspx.vb" Inherits="frmAddOwner" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Owner</title>
    <link href="CSS/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="scripts/jquery.autocomplete.js" type="text/javascript"></script>
    
    <script type="text/javascript">

        $(document).ready(function() 
        {
            $("#<%=txtOwner.ClientID%>").autocomplete('Owner.ashx');
        });       
        
    </script>

</head>
<body>
    <form id="frmAddOwner" runat="server">
    <div>
        <table style="width: 453px">
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Smaller"
                        Text="Asset Owner"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 130px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="Small" ForeColor="Red"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 67px">
                    <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Owner Type"></asp:Label></td>
                <td style="width: 5px">
                    :</td>
                <td>
                    <asp:DropDownList ID="ddlUserType" runat="server" AutoPostBack="True" Font-Names="Verdana"
                        Font-Size="X-Small">
                        <asp:ListItem>Select One</asp:ListItem>
                        <asp:ListItem>User</asp:ListItem>
                        <asp:ListItem>Office</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td style="width: 130px">
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Owner"></asp:Label></td>
                <td style="width: 5px">
                    :</td>
                <td>
                    <asp:TextBox ID="txtOwner" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="300px"></asp:TextBox>
                    <asp:HiddenField ID="hdfOwnerID" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 130px">
                    <asp:Label ID="Label39" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Job Function"></asp:Label></td>
                <td style="width: 5px">
                    :</td>
                <td>
                    <asp:DropDownList ID="ddlJobFunction" runat="server" Font-Names="Verdana" Font-Size="X-Small" AutoPostBack="True">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td style="width: 130px">
                    <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Job Grade"></asp:Label></td>
                <td style="width: 5px">
                    :</td>
                <td>
                    <asp:TextBox ID="txtJobGrade" runat="server" Enabled="False" Font-Names="Verdana"
                        Font-Size="X-Small" Width="150px"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 130px">
                    <asp:Label ID="Label3" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Internal Audit"></asp:Label></td>
                <td style="width: 5px">
                    :</td>
                <td>
                    <asp:CheckBox ID="chkInternalAudit" runat="server" Font-Names="Verdana" Font-Size="X-Small" /></td>
            </tr>
            <tr>
                <td style="width: 130px">
                    <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="MIS Seed Member"></asp:Label></td>
                <td style="width: 5px">
                    :</td>
                <td>
                    <asp:CheckBox ID="chkMIS_Seed_Member" runat="server" Font-Names="Verdana" Font-Size="X-Small" /></td>
            </tr>
            <tr>
                <td style="width: 130px">
                </td>
                <td style="width: 5px">
                </td>
                <td align="left">
                    <table style="width: 188px">
                        <tr>
                            <td>
                                <asp:Button ID="btnAdd" runat="server" Text="Add" Width="65px" /></td>
                            <td style="width: 50px">
                            </td>
                            <td>
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="65px" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 67px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
