<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAddUser.aspx.vb" Inherits="frmAddUser" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Add User</title>
</head>
<body>
    <form id="frmAddUser" method="post" runat="server">
    <div>
        <table style="width: 453px">
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Smaller"
                        Text="Add User"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 130px">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="Small" ForeColor="Red"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 67px">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 67px">
                    <asp:Label ID="lblStation" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Text="Station:"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="ddlStation" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="323px">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td style="width: 130px">
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="User Name:"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtUserName" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="315px"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 130px">
                </td>
                <td align="left">
                    <asp:Button ID="btnView" runat="server" Text="View" Width="60px" /></td>
            </tr>
            <tr>
                <td style="width: 130px">
                    <asp:CheckBox ID="chkSelectAllCC" runat="server" AutoPostBack="True" Font-Names="Verdana"
                        Font-Size="X-Small" Text="Select All" /></td>
                <td align="left"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel ID="Panel1" runat="server" BorderStyle="Solid" BorderWidth="1px" Height="200px"
                        ScrollBars="Both" Width="450px">
                        <asp:GridView ID="gvName" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="UserID" Font-Names="Verdana" Font-Size="X-Small">
                            <Columns>
                                <asp:TemplateField HeaderText="Select">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkCC" runat="server" />&nbsp;
                                        <br />
                                        <asp:HiddenField ID="hfID" runat="server" Value='<%# Bind("UserID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="Fullname" HeaderText="Name" />
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnAddToList" runat="server" Text="Add To List" Width="156px" /></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="grvList" runat="server" AutoGenerateColumns="False" BackColor="White"
                        BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="UserID"
                        Font-Names="Verdana" Font-Size="X-Small" GridLines="Vertical" Width="450px">
                        <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnRemove" runat="server" CommandArgument='<%# Bind("UserID") %>'
                                        CommandName="Select">Remove</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Fullname" HeaderText="Name" />
                        </Columns>
                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="Gainsboro" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="65px" />
                    &nbsp;
                    <asp:Button ID="btnAdd" runat="server" Text="Add" Width="65px" /></td>
            </tr>
            <tr>
                <td style="width: 67px">
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
