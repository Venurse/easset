<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmLogin2.aspx.vb" Inherits="frmLogin2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Login</title>
    <script type="text/javascript">
//    <script language="javascript">
        function RefreshParent()
        {
            window.parent.location.href = window.parent.location.href;
        }
         function showDate() 
        {
            var month_names = new Array("Jan", "Feb", "Mar", 
            "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
            "Oct", "Nov", "Dec");

            var d = new Date();
            var curr_day = d.getDay();
            var curr_date = d.getDate();
            var hours = d.getHours();
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            
            var curr_hours;
            var curr_minutes;
            var curr_seconds;

            if(hours<10)
            {
                curr_hours = "0" + hours;
            }
            else
            {
                curr_hours = hours;
            }
            
            if(minutes < 10)
            {
                curr_minutes = "0" + minutes;
            }
             else
            {
                curr_minutes = minutes;
            }
            
           if(seconds < 10)
            {
                curr_seconds = "0" + seconds;
            }
             else
            {
                curr_seconds = seconds;
            }

            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
 
            document.getElementById("hdfCurrentDateTime").value = curr_date + " " +  month_names[curr_month] +  " " + curr_year + " " + curr_hours + ":" + curr_minutes + ":" + curr_seconds
         }
    </script>
</head>
<body background="Image/leavetype.jpg">
    <form id="frmLogin" runat="server">
    <div>

        <asp:Panel ID="Panel1" runat="server" >
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="Small" ForeColor="Red"></asp:Label><br />
                    <table style="width: 500px">
                        <tr>
                            <td style="width: 30%">
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td style="width: 200px" align="left">
                                <asp:Label ID="lblReLogin" runat="server" Visible="False"></asp:Label></td>
                            <td align="left" style="width: 30%">
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                            </td>
                            <td align="left">
                                <asp:Label ID="Label1" runat="server" Text="Login ID" ForeColor="Black"></asp:Label></td>
                            <td>
                                :</td>
                            <td style="width: 178px" align="left">
                                <asp:TextBox ID="txtUserID" runat="server" AutoCompleteType="Disabled" Width="150px"></asp:TextBox></td>
                            <td align="left" style="width: 178px" height="30">
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                            </td>
                            <td align="left">
                                <asp:Label ID="Label3" runat="server" Text="Password" ForeColor="Black"></asp:Label></td>
                            <td>
                                :</td>
                            <td align="left" style="width: 178px">
                                <asp:TextBox ID="txtPwd" runat="server" MaxLength="20" TextMode="Password" Width="150px"></asp:TextBox></td>
                            <td align="left" style="width: 178px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td style="width: 178px" align="left" height="40" valign="middle">
                                <asp:Button ID="btnLogin" runat="server" Text="Login" Width="55px" />
                                &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" /></td>
                            <td align="left" style="width: 178px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text="Station" ForeColor="Black" Visible="False"></asp:Label></td>
                            <td>
                            </td>
                            <td style="width: 178px" align="left">
                                <asp:DropDownList ID="ddlStation" runat="server" Width="155px" Visible="False">
                                </asp:DropDownList></td>
                            <td align="left" style="width: 178px">
                                <asp:HiddenField ID="hdfCurrentDateTime" runat="server" />
                            </td>
                        </tr>
                    </table>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
