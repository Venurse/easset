Imports System.IO

Partial Class frmOpenFile
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim strFile As String
            strFile = Request.QueryString("FilePath").ToString

            'Response.Redirect(strFile)


            If strFile.Contains("?") = True Then
                Response.Redirect(strFile + "&t=" + Format(Now, "hhmmss"))
            Else
                Response.Redirect(strFile + "?t=" + Format(Now, "hhmmss"))
            End If

        Catch ex As Exception
            lblMsg.Text = ex.Message.ToString
        End Try
    End Sub
End Class
