Imports System.Data
Imports System.Data.SqlClient

Partial Class frmAddUser2
    Inherits System.Web.UI.Page

    Dim dsStation As New DataSet
    Dim dsDepartment As New DataSet
    Dim dsEmployee As New DataSet
    Dim dsApplyTo As New DataSet

    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strApplyToList As String
        lblMsg.Text = ""

        If IsPostBack() = False Then

            Session("DA_New_USERLIST") = ""
            Session("DA_USERLIST_Name") = ""

            If Session("DA_USERLIST") = "" Then
                strApplyToList = ""
                Session("DA_New_USERLIST") = ""
            Else
                strApplyToList = Session("DA_USERLIST").ToString
                Session("DA_New_USERLIST") = Session("DA_USERLIST")
            End If
            BindDropDownList()
            GetApplyToData(strApplyToList)

        End If
    End Sub

    Private Sub BindDropDownList()
        Try

            dsStation = DIMERCO.SDK.Utilities.ReSM.GetStationList()
            Dim dtView As DataView = dsStation.Tables(0).DefaultView
            dtView.Sort = "StationCode ASC"
            ddlStation.DataSource = dtView
            ddlStation.DataValueField = dtView.Table.Columns("StationID").ToString
            ddlStation.DataTextField = dtView.Table.Columns("StationCode").ToString
            ddlStation.DataBind()
            ddlStation.SelectedValue = Session("DA_StationID").ToString 'Session("DA_AddUserStationID").ToString

        Catch ex As Exception
            Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
        End Try
    End Sub

    Private Sub GetApplyToData(ByVal strApplyToList As String)

        Dim strStation As String = ""
        Dim strDepartmentCode As String = 0

        If ddlStation.Items.Count <> 0 Then
            strStation = ddlStation.SelectedItem.Value.ToString
        End If

        'If ddlStation.Items.Count <> 0 Then
        '    If ddlDepartment.SelectedValue.ToString = "" Then
        '        strDepartmentCode = ""
        '    Else
        '        strDepartmentCode = ddlDepartment.SelectedItem.Value.ToString
        '    End If
        'End If

        dsEmployee = DIMERCO.SDK.Utilities.LSDK.getUserProfilebyFullNameNStation(strStation, txtUserName.Text.ToString, DIMERCO.SDK.Utilities.LSDK.EmpStatus.ACTIVE)

        Dim dtView As DataView = dsEmployee.Tables(0).DefaultView
        dtView.Sort = "FullName ASC"

        'Bind Employee 
        'dsEmployee = DIMERCO.SDK.Utilities.LSDK.getUserDataByStationIDNDeptID(strStation, strDepartmentCode, strApplyToList)
        gvUser.Visible = True
        gvUser.DataSource = dtView
        gvUser.DataMember = dtView.Table.TableName
        gvUser.DataBind()

        
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click

        If Session("DA_New_USERLIST") <> Nothing Then
            GetApplyToData(Session("DA_New_USERLIST").ToString)
        Else
            GetApplyToData("")
        End If
    End Sub

    Protected Sub gvUser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvUser.SelectedIndexChanged
        Dim strPayeeID As String
        Dim lblname As Label

        strPayeeID = gvUser.SelectedValue.ToString
        lblname = gvUser.SelectedRow.FindControl("lblname")


        Session("DA_USERLIST") = strPayeeID
        Session("DA_USERLIST_Name") = lblname.Text.ToString()
        Session("DA_New_USERLIST") = Nothing

        Dim ParentFormID As String = ""
        ParentFormID = Request.QueryString("ParentForm").ToString
        Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener =self;window.close();</script>")

    End Sub

End Class
