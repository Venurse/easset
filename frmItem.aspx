<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmItem.aspx.vb" Inherits="frmItem" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Item</title>
</head>
<body>
    <form id="frmItem" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 300px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td style="width: 10%">
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Item"></asp:Label></td>
                <td style="font-size: 12pt; width: 10%; color: #000000; font-family: Times New Roman">
                </td>
            </tr>
            <tr>
                <td style="width: 10%">
                </td>
                <td colspan="7">
                    &nbsp;</td>
                <td style="font-size: 12pt; width: 10%; color: #000000; font-family: Times New Roman">
                </td>
            </tr>
            <tr>
                <td style="width: 10%">
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td style="font-size: 12pt; width: 10%; color: #000000; font-family: Times New Roman">
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Group"></asp:Label></td>
                <td>
                    :</td>
                <td colspan="5">
                    <asp:DropDownList ID="ddlCategoryGroup" runat="server" AutoPostBack="True" Font-Names="Verdana"
                        Font-Size="X-Small">
                    </asp:DropDownList></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Category Code"></asp:Label></td>
                <td>
                    :</td>
                <td colspan="5">
                    <asp:TextBox ID="txtCategory" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        MaxLength="500" Width="420px"></asp:TextBox></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label24" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Category Description"></asp:Label></td>
                <td>
                    :</td>
                <td colspan="5">
                    <asp:TextBox ID="txtDescription" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="420px"></asp:TextBox></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td colspan="5">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" Width="79px" />
                    &nbsp; &nbsp;
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="65px" /></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; font-family: Times New Roman">
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
                <td colspan="5" style="height: 21px">
                </td>
                <td style="height: 21px">
                </td>
            </tr>
            <tr style="font-size: 12pt; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        DataKeyNames="CategoryID" Font-Names="Verdana" Font-Size="X-Small" ForeColor="#333333"
                        Width="80%">
                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                        <Columns>
                            <asp:TemplateField HeaderText="Select">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnSelect" runat="server" CommandArgument='<%# Bind("CategoryID") %>'
                                        CommandName="Select" Text="Select"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("Category") %>'></asp:Label>
                                    <asp:HiddenField ID="hdfParentCategoryID" runat="server" Value='<%# Bind("ParentCategoryID") %>' /><asp:HiddenField ID="hdfParentCategory" runat="server" Value='<%# Bind("ParentCategory") %>' />
                                    <asp:HiddenField ID="hdfDepreciationPeriod" runat="server" Value='<%# Bind("DepreciationPeriod") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category Description">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
