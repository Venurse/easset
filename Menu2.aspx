<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Menu2.aspx.vb" Inherits="Menu2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TreeView ID="TreeView1" runat="server" NodeIndent="10" NodeWrap="True" Width="164px" ExpandDepth="0">
            <ParentNodeStyle Font-Bold="False" />
            <HoverNodeStyle BackColor="#C0FFC0" Font-Underline="True" ForeColor="#5555DD" />
            <SelectedNodeStyle BackColor="#C0C0FF" Font-Underline="True" ForeColor="#5555DD"
                HorizontalPadding="0px" VerticalPadding="0px" />
            <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" HorizontalPadding="0px"
                NodeSpacing="0px" VerticalPadding="0px" />
        </asp:TreeView>
    </div>
    </form>
</body>
</html>
