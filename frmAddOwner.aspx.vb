Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class frmAddOwner
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ddlUserType.SelectedValue = "Select One"
        txtOwner.Text = ""
        chkInternalAudit.Checked = False
        chkMIS_Seed_Member.Checked = False
        ddlJobFunction.SelectedValue = ""
        txtJobGrade.Text = ""
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack() = False Then
            BindDropDownList()
        End If

    End Sub

    Protected Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim dtViewJobFunction As DataView = clsCF.GeteRequistionJobFunction()
                ddlJobFunction.DataSource = dtViewJobFunction
                ddlJobFunction.DataValueField = dtViewJobFunction.Table.Columns("Job_Function").ToString
                ddlJobFunction.DataTextField = dtViewJobFunction.Table.Columns("Job_Function").ToString
                ddlJobFunction.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub ddlJobFunction_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlJobFunction.SelectedIndexChanged

        If ddlUserType.SelectedValue = "Select One" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select User Type.')</script>")
            ddlJobFunction.SelectedValue = ""
            ddlUserType.Focus()
            Exit Sub
        End If

        If ddlUserType.SelectedValue = "User" Then
            Dim strAuthor As String = ""
            Dim intBkIndex As Integer = 0
            Dim intBkEndIndex As Integer = 0
            hdfOwnerID.Value = ""

            strAuthor = txtOwner.Text.ToString()
            intBkIndex = strAuthor.IndexOf("(")
            intBkEndIndex = strAuthor.IndexOf(")")

            If intBkIndex = -1 Or intBkEndIndex = -1 Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid User')</script>")
                Exit Sub
            End If

            hdfOwnerID.Value = strAuthor.Substring(intBkIndex + 1, 5).ToString()

            Dim dsOwnerInfo As DataSet = clsCF.GetOwnerInformation(hdfOwnerID.Value.ToString)
            If dsOwnerInfo.Tables.Count > 0 Then
                If dsOwnerInfo.Tables(0).Rows.Count > 0 Then
                    txtJobGrade.Text = dsOwnerInfo.Tables(0).Rows(0)("JobGrade").ToString

                    If dsOwnerInfo.Tables(0).Rows(0)("IsInternalAudit").ToString = "Y" Then
                        chkInternalAudit.Checked = True
                    Else
                        chkInternalAudit.Checked = False
                    End If

                    If dsOwnerInfo.Tables(0).Rows(0)("IsSeedMember").ToString = "Y" Then
                        chkMIS_Seed_Member.Checked = True
                    Else
                        chkMIS_Seed_Member.Checked = False
                    End If

                End If
            End If
        End If
    End Sub

    Protected Sub ddlUserType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUserType.SelectedIndexChanged

        Dim strAuthor As String = ""
        Dim intBkIndex As Integer = 0
        Dim intBkEndIndex As Integer = 0
        hdfOwnerID.Value = ""

        strAuthor = txtOwner.Text.ToString()
        intBkIndex = strAuthor.IndexOf("(")
        intBkEndIndex = strAuthor.IndexOf(")")

        If intBkIndex = -1 Or intBkEndIndex = -1 Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid User')</script>")
            Exit Sub
        End If


        If ddlUserType.SelectedValue = "Office" Then
            ddlJobFunction.SelectedValue = ""
            txtJobGrade.Text = ""
            chkInternalAudit.Checked = False
            chkMIS_Seed_Member.Checked = False

            If intBkIndex > 0 Then
                txtOwner.Text = ""
            Else
                hdfOwnerID.Value = txtOwner.Text.ToString
            End If
        ElseIf ddlUserType.SelectedValue = "User" Then
            If intBkIndex = -1 Then
                txtOwner.Text = ""
                ddlJobFunction.SelectedValue = ""
                txtJobGrade.Text = ""
                chkInternalAudit.Checked = False
                chkMIS_Seed_Member.Checked = False
            Else
                hdfOwnerID.Value = txtOwner.Text.ToString
            End If
        End If

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim strAuthor As String = ""
        Dim intBkIndex As Integer = 0
        hdfOwnerID.Value = ""

        strAuthor = txtOwner.Text.ToString()
        intBkIndex = strAuthor.IndexOf("(")

        If ddlUserType.SelectedValue = "User" Then
            If intBkIndex = -1 Then
                txtOwner.Text = ""
                ddlJobFunction.SelectedValue = ""
                txtJobGrade.Text = ""
                chkInternalAudit.Checked = False
                chkMIS_Seed_Member.Checked = False
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid User Detail.')</script>")
                Exit Sub
            Else
                hdfOwnerID.Value = txtOwner.Text.ToString
            End If
        ElseIf ddlUserType.SelectedValue = "Office" Then
            ddlJobFunction.SelectedValue = ""
            txtJobGrade.Text = ""
            chkInternalAudit.Checked = False
            chkMIS_Seed_Member.Checked = False

            If intBkIndex > 0 Then
                txtOwner.Text = ""
                'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid User Detail.')</script>")
                'Exit Sub
            Else
                hdfOwnerID.Value = txtOwner.Text.ToString
            End If
        End If

    End Sub
End Class
