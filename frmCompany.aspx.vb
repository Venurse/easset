Imports System.Data
Imports System.Data.SqlClient

Partial Class frmCompany
    Inherits System.Web.UI.Page
    Dim clsCF As New clsComFunction
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")
    Dim streChainVP As String = ConfigurationSettings.AppSettings("eChainVP_db")
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'GetCompany()
    End Sub

    Private Sub GetCompany()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetVendorPayee")

                With MyCommand
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = ""
                    .Parameters.Add("@Type", SqlDbType.VarChar, 50)
                    .Parameters("@Type").Value = "ReSM_Vendor"
                    .Parameters.Add("@StationID", SqlDbType.VarChar)
                    .Parameters("@StationID").Value = Session("AccessStation").ToString
                    .Parameters.Add("@Name", SqlDbType.VarChar, 50)
                    .Parameters("@Name").Value = txtName.Text.ToString()
                    .CommandTimeout = 0
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "Company"


                'If MyData.Tables(0).Rows.Count > 0 Then
                gvCompany.Visible = True
                gvCompany.DataSource = MyData
                gvCompany.DataMember = MyData.Tables(0).TableName
                gvCompany.DataBind()
                'Else
                'gvCompany.Visible = False
                'End If

            Catch ex As Exception
                Me.lblMsg.Text = "GetCompany: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtName.Text.ToString() = "" Then
            lblMsg.Text = "Please enter name!"
            Exit Sub
        End If

        GetCompany()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        Response.Write("<script language=""Javascript"">window.opener =self;window.close();</script>")
        Response.End()
    End Sub

    Protected Sub gvCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvCompany.SelectedIndexChanged
        Dim strCompanyID As String
        Dim lblcode As Label
        Dim lblname As Label

        strCompanyID = gvCompany.SelectedValue.ToString

        lblcode = gvCompany.SelectedRow.FindControl("lblcode")
        lblname = gvCompany.SelectedRow.FindControl("lblname")

        Session("DA_CompanyID") = strCompanyID
        Session("DA_CompanyCode") = lblcode.Text.ToString()
        Session("DA_CompanyName") = lblname.Text.ToString()

        Dim ParentFormID As String = ""
        ParentFormID = Request.QueryString("ParentForm").ToString
        Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener =self;window.close();</script>")

    End Sub
End Class
