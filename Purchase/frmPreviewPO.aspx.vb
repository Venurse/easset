Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings
Imports Microsoft.Reporting.WebForms

Partial Class Purchase_frmPreviewPO
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack() = False Then
            hdfParentURL.Value = Session("ParentURL").ToString
            BindReportRDLC()
        End If
    End Sub

    Private Sub BindReportRDLC()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetStationPORdlc")

                With MyCommand

                    .Parameters.Add("@StationID", SqlDbType.VarChar, 6)
                    .Parameters("@StationID").Value = Session("PO_StationID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "PO_RDLC_Path"

                'ReportViewer1.ProcessingMode = ProcessingMode.Local


                If MyData.Tables.Count > 0 Then
                    If MyData.Tables(0).Rows.Count > 0 Then
                        If MyData.Tables(0).Rows(0)("PO_RDLC_Path").ToString <> "" Then
                            ''Dim dtTable As Data.DataTable
                            'Dim rds As Microsoft.Reporting.WebForms.ReportDataSource

                            'ReportViewer1.Reset()
                            'ReportViewer1.LocalReport.Dispose()

                            'rds = New Microsoft.Reporting.WebForms.ReportDataSource("DataSet1_SP_GetPOPreviewData", ObjectDataSource1)
                            'ReportViewer1.LocalReport.DataSources.Clear()
                            'ReportViewer1.LocalReport.DataSources.Add(rds)
                            ReportViewer1.LocalReport.ReportPath = MyData.Tables(0).Rows(0)("PO_RDLC_Path").ToString

                            'Dim rptParam(1) As Microsoft.Reporting.WebForms.ReportParameter
                            'rptParam(0) = New Microsoft.Reporting.WebForms.ReportParameter("ApplyNewID", Session("ApplyNewID").ToString)
                            'rptParam(1) = New Microsoft.Reporting.WebForms.ReportParameter("POID", Session("POID").ToString)
                            'ReportViewer1.LocalReport.SetParameters(rptParam)
                            'ReportViewer1.DataBind()

                            'ReportViewer1.LocalReport.Refresh()
                            'ReportViewer1.Visible = True
                            'ReportViewer1.ShowReportBody = True

                            'Me.ReportViewer1.LocalReport.ReportPath = MyData.Tables(0).Rows(0)("PO_RDLC_Path").ToString
                        End If

                        hdfPOPath.Value = MyData.Tables(0).Rows(0)("PO_Path").ToString
                    End If
                End If

            Catch ex As Exception
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + ex.Message.ToString + "')</script>")
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using

    End Sub

    Protected Sub Page_SaveStateComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SaveStateComplete
        Dim warnings As Warning() = Nothing
        Dim streamids As String() = Nothing
        Dim mimeType As String = Nothing
        Dim encoding As String = Nothing
        Dim extension As String = Nothing
        Dim bytes As Byte()

        If hdfPOPath.Value.ToString = "" Then
            Exit Sub
        End If

        Dim FolderLocation As String = hdfPOPath.Value.ToString + "/" + Session("ApplyNewID").ToString + "/" + Session("POID").ToString + "/"
        Dim filepath As String = FolderLocation & Session("POID").ToString & ".pdf"

        If FileIO.FileSystem.DirectoryExists(FolderLocation) = False Then
            FileIO.FileSystem.CreateDirectory(FolderLocation)
        End If

        If FileIO.FileSystem.FileExists(filepath) = True Then
            If FileIO.FileSystem.FileExists(filepath) = True Then
                FileIO.FileSystem.DeleteFile(filepath)
            End If
        End If

        SavePOPDFPath()

        'Then create new pdf file
        bytes = ReportViewer1.LocalReport.Render("PDF", Nothing, mimeType, encoding, extension, streamids, warnings)
        Dim fs As New FileStream(filepath, FileMode.Create)
        fs.Write(bytes, 0, bytes.Length)
        fs.Close()
        'Set the appropriate ContentType.
        Response.ContentType = "Application/pdf"
        'Write the file directly to the HTTP output stream.
        Response.WriteFile(filepath)
        Response.End()


        'Server.TransferRequest(Request.Url.AbsolutePath, False)

    End Sub

    Private Sub SavePOPDFPath()

        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String = ""
                Dim strPOID As String = ""

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SavePOPDFPath", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@POID", SqlDbType.VarChar, 20)
                    .Parameters("@POID").Value = Session("POID").ToString
                    .Parameters.Add("@FileName", SqlDbType.NVarChar, 500)
                    .Parameters("@FileName").Value = Session("POID").ToString & ".pdf"

                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else

                End If
            Catch ex As Exception
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + ex.Message.ToString + "')</script>")
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

End Class
