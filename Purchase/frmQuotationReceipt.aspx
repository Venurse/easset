<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmQuotationReceipt.aspx.vb" Inherits="Purchase_frmQuotationReceipt" ValidateRequest="false"%>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" Namespace="eWorld.UI" TagPrefix="ew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Quotation / Receipt</title>
    <script type="text/javascript">
         function showDate() 
        {
            var month_names = new Array("Jan", "Feb", "Mar", 
            "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
            "Oct", "Nov", "Dec");

            var d = new Date();
            var curr_day = d.getDay();
            var curr_date = d.getDate();
            var hours = d.getHours();
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            
            var curr_hours;
            var curr_minutes;
            var curr_seconds;

            if(hours<10)
            {
                curr_hours = "0" + hours;
            }
            else
            {
                curr_hours = hours;
            }
            
            if(minutes < 10)
            {
                curr_minutes = "0" + minutes;
            }
             else
            {
                curr_minutes = minutes;
            }
            
           if(seconds < 10)
            {
                curr_seconds = "0" + seconds;
            }
             else
            {
                curr_seconds = seconds;
            }

            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
 
            document.getElementById("hdfCurrentDateTime").value = curr_date + " " +  month_names[curr_month] +  " " + curr_year + " " + curr_hours + ":" + curr_minutes + ":" + curr_seconds
         }
//         function ReplaceParagrah()
//         {
//            var strSpec  = document.getElementById("txtSpecification").value;
//            strSpec = strSpec.replace("<p>","")
//            strSpec = strSpec.replace("</p>","")
//         
//            document.getElementById("txtSpecification").value = strSpec;
//         }
     </script>
    <style type="text/css">
        .style1
        {
            width: 162px;
        }
    </style>
</head>
<body onunload="window.opener.document.frmApplyNew.submit();">
    <form id="frmQuotationReceipt" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Quotation / Receipt"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:HiddenField ID="hdfImageDocPath_Temp" runat="server" /><asp:HiddenField ID="hdfImageDocPath_Temp_Open" runat="server" />
                    <asp:HiddenField ID="hdfApplyNewID" runat="server" />
                    <asp:HiddenField ID="hdfQRC_AlbumName" runat="server" />
                    <asp:HiddenField ID="hdfCurrentDateTime" runat="server" />
                    <asp:HiddenField ID="hdfStationID" runat="server" /><asp:HiddenField ID="hdfApplyNewType" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 150px">
                                <asp:Label ID="Label23" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Compare ID"></asp:Label></td>
                            <td style="width: 5px">
                                :</td>
                            <td>
                            <asp:Label ID="lblQuotationCompareID" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 150px">
                            <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Type"></asp:Label></td>
                            <td style="width: 5px">
                                :</td>
                            <td>
                            <asp:DropDownList ID="ddlQuoRecpt" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                <asp:ListItem>Select One</asp:ListItem>
                                <asp:ListItem>Quotation</asp:ListItem>
                                <asp:ListItem>Receipt</asp:ListItem>
                            </asp:DropDownList>
                                <asp:Label ID="lblQuotationReceiptID" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                            <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Vendor/Payee"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:TextBox ID="txtPayee" runat="server" Font-Names="Verdana" Font-Size="X-Small" Width="300px" ReadOnly="True"></asp:TextBox>
                                <asp:Button ID="btnVendor" runat="server" Text="...." Font-Names="Verdana" Font-Size="X-Small" /><asp:HiddenField ID="hdfPayee" runat="server" />
                                 <asp:HiddenField ID="hdfPayeeType" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label22" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Contact Person"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlVendorContactPerson" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label20" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Shop Name"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:TextBox ID="txtShopName" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    MaxLength="500" Width="400px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                            <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Quotation / Receipt No"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                            <asp:TextBox ID="txtQuoRecptNo" runat="server" Font-Names="Verdana" Font-Size="X-Small" Width="200px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                            <asp:Label ID="Label8" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Total Amount (Include Tax)"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlCurrency" runat="server" Font-Names="Verdana" Font-Size="X-Small" Enabled="False">
                                </asp:DropDownList>
                                <ew:NumericBox ID="txtTotalAmount" runat="server" Font-Names="Verdana" Font-Size="X-Small"></ew:NumericBox></td>
                        </tr>
                        <tr>
                            <td>
                    <asp:Label ID="Label6" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Quotation / Receipt Date"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <ew:CalendarPopup ID="cldQuoRecptDate" runat="server" Width="120px">
<SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></SelectedDateStyle>

<HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></HolidayStyle>

<OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Gray"></OffMonthStyle>

<MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></MonthHeaderStyle>

<WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></WeekdayStyle>

<GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></GoToTodayStyle>

<ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></ClearDateStyle>

<WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></WeekendStyle>

<DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></DayHeaderStyle>

<TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></TodayDayStyle>
</ew:CalendarPopup>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <asp:Label ID="Label7" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Payment Terms"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                            <asp:DropDownList ID="ddlPaymentTerm" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                            </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>
                            <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Quotation / Receipt"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                        <asp:FileUpload ID="fuQuoRecpt" runat="server" Width="412px" />
                        <asp:Button ID="btnAttach" runat="server" Font-Names="Arial" Font-Size="Small" Text="Attach" Width="65px" Height="21px" /></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            <asp:GridView ID="gvFile" runat="server" AutoGenerateColumns="False" Font-Names="Verdana" Font-Size="X-Small" DataKeyNames="FileName">
                            <Columns>
                                <asp:TemplateField HeaderText="Remove">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnRemove" runat="server" CommandArgument='<%# Bind("FileName") %>' CommandName="Delete">Remove</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="File">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPath" runat="server" Text='<%# Bind("Path") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 51px">
                            </td>
                            <td style="height: 51px">
                            </td>
                            <td style="height: 51px">
                            <table style="width: 450px">
                                <tr>
                                    <td style="width: 150px">
                                        <asp:Button ID="btnSave" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Save"
                                            Width="85px" OnClientClick="showDate();" /></td>
                                    <td style="width: 150px">
                                        <asp:Button ID="btnCancel_H" runat="server" Font-Names="Verdana" Font-Size="Small"
                                            Text="Clear" Width="85px" /></td>
                                    <td style="width: 150px">
                                    </td>
                                </tr>
                            </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <asp:GridView ID="gvQuoRecpt" runat="server" AutoGenerateColumns="False" BackColor="White"
                        BorderColor="#C0C0FF" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" Font-Names="Verdana"
                        Font-Size="X-Small" DataKeyNames="QuotationReceiptID">
                        <RowStyle BackColor="White" ForeColor="#003399" />
                        <Columns>
                            <asp:TemplateField HeaderText="Remove">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnRemove" runat="server" CommandArgument='<%# Bind("QuotationReceiptID") %>' CommandName="Delete" OnClientClick="return confirm('Are you sure you want to remove this Quotation\Receipt?');" Enabled='<%# Bind("AllowEditDelete") %>'>Remove</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type">
                                <ItemTemplate>
                                    <asp:Label ID="lblType" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vendor / Payee">
                                <ItemTemplate>
                                    <asp:Label ID="lblVendorPayeeName" runat="server" Text='<%# Bind("VendorPayeeName") %>'></asp:Label>
                                    <asp:HiddenField ID="hdVendorPayeeID" runat="server" Value='<%# Bind("VendorPayeeID") %>' />
                                    <asp:HiddenField ID="hdfVendorPayeeType" runat="server" Value='<%# Bind("VendorPayeeType") %>' />
                                    <asp:HiddenField ID="hdfShopName" runat="server" Value='<%# Bind("ShopName") %>' />
                                    <asp:HiddenField ID="hdfVendorContactID" runat="server" Value='<%# Bind("VendorContactID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quotation/Receipt ID">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnQuotationReceiptID" runat="server" CommandArgument='<%# Bind("QuotationReceiptID") %>'
                                        Text='<%# Bind("QuotationReceiptID") %>' CommandName="Select" Enabled='<%# Bind("AllowEditDelete") %>'></asp:LinkButton>
                                    <%--<asp:HiddenField ID="hdfQuotationReceipt" runat="server" Value='<%# Bind("QuotationReceipt") %>' />--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quotation/Receipt No">
                                <ItemTemplate>
                                    <asp:Label ID="lblQuotationReceiptNo" runat="server" Text='<%# Bind("QuotationReceiptNo") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Currency Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblCurrencyCode" runat="server" Text='<%# Bind("CurrencyCode") %>'></asp:Label>
                                    <asp:HiddenField ID="hdfCurrencyCode" runat="server" Value='<%# Bind("CurrencyCodeID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total Amount">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotalAmount" runat="server" Text='<%# Bind("TotalAmount") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quotation Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblQuotationReceiptDate" runat="server" Text='<%# Bind("QuotationReceiptDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Payment Term">
                                <ItemTemplate>
                                    <asp:Label ID="lblPaymentTerm" runat="server" Text='<%# Bind("PaymentTerm") %>'></asp:Label>
                                    <asp:HiddenField ID="hdPaymentTermID" runat="server" Value='<%# Bind("PaymentTermID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quotation / Receipt">
                                <ItemTemplate>
                                    <asp:Label ID="lblQuoRecpt" runat="server" Text='<%# Bind("QuoRecpt") %>'></asp:Label>
                                    <asp:HiddenField ID="hdfQR_AlbumName" runat="server" Value='<%# Bind("FilePath") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                        <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                    </asp:GridView>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 150px">
                                <asp:Label ID="Label32" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Total Amount"></asp:Label></td>
                            <td style="width: 5px; font-size: 12pt; font-family: Times New Roman;">
                                :</td>
                            <td style="font-size: 12pt; font-family: Times New Roman">
                                <asp:Label ID="lblQuoCompare_TotalAmt" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <hr />
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="3">
                    <asp:Label ID="Label9" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                        Font-Underline="True" Text="Quotation / Receipt Detail :-"></asp:Label></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="lblID" runat="server" Font-Names="Verdana" Font-Size="X-Small" Visible="False"></asp:Label></td>
                <td>
                    </td>
                <td colspan="5">
                    <asp:HiddenField ID="hdfQuotationPricePointCtrl" runat="server" />
                    <asp:HiddenField ID="hdfType" runat="server" />
                    <asp:HiddenField ID="hdfPricePointQuotation" runat="server" /><asp:HiddenField ID="hdfStationPricePointeRequisitionLimit" runat="server" />
                    <asp:HiddenField ID="hdfStationPricePointeRequisition" runat="server" />
                    <asp:HiddenField ID="hdfCategoryPricePointeRequisitionLimit" runat="server" />
                    <asp:HiddenField ID="hdfCategoryPricePointeRequisition" runat="server" />
                    <asp:HiddenField ID="hdfSend_eReq" runat="server" />
                    <asp:HiddenField ID="hdfIs_Budget_Item" runat="server" />
                    <asp:HiddenField ID="hdfGroupItemType" runat="server" /><asp:HiddenField ID="hdfSend_eReq_PerUnit" runat="server" />
                    <asp:HiddenField ID="hdfPendingQty" runat="server" />
                    <asp:HiddenField ID="hdfPendingAmt" runat="server" />
                    <asp:HiddenField ID="hdfBalanceQty" runat="server" />
                    <asp:HiddenField ID="hdfBalanceAmt" runat="server" />
                    <asp:HiddenField ID="hdfBudgetQty" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8" style="font-size: 12pt; font-family: Times New Roman">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 150px">
                            <asp:Label ID="Label11" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Quotation / Receipt No"></asp:Label></td>
                            <td style="font-size: 12pt; width: 5px; font-family: Times New Roman">
                                :</td>
                            <td style="font-size: 12pt; font-family: Times New Roman">
                                <asp:DropDownList ID="ddlQuoRecptNo" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 150px">
                                <asp:Label ID="Label25" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Category"></asp:Label></td>
                            <td style="font-size: 12pt; width: 5px; font-family: Times New Roman">
                                :</td>
                            <td style="font-size: 12pt; font-family: Times New Roman">
                                <asp:DropDownList ID="ddlCategory" runat="server" Font-Names="Verdana" Font-Size="X-Small" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="lblCategoryBudgetStatus" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="#C00000">Total Unit:  ; Approved:  ; Pending:  ; Balance: </asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 150px">
                    <asp:Label ID="Label10" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Sub Category"></asp:Label></td>
                            <td style="font-size: 12pt; width: 5px; font-family: Times New Roman">
                                :</td>
                            <td style="font-size: 12pt; font-family: Times New Roman">
                    <asp:DropDownList ID="ddlSubCategory" runat="server" Font-Names="Verdana" Font-Size="X-Small" AutoPostBack="True">
                    </asp:DropDownList>&nbsp;<asp:CheckBox ID="chkSpecialApproval" runat="server" AutoPostBack="True"
                        Font-Names="Verdana" Font-Size="X-Small" Text="Special Approval" />
                    <asp:Label ID="lblSubCategoryBudgetStatus" runat="server" Font-Names="Verdana" Font-Size="X-Small" Font-Bold="True" ForeColor="#C00000" Visible="False">Total Unit:  ; Approved:  ; Pending:  ; Balance: </asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label31" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Exchange Type"></asp:Label></td>
                            <td>
                                <asp:Label ID="Label33" runat="server" Text=":" Width="1px"></asp:Label></td>
                            <td>
                                <asp:DropDownList ID="ddlSpecialRequest" runat="server" Font-Names="Verdana"
                                    Font-Size="X-Small">
                                </asp:DropDownList><asp:DropDownList ID="ddlSpecialReqCategory" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label12" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Price Per Unit (Include Tax)"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <ew:NumericBox ID="txtPricePerUnit" runat="server" Font-Names="Verdana" Font-Size="X-Small" PositiveNumber="True"></ew:NumericBox>
                                <asp:Label ID="Label29" runat="server" Text="/"></asp:Label><asp:Label ID="lblBudgetCurrency"
                                    runat="server" Font-Bold="True" Font-Italic="False" Font-Names="Verdana" Font-Size="X-Small"></asp:Label>
                                <asp:DropDownList ID="ddlBudgetAmount" runat="server" AutoPostBack="True" Font-Bold="True"
                                    Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList>
                                <asp:Label ID="lblBudgetStatus" runat="server" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="X-Small" ForeColor="#C00000">Total Unit:  ; Approved:  ; Pending:  ; Balance: </asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label13" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Quantity"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <ew:NumericBox ID="txtQty" runat="server" Font-Names="Verdana" Font-Size="X-Small" MaxLength="3" PositiveNumber="True"></ew:NumericBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label14" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Type"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <asp:RadioButton ID="rbtnCost" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        GroupName="Type" Text="Cost" Checked="True" />
                    &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;<asp:RadioButton ID="rbtnOthCharges" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        GroupName="Type" Text="Others Charges" /></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label15" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Brand"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlBrand" runat="server" AutoPostBack="True" Font-Names="Verdana"
                                    Font-Size="X-Small">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtBrand" runat="server" Font-Names="Verdana" Font-Size="X-Small" Width="150px" MaxLength="50"></asp:TextBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label16" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Model"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <asp:TextBox ID="txtModel" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="300px" MaxLength="100"></asp:TextBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label17" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Specification"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <FTB:FreeTextBox ID="txtSpecification" runat="server" AllowHtmlMode="False" AutoGenerateToolbarsFromString="True"
                                    AutoParseStyles="False" BreakMode="Paragraph" ButtonDownImage="False"
                                    DownLevelMode="TextArea" EnableHtmlMode="False" EnableViewState="False" Height="200px"
                                    PasteMode="Disabled" ToolbarLayout="Bold,Italic,Underline,Strikethrough;Superscript,Subscript|BulletedList,NumberedList,Indent,Outdent|FontFacesMenu,FontSizesMenu"
                                    ToolbarStyleConfiguration="Office2003" Width="650px" ClientSideTextChanged="">
                                </FTB:FreeTextBox>
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label18" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Remarks"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <asp:TextBox ID="txtRemarks" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        TextMode="MultiLine" Width="400px"></asp:TextBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td colspan="3">
                             <%-- <ew:CollapsablePanel id="CollapsablePanel1" runat="server" BorderColor="Silver" BorderWidth="1px" Collapsable =true 
                                    Collapsed="false" CollapserAlign="Left" ExpandText="Add Asset User - *Please complete above data before Add User" Font-Names="Verdana"
                                    Font-Size="X-Small" TitleVerticalAlignment="Top" AllowSliding="True">--%>
                                     <asp:Panel ID="Panel1" runat="server" Font-Names="Verdana" 
                                                    Font-Size="X-Small" GroupingText="Add Asset User - *Please complete above data before Add User" Width = "850">
                                    
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 147px">
                                                <asp:Label ID="Label21" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Condition"></asp:Label></td>
                                            <td style="width: 5px">
                                                :</td>
                                            <td>
                                                <asp:DropDownList ID="ddlCondition" runat="server" Font-Names="Verdana" 
                                                    Font-Size="X-Small" AutoPostBack="True">
                                                    <asp:ListItem></asp:ListItem>
                                                    <asp:ListItem Value="A">Add New</asp:ListItem>
                                                    <asp:ListItem Value="R">Replace</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td>
                                                <asp:Panel ID="pnlExistingAsset" runat="server" Font-Names="Verdana" 
                                                    Font-Size="X-Small" GroupingText="Existing Asset" Width = "550">
                                                    <table width="500">
                                                        <tr>
                                                            <td width="60">
                                                                <asp:Label ID="Label41" runat="server" Font-Names="Verdana" Font-Size="X-Small" 
                                                                    Text="Asset ID"></asp:Label>
                                                            </td>
                                                            <td width="5">
                                                                :</td>
                                                            <td width="50">
                                                                <asp:Button ID="btnSearchAsset" runat="server" Font-Names="Verdana" 
                                                                    Font-Size="X-Small" Text="Search" />
                                                            </td>
                                                            <td width="90">
                                                                <asp:TextBox ID="txtExistingAsset" runat="server" Width="120px"></asp:TextBox>
                                                            </td>
                                                            <td width="80">
                                                                <asp:Label ID="Label40" runat="server" Font-Names="Verdana" Font-Size="X-Small" 
                                                                    Text="Purchase Date"></asp:Label>
                                                            </td>
                                                            <td width="5">
                                                                :</td>
                                                            <td width="150">
                                                                <ew:CalendarPopup ID="cldEAssetPurchaseDate" runat="server" Width="100px" 
                                                                    Font-Names="Verdana" Font-Size="X-Small" SelectedDate="" Nullable="True" 
                                                                    VisibleDate="">
                                                                    <SelectedDateStyle BackColor="Yellow" 
                                                                        Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" 
                                                                        ForeColor="Black" />
                                                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" 
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <OffMonthStyle BackColor="AntiqueWhite" 
                                                                        Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" 
                                                                        ForeColor="Gray" />
                                                                    <MonthHeaderStyle BackColor="Yellow" 
                                                                        Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" 
                                                                        ForeColor="Black" />
                                                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" 
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" 
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" 
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" 
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" 
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <TodayDayStyle BackColor="LightGoldenrodYellow" 
                                                                        Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" 
                                                                        ForeColor="Black" />
                                                                </ew:CalendarPopup>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label26" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Location"></asp:Label></td>
                                            <td>
                                                :</td>
                                            <td colspan="2">
                                                <asp:DropDownList ID="ddlLocation" runat="server" Font-Names="Verdana"
                                                    Font-Size="X-Small">
                                                </asp:DropDownList></td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label27" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="User"></asp:Label></td>
                                            <td>
                                                :</td>
                                            <td colspan="2">
                                                <asp:DropDownList ID="ddlUserType" runat="server" AutoPostBack="True" Font-Names="Verdana"
                                                    Font-Size="X-Small">
                                                    <asp:ListItem>Select One</asp:ListItem>
                                                    <asp:ListItem>User</asp:ListItem>
                                                    <asp:ListItem>Office</asp:ListItem>
                                                </asp:DropDownList><br />
                                                <asp:TextBox ID="txtOwner" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                                    MaxLength="100" Width="225px"></asp:TextBox><asp:Button ID="btnOwner" runat="server"
                                                        Enabled="False" Font-Size="X-Small" Text="...." /><br />
                                                <asp:CheckBox ID="chkInternalAudit" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                                    Text="Internal Audit" /><br />
                                                <asp:CheckBox ID="chkMIS_Seed_Member" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                                    Text="MIS Seed Member" /><asp:HiddenField ID="hdfUserID" runat="server" />
                                            </td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label38" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Job Grade"></asp:Label></td>
                                            <td>
                                                :</td>
                                            <td colspan="2">
                                                <asp:TextBox ID="txtJobGrade" runat="server" Enabled="False" Font-Names="Verdana"
                                                    Font-Size="X-Small" Width="150px"></asp:TextBox></td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label39" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Job Function"></asp:Label></td>
                                            <td>
                                                :</td>
                                            <td colspan="2">
                                                <asp:DropDownList ID="ddlJobFunction" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                                </asp:DropDownList></td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label28" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Application To Be Used"></asp:Label></td>
                                            <td>
                                                :</td>
                                            <td colspan="2">
                                                <asp:TextBox ID="txtAppUsed" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                                    Width="500px"></asp:TextBox></td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label30" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Remarks"></asp:Label></td>
                                            <td>
                                                :</td>
                                            <td colspan="2">
                                                <asp:TextBox ID="txtUserRemarks" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                                    MaxLength="500" TextMode="MultiLine" Width="500px"></asp:TextBox></td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td colspan="2">
                                                <asp:Button ID="btnSaveUser" runat="server" OnClientClick="showDate();" Text="Add User"
                                                    Width="79px" /></td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                    </table>
                                    
                                     </asp:Panel>
                              <%-- </ew:CollapsablePanel>--%>
                                
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                               <asp:Label ID="Label42" runat="server" Font-Names="Verdana" 
                                    Font-Size="X-Small" Text="User"></asp:Label>
                               </td>
                            <td>
                                :</td>
                            <td>
                                <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                    DataKeyNames="QuotationReceiptID,CategoryID" Font-Names="Verdana" Font-Size="X-Small"
                                    ForeColor="#333333" Width="90%">
                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnDelete" runat="server" CommandArgument='<%# Bind("OwnerID") %>'
                                                    CommandName="Delete" Text="Delete"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="User">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblUser" runat="server" Text='<%# Bind("OwnerName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Job Grade">
                                            <ItemTemplate>
                                                <asp:Label ID="lblJobGrade" runat="server" Text='<%# Bind("JobGrade") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Job Function">
                                            <ItemTemplate>
                                                <asp:Label ID="lblJobFunction" runat="server" Text='<%# Bind("Job_Function") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Internal Audit">
                                            <ItemTemplate>
                                                <asp:Label ID="lblInternalAudit" runat="server" Text='<%# Bind("Internal_Audit") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MIS Seed Member">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMISSeedMember" runat="server" Text='<%# Bind("MIS_Seed_Member") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Condition">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCondition" runat="server" Text='<%# Bind("Condition") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="AppUsed" HeaderText="App To Be Used" />
                                        <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                        <asp:BoundField DataField="ExistingAssetID" HeaderText="Existing Asset ID" />
                                        <asp:BoundField DataField="ExistingAssetPurchaseDate" 
                                            HeaderText="Existing Asset Purchase Date" />
                                    </Columns>
                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <table style="width: 450px">
                                    <tr>
                                        <td style="width: 150px">
                                                <asp:Button ID="btnAdd" runat="server" OnClientClick="showDate();" Text="Save"
                                                    Width="79px" />
                                    </td>
                                        <td style="width: 150px">
                                                <asp:Button ID="btnClear" runat="server" OnClientClick="showDate();" Text="Clear"
                                                    Width="79px" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <asp:GridView ID="gvQuoRecptDetail" runat="server" AutoGenerateColumns="False" BackColor="White"
                        BorderColor="#C0C0FF" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" Font-Names="Verdana"
                        Font-Size="X-Small" DataKeyNames="QuotationReceiptID">
                        <RowStyle BackColor="White" ForeColor="#003399" />
                        <Columns>
                            <asp:TemplateField HeaderText="Remove">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnRemove" runat="server" CommandArgument='<%# Bind("ID") %>' CommandName="Delete" OnClientClick="return confirm('Are you sure you want to remove this Item Detail?');" Enabled='<%# Bind("AllowEditDelete") %>'>Remove</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnSubCategory" runat="server" CommandArgument='<%# Bind("CategoryID") %>'
                                        Text='<%# Bind("Category") %>' CommandName="select" Enabled='<%# Bind("AllowEditDelete") %>'></asp:LinkButton>
                                    <asp:Label ID="lblSpecialReqCategory" runat="server" Text='<%# Bind("SpecialReqCategory") %>'
                                        Visible='<%# Bind("SpecialRequest") %>'></asp:Label><br />
                                    <asp:HiddenField ID="hdSubCategoryID" runat="server" Value='<%# Bind("CategoryID") %>' />
                                    <asp:HiddenField ID="hdfParentCategoryID" runat="server" Value='<%# Bind("ParentCategoryID") %>' />
                                    <asp:HiddenField ID="hdfBudgetListItemID" runat="server" Value='<%# Bind("BudgetListItemID") %>' />
                                    <asp:HiddenField ID="hdfActualCategory" runat="server" Value='<%# Bind("ActualCategoryID") %>' />
                                    <asp:HiddenField ID="hdfSpecialRequest" runat="server" Value='<%# Bind("SpecialRequest") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quotation/Receipt No">
                                <ItemTemplate>
                                    <asp:Label ID="lblQuotationReceiptNo" runat="server" Text='<%# Bind("QuotationReceiptNo") %>'></asp:Label>
                                    <asp:HiddenField ID="hdQuotationReceiptID" runat="server" Value='<%# Bind("QuotationReceiptID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Price Per Unit">
                                <ItemTemplate>
                                    <asp:Label ID="lblPricePerUnit" runat="server" Text='<%# Bind("PricePerUnit") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quantity">
                                <ItemTemplate>
                                    <asp:Label ID="lblQuantity" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type">
                                <ItemTemplate>
                                    <asp:Label ID="lblType" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Brand">
                                <ItemTemplate>
                                    <asp:Label ID="lblBrand" runat="server" Text='<%# Bind("BrandName") %>'></asp:Label>
                                    <asp:HiddenField ID="hdfBrand" runat="server" Value='<%# Bind("Brand") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Model">
                                <ItemTemplate>
                                    <asp:Label ID="lblModel" runat="server" Text='<%# Bind("Model") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Specification">
                                <ItemTemplate>
                                    <asp:Label ID="lblSpecification" runat="server" Text='<%# Bind("Specification") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="250px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remarks">
                                <ItemTemplate>
                                    <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Budget Status">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Approval_Desc") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="Red" Text='<%# Bind("Approval_Desc") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Font-Bold="False" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                        <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                    </asp:GridView>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td style="height: 89px">
                </td>
                <td colspan="7" style="height: 89px">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 150px">
                    <asp:Label ID="Label19" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Total Amount"></asp:Label></td>
                            <td style="font-size: 12pt; width: 5px; font-family: Times New Roman">
                                :</td>
                            <td style="font-size: 12pt; font-family: Times New Roman">
                    <asp:Label ID="lblTotalAmount" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 150px">
                            </td>
                            <td style="font-size: 12pt; width: 5px; font-family: Times New Roman">
                            </td>
                            <td style="font-size: 12pt; font-family: Times New Roman">
                                <table style="width: 450px">
                                    <tr>
                                        <td style="width: 150px">
                                            <asp:Button ID="btnReturn" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Return To Page"
                                    Width="135px" /></td>
                                        <td style="width: 150px">
                                        </td>
                                        <td style="width: 150px">
                                            <asp:Button ID="btnCancel" runat="server" Font-Names="Verdana" Font-Size="Small"
                                    Text="Delete All" Width="85px" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="font-size: 12pt; font-family: Times New Roman; height: 89px;">
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
