Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Purchase_frmApplyNew
    Inherits System.Web.UI.Page
    Dim dsStation As New DataSet

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        Me.Page.MaintainScrollPositionOnPostBack = True


        If IsPostBack() = False Then
            'Session("ApplyNewID") = ""
            'hdfItemCount.Value = "0"

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            hdfWithBudgetList.Value = ""
            hdfWithOEB.Value = ""
            btnCancel.Visible = False
            btnSave.Enabled = False
            btnSend.Enabled = False
            btnDelete.Enabled = False
            btnQuoReceipt.Enabled = False
            gvQuoReceipt.Columns(0).Visible = False
            gvQuoReceipt.Columns(1).Visible = False

            If Session("CurrentDateTime") <> Nothing Then
                hdfCurrentDateTime.Value = Session("CurrentDateTime").ToString()
            End If

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "P0001")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If




            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Add New Purchase Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnSave.Enabled = True
                        btnQuoReceipt.Enabled = True
                        btnSend.Enabled = True
                        btnDelete.Enabled = True
                        gvQuoReceipt.Columns(0).Visible = True
                        gvQuoReceipt.Columns(1).Visible = True
                    Else
                        Session("blnWrite") = False
                        btnSave.Enabled = False
                        btnQuoReceipt.Enabled = False
                        btnSend.Enabled = False
                        btnDelete.Enabled = False
                        gvQuoReceipt.Columns(0).Visible = False
                        gvQuoReceipt.Columns(1).Visible = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If


            If Request.QueryString("ANT").ToString = "N" Then
                Session("DA_AssetID") = ""
                'Session("ApplyNewID") = ""
                lblType.Text = "Purchase"
                If Request.QueryString("ID").ToString = "0" Then
                    Session("ApplyNewID") = ""
                End If
            ElseIf Request.QueryString("ANT").ToString = "M" Then
                lblType.Text = "Maintenance"
                If Session("DA_MaintenanceID") <> Nothing Then
                    hdfMaintenanceID.Value = Session("DA_MaintenanceID").ToString
                Else
                    hdfMaintenanceID.Value = ""
                End If
                If Session("DA_AssetID") <> Nothing Then
                    hdfAssetID.Value = Session("DA_AssetID").ToString
                Else
                    hdfAssetID.Value = ""
                End If
                Session("DA_MaintenanceID") = ""
                Session("DA_AssetID") = ""
                If Request.QueryString("ID").ToString = "0" Then
                    Session("ApplyNewID") = ""
                End If
            End If


            BindDropDownList()
            ddlStation_SelectedIndexChanged(sender, e)
            Session("DA_USERLIST") = ""

            If Session("ApplyNewID") <> Nothing Then
                If Session("ApplyNewID").ToString <> "" Then
                    hdfApplyNewID.Value = Session("ApplyNewID").ToString
                    GetApplyNewData()
                    Session("ApplyNewID") = ""
                End If
            End If

            Me.btnSave.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnSave, "", "btnSend", "btnDelete", "btnQuoReceipt", "", ""))
            Me.btnSend.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnSend, "", "btnSave", "btnDelete", "btnQuoReceipt", "", ""))
            Me.btnDelete.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnDelete, "Are you sure want to Delete this Application?", "btnSave", "btnSend", "btnQuoReceipt", "", ""))

        Else
            If Session("DA_USERLIST") <> "" Then
                If Session("DA_USERLIST_TYPE") = "To" Then
                    If Session("DA_USERLIST").ToString <> "" Then
                        hdfTo.Value = Session("DA_USERLIST").ToString
                    End If
                    clsCF.ShowApplyTo(Session("DA_USERLIST").ToString, "To", txtTo, gvCC)
                    'ShowApplyTo(Session("DA_USERLIST").ToString, "To")
                ElseIf Session("DA_USERLIST_TYPE") = "CC" Then
                    If Session("DA_USERLIST").ToString <> "" Then
                        hdfCC.Value = Session("DA_USERLIST").ToString
                    End If
                    clsCF.ShowApplyTo(Session("DA_USERLIST").ToString, "CC", txtTo, gvCC)
                    'ShowApplyTo(Session("DA_USERLIST").ToString, "CC")
                End If
                Session("DA_USERLIST_TYPE") = ""
                Session("DA_USERLIST") = ""
            Else
                If Session("DA_USERLIST_TYPE") = "To" Then
                    If Session("DA_USERLIST").ToString <> "" Then
                        hdfTo.Value = Session("DA_USERLIST").ToString
                    End If
                    clsCF.ShowApplyTo(hdfTo.Value.ToString, "To", txtTo, gvCC)
                ElseIf Session("DA_USERLIST_TYPE") = "CC" Then
                    If Session("DA_USERLIST").ToString <> "" Then
                        hdfCC.Value = Session("DA_USERLIST").ToString
                    End If
                    clsCF.ShowApplyTo(hdfCC.Value.ToString, "CC", txtTo, gvCC)
                End If
                Session("DA_USERLIST_TYPE") = ""
                Session("DA_USERLIST") = ""
            End If
        End If

        'If Session("ApplyNewID") <> Nothing Then
        If Session("ApplyNewID").ToString <> "" Then
            hdfApplyNewID.Value = Session("ApplyNewID").ToString
            'Session("ApplyNewID") = ""
            GetApprovalParty()
            'GetApplyNewData()
        End If

        'If hdfApplyNewID.Value.ToString <> "" Then
        '    GetApplyNewData()
        'End If
        'End If

    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[AccessStation]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)


                'If MyData.Tables(0).Rows(0)("AccessStation").ToString <> "" Then
                '    dsStation = DIMERCO.SDK.Utilities.LSDK.getStationDataByWithList(MyData.Tables(0).Rows(0)("AccessStation").ToString)
                'Else
                '    dsStation = DIMERCO.SDK.Utilities.ReSM.GetStationList()
                'End If
                'Dim dtView As DataView = dsStation.Tables(0).DefaultView
                'dtView.Sort = "StationCode ASC"

                'ddlStation.DataSource = dtView
                'ddlStation.DataValueField = dtView.Table.Columns("StationID").ToString
                'ddlStation.DataTextField = dtView.Table.Columns("StationCode").ToString
                'ddlStation.DataBind()

                If Session("DA_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("DA_StationID").ToString

                    'GetApprovalParty_ByStation()
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    'Private Sub ShowApplyTo(ByVal strApplyList As String, ByVal strType As String)
    '    Try
    '        Dim MyData As New DataSet

    '        If strType = "To" Then
    '            MyData = DIMERCO.SDK.Utilities.LSDK.getUserDataByList(strApplyList)
    '            txtTo.Text = MyData.Tables(0).Rows(0)("Fullname").ToString
    '        End If

    '        If strType = "CC" Then
    '            MyData = DIMERCO.SDK.Utilities.LSDK.getUserDataByList(strApplyList)
    '            gvCC.DataSource = MyData
    '            gvCC.DataMember = MyData.Tables(0).TableName
    '            gvCC.DataBind()
    '        End If
    '    Catch ex As Exception
    '        Me.lblMsg.Text = "ShowApplyTo: " & ex.Message.ToString
    '    End Try
    'End Sub

    'Protected Sub btnTo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTo.Click

    '    Dim strScript As String

    '    If hdfTo.Value.ToString = "" Then
    '        Session("DA_USERLIST_TYPE") = "To"
    '        Session("DA_USERLIST") = ""
    '        Session("DA_New_USERLIST") = ""
    '        Session("DA_USERLIST_Name") = ""
    '    Else
    '        Session("DA_USERLIST_TYPE") = "To"
    '        Session("DA_USERLIST") = hdfTo.Value.ToString
    '        Session("DA_USERLIST_Name") = txtTo.Text.ToString
    '    End If

    '    Session("DA_AddUserStationID") = ""
    '    Session("DA_AddUserStationID") = Session("DA_StationID").ToString

    '    strScript = "<script language=javascript>"
    '    strScript += "theChild = window.open('../frmAddUser2.aspx?ParentForm=frmApplyNew','AddUser','height=550, width=550,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
    '    strScript += "</script>"
    '    Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)

    'End Sub

    Protected Sub lbtnCC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnCC.Click
        Dim strScript As String

        If hdfCC.Value.ToString = "" Then
            Session("DA_USERLIST_TYPE") = "CC"
            Session("DA_USERLIST") = ""
            Session("DA_New_USERLIST") = ""
        Else
            Session("DA_USERLIST_TYPE") = "CC"
            Session("DA_USERLIST") = hdfCC.Value.ToString
        End If

        Session("DA_AddUserStationID") = ""
        Session("DA_AddUserStationID") = Session("DA_StationID").ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmAddUser.aspx?ParentForm=frmApplyNew','AddUser','height=550, width=500,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Private Sub GetApplyNewQuoReceiptCompareData(ByVal strApplyNewID As String)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetApplyNewQuoReceiptCompareData")

                With MyCommand
                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = strApplyNewID
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "QuoReceiptData"


                gvQuoReceipt.DataSource = MyData
                gvQuoReceipt.DataMember = MyData.Tables(0).TableName
                gvQuoReceipt.DataBind()

                lblMsg.Text = ""


            Catch ex As Exception
                Me.lblMsg.Text = "GetApplyNewQuoReceiptCompareData: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnQuoReceipt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQuoReceipt.Click
        Dim strScript As String

        If hdfWithBudgetList.Value.ToString() = "0" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please keyin asset Budget List.')</script>")
            Exit Sub
        End If

        If hdfWithOEB.Value.ToString() = "0" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please keyin Operating Expenses Budget into Budget List.')</script>")
            Exit Sub
        End If


        'Dim strReturn As String
        'strReturn = SaveData("D")
        'If strReturn <> "" Then
        '    If strReturn <> "Save Successful" Then
        '        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strReturn + "')</script>")
        '        Exit Sub
        '    End If
        'End If

        Session("ApplyNewID") = ""
        Session("ApplyNewID") = hdfApplyNewID.Value.ToString
        Session("AN_StationID") = ""
        Session("AN_StationID") = ddlStation.SelectedValue.ToString
        Session("AN_To") = ""
        Session("AN_To") = ddlTo.SelectedValue.ToString() 'hdfTo.Value.ToString
        Session("AN_CC") = ""
        Session("AN_CC") = hdfCC.Value.ToString
        Session("AN_ApplyNewType") = ""
        Session("AN_ApplyNewType") = lblType.Text.ToString
        Session("AN_PurchaseDes") = ""
        Session("AN_PurchaseDes") = txtPurchaseDes.Text.ToString
        'Session("AN_Type") = ""
        'If chkCashCarry.Checked = True Then
        '    Session("AN_Type") = "Cash and Carry"
        'Else
        '    Session("AN_Type") = "Vendor"
        'End If
        Session("AN_Reason") = ""
        Session("AN_Reason") = txtReason.Text.ToString
        Session("AN_Remarks") = ""
        Session("AN_Remarks") = txtRemarks.Text.ToString

        Session("AN_AssetID") = ""
        Session("AN_AssetID") = hdfAssetID.Value.ToString

        Session("AN_MaintenanceID") = ""
        Session("AN_MaintenanceID") = hdfMaintenanceID.Value.ToString

        Session("AN_QuotationCompareID") = ""

        Session("CurrentDatetime") = hdfCurrentDateTime.Value.ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('frmQuotationReceipt.aspx?ParentForm=frmApplyNew','AddQuotationReceipt','height=700, width=1200,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)

        'Response.Redirect("frmQuotationReceipt.aspx")

    End Sub

    Protected Sub ddlStation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStation.SelectedIndexChanged
        If ddlStation.SelectedValue.ToString = "" Then
            lblMsg.Text = "Please select Station."
            Exit Sub
        End If

        Session("DA_StationID") = ddlStation.SelectedValue.ToString
        CheckStationBudgetList(ddlStation.SelectedValue.ToString())
        'GetApprovalParty_ByStation()
    End Sub

    Private Sub CheckStationBudgetList(ByVal strStation As String)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_CheckStationBudgetList")

                With MyCommand
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = strStation
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)

                    If hdfCurrentDateTime.Value.ToString <> "" Then
                        .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                    Else
                        .Parameters("@CurrentDate").Value = Format(DateTime.Now, "dd MMM yyyy hh:mm:ss").ToString
                    End If


                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "StationBudgetList"

                hdfWithBudgetList.Value = MyData.Tables(0).Rows(0)("CntAssetBudget").ToString()
                hdfWithOEB.Value = MyData.Tables(0).Rows(0)("CntOEB").ToString()

                If MyData.Tables(0).Rows(0)("CntAssetBudget").ToString() = "0" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please keyin current year Asset Budget List.')</script>")
                    Exit Sub
                End If

                If MyData.Tables(0).Rows(0)("CntOEB").ToString() = "0" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please keyin current year Operating Expenses Budget into Budget List.')</script>")
                    Exit Sub
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "CheckStationBudgetList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    'Private Sub GetApprovalParty_ByStation()
    '    Try
    '        Dim dsStation As New DataSet
    '        Dim MyData As New DataSet
    '        Dim MyAdapter As SqlDataAdapter
    '        Dim MyCommand As SqlCommand
    '        Dim MyConnection As New SqlConnection(strConnectionString)

    '        MyCommand = New SqlCommand("SP_GetApprovalParty_ByStation")

    '        With MyCommand

    '            .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
    '            .Parameters("@ReSM").Value = strReSM
    '            .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
    '            .Parameters("@ReferenceType").Value = "[ApprovalParty]"
    '            .Parameters.Add("@ApplyNewType", SqlDbType.VarChar, 50)
    '            .Parameters("@ApplyNewType").Value = lblType.Text.ToString

    '            .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
    '            .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
    '            .CommandType = CommandType.StoredProcedure
    '            .Connection = MyConnection
    '        End With

    '        MyAdapter = New SqlDataAdapter(MyCommand)
    '        MyAdapter.Fill(MyData)
    '        MyData.Tables(0).TableName = "ApprovalParty"

    '        ddlTo.DataSource = MyData.Tables(0)
    '        ddlTo.DataValueField = MyData.Tables(0).Columns("USERID").ToString
    '        ddlTo.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
    '        ddlTo.DataBind()


    '        txtTo.Text = ""
    '        hdfTo.Value = ""

    '        If MyData.Tables(0).Rows.Count > 0 Then
    '            'If MyData.Tables(0).Rows.Count = 1 Then
    '            '    txtTo.Text = MyData.Tables(0).Rows(0)("nvchFullname").ToString
    '            '    hdfTo.Value = MyData.Tables(0).Rows(0)("L1_ApprovalParty").ToString
    '            'End If
    '        Else
    '            lblMsg.Text = "No Configure Approval Party. Please contact system admin."
    '            Exit Sub
    '        End If

    '    Catch ex As Exception
    '        Me.lblMsg.Text = "ddlStation_SelectedIndexChanged: " & ex.Message.ToString
    '    Finally
    '        'sqlConn.Close()
    '    End Try
    'End Sub

    Protected Sub gvQuoReceipt_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvQuoReceipt.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim strQuotationCompareID As String
                Dim lbtnRemove As New LinkButton

                lblMsg.Text = ""

                lbtnRemove = gvQuoReceipt.Rows(e.RowIndex).FindControl("lbtnRemove")
                strQuotationCompareID = lbtnRemove.CommandArgument.ToString

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteQuotationCompare", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@QuotationCompareID", SqlDbType.VarChar, 20)
                    .Parameters("@QuotationCompareID").Value = strQuotationCompareID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    GetApplyNewQuoReceiptCompareData(hdfApplyNewID.Value.ToString)

                    'lblMsg.Text = "Delete Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")

                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvQuoReceipt_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        'DELETE APPLY NEW DATA
        DeleteApplyNewData()

    End Sub

    Private Sub DeleteApplyNewData()
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteApplyNewData", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = hdfApplyNewID.Value.ToString
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    lblMsg.Text = strMsg
                Else
                    lblMsg.Text = ""
                    hdfApplyNewID.Value = ""
                    ddlStation.SelectedValue = Session("DA_StationID").ToString
                    ddlTo.SelectedValue = ""
                    'txtTo.Text = ""
                    'hdfTo.Value = ""
                    gvCC.Dispose()
                    gvCC.Visible = False
                    hdfCC.Value = ""
                    txtPurchaseDes.Text = ""
                    gvQuoReceipt.Dispose()
                    gvQuoReceipt.Visible = False
                    gvDetail.Dispose()
                    gvDetail.Visible = False
                    txtReason.Text = ""
                    txtRemarks.Text = ""

                    hdfAssetID.Value = ""
                    hdfMaintenanceID.Value = ""
                    'lblMsg.Text = "Delete Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "DeleteApplyNewData: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'D - DRAFT
        Dim strReturn As String
        strReturn = SaveData("D")
        If strReturn <> "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strReturn + "')</script>")
        End If
    End Sub

    Private Function SaveData(ByVal strStatus As String) As String
        Using sqlConn As New SqlConnection(strConnectionString)
            Try

                Dim strMsg As String = ""
                Dim intMailID As Integer = 0
                Dim strApplyNewID As String = ""


                lblMsg.Text = ""
                SaveData = ""

                If ddlStation.SelectedValue.ToString = "" Then
                    'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Station')</script>")
                    ddlStation.Focus()
                    Return "Please select Station"
                    Exit Function
                End If



                If txtPurchaseDes.Text.ToString = "" Then
                    'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Purchase Description')</script>")
                    txtPurchaseDes.Focus()
                    Return "Please enter Purchase Description"
                    Exit Function
                End If



                If strStatus = "P" Then
                    If txtReason.Text.ToString = "" Then
                        'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Reason')</script>")
                        txtReason.Focus()
                        Return "Please enter Reason"
                        Exit Function
                    End If
                End If

                Dim strRemarks As String = ""

                strRemarks = txtRemarks.Text.Replace("<p>", "")
                strRemarks = strRemarks.Replace("</p>", "</br>")


                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveApplyNew", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    If hdfApplyNewID.Value.ToString = "" Then
                        .Parameters("@ApplyNewID").Value = ""
                    Else
                        .Parameters("@ApplyNewID").Value = hdfApplyNewID.Value.ToString
                    End If

                    .Parameters.Add("@To", SqlDbType.VarChar, 6)
                    '.Parameters("@To").Value = hdfTo.Value.ToString
                    .Parameters("@To").Value = ddlTo.SelectedValue.ToString()

                    .Parameters.Add("@CC", SqlDbType.NVarChar)
                    .Parameters("@CC").Value = hdfCC.Value.ToString

                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@StationCode", SqlDbType.NVarChar, 10)
                    .Parameters("@StationCode").Value = ddlStation.SelectedItem.Text.ToString

                    .Parameters.Add("@Purchase_Description", SqlDbType.NVarChar, 300)
                    .Parameters("@Purchase_Description").Value = txtPurchaseDes.Text.ToString
                    .Parameters.Add("@Remarks", SqlDbType.NVarChar)
                    .Parameters("@Remarks").Value = strRemarks
                    .Parameters.Add("@Reason", SqlDbType.NVarChar)
                    .Parameters("@Reason").Value = txtReason.Text.ToString
                    .Parameters.Add("@Status", SqlDbType.VarChar, 1)
                    .Parameters("@Status").Value = strStatus

                    .Parameters.Add("@ApplyNewType", SqlDbType.VarChar, 50)
                    .Parameters("@ApplyNewType").Value = lblType.Text.ToString

                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString

                    .Parameters.Add("@Issue", SqlDbType.NVarChar)
                    .Parameters("@Issue").Value = ""

                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = hdfAssetID.Value.ToString()

                    .Parameters.Add("@MaintenanceID", SqlDbType.VarChar, 20)
                    .Parameters("@MaintenanceID").Value = hdfMaintenanceID.Value.ToString()

                    .Parameters.Add("@ApplyNew_ID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNew_ID").Direction = ParameterDirection.Output
                    .Parameters.Add("@biMailID", SqlDbType.BigInt)
                    .Parameters("@biMailID").Direction = ParameterDirection.Output
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                    intMailID = CInt(.Parameters("@biMailID").Value.ToString())
                    strApplyNewID = .Parameters("@ApplyNew_ID").Value.ToString()
                End With

                If strMsg <> "" Then
                    'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                    Return strMsg
                Else
                    hdfApplyNewID.Value = strApplyNewID
                    If strStatus = "D" Then
                        'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                        Return "Save Successful"
                    Else
                        Dim strResult As String = ""
                        strResult = clsCF.SendMail_ApplyNew(intMailID, strApplyNewID)

                        If strResult = "" Then
                            clsCF.UpdateMailQueue(intMailID, 1, "Send Successful", hdfCurrentDateTime.Value.ToString)
                            GetApplyNewData()
                            'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Send Successful')</script>")
                            Return "Send Successful"
                        Else
                            clsCF.UpdateMailQueue(intMailID, 2, "Send Fail - " & strResult, hdfCurrentDateTime.Value.ToString)
                            'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Send Fail - " & strResult & "')</script>")
                            Return "Send Fail - " & strResult
                        End If
                    End If

                End If
            Catch ex As Exception
                Return ex.Message.ToString
                'Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Function

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        'P - PENDING
        'SaveData("P")
        Dim strReturn As String = ""
        strReturn = CheckApprovalPartyAndItemPurchase()
        If strReturn <> "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strReturn + "')</script>")
            Exit Sub
        End If


        strReturn = SaveData("P")
        If strReturn <> "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strReturn + "')</script>")
        End If
    End Sub

    Private Function CheckApprovalPartyAndItemPurchase() As String
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                CheckApprovalPartyAndItemPurchase = ""
                If hdfApplyNewID.Value.ToString = "" Then
                    Return ""
                    Exit Function
                End If

                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_CheckApprovalPartyAndItemPurchase")

                With MyCommand
                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = hdfApplyNewID.Value.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "ApprovalParty"

                hdfItemCount.Value = MyData.Tables(1).Rows(0)("ItemCount").ToString

                If hdfItemCount.Value.ToString = "0" Then
                    'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter purchase Item')</script>")
                    btnQuoReceipt.Focus()
                    Return "Please enter purchase Item"
                    Exit Function
                End If

                If ddlTo.Items.Count = 0 Then
                    ddlTo.DataSource = MyData.Tables(0)
                    ddlTo.DataValueField = MyData.Tables(0).Columns("L1_ApprovalParty").ToString
                    ddlTo.DataTextField = MyData.Tables(0).Columns("nvchFullname").ToString
                    ddlTo.DataBind()
                End If

                If MyData.Tables(0).Rows.Count = 1 Then
                    'txtTo.Text = MyData.Tables(0).Rows(0)("nvchFullname").ToString
                    'hdfTo.Value = MyData.Tables(0).Rows(0)("L1_ApprovalParty").ToString
                    ddlTo.SelectedValue = MyData.Tables(0).Rows(0)("L1_ApprovalParty").ToString
                ElseIf MyData.Tables(0).Rows.Count = 2 Then
                    'txtTo.Text = ddlTo.SelectedItem.Text.ToString()
                    'hdfTo.Value = ddlTo.SelectedValue.ToString()
                    'ddlTo.SelectedValue = MyData.Tables(0).Rows(0)("L1_ApprovalParty").ToString
                ElseIf MyData.Tables(0).Rows.Count = 0 Then
                    'txtTo.Text = ""
                    'hdfTo.Value = ""
                    Return "No Configure Approval Party. Please contact system admin"
                    Exit Function
                End If




                'If hdfItemCount.Value.ToString <> "0" Then
                '    If txtTo.Text.ToString = "" Then
                '        'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('No Configure Approval Party. Please contact system admin')</script>")
                '        Return "No Configure Approval Party. Please contact system admin"
                '        Exit Function
                '    End If
                'End If

                If ddlTo.SelectedValue.ToString = "" Then
                    Return "Please select Approval Party."
                    Exit Function
                End If



                If MyData.Tables(2).Rows.Count > 0 Then
                    If MyData.Tables(2).Rows(0)("TotalQRAmount") <> MyData.Tables(2).Rows(0)("TotalQRDAmount") Then
                        btnQuoReceipt.Focus()
                        Return "Invalid Quotation/Receipt Amount."
                        Exit Function
                    End If
                End If

                If MyData.Tables(3).Rows.Count > 0 Then
                    If MyData.Tables(3).Rows(0)("ItemType") = "IT" Then
                        If MyData.Tables(3).Rows(0)("WithUserDetail") = 0 Then
                            Return "Purchase item need to send eRequisition. Please enter asset User."
                            Exit Function
                        End If

                        If MyData.Tables(3).Rows(0)("WithBrand") = 0 Then
                            Return "Purchase item need to send eRequisition. Please enter/select Brand"
                            Exit Function
                        End If

                        If MyData.Tables(3).Rows(0)("WithModel") = 0 Then
                            Return "Purchase item need to send eRequisition. Please enter/select Model"
                            Exit Function
                        End If

                        If MyData.Tables(3).Rows(0)("WithSpecification") = 0 Then
                            Return "Purchase item need to send eRequisition. Please enter Specification"
                            Exit Function
                        End If

                        If MyData.Tables(3).Rows(0)("WithQTY_NotMatch") = 0 Then
                            Return "Invalid Quantity - Total User not match with Purchase Quantity."
                            Exit Function
                        End If
                    Else
                        Return ""
                    End If
                Else
                    Return ""
                End If



            Catch ex As Exception
                Return ex.Message.ToString
                'Me.lblMsg.Text = "CheckApprovalPartyAndItemPurchase: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using

    End Function

    Private Sub GetApplyNewData()
        If hdfApplyNewID.Value.ToString = "" Then
            Exit Sub
        End If

        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetApplyNewData")

                With MyCommand
                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = hdfApplyNewID.Value.ToString
                    '.Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    '.Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "ApplyNewData"
                MyData.Tables(1).TableName = "QuoReceipt"

                hdfApplyNewID.Value = MyData.Tables(0).Rows(0)("ApplyNewID").ToString
                ddlStation.SelectedValue = MyData.Tables(0).Rows(0)("StationID").ToString

                hdfCC.Value = MyData.Tables(0).Rows(0)("CC").ToString
                clsCF.ShowApplyTo(hdfCC.Value.ToString, "CC", txtTo, gvCC)

                lblType.Text = MyData.Tables(0).Rows(0)("ApplyNewType").ToString
                txtPurchaseDes.Text = MyData.Tables(0).Rows(0)("Purchase_Description").ToString

                txtReason.Text = MyData.Tables(0).Rows(0)("Reason").ToString
                txtRemarks.Text = MyData.Tables(0).Rows(0)("Remarks").ToString
                hdfMaintenanceID.Value = MyData.Tables(0).Rows(0)("MaintenanceID").ToString
                hdfAssetID.Value = MyData.Tables(0).Rows(0)("AssetID").ToString

                gvQuoReceipt.DataSource = MyData
                gvQuoReceipt.DataMember = MyData.Tables(1).TableName
                gvQuoReceipt.DataBind()

                gvDetail.Visible = False

                hdfItemCount.Value = MyData.Tables(4).Rows(0)("ItemCount").ToString

                'RESET APPROVAL PARTY BASED ON ITEM TYPE (IT / NON IT)
                'txtTo.Text = ""
                'hdfTo.Value = ""
                'If MyData.Tables(2).Rows.Count > 0 Then
                '    txtTo.Text = MyData.Tables(2).Rows(0)("nvchFullname").ToString
                '    hdfTo.Value = MyData.Tables(2).Rows(0)("L1_ApprovalParty").ToString
                'End If

                If MyData.Tables(2).Rows.Count > 0 Then
                    If ddlTo.Items.Count < 2 Then
                        ddlTo.DataSource = MyData.Tables(2)
                        ddlTo.DataValueField = MyData.Tables(2).Columns("L1_ApprovalParty").ToString
                        ddlTo.DataTextField = MyData.Tables(2).Columns("nvchFullname").ToString
                        ddlTo.DataBind()
                    End If
                    'ddlTo.SelectedValue = MyData.Tables(0).Rows(0)("L1_ApprovedBy_Name").ToString
                    If MyData.Tables(0).Rows(0)("L1_ApprovedBy").ToString <> "" Then
                        ddlTo.SelectedValue = MyData.Tables(0).Rows(0)("L1_ApprovedBy").ToString
                    End If
                End If


                Dim strStatus As String = MyData.Tables(0).Rows(0)("Status").ToString

                If strStatus = "D" Then
                    btnSave.Enabled = True
                    btnSend.Enabled = True
                    btnDelete.Enabled = True
                    btnQuoReceipt.Enabled = True
                    btnCancel.Visible = False
                ElseIf strStatus = "W" Then
                    btnSave.Enabled = True
                    btnSend.Enabled = True
                    btnDelete.Enabled = False
                    btnQuoReceipt.Enabled = True
                    btnCancel.Visible = True
                ElseIf strStatus = "P" Then
                    btnCancel.Visible = True
                    btnSave.Enabled = False
                    btnSend.Enabled = False
                    btnDelete.Enabled = False
                    btnQuoReceipt.Enabled = False
                Else ''Status = "A" or "R"
                    btnCancel.Visible = False
                    btnSave.Enabled = False
                    btnSend.Enabled = False
                    btnDelete.Enabled = False
                    btnQuoReceipt.Enabled = False
                End If

                gvApprovalHistory.Visible = True
                gvApprovalHistory.DataSource = MyData
                gvApprovalHistory.DataMember = MyData.Tables(3).TableName
                gvApprovalHistory.DataBind()


                If MyData.Tables(0).Rows(0)("CreatedBy").ToString <> Session("DA_UserID").ToString Then
                    If Session("blnModifyOthers") = False Then
                        btnSave.Enabled = False
                        btnSend.Enabled = False
                        btnDelete.Enabled = False
                        btnQuoReceipt.Enabled = False
                        gvQuoReceipt.Columns(1).Visible = False
                        btnCancel.Visible = False
                    Else

                        If strStatus = "D" Then
                            btnSave.Enabled = True
                            btnSend.Enabled = True
                            btnDelete.Enabled = True
                            btnQuoReceipt.Enabled = True
                            gvQuoReceipt.Columns(1).Visible = True
                            btnCancel.Visible = False
                        ElseIf strStatus = "W" Then
                            btnSave.Enabled = True
                            btnSend.Enabled = True
                            btnDelete.Enabled = False
                            btnQuoReceipt.Enabled = True
                            gvQuoReceipt.Columns(1).Visible = True
                            btnCancel.Visible = True
                        ElseIf strStatus = "P" Then
                            btnCancel.Visible = True
                            btnSave.Enabled = False
                            btnSend.Enabled = False
                            btnDelete.Enabled = False
                            btnQuoReceipt.Enabled = False
                            gvQuoReceipt.Columns(1).Visible = False
                        Else
                            btnCancel.Visible = False
                            btnSave.Enabled = False
                            btnSend.Enabled = False
                            btnDelete.Enabled = False
                            btnQuoReceipt.Enabled = False
                            gvQuoReceipt.Columns(1).Visible = False
                        End If
                    End If

                    If Session("blnDeleteOthers") = True Then
                        If strStatus = "D" Then
                            btnDelete.Enabled = True
                            btnQuoReceipt.Enabled = True
                            gvQuoReceipt.Columns(0).Visible = True
                            btnCancel.Visible = False
                        ElseIf strStatus = "W" Then
                            btnCancel.Visible = True
                            btnDelete.Enabled = False
                            btnQuoReceipt.Enabled = True
                            gvQuoReceipt.Columns(0).Visible = True
                        Else
                            btnCancel.Visible = False
                            btnDelete.Enabled = False
                            btnQuoReceipt.Enabled = False
                            gvQuoReceipt.Columns(0).Visible = False
                        End If
                    Else
                        btnCancel.Visible = False
                        btnDelete.Enabled = False
                        btnQuoReceipt.Enabled = False
                        gvQuoReceipt.Columns(0).Visible = False
                    End If
                End If


            Catch ex As Exception
                Me.lblMsg.Text = "GetApplyNewData: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub GetApprovalParty()
        If hdfApplyNewID.Value.ToString = "" Then
            Exit Sub
        End If

        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetApplyNewData")

                With MyCommand
                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = hdfApplyNewID.Value.ToString
                    '.Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    '.Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "ApplyNewData"
                MyData.Tables(1).TableName = "QuoReceipt"


                hdfApplyNewID.Value = MyData.Tables(0).Rows(0)("ApplyNewID").ToString
                ddlStation.SelectedValue = MyData.Tables(0).Rows(0)("StationID").ToString

                lblType.Text = MyData.Tables(0).Rows(0)("ApplyNewType").ToString
                hdfMaintenanceID.Value = MyData.Tables(0).Rows(0)("MaintenanceID").ToString
                hdfAssetID.Value = MyData.Tables(0).Rows(0)("AssetID").ToString

                gvQuoReceipt.DataSource = MyData
                gvQuoReceipt.DataMember = MyData.Tables(1).TableName
                gvQuoReceipt.DataBind()

                gvDetail.Visible = False

                hdfItemCount.Value = MyData.Tables(4).Rows(0)("ItemCount").ToString

                If MyData.Tables(2).Rows.Count > 0 Then
                    If ddlTo.Items.Count < 2 Then
                        ddlTo.DataSource = MyData.Tables(2)
                        ddlTo.DataValueField = MyData.Tables(2).Columns("L1_ApprovalParty").ToString
                        ddlTo.DataTextField = MyData.Tables(2).Columns("nvchFullname").ToString
                        ddlTo.DataBind()

                        If MyData.Tables(2).Rows.Count = 2 Then
                            ddlTo.SelectedIndex = 1
                        End If
                    End If
                    If MyData.Tables(0).Rows(0)("L1_ApprovedBy").ToString <> "" Then
                        ddlTo.SelectedValue = MyData.Tables(0).Rows(0)("L1_ApprovedBy").ToString
                    End If
                End If


                Dim strStatus As String = MyData.Tables(0).Rows(0)("Status").ToString

                If strStatus = "D" Then
                    btnSave.Enabled = True
                    btnSend.Enabled = True
                    btnDelete.Enabled = True
                    btnQuoReceipt.Enabled = True
                    btnCancel.Visible = False
                ElseIf strStatus = "W" Then
                    btnSave.Enabled = True
                    btnSend.Enabled = True
                    btnDelete.Enabled = False
                    btnQuoReceipt.Enabled = True
                    btnCancel.Visible = True
                ElseIf strStatus = "P" Then
                    btnCancel.Visible = True
                    btnSave.Enabled = False
                    btnSend.Enabled = False
                    btnDelete.Enabled = False
                    btnQuoReceipt.Enabled = False
                Else ''Status = "A" or "R"
                    btnCancel.Visible = False
                    btnSave.Enabled = False
                    btnSend.Enabled = False
                    btnDelete.Enabled = False
                    btnQuoReceipt.Enabled = False
                End If

                gvApprovalHistory.Visible = True
                gvApprovalHistory.DataSource = MyData
                gvApprovalHistory.DataMember = MyData.Tables(3).TableName
                gvApprovalHistory.DataBind()


                If MyData.Tables(0).Rows(0)("CreatedBy").ToString <> Session("DA_UserID").ToString Then
                    If Session("blnModifyOthers") = False Then
                        btnSave.Enabled = False
                        btnSend.Enabled = False
                        btnDelete.Enabled = False
                        btnQuoReceipt.Enabled = False
                        gvQuoReceipt.Columns(1).Visible = False
                        btnCancel.Visible = False
                    Else


                        If strStatus = "D" Then
                            btnSave.Enabled = True
                            btnSend.Enabled = True
                            btnDelete.Enabled = True
                            btnQuoReceipt.Enabled = True
                            gvQuoReceipt.Columns(1).Visible = True
                            btnCancel.Visible = False
                        ElseIf strStatus = "W" Then
                            btnSave.Enabled = True
                            btnSend.Enabled = True
                            btnDelete.Enabled = False
                            btnQuoReceipt.Enabled = True
                            gvQuoReceipt.Columns(1).Visible = True
                            btnCancel.Visible = True
                        ElseIf strStatus = "P" Then
                            btnCancel.Visible = True
                            btnSave.Enabled = False
                            btnSend.Enabled = False
                            btnDelete.Enabled = False
                            btnQuoReceipt.Enabled = False
                            gvQuoReceipt.Columns(1).Visible = False
                        Else
                            btnCancel.Visible = False
                            btnSave.Enabled = False
                            btnSend.Enabled = False
                            btnDelete.Enabled = False
                            btnQuoReceipt.Enabled = False
                            gvQuoReceipt.Columns(1).Visible = False
                        End If
                    End If

                    If Session("blnDeleteOthers") = True Then


                        If strStatus = "D" Then
                            btnDelete.Enabled = True
                            btnQuoReceipt.Enabled = True
                            gvQuoReceipt.Columns(0).Visible = True
                        ElseIf strStatus = "W" Then
                            btnCancel.Visible = True
                            btnDelete.Enabled = False
                            btnQuoReceipt.Enabled = True
                            gvQuoReceipt.Columns(0).Visible = True
                        Else
                            btnCancel.Visible = False
                            btnDelete.Enabled = False
                            btnQuoReceipt.Enabled = False
                            gvQuoReceipt.Columns(0).Visible = False
                        End If
                    Else
                        btnCancel.Visible = False
                        btnDelete.Enabled = False
                        btnQuoReceipt.Enabled = False
                        gvQuoReceipt.Columns(0).Visible = False
                    End If
                End If


            Catch ex As Exception
                Me.lblMsg.Text = "GetApprovalParty: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub gvQuoReceipt_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvQuoReceipt.SelectedIndexChanged
        'Dim strQuotationCompareID As String = ""
        'strQuotationCompareID = gvQuoReceipt.SelectedDataKey.Value.ToString


    End Sub

    Protected Sub gvCC_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvCC.SelectedIndexChanged
        Dim strCC As String
        Dim strRemove As String = ""

        strRemove = gvCC.SelectedValue.ToString

        strCC = hdfCC.Value.ToString.ToUpper()
        strCC = strCC.Replace(strRemove.ToUpper(), "").ToString
        strCC = strCC.Replace(",,", ",").ToString
        hdfCC.Value = strCC
        Session("DA_USERLIST_TYPE") = "CC"
        Session("DA_New_USERLIST") = strCC
        Session("DA_USERLIST") = strCC
        clsCF.ShowApplyTo(strCC, "CC", txtTo, gvCC)
        'ShowApplyTo(strCC, "CC")
    End Sub

    Protected Sub gvQuoReceipt_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvQuoReceipt.RowCommand
        Dim strQuotationCompareID As String
        strQuotationCompareID = e.CommandArgument.ToString()

        If e.CommandName = "SelectEdit" Then

            If hdfWithBudgetList.Value.ToString() = "0" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please keyin asset Budget List.')</script>")
                Exit Sub
            End If

            If hdfWithOEB.Value.ToString() = "0" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please keyin Operating Expenses Budget into Budget List.')</script>")
                Exit Sub
            End If

            Dim strScript As String
            Session("ApplyNewID") = ""
            Session("ApplyNewID") = hdfApplyNewID.Value.ToString
            Session("AN_StationID") = ""
            Session("AN_StationID") = ddlStation.SelectedValue.ToString
            Session("AN_To") = ""
            Session("AN_To") = hdfTo.Value.ToString
            Session("AN_CC") = ""
            Session("AN_CC") = hdfCC.Value.ToString
            Session("AN_ApplyNewType") = ""
            Session("AN_ApplyNewType") = lblType.Text.ToString
            Session("AN_PurchaseDes") = ""
            Session("AN_PurchaseDes") = txtPurchaseDes.Text.ToString
            Session("AN_Reason") = ""
            Session("AN_Reason") = txtReason.Text.ToString
            Session("AN_Remarks") = ""
            Session("AN_Remarks") = txtRemarks.Text.ToString
            Session("AN_QuotationCompareID") = strQuotationCompareID

            Session("AN_AssetID") = ""
            Session("AN_AssetID") = hdfAssetID.Value.ToString

            Session("AN_MaintenanceID") = ""
            Session("AN_MaintenanceID") = hdfMaintenanceID.Value.ToString

            Session("CurrentDatetime") = hdfCurrentDateTime.Value.ToString

            strScript = "<script language=javascript>"
            strScript += "theChild = window.open('frmQuotationReceipt.aspx?ParentForm=frmApplyNew','AddQuotationReceipt','height=750, width=1200,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
            strScript += "</script>"
            Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
        ElseIf e.CommandName = "View" Then
            GetQuoReceiptDetail(strQuotationCompareID)
        End If

    End Sub

    Private Sub GetQuoReceiptDetail(ByVal strQuotationCompareID As String)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetQuoReceiptDetail")

                With MyCommand
                    .Parameters.Add("@QuotationCompareID", SqlDbType.VarChar, 20)
                    .Parameters("@QuotationCompareID").Value = strQuotationCompareID
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "QuoReceiptDetail"

                If MyData.Tables(0).Rows.Count = 0 Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('No Quotation/Receipt Detail')</script>")
                    Exit Sub
                End If

                gvDetail.Visible = True
                gvDetail.DataSource = MyData
                gvDetail.DataMember = MyData.Tables(0).TableName
                gvDetail.DataBind()

                If MyData.Tables(0).Rows.Count > 0 Then
                    gvDetail.Caption = "Compare ID: " + MyData.Tables(0).Rows(0)("CompareID").ToString
                End If
                lblMsg.Text = ""


            Catch ex As Exception
                Me.lblMsg.Text = "GetQuoReceiptDetail: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim intMailID As Integer

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_CancelApplyNewPurchase", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = hdfApplyNewID.Value.ToString
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@biMailID", SqlDbType.BigInt)
                    .Parameters("@biMailID").Direction = ParameterDirection.Output
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                    intMailID = CInt(.Parameters("@biMailID").Value.ToString())
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    Dim strResult As String = ""
                    strResult = clsCF.SendMail_ApplyNew(intMailID, hdfApplyNewID.Value.ToString())

                    If strResult = "" Then
                        clsCF.UpdateMailQueue(intMailID, 1, "Cancel Successful", hdfCurrentDateTime.Value.ToString)
                        GetApplyNewData()
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Cancel Successful')</script>")
                    Else
                        clsCF.UpdateMailQueue(intMailID, 2, "Send Fail - " & strResult, hdfCurrentDateTime.Value.ToString)
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Cancel Fail - " & strResult & "')</script>")
                    End If
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnCancel_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub
End Class
