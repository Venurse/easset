Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Purchase_frmPurchaseApproval
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        Me.Page.MaintainScrollPositionOnPostBack = True

        If IsPostBack() = False Then
            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            btnApprove.Enabled = False
            btnReject.Enabled = False
            btnModificationReq.Enabled = False


            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "P0003")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Purchase Approval Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnApprove.Enabled = True
                        btnReject.Enabled = True
                        btnModificationReq.Enabled = True

                    Else
                        Session("blnWrite") = False
                        btnApprove.Enabled = False
                        btnReject.Enabled = False
                        btnModificationReq.Enabled = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            GetPurchaseApprovalList()

            Me.btnApprove.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnApprove, "", "btnReject", "btnModificationReq", "", "", ""))
            Me.btnReject.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnReject, "", "btnApprove", "btnModificationReq", "", "", ""))
            Me.btnModificationReq.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnModificationReq, "", "btnApprove", "btnReject", "", "", ""))

        End If
    End Sub

    Private Sub GetPurchaseApprovalList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetPurchaseApprovalList")

                With MyCommand
                    .Parameters.Add("@ApprovalParty", SqlDbType.VarChar, 6)
                    .Parameters("@ApprovalParty").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "PurchaseApprovalList"

                gvApplyNewList.DataSource = MyData
                gvApplyNewList.DataMember = MyData.Tables(0).TableName
                gvApplyNewList.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "GetPurchaseApprovalList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub rbtnSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        deselect_RB_in_gridview()
        'deselect radiobutton1
        'RadioButton1.Checked = False
        'check the radiobutton which is checked
        Dim SenderRB As RadioButton = sender
        SenderRB.Checked = True

        ShowDetail()
        '--------------------------------------
        'Reflect the event
        '---------------------------------------
        'fire_visible_window()
    End Sub

    Sub ShowDetail()
        Dim gvr As GridViewRow
        Dim i As Int32
        For Each gvr In gvHeader.Rows
            Dim lbtnView As LinkButton
            lbtnView = CType(gvHeader.Rows(i).FindControl("lbtnView"), LinkButton)

            Dim rb As RadioButton
            rb = CType(gvHeader.Rows(i).FindControl("rbtnSelect"), RadioButton)
            If rb.Checked = True Then
                hdfQuotationCompareID.Value = lbtnView.CommandArgument.ToString()
                GetQuoReceiptDetail(lbtnView.CommandArgument.ToString())
                Exit Sub
            End If
            i += 1
        Next
    End Sub

    Sub deselect_RB_in_gridview()
        Dim gvr As GridViewRow
        Dim i As Int32
        hdfQuotationCompareID.Value = ""
        'deselect all radiobutton in gridview
        For Each gvr In gvHeader.Rows
            Dim rb As RadioButton
            rb = CType(gvHeader.Rows(i).FindControl("rbtnSelect"), RadioButton)
            rb.Checked = False
            i += 1
        Next
    End Sub

    Protected Sub gvApplyNewList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvApplyNewList.SelectedIndexChanged
        Dim strApplyNewID As String = gvApplyNewList.SelectedValue.ToString()
        Dim hdfApprovalLevel As New HiddenField

        ClearField()

        hdfApprovalLevel = gvApplyNewList.SelectedRow.FindControl("hdfApprovalLevel")

        hdfLevel.Value = hdfApprovalLevel.Value.ToString

        GetApprovalSelected_ApplyNewData(strApplyNewID)
    End Sub

    Private Sub ClearField()
        lblPurchaseNo.Text = ""
        lblPurchaseDesc.Text = ""
        lblCreatedBy.Text = ""
        lblStation.Text = ""
        lblPurchaseReason.Text = ""
        lblType.Text = ""
        lblRemarks.Text = ""
        txtApprovalReason.Text = ""
        txtRemarks.Text = ""
        gvHeader.Visible = False
        gvDetail.Visible = False
        gvApprovalHistory.Visible = False

    End Sub

    Private Sub GetApprovalSelected_ApplyNewData(ByVal strApplyNewID As String)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetApprovalSelected_ApplyNewData")

                With MyCommand
                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = strApplyNewID
                    .Parameters.Add("@ApprovalLevel", SqlDbType.VarChar, 5)
                    .Parameters("@ApprovalLevel").Value = hdfLevel.Value.ToString
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "ApplyNewData"
                MyData.Tables(1).TableName = "QuoReceipt"

                lblPurchaseNo.Text = MyData.Tables(0).Rows(0)("ApplyNewID").ToString
                lblPurchaseDesc.Text = MyData.Tables(0).Rows(0)("Purchase_Description").ToString
                lblCreatedBy.Text = MyData.Tables(0).Rows(0)("CreatedBy_Name").ToString
                lblStation.Text = MyData.Tables(0).Rows(0)("StationCode").ToString
                lblPurchaseReason.Text = MyData.Tables(0).Rows(0)("Reason").ToString
                lblType.Text = MyData.Tables(0).Rows(0)("ApplyNewType").ToString
                lblRemarks.Text = MyData.Tables(0).Rows(0)("Remarks").ToString


                gvHeader.Visible = True
                gvHeader.DataSource = MyData
                gvHeader.DataMember = MyData.Tables(1).TableName
                gvHeader.DataBind()
                If gvHeader.Rows.Count = 1 Then
                    Dim rbtnSelect As New RadioButton
                    rbtnSelect = gvHeader.Rows(0).FindControl("rbtnSelect")
                    rbtnSelect.Checked = True
                    ShowDetail()
                End If

                gvApprovalHistory.Visible = True
                gvApprovalHistory.DataSource = MyData
                gvApprovalHistory.DataMember = MyData.Tables(2).TableName
                gvApprovalHistory.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "GetApprovalSelected_ApplyNewData: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub GetQuoReceiptDetail(ByVal strQuotationCompareID As String)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetQuoReceiptDetail_Approval")

                With MyCommand
                    .Parameters.Add("@QuotationCompareID", SqlDbType.VarChar, 20)
                    .Parameters("@QuotationCompareID").Value = strQuotationCompareID
                    .Parameters.Add("@ApprovalLevel", SqlDbType.VarChar, 5)
                    .Parameters("@ApprovalLevel").Value = hdfLevel.Value.ToString
                    .Parameters.Add("@ApprovalParty", SqlDbType.VarChar, 6)
                    .Parameters("@ApprovalParty").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "QuoReceiptDetail"


                gvDetail.Visible = True
                gvDetail.DataSource = MyData
                gvDetail.DataMember = MyData.Tables(0).TableName
                gvDetail.DataBind()

                lblMsg.Text = ""


            Catch ex As Exception
                Me.lblMsg.Text = "GetQuoReceiptDetail: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        If hdfQuotationCompareID.Value.ToString = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Quotation Group to approval')</script>")
            Exit Sub
        End If
        'A - APPROVED
        PurchaseApproval("A")
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        If txtApprovalReason.Text.ToString = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please provide Reason')</script>")
            Exit Sub
        End If
        'R - REJECTED
        PurchaseApproval("R")
    End Sub

    Protected Sub btnModificationReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificationReq.Click
        If txtApprovalReason.Text.ToString = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please provide Reason')</script>")
            Exit Sub
        End If
        'W - WAIT FOR REVISED - REQUEST MODIFY
        PurchaseApproval("W")
    End Sub

    Private Sub PurchaseApproval(ByVal strStatus As String)
        Using sqlConn As New SqlConnection(strConnectionString)
            Try

                Dim strMsg As String = ""
                Dim strMailID As String = ""
                Dim strApplyNewID As String = ""
                Dim strQuotationReceiptID_Category_ID As String = ""
                Dim i As Integer

                lblMsg.Text = ""

                Dim hdfQRCategoryID As New HiddenField
                Dim chkSelect As New CheckBox

                For i = 0 To gvDetail.Rows.Count - 1

                    hdfQRCategoryID = gvDetail.Rows(i).FindControl("hdfQRCategoryID")
                    chkSelect = gvDetail.Rows(i).FindControl("chkSelect")
                    If strStatus = "A" Or strStatus = "W" Then
                        If chkSelect.Checked = True Then
                            If strQuotationReceiptID_Category_ID = "" Then
                                strQuotationReceiptID_Category_ID = hdfQRCategoryID.Value.ToString + "|"
                            Else
                                strQuotationReceiptID_Category_ID = strQuotationReceiptID_Category_ID + hdfQRCategoryID.Value.ToString + "|"
                            End If
                        End If
                    Else
                        If strQuotationReceiptID_Category_ID = "" Then
                            strQuotationReceiptID_Category_ID = hdfQRCategoryID.Value.ToString + "|"
                        Else
                            strQuotationReceiptID_Category_ID = strQuotationReceiptID_Category_ID + hdfQRCategoryID.Value.ToString + "|"
                        End If
                    End If

                Next

                If strQuotationReceiptID_Category_ID = "" Then
                    If strStatus <> "R" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select item for approval')</script>")
                        Exit Sub
                    End If
                End If

                'Exit Sub

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_PurchaseApproval", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = lblPurchaseNo.Text.ToString

                    .Parameters.Add("@QuotationCompareID", SqlDbType.VarChar, 20)
                    .Parameters("@QuotationCompareID").Value = hdfQuotationCompareID.Value.ToString
                    .Parameters.Add("@QuotationReceiptID_Category_ID", SqlDbType.NVarChar)
                    .Parameters("@QuotationReceiptID_Category_ID").Value = strQuotationReceiptID_Category_ID


                    .Parameters.Add("@Reason", SqlDbType.NVarChar)
                    .Parameters("@Reason").Value = txtApprovalReason.Text.ToString
                    .Parameters.Add("@Remarks", SqlDbType.NVarChar)
                    .Parameters("@Remarks").Value = txtRemarks.Text.ToString

                    .Parameters.Add("@Status", SqlDbType.VarChar, 1)
                    .Parameters("@Status").Value = strStatus

                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString

                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@ApprovalLevel", SqlDbType.VarChar, 5)
                    .Parameters("@ApprovalLevel").Value = hdfLevel.Value.ToString()


                    .Parameters.Add("@biMailID", SqlDbType.NVarChar, 500)
                    .Parameters("@biMailID").Direction = ParameterDirection.Output
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                    strMailID = .Parameters("@biMailID").Value.ToString()
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    Dim strResult As String = ""

                    Dim MailID As String() = strMailID.Split(New Char() {","c})
                    Dim intMailID As Integer
                    Dim strStatusMsg As String = ""

                    For Each intMailID In MailID
                        strResult = clsCF.SendMail(intMailID)

                        If strResult = "" Then
                            If strStatus = "A" Then
                                strStatusMsg = "Approved."
                            ElseIf strStatus = "R" Then
                                strStatusMsg = "Rejected."
                            Else
                                strStatusMsg = "Sent for Request Modification."
                            End If
                            clsCF.UpdateMailQueue(intMailID, 1, strStatusMsg, hdfCurrentDateTime.Value.ToString)
                            strStatusMsg = strStatusMsg + " Successful"
                        Else
                            strStatusMsg = "Send Fail - " & strResult
                            clsCF.UpdateMailQueue(intMailID, 2, lblMsg.Text.ToString, hdfCurrentDateTime.Value.ToString)
                        End If
                    Next

                    ClearField()
                    GetPurchaseApprovalList()

                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strStatusMsg + "');</script>")

                End If
            Catch ex As Exception
                Me.lblMsg.Text = "PurchaseApproval: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

End Class
