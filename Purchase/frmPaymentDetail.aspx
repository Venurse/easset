<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmPaymentDetail.aspx.vb" Inherits="Purchase_frmPaymentDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Payment Detail</title>
    <script type="text/javascript">
         function showDate() 
        {
            var month_names = new Array("Jan", "Feb", "Mar", 
            "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
            "Oct", "Nov", "Dec");

            var d = new Date();
            var curr_day = d.getDay();
            var curr_date = d.getDate();
            var hours = d.getHours();
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            
            var curr_hours;
            var curr_minutes;
            var curr_seconds;

            if(hours<10)
            {
                curr_hours = "0" + hours;
            }
            else
            {
                curr_hours = hours;
            }
            
            if(minutes < 10)
            {
                curr_minutes = "0" + minutes;
            }
             else
            {
                curr_minutes = minutes;
            }
            
           if(seconds < 10)
            {
                curr_seconds = "0" + seconds;
            }
             else
            {
                curr_seconds = seconds;
            }

            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
 
            document.getElementById("hdfCurrentDateTime").value = curr_date + " " +  month_names[curr_month] +  " " + curr_year + " " + curr_hours + ":" + curr_minutes + ":" + curr_seconds   
         }
         
         function getUrlParams() 
         {  
            var params = {};  
            window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(str,key,value) {    params[key] = value;  });   
            return params;
         }
         
         
//        window.onunload = refreshParent; 
//        function refreshParent() 
//        { 
//            var params = getUrlParams();

//            if (params.ParentForm == "frmPurchaseDetail")
//            {   
//                window.opener.document.frmPurchaseDetail.submit();
//            }
//            else if (params.ParentForm  == "frmPayment")
//           {
//                window.opener.document.frmPayment.submit();
//           }


           
//            window.opener.document.forms[0].submit();
//            window.opener.document.forms[1].submit();
//            
//         var queryString = window.location.search.substring(1); 

//            var qrStr = window.location.search; 
//            var spQrStr = qrStr.substring(1); 
//            var arrQrStr = new Array(); 
//            var arr = spQrStr.split('&'); 
//         
//         
//            for (var i=0;i<arr.length;i++)
//            { 
//                var queryvalue = arr[i].split('='); 
//            }

            
         }
//         
////         1.function gup( name )2.{3.  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");4.  var regexS = "[\\?&]"+name+"=([^&#]*)";5.  var regex = new RegExp( regexS );6.  var results = regex.exec( window.location.href );7.  if( results == null )8.    return "";9.  else10.    return results[1];11.}
     </script>
</head>
<body>
    <form id="frmPaymentDetail" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Payment Detail"></asp:Label></td>
                <td style="font-size: 12pt; font-family: Times New Roman">
                </td>
            </tr>
            <tr style="font-size: 12pt; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td style="color: #000000">
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Station"></asp:Label></td>
                            <td style="font-size: 12pt; width: 1px; font-family: Times New Roman">
                                :</td>
                            <td style="font-size: 12pt; font-family: Times New Roman">
                                <asp:Label ID="lblStation" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label><br />
                                <asp:HiddenField ID="hdfCurrentDateTime" runat="server" /><asp:HiddenField ID="hdfStationID" runat="server" />
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Payment ID"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="lblPaymentID" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Vendor/Payee"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="lblVendorPayee" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label6" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Payment Type"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="lblPaymentType" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label11" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Payment Date"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="lblPaymentDate" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label7" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Voucher No"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="lblVoucherNo" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label10" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Total Amount"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="lblAmount" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label12" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="File"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="lblFile" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Created By"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="lblCreatedBy" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label13" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Status"></asp:Label></td>
                            <td>
                                <asp:Label ID="Label14" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text=":"
                                    Width="1px"></asp:Label></td>
                            <td>
                                <asp:Label ID="lblStatus" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label9" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Reason"></asp:Label></td>
                            <td>
                                <asp:Label ID="Label15" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text=":"
                                    Width="1px"></asp:Label></td>
                            <td>
                                <asp:Label ID="lblReason" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label8" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Invoice/Receipt"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:GridView ID="gvInvReceipt" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label16" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Reason"></asp:Label></td>
                            <td>
                                <asp:Label ID="Label17" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text=":"
                                    Width="1px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtReason" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    MaxLength="50" Width="350px"></asp:TextBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <table style="width: 450px">
                                    <tr>
                                        <td style="width: 150px">
                                            <asp:Button ID="btnVoid" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Void"
                                                Width="85px" OnClientClick="showDate();" /></td>
                                        <td style="width: 150px"><asp:Button ID="btnClose" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Close"
                                                Width="85px" /></td>
                                        <td style="width: 150px">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="font-size: 12pt; font-family: Times New Roman">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="8">
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
