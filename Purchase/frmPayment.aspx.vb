Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings
Partial Class Purchase_frmPayment
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Public Shared hifSupDocPayment As ArrayList = New ArrayList()
    Dim strFileExtension As String = ConfigurationSettings.AppSettings("DocExtension")
    Dim intFileSizeLimitKB As Integer = ConfigurationSettings.AppSettings("intFileSizeLimitKB")
    Dim intFileSizeLimit As Integer = ConfigurationSettings.AppSettings("intFileSizeLimit")

    Dim dsInv As New DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack() = False Then
            hdfApplyNewID.Value = ""
            hdfPayee.Value = ""
            txtPayee.Text = ""
            hdfPayeeType.Value = ""

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            'gvFile.Columns(0).Visible = False
            btnSave.Enabled = False
            gvPayment.Columns(0).Visible = False


            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "P0008")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Payment Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnSave.Enabled = True
                        gvPayment.Columns(0).Visible = True
                    Else
                        Session("blnWrite") = False
                        btnSave.Enabled = False
                        gvPayment.Columns(0).Visible = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            If Session("ApplyNewID") <> Nothing Then
                If Session("ApplyNewID").ToString <> "" Then
                    hdfApplyNewID.Value = Session("ApplyNewID").ToString
                    txtPurchaseNo.Text = hdfApplyNewID.Value.ToString
                    txtPurchaseNo.Enabled = False
                    btnSearch.Visible = False
                    Session("ApplyNewID") = ""
                Else
                    txtPurchaseNo.Enabled = True
                    btnSearch.Visible = True
                End If
            Else
                txtPurchaseNo.Enabled = True
                btnSearch.Visible = True
            End If

            

            BindDropDownList()

            If Session("PaymentStationID") <> Nothing Then
                ddlStation.SelectedValue = Session("PaymentStationID").ToString
                Session("PaymentStationID") = ""
            End If

            If Session("PaymentID") <> Nothing Then
                hdfPaymentID.Value = Session("PaymentID").ToString
                Session("PaymentID") = ""
            End If

            If hdfApplyNewID.Value.ToString <> "" Then
                GetPayment(hdfApplyNewID.Value.ToString, "", "")
            End If

            GetInvReceiptForPayment()
            GetCheckIs_InChargeParty()
        Else

            If Session("DA_PayeeName") <> Nothing Then
                txtPayee.Text = Session("DA_PayeeName").ToString
                Session("DA_PayeeName") = ""
            End If
            If Session("DA_PayeeType") <> Nothing Then
                hdfPayeeType.Value = Session("DA_PayeeType").ToString
                Session("DA_PayeeType") = ""
            End If

            If Session("DA_PayeeID") <> Nothing Then
                hdfPayee.Value = Session("DA_PayeeID").ToString
                Session("DA_PayeeID") = ""
                'GetPayment(txtPurchaseNo.Text.ToString, "", hdfPayee.Value.ToString)
                'GetInvReceiptForPayment()

                If hdfApplyNewID.Value.ToString <> "" Then
                    GetPayment(hdfApplyNewID.Value.ToString, "", "")
                Else
                    GetPayment(hdfApplyNewID.Value.ToString, "", hdfPayee.Value.ToString)
                End If
                GetInvReceiptForPayment()
            End If


            'If hdfApplyNewID.Value.ToString <> "" Then
            '    GetPayment(hdfApplyNewID.Value.ToString, "", "")
            'Else
            '    GetPayment(hdfApplyNewID.Value.ToString, hdfPaymentID.Value.ToString, "")
            'End If

            'GetInvReceiptForPayment()

            End If
    End Sub

    Private Sub GetCheckIs_InChargeParty()
        Dim dsInChargeParty As New DataSet
        dsInChargeParty = clsCF.GetCheckIs_InChargeParty(Session("DA_UserID").ToString, ddlStation.SelectedValue.ToString)

        If dsInChargeParty.Tables.Count > 0 Then
            If dsInChargeParty.Tables(0).Rows(0)("Issue_Payment").ToString <> 0 Then
                btnSave.Enabled = True
            Else
                btnSave.Enabled = False
            End If
        End If
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[AccessStation],[PaymentType],[ImageDocPath_Temp],[ImageDocPath_Temp_Open]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"
                MyData.Tables(1).TableName = "Year"

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)

                If Session("DA_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("DA_StationID").ToString
                End If

                ddlPaymentType.DataSource = MyData.Tables(1)
                ddlPaymentType.DataValueField = MyData.Tables(1).Columns("PaymentTypeID").ToString
                ddlPaymentType.DataTextField = MyData.Tables(1).Columns("PaymentType").ToString
                ddlPaymentType.DataBind()

                If MyData.Tables(2).Rows.Count > 0 Then
                    hdfImageDocPath_Temp.Value = MyData.Tables(2).Rows(0)("ImageDocPath_Temp").ToString
                Else
                    hdfImageDocPath_Temp.Value = ""
                End If

                If MyData.Tables(3).Rows.Count > 0 Then
                    hdfImageDocPath_Temp_Open.Value = MyData.Tables(3).Rows(0)("ImageDocPath_Temp").ToString
                Else
                    hdfImageDocPath_Temp_Open.Value = ""
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnVendor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVendor.Click
        Dim strScript As String

        Session("DA_PayeeID") = Nothing
        Session("DA_PayeeCode") = Nothing
        Session("DA_PayeeName") = Nothing

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmPayee.aspx?ParentForm=frmPayment','Vendor_Payee','height=750, width=700,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        'GET PURCHASE DETAIL

        If ddlStation.SelectedValue.ToString = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Station')</script>")
            Exit Sub
        End If

        'If txtPurchaseNo.Text.ToString = "" Then
        '    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Purchase No')</script>")
        '    Exit Sub
        'End If

        GetPayment(txtPurchaseNo.Text.ToString, "", "")

        GetInvReceiptForPayment()

        'Try
        '    Dim dsInvoiceReceipt As New DataSet
        '    Dim MyData As New DataSet
        '    Dim MyAdapter As SqlDataAdapter
        '    Dim MyCommand As SqlCommand
        '    Dim MyConnection As New SqlConnection(strConnectionString)

        '    MyCommand = New SqlCommand("SP_GetVendorPayee_InvReceipt_ByPurchaseNo")

        '    With MyCommand

        '        .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
        '        .Parameters("@ApplyNewID").Value = txtPurchaseNo.Text.ToString
        '        .Parameters.Add("@StationID", SqlDbType.VarChar, 20)
        '        .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
        '        .CommandType = CommandType.StoredProcedure
        '        .Connection = MyConnection
        '    End With

        '    MyAdapter = New SqlDataAdapter(MyCommand)
        '    MyAdapter.Fill(MyData)
        '    MyData.Tables(0).TableName = "VENDORPAYEE"

        '    If MyData.Tables(0).Rows.Count > 1 Then
        '        ddlVendorPayee.Visible = True
        '        ddlVendorPayee.DataSource = MyData.Tables(0)
        '        ddlVendorPayee.DataValueField = MyData.Tables(0).Columns("VendorPayeeID").ToString
        '        ddlVendorPayee.DataTextField = MyData.Tables(0).Columns("VendorPayeeName").ToString
        '        ddlVendorPayee.DataBind()

        '        txtVendorPayee.Visible = False
        '        btnVendor.Visible = False
        '        hdfVendorID.Value = ""
        '    Else
        '        btnVendor.Visible = False
        '        txtVendorPayee.Visible = True
        '        txtVendorPayee.Text = MyData.Tables(0).Columns("VendorPayeeName").ToString
        '        hdfVendorID.Value = MyData.Tables(0).Columns("VendorPayeeID").ToString
        '    End If

        '    dsInvoiceReceipt.Tables.Add(MyData.Tables(1).Copy)
        '    dsInvoiceReceipt.Tables(0).TableName = "InvoiceReceipt"
        '    ViewState("dsdsInvoiceReceipt") = dsInvoiceReceipt

        '    If ddlVendorPayee.Visible = True Then
        '        BindgvInvoiceReceipt(ddlVendorPayee.SelectedValue.ToString)
        '    Else
        '        BindgvInvoiceReceipt(hdfVendorID.Value.ToString)
        '    End If


        'Catch ex As Exception
        '    Me.lblMsg.Text = "GetPurchaseOrder: " & ex.Message.ToString
        'Finally
        '    'sqlConn.Close()
        'End Try
    End Sub

    Private Sub BindgvInvoiceReceipt(ByVal strVendorPayee As String, ByVal strPaymentID As String)
        dsInv = ViewState("dsdsInvoiceReceipt")



        Dim objDataSet2 As New DataSet
        If dsInv.Tables(0).Rows.Count > 0 Then
            objDataSet2.Merge(dsInv.Tables(0).Select("VendorPayeeID=" & strVendorPayee))
        End If

        If objDataSet2.Tables.Count = 0 Then
            objDataSet2 = dsInv.Clone()
        End If

        Dim objDataSet3 As New DataSet
        If hdfApplyNewID.Value.ToString <> "" Then
            objDataSet3.Merge(objDataSet2.Tables(0).Select("ApplyNewID='" & hdfApplyNewID.Value.ToString & "'"))
        End If

        If objDataSet3.Tables.Count = 0 Then
            objDataSet3 = objDataSet2.Clone()
        End If

        
        Dim objDataSet4 As New DataSet
        If strPaymentID <> "" Then
            objDataSet4.Merge(objDataSet2.Tables(0).Select("PaymentID='" & strPaymentID & "' OR PaymentID='' OR BalanceAmount <> 0.00"))
            If objDataSet4.Tables.Count = 0 Then
                objDataSet4 = dsInv.Clone()
            End If
            gvInvoiceReceipt.DataSource = objDataSet4
            gvInvoiceReceipt.DataMember = objDataSet4.Tables(0).TableName
            gvInvoiceReceipt.DataBind()
        Else
            'objDataSet4.Merge(objDataSet2.Tables(0).Select("PaymentID='" & strPaymentID & "' AND BalanceAmount <> 0.00"))
            objDataSet4.Merge(objDataSet2.Tables(0).Select("BalanceAmount <> 0.00"))
            If objDataSet4.Tables.Count = 0 Then
                objDataSet4 = dsInv.Clone()
            End If
            gvInvoiceReceipt.DataSource = objDataSet4
            gvInvoiceReceipt.DataMember = objDataSet4.Tables(0).TableName
            gvInvoiceReceipt.DataBind()
        End If



        SumAmount()

        'gvInvoiceReceipt.DataSource = objDataSet2
        'gvInvoiceReceipt.DataMember = objDataSet2.Tables(0).TableName
        'gvInvoiceReceipt.DataBind()
    End Sub

    Protected Sub ddlVendorPayee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVendorPayee.SelectedIndexChanged
        BindgvInvoiceReceipt(ddlVendorPayee.SelectedValue.ToString, hdfPaymentID.Value.ToString)
    End Sub

    Private Function SumAmount() As Boolean
        Dim i As Int32
        Dim decTotalAmount As Decimal = 0
        Dim blnValidate As Boolean
        blnValidate = True
        txtAmount.ForeColor = Drawing.Color.Empty

        For i = 0 To gvInvoiceReceipt.Rows.Count - 1
            Dim chkSelect As CheckBox

            chkSelect = CType(gvInvoiceReceipt.Rows(i).FindControl("chkSelect"), CheckBox)

            Dim lblAmount As Label
            lblAmount = CType(gvInvoiceReceipt.Rows(i).FindControl("lblAmount"), Label)
            Dim lblBalanceAmount As Label
            lblBalanceAmount = CType(gvInvoiceReceipt.Rows(i).FindControl("lblBalanceAmount"), Label)
            lblBalanceAmount.ForeColor = Drawing.Color.Empty

            Dim txtPaymentAmount As eWorld.UI.NumericBox
            txtPaymentAmount = CType(gvInvoiceReceipt.Rows(i).FindControl("txtPaymentAmount"), eWorld.UI.NumericBox)
            txtPaymentAmount.ForeColor = Drawing.Color.Empty

            If chkSelect.Checked = True Then
                If txtPaymentAmount.Text.ToString <> "" Then
                    decTotalAmount = decTotalAmount + CDec(txtPaymentAmount.Text.ToString)
                    lblBalanceAmount.Text = CDec(lblAmount.Text.ToString) - CDec(txtPaymentAmount.Text.ToString)
                    If CDec(lblBalanceAmount.Text.ToString) < 0 Then
                        blnValidate = False
                        lblBalanceAmount.ForeColor = Drawing.Color.Red
                    End If
                    If CDec(txtPaymentAmount.Text.ToString) <= 0 Then
                        blnValidate = False
                        txtPaymentAmount.ForeColor = Drawing.Color.Red
                    End If
                End If
            End If
        Next

        
        txtAmount.Text = decTotalAmount

        If CDec(txtAmount.Text.ToString) <= 0 Then
            blnValidate = False
            txtAmount.ForeColor = Drawing.Color.Red
        End If

        Return blnValidate

    End Function

    Protected Sub gvFile_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvFile.RowDeleting
        fuPayment.Dispose()

        hifSupDocPayment.Clear()
        Dim tbInvReceipt As DataTable = New DataTable("InvReceipt")
        tbInvReceipt.Columns.Add("Filename")
        tbInvReceipt.Columns.Add("DocPickLink")

        Dim dsInvReceipt As DataSet = New DataSet("InvReceipt")
        dsInvReceipt.Tables.Add(tbInvReceipt)

        gvFile.Visible = True
        gvFile.DataSource = dsInvReceipt
        gvFile.DataMember = dsInvReceipt.Tables(0).TableName
        gvFile.DataBind()

        fuPayment.Enabled = True
        btnAttach.Enabled = True
    End Sub

    Protected Sub btnAttach_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAttach.Click
        Dim blnContains As Boolean = False

        If fuPayment.FileName <> "" Then
            If (Regex.IsMatch(fuPayment.FileName, strFileExtension, RegexOptions.IgnoreCase)) = False Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid file format')</script>")
                'lblMsg.Text = "Invalid file format."
                Exit Sub
            End If

            ' Get the size in bytes of the file to upload.
            Dim fileSize As Integer = fuPayment.PostedFile.ContentLength

            ' Allow only files less than 2,100,000 bytes (approximately 2 MB) to be uploaded.
            'If (fileSize < 2100000) Then
            If (fileSize < intFileSizeLimitKB) Then

                Dim strAlbumPath As String
                'If hdfPOID.Value.ToString = "" Then
                strAlbumPath = hdfImageDocPath_Temp.Value.ToString + "/" + Session("DA_UserID").ToString
                'Else
                '    strAlbumPath = hdfImageDocPath_Temp.Value.ToString + "/" + hdfPOID.Value.ToString
                'End If

                Dim strAlbumPath_Open As String
                strAlbumPath_Open = hdfImageDocPath_Temp_Open.Value.ToString + "/" + Session("DA_UserID").ToString

                hifSupDocPayment.Add(fuPayment)
                Dim tbInvReceipt As DataTable = New DataTable("InvReceipt")
                tbInvReceipt.Columns.Add("Filename")
                tbInvReceipt.Columns.Add("DocPicLink")
                tbInvReceipt.Rows.Add(clsCF.RenameFileName(fuPayment.FileName.ToString), "<a href=""../frmOpenFile.aspx?FilePath=" & strAlbumPath_Open & "/" & clsCF.RenameFileName(fuPayment.FileName.ToString) & """ target=""_blank"">" & clsCF.RenameFileName(fuPayment.FileName.ToString) & "</a>")

                Dim dsInvReceipt As DataSet = New DataSet("InvReceipt")
                dsInvReceipt.Tables.Add(tbInvReceipt)

                gvFile.Visible = True
                gvFile.DataSource = dsInvReceipt
                gvFile.DataMember = dsInvReceipt.Tables(0).TableName
                gvFile.DataBind()

                If gvFile.Rows.Count > 0 Then
                    fuPayment.Enabled = False
                    btnAttach.Enabled = False
                Else
                    fuPayment.Enabled = True
                    btnAttach.Enabled = True
                End If


                If FileIO.FileSystem.DirectoryExists(strAlbumPath) = True Then
                    FileIO.FileSystem.DeleteDirectory(strAlbumPath, FileIO.DeleteDirectoryOption.DeleteAllContents)
                End If

                FileIO.FileSystem.CreateDirectory(strAlbumPath)

                If FileIO.FileSystem.FileExists(strAlbumPath + "/" + clsCF.RenameFileName(fuPayment.FileName.ToString)) = False Then
                    fuPayment.PostedFile.SaveAs(strAlbumPath + "/" + clsCF.RenameFileName(fuPayment.FileName.ToString))
                End If
            Else
                'lblMsg.Text = "Your file was not uploaded because it exceeds the " & intFileSizeLimit & " MB size limit."
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Your file was not uploaded because it exceeds the " & intFileSizeLimit & " MB size limit')</script>")
            End If
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click

        If Session("DA_StationID").ToString <> "" Then
            ddlStation.SelectedValue = Session("DA_StationID").ToString
        End If

        If Session("ApplyNewStationID") <> Nothing Then
            If Session("ApplyNewStationID").ToString <> "" Then
                ddlStation.SelectedValue = Session("ApplyNewStationID").ToString
            End If
        End If

        btnSearch.Enabled = True
        ddlVendorPayee.Dispose()
        ddlVendorPayee.Visible = False
        txtPayee.Visible = True
        btnVendor.Visible = True
        txtPayee.Text = ""
        hdfPayee.Value = ""
        ddlPaymentType.SelectedValue = "0"
        hdfPaymentID.Value = ""
        txtPaymentVoucherNo.Text = ""
        cldPaymentDate.Clear()
        txtAmount.Text = ""
        gvFile.Dispose()
        gvFile.Visible = False
        fuPayment.Dispose()
        fuPayment.Enabled = True
        btnAttach.Enabled = True
        txtPurchaseNo.Text = hdfApplyNewID.Value.ToString
        GetPayment(hdfApplyNewID.Value.ToString, "", "")
        GetInvReceiptForPayment()
    End Sub

    Private Sub ClearData()
        If Session("DA_StationID").ToString <> "" Then
            ddlStation.SelectedValue = Session("DA_StationID").ToString
        End If

        If Session("ApplyNewStationID") <> Nothing Then
            If Session("ApplyNewStationID").ToString <> "" Then
                ddlStation.SelectedValue = Session("ApplyNewStationID").ToString
            End If
        End If

        btnSearch.Enabled = True
        ddlVendorPayee.Dispose()
        ddlVendorPayee.Visible = False
        txtPayee.Visible = True
        btnVendor.Visible = True
        txtPayee.Text = ""
        hdfPayee.Value = ""
        ddlPaymentType.SelectedValue = "0"
        hdfPaymentID.Value = ""
        txtPaymentVoucherNo.Text = ""
        cldPaymentDate.Clear()
        txtAmount.Text = ""
        gvFile.Dispose()
        gvFile.Visible = False
        fuPayment.Dispose()
        fuPayment.Enabled = True
        btnAttach.Enabled = True
        txtPurchaseNo.Text = hdfApplyNewID.Value.ToString
    End Sub

    Private Sub GetPayment(ByVal strApplyNewID As String, ByVal strPaymentID As String, ByVal strPayee As String)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsInvoiceReceipt As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetPayment")

                With MyCommand

                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = strApplyNewID
                    .Parameters.Add("@PaymentID", SqlDbType.VarChar, 20)
                    .Parameters("@PaymentID").Value = strPaymentID
                    .Parameters.Add("@Payee", SqlDbType.NVarChar, 50)
                    .Parameters("@Payee").Value = strPayee
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 6)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString

                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "VendorPayee"

                If strApplyNewID <> "" Then
                    If MyData.Tables(0).Rows.Count > 1 Then
                        ddlVendorPayee.Visible = True
                        ddlVendorPayee.DataSource = MyData.Tables(0)
                        ddlVendorPayee.DataValueField = MyData.Tables(0).Columns("VendorPayeeID").ToString
                        ddlVendorPayee.DataTextField = MyData.Tables(0).Columns("VendorPayeeName").ToString
                        ddlVendorPayee.DataBind()

                        txtPayee.Visible = False
                        btnVendor.Visible = False
                        hdfPayee.Value = ""
                    Else
                        ddlVendorPayee.Visible = False
                        btnVendor.Visible = True
                        txtPayee.Visible = True
                        If hdfPayee.Value.ToString = "" Then
                            txtPayee.Text = MyData.Tables(0).Rows(0)("VendorPayeeName").ToString
                            hdfPayee.Value = MyData.Tables(0).Rows(0)("VendorPayeeID").ToString
                        End If
                    End If
                Else
                    ddlVendorPayee.Visible = False
                    btnVendor.Visible = True
                    txtPayee.Visible = True
                End If

                gvPayment.DataSource = MyData
                gvPayment.DataMember = MyData.Tables(1).TableName
                gvPayment.DataBind()

                If txtPurchaseNo.Text.ToString = "" Then
                    gvPayment.Columns(8).Visible = False
                Else
                    gvPayment.Columns(8).Visible = True
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "GetPayment: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub GetInvReceiptForPayment()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsInvoiceReceipt As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetInvReceiptForPayment")

                With MyCommand

                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = txtPurchaseNo.Text.ToString
                    .Parameters.Add("@PaymentID", SqlDbType.VarChar, 20)
                    .Parameters("@PaymentID").Value = hdfPaymentID.Value.ToString
                    .Parameters.Add("@Payee", SqlDbType.NVarChar, 50)
                    If ddlVendorPayee.Visible = True Then
                        .Parameters("@Payee").Value = ddlVendorPayee.SelectedValue.ToString
                    Else
                        .Parameters("@Payee").Value = hdfPayee.Value.ToString
                    End If

                    .Parameters.Add("@PayeeType", SqlDbType.VarChar, 50)
                    .Parameters("@PayeeType").Value = hdfPayeeType.Value.ToString


                    .Parameters.Add("@StationID", SqlDbType.VarChar, 6)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString

                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "InvReceipt"

                gvInvoiceReceipt.DataSource = MyData
                gvInvoiceReceipt.DataMember = MyData.Tables(0).TableName
                gvInvoiceReceipt.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "GetPayment: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    'Protected Sub gvPayment_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvPayment.RowDeleting
    '    Dim strPaymentID As String = 0
    '    Dim lbtnRemove As New LinkButton

    '    lblMsg.Text = ""

    '    lbtnRemove = gvPayment.Rows(e.RowIndex).FindControl("lbtnRemove")
    '    strPaymentID = lbtnRemove.CommandArgument.ToString


    '    Dim strMsg As String
    '    Dim sqlConn As New SqlConnection(strConnectionString)

    '    Try
    '        sqlConn.Open()
    '        Dim cmd As New SqlCommand("SP_DeletePayment", sqlConn)
    '        With cmd
    '            .CommandType = CommandType.StoredProcedure
    '            .Parameters.Add("@PaymentID", SqlDbType.VarChar, 20)
    '            .Parameters("@PaymentID").Value = strPaymentID
    '            .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
    '            .Parameters("@Msg").Direction = ParameterDirection.Output
    '            .CommandTimeout = 0
    '            .ExecuteNonQuery()

    '            strMsg = .Parameters("@Msg").Value.ToString()
    '        End With

    '        If strMsg <> "" Then
    '            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
    '        Else
    '            btnClear_Click(sender, e)
    '            GetPayment(hdfApplyNewID.Value.ToString, "", "")
    '            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
    '        End If
    '    Catch ex As Exception
    '        Me.lblMsg.Text = "gvPayment_RowDeleting: " & ex.Message.ToString
    '    Finally
    '        sqlConn.Close()
    '    End Try
    'End Sub

    'Protected Sub gvPayment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPayment.SelectedIndexChanged
    '    Dim lbtnVoucherNo As New LinkButton
    '    Dim hdfgv_ApplyNewID As New HiddenField
    '    Dim strPaymentID As String = ""
    '    lbtnVoucherNo = gvPayment.SelectedRow.FindControl("lbtnVoucherNo")
    '    strPaymentID = lbtnVoucherNo.CommandArgument.ToString
    '    'hdfgv_ApplyNewID = gvPayment.SelectedRow.FindControl("hdfgv_ApplyNewID")
    '    btnSearch.Enabled = False

    '    GetPayment("", strPaymentID, "")
    '    GetInvReceiptForPayment(txtPurchaseNo.Text.ToString, strPaymentID, hdfPayee.Value.ToString)

    'End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If validateData() = False Then
            Exit Sub
        End If

        SavePayment()
    End Sub

    Private Function ValidateData() As Boolean
        Dim strPaymentDate As String = ""

        ValidateData = True

        If ddlVendorPayee.Items.Count > 0 Then
            If ddlVendorPayee.SelectedValue.ToString = "" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Vendor/Payee')</script>")
                ddlVendorPayee.Focus()
                ValidateData = False
                Exit Function
            End If
        Else
            If txtPayee.Text = "" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Vendor/Payee')</script>")
                btnVendor.Focus()
                ValidateData = False
                Exit Function
            End If
        End If
        

        If ddlPaymentType.SelectedValue.ToString = "0" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Payment Type')</script>")
            ddlPaymentType.Focus()
            ValidateData = False
            Exit Function
        End If

        If txtPaymentVoucherNo.Text.ToString = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Payment Voucher No')</script>")
            txtPaymentVoucherNo.Focus()
            ValidateData = False
            Exit Function
        End If

        If txtAmount.Text.ToString = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Amount')</script>")
            txtAmount.Focus()
            ValidateData = False
            Exit Function
        End If

        If CDec(txtAmount.Text.ToString) <= 0 Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Amount')</script>")
            txtAmount.Focus()
            ValidateData = False
            Exit Function
        End If

        strPaymentDate = Format(cldPaymentDate.SelectedDate, "dd MMM yyyy").ToString

        If strPaymentDate.Contains("01 Jan 0001") = True Then
            strPaymentDate = ""
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Payment Date')</script>")
            cldPaymentDate.Focus()
            ValidateData = False
            Exit Function
        End If

        If SumAmount() = False Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Payment Amount')</script>")
            ValidateData = False
            Exit Function
        End If

        Dim i As Integer
        Dim blnInvReceipt As Boolean = False
        For i = 0 To gvInvoiceReceipt.Rows.Count - 1
            Dim chkSelect As New CheckBox
            chkSelect = gvInvoiceReceipt.Rows(i).FindControl("chkSelect")
            If chkSelect.Checked = True Then
                blnInvReceipt = True
            End If
        Next

        If gvInvoiceReceipt.Rows.Count > 0 Then
            If blnInvReceipt = False Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Invoice / Receipt')</script>")
                ValidateData = False
                Exit Function
            End If
        End If

        Return ValidateData

    End Function

    Private Sub SavePayment()
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strPaymentDate As String = ""
                Dim strFilePath As String = ""
                Dim strFileName As String = ""
                Dim strInvReceiptID As String = ""
                Dim strPaymentID As String = ""
                Dim strMailID As String = ""
                'Dim strReturnMsg As Boolean = True
                Dim strMsg As String = ""


                lblMsg.Text = ""

                Dim i As Integer
                Dim blnInvReceipt As Boolean = False
                For i = 0 To gvInvoiceReceipt.Rows.Count - 1
                    Dim chkSelect As New CheckBox
                    Dim hfInvoiceID As New HiddenField
                    Dim hdfType As New HiddenField
                    Dim strInvoiceReceipt As String = ""
                    hfInvoiceID = gvInvoiceReceipt.Rows(i).FindControl("hfInvoiceID")
                    hdfType = gvInvoiceReceipt.Rows(i).FindControl("hdfType")
                    chkSelect = gvInvoiceReceipt.Rows(i).FindControl("chkSelect")
                    Dim txtPaymentAmount As eWorld.UI.NumericBox
                    Dim strPaymentAmount As String = "0.00"
                    txtPaymentAmount = CType(gvInvoiceReceipt.Rows(i).FindControl("txtPaymentAmount"), eWorld.UI.NumericBox)
                    If txtPaymentAmount.Text.ToString = "" Then
                        strPaymentAmount = "0.00"
                    Else
                        strPaymentAmount = CDec(txtPaymentAmount.Text.ToString)
                    End If
                    If chkSelect.Checked = True Then
                        blnInvReceipt = True
                        strInvoiceReceipt = hfInvoiceID.Value.ToString + "<" + hdfType.Value.ToString + ">" + strPaymentAmount + "|"
                        If strInvReceiptID <> "" Then
                            strInvReceiptID = strInvReceiptID + strInvoiceReceipt
                        Else
                            strInvReceiptID = strInvoiceReceipt
                        End If
                    End If
                Next

                'GET ATTACHED FILE NAME FROM GV
                For i = 0 To gvFile.Rows.Count - 1
                    Dim lbtnRemove As New LinkButton
                    lbtnRemove = gvFile.Rows(i).FindControl("lbtnRemove")
                    strFileName = lbtnRemove.CommandArgument.ToString
                Next

                strPaymentDate = Format(cldPaymentDate.SelectedDate, "dd MMM yyyy").ToString

                clsCF.CheckConnectionState(sqlConn)



                Dim cmd As New SqlCommand("SP_SavePaymentDetail", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ApplyNewID", SqlDbType.NVarChar, 50)
                    .Parameters("@ApplyNewID").Value = hdfApplyNewID.Value.ToString
                    .Parameters.Add("@PaymentID", SqlDbType.VarChar, 20)
                    If hdfPaymentID.Value.ToString = "" Then
                        .Parameters("@PaymentID").Value = ""
                    Else
                        .Parameters("@PaymentID").Value = hdfPaymentID.Value.ToString
                    End If

                    .Parameters.Add("@PaymentVoucherNo", SqlDbType.NVarChar, 50)
                    .Parameters("@PaymentVoucherNo").Value = txtPaymentVoucherNo.Text.ToString

                    .Parameters.Add("@Payee", SqlDbType.NVarChar, 50)
                    If ddlVendorPayee.Items.Count > 1 Then
                        .Parameters("@Payee").Value = ddlVendorPayee.SelectedValue.ToString
                    Else
                        .Parameters("@Payee").Value = hdfPayee.Value.ToString
                    End If

                    .Parameters.Add("@InvReceiptID", SqlDbType.NVarChar)
                    .Parameters("@InvReceiptID").Value = strInvReceiptID

                    .Parameters.Add("@PaymentType", SqlDbType.Int)
                    If ddlPaymentType.SelectedValue.ToString = "0" Then
                        .Parameters("@PaymentType").Value = DBNull.Value
                    Else
                        .Parameters("@PaymentType").Value = ddlPaymentType.SelectedValue.ToString
                    End If

                    .Parameters.Add("@TotalAmount", SqlDbType.Decimal, 18, 4)
                    .Parameters("@TotalAmount").Value = CDec(txtAmount.Text.ToString)

                    .Parameters.Add("@PaymentDate", SqlDbType.NVarChar, 20)
                    .Parameters("@PaymentDate").Value = strPaymentDate

                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString

                    .Parameters.Add("@FileName", SqlDbType.NVarChar, 500)
                    .Parameters("@FileName").Value = strFileName
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@NewPaymentID", SqlDbType.VarChar, 20)
                    .Parameters("@NewPaymentID").Direction = ParameterDirection.Output
                    .Parameters.Add("@FilePath", SqlDbType.NVarChar, 500)
                    .Parameters("@FilePath").Direction = ParameterDirection.Output
                    .Parameters.Add("@biMailID", SqlDbType.NVarChar, 500)
                    .Parameters("@biMailID").Direction = ParameterDirection.Output
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                    strFilePath = .Parameters("@FilePath").Value.ToString()
                    strPaymentID = .Parameters("@NewPaymentID").Value.ToString()
                    strMailID = .Parameters("@biMailID").Value.ToString()
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    If strFilePath <> "" Then
                        Dim strAlbumPath_Temp As String
                        strAlbumPath_Temp = hdfImageDocPath_Temp.Value.ToString + "/" + Session("DA_UserID").ToString

                        If FileIO.FileSystem.DirectoryExists(strFilePath) = False Then
                            FileIO.FileSystem.CreateDirectory(strFilePath)
                        End If

                        If FileIO.FileSystem.FileExists(strAlbumPath_Temp + "/" + strFileName) = True Then
                            If FileIO.FileSystem.FileExists(strFilePath + "/" + strFileName) = True Then
                                FileIO.FileSystem.DeleteFile(strFilePath + "/" + strFileName)
                            End If

                            FileIO.FileSystem.CopyFile(strAlbumPath_Temp + "/" + strFileName, strFilePath + "/" + strFileName, True)
                        End If
                    End If


                    If strMailID <> "" Then
                        Dim strResult As String = ""
                        Dim intMailID As Integer
                        Dim strMessage As String = ""
                        Dim MailID As String() = strMailID.Split(New Char() {","c})

                        For Each intMailID In MailID
                            strResult = clsCF.SendMail(intMailID)
                            If strResult = "" Then
                                strMessage = "Post eFMS Notification Send Successful"
                                clsCF.UpdateMailQueue(intMailID, 1, strMessage, hdfCurrentDateTime.Value.ToString)
                            Else
                                strMessage = "Post eFMS Notification Send Fail - " & strResult
                                clsCF.UpdateMailQueue(intMailID, 2, strMessage, hdfCurrentDateTime.Value.ToString)
                            End If
                        Next
                    End If

                    ClearData()

                    If hdfApplyNewID.Value.ToString <> "" Then
                        strPaymentID = ""
                    End If
                    GetPayment(hdfApplyNewID.Value.ToString, strPaymentID, "")
                    GetInvReceiptForPayment()
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                End If

                'Return strReturnMsg
            Catch ex As Exception
                'strReturnMsg = False
                Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
                'Return strReturnMsg
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnCalculate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCalculate.Click
        If SumAmount() = False Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Payment Amount')</script>")
            Exit Sub
        End If
    End Sub

    Protected Sub gvPayment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPayment.SelectedIndexChanged
        Session("PaymentID") = ""
        Session("PaymentID") = gvPayment.SelectedValue.ToString
        'Response.Redirect("~/Purchase/frmPaymentDetail.aspx")

        Dim strScript As String
        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('frmPaymentDetail.aspx?ParentForm=frmPayment','PaymentDetail','height=750, width=900,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub ddlStation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStation.SelectedIndexChanged
        Session("DA_StationID") = ddlStation.SelectedValue.ToString
        GetPayment(txtPurchaseNo.Text.ToString, "", "")
        GetInvReceiptForPayment()
    End Sub
End Class
