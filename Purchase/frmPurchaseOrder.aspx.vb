Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Purchase_frmPurchaseOrder
    Inherits System.Web.UI.Page
    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Public Shared hifSupDoc As ArrayList = New ArrayList()
    Dim strFileExtension As String = ConfigurationSettings.AppSettings("DocExtension")
    Dim intFileSizeLimitKB As Integer = ConfigurationSettings.AppSettings("intFileSizeLimitKB")
    Dim intFileSizeLimit As Integer = ConfigurationSettings.AppSettings("intFileSizeLimit")

    Dim dsQR As New DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If IsPostBack() = False Then
            hdfApplyNewID.Value = ""
            btnGeneratePO.Visible = False
            btnAdd.Visible = False
           
            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            'gvFile.Columns(0).Visible = False
            btnAdd.Enabled = False
            gvPO.Columns(0).Visible = False
            btnSend.Enabled = False


            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "P0005")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Purchase Order Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnAdd.Enabled = True
                        gvPO.Columns(0).Visible = True
                        btnSend.Enabled = True
                    Else
                        Session("blnWrite") = False
                        btnAdd.Enabled = False
                        gvPO.Columns(0).Visible = False
                        btnSend.Enabled = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If


            If Session("ApplyNewID").ToString <> "" Then
                hdfApplyNewID.Value = Session("ApplyNewID").ToString
                txtPurchaseNo.Text = hdfApplyNewID.Value.ToString
                txtPurchaseNo.Enabled = False
            End If



            If Session("POID").ToString <> "" Then
                hdfPOID.Value = Session("POID").ToString
            Else
                hdfPOID.Value = ""
            End If



            BindDropDownList()
            'If Session("ApplyNewStationID") = Nothing Then
            '    Session("ApplyNewStationID") = ""
            'End If

            If Session("ApplyNewStationID").ToString <> "" Then
                ddlStation.SelectedValue = Session("ApplyNewStationID").ToString
                ddlStation.Enabled = False
            Else
                ddlStation.Enabled = True
            End If
            Session("ApplyNewID") = ""
            Session("POID") = ""
            Session("ApplyNewStationID") = ""
            GetPurchaseOrder(hdfApplyNewID.Value.ToString, hdfPOID.Value.ToString)

            GetCheckIs_InChargeParty()
            
            Me.btnAdd.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnAdd, "", "btnClose", "btnSend", "btnClear", "", ""))
            Me.btnSend.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnSend, "", "btnClose", "btnClear", "", "", ""))
            'Me.btnSend.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnSend, "", "btnClose", "btnAdd", "btnClear", "", ""))

        End If
    End Sub

    Private Sub GetCheckIs_InChargeParty()
        Dim dsInChargeParty As New DataSet
        dsInChargeParty = clsCF.GetCheckIs_InChargeParty(Session("DA_UserID").ToString, ddlStation.SelectedValue.ToString)

        If dsInChargeParty.Tables.Count > 0 Then
            If dsInChargeParty.Tables(0).Rows(0)("Issue_PO").ToString <> 0 Then
                btnAdd.Enabled = True
                gvPO.Columns(0).Visible = True
                btnSend.Enabled = True
            Else
                btnAdd.Enabled = False
                gvPO.Columns(0).Visible = False
                btnSend.Enabled = False
            End If
        End If
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.NVarChar, 500)
                    .Parameters("@ReferenceType").Value = "[AccessStation],[ImageDocPath_Temp],[ImageDocPath_Temp_Open]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"
                'MyData.Tables(1).TableName = "AssetStatus"

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)


                'If MyData.Tables(0).Rows(0)("AccessStation").ToString <> "" Then
                '    dsStation = DIMERCO.SDK.Utilities.LSDK.getStationDataByWithList(MyData.Tables(0).Rows(0)("AccessStation").ToString)
                'Else
                '    dsStation = DIMERCO.SDK.Utilities.ReSM.GetStationList()
                'End If
                'Dim dtView As DataView = dsStation.Tables(0).DefaultView
                'dtView.Sort = "StationCode ASC"

                'ddlStation.DataSource = dtView
                'ddlStation.DataValueField = dtView.Table.Columns("StationID").ToString
                'ddlStation.DataTextField = dtView.Table.Columns("StationCode").ToString
                'ddlStation.DataBind()

                If Session("DA_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("DA_StationID").ToString
                End If


                If MyData.Tables(1).Rows.Count > 0 Then
                    hdfImageDocPath_Temp.Value = MyData.Tables(1).Rows(0)("ImageDocPath_Temp").ToString
                Else
                    hdfImageDocPath_Temp.Value = ""
                End If

                If MyData.Tables(2).Rows.Count > 0 Then
                    hdfImageDocPath_Temp_Open.Value = MyData.Tables(2).Rows(0)("ImageDocPath_Temp").ToString
                Else
                    hdfImageDocPath_Temp_Open.Value = ""
                End If


            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub GetPurchaseOrder(ByVal strApplyNewID As String, ByVal strPOID As String)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetPurchaseOrder")

                With MyCommand

                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = strApplyNewID
                    .Parameters.Add("@POID", SqlDbType.VarChar, 20)
                    .Parameters("@POID").Value = strPOID
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "PurchaseOrder"

                dsQR.Tables.Add(MyData.Tables(3).Copy)
                dsQR.Tables(0).TableName = "QR"
                ViewState("dsQR") = dsQR

                ddlVendorPayee.DataSource = MyData.Tables(2)
                ddlVendorPayee.DataValueField = MyData.Tables(2).Columns("VendorPayeeID").ToString
                ddlVendorPayee.DataTextField = MyData.Tables(2).Columns("VendorPayeeName").ToString
                ddlVendorPayee.DataBind()


                ddlVendorPayee.Enabled = True
                gvQuoReceipt.Columns(0).Visible = True
                txtPONo.Enabled = True
                cldPODate.Enabled = True
                txtAmount.Enabled = True

                gvPO.DataSource = MyData
                gvPO.DataMember = MyData.Tables(0).TableName
                gvPO.DataBind()

                If strPOID = "" Then


                    If ddlVendorPayee.Items.Count = 2 Then
                        ddlVendorPayee.SelectedIndex = 1
                    ElseIf ddlVendorPayee.Items.Count > 2 Then
                        ddlVendorPayee.SelectedIndex = 1
                    End If
                    fuPO.Enabled = True
                    btnAttach.Enabled = True

                    BindgQR(ddlVendorPayee.SelectedValue.ToString, hdfPOID.Value.ToString)

                    If gvQuoReceipt.Rows.Count = 0 Then
                        'ddlVendorPayee.Enabled = False
                        Dim I As Integer = 0
                        For I = 0 To gvQuoReceipt.Rows.Count - 1
                            Dim chkSelect As New CheckBox
                            chkSelect = gvQuoReceipt.Rows(I).FindControl("chkSelect")
                            chkSelect.Enabled = False
                        Next
                        txtPONo.Enabled = False
                        cldPODate.Enabled = False
                        txtAmount.Enabled = False
                    End If

                Else
                    hdfPOID.Value = MyData.Tables(0).Rows(0)("POID").ToString
                    txtPONo.Text = MyData.Tables(0).Rows(0)("PONo").ToString
                    cldPODate.SelectedDate = Format(Convert.ToDateTime(MyData.Tables(0).Rows(0)("PODate").ToString), "dd MMM yyyy")
                    txtAmount.Text = MyData.Tables(0).Rows(0)("TotalAmount").ToString
                    ddlVendorPayee.SelectedValue = MyData.Tables(0).Rows(0)("VendorID").ToString
                    txtRemarks.Text = MyData.Tables(0).Rows(0)("Remarks").ToString

                    If MyData.Tables(1).Rows(0)("FileName").ToString = "" Then
                        Dim tbPO As DataTable = New DataTable("PO")
                        tbPO.Columns.Add("Filename")
                        tbPO.Columns.Add("DocPickLink")
                        'tbQuoRecpt.Rows.Add(fuQuoRecpt.FileName, fuQuoRecpt.PostedFile.FileName)

                        Dim dsPO As DataSet = New DataSet("PO")
                        dsPO.Tables.Add(tbPO)

                        gvFile.Visible = True
                        gvFile.DataSource = dsPO
                        gvFile.DataMember = dsPO.Tables(0).TableName
                        gvFile.DataBind()
                    Else
                        gvFile.Visible = True
                        gvFile.DataSource = MyData
                        gvFile.DataMember = MyData.Tables(1).TableName
                        gvFile.DataBind()
                    End If

                    If gvFile.Rows.Count > 0 Then
                        fuPO.Enabled = False
                        btnAttach.Enabled = False
                    Else
                        fuPO.Enabled = True
                        btnAttach.Enabled = True
                    End If

                    BindgQR(ddlVendorPayee.SelectedValue.ToString, hdfPOID.Value.ToString)

                    If MyData.Tables(0).Rows.Count > 0 Then
                        If MyData.Tables(0).Rows(0)("AllowDelete") = "0" Then
                            'ddlVendorPayee.Enabled = False
                            Dim I As Integer = 0
                            For I = 0 To gvQuoReceipt.Rows.Count - 1
                                Dim chkSelect As New CheckBox
                                chkSelect = gvQuoReceipt.Rows(I).FindControl("chkSelect")
                                chkSelect.Enabled = False
                            Next
                            txtPONo.Enabled = False
                            cldPODate.Enabled = False
                            txtAmount.Enabled = False
                        End If
                    End If

                End If

                If MyData.Tables(4).Rows(0)("AutoGeneratePO").ToString = "0" Then
                    btnAdd.Visible = True
                    txtPONo.Enabled = True
                    txtAmount.Enabled = True
                    cldPODate.Enabled = True
                    fuPO.Enabled = True
                    btnAttach.Enabled = True
                    btnGeneratePO.Visible = False
                Else
                    btnAdd.Visible = False
                    txtPONo.Enabled = False
                    txtAmount.Enabled = False
                    cldPODate.Enabled = False
                    fuPO.Enabled = False
                    btnAttach.Enabled = False

                    btnGeneratePO.Visible = True
                    If gvFile.Rows.Count > 0 Then
                        btnGeneratePO.Text = "Re-Generate & Preview PO"
                    End If

                End If

            Catch ex As Exception
                Me.lblMsg.Text = "GetPurchaseOrder: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub BindgQR(ByVal strVendorPayee As String, ByVal strPOID As String)
        dsQR = ViewState("dsQR")

        Dim objDataSet2 As New DataSet
        objDataSet2.Merge(dsQR.Tables(0).Select("VendorPayeeID='" & strVendorPayee & "'"))

        If objDataSet2.Tables.Count = 0 Then
            objDataSet2 = dsQR.Clone()
        End If


        Dim objDataSet3 As New DataSet
        If strPOID <> "" Then
            objDataSet3.Merge(objDataSet2.Tables(0).Select("POID='" & strPOID & "' OR POID=''"))
            If objDataSet3.Tables.Count = 0 Then
                objDataSet3 = dsQR.Clone()
            End If
            gvQuoReceipt.DataSource = objDataSet3
            gvQuoReceipt.DataMember = objDataSet3.Tables(0).TableName
            gvQuoReceipt.DataBind()
        Else
            objDataSet3.Merge(objDataSet2.Tables(0).Select("POID='" & strPOID & "'"))
            If objDataSet3.Tables.Count = 0 Then
                objDataSet3 = dsQR.Clone()
            End If
            gvQuoReceipt.DataSource = objDataSet3
            gvQuoReceipt.DataMember = objDataSet3.Tables(0).TableName
            gvQuoReceipt.DataBind()
        End If

       

        'objDataSet2 = 
        'dsPO.Tables(0).DefaultView.RowFilter = "VendorPayeeID = " & strVendorPayee
        'dSet.Tables[0].DefaultView.RowFilter = "Frequency like '%30%')";


        'Dim r As DataRow
        'For Each r In dsPO.Tables(0).Select("VendorPayeeID=" & strVendorPayee)
        '    'strCalBy = r.Item("nvchCalBy").ToString
        '    'strDefaultDay = r.Item("iDefaultDay").ToString
        '    'hdDefaultDay.Value = strDefaultDay
        '    'hdDefaultHour.Value = r.Item("iDefaultHour").ToString
        '    'hdDeputyApproval.Value = r.Item("tiDeputyApproval").ToString
        '    'hdNeedDeputy.Value = r.Item("tiNeedDeputy").ToString
        '    'hdfALWithoutDeputy.Value = r.Item("tiApplyLeave_WithoutDeputy").ToString
        '    'hdfApplyLimit.Value = r.Item("iApplyLimit").ToString
        'Next
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Dim ParentFormID As String = ""
        ParentFormID = Request.QueryString("ParentForm").ToString

        'Session("ApplyNewID") = ""
        'Response.Write("<script language=""Javascript"">window.close();</script>")
        Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener =self;window.close();</script>")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        ClearData()
        GetPurchaseOrder(hdfApplyNewID.Value.ToString, "")
    End Sub

    Private Sub ClearData()
        If Session("ApplyNewStationID").ToString <> "" Then
            ddlStation.SelectedValue = Session("ApplyNewStationID").ToString
        Else
            If Session("DA_StationID").ToString <> "" Then
                ddlStation.SelectedValue = Session("DA_StationID").ToString
            End If
        End If

        'txtPurchaseNo.Text = ""
        'txtVendorPayee.Text = ""
        'gvQuoReceipt.Dispose()
        'gvQuoReceipt.Visible = False
        ddlVendorPayee.Enabled = True
        hdfPOID.Value = ""
        txtPONo.Text = ""
        cldPODate.Clear()
        txtAmount.Text = ""
        gvFile.Dispose()
        gvFile.Visible = False
        fuPO.Dispose()
        fuPO.Enabled = True
        btnAttach.Enabled = True
        txtRemarks.Text = ""

    End Sub

    Protected Sub gvFile_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvFile.RowDeleting
        fuPO.Dispose()

        hifSupDoc.Clear()
        Dim tbPO As DataTable = New DataTable("PO")
        tbPO.Columns.Add("Filename")
        tbPO.Columns.Add("DocPickLink")
        'tbQuoRecpt.Rows.Add(fuQuoRecpt.FileName, fuQuoRecpt.PostedFile.FileName)

        Dim dsPO As DataSet = New DataSet("PO")
        dsPO.Tables.Add(tbPO)

        'gvFile.DeleteRow(0)
        gvFile.Visible = True
        gvFile.DataSource = dsPO
        gvFile.DataMember = dsPO.Tables(0).TableName
        gvFile.DataBind()

        fuPO.Enabled = True
        btnAttach.Enabled = True

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        GetPurchaseOrder(txtPurchaseNo.Text.ToString, "")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'SaveSendPO("D")
    End Sub

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try

                'SaveSendPO("S")
                Dim strMessage As String = ""
                Dim strMailID As String = ""
                Dim strMsg As String = ""

                Dim strPOID As String = ""
                Dim i As Integer

                For i = 0 To gvPO.Rows.Count - 1
                    Dim lbtnPONo As New LinkButton
                    lbtnPONo = gvPO.Rows(i).FindControl("lbtnPONo")
                    If strPOID = "" Then
                        strPOID = lbtnPONo.CommandArgument.ToString
                    Else
                        strPOID = strPOID + "|" + lbtnPONo.CommandArgument.ToString
                    End If
                Next


                If strPOID = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Purchase Order')</script>")
                    Exit Sub
                End If

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveSendPO", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@POID", SqlDbType.NVarChar)
                    .Parameters("@POID").Value = strPOID
                    .Parameters.Add("@Comment", SqlDbType.NVarChar)
                    .Parameters("@Comment").Value = txtComment.Text.ToString
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                    .Parameters.Add("@Action", SqlDbType.VarChar, 1)
                    .Parameters("@Action").Value = "S"
                    .Parameters.Add("@biMailID", SqlDbType.NVarChar, 500)
                    .Parameters("@biMailID").Direction = ParameterDirection.Output
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMailID = .Parameters("@biMailID").Value.ToString()
                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    lblMsg.Text = strMsg
                Else
                    lblMsg.Text = ""

                    If strMailID.ToString <> "" Then
                        Dim strResult As String = ""
                        Dim MailID As String() = strMailID.Split(New Char() {","c})
                        Dim intMailID As Integer
                        Dim strStatusMsg As String = ""

                        For Each intMailID In MailID
                            strResult = clsCF.SendMail_WithAttach(intMailID)
                            If strResult = "" Then
                                strMessage = "Purchase Order Send Successful"
                                clsCF.UpdateMailQueue(intMailID, 1, strStatusMsg, hdfCurrentDateTime.Value.ToString)
                            Else
                                strMessage = "Purchase Order Send Fail - " & strResult
                                clsCF.UpdateMailQueue(intMailID, 2, strMessage, hdfCurrentDateTime.Value.ToString)
                            End If
                        Next
                    Else
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Fail - No Email Setting.')</script>")
                    End If
                End If

                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMessage + "')</script>")

            Catch ex As Exception
                Me.lblMsg.Text = "btnSend_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    'Private Sub SaveSendPO(ByVal strAction As String)
    '    Dim strMailID As String = ""
    '    Dim strMsg As String = ""
    '    Dim sqlConn As New SqlConnection(strConnectionString)
    '    Dim strPOID As String = ""
    '    Dim i As Integer

    '    For i = 0 To gvPO.Rows.Count - 1
    '        Dim lbtnPONo As New LinkButton
    '        lbtnPONo = gvPO.Rows(i).FindControl("lbtnPONo")
    '        If strPOID = "" Then
    '            strPOID = lbtnPONo.CommandArgument.ToString
    '        Else
    '            strPOID = strPOID + "|" + lbtnPONo.CommandArgument.ToString
    '        End If
    '    Next

    '    If strPOID = "" Then
    '        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Purchase Order')</script>")
    '        Exit Sub
    '    End If

    '    Try
    '        sqlConn.Open()
    '        Dim cmd As New SqlCommand("SP_SaveSendPO", sqlConn)
    '        With cmd
    '            .CommandType = CommandType.StoredProcedure
    '            .Parameters.Add("@POID", SqlDbType.NVarChar)
    '            .Parameters("@POID").Value = strPOID
    '            .Parameters.Add("@Remarks", SqlDbType.NVarChar)
    '            .Parameters("@Remarks").Value = txtRemarks.Text.ToString
    '            .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
    '            .Parameters("@UserID").Value = Session("DA_UserID").ToString
    '            .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
    '            .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
    '            .Parameters.Add("@Action", SqlDbType.VarChar, 1)
    '            .Parameters("@Action").Value = strAction
    '            .Parameters.Add("@biMailID", SqlDbType.NVarChar, 500)
    '            .Parameters("@biMailID").Direction = ParameterDirection.Output
    '            .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
    '            .Parameters("@Msg").Direction = ParameterDirection.Output
    '            .CommandTimeout = 0
    '            .ExecuteNonQuery()

    '            strMailID = .Parameters("@biMailID").Value.ToString()
    '            strMsg = .Parameters("@Msg").Value.ToString()
    '        End With

    '        If strMsg <> "" Then
    '            lblMsg.Text = strMsg
    '        Else
    '            lblMsg.Text = ""
    '            Dim strMessage As String = ""
    '            If strAction = "S" Then
    '                Dim strResult As String = ""
    '                Dim MailID As String() = strMailID.Split(New Char() {","c})
    '                Dim intMailID As Integer
    '                Dim strStatusMsg As String = ""

    '                For Each intMailID In MailID
    '                    strResult = clsCF.SendMail_WithAttach(intMailID)
    '                    If strResult = "" Then
    '                        strMessage = "Purchase Order Send Successful"
    '                        clsCF.UpdateMailQueue(intMailID, 1, strStatusMsg)
    '                    Else
    '                        strMessage = "Purchase Order Send Fail - " & strResult
    '                        clsCF.UpdateMailQueue(intMailID, 2, strMessage)
    '                    End If
    '                Next
    '            Else
    '                strMessage = "Save Successful."
    '            End If

    '            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMessage + "')</script>")
    '        End If
    '    Catch ex As Exception
    '        Me.lblMsg.Text = "SaveSendPO: " & ex.Message.ToString
    '    Finally
    '        sqlConn.Close()
    '    End Try
    'End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SavePO("Save")
    End Sub

    Private Sub SavePO(ByVal strAction As String)

        Using sqlConn As New SqlConnection(strConnectionString)
            Try

                Dim strMsg As String = ""
                Dim strPOID As String = ""
                Dim strPODate As String = ""
                Dim strFilePath As String = ""
                Dim strFileName As String = ""
                Dim strQuotationReceiptID As String = ""

                lblMsg.Text = ""

                strPODate = Format(cldPODate.SelectedDate, "dd MMM yyyy").ToString

                If strAction = "Save" Then
                    If txtPONo.Text.ToString = "" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Purchase Order No')</script>")
                        txtPONo.Focus()
                        Exit Sub
                    End If

                    If strPODate.Contains("01 Jan 0001") = True Then
                        strPODate = ""
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Purchase Order Date')</script>")
                        cldPODate.Focus()
                        Exit Sub
                    End If
                End If

                If txtPurchaseNo.Text.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Purchase No')</script>")
                    txtPurchaseNo.Focus()
                    Exit Sub
                End If



                Dim i As Integer
                Dim blnQuo As Boolean = False
                For i = 0 To gvQuoReceipt.Rows.Count - 1
                    Dim chkSelect As New CheckBox
                    Dim hfQuotationReceiptID As New HiddenField
                    hfQuotationReceiptID = gvQuoReceipt.Rows(i).FindControl("hfQuotationReceiptID")
                    chkSelect = gvQuoReceipt.Rows(i).FindControl("chkSelect")
                    If chkSelect.Checked = True Then
                        blnQuo = True
                        If strQuotationReceiptID <> "" Then
                            strQuotationReceiptID = strQuotationReceiptID + "<" + hfQuotationReceiptID.Value.ToString
                        Else
                            strQuotationReceiptID = hfQuotationReceiptID.Value.ToString
                        End If
                    End If
                Next

                If blnQuo = False Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Purchase Order')</script>")
                    Exit Sub
                End If

                If gvQuoReceipt.Rows.Count > 0 Then
                    If blnQuo = False Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Quotation')</script>")
                        Exit Sub
                    End If
                End If

                If txtAmount.Text.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Amount')</script>")
                    txtAmount.Focus()
                    Exit Sub
                End If

                If CDec(txtAmount.Text.ToString) <= 0 Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Amount')</script>")
                    txtAmount.Focus()
                    Exit Sub
                End If



                'GET ATTACHED FILE NAME FROM GV
                For i = 0 To gvFile.Rows.Count - 1
                    Dim lbtnRemove As New LinkButton
                    lbtnRemove = gvFile.Rows(i).FindControl("lbtnRemove")
                    strFileName = lbtnRemove.CommandArgument.ToString
                Next


                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SavePO", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ApplyNewID", SqlDbType.NVarChar, 50)
                    .Parameters("@ApplyNewID").Value = hdfApplyNewID.Value.ToString
                    .Parameters.Add("@POID", SqlDbType.VarChar, 20)
                    If hdfPOID.Value.ToString = "" Then
                        .Parameters("@POID").Value = ""
                    Else
                        .Parameters("@POID").Value = hdfPOID.Value.ToString
                    End If

                    .Parameters.Add("@PONo", SqlDbType.NVarChar, 50)
                    .Parameters("@PONo").Value = txtPONo.Text.ToString
                    .Parameters.Add("@VendorID", SqlDbType.NVarChar, 50)
                    .Parameters("@VendorID").Value = ddlVendorPayee.SelectedValue.ToString
                    .Parameters.Add("@QuotationReceiptID", SqlDbType.NVarChar)
                    .Parameters("@QuotationReceiptID").Value = strQuotationReceiptID
                    .Parameters.Add("@TotalAmount", SqlDbType.Decimal, 18, 4)
                    .Parameters("@TotalAmount").Value = CDec(txtAmount.Text.ToString)
                    .Parameters.Add("@PODate", SqlDbType.NVarChar, 20)
                    .Parameters("@PODate").Value = strPODate
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@FileName", SqlDbType.NVarChar, 500)
                    .Parameters("@FileName").Value = strFileName
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                    .Parameters.Add("@Remarks", SqlDbType.NVarChar)
                    .Parameters("@Remarks").Value = txtRemarks.Text.ToString
                    .Parameters.Add("@Action", SqlDbType.NVarChar, 50)
                    .Parameters("@Action").Value = strAction


                    .Parameters.Add("@NewPOID", SqlDbType.VarChar, 20)
                    .Parameters("@NewPOID").Direction = ParameterDirection.Output
                    .Parameters.Add("@FilePath", SqlDbType.NVarChar, 500)
                    .Parameters("@FilePath").Direction = ParameterDirection.Output
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                    strPOID = .Parameters("@NewPOID").Value.ToString()
                    strFilePath = .Parameters("@FilePath").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else

                    If strAction <> "Generate" Then
                        If strFilePath <> "" Then

                            Dim strAlbumPath_Temp As String
                            strAlbumPath_Temp = hdfImageDocPath_Temp.Value.ToString + "/" + Session("DA_UserID").ToString

                            If FileIO.FileSystem.DirectoryExists(strFilePath) = False Then
                                FileIO.FileSystem.CreateDirectory(strFilePath)
                            End If

                            If FileIO.FileSystem.FileExists(strAlbumPath_Temp + "/" + strFileName) = True Then
                                If FileIO.FileSystem.FileExists(strFilePath + "/" + strFileName) = True Then
                                    FileIO.FileSystem.DeleteFile(strFilePath + "/" + strFileName)
                                End If

                                FileIO.FileSystem.CopyFile(strAlbumPath_Temp + "/" + strFileName, strFilePath + "/" + strFileName, True)
                            End If
                        End If
                    End If

                    ClearData()

                    If strAction = "Generate" Then
                        GetPurchaseOrder(hdfApplyNewID.Value.ToString, strPOID)
                        'GetPurchaseOrder(hdfApplyNewID.Value.ToString, "")
                    Else
                        GetPurchaseOrder(hdfApplyNewID.Value.ToString, "")
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                    End If


                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnAdd_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub gvPO_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvPO.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim strPOID As String = 0
                Dim lbtnRemove As New LinkButton

                lblMsg.Text = ""

                lbtnRemove = gvPO.Rows(e.RowIndex).FindControl("lbtnRemove")
                strPOID = lbtnRemove.CommandArgument.ToString

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeletePurchaseOrder", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@POID", SqlDbType.VarChar, 20)
                    .Parameters("@POID").Value = strPOID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else

                    btnClear_Click(sender, e)
                    GetPurchaseOrder(txtPurchaseNo.Text.ToString, "")
                    'lblMsg.Text = "Delete Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvPO_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub gvPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPO.SelectedIndexChanged
        Dim lbtnPONo As New LinkButton
        Dim strPOID As String = ""
        lbtnPONo = gvPO.SelectedRow.FindControl("lbtnPONo")
        strPOID = lbtnPONo.CommandArgument.ToString

        GetPurchaseOrder(txtPurchaseNo.Text.ToString, strPOID)


    End Sub

    Protected Sub btnAttach_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAttach.Click
        Dim blnContains As Boolean = False

        If fuPO.FileName <> "" Then
            If (Regex.IsMatch(fuPO.FileName, strFileExtension, RegexOptions.IgnoreCase)) = False Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid file format')</script>")
                'lblMsg.Text = "Invalid file format."
                Exit Sub
            End If

            ' Get the size in bytes of the file to upload.
            Dim fileSize As Integer = fuPO.PostedFile.ContentLength

            ' Allow only files less than 2,100,000 bytes (approximately 2 MB) to be uploaded.
            'If (fileSize < 2100000) Then
            If (fileSize < intFileSizeLimitKB) Then

                Dim strAlbumPath As String
                'If hdfPOID.Value.ToString = "" Then
                strAlbumPath = hdfImageDocPath_Temp.Value.ToString + "/" + Session("DA_UserID").ToString
                'Else
                '    strAlbumPath = hdfImageDocPath_Temp.Value.ToString + "/" + hdfPOID.Value.ToString
                'End If

                Dim strAlbumPath_Open As String
                strAlbumPath_Open = hdfImageDocPath_Temp_Open.Value.ToString + "/" + Session("DA_UserID").ToString



                hifSupDoc.Add(fuPO)
                Dim tbPO As DataTable = New DataTable("PO")
                tbPO.Columns.Add("Filename")
                tbPO.Columns.Add("DocPicLink")
                tbPO.Rows.Add(clsCF.RenameFileName(fuPO.FileName.ToString), "<a href=""../frmOpenFile.aspx?FilePath=" & strAlbumPath_Open & "/" & clsCF.RenameFileName(fuPO.FileName.ToString) & """ target=""_blank"">" & clsCF.RenameFileName(fuPO.FileName.ToString) & "</a>")

                Dim dsPO As DataSet = New DataSet("PO")
                dsPO.Tables.Add(tbPO)

                gvFile.Visible = True
                gvFile.DataSource = dsPO
                gvFile.DataMember = dsPO.Tables(0).TableName
                gvFile.DataBind()

                If gvFile.Rows.Count > 0 Then
                    fuPO.Enabled = False
                    btnAttach.Enabled = False
                Else
                    fuPO.Enabled = True
                    btnAttach.Enabled = True
                End If


                If FileIO.FileSystem.DirectoryExists(strAlbumPath) = True Then
                    FileIO.FileSystem.DeleteDirectory(strAlbumPath, FileIO.DeleteDirectoryOption.DeleteAllContents)
                End If

                FileIO.FileSystem.CreateDirectory(strAlbumPath)

                If FileIO.FileSystem.FileExists(strAlbumPath + "/" + clsCF.RenameFileName(fuPO.FileName.ToString)) = False Then
                    fuPO.PostedFile.SaveAs(strAlbumPath + "/" + clsCF.RenameFileName(fuPO.FileName.ToString))
                End If
            Else
                'lblMsg.Text = "Your file was not uploaded because it exceeds the " & intFileSizeLimit & " MB size limit."
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Your file was not uploaded because it exceeds the " & intFileSizeLimit & " MB size limit')</script>")
            End If
        End If
    End Sub

    Protected Sub ddlVendorPayee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVendorPayee.SelectedIndexChanged
        hdfPOID.Value = ""
        txtPONo.Text = ""
        txtAmount.Text = ""
        txtRemarks.Text = ""
        cldPODate.Clear()
        gvFile.Dispose()
        gvFile.Visible = False

        BindgQR(ddlVendorPayee.SelectedValue.ToString, hdfPOID.Value.ToString)
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SumAmount()
    End Sub

    Private Sub SumAmount()
        Dim i As Int32
        Dim decTotalAmount As Decimal = 0

        For i = 0 To gvQuoReceipt.Rows.Count - 1


            Dim chkSelect As CheckBox

            chkSelect = CType(gvQuoReceipt.Rows(i).FindControl("chkSelect"), CheckBox)

            Dim lblAmount As Label
            lblAmount = CType(gvQuoReceipt.Rows(i).FindControl("lblAmount"), Label)
            If chkSelect.Checked = True Then
                decTotalAmount = decTotalAmount + CDec(lblAmount.Text.ToString)
            End If

        Next
        txtAmount.Text = decTotalAmount

    End Sub

    Protected Sub btnGeneratePO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGeneratePO.Click
        SavePO("Generate")

        Session("PO_StationID") = ddlStation.SelectedValue.ToString
        Session("POID") = hdfPOID.Value.ToString
        Session("ApplyNewID") = hdfApplyNewID.Value.ToString
        Session("ParentURL") = Request.Url.ToString

        Dim strScript As String = ""
        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('frmPreviewPO.aspx?ParentForm=frmPurchaseOrder','GeneratePO','height=700, width=1150,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300')"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)


        'Dim strQuotationReceiptID As String = ""
        'Dim strMsg As String = ""
        'Dim sqlConn As New SqlConnection(strConnectionString)

        'Dim i As Integer
        'Dim blnQuo As Boolean = False
        'For i = 0 To gvQuoReceipt.Rows.Count - 1
        '    Dim chkSelect As New CheckBox
        '    Dim hfQuotationReceiptID As New HiddenField
        '    hfQuotationReceiptID = gvQuoReceipt.Rows(i).FindControl("hfQuotationReceiptID")
        '    chkSelect = gvQuoReceipt.Rows(i).FindControl("chkSelect")
        '    If chkSelect.Checked = True Then
        '        blnQuo = True
        '        If strQuotationReceiptID <> "" Then
        '            strQuotationReceiptID = strQuotationReceiptID + "<" + hfQuotationReceiptID.Value.ToString
        '        Else
        '            strQuotationReceiptID = hfQuotationReceiptID.Value.ToString
        '        End If
        '    End If
        'Next

        'If gvQuoReceipt.Rows.Count > 0 Then
        '    If blnQuo = False Then
        '        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Quotation')</script>")
        '        Exit Sub
        '    End If
        'End If

        'Try
        '    sqlConn.Open()
        '    Dim cmd As New SqlCommand("SP_GeneratePO", sqlConn)
        '    With cmd
        '        .CommandType = CommandType.StoredProcedure
        '        .Parameters.Add("@ApplyNewID", SqlDbType.NVarChar, 50)
        '        .Parameters("@ApplyNewID").Value = hdfApplyNewID.Value.ToString
        '        .Parameters.Add("@POID", SqlDbType.VarChar, 20)
        '        If hdfPOID.Value.ToString = "" Then
        '            .Parameters("@POID").Value = ""
        '        Else
        '            .Parameters("@POID").Value = hdfPOID.Value.ToString
        '        End If

        '        .Parameters.Add("@VendorID", SqlDbType.NVarChar, 50)
        '        .Parameters("@VendorID").Value = ddlVendorPayee.SelectedValue.ToString
        '        .Parameters.Add("@QuotationReceiptID", SqlDbType.NVarChar)
        '        .Parameters("@QuotationReceiptID").Value = strQuotationReceiptID
        '        .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
        '        .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
        '        .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
        '        .Parameters("@UserID").Value = Session("DA_UserID").ToString
        '        .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
        '        .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

        '        .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
        '        .Parameters("@Msg").Direction = ParameterDirection.Output
        '        .CommandTimeout = 0
        '        .ExecuteNonQuery()

        '        strMsg = .Parameters("@Msg").Value.ToString()
        '    End With

        '    If strMsg <> "" Then
        '        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
        '    Else
        '        btnClear_Click(sender, e)
        '        GetPurchaseOrder(hdfApplyNewID.Value.ToString, "")
        '        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Generate Successful')</script>")
        '    End If
        'Catch ex As Exception
        '    Me.lblMsg.Text = "btnAdd_Click: " & ex.Message.ToString
        'Finally
        '    sqlConn.Close()
        'End Try
    End Sub

    
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        'ClearData()
        GetPurchaseOrder(hdfApplyNewID.Value.ToString, "")
    End Sub
End Class
