<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmPreviewPO.aspx.vb" Inherits="Purchase_frmPreviewPO" %>


<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
<META HTTP-EQUIV="EXPIRES" CONTENT="0"> 
 
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body onunload="parent.document.location.reload();" >
    <form id="form1" runat="server">
        <asp:HiddenField ID="hdfPOPath" runat="server" />
        <asp:HiddenField ID="hdfParentURL" runat="server" />
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Height="600px" 
            Width="1100px" Font-Names="Verdana" Font-Size="8pt">
            <LocalReport ReportPath="RDLC\PO2.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                        Name="DataSet1_SP_GetPOPreviewData" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData"
            TypeName="DataSet1TableAdapters.SP_GetPOPreviewDataTableAdapter" OldValuesParameterFormatString="original_{0}">
            <SelectParameters>
                <asp:SessionParameter Name="ApplyNewID" SessionField="ApplyNewID" Type="String" />
                <asp:SessionParameter Name="POID" SessionField="POID" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
        &nbsp;
    </form>
</body>
</html>
