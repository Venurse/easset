Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Purchase_frmDetailSearch
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")

   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
          
            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "P0011")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Detail Search Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                    Else
                        Session("blnWrite") = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            BindDropDownList()

            
            'Make the first tab selected
            Menu1.Items(0).Selected = True
            'mv.SetActiveView(Tab1)
        Else
            If Session("DA_PayeeID") <> Nothing Then
                hdfPayee.Value = Session("DA_PayeeID").ToString
                Session("DA_PayeeID") = ""
            End If
            If Session("DA_PayeeName") <> Nothing Then
                txtPayee.Text = Session("DA_PayeeName").ToString
                Session("DA_PayeeName") = ""
            End If

            If Session("DA_PayeeType") <> Nothing Then
                hdfPayeeType.Value = Session("DA_PayeeType").ToString
                Session("DA_PayeeType") = ""
            End If

        End If
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.NVarChar, 500)
                    .Parameters("@ReferenceType").Value = "[AccessStation]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)

                If Session("DA_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("DA_StationID").ToString
                End If


            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim strDateFrom As String = ""
                Dim strDateTo As String = ""
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand



                strDateFrom = Format(cldDateFrom.SelectedDate, "dd MMM yyyy").ToString
                strDateTo = Format(cldDateTo.SelectedDate, "dd MMM yyyy").ToString

                If strDateFrom.Contains("01 Jan 0001") = True Then
                    strDateFrom = ""
                End If
                If strDateTo.Contains("01 Jan 0001") = True Then
                    strDateTo = ""
                End If

                Session("DA_StationID") = ddlStation.SelectedValue.ToString

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_DetailSearch")

                With MyCommand

                    .Parameters.Add("@Station", SqlDbType.VarChar, 3)
                    .Parameters("@Station").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@Payee", SqlDbType.NVarChar, 50)
                    .Parameters("@Payee").Value = hdfPayee.Value.ToString
                    .Parameters.Add("@PayeeType", SqlDbType.NVarChar, 50)
                    .Parameters("@PayeeType").Value = hdfPayeeType.Value.ToString
                    .Parameters.Add("@AssetNo", SqlDbType.NVarChar, 50)
                    .Parameters("@AssetNo").Value = txtAssetNo.Text.ToString
                    .Parameters.Add("@DocType", SqlDbType.NVarChar, 50)
                    .Parameters("@DocType").Value = ddlDocType.SelectedValue.ToString
                    .Parameters.Add("@DocNo", SqlDbType.NVarChar, 50)
                    .Parameters("@DocNo").Value = txtDocNo.Text.ToString
                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = txtPurchaseNo.Text.ToString
                    .Parameters.Add("@DocDateFrom", SqlDbType.NVarChar, 50)
                    .Parameters("@DocDateFrom").Value = strDateFrom
                    .Parameters.Add("@DocDateTo", SqlDbType.NVarChar, 50)
                    .Parameters("@DocDateTo").Value = strDateTo
                    .Parameters.Add("@AssetID", SqlDbType.NVarChar, 50)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "Asset"

                gvAsset.DataSource = MyData
                gvAsset.DataMember = MyData.Tables(0).TableName
                gvAsset.DataBind()

                gvQuotRcpt.DataSource = MyData
                gvQuotRcpt.DataMember = MyData.Tables(1).TableName
                gvQuotRcpt.DataBind()

                gvPO.DataSource = MyData
                gvPO.DataMember = MyData.Tables(2).TableName
                gvPO.DataBind()

                gvInvoice.DataSource = MyData
                gvInvoice.DataMember = MyData.Tables(3).TableName
                gvInvoice.DataBind()

                gvDO.DataSource = MyData
                gvDO.DataMember = MyData.Tables(4).TableName
                gvDO.DataBind()

                gvPayment.DataSource = MyData
                gvPayment.DataMember = MyData.Tables(5).TableName
                gvPayment.DataBind()


            Catch ex As Exception
                Me.lblMsg.Text = "btnSearch_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub Menu1_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles Menu1.MenuItemClick
        'mv.ActiveViewIndex = Int32.Parse(e.Item.Value)
        'Dim TabName As New View
        'TabName.ID = e.Item.Value.ToString
        If e.Item.Value.ToString = "Tab1" Then
            mv.SetActiveView(Tab1)
        ElseIf e.Item.Value.ToString = "Tab2" Then
            mv.SetActiveView(Tab2)
        ElseIf e.Item.Value.ToString = "Tab3" Then
            mv.SetActiveView(Tab3)
        ElseIf e.Item.Value.ToString = "Tab4" Then
            mv.SetActiveView(Tab4)
        ElseIf e.Item.Value.ToString = "Tab5" Then
            mv.SetActiveView(Tab5)
        ElseIf e.Item.Value.ToString = "Tab6" Then
            mv.SetActiveView(Tab6)
        End If


        'Dim i As Integer
        ''Make the selected menu item reflect the correct imageurl
        'For i = 0 To Menu1.Items.Count - 1
        '    If i = e.Item.Value Then
        '        'Menu1.Items(i).ImageUrl = "selectedtab.gif"
        '    Else
        '        'Menu1.Items(i).ImageUrl = "unselectedtab.gif"
        '    End If
        'Next
    End Sub

    Protected Sub gvAsset_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvAsset.SelectedIndexChanged
        Dim strID As String
        strID = gvAsset.SelectedDataKey.Value.ToString
        Session("AssetID") = ""
        Session("AssetID") = strID
        Response.Redirect("~/Asset/frmAssetDetail.aspx")

    End Sub

    Protected Sub gvPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPO.SelectedIndexChanged

    End Sub

    Protected Sub btnVendor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVendor.Click
        Dim strScript As String

        Session("DA_PayeeID") = Nothing
        Session("DA_PayeeCode") = Nothing
        Session("DA_PayeeName") = Nothing

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmPayee.aspx?ParentForm=frmDetailSearch','Vendor_Payee','height=750, width=700,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub
End Class
