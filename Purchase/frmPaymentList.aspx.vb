Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Purchase_frmPaymentList
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack() = False Then

            Session("DA_PayeeID") = Nothing
            Session("DA_PayeeCode") = Nothing
            Session("DA_PayeeName") = Nothing

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            'gvFile.Columns(0).Visible = False
            btnAddPayment.Enabled = False
            gvPaymentList.Columns(0).Visible = False


            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "P0007")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Payment List Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnAddPayment.Enabled = True
                        gvPaymentList.Columns(0).Visible = True

                    Else
                        Session("blnWrite") = False
                        btnAddPayment.Enabled = False
                        gvPaymentList.Columns(0).Visible = False

                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            BindDropDownList()
            GetPaymentList()
        Else
            If Session("DA_PayeeID") <> Nothing Then
                hdfPayee.Value = Session("DA_PayeeID").ToString
                Session("DA_PayeeID") = ""
            End If
            If Session("DA_PayeeName") <> Nothing Then
                txtPayee.Text = Session("DA_PayeeName").ToString
                Session("DA_PayeeName") = ""
            End If

            If Session("DA_PayeeType") <> Nothing Then
                hdfPayeeType.Value = Session("DA_PayeeType").ToString
                Session("DA_PayeeType") = ""
            End If
        End If
    End Sub

    Private Sub BindDropDownList()
        Try
            'Dim dsStation As New DataSet
            'Dim MyData As New DataSet
            'Dim MyAdapter As SqlDataAdapter
            'Dim MyCommand As SqlCommand
            'Dim MyConnection As New SqlConnection(strConnectionString)

            'MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

            'With MyCommand

            '    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
            '    .Parameters("@ReSM").Value = strReSM
            '    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
            '    .Parameters("@ReferenceType").Value = "[AccessStation]"
            '    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
            '    .Parameters("@UserID").Value = Session("DA_UserID").ToString
            '    .CommandType = CommandType.StoredProcedure
            '    .Connection = MyConnection
            'End With

            'MyAdapter = New SqlDataAdapter(MyCommand)
            'MyAdapter.Fill(MyData)
            'MyData.Tables(0).TableName = "AccessStation"

            Dim strIncludeStationList As String
            strIncludeStationList = Session("AccessStation").ToString
            clsCF.Bind_dllStation(strIncludeStationList, ddlStation)


            If Session("DA_StationID").ToString <> "" Then
                ddlStation.SelectedValue = Session("DA_StationID").ToString
            End If

        Catch ex As Exception
            Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
        Finally
            'sqlConn.Close()
        End Try
    End Sub

    Protected Sub btnAddPayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPayment.Click
        Response.Redirect("~/Purchase/frmPayment.aspx")
    End Sub

    Private Sub GetPaymentList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand


                If ddlStation.SelectedValue.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Station')</script>")
                    Exit Sub
                End If

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetPaymentList")

                With MyCommand

                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@VoucherNo", SqlDbType.VarChar, 100)
                    .Parameters("@VoucherNo").Value = txtPaymentVoucherNo.Text.ToString
                    .Parameters.Add("@Payee", SqlDbType.NVarChar, 50)
                    .Parameters("@Payee").Value = hdfPayee.Value.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "PaymentList"

                gvPaymentList.DataSource = MyData
                gvPaymentList.DataMember = MyData.Tables(0).TableName
                gvPaymentList.DataBind()


            Catch ex As Exception
                Me.lblMsg.Text = "GetPaymentList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnQuery_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQuery.Click
        Session("DA_StationID") = ddlStation.SelectedValue.ToString
        GetPaymentList()
    End Sub

    Protected Sub gvPaymentList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPaymentList.SelectedIndexChanged
        'Dim hdfStationID As New HiddenField
        'hdfStationID = gvPaymentList.SelectedRow.FindControl("hdfStationID")

        'Session("PaymentStationID") = ""
        'Session("PaymentStationID") = hdfStationID.Value.ToString

        Session("PaymentID") = ""
        Session("PaymentID") = gvPaymentList.SelectedValue.ToString

        Response.Redirect("~/Purchase/frmPaymentDetail.aspx")
    End Sub

    Protected Sub btnVendor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVendor.Click
        Dim strScript As String

        Session("DA_PayeeID") = Nothing
        Session("DA_PayeeCode") = Nothing
        Session("DA_PayeeName") = Nothing

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmPayee.aspx?ParentForm=frmPaymentList','Vendor_Payee','height=750, width=700,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub
End Class
