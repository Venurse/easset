<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmPurchaseList.aspx.vb" Inherits="Purchase_frmPurchaseList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Purchase List</title>
</head>
<body>
    <form id="frmPurchaseList" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Purchase List"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <table>
                        <tr>
                            <td style="width: 150px">
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Station"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:DropDownList ID="ddlStation" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                    </asp:DropDownList></td>
                            <td style="width: 50px">
                            </td>
                            <td style="width: 100px">
                    <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Status"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td style="width: 10px">
                                <asp:DropDownList ID="ddlStatus" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                </asp:DropDownList></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Purchase No"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <asp:TextBox ID="txtPurchaseNo" runat="server" Font-Names="Verdana" Font-Size="X-Small" Width="150px"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                    <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Year"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlYear" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                </asp:DropDownList></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label6" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Purchase Description"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <asp:TextBox ID="txtPurchaseDesc" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="346px"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                            </td>
                            <td>
                            </td>
                            <td colspan="5">
                    <table style="width: 335px">
                        <tr>
                            <td style="width: 150px">
                                <asp:Button ID="btnQuery" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Query"
                                    Width="65px" /></td>
                            <td style="width: 150px">
                            </td>
                            <td style="width: 150px">
                            </td>
                        </tr>
                    </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <asp:GridView ID="gvPurchaseList" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" Font-Names="Verdana" Font-Size="X-Small" AutoGenerateColumns="False" DataKeyNames="ApplyNewID">
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="#999999" />
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <Columns>
                            <asp:TemplateField HeaderText="Status">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Status") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                    <asp:HiddenField ID="hdfStatus" runat="server" Value='<%# Bind("StatusCode") %>' />
                                    <asp:HiddenField ID="hdfApplyNewType" runat="server" Value='<%# Bind("ApplyNewType") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="StationCode" HeaderText="Station" />
                            <asp:TemplateField HeaderText="Purchase No">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    &nbsp;<asp:LinkButton ID="lbtnPurchaseNo" runat="server" CommandArgument='<%# Bind("ApplyNewID") %>'
                                        Text='<%# Bind("ApplyNewNo") %>' CommandName="SELECT"></asp:LinkButton>
                                    <asp:HiddenField ID="hdfSend_eReq" runat="server" Value='<%# Bind("Send_eReq") %>' />
                                    <asp:HiddenField ID="hdfPost_eFMS" runat="server" Value='<%# Bind("Post_eFMS") %>' />
                                    <asp:HiddenField ID="hdfeReq_ACK" runat="server" />
                                    <asp:HiddenField ID="hdfPage" runat="server" Value='<%# Bind("PAGE") %>' />
                                    <asp:HiddenField ID="hdfStationID" runat="server" Value='<%# Bind("StationID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Purchase_Description" HeaderText="Purchase Description" />
                            <asp:BoundField DataField="STATUS_FLOW" HeaderText=" Process Flow" >
                                <ItemStyle ForeColor="#00C000" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CreatedBy" HeaderText="Created By" />
                            <asp:BoundField DataField="CreatedOn" HeaderText="Created Date" />
                            <asp:TemplateField HeaderText="View Detail" Visible="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnViewDetail" runat="server" CommandArgument='<%# Bind("ApplyNewID") %>'
                                        CommandName="ViewDetail" Text="View"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
