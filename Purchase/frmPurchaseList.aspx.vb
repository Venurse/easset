Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Purchase_frmPurchaseList
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If IsPostBack() = False Then
            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False


            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "P0002")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Purchase List Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                    Else
                        Session("blnWrite") = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            BindDropDownList()
            Session("DA_USERLIST") = ""


            Dim strApplyNewID_QS As String = ""
            If Request.QueryString("ApplyNewID") <> Nothing Then
                strApplyNewID_QS = Request.QueryString("ApplyNewID").ToString

                If strApplyNewID_QS <> "" Then
                    txtPurchaseNo.Text = strApplyNewID_QS
                    btnQuery_Click(sender, e)
                End If
            End If

        Else

        End If
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[AccessStation],[Year],[PurchaseStatus]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"
                MyData.Tables(1).TableName = "Year"

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)


                'If MyData.Tables(0).Rows(0)("AccessStation").ToString <> "" Then
                '    dsStation = DIMERCO.SDK.Utilities.LSDK.getStationDataByWithList(MyData.Tables(0).Rows(0)("AccessStation").ToString)
                'Else
                '    dsStation = DIMERCO.SDK.Utilities.ReSM.GetStationList()
                'End If
                'Dim dtView As DataView = dsStation.Tables(0).DefaultView
                'dtView.Sort = "StationCode ASC"

                'ddlStation.DataSource = dtView
                'ddlStation.DataValueField = dtView.Table.Columns("StationID").ToString
                'ddlStation.DataTextField = dtView.Table.Columns("StationCode").ToString
                'ddlStation.DataBind()

                If Session("DA_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("DA_StationID").ToString
                End If

                ddlYear.DataSource = MyData.Tables(1)
                ddlYear.DataValueField = MyData.Tables(1).Columns("Year").ToString
                ddlYear.DataTextField = MyData.Tables(1).Columns("Year").ToString
                ddlYear.DataBind()
                ddlYear.SelectedValue = Now.Year

                ddlStatus.DataSource = MyData.Tables(2)
                ddlStatus.DataValueField = MyData.Tables(2).Columns("StatusID").ToString
                ddlStatus.DataTextField = MyData.Tables(2).Columns("Status").ToString
                ddlStatus.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnQuery_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQuery.Click
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                Session("DA_StationID") = ddlStation.SelectedValue.ToString

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetPurchaseList")

                With MyCommand

                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@PurchaseNo", SqlDbType.VarChar, 20)
                    .Parameters("@PurchaseNo").Value = txtPurchaseNo.Text.ToString
                    .Parameters.Add("@PurchaseDesc", SqlDbType.NVarChar, 300)
                    .Parameters("@PurchaseDesc").Value = txtPurchaseDesc.Text.ToString
                    .Parameters.Add("@Status", SqlDbType.VarChar, 1)
                    .Parameters("@Status").Value = ddlStatus.SelectedValue.ToString
                    .Parameters.Add("@Year", SqlDbType.Int)
                    If ddlYear.SelectedValue.ToString = "" Then
                        .Parameters("@Year").Value = DBNull.Value
                    Else
                        .Parameters("@Year").Value = CInt(ddlYear.SelectedValue.ToString)
                    End If

                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "PurchaseList"

                gvPurchaseList.DataSource = MyData
                gvPurchaseList.DataMember = MyData.Tables(0).TableName
                gvPurchaseList.DataBind()


            Catch ex As Exception
                Me.lblMsg.Text = "btnQuery_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub gvPurchaseList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPurchaseList.SelectedIndexChanged
        Dim strID As String = ""
        strID = gvPurchaseList.SelectedDataKey.Value.ToString
        Session("ApplyNewID") = strID

        Dim hdfStatus As New HiddenField
        hdfStatus = gvPurchaseList.SelectedRow.FindControl("hdfStatus")


        Dim hdfSend_eReq As New HiddenField
        hdfSend_eReq = gvPurchaseList.SelectedRow.FindControl("hdfSend_eReq")

        Dim hdfPost_eFMS As New HiddenField
        hdfPost_eFMS = gvPurchaseList.SelectedRow.FindControl("hdfPost_eFMS")

        Dim hdfPage As New HiddenField
        hdfPage = gvPurchaseList.SelectedRow.FindControl("hdfPage")

        Dim hdfApplyNewType As New HiddenField
        hdfApplyNewType = gvPurchaseList.SelectedRow.FindControl("hdfApplyNewType")



        If hdfPage.Value.ToString = "Purchase/frmApplyNew.aspx" Then
            Response.Redirect("~/Purchase/frmApplyNew.aspx?ANT=" & hdfApplyNewType.Value.ToString & "&ID=" & strID)
        Else
            Response.Redirect("~/" & hdfPage.Value.ToString & "")
        End If

    End Sub

    Protected Sub gvPurchaseList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvPurchaseList.RowCommand
        Dim strID As String
        strID = e.CommandArgument.ToString()

        If e.CommandName = "ViewDetail" Then
            Session("ApplyNewID") = strID
            Response.Redirect("~/Purchase/frmPurchaseDetail.aspx")
        End If

    End Sub
End Class
