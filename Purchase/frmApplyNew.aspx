<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmApplyNew.aspx.vb" Inherits="Purchase_frmApplyNew" ValidateRequest="false"%>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" Namespace="eWorld.UI" TagPrefix="ew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=8, IE=9, chrome=1"/>
    <title>Apply New</title>
    <script type="text/javascript">
         function showDate() 
        {
            var month_names = new Array("Jan", "Feb", "Mar", 
            "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
            "Oct", "Nov", "Dec");

            var d = new Date();
            var curr_day = d.getDay();
            var curr_date = d.getDate();
            var hours = d.getHours();
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            
            var curr_hours;
            var curr_minutes;
            var curr_seconds;

            if(hours<10)
            {
                curr_hours = "0" + hours;
            }
            else
            {
                curr_hours = hours;
            }
            
            if(minutes < 10)
            {
                curr_minutes = "0" + minutes;
            }
             else
            {
                curr_minutes = minutes;
            }
            
           if(seconds < 10)
            {
                curr_seconds = "0" + seconds;
            }
             else
            {
                curr_seconds = seconds;
            }

            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
 
            document.getElementById("hdfCurrentDateTime").value = curr_date + " " +  month_names[curr_month] +  " " + curr_year + " " + curr_hours + ":" + curr_minutes + ":" + curr_seconds
         }
     </script>
</head>
<body>
    <form id="frmApplyNew" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Add New Purchase"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:HiddenField ID="hdfCurrentDateTime" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <table>
                        <tr>
                            <td style="width: 120px">
                                &nbsp;</td>
                            <td style="font-size: 12pt; width: 1px; font-family: Times New Roman">
                            </td>
                            <td style="font-size: 12pt; font-family: Times New Roman">
                    <asp:HiddenField ID="hdfApplyNewID" runat="server" />
                                <asp:HiddenField ID="hdfMaintenanceID" runat="server" />
                                <asp:HiddenField ID="hdfAssetID" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                    <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Station"></asp:Label></td>
                            <td style="font-size: 12pt; width: 1px; font-family: Times New Roman">
                                :</td>
                            <td style="font-size: 12pt; font-family: Times New Roman">
                    <asp:DropDownList ID="ddlStation" runat="server" Font-Names="Verdana" Font-Size="X-Small" AutoPostBack="True">
                    </asp:DropDownList>
                                <asp:HiddenField ID="hdfWithBudgetList" runat="server" />
                                <asp:HiddenField ID="hdfWithOEB" runat="server" />
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td style="width: 100px">
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="To"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlTo" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtTo" runat="server" Visible="False"></asp:TextBox>
                                <asp:HiddenField ID="hdfTo" runat="server" Visible="False" />
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td style="width: 100px">
                    <asp:LinkButton ID="lbtnCC" runat="server" Font-Names="Verdana" Font-Size="X-Small">CC</asp:LinkButton></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:GridView ID="gvCC" runat="server" AutoGenerateColumns="False" Font-Names="Verdana" Font-Size="X-Small" DataKeyNames="UserID">
                        <Columns>
                            <asp:TemplateField HeaderText="Remove">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnRemove" runat="server" CommandArgument='<%# Bind("UserID") %>' CommandName="Select">Remove</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <asp:Label ID="Label8" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:HiddenField ID="hdfCC" runat="server" />
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Type"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:Label ID="lblType" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Purchase Description"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:TextBox ID="txtPurchaseDes" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        MaxLength="300" Width="500px"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:Button ID="btnQuoReceipt" runat="server" Text="Add Quotation / Receipt" Width="167px" OnClientClick="showDate();" /></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:GridView ID="gvQuoReceipt" runat="server" BackColor="White" BorderColor="LightGray"
                        BorderStyle="Solid" BorderWidth="1px" CellPadding="4" AutoGenerateColumns="False" Font-Names="Verdana" Font-Size="X-Small" DataKeyNames="QuotationCompareID">
                        <RowStyle BackColor="White" ForeColor="#330099" />
                        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                        <Columns>
                            <asp:TemplateField HeaderText="Remove">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnRemove" runat="server" CommandArgument='<%# Bind("QuotationCompareID") %>' CommandName="Delete" OnClientClick="return confirm('Are you sure you want to remove this Quotation\Receipt Group?');" Enabled='<%# Bind("AllowEditDelete") %>'>Remove</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnEdit" runat="server" CommandArgument='<%# Bind("QuotationCompareID") %>' CommandName="SelectEdit" OnClientClick="showDate();" Enabled='<%# Bind("AllowEditDelete") %>'>Edit</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="QuotationCompareID" HeaderText="Compare ID" />
                            <asp:BoundField DataField="PayeeName" HeaderText="Vendor/Payee" />
                            <asp:BoundField DataField="TotalAmount" HeaderText="Total Amount" />
                            <asp:BoundField DataField="QuotationReceiptNo" HeaderText="Quotation / Receipt No" />
                            <asp:TemplateField HeaderText="View Detail">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnViewDetail" runat="server" CommandArgument='<%# Bind("QuotationCompareID") %>' CommandName="View">View Detail</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    &nbsp;</td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <asp:GridView ID="gvDetail" runat="server" AutoGenerateColumns="False" BackColor="White"
                        BorderColor="#C0C0FF" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" Font-Names="Verdana"
                        Font-Size="X-Small" CaptionAlign="Top">
                        <RowStyle BackColor="White" ForeColor="#003399" />
                        <Columns>
                            <asp:TemplateField HeaderText="Compare ID" Visible="False">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CompareID") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("CompareID") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quotation/Receipt No">
                                <ItemTemplate>
                                    <asp:Label ID="lblQuotationReceiptNo" runat="server" Text='<%# Bind("QuotationReceiptNo") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category">
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("Category") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Brand Model">
                                <ItemTemplate>
                                    <asp:Label ID="lblBrand" runat="server" Text='<%# Bind("BrandName") %>'></asp:Label>
                                    <asp:Label ID="lblModel" runat="server" Text='<%# Bind("Model") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Specification">
                                <ItemTemplate>
                                    <asp:Label ID="lblSpecification" runat="server" Text='<%# Bind("Specification") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Top" Width="40%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quantity">
                                <ItemTemplate>
                                    <asp:Label ID="lblQuantity" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Price Per Unit">
                                <ItemTemplate>
                                    <asp:Label ID="lblPricePerUnit" runat="server" Text='<%# Bind("PricePerUnit") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type">
                                <ItemTemplate>
                                    <asp:Label ID="lblType" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remarks">
                                <ItemTemplate>
                                    <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Top" Width="20%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Budget Status">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Approval_Desc") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="Red" Text='<%# Bind("Approval_Desc") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                        <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                    </asp:GridView>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:HiddenField ID="hdfItemCount" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td style="width: 100px">
                    <asp:Label ID="Label9" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Reason/Issue"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:TextBox ID="txtReason" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Height="80px" TextMode="MultiLine" Width="500px"></asp:TextBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td style="width: 100px">
                    <asp:Label ID="Label7" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Remarks"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <FTB:FreeTextBox ID="txtRemarks" runat="server" AllowHtmlMode="False" AutoGenerateToolbarsFromString="True"
                                    AutoParseStyles="False" BreakMode="LineBreak" ButtonDownImage="False" ClientSideTextChanged=""
                                    DownLevelMode="TextArea" EnableHtmlMode="False" EnableViewState="False" Focus="True"
                                    Height="200px" PasteMode="Disabled" ToolbarLayout="Bold,Italic,Underline,Strikethrough;Superscript,Subscript|BulletedList,NumberedList,Indent,Outdent|FontFacesMenu,FontSizesMenu"
                                    ToolbarStyleConfiguration="Office2003" Width="650px">
                                </FTB:FreeTextBox>
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td style="width: 100px">
                            </td>
                            <td style="width: 1px">
                            </td>
                            <td>
                                &nbsp;<asp:TextBox ID="txtRemarks2" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Height="150px" TextMode="MultiLine" Width="500px" Visible="False"></asp:TextBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                            </td>
                            <td style="width: 1px">
                            </td>
                            <td>
                    <table style="width: 450px">
                        <tr>
                            <td style="width: 150px">
                                <asp:Button ID="btnSave" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Draft/Save"
                                    Width="100px" OnClientClick="showDate();" /></td>
                            <td style="width: 150px">
                                <asp:Button ID="btnSend" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Send"
                                    Width="85px" OnClientClick="showDate();" /></td>
                            <td style="width: 150px">
                                <asp:Button ID="btnDelete" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Delete"
                                    Width="85px" OnClientClick="showDate();" /></td>
                            <td style="width: 150px">
                                <asp:Button ID="btnCancel" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Cancel"
                                    Width="85px" OnClientClick="showDate();" /></td>
                        </tr>
                    </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <table>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td style="width: 100px">
                                <asp:Label ID="Label10" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Approval History"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:GridView ID="gvApprovalHistory" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                    Font-Names="Verdana" Font-Size="X-Small" ForeColor="#333333" GridLines="None">
                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                    <Columns>
                                        <asp:BoundField DataField="ApprovedBy" HeaderText="Approval By" />
                                        <asp:BoundField DataField="ApprovedOn" HeaderText="Approval Date" />
                                        <asp:BoundField DataField="Reason" HeaderText="Reason" />
                                        <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                        <asp:BoundField DataField="Status" HeaderText="Status" />
                                        <asp:BoundField DataField="QuotationCompareID" HeaderText="Quotation Compare ID" />
                                        <asp:BoundField DataField="QuotationReceiptNo" HeaderText="Quotation\Receipt No" />
                                        <asp:BoundField DataField="Category" HeaderText="Category" />
                                    </Columns>
                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
