Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Purchase_frmQuotationReceipt
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Public Shared hifSupDoc As ArrayList = New ArrayList()
    Dim strFileExtension As String = ConfigurationSettings.AppSettings("DocExtension")
    Dim intFileSizeLimitKB As Integer = ConfigurationSettings.AppSettings("intFileSizeLimitKB")
    Dim intFileSizeLimit As Integer = ConfigurationSettings.AppSettings("intFileSizeLimit")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMsg.Text = ""


        Me.Page.MaintainScrollPositionOnPostBack = True

        If IsPostBack() = False Then

            Session("ExistingAssetID") = ""
            Session("ExistingAssetPurchaseDate") = ""

            'chkSpecialApproval.Visible = False
            Label33.Visible = False
            Label31.Visible = False
            ddlSpecialRequest.Visible = False
            ddlSpecialReqCategory.Visible = False
            pnlExistingAsset.Visible = False

            If Session("CurrentDatetime") <> Nothing Then
                hdfCurrentDateTime.Value = Session("CurrentDatetime").ToString
            End If

            txtOwner.Enabled = False

            hdfPayee.Value = ""
            txtPayee.Text = ""
            hdfPayeeType.Value = ""
            hdfStationID.Value = ""
            hdfQuotationPricePointCtrl.Value = ""

            If Session("AN_QuotationCompareID") <> Nothing Then
                lblQuotationCompareID.Text = Session("AN_QuotationCompareID").ToString
            End If

            If Session("ApplyNewID") <> Nothing Then
                hdfApplyNewID.Value = Session("ApplyNewID").ToString
            End If

            If Session("AN_StationID") <> Nothing Then
                hdfStationID.Value = Session("AN_StationID").ToString
                'Session("AN_StationID") = ""
            End If

            If Session("AN_ApplyNewType") <> Nothing Then
                hdfApplyNewType.Value = Session("AN_ApplyNewType").ToString
            End If

            BindDropDownList()
            BindJobFunction()
            GetStationLocation(hdfStationID.Value.ToString)

            GetQuoReceiptCompareDetail(hdfApplyNewID.Value.ToString, lblQuotationCompareID.Text.ToString)

            Me.btnSave.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnSave, "", "btnCancel_H", "btnAdd", "btnClear", "btnReturn", "btnCancel"))
            Me.btnCancel_H.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnCancel_H, "", "btnSave", "btnAdd", "btnClear", "btnReturn", "btnCancel"))
            Me.btnAdd.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnAdd, "", "btnSave", "btnCancel_H", "btnClear", "btnReturn", "btnCancel"))
            Me.btnClear.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnClear, "", "btnSave", "btnCancel_H", "btnAdd", "btnReturn", "btnCancel"))
            Me.btnCancel.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnCancel, "", "btnSave", "btnCancel_H", "btnAdd", "btnReturn", "btnClear"))

        Else
            If Session("AN_QuotationCompareID") <> Nothing Then
                lblQuotationCompareID.Text = Session("AN_QuotationCompareID").ToString
            End If

            If Session("ApplyNewID") <> Nothing Then
                hdfApplyNewID.Value = Session("ApplyNewID").ToString
            End If


            If Session("DA_PayeeID") <> Nothing Then
                hdfPayee.Value = Session("DA_PayeeID").ToString
                Session("DA_PayeeID") = ""
            End If
            If Session("DA_PayeeName") <> Nothing Then
                txtPayee.Text = Session("DA_PayeeName").ToString
                Session("DA_PayeeName") = ""
            End If

            If Session("DA_PayeeType") <> Nothing Then
                If Session("DA_PayeeType").ToString = "Vendor" Then
                    txtShopName.Enabled = False
                    ddlVendorContactPerson.Enabled = True
                    BindContactPerson()
                    'If ddlVendorContactPerson.Items.Count > 0 Then
                    '    If Session("DA_PayeeType").ToString <> hdfPayeeType.Value.ToString Then
                    '        BindContactPerson()
                    '    End If
                    'Else
                    '    BindContactPerson()
                    'End If

                Else
                    ddlVendorContactPerson.Enabled = False
                    txtShopName.Enabled = True
                End If
                hdfPayeeType.Value = Session("DA_PayeeType").ToString
                Session("DA_PayeeType") = ""
            End If

            If Session("DA_USERLIST").ToString <> "" Then
                hdfUserID.Value = Session("DA_USERLIST").ToString
                If hdfUserID.Value.ToString <> "" Then
                    Dim dsOwnerInfo As DataSet = clsCF.GetOwnerInformation(hdfUserID.Value.ToString)
                    If dsOwnerInfo.Tables.Count > 0 Then
                        If dsOwnerInfo.Tables(0).Rows.Count > 0 Then
                            txtJobGrade.Text = dsOwnerInfo.Tables(0).Rows(0)("JobGrade").ToString
                            txtOwner.Text = dsOwnerInfo.Tables(0).Rows(0)("UserName").ToString

                            'Dim i As Integer
                            'For i = 0 To dsOwnerInfo.Tables(0).Columns.Count - 1
                            '    Dim strclmname As String = dsOwnerInfo.Tables(0).Columns(i).ColumnName
                            'Next

                            If dsOwnerInfo.Tables(0).Rows(0)("IsInternalAudit").ToString = "Y" Then
                                chkInternalAudit.Checked = True
                            Else
                                chkInternalAudit.Checked = False
                            End If

                            If dsOwnerInfo.Tables(0).Rows(0)("IsSeedMember").ToString = "Y" Then
                                chkMIS_Seed_Member.Checked = True
                            Else
                                chkMIS_Seed_Member.Checked = False
                            End If

                        End If
                    End If
                End If
                Session("DA_USERLIST") = ""
            End If

            If Session("ExistingAssetID").ToString() <> "" Then
                txtExistingAsset.Text = Session("ExistingAssetID").ToString()
                Session("ExistingAssetID") = ""
            End If

            If Session("ExistingAssetPurchaseDate").ToString() <> "" Then
                cldEAssetPurchaseDate.SelectedDate = Session("ExistingAssetPurchaseDate").ToString()
                Session("ExistingAssetPurchaseDate") = ""
            End If
        End If

        

    End Sub

    Private Sub BindJobFunction()
        Try
            Dim dtViewJobFunction As DataView = clsCF.GeteRequistionJobFunction()
            ddlJobFunction.DataSource = dtViewJobFunction
            ddlJobFunction.DataValueField = dtViewJobFunction.Table.Columns("Job_Function").ToString
            ddlJobFunction.DataTextField = dtViewJobFunction.Table.Columns("Job_Function").ToString
            ddlJobFunction.DataBind()
        Catch ex As Exception
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + ex.Message.ToString + "')</script>")
            Exit Sub
        End Try
    End Sub

    Private Sub GetStationLocation(ByVal strStation As String)
        Try
            If strStation = "" Then
                lblMsg.Text = "Please select Station"
                Exit Sub
            End If

            Dim dsLocation As DataSet = clsCF.GetStationLocation(strStation)
            ddlLocation.DataSource = dsLocation
            ddlLocation.DataValueField = dsLocation.Tables(0).Columns("LocationID").ToString
            ddlLocation.DataTextField = dsLocation.Tables(0).Columns("Location").ToString
            ddlLocation.DataBind()

        Catch ex As Exception
            Me.lblMsg.Text = "GetStationLocation: " & ex.Message.ToString
        Finally
            'sqlConn.Close()
        End Try
    End Sub

    Private Sub GetCategoryUser()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetCategoryUser")

                With MyCommand
                    .Parameters.Add("@QuotationReceiptID", SqlDbType.VarChar, 20)
                    .Parameters("@QuotationReceiptID").Value = ddlQuoRecptNo.SelectedValue.ToString
                    .Parameters.Add("@CategoryID", SqlDbType.BigInt)
                    If ddlSubCategory.SelectedValue.ToString = "" Then
                        If ddlCategory.SelectedValue.ToString = "0" Then
                            .Parameters("@CategoryID").Value = DBNull.Value
                        Else
                            .Parameters("@CategoryID").Value = ddlCategory.SelectedValue.ToString
                        End If
                    Else
                        If ddlSubCategory.SelectedValue.ToString = "0" Then
                            .Parameters("@CategoryID").Value = ddlCategory.SelectedValue.ToString
                        Else
                            .Parameters("@CategoryID").Value = ddlSubCategory.SelectedValue.ToString
                        End If
                    End If
                    
                    .Parameters.Add("@ID", SqlDbType.BigInt)
                    If lblID.Text.ToString = "" Then
                        .Parameters("@ID").Value = 0
                    Else
                        .Parameters("@ID").Value = CInt(lblID.Text.ToString)
                    End If

                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)

                gvItem.Visible = True
                gvItem.DataSource = MyData
                gvItem.DataMember = MyData.Tables(0).TableName
                gvItem.DataBind()

                'If gvItem.Rows.Count.ToString <> "0" Then
                '    txtQty.Text = gvItem.Rows.Count.ToString
                'End If

            Catch ex As Exception
                Me.lblMsg.Text = "GetCategoryUser: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub GetPricePointCtrl()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetPricePointCtrl")
                With MyCommand
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = hdfStationID.Value.ToString
                    .Parameters.Add("@QuoReceiptID", SqlDbType.VarChar, 20)
                    .Parameters("@QuoReceiptID").Value = ddlQuoRecptNo.SelectedValue.ToString
                    .Parameters.Add("@Category", SqlDbType.BigInt)
                    If ddlSubCategory.SelectedValue.ToString = "0" Then
                        .Parameters("@Category").Value = ddlCategory.SelectedValue.ToString
                    Else
                        .Parameters("@Category").Value = ddlSubCategory.SelectedValue.ToString
                    End If

                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)

                hdfQuotationPricePointCtrl.Value = ""
                hdfType.Value = ""
                hdfPricePointQuotation.Value = ""
                hdfStationPricePointeRequisitionLimit.Value = ""
                hdfStationPricePointeRequisition.Value = ""
                hdfCategoryPricePointeRequisitionLimit.Value = ""
                hdfCategoryPricePointeRequisition.Value = ""
                hdfSend_eReq.Value = ""
                hdfSend_eReq_PerUnit.Value = ""
                hdfIs_Budget_Item.Value = ""
                hdfGroupItemType.Value = ""

                If MyData.Tables.Count > 0 Then
                    If MyData.Tables(0).Rows.Count > 0 Then
                        hdfQuotationPricePointCtrl.Value = MyData.Tables(0).Rows(0)("PricePointQuotationLimit").ToString
                        hdfType.Value = MyData.Tables(0).Rows(0)("Type").ToString
                        hdfPricePointQuotation.Value = MyData.Tables(0).Rows(0)("PricePointQuotation").ToString

                        hdfStationPricePointeRequisitionLimit.Value = MyData.Tables(0).Rows(0)("StationPricePointeRequisitionLimit").ToString
                        hdfStationPricePointeRequisition.Value = MyData.Tables(0).Rows(0)("StationPricePointeRequisition").ToString
                    End If

                    If MyData.Tables(1).Rows.Count > 0 Then
                        hdfGroupItemType.Value = MyData.Tables(1).Rows(0)("ItemType").ToString
                        hdfSend_eReq.Value = MyData.Tables(1).Rows(0)("Send_eReq").ToString
                        hdfSend_eReq_PerUnit.Value = MyData.Tables(1).Rows(0)("Send_eReq_PerUnit").ToString
                        hdfIs_Budget_Item.Value = MyData.Tables(1).Rows(0)("Is_Budget_Item").ToString
                        hdfCategoryPricePointeRequisitionLimit.Value = MyData.Tables(1).Rows(0)("CategoryPricePointeRequisitionLimit").ToString
                        hdfCategoryPricePointeRequisition.Value = MyData.Tables(1).Rows(0)("CategoryPricePointeRequisition").ToString
                    End If
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByID")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.NVarChar, 500)
                    .Parameters("@ReferenceType").Value = "[SubCategory], [Group_Category], [CurrencyCode],[PaymentTerm],[ImageDocPath_Temp],[ImageDocPath_Temp_Open]"
                    .Parameters.Add("@ID", SqlDbType.BigInt)
                    .Parameters("@ID").Value = 0
                    '.Parameters.Add("@Year", SqlDbType.BigInt)
                    '.Parameters("@Year").Value = Year(hdfCurrentDateTime.Value)
                    '.Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    '.Parameters("@StationID").Value = hdfStationID.Value.ToString

                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)


                'ddlSubCategory.DataSource = MyData.Tables(0)
                'ddlSubCategory.DataValueField = MyData.Tables(0).Columns("CategoryID").ToString
                'ddlSubCategory.DataTextField = MyData.Tables(0).Columns("Category").ToString
                'ddlSubCategory.DataBind()


                ddlCategory.DataSource = MyData.Tables(1)
                ddlCategory.DataValueField = MyData.Tables(1).Columns("CategoryID").ToString
                ddlCategory.DataTextField = MyData.Tables(1).Columns("Category").ToString
                ddlCategory.DataBind()

                BindSubCategory()

                'Dim dsCurrency As New DataSet
                'dsCurrency = DIMERCO.SDK.Utilities.ReSM.GetCurrency()
                'Dim newRow As DataRow = dsCurrency.Tables(0).NewRow()
                'newRow("HQID") = "0"
                'newRow("CurrencyCode") = ""

                'dsCurrency.Tables(0).Rows.Add(newRow)

                'Dim dtView As DataView = dsCurrency.Tables(0).DefaultView
                'dtView.Sort = "CurrencyCode ASC"

                Dim dtView As DataView = clsCF.GetCurrency()
                ddlCurrency.DataSource = dtView
                ddlCurrency.DataValueField = dtView.Table.Columns("HQID").ToString
                ddlCurrency.DataTextField = dtView.Table.Columns("CurrencyCode").ToString
                ddlCurrency.DataBind()

                StationCurrency()
                'Dim strStationCurrency As String
                'strStationCurrency = DIMERCO.SDK.Utilities.ReSM.GetStationCurrencyByID(hdfStationID.Value.ToString).ToString()

                'If ddlCurrency.Items.Count > 0 Then
                '    ddlCurrency.SelectedValue = DIMERCO.SDK.Utilities.ReSM.GetCurrencyID(strStationCurrency)
                'End If


                ddlPaymentTerm.DataSource = MyData.Tables(3)
                ddlPaymentTerm.DataValueField = MyData.Tables(3).Columns("PaymentTermID").ToString
                ddlPaymentTerm.DataTextField = MyData.Tables(3).Columns("PaymentTerm").ToString
                ddlPaymentTerm.DataBind()


                hdfImageDocPath_Temp.Value = MyData.Tables(4).Rows(0)("ImageDocPath_Temp").ToString
                hdfImageDocPath_Temp_Open.Value = MyData.Tables(5).Rows(0)("ImageDocPath_Temp").ToString

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub StationCurrency()
        clsCF.StationCurrencyID(hdfStationID.Value.ToString, ddlCurrency)
    End Sub

    Private Sub BindContactPerson()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetVendorContactPerson")

                With MyCommand
                    .Parameters.Add("@VendorID", SqlDbType.BigInt)
                    .Parameters("@VendorID").Value = CInt(hdfPayee.Value.ToString)
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)

                ddlVendorContactPerson.DataSource = MyData.Tables(0)
                ddlVendorContactPerson.DataValueField = MyData.Tables(0).Columns("ID").ToString
                ddlVendorContactPerson.DataTextField = MyData.Tables(0).Columns("Name").ToString
                ddlVendorContactPerson.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "BindContactPerson: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        Dim ParentFormID As String = ""
        ParentFormID = Request.QueryString("ParentForm").ToString

        If gvQuoRecpt.Rows.Count > 0 Then
            If gvQuoRecptDetail.Rows.Count = 0 Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Quotation\Receipt Detail')</script>")
                Exit Sub
            End If

            If CDec(lblQuoCompare_TotalAmt.Text.ToString) <> CDec(lblTotalAmount.Text.ToString) Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Total Amount not match')</script>")
                Exit Sub
            End If
        End If

        Session("ApplyNewID") = hdfApplyNewID.Value.ToString
        Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener =self;window.close();</script>")
    End Sub

    Protected Sub btnVendor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVendor.Click
        Dim strScript As String

        Session("DA_PayeeID") = Nothing
        Session("DA_PayeeCode") = Nothing
        Session("DA_PayeeName") = Nothing

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmPayee.aspx?ParentForm=frmQuotationReceipt','Vendor_Payee','height=750, width=700,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub btnAttach_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAttach.Click
        Dim blnContains As Boolean = False

        If fuQuoRecpt.FileName <> "" Then
            If (Regex.IsMatch(fuQuoRecpt.FileName, strFileExtension, RegexOptions.IgnoreCase)) = False Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid file format')</script>")
                'lblMsg.Text = "Invalid file format."
                Exit Sub
            End If

            ' Get the size in bytes of the file to upload.
            Dim fileSize As Integer = fuQuoRecpt.PostedFile.ContentLength

            ' Allow only files less than 2,100,000 bytes (approximately 2 MB) to be uploaded.
            'If (fileSize < 2100000) Then
            If (fileSize < intFileSizeLimitKB) Then

                Dim strAlbumPath As String
                strAlbumPath = hdfImageDocPath_Temp.Value.ToString + "/" + Session("DA_UserID").ToString
                Dim strAlbumPath_Open As String
                strAlbumPath_Open = hdfImageDocPath_Temp_Open.Value.ToString + "/" + Session("DA_UserID").ToString

                hifSupDoc.Add(fuQuoRecpt)
                Dim tbQuoRecpt As DataTable = New DataTable("QuoRecpt")
                tbQuoRecpt.Columns.Add("Filename")
                tbQuoRecpt.Columns.Add("Path")
                tbQuoRecpt.Rows.Add(clsCF.RenameFileName(fuQuoRecpt.FileName.ToString), "<a href=""../frmOpenFile.aspx?FilePath=" & strAlbumPath_Open & "/" & clsCF.RenameFileName(fuQuoRecpt.FileName.ToString) & """ target=""_blank"">" & clsCF.RenameFileName(fuQuoRecpt.FileName.ToString) & "</a>")

                Dim dsQuoRecpt As DataSet = New DataSet("QuoRecpt")
                dsQuoRecpt.Tables.Add(tbQuoRecpt)

                gvFile.Visible = True
                gvFile.DataSource = dsQuoRecpt
                gvFile.DataMember = dsQuoRecpt.Tables(0).TableName
                gvFile.DataBind()

                If gvFile.Rows.Count > 0 Then
                    fuQuoRecpt.Enabled = False
                    btnAttach.Enabled = False
                Else
                    fuQuoRecpt.Enabled = True
                    btnAttach.Enabled = True
                End If


                If FileIO.FileSystem.DirectoryExists(strAlbumPath) = True Then
                    FileIO.FileSystem.DeleteDirectory(strAlbumPath, FileIO.DeleteDirectoryOption.DeleteAllContents)
                End If

                FileIO.FileSystem.CreateDirectory(strAlbumPath)

                If FileIO.FileSystem.FileExists(strAlbumPath + "/" + clsCF.RenameFileName(fuQuoRecpt.FileName.ToString)) = False Then
                    fuQuoRecpt.PostedFile.SaveAs(strAlbumPath + "/" + clsCF.RenameFileName(fuQuoRecpt.FileName.ToString))
                End If
            Else
                'lblMsg.Text = "Your file was not uploaded because it exceeds the " & intFileSizeLimit & " MB size limit."
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Your file was not uploaded because it exceeds the " & intFileSizeLimit & " MB size limit')</script>")
            End If
        End If
    End Sub

    Protected Sub gvFile_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvFile.RowDeleting
        fuQuoRecpt.Dispose()

        hifSupDoc.Clear()
        Dim tbQuoRecpt As DataTable = New DataTable("QuoRecpt")
        tbQuoRecpt.Columns.Add("Filename")
        tbQuoRecpt.Columns.Add("Path")
        'tbQuoRecpt.Rows.Add(fuQuoRecpt.FileName, fuQuoRecpt.PostedFile.FileName)

        Dim dsQuoRecpt As DataSet = New DataSet("QuoRecpt")
        dsQuoRecpt.Tables.Add(tbQuoRecpt)

        'gvFile.DeleteRow(0)
        gvFile.Visible = True
        gvFile.DataSource = dsQuoRecpt
        gvFile.DataMember = dsQuoRecpt.Tables(0).TableName
        gvFile.DataBind()

        fuQuoRecpt.Enabled = True
        btnAttach.Enabled = True

    End Sub

    'Protected Sub gvFile_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvFile.SelectedIndexChanged
    '    Dim strFileName As String
    '    strFileName = gvFile.SelectedValue.ToString

    '    Dim hdfPath As New HiddenField
    '    hdfPath = gvFile.SelectedRow.FindControl("hdfPath")

    '    Dim fu As System.Web.UI.WebControls.FileUpload
    '    For Each fu In hifSupDoc
    '        'string path = Path.GetDirectoryName(FileUpload1.PostedFile.FileName);

    '        Dim fn As String = System.IO.Path.GetTempPath(fu.TemplateSourceDirectory.ToString)
    '        'Dim fn As String = System.IO.Path.GetFileName(fu.PostedFile.FileName.Replace(" ", ""))

    '    Next

    '    hifSupDoc.Clear()

    '    'Dim strScript As String
    '    'strScript = "<script language=javascript>"
    '    'strScript += "window.open('../frmOpenFile.aspx?FilePath=" & hdfPath & ','Vendor_Payee','height=750, width=700,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
    '    'strScript += "</script>"
    '    'Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)

    '    'strTable += "<tr><td><a href=""../frmOpenFile.aspx?FilePath=" & strPath & "/" & strAlbum & "/" & strDocPic & """ target=""_blank"">" & strDocPic & "</a></td></tr>"
    'End Sub

    Protected Sub btnCancel_H_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel_H.Click
        Cancel_H("All")
    End Sub

    Private Sub Cancel_H(ByVal strData As String)
        lblQuotationReceiptID.Text = ""
        ddlQuoRecpt.SelectedValue = "Select One"
        ddlQuoRecpt.Enabled = True
        txtQuoRecptNo.Text = ""
        txtTotalAmount.Text = ""
        cldQuoRecptDate.Clear()
        ddlPaymentTerm.SelectedValue = "0"
        gvFile.Dispose()
        gvFile.Visible = False
        btnAttach.Enabled = True
        fuQuoRecpt.Enabled = True

        If strData = "All" Then
            txtPayee.Text = ""
            hdfPayee.Value = ""
            hdfPayeeType.Value = ""
            ddlCurrency.SelectedValue = "0"
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                'DELETE Quotation / Receipt DATA
                Dim strMsg As String

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteQuotationCompare", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@QuotationCompareID", SqlDbType.VarChar, 20)
                    .Parameters("@QuotationCompareID").Value = lblQuotationCompareID.Text.ToString
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    lblQuotationCompareID.Text = ""
                    btnClear_Click(sender, e)
                    btnCancel_H_Click(sender, e)
                    GetQuoReceiptCompareDetail(hdfApplyNewID.Value.ToString, lblQuotationCompareID.Text.ToString)
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnCancel_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Cancel_D()
    End Sub

    Private Sub Cancel_D()
        lblID.Text = ""
        ddlCategory.SelectedValue = "0"
        'BindSubCategory()

        chkSpecialApproval.Checked = False

        ddlSubCategory.Items.Clear()
        ddlSubCategory.Dispose()
        ddlSpecialReqCategory.Items.Clear()
        ddlSpecialReqCategory.Dispose()
        ddlBudgetAmount.Items.Clear()
        ddlBudgetAmount.Dispose()
        ddlSpecialRequest.Items.Clear()
        ddlSpecialRequest.Dispose()


        ddlSubCategory.SelectedValue = "0"
        ddlQuoRecptNo.SelectedValue = ""
        txtPricePerUnit.Text = ""
        txtQty.Text = ""
        rbtnCost.Checked = True
        rbtnOthCharges.Checked = False
        txtBrand.Text = ""
        txtModel.Text = ""
        txtSpecification.Text = ""
        txtRemarks.Text = ""
        lblSubCategoryBudgetStatus.Text = "Total Unit:  ; Approved:  ; Pending:  ; Balance: "
        lblBudgetStatus.Text = "Total Unit:  ; Approved:  ; Pending:  ; Balance: "
        lblCategoryBudgetStatus.Text = "Total Unit:  ; Approved:  ; Pending:  ; Balance: "

        hdfQuotationPricePointCtrl.Value = ""
        hdfType.Value = ""
        hdfPricePointQuotation.Value = ""
        hdfStationPricePointeRequisitionLimit.Value = ""
        hdfStationPricePointeRequisition.Value = ""
        hdfCategoryPricePointeRequisitionLimit.Value = ""
        hdfCategoryPricePointeRequisition.Value = ""
        hdfSend_eReq.Value = ""
        hdfSend_eReq_PerUnit.Value = ""
        hdfIs_Budget_Item.Value = ""
        hdfGroupItemType.Value = ""
        'CollapsablePanel1.Collapsed = True
        gvItem.Dispose()

        GetCategoryUser()

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If Validation_QuoRecpt() = True Then
            SaveQuoRecpt()
        End If


        'txtQuoRecptNo.Text = ""
        'ddlCurrency.SelectedValue = 0
        'txtTotalAmount.Text = ""
    End Sub

    Private Function Validation_QuoRecpt() As Boolean
        Dim blnValid As Boolean = True

        If ddlQuoRecpt.SelectedValue.ToString = "Select One" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Type')</script>")
            ddlQuoRecpt.Focus()
            Return False
            Exit Function
        End If

        If hdfPayee.Value = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Vendor/Payee')</script>")
            btnVendor.Focus()
            Return False
            Exit Function
        End If

        If txtQuoRecptNo.Text = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please keyin Quotation/Receipt No')</script>")
            txtQuoRecptNo.Focus()
            Return False
            Exit Function
        End If

        If ddlCurrency.SelectedValue.ToString = "0" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Total Amount Currency Code')</script>")
            ddlCurrency.Focus()
            Return False
            Exit Function
        End If

        If txtTotalAmount.Text.ToString = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please keyin Total Amount')</script>")
            txtTotalAmount.Focus()
            Return False
            Exit Function
        End If

        If CDec(txtTotalAmount.Text.ToString) <= 0 Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Total Amount')</script>")
            txtTotalAmount.Focus()
            Return False
            Exit Function
        End If

        Dim strQuoRecptDate As String = Format(cldQuoRecptDate.SelectedDate, "dd MMM yyyy").ToString

        If strQuoRecptDate.Contains("01 Jan 0001") = True Then
            strQuoRecptDate = ""
        End If

        If strQuoRecptDate = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Quotation\Receipt Date')</script>")
            cldQuoRecptDate.Focus()
            Return False
            Exit Function
        End If

        If ddlPaymentTerm.SelectedValue.ToString = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Payment Term')</script>")
            ddlPaymentTerm.Focus()
            Return False
            Exit Function
        End If

        If gvFile.Rows.Count = 0 Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please attach Quotation / Receipt file')</script>")
            fuQuoRecpt.Focus()
            Return False
            Exit Function
        End If

        Return blnValid
    End Function

    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        BindSubCategory()
        chkSpecialApproval.Checked = False
        chkSpecialApproval_CheckedChanged(sender, e)
    End Sub

    Private Sub BindSubCategory()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetSubCategoryByID_CategoryBudgetStatus")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[SubCategory],[CategoryBudgetStatus],[BrandCategory],[SubCategoryBudgetStatus]"
                    .Parameters.Add("@ID", SqlDbType.BigInt)
                    .Parameters("@ID").Value = CInt(ddlCategory.SelectedValue.ToString)
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = hdfStationID.Value.ToString
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString
                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = hdfApplyNewID.Value.ToString
                    .Parameters.Add("@QuotationReceiptID", SqlDbType.VarChar, 20)
                    .Parameters("@QuotationReceiptID").Value = ddlQuoRecptNo.SelectedValue.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)


                Session("QR_SubCategory") = MyData.Tables(0)

                ddlSubCategory.DataSource = MyData.Tables(0)
                ddlSubCategory.DataValueField = MyData.Tables(0).Columns("CategoryID").ToString
                ddlSubCategory.DataTextField = MyData.Tables(0).Columns("Category").ToString
                ddlSubCategory.DataBind()

                ddlSpecialRequest.Enabled = False
                ddlSpecialReqCategory.Dispose()
                ddlSpecialReqCategory.Items.Clear()
                ddlSpecialReqCategory.Enabled = False

               
                Dim dtCategoryBudgetStatus As DataTable

                If ddlSubCategory.Items.Count > 1 Then
                    chkSpecialApproval.Visible = True
                    ddlBudgetAmount.Items.Clear()
                    ddlBudgetAmount.Dispose()

                    dtCategoryBudgetStatus = MyData.Tables(3)

                Else
                    chkSpecialApproval.Visible = False
                    BindBudgetAmount(MyData.Tables(1), ddlCategory.SelectedValue.ToString)
                    dtCategoryBudgetStatus = MyData.Tables(1)
                End If

                Session("QR_SubCategoryBudgetStatus") = MyData.Tables(3)
                Session("QR_CategoryBudgetStatus") = MyData.Tables(1)

                lblCategoryBudgetStatus.Visible = False
                lblSubCategoryBudgetStatus.Visible = False
                lblBudgetCurrency.Text = ""
                lblCategoryBudgetStatus.Text = ""
                lblBudgetStatus.Text = ""
                lblBudgetStatus.Visible = False

                'If ddlBudgetAmount.SelectedValue.ToString <> "" Then
                '    ShowBudgetStatus(dtCategoryBudgetStatus, ddlBudgetAmount.SelectedValue.ToString)
                'End If

                ddlBrand.DataSource = MyData.Tables(2)
                ddlBrand.DataValueField = MyData.Tables(2).Columns("BrandID").ToString
                ddlBrand.DataTextField = MyData.Tables(2).Columns("BrandName").ToString
                ddlBrand.DataBind()

                If ddlBrand.Items.Count = 1 Then
                    txtBrand.Enabled = True
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "BindSubCategory: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub ddlSubCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubCategory.SelectedIndexChanged

        chkSpecialApproval.Checked = False
        chkSpecialApproval_CheckedChanged(sender, e)

        ShowSubCategoryStatus(ddlSubCategory)
        ddlSpecialReqCategory.Dispose()
        ddlSpecialReqCategory.Items.Clear()

        Dim dtSubCategory As DataTable
        dtSubCategory = Session("QR_SubCategory")

        Dim dv As DataView
        dv = New DataView(dtSubCategory)
        If ddlSubCategory.SelectedValue.ToString <> "0" Then
            dv.RowFilter = "CategoryID <> '" + ddlSubCategory.SelectedValue.ToString + "'"
        End If

        ddlSpecialReqCategory.DataSource = dv
        ddlSpecialReqCategory.DataValueField = dv.Table.Columns("CategoryID").ToString
        ddlSpecialReqCategory.DataTextField = dv.Table.Columns("Category").ToString
        ddlSpecialReqCategory.DataBind()

    End Sub

    Private Sub ShowSubCategoryStatus(ByVal ddlSubCat As DropDownList)
        lblBudgetCurrency.Text = ""
        'lblSubCategoryBudgetStatus.Text = ""
        lblBudgetStatus.Text = ""

        lblBudgetStatus.Visible = False

        'If ddlSubCategory.SelectedValue.ToString <> "0" Then
        Dim dtSubCategoryBudgetStatus As DataTable
        dtSubCategoryBudgetStatus = Session("QR_SubCategoryBudgetStatus")

        BindBudgetAmount(dtSubCategoryBudgetStatus, ddlSubCat.SelectedValue.ToString)

        'Dim dv As DataView
        'dv = New DataView(dtSubCategoryBudgetStatus)
        'dv.RowFilter = "CategoryID ='" + ddlSubCategory.SelectedValue.ToString + "'"


        'ddlBudgetAmount.DataSource = dv
        'ddlBudgetAmount.DataValueField = dv.Table.Columns("BudgetListItemID").ToString
        'ddlBudgetAmount.DataTextField = dv.Table.Columns("Amount").ToString
        'ddlBudgetAmount.DataBind()

        If ddlBudgetAmount.SelectedValue.ToString <> "" Then
            ShowBudgetStatus(dtSubCategoryBudgetStatus, ddlBudgetAmount.SelectedValue.ToString)


            'Dim dvCBS As DataView
            'dvCBS = New DataView(dtSubCategoryBudgetStatus)
            'dvCBS.RowFilter = "BudgetListItemID ='" + ddlBudgetAmount.SelectedValue.ToString + "'"

            ''Dim introw As Integer = dv.Count
            'Dim strIs_Budget_Item As String = dvCBS(0).Item("Is_Budget_Item").ToString

            'lblBudgetCurrency.Text = dvCBS(0).Item("CurrencyCode").ToString

            'If strIs_Budget_Item.ToString = "1" Then
            '    'lblSubCategoryBudgetStatus.Text = "Total Budgeted Unit: " + dvCBS(0).Item("Quantity").ToString + " ; Approved: " + dvCBS(0).Item("Approved").ToString + " ; Pending: " + dvCBS(0).Item("Pending").ToString

            '    lblBudgetStatus.Text = "Total Budgeted Unit: " + dvCBS(0).Item("Quantity").ToString + " ; Approved: " + dvCBS(0).Item("Approved").ToString + " ; Pending: " + dvCBS(0).Item("Pending").ToString

            '    If CInt(dvCBS(0).Item("Approved").ToString) + CInt(dvCBS(0).Item("Pending").ToString) >= CInt(dvCBS(0).Item("Quantity").ToString) Then
            '        lblBudgetStatus.Text = lblBudgetStatus.Text + " ; Balance: 0 "
            '    Else
            '        lblBudgetStatus.Text = lblBudgetStatus.Text + " ; Balance: " + (CInt(dvCBS(0).Item("Quantity").ToString) - CInt(dvCBS(0).Item("Approved").ToString) - CInt(dvCBS(0).Item("Pending").ToString)).ToString
            '    End If
            '    'lblBudgetCurrency.Text = dvCBS(0).Item("CurrencyCode").ToString '+ " " + dvCBS(0).Item("Amount").ToString
            'Else
            '    If CInt(dvCBS(0).Item("Quantity").ToString) > 0 Then
            '        lblBudgetStatus.Text = "Total Budgeted Unit: " + dvCBS(0).Item("Quantity").ToString + " ; Approved: " + dvCBS(0).Item("Approved").ToString + " ; Pending: " + dvCBS(0).Item("Pending").ToString

            '        If CInt(dvCBS(0).Item("Approved").ToString) + CInt(dvCBS(0).Item("Pending").ToString) >= CInt(dvCBS(0).Item("Quantity").ToString) Then
            '            lblBudgetStatus.Text = lblBudgetStatus.Text + " ; Balance: 0 "
            '        Else
            '            lblBudgetStatus.Text = lblBudgetStatus.Text + " ; Balance: " + (CInt(dvCBS(0).Item("Quantity").ToString) - CInt(dvCBS(0).Item("Approved").ToString) - CInt(dvCBS(0).Item("Pending").ToString)).ToString
            '        End If

            '        'lblBudgetCurrency.Text = dvCBS(0).Item("CurrencyCode").ToString '+ " " + dvCBS(0).Item("Amount").ToString
            '    Else
            '        lblBudgetStatus.Text = "Total Budgeted Amount: " + dvCBS(0).Item("Amount").ToString + " ; Approved: " + (CDec(dvCBS(0).Item("Amount").ToString) - dvCBS(0).Item("Amount_Balance").ToString).ToString + " ; Pending: " + dvCBS(0).Item("Amt_Pending").ToString
            '        If CDec(dvCBS(0).Item("Amt_Pending").ToString) >= CDec(dvCBS(0).Item("Amount_Balance").ToString) Then
            '            lblBudgetStatus.Text = lblBudgetStatus.Text + " ; Balance: 0 "
            '        Else
            '            lblBudgetStatus.Text = lblBudgetStatus.Text + " ; Balance: " + (CInt(dvCBS(0).Item("Amount_Balance").ToString) - CInt(dvCBS(0).Item("Amt_Pending").ToString)).ToString
            '        End If
            '    End If

            'End If
            'lblBudgetStatus.Visible = True
        End If
        'Else
        'lblBudgetStatus.Visible = False
        'End If



    End Sub

    Private Sub BindBudgetAmount(ByVal dt As DataTable, ByVal strCategoryID As String)
        Dim dv As DataView
        'Dim dtn As DataTable
        dv = New DataView(dt)

        'dtn = dt.Select("ISNULL(SpecialCatBudget, '') <> '' ").Clone()
        'dv = New DataView(dtn)

        If chkSpecialApproval.Checked = True Then
            If ddlSpecialRequest.SelectedValue.ToString = "" Then
                dv.RowFilter = "CategoryID ='" + strCategoryID + "' AND ISNULL(SpecialCatBudget, '') <> '' "
            ElseIf ddlSpecialRequest.SelectedValue.ToString = "2" Then
                dv.RowFilter = "CategoryID  <> '" + strCategoryID + "' AND ISNULL(SpecialCatBudget, '') <> '' "
            ElseIf ddlSpecialRequest.SelectedValue.ToString = "1" Then
                dv.RowFilter = "SpecialCatBudget <> '' "
            End If
        Else
            dv.RowFilter = "CategoryID ='" + strCategoryID + "' "
        End If
        
        'Dim introw = dv.Table.Rows.Count

        ddlBudgetAmount.Items.Clear()
        ddlBudgetAmount.Dispose()

        ddlBudgetAmount.DataSource = dv

        ddlBudgetAmount.DataValueField = dv.Table.Columns("BudgetListItemID").ToString
        If chkSpecialApproval.Checked = True Then
            If ddlSpecialRequest.SelectedValue.ToString = "" Then
                ddlBudgetAmount.DataTextField = dv.Table.Columns("Amount").ToString
            Else
                ddlBudgetAmount.DataTextField = dv.Table.Columns("SpecialCatBudget").ToString
            End If
        Else
            ddlBudgetAmount.DataTextField = dv.Table.Columns("Amount").ToString
        End If

        hdfGroupItemType.Value = dv.Table.Columns("ItemType").ToString

        'ddlBudgetAmount.ToolTip = dv.Table.Columns("Remark").ToString()

        ddlBudgetAmount.DataBind()
    End Sub

    Private Sub ShowBudgetStatus(ByVal dt As DataTable, ByVal strBudgetListItemID As String)
        Dim dv As DataView
        dv = New DataView(dt)
        If strBudgetListItemID <> "" Then
            dv.RowFilter = "BudgetListItemID ='" + strBudgetListItemID + "'"
        Else
            dv.RowFilter = "BudgetListItemID ='0'"
        End If


        lblBudgetCurrency.Text = dv(0).Item("CurrencyCode").ToString

        hdfPendingQty.Value = ""
        hdfPendingAmt.Value = ""
        hdfBalanceQty.Value = ""
        hdfBalanceAmt.Value = ""
        hdfBudgetQty.Value = ""


        If dv(0).Item("Is_Budget_Item").ToString = "1" Then
            lblBudgetStatus.Text = "Total Budgeted Unit: " + dv(0).Item("Quantity").ToString + " ; Approved: " + dv(0).Item("Approved").ToString + " ; Pending: " + dv(0).Item("Pending").ToString + " ; Balance: " + dv(0).Item("Qty_Balance").ToString + " "
            hdfPendingQty.Value = dv(0).Item("Pending").ToString()
            hdfBalanceQty.Value = dv(0).Item("Qty_Balance").ToString
            hdfBudgetQty.Value = dv(0).Item("Quantity").ToString
        Else
            If CInt(dv(0).Item("Quantity").ToString) > 0 Then
                lblBudgetStatus.Text = "Total Budgeted Unit: " + dv(0).Item("Quantity").ToString + " ; Approved: " + dv(0).Item("Approved").ToString + " ; Pending: " + dv(0).Item("Pending").ToString + " ; Balance: " + dv(0).Item("Qty_Balance").ToString + " "
                hdfPendingQty.Value = dv(0).Item("Pending").ToString
                hdfBalanceQty.Value = dv(0).Item("Qty_Balance").ToString
                hdfBudgetQty.Value = dv(0).Item("Quantity").ToString
            Else
                lblCategoryBudgetStatus.Text = "Total Budgeted Amount: " + dv(0).Item("Amount").ToString + " ; Approved: " + (CDec(dv(0).Item("Amount").ToString) - dv(0).Item("Amount_Balance").ToString).ToString + " ; Pending: " + dv(0).Item("Amt_Pending").ToString
                If CDec(dv(0).Item("Amt_Pending").ToString) >= CDec(dv(0).Item("Amount_Balance").ToString) Then
                    lblBudgetStatus.Text = lblCategoryBudgetStatus.Text + " ; Balance: 0 "
                    hdfBalanceAmt.Value = "0"
                Else
                    lblBudgetStatus.Text = lblCategoryBudgetStatus.Text + " ; Balance: " + (CDec(dv(0).Item("Amount_Balance").ToString) - CDec(dv(0).Item("Amt_Pending").ToString)).ToString
                    hdfBalanceAmt.Value = (CDec(dv(0).Item("Amount_Balance").ToString) - CDec(dv(0).Item("Amt_Pending").ToString)).ToString
                End If
                hdfPendingAmt.Value = dv(0).Item("Amt_Pending").ToString

            End If
        End If
        lblBudgetStatus.Visible = True
    End Sub

    Private Sub GetQuoReceiptCompareDetail(ByVal strApplyNewID As String, ByVal strQuotationCompareID As String)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetQuoReceiptCompare_Detail")

                With MyCommand
                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = strApplyNewID
                    .Parameters.Add("@QuotationCompareID", SqlDbType.VarChar, 20)
                    .Parameters("@QuotationCompareID").Value = strQuotationCompareID
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "QuoReceiptDetail"

                If MyData.Tables(0).Rows.Count > 0 Then
                    hdfQRC_AlbumName.Value = MyData.Tables(0).Rows(0)("QRC_AlbumName").ToString
                Else
                    hdfQRC_AlbumName.Value = ""
                End If

                gvQuoRecpt.DataSource = MyData
                gvQuoRecpt.DataMember = MyData.Tables(1).TableName
                gvQuoRecpt.DataBind()


                gvQuoRecptDetail.DataSource = MyData
                gvQuoRecptDetail.DataMember = MyData.Tables(2).TableName
                gvQuoRecptDetail.DataBind()

                'If MyData.Tables(3).Rows.Count > 0 Then
                '    hdfImageDocPath_Temp.Value = MyData.Tables(3).Rows(0)("FilePath").ToString
                'Else
                '    hdfImageDocPath_Temp.Value = ""
                'End If

                ddlQuoRecptNo.DataSource = MyData.Tables(4)
                ddlQuoRecptNo.DataValueField = MyData.Tables(4).Columns("QuotationReceiptID").ToString
                ddlQuoRecptNo.DataTextField = MyData.Tables(4).Columns("QuotationReceiptNo").ToString
                ddlQuoRecptNo.DataBind()


                lblQuoCompare_TotalAmt.Text = ""
                lblTotalAmount.Text = ""

                lblQuoCompare_TotalAmt.Text = MyData.Tables(5).Rows(0)("QuotationCompare_TotalAmount").ToString
                lblTotalAmount.Text = MyData.Tables(6).Rows(0)("QuotationReceipt_Detail_TotalAmount").ToString

                lblMsg.Text = ""


            Catch ex As Exception
                Me.lblMsg.Text = "GetQuoReceiptCompareDetail: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub SaveQuoRecpt()
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strApplyNewID As String = ""
                Dim strQuotationCompareID As String = ""
                Dim strQuotationReceiptID As String = ""
                Dim strFilePath As String = ""
                Dim strQuoRecptDate As String = ""
                Dim strMsg As String = ""

                'GET ATTACHED FILE NAME FROM GV
                Dim i As Integer
                Dim strFileName As String = ""
                For i = 0 To gvFile.Rows.Count - 1
                    Dim lbtnRemove As New LinkButton
                    lbtnRemove = gvFile.Rows(i).FindControl("lbtnRemove")
                    strFileName = lbtnRemove.CommandArgument.ToString
                Next

                strQuoRecptDate = Format(cldQuoRecptDate.SelectedDate, "dd MMM yyyy").ToString

                If strQuoRecptDate.Contains("01 Jan 0001") = True Then
                    strQuoRecptDate = ""
                End If

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveQuoRecpt", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    If hdfApplyNewID.Value.ToString = "" Then
                        .Parameters("@ApplyNewID").Value = ""
                    Else
                        .Parameters("@ApplyNewID").Value = hdfApplyNewID.Value.ToString
                    End If

                    .Parameters.Add("@To", SqlDbType.VarChar, 6)
                    If Session("AN_To") <> Nothing Then
                        .Parameters("@To").Value = Session("AN_To").ToString
                    Else
                        .Parameters("@To").Value = ""
                    End If

                    .Parameters.Add("@CC", SqlDbType.NVarChar)
                    If Session("AN_CC") <> Nothing Then
                        .Parameters("@CC").Value = Session("AN_CC").ToString
                    Else
                        .Parameters("@CC").Value = ""
                    End If

                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = hdfStationID.Value.ToString

                    .Parameters.Add("@Purchase_Description", SqlDbType.NVarChar, 300)
                    If Session("AN_PurchaseDes") <> Nothing Then
                        .Parameters("@Purchase_Description").Value = Session("AN_PurchaseDes").ToString
                    Else
                        .Parameters("@Purchase_Description").Value = ""
                    End If

                    .Parameters.Add("@Remarks", SqlDbType.NVarChar)
                    If Session("AN_Remarks") <> Nothing Then
                        .Parameters("@Remarks").Value = Session("AN_Remarks").ToString
                    Else
                        .Parameters("@Remarks").Value = ""
                    End If


                    .Parameters.Add("@Reason", SqlDbType.NVarChar)
                    If Session("AN_Reason") <> Nothing Then
                        .Parameters("@Reason").Value = Session("AN_Reason").ToString
                    Else
                        .Parameters("@Reason").Value = ""
                    End If

                    .Parameters.Add("@ApplyNewType", SqlDbType.VarChar, 50)
                    If Session("AN_ApplyNewType") <> Nothing Then
                        .Parameters("@ApplyNewType").Value = Session("AN_ApplyNewType").ToString
                    Else
                        .Parameters("@ApplyNewType").Value = ""
                    End If


                    .Parameters.Add("@QuotationCompareID", SqlDbType.VarChar, 20)
                    If lblQuotationCompareID.Text.ToString = "" Then
                        .Parameters("@QuotationCompareID").Value = ""
                    Else
                        .Parameters("@QuotationCompareID").Value = lblQuotationCompareID.Text.ToString
                    End If

                    .Parameters.Add("@QuotationReceiptID", SqlDbType.VarChar, 20)
                    If lblQuotationCompareID.Text.ToString = "" Then
                        .Parameters("@QuotationReceiptID").Value = ""
                    Else
                        .Parameters("@QuotationReceiptID").Value = lblQuotationReceiptID.Text.ToString
                    End If

                    .Parameters.Add("@QuotationReceiptNo", SqlDbType.NVarChar, 50)
                    .Parameters("@QuotationReceiptNo").Value = txtQuoRecptNo.Text.ToString

                    .Parameters.Add("@QuoRecpt_Type", SqlDbType.NVarChar, 50)
                    .Parameters("@QuoRecpt_Type").Value = ddlQuoRecpt.SelectedValue.ToString

                    .Parameters.Add("@VendorPayeeID", SqlDbType.NVarChar, 50)
                    .Parameters("@VendorPayeeID").Value = hdfPayee.Value.ToString
                    .Parameters.Add("@VendorPayeeType", SqlDbType.NVarChar, 50)
                    .Parameters("@VendorPayeeType").Value = hdfPayeeType.Value.ToString

                    .Parameters.Add("@VendorContactID", SqlDbType.BigInt)
                    If ddlVendorContactPerson.Items.Count > 0 Then
                        .Parameters("@VendorContactID").Value = CInt(ddlVendorContactPerson.SelectedValue.ToString)
                    Else
                        .Parameters("@VendorContactID").Value = DBNull.Value
                    End If


                    .Parameters.Add("@QuotationReceiptDate", SqlDbType.NVarChar, 20)
                    .Parameters("@QuotationReceiptDate").Value = strQuoRecptDate
                    .Parameters.Add("@CurrencyCodeID", SqlDbType.BigInt)
                    .Parameters("@CurrencyCodeID").Value = CInt(ddlCurrency.SelectedValue.ToString)
                    .Parameters.Add("@TotalAmount", SqlDbType.Decimal, 18, 2)
                    .Parameters("@TotalAmount").Value = CDec(txtTotalAmount.Text.ToString)
                    .Parameters.Add("@PaymentTermID", SqlDbType.Int)
                    .Parameters("@PaymentTermID").Value = CInt(ddlPaymentTerm.SelectedValue.ToString)

                    .Parameters.Add("@ShopName", SqlDbType.NVarChar, 500)
                    .Parameters("@ShopName").Value = txtShopName.Text.ToString

                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString

                    .Parameters.Add("@FileName", SqlDbType.NVarChar, 500)
                    .Parameters("@FileName").Value = strFileName
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = Session("AN_AssetID").ToString
                    .Parameters.Add("@MaintenanceID", SqlDbType.VarChar, 20)
                    .Parameters("@MaintenanceID").Value = Session("AN_MaintenanceID").ToString

                    .Parameters.Add("@ApplyNew_ID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNew_ID").Direction = ParameterDirection.Output
                    .Parameters.Add("@QuotationCompare_ID", SqlDbType.VarChar, 20)
                    .Parameters("@QuotationCompare_ID").Direction = ParameterDirection.Output
                    .Parameters.Add("@QuotationReceipt_ID", SqlDbType.VarChar, 20)
                    .Parameters("@QuotationReceipt_ID").Direction = ParameterDirection.Output
                    .Parameters.Add("@FilePath", SqlDbType.NVarChar, 500)
                    .Parameters("@FilePath").Direction = ParameterDirection.Output
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                    strApplyNewID = .Parameters("@ApplyNew_ID").Value.ToString()
                    strQuotationCompareID = .Parameters("@QuotationCompare_ID").Value.ToString()
                    strQuotationReceiptID = .Parameters("@QuotationReceipt_ID").Value.ToString()
                    strFilePath = .Parameters("@FilePath").Value.ToString()
                End With

                If strMsg <> "" Then
                    lblMsg.Text = strMsg
                Else
                    lblMsg.Text = ""
                    hdfApplyNewID.Value = strApplyNewID
                    lblQuotationCompareID.Text = strQuotationCompareID

                    If hdfApplyNewID.Value.ToString <> "" Then
                        Session("ApplyNewID") = hdfApplyNewID.Value.ToString
                    End If

                    If strFilePath <> "" Then

                        Dim strAlbumPath_Temp As String
                        strAlbumPath_Temp = hdfImageDocPath_Temp.Value.ToString + "/" + Session("DA_UserID").ToString

                        If FileIO.FileSystem.DirectoryExists(strFilePath) = False Then
                            FileIO.FileSystem.CreateDirectory(strFilePath)
                        End If

                        If FileIO.FileSystem.FileExists(strAlbumPath_Temp + "/" + strFileName) = True Then
                            If FileIO.FileSystem.FileExists(strFilePath + "/" + strFileName) = True Then
                                FileIO.FileSystem.DeleteFile(strFilePath + "/" + strFileName)
                            End If

                            FileIO.FileSystem.CopyFile(strAlbumPath_Temp + "/" + strFileName, strFilePath + "/" + strFileName, True)
                        End If
                    End If

                    GetQuoReceiptCompareDetail(hdfApplyNewID.Value.ToString, lblQuotationCompareID.Text.ToString)
                    Cancel_H("Partial")
                    lblMsg.Text = "Save Successful."
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "SaveQuoRecpt: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try

        End Using

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If Validation_QuoRecptDetail("SaveQuoReceiptDetail") = True Then
            SaveQuoRecptDetail(sender, e)
        End If
    End Sub

    Private Function Validation_QuoRecptDetail(ByVal strAction As String) As Boolean
        Dim blnValid As Boolean = True

        GetPricePointCtrl()

        lblMsg.Text = ""

        If ddlQuoRecptNo.SelectedValue.ToString = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Quotation / Receipt No')</script>")
            ddlQuoRecptNo.Focus()
            Return False
            Exit Function
        End If


        If ddlCategory.SelectedValue.ToString = "0" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Category')</script>")
            ddlCategory.Focus()
            Return False
            Exit Function
        End If

        If ddlSubCategory.Items.Count > 1 Then
            If ddlSubCategory.SelectedValue.ToString = "0" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Sub Category')</script>")
                ddlSubCategory.Focus()
                Return False
                Exit Function
            End If
        End If


        If ddlSpecialRequest.SelectedValue.ToString = "1" Then
            If ddlSpecialReqCategory.SelectedValue.ToString = "0" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Special Request Category')</script>")
                ddlSpecialReqCategory.Focus()
                Return False
                Exit Function
            End If
        End If


        Dim dec As Double

        If txtPricePerUnit.Text.ToString = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Price Per Unit')</script>")
            txtPricePerUnit.Focus()
            Return False
            Exit Function
        ElseIf CDec(txtPricePerUnit.Text.ToString) <= 0 Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Price Per Unit')</script>")
            txtPricePerUnit.Focus()
            Return False
            Exit Function
        ElseIf Not Double.TryParse(txtPricePerUnit.Text.ToString, Globalization.NumberStyles.Currency, Nothing, dec) Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Price Per Unit')</script>")
            txtPricePerUnit.Focus()
            Return False
            Exit Function
        End If


        Dim num As Integer

        If txtQty.Text.ToString = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Quantity')</script>")
            txtQty.Focus()
            Return False
            Exit Function
        ElseIf CDec(txtQty.Text.ToString) <= 0 Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Quantity')</script>")
            txtQty.Focus()
            Return False
            Exit Function
        ElseIf Not Integer.TryParse(txtQty.Text.ToString, num) Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Quantity')</script>")
            txtQty.Focus()
            Return False
            Exit Function
        End If

        If hdfPendingQty.Value.ToString <> "" Then
            If CInt(hdfPendingQty.Value.ToString) > 0 Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Previous purchase pending. Please complete before attempt new purchase.')</script>")
                Return False
                Exit Function
            End If

            If CInt(hdfBudgetQty.Value.ToString) > 0 Then
                If CInt(hdfBalanceQty.Value.ToString) > 0 Then
                    If CInt(txtQty.Text.ToString) > CInt(hdfBalanceQty.Value.ToString) Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" & CInt(txtQty.Text.ToString) - CInt(hdfBalanceQty.Value.ToString) & " unit(s) unbudgeted, please apply separately.')</script>")
                        Return False
                        Exit Function
                    End If
                End If
            End If
        End If



        'If hdfPendingQty.Value.ToString <> "" Then
        '    If CInt(hdfPendingQty.Value.ToString) > 0 Then
        '        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Previous purchase approval not complete. Please complete previous purchase before proceed next purchase application.')</script>")
        '        Return False
        '        Exit Function
        '    End If
        'End If

        If hdfQuotationPricePointCtrl.Value.ToString <> "" Then
            If CDec(txtPricePerUnit.Text.ToString) > CDec(hdfQuotationPricePointCtrl.Value.ToString) Then
                If hdfType.Value.ToString <> "Quotation" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Price Per Unit over " + hdfPricePointQuotation.Value.ToString + ". Please provide Quotation.')</script>")
                    Return False
                    Exit Function
                End If
            End If
        End If


        If rbtnCost.Checked = True Then
            If hdfApplyNewType.Value.ToString = "Purchase" Then
                If hdfGroupItemType.Value.ToString = "IT" Then
                    If hdfStationPricePointeRequisitionLimit.Value.ToString <> "" Then
                        If CDec(txtPricePerUnit.Text.ToString) > CDec(hdfStationPricePointeRequisitionLimit.Value.ToString) Then
                            hdfSend_eReq.Value = "1"
                        End If
                    End If

                    If hdfCategoryPricePointeRequisitionLimit.Value.ToString <> "" Then
                        If CDec(txtPricePerUnit.Text.ToString) > CDec(hdfCategoryPricePointeRequisitionLimit.Value.ToString) Then
                            hdfSend_eReq.Value = "1"
                        End If
                    End If
                End If

                If hdfSend_eReq.Value.ToString = "1" Then

                    If ddlBrand.Items.Count > 1 Then
                        If ddlBrand.SelectedValue.ToString = "0" Then
                            If txtBrand.Text.ToString = "" Then
                                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('This item need to send eRequisition. Please enter/select Brand')</script>")
                                txtBrand.Focus()
                                Return False
                                Exit Function
                            End If
                        End If
                    End If
                    If txtModel.Text.ToString = "" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('This item need to send eRequisition. Please enter Model.')</script>")
                        txtModel.Focus()
                        Return False
                        Exit Function
                    End If
                    If txtSpecification.Text.ToString = "" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('This item need to send eRequisition. Please enter Specification.')</script>")
                        'txtSpecification.Focus
                        Return False
                        Exit Function
                    End If

                    If strAction = "SaveQuoReceiptDetail" Then
                        If gvItem.Rows.Count = 0 Then
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('This item need to send eRequisition. Please enter asset User.')</script>")
                            Return False
                            Exit Function
                        End If

                        If hdfSend_eReq_PerUnit.Value.ToString = "1" Then
                            If gvItem.Rows.Count <> CInt(txtQty.Text.ToString) Then
                                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Quantity - Total User not match with Quantity.')</script>")
                                txtQty.Focus()
                                Return False
                                Exit Function
                            End If
                        End If
                    End If
                End If
            Else '--ApplyNewType = Maintenance
                If ddlBrand.Items.Count > 1 Then
                    If ddlBrand.SelectedValue.ToString = "0" Then
                        If txtBrand.Text.ToString = "" Then
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter/select Brand')</script>")
                            txtBrand.Focus()
                            Return False
                            Exit Function
                        End If
                        If txtModel.Text.ToString = "" Then
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Model.')</script>")
                            txtModel.Focus()
                            Return False
                            Exit Function
                        End If
                    End If
                End If
            End If

            'If gvItem.Rows.Count > CInt(txtQty.Text.ToString) Then
            '    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Quantity - Total User not match with Quantity.')</script>")
            '    txtQty.Focus()
            '    Return False
            '    Exit Function
            'End If
        End If

        'If Len(txtSpecification.Text.ToString) > 1000 Then
        '    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Specification over field size limit.')</script>")
        '    Return False
        '    Exit Function
        'End If

        Return blnValid
    End Function

    Private Sub SaveQuoRecptDetail(ByVal sender As Object, ByVal e As System.EventArgs)
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String = ""
                Dim strApplyNewID As String = ""
                Dim strQuotationCompareID As String = ""
                Dim strQuotationReceiptID As String = ""
                Dim strBudgetStatus As String = ""
                Dim strSpecification As String = ""

                strSpecification = txtSpecification.Text.Replace("<p>", "")
                strSpecification = strSpecification.Replace("</p>", "</br>")

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveQuoRecptDetail", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@QuotationReceiptID", SqlDbType.VarChar, 20)
                    .Parameters("@QuotationReceiptID").Value = ddlQuoRecptNo.SelectedValue.ToString
                    .Parameters.Add("@ID", SqlDbType.Int)
                    If lblID.Text.ToString = "" Then
                        .Parameters("@ID").Value = 0
                    Else
                        .Parameters("@ID").Value = CInt(lblID.Text.ToString)
                    End If

                    .Parameters.Add("@CategoryID", SqlDbType.BigInt)
                    If ddlSubCategory.SelectedValue.ToString = "0" Then
                        .Parameters("@CategoryID").Value = ddlCategory.SelectedValue.ToString
                    Else
                        .Parameters("@CategoryID").Value = ddlSubCategory.SelectedValue.ToString
                    End If

                    .Parameters.Add("@Brand", SqlDbType.NVarChar, 50)
                    If ddlBrand.SelectedValue.ToString <> "0" Then
                        .Parameters("@Brand").Value = ddlBrand.SelectedValue.ToString
                    Else
                        .Parameters("@Brand").Value = txtBrand.Text.ToString
                    End If


                    .Parameters.Add("@Model", SqlDbType.NVarChar, 50)
                    .Parameters("@Model").Value = txtModel.Text.ToString

                    .Parameters.Add("@Specification", SqlDbType.NVarChar)
                    .Parameters("@Specification").Value = strSpecification

                    .Parameters.Add("@PricePerUnit", SqlDbType.Decimal, 18, 2)
                    .Parameters("@PricePerUnit").Value = CDec(txtPricePerUnit.Text.ToString)


                    .Parameters.Add("@Quantity", SqlDbType.BigInt)
                    .Parameters("@Quantity").Value = CInt(txtQty.Text.ToString)

                    .Parameters.Add("@Remarks", SqlDbType.NVarChar)
                    .Parameters("@Remarks").Value = txtRemarks.Text.ToString

                    .Parameters.Add("@Type", SqlDbType.NVarChar, 50)
                    If rbtnCost.Checked = True Then
                        .Parameters("@Type").Value = "Cost"
                    ElseIf rbtnOthCharges.Checked = True Then
                        .Parameters("@Type").Value = "Others Charges"
                    Else
                        .Parameters("@Type").Value = ""
                    End If


                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString

                    .Parameters.Add("@BudgetListItemID", SqlDbType.BigInt)
                    If ddlBudgetAmount.SelectedValue.ToString <> "" Then
                        .Parameters("@BudgetListItemID").Value = CInt(ddlBudgetAmount.SelectedValue.ToString)
                    Else
                        .Parameters("@BudgetListItemID").Value = 0
                    End If
                    
                    .Parameters.Add("@SpecialRequest", SqlDbType.TinyInt)

                    If chkSpecialApproval.Checked = True Then
                        If ddlSpecialRequest.SelectedValue.ToString <> "" Then
                            .Parameters("@SpecialRequest").Value = CInt(ddlSpecialRequest.SelectedValue.ToString)
                        Else
                            .Parameters("@SpecialRequest").Value = DBNull.Value
                        End If
                    Else
                        .Parameters("@SpecialRequest").Value = DBNull.Value
                    End If


                    .Parameters.Add("@ActualCategoryID", SqlDbType.BigInt)
                    If chkSpecialApproval.Checked = True Then
                        If ddlSpecialReqCategory.SelectedValue.ToString = "0" Or ddlSpecialReqCategory.SelectedValue.ToString = "" Then
                            .Parameters("@ActualCategoryID").Value = DBNull.Value
                        Else
                            .Parameters("@ActualCategoryID").Value = ddlSpecialReqCategory.SelectedValue.ToString
                        End If
                    Else
                        .Parameters("@ActualCategoryID").Value = DBNull.Value
                    End If


                    .Parameters.Add("@BudgetStatus", SqlDbType.NVarChar, 500)
                    .Parameters("@BudgetStatus").Direction = ParameterDirection.Output
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                    strBudgetStatus = .Parameters("@BudgetStatus").Value.ToString()
                End With

                If strMsg <> "" Then
                    lblMsg.Text = strMsg
                Else
                    lblMsg.Text = ""
                    GetQuoReceiptCompareDetail(hdfApplyNewID.Value.ToString, lblQuotationCompareID.Text.ToString)
                    btnClear_Click(sender, e)
                    ddlCategory.Enabled = True
                    ddlSubCategory.Enabled = True
                    ddlQuoRecptNo.Enabled = True

                    If strBudgetStatus <> "" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strBudgetStatus + ". Save Successful')</script>")
                    Else
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                    End If
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "SaveQuoRecptDetail: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub gvQuoRecptDetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvQuoRecptDetail.SelectedIndexChanged

        Cancel_D()

        Dim lbtnRemove As New LinkButton
        lbtnRemove = gvQuoRecptDetail.SelectedRow.FindControl("lbtnRemove")
        lblID.Text = lbtnRemove.CommandArgument.ToString

        Dim hdfParentCategoryID_GV As New HiddenField
        hdfParentCategoryID_GV = gvQuoRecptDetail.SelectedRow.FindControl("hdfParentCategoryID")
        ddlCategory.SelectedValue = CInt(hdfParentCategoryID_GV.Value.ToString)

        BindSubCategory()

        Dim hdSubCategoryID_GV As New HiddenField
        hdSubCategoryID_GV = gvQuoRecptDetail.SelectedRow.FindControl("hdSubCategoryID")
        ddlSubCategory.SelectedValue = CInt(hdSubCategoryID_GV.Value.ToString)

        ddlSubCategory_SelectedIndexChanged(sender, e)

        Dim hdfSpecialRequest_GV As New HiddenField
        hdfSpecialRequest_GV = gvQuoRecptDetail.SelectedRow.FindControl("hdfSpecialRequest")


        If hdfSpecialRequest_GV.Value.ToString <> "0" Then
            chkSpecialApproval.Checked = True
        End If
        chkSpecialApproval_CheckedChanged(sender, e)

        If ddlSpecialRequest.Items.Count > 0 Then
            ddlSpecialRequest.SelectedValue = hdfSpecialRequest_GV.Value.ToString
        End If

        'ddlSpecialRequest_SelectedIndexChanged(sender, e)


       
        If ddlSubCategory.Items.Count > 1 Then
            'ShowSubCategoryStatus(ddlSubCategory)
            'ddlSubCategory_SelectedIndexChanged(sender, e)

            Dim hdfActualCategory_GV As New HiddenField
            hdfActualCategory_GV = gvQuoRecptDetail.SelectedRow.FindControl("hdfActualCategory")
            ddlSpecialReqCategory.SelectedValue = hdfActualCategory_GV.Value.ToString
        End If

        

        Dim hdfBudgetListItemID_GV As New HiddenField
        hdfBudgetListItemID_GV = gvQuoRecptDetail.SelectedRow.FindControl("hdfBudgetListItemID")

        Dim item As ListItem = ddlBudgetAmount.Items.FindByValue(hdfBudgetListItemID_GV.Value)
        If Not item Is Nothing Then
            ddlBudgetAmount.SelectedValue = CInt(hdfBudgetListItemID_GV.Value.ToString)
        End If

        ddlBudgetAmount_SelectedIndexChanged(sender, e)

        Dim hdQuotationReceiptID_GV As New HiddenField
        hdQuotationReceiptID_GV = gvQuoRecptDetail.SelectedRow.FindControl("hdQuotationReceiptID")
        ddlQuoRecptNo.SelectedValue = hdQuotationReceiptID_GV.Value.ToString

        GetCategoryUser()


        Dim lblPricePerUnit_GV As New Label
        lblPricePerUnit_GV = gvQuoRecptDetail.SelectedRow.FindControl("lblPricePerUnit")
        txtPricePerUnit.Text = lblPricePerUnit_GV.Text.ToString

        Dim lblQuantity_GV As New Label
        lblQuantity_GV = gvQuoRecptDetail.SelectedRow.FindControl("lblQuantity")
        txtQty.Text = lblQuantity_GV.Text.ToString

        Dim lblType_GV As New Label
        lblType_GV = gvQuoRecptDetail.SelectedRow.FindControl("lblType")
        If lblType_GV.Text.ToString = "Cost" Then
            rbtnCost.Checked = True
        Else
            rbtnOthCharges.Checked = True
        End If

        Dim hdfBrand As New HiddenField
        hdfBrand = gvQuoRecptDetail.SelectedRow.FindControl("hdfBrand")
        If IsNumeric(hdfBrand.Value.ToString) = True Then
            ddlBrand.SelectedValue = hdfBrand.Value.ToString
        Else
            ddlBrand.SelectedValue = "0"
            txtBrand.Text = hdfBrand.Value.ToString
        End If


        Dim lblModel_gv As New Label
        lblModel_gv = gvQuoRecptDetail.SelectedRow.FindControl("lblModel")
        txtModel.Text = lblModel_gv.Text.ToString

        Dim lblSpecification_gv As New Label
        lblSpecification_gv = gvQuoRecptDetail.SelectedRow.FindControl("lblSpecification")
        txtSpecification.Text = lblSpecification_gv.Text.ToString

        Dim lblRemarks_gv As New Label
        lblRemarks_gv = gvQuoRecptDetail.SelectedRow.FindControl("lblRemarks")
        txtRemarks.Text = lblRemarks_gv.Text.ToString

    End Sub

    Protected Sub gvQuoRecptDetail_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvQuoRecptDetail.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim intID As Integer
                Dim lbtnRemove As New LinkButton
                Dim strQuotationReceiptID As String
                Dim hdfQuotationReceiptID As New HiddenField

                lblMsg.Text = ""

                lbtnRemove = gvQuoRecptDetail.Rows(e.RowIndex).FindControl("lbtnRemove")
                intID = CInt(lbtnRemove.CommandArgument.ToString)

                hdfQuotationReceiptID = gvQuoRecptDetail.Rows(e.RowIndex).FindControl("hdQuotationReceiptID")
                strQuotationReceiptID = hdfQuotationReceiptID.Value.ToString

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteQuotationReceipt_Detail", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@QuotationReceiptID", SqlDbType.VarChar, 20)
                    .Parameters("@QuotationReceiptID").Value = strQuotationReceiptID
                    .Parameters.Add("@ID", SqlDbType.Int)
                    .Parameters("@ID").Value = intID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    btnClear_Click(sender, e)
                    GetQuoReceiptCompareDetail(hdfApplyNewID.Value.ToString, lblQuotationCompareID.Text.ToString)
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvQuoRecptDetail_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub gvQuoRecpt_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvQuoRecpt.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim lbtnRemove As New LinkButton
                Dim strQuotationReceiptID As String

                lblMsg.Text = ""

                lbtnRemove = gvQuoRecpt.Rows(e.RowIndex).FindControl("lbtnRemove")
                strQuotationReceiptID = lbtnRemove.CommandArgument.ToString

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteQuotationReceipt", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@QuotationCompareID", SqlDbType.VarChar, 20)
                    .Parameters("@QuotationCompareID").Value = lblQuotationCompareID.Text.ToString
                    .Parameters.Add("@QuotationReceiptID", SqlDbType.VarChar, 20)
                    .Parameters("@QuotationReceiptID").Value = strQuotationReceiptID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    Cancel_H("All")
                    'Cancel_D()
                    btnClear_Click(sender, e)
                    Cancel_AddUser()
                    GetQuoReceiptCompareDetail(hdfApplyNewID.Value.ToString, lblQuotationCompareID.Text.ToString)

                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvQuoRecpt_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub gvQuoRecpt_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvQuoRecpt.SelectedIndexChanged
        Dim lbtnRemove As New LinkButton
        lbtnRemove = gvQuoRecpt.SelectedRow.FindControl("lbtnRemove")
        lblQuotationReceiptID.Text = lbtnRemove.CommandArgument.ToString

        Dim lblType_GV As New Label
        lblType_GV = gvQuoRecpt.SelectedRow.FindControl("lblType")
        ddlQuoRecpt.SelectedValue = lblType_GV.Text.ToString
        ddlQuoRecpt.Enabled = False

        Dim lblVendorPayeeName_GV As New Label
        lblVendorPayeeName_GV = gvQuoRecpt.SelectedRow.FindControl("lblVendorPayeeName")
        txtPayee.Text = lblVendorPayeeName_GV.Text.ToString

        Dim hdVendorPayeeID_GV As New HiddenField
        hdVendorPayeeID_GV = gvQuoRecpt.SelectedRow.FindControl("hdVendorPayeeID")
        hdfPayee.Value = hdVendorPayeeID_GV.Value.ToString



        Dim hdfVendorPayeeType_GV As New HiddenField
        hdfVendorPayeeType_GV = gvQuoRecpt.SelectedRow.FindControl("hdfVendorPayeeType")
        hdfPayeeType.Value = hdfVendorPayeeType_GV.Value.ToString

        If hdfPayeeType.Value.ToString = "Vendor" Then
            BindContactPerson()
            Dim hdfVendorContactID_GV As New HiddenField
            hdfVendorContactID_GV = gvQuoRecpt.SelectedRow.FindControl("hdfVendorContactID")
            ddlVendorContactPerson.SelectedValue = hdfVendorContactID_GV.Value.ToString
        End If

        Dim hdfShopName_GV As New HiddenField
        hdfShopName_GV = gvQuoRecpt.SelectedRow.FindControl("hdfShopName")
        txtShopName.Text = hdfShopName_GV.Value.ToString

        Dim lbtnQuotationReceiptID_GV As New LinkButton
        lbtnQuotationReceiptID_GV = gvQuoRecpt.SelectedRow.FindControl("lbtnQuotationReceiptID")
        lblQuotationReceiptID.Text = lbtnQuotationReceiptID_GV.Text.ToString

        Dim lblQuotationReceiptNo_GV As New Label
        lblQuotationReceiptNo_GV = gvQuoRecpt.SelectedRow.FindControl("lblQuotationReceiptNo")
        txtQuoRecptNo.Text = lblQuotationReceiptNo_GV.Text.ToString

        Dim hdfCurrencyCode_gv As New HiddenField
        hdfCurrencyCode_gv = gvQuoRecpt.SelectedRow.FindControl("hdfCurrencyCode")
        ddlCurrency.SelectedValue = CInt(hdfCurrencyCode_gv.Value.ToString)

        Dim lblTotalAmount_GV As New Label
        lblTotalAmount_GV = gvQuoRecpt.SelectedRow.FindControl("lblTotalAmount")
        txtTotalAmount.Text = lblTotalAmount_GV.Text.ToString

        Dim lblQuotationReceiptDate_GV As New Label
        lblQuotationReceiptDate_GV = gvQuoRecpt.SelectedRow.FindControl("lblQuotationReceiptDate")
        cldQuoRecptDate.SelectedDate = lblQuotationReceiptDate_GV.Text.ToString

        Dim hdPaymentTermID_GV As New HiddenField
        hdPaymentTermID_GV = gvQuoRecpt.SelectedRow.FindControl("hdPaymentTermID")
        ddlPaymentTerm.SelectedValue = CInt(hdPaymentTermID_GV.Value.ToString)

        Dim lblQuoRecpt_gv As New Label
        Dim strQuoRecpt As String
        Dim strFilePath As String
        lblQuoRecpt_gv = gvQuoRecpt.SelectedRow.FindControl("lblQuoRecpt")
        strQuoRecpt = lblQuoRecpt_gv.Text.ToString

        Dim hdfQR_AlbumName_gv As New HiddenField
        hdfQR_AlbumName_gv = gvQuoRecpt.SelectedRow.FindControl("hdfQR_AlbumName")
        strFilePath = hdfQR_AlbumName_gv.Value.ToString

        If strQuoRecpt.Replace("|", "").ToString <> "" Then
            Dim tbQuoRecpt As DataTable = New DataTable("QuoRecpt")
            tbQuoRecpt.Columns.Add("Filename")
            tbQuoRecpt.Columns.Add("Path")
            tbQuoRecpt.Rows.Add(strQuoRecpt.Replace("|", ""), "<a href=""../frmOpenFile.aspx?FilePath=" & strFilePath & "/" & strQuoRecpt.Replace("|", "") & """ target=""_blank"">" & strQuoRecpt.Replace("|", "") & "</a>")

            Dim dsQuoRecpt As DataSet = New DataSet("QuoRecpt")
            dsQuoRecpt.Tables.Add(tbQuoRecpt)

            gvFile.Visible = True
            gvFile.DataSource = dsQuoRecpt
            gvFile.DataMember = dsQuoRecpt.Tables(0).TableName
            gvFile.DataBind()
        End If

        If gvFile.Rows.Count > 0 Then
            fuQuoRecpt.Enabled = False
            btnAttach.Enabled = False
        Else
            fuQuoRecpt.Enabled = True
            btnAttach.Enabled = True
        End If

    End Sub

    Protected Sub ddlBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBrand.SelectedIndexChanged
        If ddlBrand.SelectedValue.ToString = "0" Then
            txtBrand.Enabled = True
            txtBrand.Focus()
        Else
            txtBrand.Enabled = False
        End If
    End Sub

    'Protected Sub btnCategoryUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCategoryUser.Click
    '    Dim strScript As String

    '    If ddlQuoRecptNo.SelectedValue.ToString = "" Then
    '        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Quotation\Receipt No')</script>")
    '        Exit Sub
    '    End If

    '    If ddlCategory.SelectedValue.ToString = "0" Then
    '        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Category')</script>")
    '        ddlCategory.Focus()
    '        Exit Sub
    '    End If

    '    If ddlSubCategory.Items.Count > 1 Then
    '        If ddlSubCategory.SelectedValue.ToString = "0" Then
    '            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Sub Category')</script>")
    '            ddlSubCategory.Focus()
    '            Exit Sub
    '        End If
    '    End If

    '    Session("AN_ApplyNewID") = ""
    '    Session("AN_ApplyNewID") = hdfApplyNewID.Value.ToString

    '    Session("AN_StationID") = ""
    '    Session("AN_StationID") = hdfStationID.Value.ToString

    '    Session("AN_QuotationReceiptID") = ""
    '    Session("AN_QuotationReceiptID") = ddlQuoRecptNo.SelectedValue.ToString

    '    Session("AN_CategoryID") = ""
    '    Session("AN_CategoryID") = ddlSubCategory.SelectedValue.ToString

    '    Session("AN_ParentCategoryID") = ""
    '    Session("AN_ParentCategoryID") = ddlCategory.SelectedValue.ToString

    '    Session("CurrentDatetime") = hdfCurrentDateTime.Value.ToString

    '    strScript = "<script language=javascript>"
    '    strScript += "theChild = window.open('frmCategoryUser.aspx?ParentForm=frmQuotationReceipt','AddCategoryUser','height=750, width=1000,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
    '    strScript += "</script>"
    '    Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    'End Sub

    Protected Sub gvItem_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvItem.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim strOwnerID As String = 0
                Dim lbtnDelete As New LinkButton

                lblMsg.Text = ""

                lbtnDelete = gvItem.Rows(e.RowIndex).FindControl("lbtnDelete")
                strOwnerID = lbtnDelete.CommandArgument.ToString

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteCategoryUser", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@QuotationReceiptID", SqlDbType.VarChar, 20)
                    .Parameters("@QuotationReceiptID").Value = ddlQuoRecptNo.SelectedValue.ToString
                    .Parameters.Add("@CategoryID", SqlDbType.BigInt)
                    If ddlSubCategory.SelectedValue.ToString = "0" Then
                        .Parameters("@CategoryID").Value = CInt(ddlCategory.SelectedValue.ToString)
                    Else
                        .Parameters("@CategoryID").Value = CInt(ddlSubCategory.SelectedValue.ToString)
                    End If
                    .Parameters.Add("@OwnerID", SqlDbType.NVarChar, 100)
                    .Parameters("@OwnerID").Value = strOwnerID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    GetCategoryUser()
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvItem_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub ddlUserType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUserType.SelectedIndexChanged
        If ddlUserType.SelectedValue = "User" Then
            btnOwner.Enabled = True
            txtOwner.Enabled = False
        ElseIf ddlUserType.SelectedValue = "Office" Then
            btnOwner.Enabled = False
            txtOwner.Enabled = True
        Else
            txtOwner.Enabled = False
            btnOwner.Enabled = False
        End If

        txtOwner.Text = ""
        hdfUserID.Value = ""
        txtJobGrade.Text = ""
        chkInternalAudit.Checked = False
        chkMIS_Seed_Member.Checked = False
        ddlJobFunction.SelectedValue = ""
    End Sub

    Protected Sub btnOwner_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOwner.Click
        Dim strScript As String

        If hdfUserID.Value.ToString = "" Then
            Session("DA_USERLIST") = ""
            Session("DA_New_USERLIST") = ""
            Session("DA_USERLIST_Name") = ""
        Else
            Session("DA_USERLIST") = hdfUserID.Value.ToString
            Session("DA_USERLIST_Name") = txtOwner.Text.ToString
        End If

        Session("DA_AddUserStationID") = ""
        If hdfStationID.Value.ToString <> "" Then
            Session("DA_AddUserStationID") = hdfStationID.Value.ToString
        Else
            Session("DA_AddUserStationID") = Session("DA_StationID").ToString
        End If


        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmAddUser2.aspx?ParentForm=frmQuotationReceipt','CategoryUser','height=550, width=550,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub btnSaveUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveUser.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim intItemNo As Integer = 0
                Dim strMsg As String = ""

                If Validation_QuoRecptDetail("SaveCategoryUser") = False Then
                    Exit Sub
                End If

                If ddlCondition.SelectedValue.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Condition')</script>")
                    Exit Sub
                End If

                If ddlLocation.SelectedValue.ToString = "0" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Location')</script>")
                    Exit Sub
                End If

                If txtOwner.Text.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please provide asset User')</script>")
                    Exit Sub
                End If

                If hdfSend_eReq.Value.ToString = "1" Then
                    If ddlUserType.SelectedValue.ToString = "Office" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('eRequisition Item must provide User Name.')</script>")
                        Exit Sub
                    End If
                End If

                If ddlUserType.SelectedValue.ToString = "User" Then
                    If ddlJobFunction.SelectedValue.ToString = "" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Job Function')</script>")
                        Exit Sub
                    End If
                End If

                Dim strEAssetPurchaseDate As String
                strEAssetPurchaseDate = Format(cldEAssetPurchaseDate.SelectedDate, "dd MMM yyyy").ToString

                If strEAssetPurchaseDate.Contains("01 Jan 0001") = True Then
                    strEAssetPurchaseDate = ""
                End If


                If ddlCondition.SelectedValue = "R" Then
                    If hdfGroupItemType.Value.ToString = "IT" Then
                        If txtExistingAsset.Text = "" Then
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Existing Asset ID.')</script>")
                            Exit Sub
                        End If

                        If strEAssetPurchaseDate = "" Then
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please provide existing Asset Purchase Date.')</script>")
                            Exit Sub
                        End If
                    End If
                End If

                Dim j As Integer = 0
                For j = 0 To gvItem.Rows.Count - 1
                    Dim lbtnDelete As LinkButton = gvItem.Rows(j).FindControl("lbtnDelete")
                    Dim strOwnerID As String = lbtnDelete.CommandArgument.ToString
                    If hdfUserID.Value.ToString <> "" Then
                        If hdfUserID.Value.ToString = strOwnerID Then
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Duplicate Asset User')</script>")
                            Exit Sub
                        End If
                    Else
                        If txtOwner.Text.ToString = strOwnerID Then
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Duplicate Asset User')</script>")
                            Exit Sub
                        End If
                    End If
                Next

                If ddlCategory.SelectedItem.Text.ToString = "Computer - Workstation" Then
                    If txtAppUsed.Text.ToString = "" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Application To Be Used')</script>")
                        Exit Sub
                    End If

                    Dim dsWorkStationType As DataSet = clsCF.GeteRequistionWorkStationType(hdfUserID.Value.ToString, ddlJobFunction.SelectedValue.ToString, chkInternalAudit.Checked, chkMIS_Seed_Member.Checked)
                    Dim dv As DataView
                    dv = New DataView(dsWorkStationType.Tables(0))
                    Dim i As Integer
                    For i = 0 To dv.Table.Rows.Count - 1
                        Dim strtype As String = dv.Table.Rows(i)("Type").ToString
                    Next
                    dv.RowFilter = "Type ='" + ddlSubCategory.SelectedItem.Text.ToString + "'"
                    Dim introw As Integer = dv.Count
                    If introw = 0 Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('SubCategory can not be found, it should be follow the IT Policy')</script>")
                        Exit Sub
                    End If
                End If


                Dim strSpecification As String = ""

                strSpecification = txtSpecification.Text.Replace("<p>", "")
                strSpecification = strSpecification.Replace("</p>", "</br>")

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveCategoryUser", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@QuotationReceiptID", SqlDbType.VarChar, 20)
                    .Parameters("@QuotationReceiptID").Value = ddlQuoRecptNo.SelectedValue.ToString
                    .Parameters.Add("@ID", SqlDbType.Int)
                    If lblID.Text.ToString = "" Then
                        .Parameters("@ID").Value = 0
                    Else
                        .Parameters("@ID").Value = CInt(lblID.Text.ToString)
                    End If

                    .Parameters.Add("@CategoryID", SqlDbType.BigInt)
                    If ddlSubCategory.SelectedValue.ToString = "0" Then
                        .Parameters("@CategoryID").Value = ddlCategory.SelectedValue.ToString
                    Else
                        .Parameters("@CategoryID").Value = ddlSubCategory.SelectedValue.ToString
                    End If

                    .Parameters.Add("@Condition", SqlDbType.VarChar, 5)
                    .Parameters("@Condition").Value = ddlCondition.SelectedValue.ToString
                    .Parameters.Add("@Location", SqlDbType.BigInt)
                    .Parameters("@Location").Value = CInt(ddlLocation.SelectedValue.ToString)
                    .Parameters.Add("@OwnerType", SqlDbType.VarChar, 50)
                    .Parameters("@OwnerType").Value = ddlUserType.SelectedValue.ToString

                    .Parameters.Add("@OwnerID", SqlDbType.NVarChar, 100)
                    If ddlUserType.SelectedValue.ToString = "User" Then
                        .Parameters("@OwnerID").Value = hdfUserID.Value.ToString
                    Else
                        .Parameters("@OwnerID").Value = txtOwner.Text.ToString
                    End If

                    .Parameters.Add("@Internal_Audit", SqlDbType.TinyInt)
                    If chkInternalAudit.Checked = True Then
                        .Parameters("@Internal_Audit").Value = 1
                    Else
                        .Parameters("@Internal_Audit").Value = 0
                    End If

                    .Parameters.Add("@MIS_Seed_Member", SqlDbType.TinyInt)
                    If chkMIS_Seed_Member.Checked = True Then
                        .Parameters("@MIS_Seed_Member").Value = 1
                    Else
                        .Parameters("@MIS_Seed_Member").Value = 0
                    End If
                    .Parameters.Add("@Job_Function", SqlDbType.NVarChar, 100)
                    .Parameters("@Job_Function").Value = ddlJobFunction.SelectedValue.ToString

                    .Parameters.Add("@JobGrade", SqlDbType.VarChar, 5)
                    .Parameters("@JobGrade").Value = txtJobGrade.Text.ToString
                    .Parameters.Add("@ItemNo", SqlDbType.Int)
                    .Parameters("@ItemNo").Direction = ParameterDirection.Output
                    .Parameters.Add("@AppUsed", SqlDbType.NVarChar)
                    .Parameters("@AppUsed").Value = txtAppUsed.Text.ToString
                    .Parameters.Add("@UserRemarks", SqlDbType.NVarChar)
                    .Parameters("@UserRemarks").Value = txtUserRemarks.Text.ToString



                    .Parameters.Add("@Brand", SqlDbType.NVarChar, 50)
                    If ddlBrand.SelectedValue.ToString <> "0" Then
                        .Parameters("@Brand").Value = ddlBrand.SelectedValue.ToString
                    Else
                        .Parameters("@Brand").Value = txtBrand.Text.ToString
                    End If


                    .Parameters.Add("@Model", SqlDbType.NVarChar, 50)
                    .Parameters("@Model").Value = txtModel.Text.ToString

                    .Parameters.Add("@Specification", SqlDbType.NVarChar)
                    .Parameters("@Specification").Value = strSpecification

                    .Parameters.Add("@PricePerUnit", SqlDbType.Decimal, 18, 2)
                    .Parameters("@PricePerUnit").Value = CDec(txtPricePerUnit.Text.ToString)


                    .Parameters.Add("@Quantity", SqlDbType.BigInt)
                    .Parameters("@Quantity").Value = CInt(txtQty.Text.ToString)

                    .Parameters.Add("@Remarks", SqlDbType.NVarChar)
                    .Parameters("@Remarks").Value = txtRemarks.Text.ToString

                    .Parameters.Add("@Type", SqlDbType.NVarChar, 50)
                    If rbtnCost.Checked = True Then
                        .Parameters("@Type").Value = "Cost"
                    ElseIf rbtnOthCharges.Checked = True Then
                        .Parameters("@Type").Value = "Others Charges"
                    Else
                        .Parameters("@Type").Value = ""
                    End If


                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString

                    .Parameters.Add("@BudgetListItemID", SqlDbType.BigInt)
                    If ddlBudgetAmount.SelectedValue.ToString() = "" Then
                        .Parameters("@BudgetListItemID").Value = 0
                    Else
                        .Parameters("@BudgetListItemID").Value = ddlBudgetAmount.SelectedValue.ToString
                    End If

                    .Parameters.Add("@ExistingAssetID", SqlDbType.VarChar, 20)
                    .Parameters("@ExistingAssetID").Value = txtExistingAsset.Text.ToString
                    .Parameters.Add("@ExistingAssetPurchaseDate", SqlDbType.VarChar, 20)
                    .Parameters("@ExistingAssetPurchaseDate").Value = strEAssetPurchaseDate

                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                    intItemNo = CInt(.Parameters("@ItemNo").Value.ToString())
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    ddlCategory.Enabled = False
                    ddlSubCategory.Enabled = False
                    ddlQuoRecptNo.Enabled = False

                    Cancel_AddUser()

                    lblID.Text = intItemNo.ToString
                    GetCategoryUser()
                    'If gvItem.Rows.Count = CInt(txtQty.Text.ToString) Then
                    '    CollapsablePanel1.Collapsed = True
                    'End If

                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Add User Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Private Sub Cancel_AddUser()
        ddlCondition.SelectedValue = ""
        ddlLocation.SelectedValue = "0"
        ddlUserType.SelectedValue = "Select One"
        txtOwner.Text = ""
        chkInternalAudit.Checked = False
        chkMIS_Seed_Member.Checked = False
        hdfUserID.Value = ""
        txtJobGrade.Text = ""
        ddlJobFunction.SelectedValue = ""

        txtAppUsed.Text = ""
        txtUserRemarks.Text = ""
    End Sub

    Protected Sub ddlBudgetAmount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBudgetAmount.SelectedIndexChanged

        hdfPendingQty.Value = ""
        hdfPendingAmt.Value = ""
        hdfBalanceQty.Value = ""
        hdfBalanceAmt.Value = ""
        hdfBudgetQty.Value = ""
        lblBudgetStatus.Text = ""
        lblBudgetCurrency.Text = ""


        If ddlSubCategory.Items.Count <= 1 Then
            Dim dtCategoryBudgetStatus As DataTable
            dtCategoryBudgetStatus = Session("QR_CategoryBudgetStatus")

            Dim dv As DataView
            dv = New DataView(dtCategoryBudgetStatus)
            dv.RowFilter = "BudgetListItemID ='" + ddlBudgetAmount.SelectedValue.ToString + "'"

            If dv(0).Item("Is_Budget_Item").ToString = "1" Then
                lblBudgetStatus.Text = "Total Budgeted Unit: " + dv(0).Item("Quantity").ToString + " ; Approved: " + dv(0).Item("Approved").ToString + " ; Pending: " + dv(0).Item("Pending").ToString + " ; Balance: " + dv(0).Item("Qty_Balance").ToString + " "
                lblBudgetCurrency.Text = dv(0).Item("CurrencyCode").ToString

                hdfPendingQty.Value = dv(0).Item("Pending").ToString
                hdfBalanceQty.Value = dv(0).Item("Qty_Balance").ToString
                hdfBudgetQty.Value = dv(0).Item("Quantity").ToString
            Else
                If CInt(dv(0).Item("Quantity").ToString) > 0 Then
                    lblBudgetStatus.Text = "Total Budgeted Unit: " + dv(0).Item("Quantity").ToString + " ; Approved: " + dv(0).Item("Approved").ToString + " ; Pending: " + dv(0).Item("Pending").ToString + " ; Balance: " + dv(0).Item("Qty_Balance").ToString + " "
                    lblBudgetCurrency.Text = dv(0).Item("CurrencyCode").ToString

                    hdfPendingQty.Value = dv(0).Item("Pending").ToString
                    hdfBalanceQty.Value = dv(0).Item("Qty_Balance").ToString
                    hdfBudgetQty.Value = dv(0).Item("Quantity").ToString
                Else
                    lblCategoryBudgetStatus.Text = "Total Budgeted Amount: " + dv(0).Item("Amount").ToString + " ; Approved: " + (CDec(dv(0).Item("Amount").ToString) - dv(0).Item("Amount_Balance").ToString).ToString + " ; Pending: " + dv(0).Item("Amt_Pending").ToString
                    If CDec(dv(0).Item("Amt_Pending").ToString) >= CDec(dv(0).Item("Amount_Balance").ToString) Then
                        lblBudgetStatus.Text = lblCategoryBudgetStatus.Text + " ; Balance: 0 "
                        hdfBalanceAmt.Value = "0"
                    Else
                        lblBudgetStatus.Text = lblCategoryBudgetStatus.Text + " ; Balance: " + (CInt(dv(0).Item("Amount_Balance").ToString) - CInt(dv(0).Item("Amt_Pending").ToString)).ToString
                        hdfBalanceAmt.Value = (CInt(dv(0).Item("Amount_Balance").ToString) - CInt(dv(0).Item("Amt_Pending").ToString)).ToString
                    End If
                    hdfPendingAmt.Value = dv(0).Item("Amt_Pending").ToString

                End If
            End If
            lblBudgetStatus.Visible = True
        Else
            Dim dtSubCategoryBudgetStatus As DataTable
            dtSubCategoryBudgetStatus = Session("QR_SubCategoryBudgetStatus")

            Dim dv As DataView
            dv = New DataView(dtSubCategoryBudgetStatus)

            If ddlBudgetAmount.SelectedValue.ToString = "" Then
                dv.RowFilter = "BudgetListItemID ='0'"
            Else
                dv.RowFilter = "BudgetListItemID ='" + ddlBudgetAmount.SelectedValue.ToString + "'"
            End If


            If dv(0).Item("Is_Budget_Item").ToString = "1" Then
                lblBudgetStatus.Text = "Total Budgeted Unit: " + dv(0).Item("Quantity").ToString + " ; Approved: " + dv(0).Item("Approved").ToString + " ; Pending: " + dv(0).Item("Pending").ToString + " ; Balance: " + dv(0).Item("Qty_Balance").ToString + " "
                lblBudgetCurrency.Text = dv(0).Item("CurrencyCode").ToString

                hdfPendingQty.Value = dv(0).Item("Pending").ToString
                hdfBalanceQty.Value = dv(0).Item("Qty_Balance").ToString
                hdfBudgetQty.Value = dv(0).Item("Quantity").ToString
            Else
                If CInt(dv(0).Item("Quantity").ToString) > 0 Then
                    lblBudgetStatus.Text = "Total Budgeted Unit: " + dv(0).Item("Quantity").ToString + " ; Approved: " + dv(0).Item("Approved").ToString + " ; Pending: " + dv(0).Item("Pending").ToString + " ; Balance: " + dv(0).Item("Qty_Balance").ToString + " "
                    lblBudgetCurrency.Text = dv(0).Item("CurrencyCode").ToString

                    hdfPendingQty.Value = dv(0).Item("Pending").ToString
                    hdfBalanceQty.Value = dv(0).Item("Qty_Balance").ToString
                    hdfBudgetQty.Value = dv(0).Item("Quantity").ToString
                Else
                    lblCategoryBudgetStatus.Text = "Total Budgeted Amount: " + dv(0).Item("Amount").ToString + " ; Approved: " + (CDec(dv(0).Item("Amount").ToString) - dv(0).Item("Amount_Balance").ToString).ToString + " ; Pending: " + dv(0).Item("Amt_Pending").ToString
                    If CDec(dv(0).Item("Amt_Pending").ToString) >= CDec(dv(0).Item("Amount_Balance").ToString) Then
                        lblBudgetStatus.Text = lblCategoryBudgetStatus.Text + " ; Balance: 0 "
                        hdfBalanceAmt.Value = "0"
                    Else
                        lblBudgetStatus.Text = lblCategoryBudgetStatus.Text + " ; Balance: " + (CInt(dv(0).Item("Amount_Balance").ToString) - CInt(dv(0).Item("Amt_Pending").ToString)).ToString
                        hdfBalanceAmt.Value = (CInt(dv(0).Item("Amount_Balance").ToString) - CInt(dv(0).Item("Amt_Pending").ToString)).ToString
                    End If
                    hdfPendingAmt.Value = dv(0).Item("Amt_Pending").ToString

                End If
            End If

            lblBudgetStatus.Visible = True
        End If
    End Sub

    Protected Sub ddlSpecialRequest_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSpecialRequest.SelectedIndexChanged
        If ddlSpecialRequest.SelectedValue.ToString = "1" Then
            ddlSpecialReqCategory.Enabled = True
        Else
            ddlSpecialReqCategory.Enabled = False
            ddlSpecialReqCategory.SelectedValue = "0"
        End If

        Dim dtSubCategoryBudgetStatus As DataTable
        dtSubCategoryBudgetStatus = Session("QR_SubCategoryBudgetStatus")

        BindBudgetAmount(dtSubCategoryBudgetStatus, ddlSubCategory.SelectedValue.ToString)
    End Sub

    Protected Sub chkSpecialApproval_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSpecialApproval.CheckedChanged

        bind_ddlSpecialRequest(ddlCategory.SelectedItem.Text.ToString)

        If chkSpecialApproval.Checked = True Then
            Label33.Visible = True
            Label31.Visible = True
            ddlSpecialRequest.Visible = True
            ddlSpecialRequest.Enabled = True

            If ddlCategory.SelectedItem.Text.ToString = "Computer - Workstation" Then
                ddlSpecialReqCategory.Visible = True
                ddlSpecialReqCategory.Enabled = True
                ddlSpecialReqCategory.SelectedValue = "0"
            Else
                ddlSpecialReqCategory.Visible = False
                ddlSpecialReqCategory.SelectedValue = "0"
            End If

            Dim dtSubCategoryBudgetStatus As DataTable
            dtSubCategoryBudgetStatus = Session("QR_SubCategoryBudgetStatus")

            BindBudgetAmount(dtSubCategoryBudgetStatus, ddlSubCategory.SelectedValue.ToString)

            ShowSubCategoryStatus(ddlSubCategory)

        Else
            Label33.Visible = False
            Label31.Visible = False
            ddlSpecialRequest.Visible = False
            ddlSpecialReqCategory.Visible = False

        End If

        If ddlSubCategory.Items.Count > 1 Then
            Dim dtSubCategoryBudgetStatus As DataTable
            dtSubCategoryBudgetStatus = Session("QR_SubCategoryBudgetStatus")
            BindBudgetAmount(dtSubCategoryBudgetStatus, ddlSubCategory.SelectedValue.ToString)
            ShowSubCategoryStatus(ddlSubCategory)
        Else
            Dim dtCategoryBudgetStatus As DataTable
            dtCategoryBudgetStatus = Session("QR_CategoryBudgetStatus")
            BindBudgetAmount(dtCategoryBudgetStatus, ddlCategory.SelectedValue.ToString)

            ShowBudgetStatus(dtCategoryBudgetStatus, ddlBudgetAmount.SelectedValue.ToString)
        End If

    End Sub

    Protected Sub bind_ddlSpecialRequest(ByVal Category As String)
        ddlSpecialRequest.Items.Clear()
        ddlSpecialRequest.Dispose()

        If chkSpecialApproval.Checked = True Then
            If Category = "Computer - Workstation" Then
                ddlSpecialRequest.Items.Add(New ListItem("Special Request Category", "1"))
            Else
                ddlSpecialRequest.Items.Add(New ListItem("Use Others Budget", "2"))
            End If
        End If
    End Sub

    Protected Sub ddlCondition_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCondition.SelectedIndexChanged
        pnlExistingAsset.Visible = False
        txtExistingAsset.Text = ""
        cldEAssetPurchaseDate.Clear()

        If ddlCondition.SelectedValue = "R" Then
            pnlExistingAsset.Visible = True
        Else
            txtExistingAsset.Text = ""
            cldEAssetPurchaseDate.Clear()
        End If
    End Sub

    Protected Sub btnSearchAsset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchAsset.Click
        Dim strScript As String

        'Session("AssetID") = ""
        'Session("AssetStationID") = ""
        'Session("AssetID") = txtAssetID.Text.ToString
        'Session("AssetStationID") = ddlStation.SelectedValue.ToString
        txtExistingAsset.Text = ""
        cldEAssetPurchaseDate.Clear()

        Session("ExistingAssetID") = ""
        Session("ExistingAssetPurchaseDate") = ""

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('frmExistingAssetList.aspx?ParentForm=frmQuotationReceipt','ExistingAsset','height=750, width=900,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub
End Class
