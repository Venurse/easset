Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Purchase_frmDeliveryOrder
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Public Shared hifSupDocDO As ArrayList = New ArrayList()
    Dim strFileExtension As String = ConfigurationSettings.AppSettings("DocExtension")
    Dim intFileSizeLimitKB As Integer = ConfigurationSettings.AppSettings("intFileSizeLimitKB")
    Dim intFileSizeLimit As Integer = ConfigurationSettings.AppSettings("intFileSizeLimit")

    Dim dsPO As New DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack() = False Then
            hdfApplyNewID.Value = ""

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            'gvFile.Columns(0).Visible = False
            btnSave.Enabled = False
            gvDO.Columns(0).Visible = False


            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "P0012")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Delivery Order Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnSave.Enabled = True
                        gvDO.Columns(0).Visible = True
                    Else
                        Session("blnWrite") = False
                        btnSave.Enabled = False
                        gvDO.Columns(0).Visible = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            If Session("ApplyNewID").ToString <> "" Then
                hdfApplyNewID.Value = Session("ApplyNewID").ToString
                txtPurchaseNo.Text = hdfApplyNewID.Value.ToString
                txtPurchaseNo.Enabled = False
            End If

            If Session("DOID").ToString <> "" Then
                hdfDOID.Value = Session("DOID").ToString
            Else
                hdfDOID.Value = ""
            End If

            BindDropDownList()
            If Session("ApplyNewStationID").ToString <> "" Then
                ddlStation.SelectedValue = Session("ApplyNewStationID").ToString
                ddlStation.Enabled = False
            Else
                ddlStation.Enabled = True
            End If

            Session("ApplyNewID") = ""
            Session("DOID") = ""
            Session("ApplyNewStationID") = ""

            GetDeliveryOrder(hdfApplyNewID.Value.ToString, hdfDOID.Value.ToString)

            Me.btnSave.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnSave, "", "btnClear", "btnClose", "", "", ""))
            Me.btnClear.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnClear, "", "btnSave", "btnClose", "", "", ""))

        Else

        End If
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.NVarChar, 500)
                    .Parameters("@ReferenceType").Value = "[AccessStation],[ImageDocPath_Temp],[ImageDocPath_Temp_Open]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"
                MyData.Tables(1).TableName = "ImageDocPath_Temp"

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)


                'If MyData.Tables(0).Rows(0)("AccessStation").ToString <> "" Then
                '    dsStation = DIMERCO.SDK.Utilities.LSDK.getStationDataByWithList(MyData.Tables(0).Rows(0)("AccessStation").ToString)
                'Else
                '    dsStation = DIMERCO.SDK.Utilities.ReSM.GetStationList()
                'End If
                'Dim dtView As DataView = dsStation.Tables(0).DefaultView
                'dtView.Sort = "StationCode ASC"

                'ddlStation.DataSource = dtView
                'ddlStation.DataValueField = dtView.Table.Columns("StationID").ToString
                'ddlStation.DataTextField = dtView.Table.Columns("StationCode").ToString
                'ddlStation.DataBind()

                If Session("DA_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("DA_StationID").ToString
                End If

                If MyData.Tables(1).Rows.Count > 0 Then
                    hdfImageDocPath_Temp.Value = MyData.Tables(1).Rows(0)("ImageDocPath_Temp").ToString
                Else
                    hdfImageDocPath_Temp.Value = ""
                End If

                If MyData.Tables(2).Rows.Count > 0 Then
                    hdfImageDocPath_Temp_Open.Value = MyData.Tables(2).Rows(0)("ImageDocPath_Temp").ToString
                Else
                    hdfImageDocPath_Temp_Open.Value = ""
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub GetDeliveryOrder(ByVal strApplyNewID As String, ByVal strDOID As String)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetDeliveryOrder")

                With MyCommand

                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = strApplyNewID
                    .Parameters.Add("@DOID", SqlDbType.VarChar, 20)
                    .Parameters("@DOID").Value = strDOID
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "Invoice"

                ddlVendorPayee.DataSource = MyData.Tables(2)
                ddlVendorPayee.DataValueField = MyData.Tables(2).Columns("VendorPayeeID").ToString
                ddlVendorPayee.DataTextField = MyData.Tables(2).Columns("VendorPayeeName").ToString
                ddlVendorPayee.DataBind()


                If strDOID = "" Then
                    gvDO.DataSource = MyData
                    gvDO.DataMember = MyData.Tables(0).TableName
                    gvDO.DataBind()

                    If ddlVendorPayee.Items.Count = 2 Then
                        ddlVendorPayee.SelectedIndex = 1
                    End If

                    fuDO.Enabled = True
                    btnAttach.Enabled = True
                Else
                    hdfDOID.Value = MyData.Tables(0).Rows(0)("DOID").ToString
                    txtDONo.Text = MyData.Tables(0).Rows(0)("DONo").ToString
                    cldDODate.SelectedDate = Format(Convert.ToDateTime(MyData.Tables(0).Rows(0)("DODate").ToString), "dd MMM yyyy")
                    ddlVendorPayee.SelectedValue = MyData.Tables(0).Rows(0)("VendorID").ToString

                    ddlStation.SelectedValue = MyData.Tables(0).Rows(0)("StationID").ToString
                    ddlStation.Enabled = False


                    If MyData.Tables(1).Rows(0)("FileName").ToString = "" Then
                        Dim tbDO As DataTable = New DataTable("DO")
                        tbDO.Columns.Add("Filename")
                        tbDO.Columns.Add("DocPickLink")

                        Dim dsInvoice As DataSet = New DataSet("DO")
                        dsInvoice.Tables.Add(tbDO)

                        gvFile.Visible = True
                        gvFile.DataSource = dsInvoice
                        gvFile.DataMember = dsInvoice.Tables(0).TableName
                        gvFile.DataBind()
                    Else
                        gvFile.Visible = True
                        gvFile.DataSource = MyData
                        gvFile.DataMember = MyData.Tables(1).TableName
                        gvFile.DataBind()
                    End If

                    If gvFile.Rows.Count > 0 Then
                        fuDO.Enabled = False
                        btnAttach.Enabled = False
                    Else
                        fuDO.Enabled = True
                        btnAttach.Enabled = True
                    End If
                End If


                dsPO.Tables.Add(MyData.Tables(3).Copy)
                dsPO.Tables(0).TableName = "PO"
                ViewState("dsPO") = dsPO

                BindgvPO(ddlVendorPayee.SelectedValue.ToString, hdfDOID.Value.ToString)



                'If MyData.Tables(3).Rows.Count = 1 Then
                '    txtVendorPayee.Visible = True
                '    btnVendor.Visible = True
                '    ddlVendorPayee.Visible = False
                'Else
                '    txtVendorPayee.Visible = False
                '    btnVendor.Visible = False
                '    ddlVendorPayee.Visible = True
                'End If

            Catch ex As Exception
                Me.lblMsg.Text = "GetDeliveryOrder: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub BindgvPO(ByVal strVendorPayee As String, ByVal strDOID As String)
        dsPO = ViewState("dsPO")

        Dim objDataSet2 As New DataSet
        objDataSet2.Merge(dsPO.Tables(0).Select("VendorPayeeID='" & strVendorPayee & "'"))

        If objDataSet2.Tables.Count = 0 Then
            objDataSet2 = dsPO.Clone()
        End If


        Dim objDataSet3 As New DataSet
        If strDOID <> "" Then
            objDataSet3.Merge(objDataSet2.Tables(0).Select("DOID='" & strDOID & "' OR DOID=''"))
            If objDataSet3.Tables.Count = 0 Then
                objDataSet3 = dsPO.Clone()
            End If
            gvPO.DataSource = objDataSet3
            gvPO.DataMember = objDataSet3.Tables(0).TableName
            gvPO.DataBind()
        Else
            objDataSet3.Merge(objDataSet2.Tables(0).Select("DOID='" & strDOID & "'"))
            If objDataSet3.Tables.Count = 0 Then
                objDataSet3 = dsPO.Clone()
            End If
            gvPO.DataSource = objDataSet3
            gvPO.DataMember = objDataSet3.Tables(0).TableName
            gvPO.DataBind()
        End If
    End Sub

    Protected Sub ddlVendorPayee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVendorPayee.SelectedIndexChanged
        BindgvPO(ddlVendorPayee.SelectedValue.ToString, hdfDOID.Value.ToString)
    End Sub

    Protected Sub gvDO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDO.SelectedIndexChanged
        Dim lbtnDONo As New LinkButton
        Dim strDOID As String = ""
        lbtnDONo = gvDO.SelectedRow.FindControl("lbtnDONo")
        strDOID = lbtnDONo.CommandArgument.ToString

        GetDeliveryOrder(txtPurchaseNo.Text.ToString, strDOID)
    End Sub

    Protected Sub gvFile_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvFile.RowDeleting
        fuDO.Dispose()

        hifSupDocDO.Clear()
        Dim tbDO As DataTable = New DataTable("DO")
        tbDO.Columns.Add("Filename")
        tbDO.Columns.Add("DocPickLink")

        Dim dsInvoice As DataSet = New DataSet("DO")
        dsInvoice.Tables.Add(tbDO)

        gvFile.Visible = True
        gvFile.DataSource = dsInvoice
        gvFile.DataMember = dsInvoice.Tables(0).TableName
        gvFile.DataBind()

        fuDO.Enabled = True
        btnAttach.Enabled = True
    End Sub

    Protected Sub btnAttach_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAttach.Click
        Dim blnContains As Boolean = False

        If fuDO.FileName <> "" Then
            If (Regex.IsMatch(fuDO.FileName, strFileExtension, RegexOptions.IgnoreCase)) = False Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid file format')</script>")
                'lblMsg.Text = "Invalid file format."
                Exit Sub
            End If

            ' Get the size in bytes of the file to upload.
            Dim fileSize As Integer = fuDO.PostedFile.ContentLength

            ' Allow only files less than 2,100,000 bytes (approximately 2 MB) to be uploaded.
            'If (fileSize < 2100000) Then
            If (fileSize < intFileSizeLimitKB) Then

                Dim strAlbumPath As String
                'If hdfPOID.Value.ToString = "" Then
                strAlbumPath = hdfImageDocPath_Temp.Value.ToString + "/" + Session("DA_UserID").ToString
                'Else
                '    strAlbumPath = hdfImageDocPath_Temp.Value.ToString + "/" + hdfPOID.Value.ToString
                'End If

                Dim strAlbumPath_Open As String
                strAlbumPath_Open = hdfImageDocPath_Temp_Open.Value.ToString + "/" + Session("DA_UserID").ToString

                hifSupDocDO.Add(fuDO)
                Dim tbDO As DataTable = New DataTable("DO")
                tbDO.Columns.Add("Filename")
                tbDO.Columns.Add("DocPicLink")
                tbDO.Rows.Add(clsCF.RenameFileName(fuDO.FileName.ToString), "<a href=""../frmOpenFile.aspx?FilePath=" & strAlbumPath_Open & "/" & clsCF.RenameFileName(fuDO.FileName.ToString) & """ target=""_blank"">" & clsCF.RenameFileName(fuDO.FileName.ToString) & "</a>")

                Dim dsDO As DataSet = New DataSet("DO")
                dsDO.Tables.Add(tbDO)

                gvFile.Visible = True
                gvFile.DataSource = dsDO
                gvFile.DataMember = dsDO.Tables(0).TableName
                gvFile.DataBind()

                If gvFile.Rows.Count > 0 Then
                    fuDO.Enabled = False
                    btnAttach.Enabled = False
                Else
                    fuDO.Enabled = True
                    btnAttach.Enabled = True
                End If


                If FileIO.FileSystem.DirectoryExists(strAlbumPath) = True Then
                    FileIO.FileSystem.DeleteDirectory(strAlbumPath, FileIO.DeleteDirectoryOption.DeleteAllContents)
                End If

                FileIO.FileSystem.CreateDirectory(strAlbumPath)

                If FileIO.FileSystem.FileExists(strAlbumPath + "/" + clsCF.RenameFileName(fuDO.FileName.ToString)) = False Then
                    fuDO.PostedFile.SaveAs(strAlbumPath + "/" + clsCF.RenameFileName(fuDO.FileName.ToString))
                End If
            Else
                'lblMsg.Text = "Your file was not uploaded because it exceeds the " & intFileSizeLimit & " MB size limit."
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Your file was not uploaded because it exceeds the " & intFileSizeLimit & " MB size limit')</script>")
                Exit Sub
            End If
        End If
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Dim ParentFormID As String = ""
        ParentFormID = Request.QueryString("ParentForm").ToString

        'Session("ApplyNewID") = ""
        Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener =self;window.close();</script>")
    End Sub

    Protected Sub gvDO_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDO.RowDeleting

        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim strDOID As String = 0
                Dim lbtnRemove As New LinkButton

                lblMsg.Text = ""

                lbtnRemove = gvDO.Rows(e.RowIndex).FindControl("lbtnRemove")
                strDOID = lbtnRemove.CommandArgument.ToString

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteDeliveryOrder", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@DOID", SqlDbType.VarChar, 20)
                    .Parameters("@DOID").Value = strDOID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else

                    btnClear_Click(sender, e)
                    GetDeliveryOrder(txtPurchaseNo.Text.ToString, "")
                    'lblMsg.Text = "Delete Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvDO_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        hdfDOID.Value = ""
        If Session("ApplyNewStationID").ToString <> "" Then
            ddlStation.SelectedValue = Session("ApplyNewStationID").ToString
        Else
            If Session("DA_StationID").ToString <> "" Then
                ddlStation.SelectedValue = Session("DA_StationID").ToString
            End If
        End If
        txtDONo.Text = ""
        cldDODate.Clear()
        gvFile.Dispose()
        gvFile.Visible = False
        fuDO.Dispose()
        fuDO.Enabled = True
        btnAttach.Enabled = True
        GetDeliveryOrder(txtPurchaseNo.Text.ToString, "")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strDODate As String = ""
                Dim strFilePath As String = ""
                Dim strFileName As String = ""
                Dim strPOID As String = ""
                Dim strMsg As String = ""

                lblMsg.Text = ""

                If txtPurchaseNo.Text.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Purchase No')</script>")
                    txtPurchaseNo.Focus()
                    Exit Sub
                End If
                If txtDONo.Text.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Delivery Order No')</script>")
                    txtDONo.Focus()
                    Exit Sub
                End If

                Dim i As Integer
                Dim blnPO As Boolean = False
                For i = 0 To gvPO.Rows.Count - 1
                    Dim chkSelect As New CheckBox
                    Dim hfPOID As New HiddenField
                    hfPOID = gvPO.Rows(i).FindControl("hfPOID")
                    chkSelect = gvPO.Rows(i).FindControl("chkSelect")
                    If chkSelect.Checked = True Then
                        blnPO = True
                        If strPOID <> "" Then
                            strPOID = strPOID + "<" + hfPOID.Value.ToString
                        Else
                            strPOID = hfPOID.Value.ToString
                        End If

                        'Exit For
                    End If
                Next

                If gvPO.Rows.Count > 0 Then
                    If blnPO = False Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Purchase Order')</script>")
                        'lblMsg.Text = "Please select Purchase Order."
                        Exit Sub
                    End If
                End If

                strDODate = Format(cldDODate.SelectedDate, "dd MMM yyyy").ToString

                If strDODate.Contains("01 Jan 0001") = True Then
                    strDODate = ""
                End If

                'GET ATTACHED FILE NAME FROM GV
                For i = 0 To gvFile.Rows.Count - 1
                    Dim lbtnRemove As New LinkButton
                    lbtnRemove = gvFile.Rows(i).FindControl("lbtnRemove")
                    strFileName = lbtnRemove.CommandArgument.ToString
                Next

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveDeliveryOrder", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ApplyNewID", SqlDbType.NVarChar, 50)
                    .Parameters("@ApplyNewID").Value = hdfApplyNewID.Value.ToString
                    .Parameters.Add("@DOID", SqlDbType.VarChar, 20)
                    If hdfDOID.Value.ToString = "" Then
                        .Parameters("@DOID").Value = ""
                    Else
                        .Parameters("@DOID").Value = hdfDOID.Value.ToString
                    End If

                    .Parameters.Add("@DONo", SqlDbType.NVarChar, 50)
                    .Parameters("@DONo").Value = txtDONo.Text.ToString
                    .Parameters.Add("@VendorID", SqlDbType.NVarChar, 50)
                    .Parameters("@VendorID").Value = ddlVendorPayee.SelectedValue.ToString
                    .Parameters.Add("@POID", SqlDbType.NVarChar)
                    .Parameters("@POID").Value = strPOID
                    .Parameters.Add("@DODate", SqlDbType.NVarChar, 20)
                    .Parameters("@DODate").Value = strDODate
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@FileName", SqlDbType.NVarChar, 500)
                    .Parameters("@FileName").Value = strFileName
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()


                    .Parameters.Add("@FilePath", SqlDbType.NVarChar, 500)
                    .Parameters("@FilePath").Direction = ParameterDirection.Output
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                    strFilePath = .Parameters("@FilePath").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else


                    If strFilePath <> "" Then

                        Dim strAlbumPath_Temp As String
                        strAlbumPath_Temp = hdfImageDocPath_Temp.Value.ToString + "/" + Session("DA_UserID").ToString

                        If FileIO.FileSystem.DirectoryExists(strFilePath) = False Then
                            FileIO.FileSystem.CreateDirectory(strFilePath)
                        End If

                        If FileIO.FileSystem.FileExists(strAlbumPath_Temp + "/" + strFileName) = True Then
                            If FileIO.FileSystem.FileExists(strFilePath + "/" + strFileName) = True Then
                                FileIO.FileSystem.DeleteFile(strFilePath + "/" + strFileName)
                            End If

                            FileIO.FileSystem.CopyFile(strAlbumPath_Temp + "/" + strFileName, strFilePath + "/" + strFileName, True)
                        End If
                    End If

                    btnClear_Click(sender, e)
                    GetDeliveryOrder(txtPurchaseNo.Text.ToString, "")
                    'lblMsg.Text = "Save Successful."
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub
End Class
