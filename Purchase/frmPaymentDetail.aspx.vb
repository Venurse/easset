Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Purchase_frmPaymentDetail
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack() = False Then

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            'gvFile.Columns(0).Visible = False
            btnVoid.Enabled = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "P0009")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Payment Detail Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnVoid.Enabled = True
                    Else
                        Session("blnWrite") = False
                        btnVoid.Enabled = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            If Session("PaymentID") <> Nothing Then
                lblPaymentID.Text = Session("PaymentID").ToString
                Session("PaymentID") = ""
            End If

            GetPaymentDetail()

            GetCheckIs_InChargeParty()
        End If
    End Sub

    Private Sub GetCheckIs_InChargeParty()
        Dim dsInChargeParty As New DataSet
        dsInChargeParty = clsCF.GetCheckIs_InChargeParty(Session("DA_UserID").ToString, hdfStationID.Value.ToString)

        If dsInChargeParty.Tables.Count > 0 Then
            If dsInChargeParty.Tables(0).Rows(0)("Issue_Payment").ToString <> 0 Then
                If lblStatus.Text.ToString <> "Void" Then
                    btnVoid.Enabled = True
                End If
            Else
                btnVoid.Enabled = False
            End If
        End If
    End Sub


    Private Sub GetPaymentDetail()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsInvoiceReceipt As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetPaymentDetail")

                With MyCommand
                    .Parameters.Add("@PaymentID", SqlDbType.VarChar, 20)
                    .Parameters("@PaymentID").Value = lblPaymentID.Text.ToString

                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "PaymentDetail"

                hdfStationID.Value = MyData.Tables(0).Rows(0)("StationID").ToString
                lblStation.Text = MyData.Tables(0).Rows(0)("StationCode").ToString
                lblVendorPayee.Text = MyData.Tables(0).Rows(0)("VendorPayeeName").ToString
                lblPaymentType.Text = MyData.Tables(0).Rows(0)("PaymentType").ToString
                lblPaymentDate.Text = MyData.Tables(0).Rows(0)("PaymentDate").ToString
                lblVoucherNo.Text = MyData.Tables(0).Rows(0)("PaymentVoucherNo").ToString
                lblAmount.Text = MyData.Tables(0).Rows(0)("Amount").ToString
                lblFile.Text = MyData.Tables(0).Rows(0)("DocPicLink").ToString
                lblCreatedBy.Text = MyData.Tables(0).Rows(0)("CreatedDetail").ToString

                If MyData.Tables(0).Rows(0)("Status").ToString = "Void" Then
                    btnVoid.Enabled = False
                    Label16.Visible = False
                    Label17.Visible = False
                    txtReason.Visible = False

                    lblReason.Visible = True
                    Label9.Visible = True
                    Label13.Visible = True
                    lblStatus.Visible = True
                    Label14.Visible = True
                    Label15.Visible = True
                    lblReason.Text = MyData.Tables(0).Rows(0)("Reason").ToString
                    lblStatus.Text = MyData.Tables(0).Rows(0)("Status").ToString
                Else
                    btnVoid.Enabled = True
                    Label16.Visible = True
                    Label17.Visible = True
                    txtReason.Visible = True

                    lblReason.Visible = False
                    Label9.Visible = False
                    Label13.Visible = False
                    lblStatus.Visible = False
                    Label14.Visible = False
                    Label15.Visible = False
                End If

                gvInvReceipt.DataSource = MyData.Tables(1)
                gvInvReceipt.DataMember = MyData.Tables(1).TableName
                gvInvReceipt.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "GetPaymentDetail: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnVoid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVoid.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strPaymentID As String = 0
                Dim lbtnRemove As New LinkButton

                lblMsg.Text = ""

                Dim strMsg As String

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_VoidPayment", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@PaymentID", SqlDbType.VarChar, 20)
                    .Parameters("@PaymentID").Value = lblPaymentID.Text.ToString
                    .Parameters.Add("@Reason", SqlDbType.NVarChar, 50)
                    .Parameters("@Reason").Value = txtReason.Text.ToString
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    GetPaymentDetail()
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Void Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvPayment_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Dim ParentFormID As String = ""
        ParentFormID = Request.QueryString("ParentForm").ToString
        Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener =self;window.close();</script>")
    End Sub
End Class
