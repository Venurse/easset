<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmPurchaseOrder.aspx.vb" Inherits="Purchase_frmPurchaseOrder" ValidateRequest="false"%>

<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
<META HTTP-EQUIV="EXPIRES" CONTENT="0"> 

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Purchase Order</title>
    <script type="text/javascript">
         function showDate() 
        {
            var month_names = new Array("Jan", "Feb", "Mar", 
            "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
            "Oct", "Nov", "Dec");

            var d = new Date();
            var curr_day = d.getDay();
            var curr_date = d.getDate();
            var hours = d.getHours();
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            
            var curr_hours;
            var curr_minutes;
            var curr_seconds;

            if(hours<10)
            {
                curr_hours = "0" + hours;
            }
            else
            {
                curr_hours = hours;
            }
            
            if(minutes < 10)
            {
                curr_minutes = "0" + minutes;
            }
             else
            {
                curr_minutes = minutes;
            }
            
           if(seconds < 10)
            {
                curr_seconds = "0" + seconds;
            }
             else
            {
                curr_seconds = seconds;
            }

            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
 
            document.getElementById("hdfCurrentDateTime").value = curr_date + " " +  month_names[curr_month] +  " " + curr_year + " " + curr_hours + ":" + curr_minutes + ":" + curr_seconds
         }
     </script>
</head>
<body onunload="window.opener.document.frmPurchaseDetail.submit();">
    <form id="frmPurchaseOrder" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Purchase Order"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <table>
                        <tr>
                            <td style="width: 100px">
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Station"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:DropDownList ID="ddlStation" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                    </asp:DropDownList>
                                <asp:HiddenField ID="hdfApplyNewID" runat="server" />
                                <asp:HiddenField ID="hdfPOID" runat="server" />
                                <asp:HiddenField ID="hdfCurrentDateTime" runat="server" />
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Purchase No"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <asp:TextBox ID="txtPurchaseNo" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="150px"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" Text="Search" /></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Vendor"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlVendorPayee" runat="server" AutoPostBack="True" Font-Names="Verdana"
                                    Font-Size="X-Small">
                                </asp:DropDownList><br />
                    <asp:TextBox ID="txtVendorPayee" runat="server" Enabled="False" Font-Names="Verdana"
                        Font-Size="X-Small" Width="200px" Visible="False"></asp:TextBox><asp:HiddenField ID="hdfVendorID" runat="server" /></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Quotation"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <asp:GridView ID="gvQuoReceipt" runat="server" AutoGenerateColumns="False" Font-Names="Verdana"
                        Font-Size="X-Small">
                        <Columns>
                            <asp:TemplateField HeaderText="Select">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" Checked='<%# Bind("Checked") %>' AutoPostBack="True" OnCheckedChanged="chkSelect_CheckedChanged" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="QuotationReceiptID" HeaderText="Quotation ID" />
                            <asp:TemplateField HeaderText="Quotation No">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("QuotationReceiptNo") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("QuotationReceiptNo") %>'></asp:Label>
                                    <asp:HiddenField ID="hfQuotationReceiptID" runat="server" Value='<%# Bind("QuotationReceiptID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total Amount">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("TotalAmount") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("TotalAmount") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="PaymentTerm" HeaderText="Payment Term" />
                        </Columns>
                    </asp:GridView>
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label6" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="PO No"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <asp:TextBox ID="txtPONo" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="150px"></asp:TextBox>
                                </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label7" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="PO Date"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <ew:calendarpopup id="cldPODate" runat="server" width="118px">
<SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></SelectedDateStyle>

<HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></HolidayStyle>

<OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Gray"></OffMonthStyle>

<MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></MonthHeaderStyle>

<WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></WeekdayStyle>

<GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></GoToTodayStyle>

<ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></ClearDateStyle>

<WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></WeekendStyle>

<DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></DayHeaderStyle>

<TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></TodayDayStyle>
</ew:calendarpopup>
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label8" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Amount"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <ew:numericbox id="txtAmount" runat="server" font-names="Verdana" font-size="X-Small"></ew:numericbox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label9" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Remarks"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:TextBox ID="txtRemarks" runat="server" Height="61px" TextMode="MultiLine" Width="450px"></asp:TextBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label10" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="PO"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <asp:FileUpload ID="fuPO" runat="server" Width="453px" />&nbsp;<asp:Button ID="btnAttach"
                        runat="server" Font-Names="Arial" Font-Size="Small" Height="21px" Text="Attach"
                        Width="65px" /></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                    <asp:GridView ID="gvFile" runat="server" AutoGenerateColumns="False" Font-Names="Verdana"
                        Font-Size="X-Small">
                        <Columns>
                            <asp:TemplateField HeaderText="Remove">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnRemove" runat="server" CommandArgument='<%# Bind("FileName") %>' CommandName="Delete">Remove</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="File">
                                <ItemTemplate>
                                    <asp:Label ID="Label11" runat="server" Text='<%# Bind("DocPicLink") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                                <asp:HiddenField ID="hdfImageDocPath_Temp" runat="server" />
                                <asp:HiddenField ID="hdfImageDocPath_Temp_Open" runat="server" />
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                    <table style="width: 450px">
                        <tr>
                            <td style="width: 150px">
                                <asp:Button ID="btnAdd" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Add"
                                    Width="85px" OnClientClick="showDate();" /><asp:Button ID="btnGeneratePO" runat="server" Text="Generate & Preview PO" OnClientClick="showDate();" /></td>
                            <td style="width: 150px">
                                <asp:Button ID="btnClear" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Clear"
                                    Width="85px" /></td>
                            <td style="width: 150px">
                                <asp:Button ID="btnRefresh" runat="server" Text="Refresh" /></td>
                        </tr>
                    </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <table>
                        <tr>
                            <td style="width: 100px">
                            </td>
                            <td style="width: 1px">
                                &nbsp;</td>
                            <td>
                    <asp:GridView ID="gvPO" runat="server" AutoGenerateColumns="False" Font-Names="Verdana"
                        Font-Size="X-Small" DataKeyNames="POID">
                        <Columns>
                            <asp:TemplateField HeaderText="Remove">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnRemove" runat="server" CommandName="Delete" CommandArgument='<%# Bind("POID") %>' Enabled='<%# Bind("AllowDelete") %>' OnClientClick="return confirm('Are you sure you want to remove this Purchase Order?');">Remove</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PO No">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnPONo" runat="server"
                                        Text='<%# Bind("PONo") %>' CommandName="Select" CommandArgument='<%# Bind("POID") %>'></asp:LinkButton>
                                    <asp:HiddenField ID="hfApplyNewNo" runat="server" Value='<%# Bind("ApplyNewID") %>' />
                                    <asp:HiddenField ID="hdfVendorID" runat="server" Value='<%# Bind("VendorID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PO Date">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("PODate") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblPODate" runat="server" Text='<%# Bind("PODate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="VendorPayeeName" HeaderText="Vendor" />
                            <asp:TemplateField HeaderText="Amount">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("TotalAmount") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("TotalAmount") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PO">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("PONo") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblPO" runat="server" Text='<%# Bind("DocPicLink") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quotation#">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("QuotationReceiptNo") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("QuotationReceiptNo") %>'></asp:Label>
                                    <asp:HiddenField ID="hfQuotationReceiptID" runat="server" Value='<%# Bind("QuotationReceiptIDList") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remarks">
                                <ItemTemplate>
                                    <asp:Label ID="Label13" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="Label12" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Comment"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:TextBox ID="txtComment" runat="server" Height="61px" TextMode="MultiLine" Width="450px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                            </td>
                            <td style="width: 1px">
                            </td>
                            <td>
                    <table style="width: 450px">
                        <tr>
                            <td style="width: 150px">
                                <asp:Button ID="btnSend" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Send All"
                                    Width="85px" OnClientClick="showDate();"/></td>
                            <td style="width: 150px">
                                <asp:Button ID="btnClose" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Return To Page"
                                    Width="120px" /></td>
                            <td style="width: 150px">
                                &nbsp;<asp:Button ID="btnSave" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Save"
                                    Width="85px" Visible="False" /></td>
                        </tr>
                    </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td colspan="5">
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
