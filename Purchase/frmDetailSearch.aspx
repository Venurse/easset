<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmDetailSearch.aspx.vb" Inherits="Purchase_frmDetailSearch" %>

<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Detail Search</title>
<%--     <link href="Style.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <form id="frmDetailSearch" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 34px">
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Detail Search"></asp:Label></td>
                <td style="font-size: 12pt; font-family: Times New Roman">
                </td>
            </tr>
            <tr style="font-size: 12pt; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    &nbsp;</td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="8">
                    <table>
                        <tr>
                            <td style="width: 100px">
                            </td>
                            <td style="font-size: 12pt; width: 1px; font-family: Times New Roman">
                            </td>
                            <td style="width: 400px">
                            </td>
                            <td style="width: 35px">
                            </td>
                            <td style="width: 100px">
                            </td>
                            <td style="width: 1px">
                            </td>
                            <td style="width: 450px">
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td style="width: 100px">
                                <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Station"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlStation" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                            <td>
                            </td>
                            <td>
                                <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Doc Type"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlDocType" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                    <asp:ListItem></asp:ListItem>
                                    <asp:ListItem>Quotation/Receipt</asp:ListItem>
                                    <asp:ListItem>Purchase Order</asp:ListItem>
                                    <asp:ListItem>Invoice</asp:ListItem>
                                    <asp:ListItem>Delivery Order</asp:ListItem>
                                    <asp:ListItem>Payment Voucher</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td style="width: 100px">
                                <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Payee"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:TextBox ID="txtPayee" runat="server" Enabled="False" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="294px"></asp:TextBox><asp:Button ID="btnVendor" runat="server" Text="...." /><asp:HiddenField
                                        ID="hdfPayee" runat="server" />
                                <asp:HiddenField ID="hdfPayeeType" runat="server" />
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:Label ID="Label7" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Doc No"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:TextBox ID="txtDocNo" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    MaxLength="50" Width="150px"></asp:TextBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td style="width: 100px">
                                <asp:Label ID="Label6" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Purchase No"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:TextBox ID="txtPurchaseNo" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    MaxLength="50" Width="150px"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                                <asp:Label ID="Label9" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Doc Date"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <ew:CalendarPopup ID="cldDateFrom" runat="server" Nullable="True" Width="70px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                                &nbsp;<asp:Label ID="Label10" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Text="To"></asp:Label>
                                <ew:CalendarPopup ID="cldDateTo" runat="server" Nullable="True" Width="70px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td style="width: 100px">
                                <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Asset No"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:TextBox ID="txtAssetNo" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    MaxLength="50" Width="147px"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                                <asp:Label ID="Label11" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Asset ID"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:TextBox ID="txtAssetID" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    MaxLength="50" Width="147px"></asp:TextBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td style="width: 100px">
                            </td>
                            <td style="width: 1px">
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" Text="Search" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
        <asp:Menu ID="Menu1" runat="server" BackColor="#FFFBD6" DynamicHorizontalOffset="2"
            Font-Names="Verdana" Font-Size="Small" ForeColor="#990000" Orientation="Horizontal"
            StaticSubMenuIndent="10px" Width="678px">
            <StaticSelectedStyle BackColor="#FFCC66" />
            <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
            <DynamicHoverStyle BackColor="#990000" ForeColor="White" />
            <DynamicMenuStyle BackColor="#FFFBD6" />
            <DynamicSelectedStyle BackColor="#FFCC66" />
            <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
            <StaticHoverStyle BackColor="#990000" ForeColor="White" />
            <Items>
                <asp:MenuItem Text="Asset" Value="Tab1"></asp:MenuItem>
                <asp:MenuItem Text="Quotation / Receipt" Value="Tab2"></asp:MenuItem>
                <asp:MenuItem Text="Purchase Order" Value="Tab3"></asp:MenuItem>
                <asp:MenuItem Text="Invoice" Value="Tab4"></asp:MenuItem>
                <asp:MenuItem Text="Delivery Order" Value="Tab5"></asp:MenuItem>
                <asp:MenuItem Text="Payment Voucher" Value="Tab6"></asp:MenuItem>
            </Items>
        </asp:Menu>
                    <br />
        <asp:MultiView ID="mv" runat="server" ActiveViewIndex="0">
            <asp:View ID="Tab1" runat="server">
                <asp:GridView ID="gvAsset" runat="server" BackColor="White" BorderColor="White" BorderStyle="Ridge"
                    BorderWidth="2px" CellPadding="3" CellSpacing="1" EmptyDataText="Record Not Found"
                    Font-Names="Verdana" Font-Size="X-Small" GridLines="None" AutoGenerateColumns="False" DataKeyNames="AssetID">
                    <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                    <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                    <Columns>
                        <asp:TemplateField HeaderText="Asset ID">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("AssetID") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnAssetID" runat="server" CommandArgument='<%# Bind("AssetID") %>'
                                    CommandName="Select" Text='<%# Bind("AssetID") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="AssetNo" HeaderText="Asset No" />
                        <asp:BoundField DataField="Category" HeaderText="Category" />
                        <asp:BoundField DataField="Brand" HeaderText="Brand" />
                        <asp:BoundField DataField="Model" HeaderText="Model" />
                        <asp:BoundField DataField="PurchaseCost" HeaderText="Purchase Cost" />
                    </Columns>
                </asp:GridView>
            </asp:View>
            <asp:View ID="Tab2" runat="server">
                <asp:GridView ID="gvQuotRcpt" runat="server" BackColor="White" BorderColor="White"
                    BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" CellSpacing="1" EmptyDataText="Record Not Found"
                    Font-Names="Verdana" Font-Size="X-Small" GridLines="None" AutoGenerateColumns="False">
                    <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                    <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                    <Columns>
                        <asp:BoundField DataField="QuotationReceiptNo" HeaderText="Quotation/Receipt No" />
                        <asp:BoundField DataField="QuotationReceiptDate" HeaderText="Doc Date" />
                        <asp:BoundField DataField="Q_TotalAmount" HeaderText="Amount" />
                        <asp:BoundField DataField="PaymentTerm" HeaderText="Payment Term" />
                        <asp:BoundField DataField="VendorPayee" HeaderText="Vendor / Payee" />
                        <asp:BoundField DataField="ShopName" HeaderText="Shop Name" />
                        <asp:TemplateField HeaderText="File">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("DocPicLink") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("DocPicLink") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:View>
            <asp:View ID="Tab3" runat="server">
                <asp:GridView ID="gvPO" runat="server" BackColor="White" BorderColor="White" BorderStyle="Ridge"
                    BorderWidth="2px" CellPadding="3" CellSpacing="1" EmptyDataText="Record Not Found"
                    Font-Names="Verdana" Font-Size="X-Small" GridLines="None" AutoGenerateColumns="False" DataKeyNames="POID">
                    <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                    <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                    <Columns>
                        <asp:TemplateField HeaderText="PO No">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("PONo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%# Bind("PONo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="PODate" HeaderText="PO Date" />
                        <asp:BoundField DataField="PO_VendorName" HeaderText="Vendor" />
                        <asp:BoundField DataField="PO_TotalAmount" HeaderText="Total Amount" />
                        <asp:TemplateField HeaderText="File">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("DocPicLink") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("DocPicLink") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:View>
            <asp:View ID="Tab4" runat="server">
                <asp:GridView ID="gvInvoice" runat="server" BackColor="White" BorderColor="White"
                    BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" CellSpacing="1" EmptyDataText="Record Not Found"
                    Font-Names="Verdana" Font-Size="X-Small" GridLines="None" AutoGenerateColumns="False" DataKeyNames="InvoiceID">
                    <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                    <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                    <Columns>
                        <asp:BoundField DataField="InvoiceNo" HeaderText="Invoice No" />
                        <asp:BoundField DataField="InvoiceDate" HeaderText="Invoice Date" />
                        <asp:BoundField DataField="InvoiceAmount" HeaderText="Invoice Amount" />
                        <asp:BoundField DataField="BalanceAmount" HeaderText="Balance Amount" />
                        <asp:BoundField DataField="Inv_VendorName" HeaderText="Vendor" />
                        <asp:TemplateField HeaderText="File">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("DocPicLink") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("DocPicLink") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:View>
            <asp:View ID="Tab5" runat="server">
                <asp:GridView ID="gvDO" runat="server" BackColor="White" BorderColor="White" BorderStyle="Ridge"
                    BorderWidth="2px" CellPadding="3" CellSpacing="1" EmptyDataText="Record Not Found"
                    Font-Names="Verdana" Font-Size="X-Small" GridLines="None" AutoGenerateColumns="False" DataKeyNames="DOID">
                    <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                    <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                    <Columns>
                        <asp:BoundField DataField="DONo" HeaderText="DO No" />
                        <asp:BoundField DataField="DODate" HeaderText="DO Date" />
                        <asp:BoundField DataField="DO_VendorName" HeaderText="Vendor" />
                        <asp:TemplateField HeaderText="File">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("DocPicLink") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("DocPicLink") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:View>
            <asp:View ID="Tab6" runat="server">
                <asp:GridView ID="gvPayment" runat="server" BackColor="White" BorderColor="White"
                    BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" CellSpacing="1" EmptyDataText="Record Not Found"
                    Font-Names="Verdana" Font-Size="X-Small" GridLines="None" AutoGenerateColumns="False">
                    <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                    <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                    <Columns>
                        <asp:BoundField DataField="PaymentVoucherNo" HeaderText="Payment Voucher No" />
                        <asp:BoundField DataField="PaymentType" HeaderText="Payment Type" />
                        <asp:BoundField DataField="Payee" HeaderText="Payee" />
                        <asp:BoundField DataField="PaymentDate" HeaderText="Payment Date" />
                        <asp:BoundField DataField="Amount" HeaderText="Amount" />
                        <asp:TemplateField HeaderText="File">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("DocPicLink") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("DocPicLink") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Status" HeaderText="Status" />
                        <asp:TemplateField HeaderText="Reason">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Reason") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Reason") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:View>
        </asp:MultiView></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td colspan="6">
                    &nbsp;</td>
            </tr>
        </table>
        <br />
        <br />
        <br />
    </div>
        &nbsp; &nbsp;&nbsp;
    </form>
</body>
</html>
