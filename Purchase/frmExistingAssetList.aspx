<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmExistingAssetList.aspx.vb" Inherits="Purchase_frmExistingAssetList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Existing Asset List</title>
</head>
<body>
    <form id="frmExistingAssetList" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Asset List"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <table>
                        <tr>
                            <td style="width: 50px">
                                <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Station"></asp:Label></td>
                            <td style="width: 5px">
                                :</td>
                            <td style="width: 150px">
                                <asp:DropDownList ID="ddlStation" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                            <td style="width: 20px">
                            </td>
                            <td style="width: 80px">
                                <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Status"></asp:Label></td>
                            <td style="width: 5px">
                                :</td>
                            <td style="width: 100px">
                                <asp:DropDownList ID="ddlStatus" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                            <td style="width: 20px">
                            </td>
                            <td style="width: 40px">
                                <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Item"></asp:Label></td>
                            <td style="width: 5px">
                                :</td>
                            <td colspan="2">
                                <asp:DropDownList ID="ddlItem" runat="server" Font-Names="Verdana" Font-Size="X-Small" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlSubCategory" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 40px">
                                <asp:Label ID="Label6" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Asset No"></asp:Label></td>
                            <td style="width: 5px">
                                :</td>
                            <td style="width: 100px">
                                <asp:TextBox ID="txtAssetNo" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:TextBox></td>
                            <td style="width: 20px">
                            </td>
                            <td>
                                <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Year"></asp:Label></td>
                            <td style="width: 5px">
                                :</td>
                            <td style="width: 100px">
                                <asp:DropDownList ID="ddlYear" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                            <td style="width: 20px">
                            </td>
                            <td style="width: 40px">
                            </td>
                            <td style="width: 5px">
                                :</td>
                            <td style="width: 209px">
                                <asp:TextBox ID="txtCategoryDesc" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="197px"></asp:TextBox></td>
                            <td>
                                </td>
                        </tr>
                        <tr>
                            <td style="width: 40px">
                                <asp:Label ID="Label8" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Asset ID"></asp:Label></td>
                            <td style="width: 5px">
                                :</td>
                            <td style="width: 100px">
                                <asp:TextBox ID="txtAssetID" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:TextBox></td>
                            <td style="width: 20px">
                            </td>
                            <td>
                                &nbsp;</td>
                            <td style="width: 5px">
                                &nbsp;</td>
                            <td style="width: 100px">
                                &nbsp;</td>
                            <td style="width: 20px">
                            </td>
                            <td style="width: 40px">
                            </td>
                            <td style="width: 5px">
                            </td>
                            <td style="width: 209px">
                                <asp:Button ID="btnQuery" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Query"
                                    Width="85px" /></td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <asp:GridView ID="gvAssetList" runat="server" AutoGenerateColumns="False" BackColor="White"
                        BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" Font-Names="Verdana"
                        Font-Size="X-Small" GridLines="Horizontal">
                        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                        <Columns>
                            <asp:BoundField DataField="StationCode" HeaderText="Station" Visible="False" />
                            <asp:BoundField DataField="Status" HeaderText="Status" />
                            <asp:TemplateField HeaderText="Asset ID">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("AssetID") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnAssetID" runat="server" CommandArgument='<%# Bind("AssetID") %>'
                                        Text='<%# Bind("AssetID") %>' CommandName="Select"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Location" HeaderText="Location" />
                            <asp:BoundField DataField="Owner" HeaderText="Owner" />
                            <asp:BoundField DataField="AssetNo" HeaderText="Asset No" />
                            <asp:BoundField DataField="Category" HeaderText="Item" />
                            <asp:BoundField DataField="BrandModel" HeaderText="Brand / Model" />
                            <asp:BoundField DataField="Qty" HeaderText="Qty" />
                            <asp:TemplateField HeaderText="Purchase Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblPurchaseDate" runat="server" 
                                        Text='<%# Bind("RequiredDate") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("RequiredDate") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                        <AlternatingRowStyle BackColor="#F7F7F7" />
                    </asp:GridView>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
