<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmPurchaseDetail.aspx.vb" Inherits="Purchase_frmPurchaseDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
<META HTTP-EQUIV="EXPIRES" CONTENT="0">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Purchase Detail</title>
</head>
<body>
    <form id="frmPurchaseDetail" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Purchase Detail"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <table>
                        <tr>
                            <td style="width: 150px">
                                &nbsp;</td>
                            <td style="font-size: 12pt; width: 1px; font-family: Times New Roman">
                            </td>
                            <td style="font-size: 12pt; font-family: Times New Roman">
                            </td>
                        </tr>
                        <tr>
                            <td>
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Purchase No"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td style="font-size: 12pt; font-family: Times New Roman">
                    <asp:Label ID="lblPurchaseNo" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                        ForeColor="#C00000"></asp:Label>
                    <asp:HiddenField ID="hdfApplyNewID" runat="server" />
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Station"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <asp:Label ID="lblStation" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label>
                                <asp:HiddenField ID="hdfStationID" runat="server" />
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Vendor / Payee"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:GridView ID="gvVendorPayee" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                    Font-Names="Verdana" Font-Size="X-Small" ShowHeader="False">
                                    <Columns>
                                        <asp:BoundField DataField="VendorPayeeName" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label6" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Quotation / Receipt"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                                <asp:GridView ID="gvQuoReceipt" runat="server" AutoGenerateColumns="False" Font-Names="Verdana"
                                    Font-Size="X-Small">
                                    <Columns>
                                        <asp:BoundField DataField="QuotationReceiptNo" HeaderText="Quotation / Receipt No" />
                                        <asp:TemplateField HeaderText="File">
                                            <ItemTemplate>
                                                <asp:Label ID="lblQuoReceipt" runat="server" Text='<%# Bind("DocPicLink") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" Font-Names="Verdana"
                        Font-Size="X-Small" EmptyDataText="No Record Found" DataKeyNames="AssetID">
                        <Columns>
                            <asp:TemplateField HeaderText="Item">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnItem" runat="server" CommandArgument='<%# Bind("AssetID") %>'
                                        Text='<%# Bind("Category") %>' CommandName="SelectItem"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Type" HeaderText="Type" />
                            <asp:BoundField DataField="PricePerUnit" HeaderText="Price Per Unit" />
                            <asp:BoundField DataField="Quantity" HeaderText="Quantity" />
                            <asp:BoundField DataField="TotalAmount" HeaderText="Total Amount" />
                            <asp:BoundField DataField="QuotationReceiptNo" HeaderText="Quotation / Receipt" />
                            <asp:TemplateField HeaderText="Cancel Item">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnCancelItem" runat="server" CommandArgument='<%# Bind("ItemKey") %>'
                                        CommandName="CancelItem" Enabled='<%# Bind("AllowCancelItem") %>'>Cancel Item</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <br />
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="3">
                    <asp:Button ID="btnAddPO" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Add Purchase Order (PO)"
                                    Width="181px" /></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <asp:GridView ID="gvPO" runat="server" AutoGenerateColumns="False" Font-Names="Verdana"
                        Font-Size="X-Small" DataKeyNames="POID" EmptyDataText="No Record Found">
                        <Columns>
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandArgument='<%# Bind("POID") %>'
                                        CommandName="delete" Enabled='<%# Bind("AllowDelete") %>' OnClientClick="return confirm('Are you sure you want to remove this Purchase Order?');">Delete</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PO No">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnPONo" runat="server" CommandArgument='<%# Bind("POID") %>'
                                        Text='<%# Bind("PONo") %>' CommandName="Select"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="PODate" HeaderText="PO Date" />
                            <asp:BoundField DataField="VendorPayeeName" HeaderText="Vendor/ Payee" />
                            <asp:BoundField DataField="TotalAmount" HeaderText="Amount" />
                            <asp:TemplateField HeaderText="PO">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PONo") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("DocPicLink") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="QuotationReceiptNo" HeaderText="Quotation#" />
                            <asp:BoundField DataField="SendDetail" HeaderText="Send PO Detail" />
                        </Columns>
                    </asp:GridView>
                    <br />
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="3">
                    <asp:Button ID="btnAddInvoice" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Add Invoice"
                                    Width="181px" /></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <asp:GridView ID="gvInvoice" runat="server" AutoGenerateColumns="False" Font-Names="Verdana"
                        Font-Size="X-Small" DataKeyNames="InvoiceID" EmptyDataText="No Record Found">
                        <Columns>
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandArgument='<%# Bind("InvoiceID") %>'
                                        CommandName="delete" OnClientClick="return confirm('Are you sure you want to remove this Invoice?');" Enabled='<%# Bind("AllowDelete") %>'>Delete</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Invoice No">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnInvoiceNo" runat="server" CommandArgument='<%# Bind("InvoiceID") %>'
                                        Text='<%# Bind("InvoiceNo") %>' CommandName="select"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="InvoiceDate" HeaderText="Invoice Date" />
                            <asp:BoundField DataField="InvoiceAmount" HeaderText="Invoice Amount" />
                            <asp:TemplateField HeaderText="Invoice">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("DocPicLink") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="PONo" HeaderText="PO No" />
                        </Columns>
                    </asp:GridView>
                    <br />
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="3">
                    <asp:Button ID="btnAddDO" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Add Delivery Order (DO)"
                                    Width="181px" /></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <asp:GridView ID="gvDO" runat="server" AutoGenerateColumns="False" Font-Names="Verdana"
                        Font-Size="X-Small" DataKeyNames="DOID" EmptyDataText="No Record Found">
                        <Columns>
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandArgument='<%# Bind("DOID") %>'
                                        CommandName="delete" OnClientClick="return confirm('Are you sure you want to remove this Delivery Order?');">Delete</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DO No">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnDONo" runat="server" CommandArgument='<%# Bind("DOID") %>'
                                        Text='<%# Bind("DONo") %>' CommandName="Select"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="DODate" HeaderText="DO Date" />
                            <asp:TemplateField HeaderText="DO">
                                <ItemTemplate>
                                    &nbsp;<asp:Label ID="Label5" runat="server" Text='<%# Bind("DocPicLink") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="PONo" HeaderText="PO No" />
                        </Columns>
                    </asp:GridView>
                    <br />
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="3">
                    <asp:Button ID="btnAddPayment" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="* Add Payment"
                                    Width="181px" /></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td style="height: 21px">
                </td>
                <td colspan="8" style="height: 21px">
                    <asp:GridView ID="gvPayment" runat="server" AutoGenerateColumns="False" Font-Names="Verdana"
                        Font-Size="X-Small" DataKeyNames="PaymentID" EmptyDataText="No Record Found">
                        <Columns>
                            <asp:TemplateField HeaderText="Payment Voucher No">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnPaymentNo" runat="server" CommandArgument='<%# Bind("PaymentID") %>'
                                        Text='<%# Bind("PaymentVoucherNo") %>' CommandName="SELECT"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="PaymentType" HeaderText="Payment Type" />
                            <asp:BoundField DataField="PayeeName" HeaderText="Payee" />
                            <asp:BoundField DataField="PaymentDate" HeaderText="Payment Date" />
                            <asp:BoundField DataField="Amount" HeaderText="Voucher Amount" />
                            <asp:BoundField DataField="DocNo" HeaderText="Doc No" />
                            <asp:BoundField DataField="PaymentAmount" HeaderText="Pay Amount" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
