Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Purchase_frmCancelPurchaseItem
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack() = False Then
            hdfApplyNewID.Value = ""
            hdfeRequsitionNo.Value = ""

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            btnCancelItem.Enabled = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "P0010")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Cancel Purchase Item Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnCancelItem.Enabled = True
                    Else
                        Session("blnWrite") = False
                        btnCancelItem.Enabled = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If


            If Session("CancelItem_ApplyNewID").ToString <> "" Then
                hdfApplyNewID.Value = Session("CancelItem_ApplyNewID").ToString
                Session("CancelItem_ApplyNewID") = ""
            End If

            If Session("CancelItem_AssetID").ToString <> "" Then
                hdfAssetID.Value = Session("CancelItem_AssetID").ToString
                Session("CancelItem_AssetID") = ""
            End If

            If Session("CancelItem_QRID").ToString <> "" Then
                hdfQRID.Value = Session("CancelItem_QRID").ToString
                Session("CancelItem_QRID") = ""
            End If

            If Session("CancelItem_QRItemID").ToString <> "" Then
                hdfItemID.Value = Session("CancelItem_QRItemID").ToString
                Session("CancelItem_QRItemID") = ""
            End If


            GetCancelItemDetail(hdfApplyNewID.Value.ToString, hdfAssetID.Value.ToString, hdfQRID.Value.ToString, CInt(hdfItemID.Value.ToString))

            'Me.btnCancelItem.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnCancelItem, "", "btnClear", "btnClose", "", "", ""))
        Else

        End If
    End Sub

    Private Sub GetCancelItemDetail(ByVal strApplyNewID As String, ByVal strAssetID As String, ByVal strQRID As String, ByVal intItemID As Integer)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetCancelItemDetail")

                With MyCommand
                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = strApplyNewID
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = strAssetID
                    .Parameters.Add("@QuotationReceiptID", SqlDbType.VarChar, 20)
                    .Parameters("@QuotationReceiptID").Value = strQRID
                    .Parameters.Add("@QRItemID", SqlDbType.Int)
                    .Parameters("@QRItemID").Value = intItemID
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "Item"

                lblItem.Text = MyData.Tables(0).Rows(0)("Category").ToString
                lblType.Text = MyData.Tables(0).Rows(0)("Type").ToString
                txtCancelQty.Text = MyData.Tables(0).Rows(0)("No_Of_Units").ToString
                lblTotalQty.Text = MyData.Tables(0).Rows(0)("No_Of_Units").ToString
                lblUnitPrice.Text = MyData.Tables(0).Rows(0)("PricePerUnit").ToString
                hdfeRequsitionNo.Value = MyData.Tables(0).Rows(0)("eRequisitionNo").ToString()

                If hdfeRequsitionNo.Value.ToString() <> "" Then
                    txtCancelQty.Enabled = False
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "GetCancelItemDetail: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Dim ParentFormID As String = ""
        ParentFormID = Request.QueryString("ParentForm").ToString

        'Session("ApplyNewID") = ""
        Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener =self;window.close();</script>")
    End Sub

    Protected Sub btnCancelItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelItem.Click

        If txtCancelQty.Text.ToString = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Cancel Quantity.')</script>")
            txtReason.Focus()
            Exit Sub
        End If

        If CInt(txtCancelQty.Text.ToString) > CInt(lblTotalQty.Text.ToString) Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Cancel Quantity. Should be less than or equal to Total Quantity.')</script>")
            txtReason.Focus()
            Exit Sub
        End If

        If txtReason.Text.ToString = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please provide Reason.')</script>")
            txtReason.Focus()
            Exit Sub
        End If

        If hdfeRequsitionNo.Value.ToString <> "" Then
            If CInt(txtCancelQty.Text.ToString) = CInt(lblTotalQty.Text.ToString) Then
                Dim strReturnMsg As String = ""

                strReturnMsg = ""
                strReturnMsg = clsCF.CancelRequisition(hdfAssetID.Value.ToString(), hdfeRequsitionNo.Value.ToString, Session("DA_UserID").ToString, hdfCurrentDateTime.Value.ToString())

                If strReturnMsg = "" Then
                    'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Cancel eRequisition Successful')</script>")
                Else
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Cancel Fail. '" + strReturnMsg + "')</script>")
                    Exit Sub
                End If
            End If
        End If

        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String = ""

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_CancelPurchaseItem", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ApplyNewID", SqlDbType.NVarChar, 20)
                    .Parameters("@ApplyNewID").Value = hdfApplyNewID.Value.ToString
                    .Parameters.Add("@AssetID", SqlDbType.NVarChar, 20)
                    .Parameters("@AssetID").Value = hdfAssetID.Value.ToString
                    .Parameters.Add("@QuotationReceiptID", SqlDbType.VarChar, 20)
                    .Parameters("@QuotationReceiptID").Value = hdfQRID.Value.ToString
                    .Parameters.Add("@QRItemID", SqlDbType.Int)
                    .Parameters("@QRItemID").Value = hdfItemID.Value.ToString
                    .Parameters.Add("@CancelQty", SqlDbType.Int)
                    .Parameters("@CancelQty").Value = CInt(txtCancelQty.Text.ToString)
                    .Parameters.Add("@Reason", SqlDbType.NVarChar, 500)
                    .Parameters("@Reason").Value = txtReason.Text.ToString

                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Cancel Successful')</script>")
                    btnCancelItem.Enabled = False
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "btnCancelItem_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

End Class
