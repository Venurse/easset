<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmPurchaseApproval.aspx.vb" Inherits="Purchase_frmPurchaseApproval" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Purchase Approval</title>
    <script type="text/javascript">
         function showDate() 
        {
            var month_names = new Array("Jan", "Feb", "Mar", 
            "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
            "Oct", "Nov", "Dec");

            var d = new Date();
            var curr_day = d.getDay();
            var curr_date = d.getDate();
            var hours = d.getHours();
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            
            var curr_hours;
            var curr_minutes;
            var curr_seconds;

            if(hours<10)
            {
                curr_hours = "0" + hours;
            }
            else
            {
                curr_hours = hours;
            }
            
            if(minutes < 10)
            {
                curr_minutes = "0" + minutes;
            }
             else
            {
                curr_minutes = minutes;
            }
            
           if(seconds < 10)
            {
                curr_seconds = "0" + seconds;
            }
             else
            {
                curr_seconds = seconds;
            }

            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
 
            document.getElementById("hdfCurrentDateTime").value = curr_date + " " +  month_names[curr_month] +  " " + curr_year + " " + curr_hours + ":" + curr_minutes + ":" + curr_seconds
         }
     </script>
</head>
<body>
    <form id="frmPurchaseApproval" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Approval"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:HiddenField ID="hdfCurrentDateTime" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:GridView ID="gvApplyNewList" runat="server" AutoGenerateColumns="False" Font-Names="Verdana" Font-Size="X-Small" DataKeyNames="ApplyNewID" EmptyDataText="Record Not Found">
                        <Columns>
                            <asp:TemplateField HeaderText="Select">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnSelect" runat="server" CommandArgument='<%# Bind("ApplyNewID") %>' CommandName="select">Select</asp:LinkButton>
                                    <asp:HiddenField ID="hdfApprovalLevel" runat="server" Value='<%# Bind("Approval_Level") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="StationCode" HeaderText="Station" />
                            <asp:BoundField DataField="ApplyNewID" HeaderText="Purchase No" />
                            <asp:BoundField DataField="Purchase_Description" HeaderText="Description" />
                            <asp:BoundField DataField="ApplyNewType" HeaderText="Type" />
                            <asp:BoundField DataField="CreatedBy_Name" HeaderText="Apply By" />
                            <asp:BoundField DataField="CreatedOn" HeaderText="Apply On" />
                            <asp:BoundField DataField="Approval_Status" HeaderText="Status" />
                        </Columns>
                        <EmptyDataRowStyle BorderColor="Silver" ForeColor="Red" />
                    </asp:GridView>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table>
                        <tr>
                            <td style="width: 150px">
                                &nbsp;</td>
                            <td style="font-size: 12pt; width: 1px; font-family: Times New Roman">
                            </td>
                            <td style="font-size: 12pt; font-family: Times New Roman">
                                <asp:HiddenField ID="hdfLevel" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 150px">
                    <asp:Label ID="Label6" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Purchase No"></asp:Label></td>
                            <td style="font-size: 12pt; width: 1px; font-family: Times New Roman">
                                :</td>
                            <td style="font-size: 12pt; font-family: Times New Roman">
                    <asp:Label ID="lblPurchaseNo" runat="server" Font-Names="Verdana" Font-Size="Small" Font-Bold="True" ForeColor="Blue"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td style="width: 100px">
                    <asp:Label ID="Label8" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Purchase Description"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:Label ID="lblPurchaseDesc" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Created By"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:Label ID="lblCreatedBy" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Station"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:Label ID="lblStation" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label11" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Type"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:Label ID="lblType" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                    <asp:Label ID="Label7" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Purchase Reason"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:Label ID="lblPurchaseReason" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                                <asp:Label ID="Label12" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Remarks"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                                <asp:Label ID="lblRemarks" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                        Text="Quotation :- "></asp:Label></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <asp:GridView ID="gvHeader" runat="server" AutoGenerateColumns="False" BackColor="White"
                        BorderColor="#C0C0FF" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" Font-Names="Verdana"
                        Font-Size="X-Small" GridLines="Horizontal" DataKeyNames="QuotationCompareID">
                        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                        <Columns>
                            <asp:BoundField DataField="No" HeaderText="No" />
                            <asp:BoundField DataField="PayeeName" HeaderText="Vendor/Payee" />
                            <asp:BoundField DataField="TotalAmount" HeaderText="Total Amount" />
                            <asp:TemplateField HeaderText="Quotation/Receipt">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
                                        ShowHeader="False">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label11" runat="server" Text='<%# Bind("Links") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:HiddenField ID="hdfQuotationCompareID" runat="server" Value='<%# Bind("QuotationCompareID") %>' />
                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:eAssetConnectionString %>"
                                        ProviderName="<%$ ConnectionStrings:eAssetConnectionString.ProviderName %>"
                                        SelectCommand="Proc_GetCompareQuoteFile" SelectCommandType="StoredProcedure">
                                        <SelectParameters>
<%--                                            <asp:SessionParameter Name="gidMySupportID" SessionField="gidMySupportID" Type="String" />--%>
                                            <asp:ControlParameter ControlID="hdfQuotationCompareID" Name="QuotationCompareID" PropertyName="Value" Type="String" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Detail" Visible="False">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnView" runat="server" CommandArgument='<%# Bind("QuotationCompareID") %>' CommandName="select">View Detail</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Select">
                                <ItemTemplate>
                                    <asp:RadioButton ID="rbtnSelect" runat="server" AutoPostBack="True" OnCheckedChanged="rbtnSelect_CheckedChanged" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                        <AlternatingRowStyle BackColor="#F7F7F7" />
                    </asp:GridView>
                    <asp:HiddenField ID="hdfQuotationCompareID" runat="server" />
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <asp:GridView ID="gvDetail" runat="server" Caption="Detail" CellPadding="4" Font-Names="Verdana"
                        Font-Size="X-Small" ForeColor="#333333" GridLines="Vertical" AutoGenerateColumns="False">
                        <RowStyle BackColor="#EFF3FB" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:BoundField DataField="CompareID" HeaderText="Compare ID" Visible="False" />
                            <asp:BoundField DataField="QuotationReceiptNo" HeaderText="Quotation/Receipt No" />
                            <asp:BoundField DataField="Category" HeaderText="Category" />
                            <asp:BoundField DataField="Brand" HeaderText="Brand" />
                            <asp:BoundField DataField="Model" HeaderText="Model" />
                            <asp:TemplateField HeaderText="Specification">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Specification") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Specification") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Quantity" HeaderText="Quantity" />
                            <asp:BoundField DataField="PricePerUnit" HeaderText="Price Per Unit" />
                            <asp:BoundField DataField="Type" HeaderText="Type" />
                            <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                            <asp:BoundField DataField="UserName" HeaderText="User" />
                            <asp:BoundField DataField="Approval_Desc" HeaderText="Budget Status">
                                <ItemStyle ForeColor="Red" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Select">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" Checked='<%# Bind("Select") %>' Enabled='<%# Bind("Enable") %>' />
                                    <asp:HiddenField ID="hdfQRCategoryID" runat="server" Value='<%# Bind("QRCategoryID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <table>
                        <tr>
                            <td style="width: 150px">
                    <asp:Label ID="Label9" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Reason"></asp:Label></td>
                            <td style="font-size: 12pt; width: 1px; font-family: Times New Roman">
                                :</td>
                            <td style="font-size: 12pt; font-family: Times New Roman">
                    <asp:TextBox ID="txtApprovalReason" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Height="50px" TextMode="MultiLine" Width="500px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 150px">
                    <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Remarks"></asp:Label></td>
                            <td style="font-size: 12pt; width: 1px; font-family: Times New Roman">
                                :</td>
                            <td style="font-size: 12pt; font-family: Times New Roman">
                    <asp:TextBox ID="txtRemarks" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Height="150px" TextMode="MultiLine" Width="500px"></asp:TextBox></td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td style="width: 100px">
                    <asp:Label ID="Label10" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Approval History"></asp:Label></td>
                            <td style="width: 1px">
                                :</td>
                            <td>
                    <asp:GridView ID="gvApprovalHistory" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        Font-Names="Verdana" Font-Size="X-Small" ForeColor="#333333" GridLines="None">
                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                        <Columns>
                            <asp:BoundField DataField="ApprovedBy" HeaderText="Approved By" />
                            <asp:BoundField DataField="ApprovedOn" HeaderText="Approved On" />
                            <asp:BoundField DataField="Reason" HeaderText="Reason" />
                            <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                            <asp:BoundField DataField="Status" HeaderText="Status" />
                        </Columns>
                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                            </td>
                            <td style="width: 1px">
                            </td>
                            <td>
                    <table style="width: 450px">
                        <tr>
                            <td style="width: 150px">
                                <asp:Button ID="btnApprove" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Approve"
                                    Width="100px" OnClientClick="showDate();" /></td>
                            <td style="width: 150px"><asp:Button ID="btnReject" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Reject All"
                                    Width="100px" OnClientClick="showDate();" /></td>
                            <td style="width: 150px"><asp:Button ID="btnModificationReq" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Modification Request"
                                    Width="155px" OnClientClick="showDate();" /></td>
                        </tr>
                    </table>
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                            </td>
                            <td style="width: 1px">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td>
                            </td>
                            <td style="width: 1px">
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td colspan="5">
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
