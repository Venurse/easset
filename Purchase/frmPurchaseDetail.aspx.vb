Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Purchase_frmPurchaseDetail
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Page.MaintainScrollPositionOnPostBack = True


        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If IsPostBack() = False Then
            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            btnAddPO.Enabled = False
            btnAddInvoice.Enabled = False
            btnAddDO.Enabled = False
            btnAddPayment.Enabled = False
            gvPO.Columns(0).Visible = False
            gvInvoice.Columns(0).Visible = False
            gvDO.Columns(0).Visible = False
            gvPayment.Columns(0).Visible = False


            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "P0004")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Purchase Detail Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnAddPO.Enabled = True
                        btnAddInvoice.Enabled = True
                        btnAddDO.Enabled = True
                        btnAddPayment.Enabled = True
                        gvPO.Columns(0).Visible = True
                        gvInvoice.Columns(0).Visible = True
                        gvDO.Columns(0).Visible = True
                        gvPayment.Columns(0).Visible = True
                    Else
                        Session("blnWrite") = False
                        btnAddPO.Enabled = False
                        btnAddInvoice.Enabled = False
                        btnAddDO.Enabled = False
                        btnAddPayment.Enabled = False
                        gvPO.Columns(0).Visible = False
                        gvInvoice.Columns(0).Visible = False
                        gvDO.Columns(0).Visible = False
                        gvPayment.Columns(0).Visible = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            If Session("ApplyNewID").ToString <> "" Then
                hdfApplyNewID.Value = Session("ApplyNewID").ToString
                GetPurchaseDetail(hdfApplyNewID.Value.ToString)
            End If
            Session("ApplyNewID") = ""

            GetCheckIs_InChargeParty()

        Else
            GetPurchaseDetail(hdfApplyNewID.Value.ToString)
            'If Session("ApplyNewID").ToString <> "" Then
            '    hdfApplyNewID.Value = Session("ApplyNewID").ToString
            '    GetPurchaseDetail(hdfApplyNewID.Value.ToString)
            'End If
        End If
    End Sub

    Private Sub GetCheckIs_InChargeParty()
        Dim dsInChargeParty As New DataSet
        dsInChargeParty = clsCF.GetCheckIs_InChargeParty(Session("DA_UserID").ToString, hdfStationID.Value.ToString)

        If dsInChargeParty.Tables.Count > 0 Then
            If dsInChargeParty.Tables(0).Rows(0)("Issue_PO").ToString <> 0 Then
                btnAddPO.Enabled = True
                gvPO.Columns(0).Visible = True
            Else
                btnAddPO.Enabled = False
                gvPO.Columns(0).Visible = False
            End If

            If dsInChargeParty.Tables(0).Rows(0)("Issue_Payment").ToString <> 0 Then
                btnAddInvoice.Enabled = True
                btnAddPayment.Enabled = True
                gvInvoice.Columns(0).Visible = True
            Else
                btnAddInvoice.Enabled = False
                btnAddPayment.Enabled = False
                gvInvoice.Columns(0).Visible = False
            End If
        End If
    End Sub

    Private Sub GetPurchaseDetail(ByVal strApplyNewID As String)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetPurchaseDetail")

                With MyCommand
                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = strApplyNewID
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "PurchaseDetail"


                If MyData.Tables(0).Rows.Count > 0 Then
                    lblPurchaseNo.Text = MyData.Tables(0).Rows(0)("ApplyNewID").ToString
                    hdfApplyNewID.Value = MyData.Tables(0).Rows(0)("ApplyNewID").ToString
                    lblStation.Text = MyData.Tables(0).Rows(0)("StationCode").ToString
                    hdfStationID.Value = MyData.Tables(0).Rows(0)("StationID").ToString
                End If

                gvItem.DataSource = MyData
                gvItem.DataMember = MyData.Tables(1).TableName
                gvItem.DataBind()

                gvPO.DataSource = MyData
                gvPO.DataMember = MyData.Tables(2).TableName
                gvPO.DataBind()

                gvInvoice.DataSource = MyData
                gvInvoice.DataMember = MyData.Tables(3).TableName
                gvInvoice.DataBind()

                gvDO.DataSource = MyData
                gvDO.DataMember = MyData.Tables(4).TableName
                gvDO.DataBind()

                gvVendorPayee.DataSource = MyData
                gvVendorPayee.DataMember = MyData.Tables(5).TableName
                gvVendorPayee.DataBind()
                If MyData.Tables(5).Rows.Count > 1 Then
                    gvVendorPayee.BorderStyle = BorderStyle.NotSet
                Else
                    gvVendorPayee.BorderStyle = BorderStyle.None
                End If

                gvQuoReceipt.DataSource = MyData
                gvQuoReceipt.DataMember = MyData.Tables(6).TableName
                gvQuoReceipt.DataBind()

                gvPayment.DataSource = MyData
                gvPayment.DataMember = MyData.Tables(7).TableName
                gvPayment.DataBind()

                lblMsg.Text = ""

            Catch ex As Exception
                Me.lblMsg.Text = "GetPurchaseDetail: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnAddPO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPO.Click
        Dim strScript As String

        Session("ApplyNewID") = ""
        Session("POID") = ""
        Session("ApplyNewStationID") = ""
        Session("ApplyNewID") = hdfApplyNewID.Value.ToString
        Session("ApplyNewStationID") = hdfStationID.Value.ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('frmPurchaseOrder.aspx?ParentForm=frmPurchaseDetail','AddPO','height=750, width=900,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub btnAddInvoice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddInvoice.Click
        Dim strScript As String

        Session("InvoiceID") = ""
        Session("ApplyNewID") = ""
        Session("ApplyNewStationID") = ""
        Session("ApplyNewID") = hdfApplyNewID.Value.ToString
        Session("ApplyNewStationID") = hdfStationID.Value.ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('frmInvoice.aspx?ParentForm=frmPurchaseDetail','AddInvoice','height=750, width=900,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub btnAddDO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDO.Click
        Dim strScript As String

        Session("DOID") = ""
        Session("ApplyNewID") = ""
        Session("ApplyNewStationID") = ""
        Session("ApplyNewID") = hdfApplyNewID.Value.ToString
        Session("ApplyNewStationID") = hdfStationID.Value.ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('frmDeliveryOrder.aspx?ParentForm=frmPurchaseDetail','AddDO','height=750, width=900,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub btnAddPayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPayment.Click
        Dim strScript As String

        Session("ApplyNewID") = ""
        Session("ApplyNewStationID") = ""
        Session("ApplyNewID") = hdfApplyNewID.Value.ToString
        Session("ApplyNewStationID") = hdfStationID.Value.ToString
        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('frmPayment.aspx?ParentForm=frmPurchaseDetail','AddPayment','height=750, width=900,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    'Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
    '    Dim lbtnItem As New LinkButton
    '    lbtnItem = gvItem.SelectedRow.FindControl("lbtnItem")

    '    Session("AssetID") = ""
    '    Session("AssetID") = lbtnItem.CommandArgument.ToString
    '    Response.Redirect("~/Asset/frmAssetDetail.aspx")
    'End Sub

    Protected Sub gvPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPO.SelectedIndexChanged
        Dim strScript As String

        Session("ApplyNewID") = ""
        Session("ApplyNewID") = hdfApplyNewID.Value.ToString
        Session("POID") = ""
        Session("POID") = gvPO.SelectedValue.ToString
        Session("ApplyNewStationID") = hdfStationID.Value.ToString


        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('frmPurchaseOrder.aspx?ParentForm=frmPurchaseDetail','AddPO','height=750, width=900,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub gvInvoice_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvInvoice.SelectedIndexChanged
        Dim strScript As String

        Session("ApplyNewID") = ""
        Session("ApplyNewID") = hdfApplyNewID.Value.ToString
        Session("InvoiceID") = ""
        Session("InvoiceID") = gvInvoice.SelectedValue.ToString
        Session("ApplyNewStationID") = hdfStationID.Value.ToString


        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('frmInvoice.aspx?ParentForm=frmPurchaseDetail','AddInvoice','height=750, width=900,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub gvDO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDO.SelectedIndexChanged
        Dim strScript As String

        Session("ApplyNewID") = ""
        Session("ApplyNewID") = hdfApplyNewID.Value.ToString
        Session("DOID") = ""
        Session("DOID") = gvDO.SelectedValue.ToString
        Session("ApplyNewStationID") = hdfStationID.Value.ToString


        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('frmDeliveryOrder.aspx?ParentForm=frmPurchaseDetail','AddDO','height=750, width=900,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub gvPayment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPayment.SelectedIndexChanged
        Session("PaymentID") = ""
        Session("PaymentID") = gvPayment.SelectedValue.ToString

        Dim strScript As String
        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('frmPaymentDetail.aspx?ParentForm=frmPurchaseDetail','PaymentDetail','height=750, width=900,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

   
    Protected Sub gvPO_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvPO.RowCommand
        Dim strPOID As String
        strPOID = e.CommandArgument.ToString()

        If e.CommandName = "Select" Then

        ElseIf e.CommandName = "View" Then
            Dim hdfPath As New HiddenField
            hdfPath = gvPO.SelectedRow.FindControl("hdfPath")

            Dim lbtnDocPic As New LinkButton
            lbtnDocPic = gvPO.SelectedRow.FindControl("lbtnDocPic")
            If lbtnDocPic.Text.ToString <> "" Then

            End If
        End If
    End Sub

    Protected Sub gvPO_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvPO.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim strPOID As String = 0
                Dim lbtnDelete As New LinkButton

                lblMsg.Text = ""

                lbtnDelete = gvPO.Rows(e.RowIndex).FindControl("lbtnDelete")
                strPOID = lbtnDelete.CommandArgument.ToString

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeletePurchaseOrder", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@POID", SqlDbType.VarChar, 20)
                    .Parameters("@POID").Value = strPOID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else

                    GetPurchaseDetail(hdfApplyNewID.Value.ToString)
                    'lblMsg.Text = "Delete Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvPO_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub gvInvoice_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvInvoice.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim strInvoiceID As String = 0
                Dim lbtnDelete As New LinkButton

                lblMsg.Text = ""

                lbtnDelete = gvInvoice.Rows(e.RowIndex).FindControl("lbtnDelete")
                strInvoiceID = lbtnDelete.CommandArgument.ToString

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteInvoice", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@InvoiceID", SqlDbType.VarChar, 20)
                    .Parameters("@InvoiceID").Value = strInvoiceID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else

                    GetPurchaseDetail(hdfApplyNewID.Value.ToString)
                    'lblMsg.Text = "Delete Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvInvoice_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub gvDO_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDO.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim strDOID As String = 0
                Dim lbtnDelete As New LinkButton

                lblMsg.Text = ""

                lbtnDelete = gvDO.Rows(e.RowIndex).FindControl("lbtnDelete")
                strDOID = lbtnDelete.CommandArgument.ToString

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteDeliveryOrder", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@DOID", SqlDbType.VarChar, 20)
                    .Parameters("@DOID").Value = strDOID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else

                    GetPurchaseDetail(hdfApplyNewID.Value.ToString)
                    'lblMsg.Text = "Delete Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvDO_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub gvPayment_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvPayment.RowDeleting

    End Sub

    Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand
        Dim strID As String
        strID = e.CommandArgument.ToString()

        If e.CommandName = "SelectItem" Then
            Session("AssetID") = ""
            Session("AssetID") = strID
            Response.Redirect("~/Asset/frmAssetDetail.aspx")

        ElseIf e.CommandName = "CancelItem" Then

            Session("CancelItem_ApplyNewID") = ""
            Session("CancelItem_ApplyNewID") = hdfApplyNewID.Value.ToString

            Session("CancelItem_AssetID") = ""
            Session("CancelItem_AssetID") = Mid(strID, 1, 14)

            Session("CancelItem_QRID") = ""
            Session("CancelItem_QRID") = Mid(strID, 16, 14)

            Session("CancelItem_QRItemID") = ""
            Session("CancelItem_QRItemID") = Mid(strID, 31, Len(strID) - 30)


            Dim strScript As String = ""
            strScript = "<script language=javascript>"
            strScript += "theChild = window.open('frmCancelPurchaseItem.aspx?ParentForm=frmPurchaseDetail','CancelItem','height=750, width=900,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
            strScript += "</script>"
            Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
        End If
    End Sub
End Class
