<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmPaymentList.aspx.vb" Inherits="Purchase_frmPaymentList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Payment List</title>
</head>
<body>
    <form id="frmPaymentList" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Payment List"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <asp:Button ID="btnAddPayment" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Text="Add New Payment" /></td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    &nbsp;</td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <table>
                        <tr>
                            <td style="width: 50px">
                                <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Station"></asp:Label></td>
                            <td style="width: 5px">
                                :</td>
                            <td style="width: 100px">
                                <asp:DropDownList ID="ddlStation" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                            <td style="width: 30px">
                            </td>
                            <td style="width: 120px">
                                <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Payment Voucher No"></asp:Label></td>
                            <td style="width: 5px">
                                :</td>
                            <td style="width: 150px">
                                <asp:TextBox ID="txtPaymentVoucherNo" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    Width="150px"></asp:TextBox></td>
                            <td style="width: 30px">
                            </td>
                            <td style="width: 50px">
                                <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Payee"></asp:Label></td>
                            <td style="width: 5px">
                                :</td>
                            <td>
                                <asp:TextBox ID="txtPayee" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                    ReadOnly="True" Width="200px"></asp:TextBox><asp:Button ID="btnVendor" runat="server"
                                        Font-Names="Verdana" Font-Size="X-Small" Text="...." />
                                <asp:HiddenField ID="hdfPayee" runat="server" />
                                <asp:HiddenField ID="hdfPayeeType" runat="server" />
                            </td>
                            <td style="width: 10px">
                            </td>
                            <td>
                                <asp:Button ID="btnQuery" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Query"
                                    Width="65px" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <asp:GridView ID="gvPaymentList" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        Font-Names="Verdana" Font-Size="X-Small" ForeColor="#333333" GridLines="None" DataKeyNames="PaymentID">
                        <RowStyle BackColor="#EFF3FB" />
                        <Columns>
                            <asp:TemplateField HeaderText="Payment No">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnPaymentNo" runat="server" CommandArgument='<%# Bind("PaymentID") %>'
                                        Text='<%# Bind("PaymentID") %>' CommandName="Select"></asp:LinkButton>
                                    <asp:HiddenField ID="hdfStationID" runat="server" Value='<%# Bind("StationID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="PaymentVoucherNo" HeaderText="Payment Vourcher No" />
                            <asp:BoundField DataField="Amount" HeaderText="Amount" />
                            <asp:BoundField DataField="PaymentType" HeaderText="Payment Type" />
                            <asp:BoundField DataField="PaymentDate" HeaderText="Payment Date" />
                            <asp:BoundField DataField="PayeeName" HeaderText="Payee" />
                            <asp:BoundField DataField="Status" HeaderText="Status" />
                        </Columns>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
