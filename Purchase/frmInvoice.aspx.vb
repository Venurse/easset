Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Purchase_frmInvoice
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Public Shared hifSupDocInv As ArrayList = New ArrayList()
    Dim strFileExtension As String = ConfigurationSettings.AppSettings("DocExtension")
    Dim intFileSizeLimitKB As Integer = ConfigurationSettings.AppSettings("intFileSizeLimitKB")
    Dim intFileSizeLimit As Integer = ConfigurationSettings.AppSettings("intFileSizeLimit")

    Dim dsPO As New DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack() = False Then
            hdfApplyNewID.Value = ""

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            'gvFile.Columns(0).Visible = False
            btnSave.Enabled = False
            gvInvoice.Columns(0).Visible = False


            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "P0006")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Invoice Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnSave.Enabled = True
                        gvInvoice.Columns(0).Visible = True
                    Else
                        Session("blnWrite") = False
                        btnSave.Enabled = False
                        gvInvoice.Columns(0).Visible = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If


            If Session("ApplyNewID").ToString <> "" Then
                hdfApplyNewID.Value = Session("ApplyNewID").ToString
                txtPurchaseNo.Text = hdfApplyNewID.Value.ToString
                txtPurchaseNo.Enabled = False
            End If

            If Session("InvoiceID").ToString <> "" Then
                hdfInvoiceID.Value = Session("InvoiceID").ToString
            Else
                hdfInvoiceID.Value = ""
            End If

            BindDropDownList()
            If Session("ApplyNewStationID").ToString <> "" Then
                ddlStation.SelectedValue = Session("ApplyNewStationID").ToString
                ddlStation.Enabled = False
            Else
                ddlStation.Enabled = True
            End If

            Session("ApplyNewID") = ""
            Session("InvoiceID") = ""
            Session("ApplyNewStationID") = ""

            GetInvoice(hdfApplyNewID.Value.ToString, hdfInvoiceID.Value.ToString)

            GetCheckIs_InChargeParty()

            Me.btnSave.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnSave, "", "btnClear", "btnClose", "", "", ""))
            Me.btnClear.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnClear, "", "btnSave", "btnClose", "", "", ""))

        Else

        End If
    End Sub

    Private Sub GetCheckIs_InChargeParty()
        Dim dsInChargeParty As New DataSet
        dsInChargeParty = clsCF.GetCheckIs_InChargeParty(Session("DA_UserID").ToString, ddlStation.SelectedValue.ToString)

        If dsInChargeParty.Tables.Count > 0 Then
            If dsInChargeParty.Tables(0).Rows(0)("Issue_Payment").ToString <> 0 Then
                btnSave.Enabled = True
                gvInvoice.Columns(0).Visible = True
            Else
                btnSave.Enabled = False
                gvInvoice.Columns(0).Visible = False
            End If
        End If
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.NVarChar, 500)
                    .Parameters("@ReferenceType").Value = "[AccessStation],[ImageDocPath_Temp],[ImageDocPath_Temp_Open]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)


                'If MyData.Tables(0).Rows(0)("AccessStation").ToString <> "" Then
                '    dsStation = DIMERCO.SDK.Utilities.LSDK.getStationDataByWithList(MyData.Tables(0).Rows(0)("AccessStation").ToString)
                'Else
                '    dsStation = DIMERCO.SDK.Utilities.ReSM.GetStationList()
                'End If
                'Dim dtView As DataView = dsStation.Tables(0).DefaultView
                'dtView.Sort = "StationCode ASC"

                'ddlStation.DataSource = dtView
                'ddlStation.DataValueField = dtView.Table.Columns("StationID").ToString
                'ddlStation.DataTextField = dtView.Table.Columns("StationCode").ToString
                'ddlStation.DataBind()

                If Session("DA_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("DA_StationID").ToString
                End If

                If MyData.Tables(1).Rows.Count > 0 Then
                    hdfImageDocPath_Temp.Value = MyData.Tables(1).Rows(0)("ImageDocPath_Temp").ToString
                Else
                    hdfImageDocPath_Temp.Value = ""
                End If

                If MyData.Tables(2).Rows.Count > 0 Then
                    hdfImageDocPath_Temp_Open.Value = MyData.Tables(2).Rows(0)("ImageDocPath_Temp").ToString
                Else
                    hdfImageDocPath_Temp_Open.Value = ""
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub GetInvoice(ByVal strApplyNewID As String, ByVal strInvoiceID As String)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetInvoice")

                With MyCommand

                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = strApplyNewID
                    .Parameters.Add("@InvoiceID", SqlDbType.VarChar, 20)
                    .Parameters("@InvoiceID").Value = strInvoiceID
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "Invoice"

                ddlVendorPayee.DataSource = MyData.Tables(3)
                ddlVendorPayee.DataValueField = MyData.Tables(3).Columns("VendorPayeeID").ToString
                ddlVendorPayee.DataTextField = MyData.Tables(3).Columns("VendorPayeeName").ToString
                ddlVendorPayee.DataBind()

                ddlVendorPayee.Enabled = True
                btnVendor.Enabled = True
                txtInvoiceNo.Enabled = True
                cldInvoiceDate.Enabled = True
                txtAmount.Enabled = True

                dsPO.Tables.Add(MyData.Tables(4).Copy)
                dsPO.Tables(0).TableName = "PO"
                ViewState("dsPO") = dsPO

                If strInvoiceID = "" Then
                    gvInvoice.DataSource = MyData
                    gvInvoice.DataMember = MyData.Tables(0).TableName
                    gvInvoice.DataBind()

                    If ddlVendorPayee.Items.Count = 2 Then
                        ddlVendorPayee.SelectedIndex = 1
                    End If

                    fuInvoice.Enabled = True
                    btnAttach.Enabled = True

                    BindgvPO(ddlVendorPayee.SelectedValue.ToString, hdfInvoiceID.Value.ToString)

                    'If gvPO.Rows.Count = 0 Then
                    '    ddlVendorPayee.Enabled = False
                    '    btnVendor.Enabled = False
                    '    Dim I As Integer = 0
                    '    For I = 0 To gvPO.Rows.Count - 1
                    '        Dim chkSelect As New CheckBox
                    '        chkSelect = gvPO.Rows(I).FindControl("chkSelect")
                    '        chkSelect.Enabled = False
                    '    Next

                    '    txtInvoiceNo.Enabled = False
                    '    cldInvoiceDate.Enabled = False
                    '    txtAmount.Enabled = False
                    'End If
                Else
                    hdfInvoiceID.Value = MyData.Tables(0).Rows(0)("InvoiceID").ToString
                    txtInvoiceNo.Text = MyData.Tables(0).Rows(0)("InvoiceNo").ToString
                    cldInvoiceDate.SelectedDate = Format(Convert.ToDateTime(MyData.Tables(0).Rows(0)("InvoiceDate").ToString), "dd MMM yyyy")
                    txtAmount.Text = MyData.Tables(0).Rows(0)("InvoiceAmount").ToString
                    ddlVendorPayee.SelectedValue = MyData.Tables(0).Rows(0)("VendorID").ToString

                    ddlStation.SelectedValue = MyData.Tables(0).Rows(0)("StationID").ToString
                    ddlStation.Enabled = False


                    If MyData.Tables(2).Rows(0)("FileName").ToString = "" Then
                        Dim tbInvoice As DataTable = New DataTable("Invoice")
                        tbInvoice.Columns.Add("Filename")
                        tbInvoice.Columns.Add("DocPickLink")

                        Dim dsInvoice As DataSet = New DataSet("Invoice")
                        dsInvoice.Tables.Add(tbInvoice)

                        gvFile.Visible = True
                        gvFile.DataSource = dsInvoice
                        gvFile.DataMember = dsInvoice.Tables(0).TableName
                        gvFile.DataBind()

                    Else
                        gvFile.Visible = True
                        gvFile.DataSource = MyData
                        gvFile.DataMember = MyData.Tables(2).TableName
                        gvFile.DataBind()
                    End If

                    If gvFile.Rows.Count > 0 Then
                        fuInvoice.Enabled = False
                        btnAttach.Enabled = False
                    Else
                        fuInvoice.Enabled = True
                        btnAttach.Enabled = True
                    End If

                    BindgvPO(ddlVendorPayee.SelectedValue.ToString, hdfInvoiceID.Value.ToString)

                    If MyData.Tables(0).Rows.Count > 0 Then
                        If MyData.Tables(0).Rows(0)("AllowEditDelete") = "0" Then
                            ddlVendorPayee.Enabled = False
                            btnVendor.Enabled = False
                            Dim I As Integer = 0
                            For I = 0 To gvPO.Rows.Count - 1
                                Dim chkSelect As New CheckBox
                                chkSelect = gvPO.Rows(I).FindControl("chkSelect")
                                chkSelect.Enabled = False
                            Next

                            txtInvoiceNo.Enabled = False
                            cldInvoiceDate.Enabled = False
                            txtAmount.Enabled = False
                        End If
                    End If
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "GetInvoice: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub BindgvPO(ByVal strVendorPayee As String, ByVal strInvoiceID As String)
        dsPO = ViewState("dsPO")

        Dim objDataSet2 As New DataSet
        objDataSet2.Merge(dsPO.Tables(0).Select("VendorPayeeID='" & strVendorPayee & "'"))

        If objDataSet2.Tables.Count = 0 Then
            objDataSet2 = dsPO.Clone()
        End If


        Dim objDataSet3 As New DataSet
        If strInvoiceID <> "" Then
            objDataSet3.Merge(objDataSet2.Tables(0).Select("InvoiceID='" & strInvoiceID & "' OR InvoiceID=''"))
            If objDataSet3.Tables.Count = 0 Then
                objDataSet3 = dsPO.Clone()
            End If
            gvPO.DataSource = objDataSet3
            gvPO.DataMember = objDataSet3.Tables(0).TableName
            gvPO.DataBind()
        Else
            objDataSet3.Merge(objDataSet2.Tables(0).Select("InvoiceID='" & strInvoiceID & "'"))
            If objDataSet3.Tables.Count = 0 Then
                objDataSet3 = dsPO.Clone()
            End If
            gvPO.DataSource = objDataSet3
            gvPO.DataMember = objDataSet3.Tables(0).TableName
            gvPO.DataBind()
        End If


        

        'objDataSet2 = 
        'dsPO.Tables(0).DefaultView.RowFilter = "VendorPayeeID = " & strVendorPayee
        'dSet.Tables[0].DefaultView.RowFilter = "Frequency like '%30%')";


        'Dim r As DataRow
        'For Each r In dsPO.Tables(0).Select("VendorPayeeID=" & strVendorPayee)
        '    'strCalBy = r.Item("nvchCalBy").ToString
        '    'strDefaultDay = r.Item("iDefaultDay").ToString
        '    'hdDefaultDay.Value = strDefaultDay
        '    'hdDefaultHour.Value = r.Item("iDefaultHour").ToString
        '    'hdDeputyApproval.Value = r.Item("tiDeputyApproval").ToString
        '    'hdNeedDeputy.Value = r.Item("tiNeedDeputy").ToString
        '    'hdfALWithoutDeputy.Value = r.Item("tiApplyLeave_WithoutDeputy").ToString
        '    'hdfApplyLimit.Value = r.Item("iApplyLimit").ToString
        'Next
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strInvoiceDate As String = ""
                Dim strFilePath As String = ""
                Dim strFileName As String = ""
                Dim strPOID As String = ""
                Dim strMailID As String = ""

                Dim strMsg As String = ""

                lblMsg.Text = ""

                If txtPurchaseNo.Text.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Purchase No')</script>")
                    txtPurchaseNo.Focus()
                    Exit Sub
                End If
                If txtInvoiceNo.Text.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Invoice No')</script>")
                    txtInvoiceNo.Focus()
                    Exit Sub
                End If
                If txtAmount.Text.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Amount')</script>")
                    txtAmount.Focus()
                    Exit Sub
                End If

                If CDec(txtAmount.Text.ToString) <= 0 Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Amount')</script>")
                    txtAmount.Focus()
                    Exit Sub
                End If

                Dim i As Integer
                Dim blnPO As Boolean = False
                For i = 0 To gvPO.Rows.Count - 1
                    Dim chkSelect As New CheckBox
                    Dim hfPOID As New HiddenField
                    hfPOID = gvPO.Rows(i).FindControl("hfPOID")
                    chkSelect = gvPO.Rows(i).FindControl("chkSelect")
                    If chkSelect.Checked = True Then
                        blnPO = True
                        If strPOID <> "" Then
                            strPOID = strPOID + "<" + hfPOID.Value.ToString
                        Else
                            strPOID = hfPOID.Value.ToString
                        End If

                        'Exit For
                    End If
                Next

                'If gvPO.Rows.Count > 0 Then
                If blnPO = False Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Purchase Order')</script>")
                    'lblMsg.Text = "Please select Purchase Order."
                    Exit Sub
                End If
                'End If

                strInvoiceDate = Format(cldInvoiceDate.SelectedDate, "dd MMM yyyy").ToString

                If strInvoiceDate.Contains("01 Jan 0001") = True Then
                    strInvoiceDate = ""
                End If

                'GET ATTACHED FILE NAME FROM GV
                For i = 0 To gvFile.Rows.Count - 1
                    Dim lbtnRemove As New LinkButton
                    lbtnRemove = gvFile.Rows(i).FindControl("lbtnRemove")
                    strFileName = lbtnRemove.CommandArgument.ToString
                Next

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveInvoice", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ApplyNewID", SqlDbType.NVarChar, 50)
                    .Parameters("@ApplyNewID").Value = hdfApplyNewID.Value.ToString
                    .Parameters.Add("@InvoiceID", SqlDbType.VarChar, 20)
                    If hdfInvoiceID.Value.ToString = "" Then
                        .Parameters("@InvoiceID").Value = ""
                    Else
                        .Parameters("@InvoiceID").Value = hdfInvoiceID.Value.ToString
                    End If

                    .Parameters.Add("@InvoiceNo", SqlDbType.NVarChar, 50)
                    .Parameters("@InvoiceNo").Value = txtInvoiceNo.Text.ToString
                    .Parameters.Add("@VendorID", SqlDbType.NVarChar, 50)
                    .Parameters("@VendorID").Value = ddlVendorPayee.SelectedValue.ToString
                    .Parameters.Add("@POID", SqlDbType.NVarChar)
                    .Parameters("@POID").Value = strPOID
                    .Parameters.Add("@TotalAmount", SqlDbType.Decimal, 18, 4)
                    .Parameters("@TotalAmount").Value = CDec(txtAmount.Text.ToString)
                    .Parameters.Add("@InvoiceDate", SqlDbType.NVarChar, 20)
                    .Parameters("@InvoiceDate").Value = strInvoiceDate
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@FileName", SqlDbType.NVarChar, 500)
                    .Parameters("@FileName").Value = strFileName
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()


                    .Parameters.Add("@FilePath", SqlDbType.NVarChar, 500)
                    .Parameters("@FilePath").Direction = ParameterDirection.Output
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .Parameters.Add("@biMailID", SqlDbType.NVarChar, 500)
                    .Parameters("@biMailID").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                    strFilePath = .Parameters("@FilePath").Value.ToString()
                    strMailID = .Parameters("@biMailID").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else


                    If strFilePath <> "" Then

                        Dim strAlbumPath_Temp As String
                        strAlbumPath_Temp = hdfImageDocPath_Temp.Value.ToString + "/" + Session("DA_UserID").ToString

                        If FileIO.FileSystem.DirectoryExists(strFilePath) = False Then
                            FileIO.FileSystem.CreateDirectory(strFilePath)
                        End If

                        If FileIO.FileSystem.FileExists(strAlbumPath_Temp + "/" + strFileName) = True Then
                            If FileIO.FileSystem.FileExists(strFilePath + "/" + strFileName) = True Then
                                FileIO.FileSystem.DeleteFile(strFilePath + "/" + strFileName)
                            End If

                            FileIO.FileSystem.CopyFile(strAlbumPath_Temp + "/" + strFileName, strFilePath + "/" + strFileName, True)
                        End If
                    End If

                    If strMailID <> "" Then
                        Dim strResult As String = ""
                        Dim intMailID As Integer
                        Dim strMessage As String = ""
                        Dim MailID As String() = strMailID.Split(New Char() {","c})

                        For Each intMailID In MailID
                            strResult = clsCF.SendMail(intMailID)
                            If strResult = "" Then
                                strMessage = "Post eFMS Notification Send Successful"
                                clsCF.UpdateMailQueue(intMailID, 1, strMessage, hdfCurrentDateTime.Value.ToString)
                            Else
                                strMessage = "Post eFMS Notification Send Fail - " & strResult
                                clsCF.UpdateMailQueue(intMailID, 2, strMessage, hdfCurrentDateTime.Value.ToString)
                            End If
                        Next
                    End If

                    btnClear_Click(sender, e)
                    GetInvoice(txtPurchaseNo.Text.ToString, "")
                    'lblMsg.Text = "Save Successful."
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        hdfInvoiceID.Value = ""
        If Session("ApplyNewStationID").ToString <> "" Then
            ddlStation.SelectedValue = Session("ApplyNewStationID").ToString
        Else
            If Session("DA_StationID").ToString <> "" Then
                ddlStation.SelectedValue = Session("DA_StationID").ToString
            End If
        End If
        txtInvoiceNo.Text = ""
        cldInvoiceDate.Clear()
        txtAmount.Text = ""
        gvFile.Dispose()
        gvFile.Visible = False
        fuInvoice.Dispose()
        fuInvoice.Enabled = True
        btnAttach.Enabled = True
        GetInvoice(txtPurchaseNo.Text.ToString, "")
    End Sub

    Protected Sub gvInvoice_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvInvoice.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim strInvoiceID As String = 0
                Dim lbtnRemove As New LinkButton

                lblMsg.Text = ""

                lbtnRemove = gvInvoice.Rows(e.RowIndex).FindControl("lbtnRemove")
                strInvoiceID = lbtnRemove.CommandArgument.ToString

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteInvoice", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@InvoiceID", SqlDbType.VarChar, 20)
                    .Parameters("@InvoiceID").Value = strInvoiceID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else

                    btnClear_Click(sender, e)
                    GetInvoice(txtPurchaseNo.Text.ToString, "")
                    'lblMsg.Text = "Delete Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvInvoice_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Dim ParentFormID As String = ""
        ParentFormID = Request.QueryString("ParentForm").ToString

        'Session("ApplyNewID") = ""
        Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener =self;window.close();</script>")
    End Sub

    Protected Sub btnAttach_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAttach.Click
        Dim blnContains As Boolean = False

        If fuInvoice.FileName <> "" Then
            If (Regex.IsMatch(fuInvoice.FileName, strFileExtension, RegexOptions.IgnoreCase)) = False Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid file format')</script>")
                'lblMsg.Text = "Invalid file format."
                Exit Sub
            End If

            ' Get the size in bytes of the file to upload.
            Dim fileSize As Integer = fuInvoice.PostedFile.ContentLength

            ' Allow only files less than 2,100,000 bytes (approximately 2 MB) to be uploaded.
            'If (fileSize < 2100000) Then
            If (fileSize < intFileSizeLimitKB) Then

                Dim strAlbumPath As String
                'If hdfPOID.Value.ToString = "" Then
                strAlbumPath = hdfImageDocPath_Temp.Value.ToString + "/" + Session("DA_UserID").ToString
                'Else
                '    strAlbumPath = hdfImageDocPath_Temp.Value.ToString + "/" + hdfPOID.Value.ToString
                'End If


                Dim strAlbumPath_Open As String
                strAlbumPath_Open = hdfImageDocPath_Temp_Open.Value.ToString + "/" + Session("DA_UserID").ToString



                hifSupDocInv.Add(fuInvoice)
                Dim tbQuoRecpt As DataTable = New DataTable("Invoice")
                tbQuoRecpt.Columns.Add("Filename")
                tbQuoRecpt.Columns.Add("DocPicLink")
                tbQuoRecpt.Rows.Add(clsCF.RenameFileName(fuInvoice.FileName.ToString), "<a href=""../frmOpenFile.aspx?FilePath=" & strAlbumPath_Open & "/" & clsCF.RenameFileName(fuInvoice.FileName.ToString) & """ target=""_blank"">" & clsCF.RenameFileName(fuInvoice.FileName.ToString) & "</a>")

                Dim dsInvoice As DataSet = New DataSet("Invoice")
                dsInvoice.Tables.Add(tbQuoRecpt)

                gvFile.Visible = True
                gvFile.DataSource = dsInvoice
                gvFile.DataMember = dsInvoice.Tables(0).TableName
                gvFile.DataBind()

                If gvFile.Rows.Count > 0 Then
                    fuInvoice.Enabled = False
                    btnAttach.Enabled = False
                Else
                    fuInvoice.Enabled = True
                    btnAttach.Enabled = True
                End If


                If FileIO.FileSystem.DirectoryExists(strAlbumPath) = True Then
                    FileIO.FileSystem.DeleteDirectory(strAlbumPath, FileIO.DeleteDirectoryOption.DeleteAllContents)
                End If

                FileIO.FileSystem.CreateDirectory(strAlbumPath)

                If FileIO.FileSystem.FileExists(strAlbumPath + "/" + clsCF.RenameFileName(fuInvoice.FileName.ToString)) = False Then
                    fuInvoice.PostedFile.SaveAs(strAlbumPath + "/" + clsCF.RenameFileName(fuInvoice.FileName.ToString))
                End If
            Else
                'lblMsg.Text = "Your file was not uploaded because it exceeds the " & intFileSizeLimit & " MB size limit."
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Your file was not uploaded because it exceeds the " & intFileSizeLimit & " MB size limit')</script>")
            End If
        End If
    End Sub

    Protected Sub gvFile_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvFile.RowDeleting
        fuInvoice.Dispose()

        hifSupDocInv.Clear()
        Dim tbInvoice As DataTable = New DataTable("Invoice")
        tbInvoice.Columns.Add("Filename")
        tbInvoice.Columns.Add("DocPickLink")

        Dim dsInvoice As DataSet = New DataSet("Invoice")
        dsInvoice.Tables.Add(tbInvoice)

        gvFile.Visible = True
        gvFile.DataSource = dsInvoice
        gvFile.DataMember = dsInvoice.Tables(0).TableName
        gvFile.DataBind()

        fuInvoice.Enabled = True
        btnAttach.Enabled = True
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

    End Sub

    Protected Sub btnVendor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVendor.Click
        Dim strScript As String

        Session("DA_PayeeID") = Nothing
        Session("DA_PayeeCode") = Nothing
        Session("DA_PayeeName") = Nothing

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmPayee.aspx?ParentForm=frmQuotationReceipt','Vendor_Payee','height=750, width=700,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub ddlVendorPayee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVendorPayee.SelectedIndexChanged
        BindgvPO(ddlVendorPayee.SelectedValue.ToString, hdfInvoiceID.Value.ToString)
    End Sub

    Protected Sub gvInvoice_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvInvoice.SelectedIndexChanged
        Dim lbtnInvoiceNo As New LinkButton
        Dim strInvoiceID As String = ""
        lbtnInvoiceNo = gvInvoice.SelectedRow.FindControl("lbtnInvoiceNo")
        strInvoiceID = lbtnInvoiceNo.CommandArgument.ToString

        GetInvoice(txtPurchaseNo.Text.ToString, strInvoiceID)
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SumAmount()
    End Sub

    Private Sub SumAmount()
        Dim i As Int32
        Dim decTotalAmount As Decimal = 0

        For i = 0 To gvPO.Rows.Count - 1


            Dim chkSelect As CheckBox

            chkSelect = CType(gvPO.Rows(i).FindControl("chkSelect"), CheckBox)

            Dim lblAmount As Label
            lblAmount = CType(gvPO.Rows(i).FindControl("lblAmount"), Label)
            If chkSelect.Checked = True Then
                decTotalAmount = decTotalAmount + CDec(lblAmount.Text.ToString)
            End If

        Next
        txtAmount.Text = decTotalAmount

    End Sub
End Class
