Imports System.Data
Imports System.Data.SqlClient

Partial Class frmPayee
    Inherits System.Web.UI.Page

    Dim dsStation As New DataSet
    Dim dsApplyTo As New DataSet

    Dim clsCF As New clsComFunction
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

       
        'Dim strApplyToList As String
        'lblMsg.Text = ""

        If IsPostBack() = False Then

            BindDropDownList()

            '    Session("DA_New_USERLIST") = ""

            '    If Session("DA_USERLIST") = "" Then
            '        strApplyToList = ""
            '        Session("DA_New_USERLIST") = ""
            '    Else
            '        strApplyToList = Session("DA_USERLIST").ToString
            '        Session("DA_New_USERLIST") = Session("DA_USERLIST")
            '    End If
            '    GetApplyToData(strApplyToList)

        End If
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[AccessStation]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)

                'If MyData.Tables(0).Rows(0)("AccessStation").ToString <> "" Then
                '    dsStation = DIMERCO.SDK.Utilities.LSDK.getStationDataByWithList(MyData.Tables(0).Rows(0)("AccessStation").ToString)
                'Else
                '    dsStation = DIMERCO.SDK.Utilities.ReSM.GetStationList()
                'End If
                'Dim dtView As DataView = dsStation.Tables(0).DefaultView
                'dtView.Sort = "StationCode ASC"

                'ddlStation.DataSource = dtView
                'ddlStation.DataValueField = dtView.Table.Columns("StationID").ToString
                'ddlStation.DataTextField = dtView.Table.Columns("StationCode").ToString
                'ddlStation.DataBind()

                If Session("DA_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("DA_StationID").ToString
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub GetVendorPayee()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetVendorPayee")

                With MyCommand
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@Type", SqlDbType.NVarChar, 50)
                    If ddlType.SelectedValue.ToString = "Select One" Then
                        .Parameters("@Type").Value = ""
                    Else
                        .Parameters("@Type").Value = ddlType.SelectedValue.ToString
                    End If

                    .Parameters.Add("@StationID", SqlDbType.VarChar)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@Name", SqlDbType.VarChar, 500)
                    .Parameters("@Name").Value = txtName.Text.ToString()
                    .CommandTimeout = 0
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "VendorPayee"


                If MyData.Tables(0).Rows.Count > 0 Then

                    gvVendorPayee.Visible = True
                    gvVendorPayee.DataSource = MyData
                    gvVendorPayee.DataMember = MyData.Tables(0).TableName
                    gvVendorPayee.DataBind()

                    If ddlType.SelectedValue.ToString = "User" Then
                        gvVendorPayee.Columns(1).Visible = False
                    Else
                        gvVendorPayee.Columns(1).Visible = True
                    End If
                Else
                    gvVendorPayee.Visible = False
                End If


            Catch ex As Exception
                Me.lblMsg.Text = "GetVendorPayee: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub GetApplyToData(ByVal strApplyToList As String)

        
       


    End Sub


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        'If ddlType.SelectedValue.ToString = "User" Then
        '    Dim dsEmployeeList As New DataSet

        '    dsEmployeeList = DIMERCO.SDK.Utilities.LSDK.getUserProfilebyFullNameNStation(ddlStation.SelectedValue.ToString, "", DIMERCO.SDK.Utilities.LSDK.EmpStatus.ACTIVE)

        '    Dim dtView As DataView = dsEmployeeList.Tables(0).DefaultView
        '    dtView.Sort = "Fullname ASC"

        '    gvVendorPayee.DataSource = dtView
        '    gvVendorPayee.DataBind()
        'Else

        If ddlType.SelectedValue.ToString = "Select One" Then
            lblMsg.Text = "Please select Type."
            Exit Sub
        End If

        GetVendorPayee()
        'End If

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ddlType.SelectedValue = "Select One"
        txtName.Text = ""
        'Close the window
        Response.Write("<script language=""Javascript"">window.opener =self;window.close();</script>")
        Response.End()
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        'If ddlType.SelectedValue.ToString = "User" Then
        '    Dim dsEmployeeList As New DataSet

        '    dsEmployeeList = DIMERCO.SDK.Utilities.LSDK.getUserProfilebyFullNameNStation(ddlStation.SelectedValue.ToString, "", DIMERCO.SDK.Utilities.LSDK.EmpStatus.ACTIVE)

        '    Dim dtView As DataView = dsEmployeeList.Tables(0).DefaultView
        '    dtView.Sort = "Fullname ASC"

        '    gvVendorPayee.DataSource = dtView
        '    gvVendorPayee.DataBind()
        'Else

        gvVendorPayee.Visible = True

        If ddlType.SelectedValue.ToString = "User" Then
            ddlStation.Enabled = True
        ElseIf ddlType.SelectedValue.ToString = "Vendor" Then
            ddlStation.Enabled = False
        Else
            ddlStation.Enabled = False
            gvVendorPayee.Dispose()
            gvVendorPayee.Visible = False
            Exit Sub
        End If
        GetVendorPayee()
        'End If
    End Sub

    Protected Sub ddlStation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStation.SelectedIndexChanged
        'If ddlType.SelectedValue.ToString = "User" Then
        '    Dim dsEmployeeList As New DataSet

        '    dsEmployeeList = DIMERCO.SDK.Utilities.LSDK.getUserProfilebyFullNameNStation(ddlStation.SelectedValue.ToString, "", DIMERCO.SDK.Utilities.LSDK.EmpStatus.ACTIVE)

        '    Dim dtView As DataView = dsEmployeeList.Tables(0).DefaultView
        '    dtView.Sort = "Fullname ASC"

        '    gvVendorPayee.DataSource = dtView
        '    gvVendorPayee.DataBind()
        'Else
        GetVendorPayee()
        'End If

    End Sub

    Protected Sub gvVendorPayee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvVendorPayee.SelectedIndexChanged
        Dim strPayeeID As String
        Dim lblname As Label
        Dim lblCode As Label

        strPayeeID = gvVendorPayee.SelectedValue.ToString
        lblname = gvVendorPayee.SelectedRow.FindControl("lblname")
        lblCode = gvVendorPayee.SelectedRow.FindControl("lblCode")

        Session("DA_PayeeID") = strPayeeID
        Session("DA_PayeeCode") = lblCode.Text.ToString
        Session("DA_PayeeName") = lblname.Text.ToString()
        Session("DA_PayeeType") = ddlType.SelectedValue.ToString

        Dim ParentFormID As String = ""
        ParentFormID = Request.QueryString("ParentForm").ToString
        Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener =self;window.close();</script>")

    End Sub
End Class
