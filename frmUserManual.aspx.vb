Imports System.IO

Partial Class frmUserManual
    Inherits System.Web.UI.Page
    Dim strUserManual As String = ConfigurationSettings.AppSettings("UserManual")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Response.Redirect(strUserManual)
        Catch ex As Exception
            lblMsg.Text = ex.Message.ToString
        End Try
    End Sub
End Class
