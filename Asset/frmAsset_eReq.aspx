<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAsset_eReq.aspx.vb" Inherits="Asset_frmAsset_eReq" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>eRequisition Approval</title>
    <script type="text/javascript">
         function showDate() 
        {
            var month_names = new Array("Jan", "Feb", "Mar", 
            "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
            "Oct", "Nov", "Dec");

            var d = new Date();
            var curr_day = d.getDay();
            var curr_date = d.getDate();
            var hours = d.getHours();
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            
            var curr_hours;
            var curr_minutes;
            var curr_seconds;

            if(hours<10)
            {
                curr_hours = "0" + hours;
            }
            else
            {
                curr_hours = hours;
            }
            
            if(minutes < 10)
            {
                curr_minutes = "0" + minutes;
            }
             else
            {
                curr_minutes = minutes;
            }
            
           if(seconds < 10)
            {
                curr_seconds = "0" + seconds;
            }
             else
            {
                curr_seconds = seconds;
            }

            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
 
            document.getElementById("hdfCurrentDateTime").value = curr_date + " " +  month_names[curr_month] +  " " + curr_year + " " + curr_hours + ":" + curr_minutes + ":" + curr_seconds
         }
     </script>
</head>
<body>
    <form id="frmAsset_eReq" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%"></td>
                <td style="width: 100px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 180px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 90px">
                </td>
                <td style="width: 5px">
                </td>
                <td></td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="eRequisition Approval"></asp:Label>
                    <asp:HiddenField ID="hdfCurrentDateTime" runat="server" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">
                    <asp:GridView ID="gveReqList" runat="server" AutoGenerateColumns="False" 
                        Font-Names="Verdana" Font-Size="X-Small" DataKeyNames="eRequisitionNo" 
                        EmptyDataText="Record Not Found">
                        <Columns>
                            <asp:TemplateField HeaderText="Select">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnSelect" runat="server" 
                                        CommandArgument='<%# Bind("eRequisitionNo") %>' CommandName="select">Select</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="StationCode" HeaderText="Station" />
                            <asp:BoundField DataField="eRequisitionNo" HeaderText="eRequisition No" />
                            <asp:BoundField DataField="Asset" HeaderText="Asset" />
                            <asp:BoundField DataField="Purchase_Budget" 
                                HeaderText="Amount (Purchase / Budget)" />
                            <asp:BoundField DataField="ApplyStatus" HeaderText="Apply Status" />
                            <asp:BoundField DataField="CreatedByName" HeaderText="Apply By" />
                            <asp:BoundField DataField="CreatedOn" HeaderText="Apply On" />
                        </Columns>
                        <EmptyDataRowStyle BorderColor="Silver" ForeColor="Red" />
                    </asp:GridView>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>&nbsp;&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Asset Information :-" Font-Bold="True" ForeColor="Blue" BackColor="GradientActiveCaption" Width="100%"></asp:Label></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="Label48" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="eRequisition No"></asp:Label></td>
                <td>:</td>
                <td>
                    <asp:Label ID="lbleRequisitionNo" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                <td></td>
                <td><asp:Label ID="Label54" runat="server" Font-Names="Verdana" Font-Size="X-Small" 
                        Text="Apply Date"></asp:Label></td>
                <td>:</td>
                <td><asp:Label ID="lblDateRequired" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small"></asp:Label></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Label ID="Label10" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Station"></asp:Label></td>
                <td>:</td>
                <td>
                    <asp:Label ID="lblStation" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small"></asp:Label></td>
                <td></td>
                <td><asp:Label ID="Label94" runat="server" Font-Names="Verdana" Font-Size="X-Small" 
                        Text="Apply By"></asp:Label></td>
                <td>:</td>
                <td><asp:Label ID="lblApplyBy" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small"></asp:Label></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><asp:Label ID="Label11" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Location"></asp:Label></td>
                <td>:</td>
                <td>
                    <asp:Label ID="lblLocation" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small"></asp:Label></td>
                <td>&nbsp;</td>
                <td><asp:Label ID="Label23" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Condition"></asp:Label></td>
                <td>:</td>
                <td><asp:Label ID="lblCondition" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small"></asp:Label></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Category"></asp:Label></td>
                <td>:</td>
                <td colspan="5">
                    <asp:Label ID="lblParentCategory" runat="server" BorderColor="Silver" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Verdana" Font-Size="X-Small"></asp:Label>&nbsp;<asp:Label 
                        ID="lblCategory" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small"></asp:Label>
                    <br />
                    <asp:Label ID="lblSpecialRequest" runat="server" BorderColor="Transparent" Font-Names="Verdana"
                        Font-Size="X-Small" ForeColor="#C000C0">Special Request - </asp:Label><asp:Label
                            ID="lblSpecialReqCategory" runat="server" BorderColor="Silver" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Verdana" Font-Size="X-Small" ForeColor="#C000C0"></asp:Label>
                    </td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><asp:Label ID="Label13" runat="server" Font-Names="Verdana" Font-Size="X-Small" 
                        Text="Brand &amp; Model"></asp:Label></td>
                <td>:</td>
                <td colspan="5">
                    <asp:Label ID="lblBrandModel" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small"></asp:Label></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><asp:Label ID="Label92" runat="server" Font-Names="Verdana" Font-Size="X-Small" 
                        Text="Specification"></asp:Label></td>
                <td>:</td>
                <td colspan="5"><asp:Label ID="lblSpecification" runat="server" 
                        Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Label ID="Label87" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Application To Be Used"></asp:Label></td>
                <td>:</td>
                <td colspan="5">
                    <asp:Label ID="lblAppToBeUsed" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small"></asp:Label></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Label ID="Label7" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Description"></asp:Label></td>
                <td>:</td>
                <td colspan="5">
                    <asp:Label ID="lblDescription" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small"></asp:Label></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><asp:Label ID="Label26" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Remarks"></asp:Label></td>
                <td>:</td>
                <td colspan="5"><asp:Label ID="lblRemarks" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small"></asp:Label></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Label ID="Label9" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Owner"></asp:Label></td>
                <td>:</td>
                <td colspan="5"><asp:GridView ID="gvUserList" runat="server" 
                        AutoGenerateColumns="False" DataKeyNames="OwnerID"
                        Font-Names="Verdana" Font-Size="X-Small" BackColor="White" 
                        BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" 
                        ForeColor="Black" GridLines="Horizontal">
                        <Columns>
                            <asp:TemplateField HeaderText="Name">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("FullName") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Bind("OwnerName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Job Grade">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("JobGrade") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblJobGrade0" runat="server" Text='<%# Bind("JobGrade") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="JobFunction" HeaderText="Job Function" />
                            <asp:TemplateField HeaderText="Internal Audit">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("InternalAudit") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblInternalAudit" runat="server" Text='<%# Bind("Internal_Audit") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="MIS Seed Member">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("MISSeedMember") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblMISSeedMember0" runat="server" 
                                        Text='<%# Bind("MIS_Seed_Member") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ExistingAssetID" HeaderText="Existing Asset">
                            </asp:BoundField>
                            <asp:BoundField DataField="ExistingAssetPurchaseDate" 
                                HeaderText="Existing Asset Purchase Date">
                                <ItemStyle ForeColor="#0000CC" />
                            </asp:BoundField>
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                    </asp:GridView>
                    </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Label ID="Label17" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Vendor"></asp:Label></td>
                <td>:</td>
                <td>
                    <asp:Label ID="lblVendor" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small"></asp:Label></td>
                <td></td>
                <td>
                    <asp:Label ID="Label19" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Warranty Period"></asp:Label></td>
                <td>:</td>
                <td>
                    <asp:Label ID="lblWarrantyPeriod" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small"></asp:Label></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Label ID="Label25" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Quantity"></asp:Label></td>
                <td>:</td>
                <td><asp:Label ID="lblQuantity" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small" Font-Bold="True"></asp:Label></td>
                <td></td>
                <td>
                    <asp:Label ID="Label31" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Quotation/Receipt"></asp:Label></td>
                <td>:</td>
                <td>
                    <asp:LinkButton ID="lbtnQuotation" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small"></asp:LinkButton>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Label ID="Label28" runat="server" Font-Names="Verdana" Font-Size="X-Small" 
                        Text="Purchase Cost per Unit"></asp:Label>
                    <br />
                    <asp:Label ID="Label93" runat="server" Font-Names="Verdana" Font-Size="X-Small" 
                        Text="(Quoted/Budgeted)"></asp:Label></td>
                <td>:</td>
                <td colspan="5"><asp:Label ID="lblPurchaseCost" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small" Font-Bold="True" ForeColor="#3333CC"></asp:Label></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Label ID="Label32" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Currency Rate"></asp:Label></td>
                <td>:</td>
                <td colspan="5"><asp:Label ID="lblCurrencyRate" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small" Font-Bold="True"></asp:Label></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Label ID="Label37" runat="server" Font-Names="Verdana" Font-Size="X-Small" 
                        Text="Local Approved By"></asp:Label></td>
                <td>:</td>
                <td colspan="5"><asp:Label ID="lblLocalApprovedBy" runat="server" 
                        Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>&nbsp;</td>
                <td></td>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="7">
                        <table width="100%">
                            <tr>
                                <td colspan="5">
                                    <asp:Label ID="Label64" runat="server" BackColor="#C0C0FF" Font-Bold="True" Font-Names="Verdana"
                                        Font-Size="X-Small" Text="eRequisition Approval :-" Width="100%"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 100px">
                                    <asp:Label ID="Label63" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Comment :"></asp:Label></td>
                                <td colspan="4">
                                    <asp:TextBox ID="txtComment" runat="server" Width="716px"></asp:TextBox></td>
                            </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnApprove" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Approve"
                                    Width="100px" OnClientClick="showDate();" /></td>
                            <td style="width: 100px">
                                &nbsp;</td>
                            <td style="width: 100px">
                                &nbsp;</td>
                            <td>
                                <asp:Button ID="btnModify" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Modify"
                                    Width="100px" OnClientClick="showDate();" />
                            </td>
                            <td>
                                <asp:Button ID="btnReject" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Reject"
                                    Width="100px" OnClientClick="showDate();" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;</td>
                            <td style="width: 100px">
                                &nbsp;</td>
                            <td style="width: 100px">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                    </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
