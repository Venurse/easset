<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAssetSoftware.aspx.vb" Inherits="Asset_frmAssetSoftware" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Asset Software</title>
</head>
<body>
    <form id="frmAssetSoftware" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Asset Software"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="lblID" runat="server" Font-Names="Verdana" Font-Size="X-Small" Visible="False"></asp:Label>
                    <asp:HiddenField ID="hdfAssetID" runat="server" />
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table style="width: 650px">
                        <tr>
                            <td style="width: 150px">
                                <asp:Label ID="Label7" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Category"></asp:Label></td>
                            <td style="width: 5px">
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlSoftwareCategory" runat="server" AutoPostBack="True" Font-Names="Verdana"
                                    Font-Size="X-Small">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 150px">
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Software"></asp:Label></td>
                            <td style="width: 5px">
                                :</td>
                            <td>
                    <asp:DropDownList ID="ddlSoftware" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                    </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Version" Visible="False"></asp:Label></td>
                            <td>
                                </td>
                            <td>
                    <asp:TextBox ID="txtVersion" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="250px" Visible="False"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                    <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Language"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <asp:TextBox ID="txtLanguage" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="250px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                    <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="License"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <asp:DropDownList ID="ddlLicense" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                        <asp:ListItem>Y</asp:ListItem>
                        <asp:ListItem>N</asp:ListItem>
                    </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>
                    <asp:Label ID="Label6" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Refence"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <asp:TextBox ID="txtReference" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="450px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <table style="width: 450px">
                                    <tr>
                                        <td style="width: 150px">
                                <asp:Button ID="btnSave" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Save"
                                    Width="85px" /></td>
                                        <td style="width: 150px">
                                <asp:Button ID="btnClear" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Clear"
                                    Width="85px" /></td>
                                        <td style="width: 150px">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="8">
                    <asp:GridView ID="gvSoftware" runat="server" AutoGenerateColumns="False" Font-Names="Verdana"
                        Font-Size="X-Small" BorderWidth="1px" DataKeyNames="AssetID,SoftwareID,ID">
                        <Columns>
                            <asp:TemplateField HeaderText="Remove">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnRemove" runat="server" CommandArgument='<%# Bind("SoftwareID") %>' CommandName="Delete" OnClientClick="return confirm('Are you sure you want to remove this Software?');">Remove</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Software">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Software") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnSoftware" runat="server" CommandArgument='<%# Bind("SoftwareID") %>'
                                        Text='<%# Bind("Software") %>' CommandName="Select"></asp:LinkButton>
                                    <asp:HiddenField ID="hdfID" runat="server" Value='<%# Bind("ID") %>' />
                                    <asp:HiddenField ID="hdfSoftwareCategoryID" runat="server" Value='<%# Bind("SoftwareCategoryID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Version" Visible="False">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Version") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblVersion" runat="server" Text='<%# Bind("Version") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Language">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Language") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblLanguage" runat="server" Text='<%# Bind("Language") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="License">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("License") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblLicense" runat="server" Text='<%# Bind("License") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reference">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Reference") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblReference" runat="server" Text='<%# Bind("Reference") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table style="width: 650px">
                        <tr>
                            <td style="width: 150px">
                            </td>
                            <td style="width: 5px">
                            </td>
                            <td>
                                <table style="width: 450px">
                                    <tr>
                                        <td style="width: 150px">
                                            <asp:Button ID="btnReturn" runat="server" Font-Names="Verdana" Font-Size="Small"
                                                Text="Return" Width="85px" /></td>
                                        <td style="width: 150px">
                                        </td>
                                        <td style="width: 150px">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
