Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings
Imports OfficeOpenXml

Partial Class Asset_frmLansweeperMappingStatusReport
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")
    Dim blnAssetDetailPagePopUP As String = ConfigurationSettings.AppSettings("AssetDetailPagePopUP")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If IsPostBack() = False Then
            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "A0011")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Lansweeper Mapping Status Report page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        'btnQuery.Visible = True
                        'gvAssetList.Columns(1).Visible = True
                    Else
                        Session("blnWrite") = False
                        'btnQuery.Visible = False
                        'gvAssetList.Columns(1).Visible = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                    'btnQuery.Visible = True
                    'gvAssetList.Columns(1).Visible = True
                Else
                    Session("blnModifyOthers") = False
                    'btnQuery.Visible = False
                    'gvAssetList.Columns(1).Visible = False
                End If

                'If Mid(strAccessRight, 5, 1) = 1 Then
                '    Session("blnDeleteOthers") = True
                '    btnQuery.Visible = True
                '    gvAssetList.Columns(1).Visible = True
                'Else
                '    Session("blnDeleteOthers") = False
                '    btnQuery.Visible = False
                '    gvAssetList.Columns(1).Visible = False
                'End If
            End If

            BindDropDownList()
            GetReportData()

        Else

        End If
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[AccessStation],[Year],[Mapping_Group_Category]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)

                If Session("DA_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("DA_StationID").ToString
                End If


                ddlYear.DataSource = MyData.Tables(1)
                ddlYear.DataValueField = MyData.Tables(1).Columns("Year").ToString
                ddlYear.DataTextField = MyData.Tables(1).Columns("Year").ToString
                ddlYear.DataBind()
                ddlYear.SelectedValue = Now.Year


                ddlItem.DataSource = MyData.Tables(2)
                ddlItem.DataValueField = MyData.Tables(2).Columns("CategoryID").ToString
                ddlItem.DataTextField = MyData.Tables(2).Columns("Category").ToString
                ddlItem.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub GetReportData()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_LansweeperMappingStatusReport")

                With MyCommand

                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@Status", SqlDbType.VarChar, 20)
                    .Parameters("@Status").Value = ddlStatus.SelectedValue.ToString
                    .Parameters.Add("@Year", SqlDbType.Int)
                    If ddlYear.SelectedValue.ToString = "" Then
                        .Parameters("@Year").Value = DBNull.Value
                    Else
                        .Parameters("@Year").Value = CInt(ddlYear.SelectedValue.ToString)
                    End If

                    .Parameters.Add("@Item", SqlDbType.BigInt)
                    .Parameters("@Item").Value = CInt(ddlItem.SelectedValue.ToString)
                    .Parameters.Add("@SubCategoryID", SqlDbType.BigInt)
                    If ddlSubCategory.Items.Count > 0 Then
                        .Parameters("@SubCategoryID").Value = CInt(ddlSubCategory.SelectedValue.ToString)
                    Else
                        .Parameters("@SubCategoryID").Value = 0
                    End If
                    .Parameters.Add("@AssetNo", SqlDbType.NVarChar, 50)
                    .Parameters("@AssetNo").Value = txtAssetNo.Text.ToString
                    .Parameters.Add("@AssetID", SqlDbType.NVarChar, 50)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString

                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AssetList"

                Session("LansweeperReportData") = MyData
                gvAssetList.DataSource = MyData
                gvAssetList.DataMember = MyData.Tables(0).TableName
                gvAssetList.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "GetReportData: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnQuery_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQuery.Click
        Session("DA_StationID") = ddlStation.SelectedValue.ToString
        GetReportData()
    End Sub

    Protected Sub gvAssetList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvAssetList.SelectedIndexChanged
        Dim lbtnAssetID As New LinkButton
        lbtnAssetID = gvAssetList.SelectedRow.FindControl("lbtnAssetID")

        Session("AssetID") = ""
        Session("AssetID") = lbtnAssetID.CommandArgument.ToString

        ' verify whether AssetDetailPage show in a popup window or not.
        If blnAssetDetailPagePopUP Then
            Dim strScript As String
            strScript = "<script language=javascript>"
            strScript += "theChild = window.open('frmAssetDetail.aspx?ParentForm=frmLansweeperMappingStatusReport','Asset Detail','status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no,fullscreen=yes,titlebar=no,border-style=none');"
            strScript += "</script>"
            Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
        Else
            Response.Redirect("~/Asset/frmAssetDetail.aspx")
        End If

    End Sub

    Protected Sub ddlItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlItem.SelectedIndexChanged
        GetSubCategory()
    End Sub

    Private Sub GetSubCategory()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByID")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[SubCategory]"
                    .Parameters.Add("@ID", SqlDbType.BigInt)
                    .Parameters("@ID").Value = CInt(ddlItem.SelectedValue.ToString)
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "SubCategory"


                ddlSubCategory.DataSource = MyData.Tables(0)
                ddlSubCategory.DataValueField = MyData.Tables(0).Columns("CategoryID").ToString
                ddlSubCategory.DataTextField = MyData.Tables(0).Columns("Category").ToString
                ddlSubCategory.DataBind()


            Catch ex As Exception
                Me.lblMsg.Text = "GetSubCategory: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub


    Protected Sub btnExport_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click


        If Session("UserID") Is Nothing Then
            btnQuery_Click(sender, e)
        End If

        Dim dsList As DataSet
        dsList = Session("LansweeperReportData")

        If dsList.Tables(0).Rows.Count = 0 Then
            ClientScript.RegisterStartupScript(Me.GetType(), "Report", "<script>alert('No record found.')</script>")
            Exit Sub
        End If
        Dim excel As New ExcelPackage
        Dim workSheet As ExcelWorksheet
        Dim cell As Object
        Dim totalCols As Integer
        Dim totalRows As Integer

        workSheet = excel.Workbook.Worksheets.Add("Report")
        cell = workSheet.Cells
        totalCols = dsList.Tables(0).Columns.Count - 1
        totalRows = dsList.Tables(0).Rows.Count

        Dim ColName As String = ""
        Dim intCol As Integer = 1

        Dim type As String
        Dim col As Integer
        Dim row As Integer

        workSheet.View.ZoomScale = 90
        For col = 0 To totalCols
            ColName = dsList.Tables(0).Columns(col).ColumnName
            cell = workSheet.Cells(1, intCol)
            cell.Style.Font.Bold = True
            cell.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid
            cell.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSkyBlue)
            cell.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin)
            cell.Value = ColName

            type = dsList.Tables(0).Columns(col).DataType.ToString()
            If type = "System.DateTime" Then
                workSheet.Column(intCol).Style.Numberformat.Format = "dd/mm/yyyy"
            End If
            intCol = intCol + 1
        Next


        For row = 1 To totalRows
            intCol = 1
            For col = 0 To totalCols
                cell.AutoFitColumns(10, 100)
                cell = workSheet.Cells(row + 1, intCol)
                cell.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin)
                cell.Value = dsList.Tables(0).Rows(row - 1)(col)
                intCol = intCol + 1
            Next
        Next



        Using memoryStream As New MemoryStream
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment;  filename=eAssetReport.xls")
            excel.SaveAs(memoryStream)
            memoryStream.WriteTo(Response.OutputStream)
            Response.Flush()
            Response.End()
        End Using


    End Sub
End Class
