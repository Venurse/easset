﻿Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Asset_frmLanSweeperMapping
    Inherits System.Web.UI.Page
    Dim clsCF As New clsComFunction
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strLansweeperConnectionString As String = ConfigurationSettings.AppSettings("LansweeperConnectionString")
    Dim AssetID As String
    Dim AssetNo As String
    Dim eRequisitionNo As String
    Dim Category As String
    Dim Currency As String
    Dim PurchaseAmt As Decimal
    Dim BudgetAmt As Decimal
    Dim MappedAssetList As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Page.MaintainScrollPositionOnPostBack = True

        If IsPostBack() = False Then
            hdfSearchType.Value = "Load"
            ''BindDropDownList()
            BindDropDownListAssetType()
            BindDropDownListIPLocation()
            GetLanSweeper()
        End If

    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[StationList_Blank]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "Station"

                ddlStation.DataSource = MyData.Tables(0)
                ddlStation.DataValueField = MyData.Tables(0).Columns("StationID").ToString
                ddlStation.DataTextField = MyData.Tables(0).Columns("StationCode").ToString
                ddlStation.DataBind()

                ddlStation.SelectedValue = ""

                'If Session("DA_StationID").ToString <> "" Then
                '    ddlStation.SelectedValue = Session("DA_StationID").ToString
                'End If

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub BindDropDownListAssetType()
        Using MyConnection As New SqlConnection(strLansweeperConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData")

                With MyCommand

                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 500)
                    .Parameters("@ReferenceType").Value = "Asset Type"
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "Asset Type"

                ddlAssetType.DataSource = MyData.Tables(0)
                ddlAssetType.DataValueField = MyData.Tables(0).Columns("AssetType").ToString
                ddlAssetType.DataTextField = MyData.Tables(0).Columns("AssetTypeName").ToString
                ddlAssetType.DataBind()

                ddlAssetType.SelectedValue = "-99999"

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub BindDropDownListIPLocation()
        Using MyConnection As New SqlConnection(strLansweeperConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData")

                With MyCommand

                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 500)
                    .Parameters("@ReferenceType").Value = "IP Locations"
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "IP Locations"

                ddlIPLocation.DataSource = MyData.Tables(0)
                ddlIPLocation.DataValueField = MyData.Tables(0).Columns("LocationID").ToString
                ddlIPLocation.DataTextField = MyData.Tables(0).Columns("IPLocation").ToString
                ddlIPLocation.DataBind()

                ddlIPLocation.SelectedValue = 0

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub GetLanSweeper()
        Using MyConnection As New SqlConnection(strLansweeperConnectionString)

            Try

                Dim strSeenDateFrom As String = Format(cldSeenDateFrom.SelectedDate, "dd MMM yyyy").ToString
                If strSeenDateFrom.Contains("01 Jan 0001") = True Then
                    strSeenDateFrom = ""
                End If

                Dim strSeenDateTo As String = Format(cldSeenDateTo.SelectedDate, "dd MMM yyyy").ToString
                If strSeenDateTo.Contains("01 Jan 0001") = True Then
                    strSeenDateTo = ""
                End If

                Dim AssetID As Integer = 0
                If txtAssetID.Text.ToString <> "" Then
                    AssetID = CInt(txtAssetID.Text.ToString)
                End If

                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetAssetListForMapping")

                With MyCommand

                    .Parameters.Add("@MappedAssetList", SqlDbType.NVarChar)
                    .Parameters("@MappedAssetList").Value = Session("MappedAssetList").ToString
                    .Parameters.Add("@AssetName", SqlDbType.VarChar, 500)
                    .Parameters("@AssetName").Value = txtAssetName.Text.ToString
                    .Parameters.Add("@Station", SqlDbType.VarChar, 6)
                    .Parameters("@Station").Value = "" 'ddlStation.SelectedItem.Text.ToString
                    .Parameters.Add("@SeenDateFrom", SqlDbType.VarChar, 20)
                    .Parameters("@SeenDateFrom").Value = strSeenDateFrom
                    .Parameters.Add("@SeenDateTo", SqlDbType.VarChar, 20)
                    .Parameters("@SeenDateTo").Value = strSeenDateTo
                    .Parameters.Add("@LansweeperAssetID", SqlDbType.BigInt)
                    .Parameters("@LansweeperAssetID").Value = AssetID
                    .Parameters.Add("@AssetTypeID", SqlDbType.BigInt)
                    .Parameters("@AssetTypeID").Value = CInt(ddlAssetType.SelectedValue.ToString())
                    .Parameters.Add("@IPAddress", SqlDbType.NVarChar, 50)
                    .Parameters("@IPAddress").Value = txtIPAddress.Text.ToString
                    .Parameters.Add("@IPLocation", SqlDbType.BigInt)
                    .Parameters("@IPLocation").Value = CInt(ddlIPLocation.SelectedValue.ToString())
                    .Parameters.Add("@SearchType", SqlDbType.NVarChar, 50)
                    .Parameters("@SearchType").Value = hdfSearchType.Value.ToString

                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AssetList"
                MyData.Tables(1).TableName = "MappedAssetList"

                gvLanSweeperAssetList.DataSource = MyData
                gvLanSweeperAssetList.DataMember = MyData.Tables(0).TableName
                gvLanSweeperAssetList.DataBind()

                gvMapped.DataSource = MyData
                gvMapped.DataMember = MyData.Tables(1).TableName
                gvMapped.DataBind()


            Catch ex As Exception
                Me.lblMsg.Text = "GetLanSweeper: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using

    End Sub

    Protected Sub btnMapping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMapping.Click

        Dim strReturnMsg As String = ""
        Dim chkSelect As New CheckBox
        Dim lblAssetID As New Label
        Dim strAssetID As String = ""
        Dim strMapAssetList As String = ""
        Dim i As Integer

        For i = 0 To gvLanSweeperAssetList.Rows.Count - 1
            chkSelect = gvLanSweeperAssetList.Rows(i).FindControl("chkSelect")
            lblAssetID = gvLanSweeperAssetList.Rows(i).FindControl("lblAssetID")
            If chkSelect.Checked = True Then
                strAssetID = lblAssetID.Text.ToString()
                If strMapAssetList = "" Then
                    strMapAssetList = strAssetID
                Else
                    strMapAssetList = strMapAssetList + "," + strAssetID
                End If
            End If
        Next

        If strMapAssetList = "" Then
            ClientScript.RegisterStartupScript(Me.GetType(), "Mapping", "<script>alert('Please select Asset to Mapping.')</script>")
            Return
        End If

        strReturnMsg = MappingLansweeper(strMapAssetList)

        If strReturnMsg = "" Then
            strReturnMsg = MappingeAsset(strMapAssetList)
        End If

        ClientScript.RegisterStartupScript(Me.GetType(), "Mapping", "<script>alert('" + strReturnMsg + "')</script>")

    End Sub

    Private Function MappingLansweeper(ByVal strMapAssetList As String) As String
        Dim strMsg As String = ""
        Dim strReturnMsg As String = ""

        Using sqlConn As New SqlConnection(strLansweeperConnectionString)
            Try
                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveMapping", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@MapAssetList", SqlDbType.NVarChar)
                    .Parameters("@MapAssetList").Value = strMapAssetList
                    .Parameters.Add("@eAssetAssetID", SqlDbType.NVarChar, 50)
                    .Parameters("@eAssetAssetID").Value = Session("AssetID").ToString
                    .Parameters.Add("@eAssetAssetNo", SqlDbType.NVarChar, 50)
                    .Parameters("@eAssetAssetNo").Value = Session("AssetNo").ToString
                    .Parameters.Add("@eAsseteRequisitionNo", SqlDbType.NVarChar, 50)
                    .Parameters("@eAsseteRequisitionNo").Value = Session("eRequisitionNo").ToString
                    .Parameters.Add("@eAssetCategory", SqlDbType.NVarChar, 255)
                    .Parameters("@eAssetCategory").Value = Session("Category").ToString
                    .Parameters.Add("@eAssetCurrency", SqlDbType.VarChar, 6)
                    .Parameters("@eAssetCurrency").Value = Session("Currency").ToString
                    .Parameters.Add("@eAssetPurchaseAmt", SqlDbType.Decimal, 18, 2)
                    .Parameters("@eAssetPurchaseAmt").Value = CDec(Session("PurchaseAmt").ToString)
                    .Parameters.Add("@eAssetBudgetAmt", SqlDbType.Decimal, 18, 2)
                    .Parameters("@eAssetBudgetAmt").Value = CDec(Session("BudgetAmt").ToString)
                    .Parameters.Add("@PurchaseDate", SqlDbType.VarChar, 20)
                    .Parameters("@PurchaseDate").Value = Session("PurchaseDate").ToString
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output

                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()

                End With

                If strMsg <> "" Then
                    strReturnMsg = strMsg
                    ''ClientScript.RegisterStartupScript(Me.GetType(), "Mapping", "<script>alert('" + strMsg + "')</script>")
                Else
                    strReturnMsg = ""
                    ''ClientScript.RegisterStartupScript(Me.GetType(), "Mapping", "<script>alert('Mapping successful.')</script>")
                End If

                Return strReturnMsg
            Catch ex As Exception
                strReturnMsg = ex.Message.ToString
                Return strReturnMsg
                ''ClientScript.RegisterStartupScript(Me.GetType(), "Mapping", "<script>alert('" + ex.Message.ToString + "')</script>")
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Function

    Private Function MappingeAsset(ByVal strMapAssetList As String) As String
        Dim strMsg As String = ""
        Dim strMappedAssetList As String = ""
        Dim strReturnMsg As String = ""

        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveLansweeperMapping", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@eAssetAssetID", SqlDbType.NVarChar, 50)
                    .Parameters("@eAssetAssetID").Value = Session("AssetID").ToString
                    .Parameters.Add("@MapAssetID", SqlDbType.NVarChar, 4000)
                    .Parameters("@MapAssetID").Value = strMapAssetList
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .Parameters.Add("@MappedAssetList", SqlDbType.NVarChar, 4000)
                    .Parameters("@MappedAssetList").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                    strMappedAssetList = .Parameters("@MappedAssetList").Value.ToString()
                End With

                If strMsg <> "" Then
                    strReturnMsg = strMsg
                Else
                    Session("MappedAssetList") = strMappedAssetList
                    hdfSearchType.Value = "Search"
                    GetLanSweeper()
                    strReturnMsg = "Mapping Successful"
                End If

                Return strReturnMsg
            Catch ex As Exception
                strReturnMsg = ex.Message.ToString
                Return strReturnMsg
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Function

    Protected Sub gvMapped_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvMapped.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim strReturnMsg As String
                Dim strMappedAssetList As String
                Dim intID As Integer = 0
                Dim lbtnRemove As New LinkButton

                lblMsg.Text = ""

                lbtnRemove = gvMapped.Rows(e.RowIndex).FindControl("lbtnRemove")
                intID = CInt(lbtnRemove.CommandArgument.ToString)

                ''UnMapping Lansweeper Asset
                strReturnMsg = UnMappingLansweeper(intID)

                If strReturnMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strReturnMsg + "')</script>")
                Else
                    clsCF.CheckConnectionState(sqlConn)

                    Dim cmd As New SqlCommand("SP_DeleteLanSweeperMapping", sqlConn)
                    With cmd
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                        .Parameters("@AssetID").Value = Session("AssetID").ToString
                        .Parameters.Add("@LansweeperAssetID", SqlDbType.Int)
                        .Parameters("@LansweeperAssetID").Value = intID
                        .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                        .Parameters("@UserID").Value = Session("DA_UserID").ToString
                        .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                        .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                        .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                        .Parameters("@Msg").Direction = ParameterDirection.Output
                        .Parameters.Add("@MappedAssetList", SqlDbType.NVarChar, 4000)
                        .Parameters("@MappedAssetList").Direction = ParameterDirection.Output
                        .CommandTimeout = 0
                        .ExecuteNonQuery()

                        strMsg = .Parameters("@Msg").Value.ToString()
                        strMappedAssetList = .Parameters("@MappedAssetList").Value.ToString()
                    End With

                    If strMsg <> "" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                    Else
                        Session("MappedAssetList") = strMappedAssetList
                        GetLanSweeper()
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Remove Successful')</script>")
                    End If
                End If

                
            Catch ex As Exception
                Me.lblMsg.Text = "gvMapped_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Private Function UnMappingLansweeper(ByVal intID As Integer) As String
        Dim strMsg As String = ""
        Dim strReturnMsg As String = ""

        Using sqlConn As New SqlConnection(strLansweeperConnectionString)
            Try
                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_UnMapping", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@UnMapAssetID", SqlDbType.BigInt)
                    .Parameters("@UnMapAssetID").Value = intID
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output

                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()

                End With

                If strMsg <> "" Then
                    strReturnMsg = strMsg
                Else
                    strReturnMsg = ""
                End If

                Return strReturnMsg
            Catch ex As Exception
                strReturnMsg = ex.Message.ToString
                Return strReturnMsg
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Function

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Session("AssetNo") = ""
        Session("eRequisitionNo") = ""
        Session("Category") = ""
        Session("Currency") = ""
        Session("PurchaseAmt") = ""
        Session("BudgetAmt") = ""
        Session("PurchaseDate") = ""
        hdfSearchType.Value = "Load"

        Dim ParentFormID As String = ""
        ParentFormID = Request.QueryString("ParentForm").ToString
        Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener=self;window.close();</script>")

       
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        hdfSearchType.Value = "Search"
        GetLanSweeper()
    End Sub
End Class
