Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Asset_frmAssetChild
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack() = False Then
            hdfAssetID.Value = ""

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            btnSave.Enabled = False
            gvChildAsset.Columns(0).Visible = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "A0006")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Asset Child Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnSave.Enabled = True
                        gvChildAsset.Columns(0).Visible = True
                    Else
                        Session("blnWrite") = False
                        btnSave.Enabled = False
                        gvChildAsset.Columns(0).Visible = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            BindDropDownList()
            If Session("AssetID") <> Nothing Then
                If Session("AssetID").ToString <> "" Then
                    hdfAssetID.Value = Session("AssetID").ToString
                End If
            End If
            GetAssetChild()
            Session("AssetID") = ""
        End If
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByID")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[ChildAsset_SubCategory]"
                    .Parameters.Add("@ID", SqlDbType.BigInt)
                    .Parameters("@ID").Value = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "Category"

                ddlItem.DataSource = MyData.Tables(0)
                ddlItem.DataValueField = MyData.Tables(0).Columns("CategoryID").ToString
                ddlItem.DataTextField = MyData.Tables(0).Columns("Category").ToString
                ddlItem.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Using sqlConn As New SqlConnection(strConnectionString)
            Try

                Dim strMsg As String = ""
                lblMsg.Text = ""

                If ddlAssetNo.SelectedValue.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Child Asset's Asset No')</script>")
                    'lblMsg.Text = "Pls select Child Asset's Asset No."
                    Exit Sub
                End If

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveAssetChild", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = hdfAssetID.Value.ToString
                    .Parameters.Add("@Child_AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@Child_AssetID").Value = ddlAssetNo.SelectedValue.ToString
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else

                    btnClear_Click(sender, e)
                    GetAssetChild()

                    'lblMsg.Text = "Save Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub gvChildAsset_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvChildAsset.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim strID As String = 0
                Dim lbtnRemove As New LinkButton

                lblMsg.Text = ""

                lbtnRemove = gvChildAsset.Rows(e.RowIndex).FindControl("lbtnRemove")
                strID = lbtnRemove.CommandArgument.ToString

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteAssetChild", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = hdfAssetID.Value.ToString
                    .Parameters.Add("@Child_AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@Child_AssetID").Value = strID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    btnClear_Click(sender, e)
                    GetAssetChild()
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                    'lblMsg.Text = "Delete Successful"
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvChildAsset_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        ddlItem.SelectedValue = 0
        ddlAssetNo.SelectedValue = ""
        lblOwnerType.Text = ""
        lblOwner.Text = ""
        lblBrand.Text = ""
        lblModel.Text = ""

    End Sub

    Protected Sub ddlItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlItem.SelectedIndexChanged
        GetAssetNo()
    End Sub

    Private Sub GetAssetNo()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetAssetNo")

                With MyCommand

                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = Session("AssetStationID").ToString
                    .Parameters.Add("@Item", SqlDbType.BigInt)
                    .Parameters("@Item").Value = CInt(ddlItem.SelectedValue.ToString)
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AssetList"

                ViewState("AssetNoDetail") = MyData

                ddlAssetNo.DataSource = MyData.Tables(0)
                ddlAssetNo.DataValueField = MyData.Tables(0).Columns("AssetID").ToString
                ddlAssetNo.DataTextField = MyData.Tables(0).Columns("AssetNo").ToString
                ddlAssetNo.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "GetAssetNo: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub GetAssetChild()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetAssetChild")

                With MyCommand

                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = hdfAssetID.Value.ToString
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AssetChild"

                gvChildAsset.DataSource = MyData
                gvChildAsset.DataMember = MyData.Tables(0).TableName
                gvChildAsset.DataBind()

                If MyData.Tables(1).Rows(0)("CreatedBy").ToString <> Session("DA_UserID").ToString Then
                    If Session("blnModifyOthers") = False Then
                        btnSave.Enabled = False
                        gvChildAsset.Columns(0).Visible = False
                    Else
                        btnSave.Enabled = True
                        gvChildAsset.Columns(0).Visible = True
                    End If

                    If Session("blnDeleteOthers") = True Then
                        gvChildAsset.Columns(0).Visible = True
                    Else
                        gvChildAsset.Columns(0).Visible = False
                    End If
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "GetAssetChild: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub ddlAssetNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAssetNo.SelectedIndexChanged
        Dim dsAssetNoDetail As DataSet
        dsAssetNoDetail = ViewState("AssetNoDetail")

        Dim strChild_AssetID As String = ""
        strChild_AssetID = ddlAssetNo.SelectedValue.ToString

        lblOwnerType.Text = ""
        lblOwner.Text = ""
        lblBrand.Text = ""
        lblModel.Text = ""

        If strChild_AssetID <> "" Then
            Dim r As DataRow
            For Each r In dsAssetNoDetail.Tables(0).Select("AssetID='" & strChild_AssetID & "'")
                lblOwnerType.Text = r.Item("OwnerType").ToString
                lblOwner.Text = r.Item("OwnerName").ToString
                lblBrand.Text = r.Item("Brand").ToString
                lblModel.Text = r.Item("Model").ToString
            Next
        End If
    End Sub

    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        Dim ParentFormID As String = ""
        ParentFormID = Request.QueryString("ParentForm").ToString
        Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener =self;window.close();</script>")
    End Sub
End Class
