Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings


Partial Class Asset_frmSubAssetDetail
    Inherits System.Web.UI.Page
    Dim clsNewAsset As New DIMERCO.eAsset.BO.clsNewAsset
    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")
    Dim strFileExtension As String = ConfigurationSettings.AppSettings("DocExtension")
    Dim intFileSizeLimitKB As Integer = ConfigurationSettings.AppSettings("intFileSizeLimitKB")
    Dim intFileSizeLimit As Integer = ConfigurationSettings.AppSettings("intFileSizeLimit")

    Public Shared hifSupDoc As ArrayList = New ArrayList()
    Dim strRightCode As String = ConfigurationSettings.AppSettings("RightCode")
    Public Shared dsUseList As DataTable = New DataTable()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Page.MaintainScrollPositionOnPostBack = True

        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If IsPostBack() = False Then
            Session("DA_USERLIST") = ""
            Session("DA_USERLIST_TYPE") = ""

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            DisableButton()

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "A0008")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If

            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Add Sub Asset page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnSave.Enabled = True
                        gvQuotationReceipt.Columns(0).Visible = True
                        fuBrowse.Enabled = True
                        btnAddQuoReceipt.Enabled = True
                    Else
                        Session("blnWrite") = False
                        btnSave.Enabled = False
                        gvQuotationReceipt.Columns(0).Visible = False
                        fuBrowse.Enabled = False
                        btnAddQuoReceipt.Enabled = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If

                dsUseList.Clear()
                dsUseList.Dispose()
                dsUseList.Columns.Clear()
                dsUseList.Columns.Add("OwnerID")
                dsUseList.Columns.Add("FullName")
                dsUseList.Columns.Add("JobGrade")
                dsUseList.Columns.Add("Internal_Audit")
                dsUseList.Columns.Add("MIS_Seed_Member")
                dsUseList.Columns.Add("Job_Function")

            End If

            BindDropDownList()

            If Request.QueryString("MainAssetID") <> Nothing Then
                hdfMainAssetID.Value = Request.QueryString("MainAssetID").ToString
                GetMainAssetDetail()

                Dim ds As DataSet = GetSubAsset("")
                gvSubAssetTemp.DataSource = ds
                gvSubAssetTemp.DataMember = ds.Tables(0).TableName
                gvSubAssetTemp.DataBind()

                btnGenerate.Text = "Generate"
            End If


        Else
            If Session("DA_USERLIST") <> "" Then
                If Session("DA_USERLIST_TYPE") = "Owner" Then
                    'If Session("DA_USERLIST").ToString <> "" Then
                    '    hdfOwnerUserID.Value = Session("DA_USERLIST").ToString
                    '    If hdfOwnerUserID.Value.ToString <> "" Then
                    '        Dim dsOwnerInfo As DataSet = clsCF.GetOwnerInformation(hdfOwnerUserID.Value.ToString)
                    '        If dsOwnerInfo.Tables.Count > 0 Then
                    '            If dsOwnerInfo.Tables(0).Rows.Count > 0 Then
                    '                txtJobGrade.Text = dsOwnerInfo.Tables(0).Rows(0)("JobGrade").ToString

                    '                If dsOwnerInfo.Tables(0).Rows(0)("IsInternalAudit").ToString = "Y" Then
                    '                    chkInternalAudit.Checked = True
                    '                Else
                    '                    chkInternalAudit.Checked = False
                    '                End If

                    '                If dsOwnerInfo.Tables(0).Rows(0)("IsSeedMember").ToString = "Y" Then
                    '                    chkMIS_Seed_Member.Checked = True
                    '                Else
                    '                    chkMIS_Seed_Member.Checked = False
                    '                End If

                    '            End If
                    '        End If
                    '    End If
                    'End If
                    'ShowApplyTo(Session("DA_USERLIST").ToString, "Owner")
                    ''ChildAssetMaintainRelation()
                ElseIf Session("DA_USERLIST_TYPE") = "LocationApprovedBy" Then
                    If Session("DA_USERLIST").ToString <> "" Then
                        hdfLocationApprovedBy.Value = Session("DA_USERLIST").ToString
                    End If
                    ShowApplyTo(Session("DA_USERLIST").ToString, "LocationApprovedBy")
                ElseIf Session("DA_USERLIST_TYPE") = "CCParty" Then
                    If Session("DA_USERLIST").ToString <> "" Then
                        hdfCCParty.Value = Session("DA_USERLIST").ToString
                    End If
                    ShowApplyTo(Session("DA_USERLIST").ToString, "CCParty")
                ElseIf Session("DA_USERLIST_TYPE") = "UserList" Then
                    If Session("DA_USERLIST").ToString <> "" Then
                        hdfUserList.Value = Session("DA_USERLIST").ToString
                    End If
                    ShowApplyTo(Session("DA_USERLIST").ToString, "UserList")
                End If
                Session("DA_USERLIST_TYPE") = ""
                Session("DA_USERLIST") = ""
            Else
                If Session("DA_USERLIST_TYPE") = "Owner" Then
                    'If Session("DA_USERLIST").ToString <> "" Then
                    '    hdfOwnerUserID.Value = Session("DA_USERLIST").ToString
                    'End If
                    'ShowApplyTo(hdfOwnerUserID.Value.ToString, "Owner")
                    ''ChildAssetMaintainRelation()
                ElseIf Session("DA_USERLIST_TYPE") = "LocationApprovedBy" Then
                    If Session("DA_USERLIST").ToString <> "" Then
                        hdfLocationApprovedBy.Value = Session("DA_USERLIST").ToString
                    End If
                    ShowApplyTo(hdfLocationApprovedBy.Value, "LocationApprovedBy")
                ElseIf Session("DA_USERLIST_TYPE") = "CCParty" Then
                    If Session("DA_USERLIST").ToString <> "" Then
                        hdfCCParty.Value = Session("DA_USERLIST").ToString
                    End If
                    ShowApplyTo(hdfCCParty.Value, "CCParty")
                End If
                Session("DA_USERLIST_TYPE") = ""
                Session("DA_USERLIST") = ""
            End If

            
            If Session("DA_CategoryID").ToString <> "" Then
                hdfCategoryID.Value = Session("DA_CategoryID").ToString
                Session("DA_CategoryID") = ""

                hdfParentCategoryID.Value = ""
                hdfParentCategory.Value = ""

                lblParentCategory.Text = ""
                lblParentCategory.Visible = False
            End If

            If Session("DA_Category").ToString <> "" Then
                lblCategory.Text = Session("DA_Category").ToString
                Session("DA_Category") = ""
            End If

            If Session("DA_ParentCategoryID").ToString <> "" Then
                hdfParentCategoryID.Value = Session("DA_ParentCategoryID").ToString
                BindBrand()
                Session("DA_ParentCategoryID") = ""
            End If

            If Session("DA_ParentCategory").ToString <> "" Then
                hdfParentCategory.Value = Session("DA_ParentCategory").ToString
                Session("DA_ParentCategory") = ""
                If hdfParentCategory.Value.ToString <> "" Then
                    lblParentCategory.Text = hdfParentCategory.Value.ToString + " - "
                    lblParentCategory.Visible = True
                End If
            End If

            If Session("DA_CategoryDepreciation_Period").ToString <> "" Then
                txtDepreciationPeriod.Text = Session("DA_CategoryDepreciation_Period").ToString
                Session("DA_CategoryDepreciation_Period") = ""
            End If
        End If
    End Sub

    Private Sub DisableButton()
        btnSave.Enabled = False
        gvQuotationReceipt.Columns(0).Visible = False
        fuBrowse.Enabled = False
        btnAddQuoReceipt.Enabled = False
    End Sub

    Private Sub ShowApplyTo(ByVal strApplyList As String, ByVal strType As String)
        Try
            Dim MyData As New DataSet
            Dim i As Integer

            If strType = "Owner" Then
                txtOwner.Text = ""
                MyData = DIMERCO.SDK.Utilities.LSDK.getUserDataByList(strApplyList)
                txtOwner.Text = MyData.Tables(0).Rows(0)("Fullname").ToString
            End If

            If strType = "LocationApprovedBy" Then
                lblLocationApprovedBy.Text = ""
                MyData = DIMERCO.SDK.Utilities.LSDK.getUserDataByList(strApplyList)
                For i = 0 To MyData.Tables(0).Rows.Count - 1
                    If lblLocationApprovedBy.Text.ToString = "" Then
                        lblLocationApprovedBy.Text = MyData.Tables(0).Rows(i)("Fullname").ToString()
                    Else
                        lblLocationApprovedBy.Text = lblLocationApprovedBy.Text.ToString + ", " + MyData.Tables(0).Rows(i)("Fullname").ToString
                    End If
                Next
            End If

            'End If
            If strType = "CCParty" Then
                lblCCParty.Text = ""
                MyData = DIMERCO.SDK.Utilities.LSDK.getUserDataByList(strApplyList)

                hdfCCParty.Value = strApplyList
                For i = 0 To MyData.Tables(0).Rows.Count - 1
                    If lblCCParty.Text.ToString = "" Then
                        lblCCParty.Text = MyData.Tables(0).Rows(i)("Fullname").ToString()
                    Else
                        lblCCParty.Text = lblCCParty.Text.ToString + ", " + MyData.Tables(0).Rows(i)("Fullname").ToString
                    End If
                Next
            End If

            If strType = "UserList" Then
                MyData = DIMERCO.SDK.Utilities.LSDK.getUserDataByList(strApplyList)

                hdfUserList.Value = strApplyList

                For i = 0 To MyData.Tables(0).Rows.Count - 1

                    If dsUseList.Select("OwnerID='" + MyData.Tables(0).Rows(i)("UserID").ToString() + "'").Length <= 0 Then

                        Dim dsOwnerInfo As DataSet = clsCF.GetOwnerInformation(MyData.Tables(0).Rows(i)("UserID").ToString())
                        If dsOwnerInfo.Tables.Count > 0 Then
                            If dsOwnerInfo.Tables(0).Rows.Count > 0 Then
                                Dim newUserRow As DataRow = dsUseList.NewRow()

                                newUserRow("OwnerID") = MyData.Tables(0).Rows(i)("UserID").ToString()
                                newUserRow("FullName") = MyData.Tables(0).Rows(i)("FULLNAME").ToString()
                                newUserRow("JobGrade") = dsOwnerInfo.Tables(0).Rows(0)("JobGrade").ToString
                                If dsOwnerInfo.Tables(0).Rows(0)("IsInternalAudit").ToString = "Y" Then
                                    newUserRow("Internal_Audit") = "Y"
                                Else
                                    newUserRow("Internal_Audit") = "N"
                                End If
                                If dsOwnerInfo.Tables(0).Rows(0)("IsSeedMember").ToString = "Y" Then
                                    newUserRow("MIS_Seed_Member") = "Y"
                                Else
                                    newUserRow("MIS_Seed_Member") = "N"
                                End If
                                newUserRow("Job_Function") = ""

                                dsUseList.Rows.Add(newUserRow)

                            End If
                        End If
                    End If
                Next

                gvUserList.DataSource = dsUseList
                gvUserList.DataMember = dsUseList.TableName
                gvUserList.DataBind()
            End If
        Catch ex As Exception
            Me.lblMsg.Text = "ShowApplyTo: " & ex.Message.ToString
        End Try
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[AccessStation],[CurrencyCode],[AssetStatus], [WarrantyType]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"
                MyData.Tables(1).TableName = "CurrencyCode"

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)

                If Session("DA_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("DA_StationID").ToString
                    GetStationLocation(ddlStation.SelectedValue.ToString)
                End If

                Dim dtViewCurrency As DataView = clsCF.GetCurrency()

                ddlCurrencyCode.DataSource = dtViewCurrency
                ddlCurrencyCode.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlCurrencyCode.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlCurrencyCode.DataBind()

                ddlOthCurrencyCode.DataSource = dtViewCurrency.Table
                ddlOthCurrencyCode.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlOthCurrencyCode.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlOthCurrencyCode.DataBind()

                StationCurrency()

                ddlStatus.DataSource = MyData.Tables(2)
                ddlStatus.DataValueField = MyData.Tables(2).Columns("StatusID").ToString
                ddlStatus.DataTextField = MyData.Tables(2).Columns("Status").ToString
                ddlStatus.DataBind()

                ddlWarranty.DataSource = MyData.Tables(3)
                ddlWarranty.DataValueField = MyData.Tables(3).Columns("WarrantyID").ToString
                ddlWarranty.DataTextField = MyData.Tables(3).Columns("WarrantyType").ToString
                ddlWarranty.DataBind()

                Dim dtAssetClassCOA As New DataView
                dtAssetClassCOA = clsCF.GetCOAList("1")
                ddlAssetClassCOA.DataSource = dtAssetClassCOA
                ddlAssetClassCOA.DataValueField = dtAssetClassCOA.Table.Columns("COA").ToString
                ddlAssetClassCOA.DataTextField = dtAssetClassCOA.Table.Columns("COADesc").ToString
                ddlAssetClassCOA.DataBind()

                Dim dtDepreciationCOA As New DataView
                dtDepreciationCOA = clsCF.GetCOAList("2")
                ddlAccDepreciationCOA.DataSource = dtDepreciationCOA
                ddlAccDepreciationCOA.DataValueField = dtDepreciationCOA.Table.Columns("COA").ToString
                ddlAccDepreciationCOA.DataTextField = dtDepreciationCOA.Table.Columns("COADesc").ToString
                ddlAccDepreciationCOA.DataBind()




                

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub BindBrand()
        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByID")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[BrandCategory]"
                    .Parameters.Add("@ID", SqlDbType.Int)
                    If hdfParentCategoryID.Value.ToString = "" Then
                        .Parameters("@ID").Value = 0
                    Else
                        .Parameters("@ID").Value = hdfParentCategoryID.Value.ToString
                    End If

                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "Brand"

                ddlBrand.DataSource = MyData
                ddlBrand.DataValueField = MyData.Tables(0).Columns("BrandID").ToString()
                ddlBrand.DataTextField = MyData.Tables(0).Columns("BrandName").ToString
                ddlBrand.DataBind()

                If ddlBrand.Items.Count = 1 Then
                    txtBrand.Enabled = True
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "BindBrand: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub GetMainAssetDetail()
        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetMainAssetDetail")

                With MyCommand

                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = hdfMainAssetID.Value.ToString
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(1).TableName = "AssetDetail"

                ddlLocation.DataSource = MyData.Tables(0)
                ddlLocation.DataValueField = MyData.Tables(0).Columns("LocationID").ToString
                ddlLocation.DataTextField = MyData.Tables(0).Columns("Location").ToString
                ddlLocation.DataBind()

                If MyData.Tables(1).Rows(0)("ApplyNewID").ToString = "" Then
                    btnAddQuoReceipt.Visible = True
                    fuBrowse.Visible = True
                    btnAddQuoReceipt.Enabled = True
                    fuBrowse.Enabled = True
                Else
                    btnAddQuoReceipt.Visible = False
                    fuBrowse.Visible = False
                End If

                ddlStation.SelectedValue = MyData.Tables(1).Rows(0)("StationID").ToString

                ddlLocation.SelectedValue = MyData.Tables(1).Rows(0)("LocationID").ToString


                ddlCondition.SelectedValue = MyData.Tables(1).Rows(0)("Condition").ToString
                txtVendorRefNo.Text = MyData.Tables(1).Rows(0)("Vendor_RefNo").ToString

                ddlStatus.SelectedValue = MyData.Tables(1).Rows(0)("AssetStatusID").ToString

                If MyData.Tables(1).Rows(0)("Disable_AssetStatus").ToString() = 1 Then
                    ddlStatus.Enabled = True
                Else
                    ddlStatus.Enabled = False
                End If


                lblVendorPayee.Text = MyData.Tables(1).Rows(0)("VendorPayeeName").ToString
                hdfVendorPayeeID.Value = MyData.Tables(1).Rows(0)("VendorPayeeID").ToString
                hdfVendorPayeeType.Value = MyData.Tables(1).Rows(0)("VendorPayeeType").ToString
                hdfVendorDetail.Value = MyData.Tables(1).Rows(0)("VendorPayeeDetail").ToString

                If hdfVendorPayeeType.Value.ToString = "User" Then
                    txtShopName.Visible = True
                    txtShopName.Text = MyData.Tables(1).Rows(0)("ShopName").ToString
                    btnVendorPayee.Enabled = False
                ElseIf hdfVendorPayeeType.Value.ToString = "Vendor" Then
                    txtShopName.Visible = False
                    txtShopName.Text = ""
                    btnVendorPayee.Enabled = True
                Else
                    txtShopName.Text = ""
                    txtShopName.Visible = False
                    btnVendorPayee.Enabled = True
                End If


                If MyData.Tables(1).Rows(0)("RequiredDate").ToString <> "" Then
                    cldRequiredDate.SelectedDate = Format(Convert.ToDateTime(MyData.Tables(1).Rows(0)("RequiredDate").ToString), "dd MMM yyyy")
                Else
                    cldRequiredDate.Clear()
                End If

                If MyData.Tables(1).Rows(0)("PurchaseCurrency").ToString <> "" Then
                    ddlCurrencyCode.SelectedValue = MyData.Tables(1).Rows(0)("PurchaseCurrency").ToString
                End If
                txtCurrencyRate.Text = MyData.Tables(1).Rows(0)("PurchaseCurrencyRate").ToString

                If txtPurchaseCost.Text.ToString <> "" And txtCurrencyRate.Text.ToString <> "" Then
                    txtCRQuoted.Text = Format(CDec(txtPurchaseCost.Text.ToString) / CDec(txtCurrencyRate.Text.ToString), "0.0000")
                End If
                If txtPurchaseBudgeted.Text.ToString <> "" And txtCurrencyRate.Text.ToString <> "" Then
                    txtCRBudgeted.Text = Format(CDec(txtPurchaseBudgeted.Text.ToString) / CDec(txtCurrencyRate.Text.ToString), "0.0000")
                End If

                hdfLocationApprovedBy.Value = MyData.Tables(1).Rows(0)("LocationApprovedBy").ToString

                ShowApplyTo(hdfLocationApprovedBy.Value, "LocationApprovedBy")
                If hdfLocationApprovedBy.Value.ToString <> "" Then
                    btnLocApprovedBy.Enabled = False
                Else
                    btnLocApprovedBy.Enabled = True
                End If

                hdfCCParty.Value = MyData.Tables(1).Rows(0)("eMailCCParty").ToString
                ShowApplyTo(hdfCCParty.Value.ToString, "CCParty")


                If MyData.Tables(1).Rows(0)("PurchaseDate").ToString <> "" Then
                    cldPurchaseDate.SelectedDate = Format(Convert.ToDateTime(MyData.Tables(1).Rows(0)("PurchaseDate").ToString), "dd MMM yyyy")
                Else
                    cldPurchaseDate.Clear()
                End If

                If MyData.Tables(1).Rows(0)("OthChargesCurrency").ToString <> "" Then
                    ddlOthCurrencyCode.SelectedValue = MyData.Tables(1).Rows(0)("OthChargesCurrency").ToString
                End If

                hdfCreatedBy.Value = MyData.Tables(1).Rows(0)("CreatedBy").ToString
                lblCreatedBy.Text = MyData.Tables(1).Rows(0)("CreatedByName").ToString
                lblCreatedOn.Text = MyData.Tables(1).Rows(0)("CreatedOn").ToString

                gvQuotationReceipt.DataSource = MyData
                gvQuotationReceipt.DataMember = MyData.Tables(2).TableName
                gvQuotationReceipt.DataBind()

                gvParentAsset.DataSource = MyData
                gvParentAsset.DataMember = MyData.Tables(1).TableName
                gvParentAsset.DataBind()



                If MyData.Tables(1).Rows(0)("CreatedBy").ToString <> Session("DA_UserID").ToString Then
                    If Session("blnModifyOthers") = False Then
                        btnSave.Enabled = False
                    Else
                        btnSave.Enabled = True
                    End If
                End If



            Catch ex As Exception
                Me.lblMsg.Text = "GetMainAssetDetail: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using

    End Sub

    Private Sub GetAssetSubData()
        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetAssetDetail")

                With MyCommand

                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(1).TableName = "AssetDetail"


               

            Catch ex As Exception
                Me.lblMsg.Text = "GetAssetDetail: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Function DataValidation() As Boolean
        DataValidation = True

        If ddlStation.SelectedValue.ToString = "" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Station.')</script>")
            ddlStation.Focus()
            Exit Function
        End If
        If ddlLocation.SelectedValue.ToString = "" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Location.')</script>")
            ddlLocation.Focus()
            Exit Function
        End If

        If ddlUserType.SelectedValue.ToString <> "Select One" Then

            If ddlUserType.SelectedValue.ToString = "User" Then
                If gvUserList.Rows.Count <= 0 Then
                    DataValidation = False
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please Select Owner.')</script>")
                    btnAddUser.Focus()
                    Exit Function
                End If

                'If ddlJobFunction.SelectedValue.ToString = "" Then
                '    DataValidation = False
                '    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please Select Job Function.')</script>")
                '    ddlJobFunction.Focus()
                '    Exit Function
                'End If
            Else
                If txtOwner.Text.ToString = "" Then
                    DataValidation = False
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Owner.')</script>")
                    txtOwner.Focus()
                    Exit Function
                End If
            End If
        End If

        If lblCategory.Text.ToString = "" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please Select Category.')</script>")
            btnCategory.Focus()
            Exit Function
        End If

        If ddlCondition.SelectedValue.ToString = "" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Condition.')</script>")
            ddlCondition.Focus()
            Exit Function
        End If

        If txtAvailInInventory.Text.ToString = "" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Available In Inventory.')</script>")
            txtAvailInInventory.Focus()
            Exit Function
        End If

        If txtQuantity.Text.ToString = "" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Quantity.')</script>")
            txtQuantity.Focus()
            Exit Function
        End If

        If txtDepreciationPeriod.Text.ToString = "" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Depreciation Period.')</script>")
            txtDepreciationPeriod.Focus()
            Exit Function
        End If


        If ddlCurrencyCode.SelectedValue.ToString = "0" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Purchase Cost Currency.')</script>")
            ddlCurrencyCode.Focus()
            Exit Function
        End If

        If txtPurchaseCost.Text.ToString = "" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Purchase Cost.')</script>")
            txtPurchaseCost.Focus()
            Exit Function
        End If

        If txtResidualValue.Text.ToString = "" Then
            txtResidualValue.Text = "0.00"
            'DataValidation = False
            'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Residual Value.')</script>")
            'txtResidualValue.Focus()
            'Exit Function
        End If

        If txtNetBookValue.Text.ToString = "" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter NetBook Value.')</script>")
            txtNetBookValue.Focus()
            Exit Function
        End If



        Return DataValidation

    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        
    End Sub

    Private Function SaveAssetData(ByVal strAction As String) As String
        Using sqlConn As New SqlConnection(strConnectionString)

            Dim strReturnMsg As String = ""
            Dim strMsg As String = ""

            Dim strPurchaseDate As String = ""
            Dim strDepreciationEffective As String = ""
            Dim strObsoleteDate As String = ""
            Dim strDisposalDate As String = ""
            Dim strWriteoffDate As String = ""
            Dim strRequiredDate As String = ""
            Dim StrNewAssetID As String = ""
            Dim strChildAssetList As String = ""
            Dim strUserList As String = ""

            strPurchaseDate = Format(cldPurchaseDate.SelectedDate, "dd MMM yyyy").ToString
            If strPurchaseDate.Contains("01 Jan 0001") = True Then
                strPurchaseDate = ""
            End If

            strDepreciationEffective = Format(cldDepreciationEffective.SelectedDate, "dd MMM yyyy").ToString
            If strDepreciationEffective.Contains("01 Jan 0001") = True Then
                strDepreciationEffective = ""
            End If

            strObsoleteDate = Format(cldObsoleteDate.SelectedDate, "dd MMM yyyy").ToString
            If strObsoleteDate.Contains("01 Jan 0001") = True Then
                strObsoleteDate = ""
            End If


            strDisposalDate = Format(cldDisposalDate.SelectedDate, "dd MMM yyyy").ToString
            If strDisposalDate.Contains("01 Jan 0001") = True Then
                strDisposalDate = ""
            End If

            strWriteoffDate = Format(cldWriteoffDate.SelectedDate, "dd MMM yyyy").ToString
            If strWriteoffDate.Contains("01 Jan 0001") = True Then
                strWriteoffDate = ""
            End If


            strRequiredDate = Format(cldRequiredDate.SelectedDate, "dd MMM yyyy").ToString
            If strRequiredDate.Contains("01 Jan 0001") = True Then
                strRequiredDate = ""
            End If

            If txtVAT.Text.ToString = "" Then
                txtVAT.Text = "0.00"
            End If

            If txtDiscount.Text.ToString = "" Then
                txtDiscount.Text = "0.00"
            End If

            'If strAction = "Save" Then
            '    strUserList = ""
            'Else
            Dim i As Integer
            Dim strUser As String = ""
            Dim lbtnUserID_gv As New LinkButton
            Dim lblJobGrade_gv As New Label
            Dim lblInternalAudit_gv As New Label
            Dim lblMISSeedMember_gv As New Label
            Dim ddlJobFunction_gv As New DropDownList
            Dim iInternalAudit As Integer = 0
            Dim iMISSeedMember As Integer = 0

            strUserList = ""

            For i = 0 To gvUserList.Rows.Count - 1
                strUser = ""
                lbtnUserID_gv = gvUserList.Rows(i).Cells(0).FindControl("lbtnRemove")
                lblJobGrade_gv = gvUserList.Rows(i).Cells(2).FindControl("lblJobGrade")
                lblInternalAudit_gv = gvUserList.Rows(i).Cells(3).FindControl("lblInternalAudit")
                lblMISSeedMember_gv = gvUserList.Rows(i).Cells(4).FindControl("lblMISSeedMember")
                ddlJobFunction_gv = gvUserList.Rows(i).Cells(5).FindControl("ddlJobFunction")

                If lblInternalAudit_gv.Text.ToString = "Y" Then
                    iInternalAudit = 1
                End If
                If lblMISSeedMember_gv.Text.ToString = "Y" Then
                    iMISSeedMember = 1
                End If

                strUser = lbtnUserID_gv.CommandArgument.ToString() + "<" + lblJobGrade_gv.Text.ToString + ">" + iInternalAudit.ToString + "^" + iMISSeedMember.ToString + "~" + ddlJobFunction_gv.SelectedValue.ToString()
                If strUserList = "" Then
                    strUserList = strUser + "|"
                Else
                    strUserList = strUserList + strUser + "|"
                End If
            Next
            'End If

            Try
                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveSubAssetDetail", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@LocationID", SqlDbType.BigInt)
                    .Parameters("@LocationID").Value = ddlLocation.SelectedValue.ToString



                    .Parameters.Add("@OwnerType", SqlDbType.VarChar, 50)
                    .Parameters("@OwnerType").Value = ddlUserType.SelectedValue.ToString
                    .Parameters.Add("@OwnerID", SqlDbType.NVarChar, 100)
                    If ddlUserType.SelectedValue.ToString = "User" Then
                        .Parameters("@OwnerID").Value = hdfOwnerUserID.Value.ToString
                    Else
                        .Parameters("@OwnerID").Value = txtOwner.Text.ToString
                    End If
                    '.Parameters.Add("@Internal_Audit", SqlDbType.TinyInt)
                    'If chkInternalAudit.Checked = True Then
                    '    .Parameters("@Internal_Audit").Value = 1
                    'Else
                    '    .Parameters("@Internal_Audit").Value = 0
                    'End If

                    '.Parameters.Add("@MIS_Seed_Member", SqlDbType.TinyInt)
                    'If chkMIS_Seed_Member.Checked = True Then
                    '    .Parameters("@MIS_Seed_Member").Value = 1
                    'Else
                    '    .Parameters("@MIS_Seed_Member").Value = 0
                    'End If
                    '.Parameters.Add("@Job_Function", SqlDbType.NVarChar, 100)
                    '.Parameters("@Job_Function").Value = ddlJobFunction.SelectedValue.ToString

                    .Parameters.Add("@CategoryID", SqlDbType.BigInt)
                    .Parameters("@CategoryID").Value = CInt(hdfCategoryID.Value.ToString)

                    .Parameters.Add("@Brand", SqlDbType.NVarChar, 50)
                    If ddlBrand.SelectedValue.ToString <> "0" Then
                        .Parameters("@Brand").Value = ddlBrand.SelectedValue.ToString
                    Else
                        .Parameters("@Brand").Value = txtBrand.Text.ToString
                    End If


                    .Parameters.Add("@Model", SqlDbType.NVarChar, 100)
                    .Parameters("@Model").Value = txtModel.Text.ToString
                    .Parameters.Add("@Application_ToBeUsed", SqlDbType.NVarChar)
                    .Parameters("@Application_ToBeUsed").Value = txtAppToBeUsed.Text.ToString
                    .Parameters.Add("@Description", SqlDbType.NVarChar)
                    .Parameters("@Description").Value = txtDescription.Text.ToString
                    .Parameters.Add("@AssetNo", SqlDbType.NVarChar, 50)
                    .Parameters("@AssetNo").Value = txtAssetNo.Text.ToString
                    .Parameters.Add("@IP_Address", SqlDbType.NVarChar, 15)
                    .Parameters("@IP_Address").Value = txtIPAddress.Text.ToString
                    .Parameters.Add("@SerialNo", SqlDbType.NVarChar, 50)
                    .Parameters("@SerialNo").Value = txtSerial.Text.ToString
                    .Parameters.Add("@Condition", SqlDbType.VarChar, 5)
                    .Parameters("@Condition").Value = ddlCondition.SelectedValue.ToString
                    .Parameters.Add("@Warranty", SqlDbType.Int)
                    If txtWarranty.Text.ToString = "" Then
                        .Parameters("@Warranty").Value = DBNull.Value
                    Else
                        .Parameters("@Warranty").Value = CInt(txtWarranty.Text.ToString)
                    End If
                    .Parameters.Add("@WarrantyType", SqlDbType.Int)
                    If CInt(ddlWarranty.SelectedValue.ToString) = 0 Then
                        .Parameters("@WarrantyType").Value = DBNull.Value
                    Else
                        .Parameters("@WarrantyType").Value = CInt(ddlWarranty.SelectedValue.ToString)
                    End If
                    .Parameters.Add("@Vendor_RefNo", SqlDbType.NVarChar, 100)
                    .Parameters("@Vendor_RefNo").Value = txtVendorRefNo.Text.ToString

                    .Parameters.Add("@AssetStatusID", SqlDbType.Int)
                    If CInt(ddlStatus.SelectedValue.ToString) = 0 Then
                        .Parameters("@AssetStatusID").Value = DBNull.Value
                    Else
                        .Parameters("@AssetStatusID").Value = CInt(ddlStatus.SelectedValue.ToString)
                    End If

                    .Parameters.Add("@VendorPayeeID", SqlDbType.NVarChar, 50)
                    .Parameters("@VendorPayeeID").Value = hdfVendorPayeeID.Value.ToString
                    .Parameters.Add("@VendorPayeeType", SqlDbType.NVarChar, 50)
                    .Parameters("@VendorPayeeType").Value = hdfVendorPayeeType.Value.ToString
                    .Parameters.Add("@ShopName", SqlDbType.NVarChar, 500)
                    .Parameters("@ShopName").Value = txtShopName.Text.ToString

                    .Parameters.Add("@Warranty_Period", SqlDbType.Decimal, 18, 2)
                    If txtWarrantyPeriod.Text.ToString = "" Then
                        .Parameters("@Warranty_Period").Value = DBNull.Value
                    Else
                        .Parameters("@Warranty_Period").Value = CDec(txtWarrantyPeriod.Text.ToString)
                    End If

                    .Parameters.Add("@Availability_In_Inventory", SqlDbType.Int)
                    If txtAvailInInventory.Text.ToString = "" Then
                        .Parameters("@Availability_In_Inventory").Value = DBNull.Value
                    Else
                        .Parameters("@Availability_In_Inventory").Value = CInt(txtAvailInInventory.Text.ToString)
                    End If

                    .Parameters.Add("@Remarks", SqlDbType.NVarChar)
                    .Parameters("@Remarks").Value = txtRemarks.Text.ToString


                    .Parameters.Add("@PurchaseCurrency", SqlDbType.Int)
                    If CInt(ddlCurrencyCode.SelectedValue.ToString) = 0 Then
                        .Parameters("@PurchaseCurrency").Value = DBNull.Value
                    Else
                        .Parameters("@PurchaseCurrency").Value = CInt(ddlCurrencyCode.SelectedValue.ToString)
                    End If

                    .Parameters.Add("@PurchaseCost", SqlDbType.Decimal, 18, 2)
                    If txtPurchaseCost.Text.ToString = "" Then
                        .Parameters("@PurchaseCost").Value = DBNull.Value
                    Else
                        .Parameters("@PurchaseCost").Value = CDec(txtPurchaseCost.Text.ToString)
                    End If

                    .Parameters.Add("@BudgetAmount", SqlDbType.Decimal, 18, 2)
                    If txtPurchaseBudgeted.Text.ToString = "" Then
                        .Parameters("@BudgetAmount").Value = DBNull.Value
                    Else
                        .Parameters("@BudgetAmount").Value = CDec(txtPurchaseBudgeted.Text.ToString)
                    End If

                    .Parameters.Add("@PurchaseCurrencyRate", SqlDbType.Decimal, 18, 2)
                    If txtCurrencyRate.Text.ToString = "" Then
                        .Parameters("@PurchaseCurrencyRate").Value = DBNull.Value
                    Else
                        .Parameters("@PurchaseCurrencyRate").Value = CDec(txtCurrencyRate.Text.ToString)
                    End If

                    .Parameters.Add("@LocationApprovedBy", SqlDbType.NVarChar)
                    .Parameters("@LocationApprovedBy").Value = hdfLocationApprovedBy.Value.ToString.Replace(",,", ",")

                    .Parameters.Add("@eMailCCParty", SqlDbType.NVarChar)
                    .Parameters("@eMailCCParty").Value = hdfCCParty.Value.ToString.Replace(",,", ",")


                    .Parameters.Add("@PurchaseDate", SqlDbType.NVarChar, 20)
                    .Parameters("@PurchaseDate").Value = strPurchaseDate

                    .Parameters.Add("@Depreciation_Period", SqlDbType.Decimal, 18, 2)
                    If txtDepreciationPeriod.Text.ToString = "" Then
                        .Parameters("@Depreciation_Period").Value = DBNull.Value
                    Else
                        .Parameters("@Depreciation_Period").Value = CDec(txtDepreciationPeriod.Text.ToString)
                    End If

                    .Parameters.Add("@No_Of_Units", SqlDbType.Int)
                    If txtQuantity.Text.ToString = "" Then
                        .Parameters("@No_Of_Units").Value = DBNull.Value
                    Else
                        .Parameters("@No_Of_Units").Value = CInt(txtQuantity.Text.ToString)
                    End If
                    .Parameters.Add("@Units", SqlDbType.NVarChar, 50)
                    .Parameters("@Units").Value = txtUnit.Text.ToString

                    .Parameters.Add("@OthChargesCurrency", SqlDbType.Int)
                    If CInt(ddlOthCurrencyCode.SelectedValue.ToString) = 0 Then
                        .Parameters("@OthChargesCurrency").Value = DBNull.Value
                    Else
                        If txtOthCharges.Text.ToString = "" Then
                            .Parameters("@OthChargesCurrency").Value = DBNull.Value
                        Else
                            .Parameters("@OthChargesCurrency").Value = CInt(ddlOthCurrencyCode.SelectedValue.ToString)
                        End If

                    End If

                    .Parameters.Add("@OthersCharges", SqlDbType.Decimal, 18, 2)
                    If txtOthCharges.Text.ToString = "" Then
                        .Parameters("@OthersCharges").Value = DBNull.Value
                    Else
                        .Parameters("@OthersCharges").Value = CDec(txtOthCharges.Text.ToString)
                    End If


                    .Parameters.Add("@AssetClass_COA", SqlDbType.BigInt)
                    If ddlAssetClassCOA.SelectedValue.ToString = "" Then
                        .Parameters("@AssetClass_COA").Value = DBNull.Value
                    Else
                        .Parameters("@AssetClass_COA").Value = CInt(ddlAssetClassCOA.SelectedValue.ToString)
                    End If

                    .Parameters.Add("@Depreciation_Effective", SqlDbType.NVarChar, 20)
                    .Parameters("@Depreciation_Effective").Value = strDepreciationEffective

                    .Parameters.Add("@Depreciation_Month", SqlDbType.Decimal, 18, 2)
                    If txtDepreciationMonth.Text.ToString = "" Then
                        .Parameters("@Depreciation_Month").Value = DBNull.Value
                    Else
                        .Parameters("@Depreciation_Month").Value = CDec(txtDepreciationMonth.Text.ToString)
                    End If

                    .Parameters.Add("@Accumulated_Depreciation", SqlDbType.Decimal, 18, 2)
                    If txtAccDepreciation.Text.ToString = "" Then
                        .Parameters("@Accumulated_Depreciation").Value = DBNull.Value
                    Else
                        .Parameters("@Accumulated_Depreciation").Value = CDec(txtAccDepreciation.Text.ToString)
                    End If

                    .Parameters.Add("@Depreciation_Expense_COA", SqlDbType.BigInt)
                    If ddlDepExpCOA.SelectedValue.ToString = "" Then
                        .Parameters("@Depreciation_Expense_COA").Value = DBNull.Value
                    Else
                        .Parameters("@Depreciation_Expense_COA").Value = CInt(ddlDepExpCOA.SelectedValue.ToString)
                    End If

                    .Parameters.Add("@Accumulated_Depreciation_COA", SqlDbType.BigInt)
                    If ddlAccDepreciationCOA.SelectedValue.ToString = "" Then
                        .Parameters("@Accumulated_Depreciation_COA").Value = DBNull.Value
                    Else
                        .Parameters("@Accumulated_Depreciation_COA").Value = CInt(ddlAccDepreciationCOA.SelectedValue.ToString)
                    End If

                    .Parameters.Add("@ResidualValue", SqlDbType.Decimal, 18, 2)
                    If txtResidualValue.Text.ToString = "" Then
                        .Parameters("@ResidualValue").Value = DBNull.Value
                    Else
                        .Parameters("@ResidualValue").Value = CDec(txtResidualValue.Text.ToString)
                    End If

                    .Parameters.Add("@NetBookValue", SqlDbType.Decimal, 18, 2)
                    If txtNetBookValue.Text.ToString = "" Then
                        .Parameters("@NetBookValue").Value = DBNull.Value
                    Else
                        .Parameters("@NetBookValue").Value = CDec(txtNetBookValue.Text.ToString)
                    End If

                    .Parameters.Add("@Obsolate_Date", SqlDbType.NVarChar, 20)
                    .Parameters("@Obsolate_Date").Value = strObsoleteDate

                    .Parameters.Add("@WriteOff_Date", SqlDbType.NVarChar, 20)
                    .Parameters("@WriteOff_Date").Value = strWriteoffDate

                    .Parameters.Add("@Disposal_Date", SqlDbType.NVarChar, 20)
                    .Parameters("@Disposal_Date").Value = strDisposalDate


                    .Parameters.Add("@Required_Date", SqlDbType.NVarChar, 20)
                    .Parameters("@Required_Date").Value = strRequiredDate
                    .Parameters.Add("@ChildAssetList", SqlDbType.NVarChar)
                    .Parameters("@ChildAssetList").Value = strChildAssetList
                    .Parameters.Add("@OthersInfo", SqlDbType.NVarChar)
                    .Parameters("@OthersInfo").Value = txtOthersInfo.Text.ToString

                    .Parameters.Add("@VAT", SqlDbType.Decimal, 18, 2)
                    .Parameters("@VAT").Value = txtVAT.Text.ToString

                    .Parameters.Add("@Discount", SqlDbType.Decimal, 18, 2)
                    .Parameters("@Discount").Value = txtDiscount.Text.ToString

                    .Parameters.Add("@UserList", SqlDbType.NVarChar)
                    .Parameters("@UserList").Value = strUserList

                    .Parameters.Add("@SplitRecord", SqlDbType.TinyInt)
                    If chkSplit.Checked = True Then
                        .Parameters("@SplitRecord").Value = 1
                    Else
                        .Parameters("@SplitRecord").Value = 0
                    End If

                    .Parameters.Add("@MainAssetID", SqlDbType.VarChar, 20)
                    .Parameters("@MainAssetID").Value = hdfMainAssetID.Value.ToString()
                    .Parameters.Add("@Specification", SqlDbType.NVarChar)
                    .Parameters("@Specification").Value = txtSpecification.Text.ToString()


                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    '.Parameters.Add("@NewAssetID", SqlDbType.VarChar, 20)
                    '.Parameters("@NewAssetID").Direction = ParameterDirection.Output

                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                    'StrNewAssetID = .Parameters("@NewAssetID").Value.ToString()
                End With

                If strMsg <> "" Then
                    strReturnMsg = strMsg
                    'lblMsg.Text = strMsg
                    'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                    'Else

                    '    Clear()

                    '    'strReturnMsg = "Save Successful"
                    '    Dim ds As DataSet = GetSubAsset("")

                    '    gvSubAssetTemp.DataSource = ds
                    '    gvSubAssetTemp.DataMember = ds.Tables(0).TableName
                    '    gvSubAssetTemp.DataBind()


                    '    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                End If

                Return strReturnMsg
            Catch ex As Exception
                Return ex.Message.ToString
                'Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using

    End Function

    'Protected Sub btnOwner_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOwner.Click
    '    Dim strScript As String

    '    If hdfOwnerUserID.Value.ToString = "" Then
    '        Session("DA_USERLIST") = ""
    '        Session("DA_New_USERLIST") = ""
    '        Session("DA_USERLIST_Name") = ""
    '        Session("DA_USERLIST_TYPE") = "Owner"
    '    Else
    '        Session("DA_USERLIST_TYPE") = "Owner"
    '        Session("DA_USERLIST") = hdfOwnerUserID.Value.ToString
    '        Session("DA_USERLIST_Name") = txtOwner.Text.ToString
    '    End If

    '    Session("DA_AddUserStationID") = ""
    '    If ddlStation.SelectedValue.ToString <> "" Then
    '        Session("DA_AddUserStationID") = ddlStation.SelectedValue.ToString
    '    Else
    '        Session("DA_AddUserStationID") = Session("DA_StationID").ToString
    '    End If


    '    strScript = "<script language=javascript>"
    '    strScript += "theChild = window.open('../frmAddUser2.aspx?ParentForm=frmAssetDetail','AddOwner','height=550, width=550,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
    '    strScript += "</script>"
    '    Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)

    'End Sub

    Protected Sub ddlUserType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUserType.SelectedIndexChanged
        btnAddUser.Visible = False
        txtOwner.Visible = False
        If ddlUserType.SelectedValue = "User" Then
            btnAddUser.Visible = True
            txtOwner.Text = ""
            txtOwner.Visible = False
            gvUserList.Visible = True
            'If txtAssetID.Text.ToString = "" Then
            '    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter owner at User  List.')</script>")
            '    btnOwner.Enabled = False
            '    txtOwner.Enabled = False
            '    txtOwner.Text = ""
            '    hdfOwnerUserID.Value = ""
            'Else
            '    btnOwner.Enabled = True
            '    txtOwner.Enabled = False
            '    txtOwner.Text = ""
            '    hdfOwnerUserID.Value = ""
            'End If
        ElseIf ddlUserType.SelectedValue = "Office" Then
            txtOwner.Visible = True
            txtOwner.Enabled = True
            'btnOwner.Enabled = False
            txtOwner.Text = ""
            hdfOwnerUserID.Value = ""
            hdfUserList.Value = ""

            dsUseList.Clear()
            gvUserList.DataSource = dsUseList
            gvUserList.DataMember = dsUseList.TableName
            gvUserList.DataBind()
            gvUserList.Visible = False
            'Else
            '    txtOwner.Enabled = False
            '    btnOwner.Enabled = False
            '    txtOwner.Text = ""
            '    hdfOwnerUserID.Value = ""

            '    'lblMsg.Text = "Please select User Type."
            '    Exit Sub
        End If
    End Sub

    Protected Sub btnCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCategory.Click


        Dim strScript As String

        Session("DA_CategoryDepreciation_Period") = ""
        Session("DA_ParentCategoryID") = ""
        Session("DA_CategoryID") = ""
        Session("DA_Category") = ""

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmItem.aspx?ParentForm=frmSubAssetDetail','AddCategory','height=550, width=550,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub btnVendorPayee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVendorPayee.Click
        Dim strScript As String

        If hdfVendorPayeeID.Value.ToString = "" Then
            Session("DA_USERLIST") = ""
            Session("DA_New_USERLIST") = ""
            Session("DA_USERLIST_Name") = ""
            Session("DA_USERLIST_TYPE") = "VendorPayee"
        Else
            Session("DA_USERLIST_TYPE") = "VendorPayee"
            Session("DA_USERLIST") = hdfVendorPayeeID.Value.ToString
            Session("DA_USERLIST_Name") = lblVendorPayee.Text.ToString
        End If

        Session("DA_AddUserStationID") = ""
        Session("DA_AddUserStationID") = Session("DA_StationID").ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmPayee.aspx?ParentForm=frmAssetDetail','AddVendorPayee','height=550, width=550,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)



    End Sub

    Protected Sub btnLocApprovedBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLocApprovedBy.Click
        Dim strScript As String

        If hdfLocationApprovedBy.Value.ToString = "" Then
            Session("DA_USERLIST") = ""
            Session("DA_New_USERLIST") = ""
            Session("DA_USERLIST_Name") = ""
            Session("DA_USERLIST_TYPE") = "LocationApprovedBy"
        Else
            Session("DA_USERLIST_TYPE") = "LocationApprovedBy"
            Session("DA_USERLIST") = hdfLocationApprovedBy.Value.ToString
            Session("DA_USERLIST_Name") = lblLocationApprovedBy.Text.ToString
        End If

        Session("DA_AddUserStationID") = ""
        Session("DA_AddUserStationID") = Session("DA_StationID").ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmAddUser2.aspx?ParentForm=frmAssetDetail','AddLocationApprovedBy','height=550, width=550,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)

    End Sub

    Protected Sub btnCCParty_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCCParty.Click
        Dim strScript As String

        If hdfCCParty.Value.ToString = "" Then
            Session("DA_USERLIST") = ""
            Session("DA_New_USERLIST") = ""
            Session("DA_USERLIST_Name") = ""
            Session("DA_USERLIST_TYPE") = "CCParty"
        Else
            Session("DA_USERLIST_TYPE") = "CCParty"
            Session("DA_USERLIST") = hdfCCParty.Value.ToString
            Session("DA_USERLIST_Name") = lblCCParty.Text.ToString
        End If

        Session("DA_AddUserStationID") = ""
        Session("DA_AddUserStationID") = Session("DA_StationID").ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmAddUser.aspx?ParentForm=frmSubAssetDetail','AddCCParty','height=550, width=550,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)

    End Sub

    Protected Sub ddlStation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStation.SelectedIndexChanged
        GetStationLocation(ddlStation.SelectedValue.ToString)
        StationCurrency()
    End Sub

    Private Sub StationCurrency()
        clsCF.StationCurrencyID(ddlStation.SelectedValue.ToString, ddlCurrencyCode)
        clsCF.StationCurrencyID(ddlStation.SelectedValue.ToString, ddlOthCurrencyCode)
        'ddlCurrencyCode.SelectedValue = clsCF.StationCurrencyID(ddlStation.SelectedValue.ToString)
        'ddlOthCurrencyCode.SelectedValue = clsCF.StationCurrencyID(ddlStation.SelectedValue.ToString)
    End Sub

    Private Sub GetStationLocation(ByVal strStation As String)
        Try
            If strStation = "" Then
                lblMsg.Text = "Please select Station"
                Exit Sub
            End If

            Dim dsLocation As DataSet = clsCF.GetStationLocation(strStation)
            ddlLocation.DataSource = dsLocation.Tables(0)
            ddlLocation.DataValueField = dsLocation.Tables(0).Columns("LocationID").ToString
            ddlLocation.DataTextField = dsLocation.Tables(0).Columns("Location").ToString
            ddlLocation.DataBind()

            'Dim dsStation As New DataSet
            'Dim MyData As New DataSet
            'Dim MyAdapter As SqlDataAdapter
            'Dim MyCommand As SqlCommand
            'Dim MyConnection As New SqlConnection(strConnectionString)

            'MyCommand = New SqlCommand("SP_GetReferenceData_ByStation")

            'With MyCommand
            '    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
            '    .Parameters("@ReSM").Value = strReSM
            '    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
            '    .Parameters("@ReferenceType").Value = "[Location]"
            '    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
            '    .Parameters("@StationID").Value = strStation
            '    .CommandType = CommandType.StoredProcedure
            '    .Connection = MyConnection
            'End With

            'MyAdapter = New SqlDataAdapter(MyCommand)
            'MyAdapter.Fill(MyData)
            'MyData.Tables(0).TableName = "Location"
            'ddlLocation.DataSource = MyData.Tables(0)
            'ddlLocation.DataValueField = MyData.Tables(0).Columns("LocationID").ToString
            'ddlLocation.DataTextField = MyData.Tables(0).Columns("Location").ToString
            'ddlLocation.DataBind()

        Catch ex As Exception
            Me.lblMsg.Text = "GetStationLocation: " & ex.Message.ToString
        Finally
            'sqlConn.Close()
        End Try
    End Sub

    Protected Sub btnAddQuoReceipt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddQuoReceipt.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                'Save Quotation File into db
                Dim i As Integer = 0
                Dim strFileName As String = ""
                Dim strFilePath As String = ""
                Dim strMsg As String = ""
                Dim blnContains As Boolean = False

                If fuBrowse.FileName <> "" Then
                    If (Regex.IsMatch(fuBrowse.FileName, strFileExtension, RegexOptions.IgnoreCase)) = False Then
                        lblMsg.Text = "Invalid file format."
                        Exit Sub
                    End If

                    ' Get the size in bytes of the file to upload.
                    Dim fileSize As Integer = fuBrowse.PostedFile.ContentLength

                    ' Allow only files less than 2,100,000 bytes (approximately 2 MB) to be uploaded.
                    'If (fileSize < 2100000) Then
                    If (fileSize < intFileSizeLimitKB) Then

                        clsCF.CheckConnectionState(sqlConn)

                        Dim cmd As New SqlCommand("SP_SaveAssetQuoReceiptFile", sqlConn)
                        With cmd
                            .CommandType = CommandType.StoredProcedure
                            .Parameters.Add("@AssetID", SqlDbType.NVarChar, 50)
                            .Parameters("@AssetID").Value = txtAssetID.Text.ToString

                            .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                            .Parameters("@UserID").Value = Session("DA_UserID").ToString
                            .Parameters.Add("@FileName", SqlDbType.NVarChar, 500)
                            .Parameters("@FileName").Value = clsCF.RenameFileName(fuBrowse.FileName.ToString)
                            .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                            .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                            .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                            .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString

                            .Parameters.Add("@FilePath", SqlDbType.NVarChar, 500)
                            .Parameters("@FilePath").Direction = ParameterDirection.Output
                            .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                            .Parameters("@Msg").Direction = ParameterDirection.Output
                            .CommandTimeout = 0
                            .ExecuteNonQuery()

                            strMsg = .Parameters("@Msg").Value.ToString()
                            strFilePath = .Parameters("@FilePath").Value.ToString()
                        End With

                        If strMsg <> "" Then
                            'lblMsg.Text = strMsg
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                        Else
                            If strFilePath <> "" Then

                                'strAlbumPath = hfServerPath.Value
                                Dim fn As String = ""
                                fn = System.IO.Path.GetFileName(fuBrowse.PostedFile.FileName)

                                If fn <> "" Then
                                    If FileIO.FileSystem.DirectoryExists(strFilePath) = False Then
                                        FileIO.FileSystem.CreateDirectory(strFilePath)
                                    End If

                                    If FileIO.FileSystem.FileExists(strFilePath + "/" + fn) = False Then
                                        fuBrowse.PostedFile.SaveAs(strFilePath + "/" + fn)
                                    Else
                                        FileIO.FileSystem.DeleteFile(strFilePath + "/" + fn)
                                        fuBrowse.PostedFile.SaveAs(strFilePath + "/" + fn)
                                    End If
                                End If
                            End If

                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Add Successful')</script>")
                            GetAssetQuoReceiptFile()
                        End If
                    Else
                        lblMsg.Text = "Your file was not uploaded because it exceeds the " & intFileSizeLimit & " MB size limit."
                    End If
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnAddQuoReceipt_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Private Function CheckUserAsset() As String

        Using sqlConn As New SqlConnection(strConnectionString)

            Dim strMsg As String = ""

            CheckUserAsset = ""

            Try
                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_CheckUserAsset", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString

                    .Parameters.Add("@OwnerType", SqlDbType.NVarChar, 50)
                    .Parameters("@OwnerType").Value = ddlUserType.SelectedValue.ToString
                    .Parameters.Add("@Owner", SqlDbType.NVarChar, 100)
                    .Parameters("@Owner").Value = hdfOwnerUserID.Value.ToString
                    .Parameters.Add("@CategoryID", SqlDbType.BigInt)
                    .Parameters("@CategoryID").Value = CInt(hdfCategoryID.Value.ToString)
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output

                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With
                CheckUserAsset = strMsg

                'If strMsg <> "" Then
                '    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                'End If
                Return CheckUserAsset
            Catch ex As Exception
                Me.lblMsg.Text = "CheckUserAsset: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try

        End Using
    End Function

    Protected Sub ddlBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBrand.SelectedIndexChanged
        If ddlBrand.SelectedValue.ToString = "0" Then
            txtBrand.Enabled = True
            txtBrand.Focus()
        Else
            txtBrand.Text = ""
            txtBrand.Enabled = False
        End If
    End Sub

    Protected Sub gvQuotationReceipt_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvQuotationReceipt.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim intID As Integer = 0
                Dim lbtnDelete As New LinkButton

                lblMsg.Text = ""

                lbtnDelete = gvQuotationReceipt.Rows(e.RowIndex).FindControl("lbtnDelete")
                intID = CInt(lbtnDelete.CommandArgument.ToString)

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteAssetQuoReceiptFile", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                    .Parameters.Add("@DocPicID", SqlDbType.Int)
                    .Parameters("@DocPicID").Value = intID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    GetAssetQuoReceiptFile()
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvQuotationReceipt_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Private Sub GetAssetQuoReceiptFile()
        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetAssetQuoReceiptFile")

                With MyCommand

                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AssetQuoReceiptFile"


                gvQuotationReceipt.DataSource = MyData
                gvQuotationReceipt.DataMember = MyData.Tables(0).TableName
                gvQuotationReceipt.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "GetAssetQuoReceiptFile: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnAddUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddUser.Click
        Dim strScript As String

        If gvUserList.Rows.Count = 0 Then
            Session("DA_USERLIST") = ""
            Session("DA_New_USERLIST") = ""
            Session("DA_USERLIST_TYPE") = "UserList"
        Else
            Dim i As Integer
            Dim strUser As String = ""
            Dim lbtnUserID_gv As New LinkButton
            Dim lblJobGrade_gv As New Label
            Dim lblInternalAudit_gv As New Label
            Dim lblMISSeedMember_gv As New Label
            Dim ddlJobFunction_gv As New DropDownList

            hdfUserList.Value = ""

            For i = 0 To gvUserList.Rows.Count - 1
                strUser = ""
                lbtnUserID_gv = gvUserList.Rows(i).Cells(0).FindControl("lbtnRemove")
                lblJobGrade_gv = gvUserList.Rows(i).Cells(2).FindControl("lblJobGrade")
                lblInternalAudit_gv = gvUserList.Rows(i).Cells(3).FindControl("lblInternalAudit")
                lblMISSeedMember_gv = gvUserList.Rows(i).Cells(4).FindControl("lblMISSeedMember")
                ddlJobFunction_gv = gvUserList.Rows(i).Cells(5).FindControl("ddlJobFunction")


                If dsUseList.Select("OwnerID='" + lbtnUserID_gv.CommandArgument.ToString() + "'").Length > 0 Then
                    Dim myRow() As Data.DataRow
                    myRow = dsUseList.Select("OwnerID='" + lbtnUserID_gv.CommandArgument.ToString() + "'")
                    myRow(0)("Job_Function") = ddlJobFunction_gv.SelectedValue.ToString()
                End If

                If hdfUserList.Value.ToString = "" Then
                    hdfUserList.Value = lbtnUserID_gv.CommandArgument.ToString()
                Else
                    hdfUserList.Value = hdfUserList.Value.ToString + "," + lbtnUserID_gv.CommandArgument.ToString()
                End If
            Next


            Session("DA_USERLIST_TYPE") = "UserList"
            Session("DA_USERLIST") = hdfUserList.Value.ToString
        End If

        Session("DA_AddUserStationID") = ""
        Session("DA_AddUserStationID") = Session("DA_StationID").ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmAddUser.aspx?ParentForm=frmSubAssetDetail','AddUser','height=550, width=550,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub gvUserList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvUserList.SelectedIndexChanged
        Dim strRemove As String = ""

        strRemove = gvUserList.SelectedValue.ToString

        If dsUseList.Select("OwnerID='" + strRemove + "'").Length > 0 Then
            Dim myRow() As Data.DataRow
            myRow = dsUseList.Select("OwnerID='" + strRemove + "'")
            dsUseList.Rows.Remove(myRow(0))
        End If

        Dim i As Integer
        Dim strUser As String = ""
        Dim lbtnUserID_gv As New LinkButton
        Dim lblJobGrade_gv As New Label
        Dim lblInternalAudit_gv As New Label
        Dim lblMISSeedMember_gv As New Label
        Dim ddlJobFunction_gv As New DropDownList

        For i = 0 To gvUserList.Rows.Count - 1
            strUser = ""
            lbtnUserID_gv = gvUserList.Rows(i).Cells(0).FindControl("lbtnRemove")
            lblJobGrade_gv = gvUserList.Rows(i).Cells(2).FindControl("lblJobGrade")
            lblInternalAudit_gv = gvUserList.Rows(i).Cells(3).FindControl("lblInternalAudit")
            lblMISSeedMember_gv = gvUserList.Rows(i).Cells(4).FindControl("lblMISSeedMember")
            ddlJobFunction_gv = gvUserList.Rows(i).Cells(5).FindControl("ddlJobFunction")

            If dsUseList.Select("OwnerID='" + lbtnUserID_gv.CommandArgument.ToString() + "'").Length > 0 Then
                Dim myRow() As Data.DataRow
                myRow = dsUseList.Select("OwnerID='" + lbtnUserID_gv.CommandArgument.ToString() + "'")
                myRow(0)("Job_Function") = ddlJobFunction_gv.SelectedValue.ToString()
            End If
        Next

        gvUserList.DataSource = dsUseList
        gvUserList.DataMember = dsUseList.TableName
        gvUserList.DataBind()

        'strUser = hdfUserList.Value.ToString.ToUpper()
        'strUser = strUser.Replace(strRemove.ToUpper(), "").ToString
        'strUser = strUser.Replace(",,", ",").ToString
        'hdfUserList.Value = strUser
        'Session("DA_USERLIST_TYPE") = "UserList"
        'Session("DA_New_USERLIST") = strUser
        'Session("DA_USERLIST") = strUser
        'ShowApplyTo(Session("DA_USERLIST").ToString, "UserList")
    End Sub

    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        lblMsg.Text = ""
        If DataValidation() = False Then
            Exit Sub
        Else
            Dim strReturnMsg As String = ""
            strReturnMsg = SaveAssetData(btnGenerate.Text.ToString)
            If strReturnMsg <> "" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strReturnMsg + "')</script>")
            Else
                Clear()

                Dim ds As DataSet = GetSubAsset("")
                gvSubAssetTemp.DataSource = ds
                gvSubAssetTemp.DataMember = ds.Tables(0).TableName
                gvSubAssetTemp.DataBind()

                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + btnGenerate.Text.ToString + " Successful')</script>")
            End If
        End If
    End Sub

    Protected Sub gvSubAssetTemp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSubAssetTemp.SelectedIndexChanged


        Dim ds As DataSet = GetSubAsset(gvSubAssetTemp.SelectedValue.ToString())

        txtAssetID.Text = ds.Tables(0).Rows(0)("AssetID").ToString
        ddlStation.SelectedValue = ds.Tables(0).Rows(0)("StationID").ToString
        ddlLocation.SelectedValue = ds.Tables(0).Rows(0)("LocationID").ToString

        txtOwner.Text = ds.Tables(0).Rows(0)("OwnerName").ToString
        hdfOwnerUserID.Value = ds.Tables(0).Rows(0)("OwnerID").ToString

        ddlUserType.SelectedValue = ds.Tables(0).Rows(0)("OwnerType").ToString

        txtOwner.Enabled = False
        btnAddUser.Visible = False

        If ddlUserType.SelectedValue = "User" Then
            btnAddUser.Visible = True
        ElseIf ddlUserType.SelectedValue = "Office" Then
            txtOwner.Enabled = True
        End If

        lblCategory.Text = ds.Tables(0).Rows(0)("Category").ToString
        hdfCategoryID.Value = ds.Tables(0).Rows(0)("CategoryID").ToString
        hdfParentCategory.Value = ds.Tables(0).Rows(0)("ParentCategory").ToString
        If hdfParentCategory.Value.ToString <> "" Then
            lblParentCategory.Text = ds.Tables(0).Rows(0)("ParentCategory").ToString + " - "
            lblParentCategory.Visible = True
        Else
            lblParentCategory.Text = ""
            lblParentCategory.Visible = False
        End If


        hdfParentCategoryID.Value = ds.Tables(0).Rows(0)("ParentCategoryID").ToString
        BindBrand()
        If IsNumeric(ds.Tables(0).Rows(0)("Brand").ToString) = True Then

            If ddlBrand.Items.Count > 0 Then
                If ddlBrand.Items.Contains(New ListItem(ds.Tables(0).Rows(0)("Brandname").ToString, ds.Tables(0).Rows(0)("Brand").ToString)) = True Then
                    ddlBrand.SelectedValue = ds.Tables(0).Rows(0)("Brand").ToString
                    txtBrand.Text = ""
                    txtBrand.Enabled = False
                Else
                    txtBrand.Text = ""
                    txtBrand.Enabled = True
                    txtBrand.Text = ds.Tables(0).Rows(0)("Brandname").ToString
                End If
            Else
                txtBrand.Text = ""
                txtBrand.Enabled = True
                txtBrand.Text = ds.Tables(0).Rows(0)("Brandname").ToString
            End If
        Else
            ddlBrand.SelectedValue = "0"
            txtBrand.Text = ""
            txtBrand.Enabled = True
            txtBrand.Text = ds.Tables(0).Rows(0)("Brand").ToString
        End If

        txtModel.Text = ds.Tables(0).Rows(0)("Model").ToString
        txtDescription.Text = ds.Tables(0).Rows(0)("Description").ToString

        txtAssetNo.Text = ds.Tables(0).Rows(0)("AssetNo").ToString
        txtIPAddress.Text = ds.Tables(0).Rows(0)("IP_Address").ToString

        txtSerial.Text = ds.Tables(0).Rows(0)("SerialNo").ToString
        ddlCondition.SelectedValue = ds.Tables(0).Rows(0)("Condition").ToString
        txtWarranty.Text = ds.Tables(0).Rows(0)("Warranty").ToString
        ddlWarranty.SelectedValue = ds.Tables(0).Rows(0)("WarrantyType").ToString
        txtVendorRefNo.Text = ds.Tables(0).Rows(0)("Vendor_RefNo").ToString

        ddlStatus.SelectedValue = ds.Tables(0).Rows(0)("AssetStatusID").ToString

        If ds.Tables(0).Rows(0)("Disable_AssetStatus").ToString() = 1 Then
            ddlStatus.Enabled = True
        Else
            ddlStatus.Enabled = False
        End If


        lblVendorPayee.Text = ds.Tables(0).Rows(0)("VendorPayeeName").ToString
        hdfVendorPayeeID.Value = ds.Tables(0).Rows(0)("VendorPayeeID").ToString
        hdfVendorPayeeType.Value = ds.Tables(0).Rows(0)("VendorPayeeType").ToString
        hdfVendorDetail.Value = ds.Tables(0).Rows(0)("VendorPayeeDetail").ToString

        If hdfVendorPayeeType.Value.ToString = "User" Then
            txtShopName.Visible = True
            txtShopName.Text = ds.Tables(0).Rows(0)("ShopName").ToString
            btnVendorPayee.Enabled = False
        ElseIf hdfVendorPayeeType.Value.ToString = "Vendor" Then
            txtShopName.Visible = False
            txtShopName.Text = ""
            btnVendorPayee.Enabled = True
        Else
            txtShopName.Text = ""
            txtShopName.Visible = False
            btnVendorPayee.Enabled = True
        End If


        txtWarrantyPeriod.Text = ds.Tables(0).Rows(0)("Warranty_Period").ToString
        txtAvailInInventory.Text = ds.Tables(0).Rows(0)("Availability_In_Inventory").ToString
        If ds.Tables(0).Rows(0)("RequiredDate").ToString <> "" Then
            cldRequiredDate.SelectedDate = Format(Convert.ToDateTime(ds.Tables(0).Rows(0)("RequiredDate").ToString), "dd MMM yyyy")
        Else
            cldRequiredDate.Clear()
        End If

        txtRemarks.Text = ds.Tables(0).Rows(0)("Remarks").ToString
        If ds.Tables(0).Rows(0)("PurchaseCurrency").ToString <> "" Then
            ddlCurrencyCode.SelectedValue = ds.Tables(0).Rows(0)("PurchaseCurrency").ToString
        End If

        txtPurchaseCost.Text = ds.Tables(0).Rows(0)("PurchaseCost").ToString
        txtPurchaseBudgeted.Text = ds.Tables(0).Rows(0)("BudgetAmount").ToString
        txtCurrencyRate.Text = ds.Tables(0).Rows(0)("PurchaseCurrencyRate").ToString

        If txtPurchaseCost.Text.ToString <> "" And txtCurrencyRate.Text.ToString <> "" Then
            txtCRQuoted.Text = Format(CDec(txtPurchaseCost.Text.ToString) / CDec(txtCurrencyRate.Text.ToString), "0.0000")
        End If
        If txtPurchaseBudgeted.Text.ToString <> "" And txtCurrencyRate.Text.ToString <> "" Then
            txtCRBudgeted.Text = Format(CDec(txtPurchaseBudgeted.Text.ToString) / CDec(txtCurrencyRate.Text.ToString), "0.0000")
        End If

        hdfLocationApprovedBy.Value = ds.Tables(0).Rows(0)("LocationApprovedBy").ToString

        ShowApplyTo(hdfLocationApprovedBy.Value, "LocationApprovedBy")
        If hdfLocationApprovedBy.Value.ToString <> "" Then
            btnLocApprovedBy.Enabled = False
        Else
            btnLocApprovedBy.Enabled = True
        End If

        hdfCCParty.Value = ds.Tables(0).Rows(0)("eMailCCParty").ToString
        ShowApplyTo(hdfCCParty.Value.ToString, "CCParty")


        If ds.Tables(0).Rows(0)("PurchaseDate").ToString <> "" Then
            cldPurchaseDate.SelectedDate = Format(Convert.ToDateTime(ds.Tables(0).Rows(0)("PurchaseDate").ToString), "dd MMM yyyy")
        Else
            cldPurchaseDate.Clear()
        End If

        txtDepreciationPeriod.Text = ds.Tables(0).Rows(0)("Depreciation_Period").ToString
        txtQuantity.Text = ds.Tables(0).Rows(0)("No_Of_Units").ToString
        txtUnit.Text = ds.Tables(0).Rows(0)("Units").ToString

        'chkSplit.Enabled = False
        'If txtQuantity.Text.ToString <> "" Then
        '    If Convert.ToInt32(txtQuantity.Text.ToString) <> 1 Then
        '        chkSplit.Enabled = True
        '    End If
        'End If


        If ds.Tables(0).Rows(0)("OthChargesCurrency").ToString <> "" Then
            ddlOthCurrencyCode.SelectedValue = ds.Tables(0).Rows(0)("OthChargesCurrency").ToString
        End If

        txtOthCharges.Text = ds.Tables(0).Rows(0)("OthersCharges").ToString


        If ds.Tables(0).Rows(0)("AssetClass_COA").ToString <> "" Then
            ddlAssetClassCOA.SelectedValue = ds.Tables(0).Rows(0)("AssetClass_COA").ToString
        Else
            ddlAssetClassCOA.SelectedValue = ""
        End If

        If ds.Tables(0).Rows(0)("Depreciation_Effective").ToString <> "" Then
            cldDepreciationEffective.SelectedDate = Format(Convert.ToDateTime(ds.Tables(0).Rows(0)("Depreciation_Effective").ToString), "dd MMM yyyy")
        Else
            cldDepreciationEffective.Clear()
        End If

        txtDepreciationMonth.Text = ds.Tables(0).Rows(0)("Depreciation_Month").ToString
        txtAccDepreciation.Text = ds.Tables(0).Rows(0)("Accumulated_Depreciation").ToString()
        If ds.Tables(0).Rows(0)("Depreciation_Expense_COA").ToString() <> "" Then
            ddlDepExpCOA.SelectedValue = ds.Tables(0).Rows(0)("Depreciation_Expense_COA").ToString()
        Else
            ddlDepExpCOA.SelectedValue = ""
        End If


        If ds.Tables(0).Rows(0)("Accumulated_Depreciation_COA").ToString() <> "" Then
            ddlAccDepreciationCOA.SelectedValue = ds.Tables(0).Rows(0)("Accumulated_Depreciation_COA").ToString()
        Else
            ddlAccDepreciationCOA.SelectedValue = ""
        End If

        txtResidualValue.Text = ds.Tables(0).Rows(0)("ResidualValue").ToString()
        txtNetBookValue.Text = ds.Tables(0).Rows(0)("NetBookValue").ToString()
        If ds.Tables(0).Rows(0)("Obsolate_Date").ToString <> "" Then
            cldObsoleteDate.SelectedDate = Format(Convert.ToDateTime(ds.Tables(0).Rows(0)("Obsolate_Date").ToString), "dd MMM yyyy")
        Else
            cldObsoleteDate.Clear()
        End If
        If ds.Tables(0).Rows(0)("Disposal_Date").ToString <> "" Then
            cldDisposalDate.SelectedDate = Format(Convert.ToDateTime(ds.Tables(0).Rows(0)("Disposal_Date").ToString), "dd MMM yyyy")
        Else
            cldDisposalDate.Clear()
        End If

        If ds.Tables(0).Rows(0)("WriteOff_Date").ToString <> "" Then
            cldWriteoffDate.SelectedDate = Format(Convert.ToDateTime(ds.Tables(0).Rows(0)("WriteOff_Date").ToString), "dd MMM yyyy")
        Else
            cldWriteoffDate.Clear()
        End If

        hdfCreatedBy.Value = ds.Tables(0).Rows(0)("CreatedBy").ToString
        lblCreatedBy.Text = ds.Tables(0).Rows(0)("CreatedByName").ToString
        lblCreatedOn.Text = ds.Tables(0).Rows(0)("CreatedOn").ToString

        txtAppToBeUsed.Text = ds.Tables(0).Rows(0)("Application_ToBeUsed").ToString

        txtOthersInfo.Text = ds.Tables(0).Rows(0)("OthersInfo").ToString

        txtVAT.Text = ds.Tables(0).Rows(0)("VAT").ToString
        txtDiscount.Text = ds.Tables(0).Rows(0)("Discount").ToString
        txtSpecification.Text = ds.Tables(0).Rows(0)("Specification").ToString

        Dim j As Integer
        For j = 0 To ds.Tables(1).Rows.Count - 1
            If dsUseList.Select("OwnerID='" + ds.Tables(1).Rows(j)("OwnerID").ToString() + "'").Length <= 0 Then
                Dim newUserRow As DataRow = dsUseList.NewRow()
                newUserRow("OwnerID") = ds.Tables(1).Rows(j)("OwnerID").ToString()
                newUserRow("FullName") = ds.Tables(1).Rows(j)("FULLNAME").ToString()
                newUserRow("JobGrade") = ds.Tables(1).Rows(j)("JobGrade").ToString
                newUserRow("Internal_Audit") = ds.Tables(1).Rows(j)("Internal_Audit").ToString
                newUserRow("MIS_Seed_Member") = ds.Tables(1).Rows(j)("MIS_Seed_Member").ToString
                newUserRow("Job_Function") = ds.Tables(1).Rows(j)("Job_Function").ToString
                dsUseList.Rows.Add(newUserRow)
            End If
        Next

        gvUserList.DataSource = ds.Tables(1)
        gvUserList.DataMember = ds.Tables(1).TableName
        gvUserList.DataBind()

        btnGenerate.Text = "Save"
    End Sub

    Public Function GeteRequistionJobFunction() As DataView
        Dim dtViewJobFunction As DataView = clsCF.GeteRequistionJobFunction()
        Return dtViewJobFunction
    End Function

    Protected Function GetSubAsset(ByVal strAssetID As String) As DataSet
        Using MyConnection As New SqlConnection(strConnectionString)
            Dim MyData As New DataSet
            Try
                Dim dsStation As New DataSet

                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetSubAsset")

                With MyCommand

                    .Parameters.Add("@MainAssetID", SqlDbType.VarChar, 20)
                    .Parameters("@MainAssetID").Value = hdfMainAssetID.Value.ToString
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = strAssetID
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "SubAsset"
                Return MyData

               
            Catch ex As Exception
                Me.lblMsg.Text = "GetSubAsset: " & ex.Message.ToString
                Return MyData
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Function

    Protected Sub btnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim strID As String = 0

                lblMsg.Text = ""

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_ConfirmSubAsset", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@MainAssetID", SqlDbType.VarChar, 20)
                    .Parameters("@MainAssetID").Value = hdfMainAssetID.Value.ToString()
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    'Dim ds As DataSet = GetSubAsset("")
                    'gvSubAssetTemp.DataSource = ds
                    'gvSubAssetTemp.DataMember = ds.Tables(0).TableName
                    'gvSubAssetTemp.DataBind()

                    Session("AssetID") = hdfMainAssetID.Value.ToString()
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Confirm Successful.'); document.location.href ='frmAssetDetail.aspx';</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvChildAsset_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub Clear()
        txtAssetID.Text = ""
        'ddlLocation.SelectedValue = ""
        txtAssetNo.Text = ""
        txtIPAddress.Text = ""
        txtSerial.Text = ""
        txtOthersInfo.Text = ""
        txtWarranty.Text = ""
        ddlWarranty.SelectedIndex = 0
        ddlUserType.SelectedValue = "Select One"
        btnAddUser.Visible = False
        txtOwner.Text = ""
        txtOwner.Visible = False

        dsUseList.Clear()
        dsUseList.Dispose()
        gvUserList.DataSource = dsUseList
        gvUserList.DataMember = dsUseList.TableName
        gvUserList.DataBind()

        hdfUserList.Value = ""
        lblParentCategory.Text = ""
        lblCategory.Text = ""
        hdfCategoryID.Value = ""
        hdfParentCategory.Value = ""
        hdfParentCategoryID.Value = ""
        txtVendorRefNo.Text = ""
        ddlBrand.SelectedIndex = 0
        txtBrand.Text = ""
        txtModel.Text = ""
        txtAppToBeUsed.Text = ""
        txtDescription.Text = ""
        txtSpecification.Text = ""
        txtWarrantyPeriod.Text = ""
        txtDepreciationPeriod.Text = ""
        txtAvailInInventory.Text = ""
        txtQuantity.Text = ""
        txtUnit.Text = ""
        txtRemarks.Text = ""
        txtPurchaseCost.Text = ""
        txtPurchaseBudgeted.Text = ""
        txtOthCharges.Text = ""
        txtVAT.Text = ""
        txtDiscount.Text = ""
        txtCRQuoted.Text = ""
        txtCRBudgeted.Text = ""
        lblCreatedBy.Text = ""
        hdfCreatedBy.Value = ""
        lblCreatedOn.Text = ""
        ddlAssetClassCOA.SelectedIndex = 0
        cldDepreciationEffective.Clear()
        txtDepreciationMonth.Text = ""
        txtAccDepreciation.Text = ""
        ddlAccDepreciationCOA.SelectedIndex = 0
        txtResidualValue.Text = ""
        txtNetBookValue.Text = ""
        cldObsoleteDate.Clear()
        cldDisposalDate.Clear()
        cldWriteoffDate.Clear()
        chkSplit.Checked = False


    End Sub

    Protected Sub gvSubAssetTemp_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvSubAssetTemp.RowDeleting
        Dim strAssetID As String = ""
        Dim lbtnDelete As LinkButton

        Clear()

        lbtnDelete = gvSubAssetTemp.Rows(e.RowIndex).FindControl("lbtnDelete")

        strAssetID = lbtnDelete.CommandArgument.ToString()

        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim strID As String = 0

                lblMsg.Text = ""

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteSubAsset", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = strAssetID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    Dim ds As DataSet = GetSubAsset("")
                    gvSubAssetTemp.DataSource = ds
                    gvSubAssetTemp.DataMember = ds.Tables(0).TableName
                    gvSubAssetTemp.DataBind()

                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvChildAsset_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using


    End Sub
End Class
