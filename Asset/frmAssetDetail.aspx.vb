Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings


Partial Class Asset_frmAssetDetail
    Inherits System.Web.UI.Page
    Dim clsNewAsset As New DIMERCO.eAsset.BO.clsNewAsset
    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")
    Dim strFileExtension As String = ConfigurationSettings.AppSettings("DocExtension")
    Dim intFileSizeLimitKB As Integer = ConfigurationSettings.AppSettings("intFileSizeLimitKB")
    Dim intFileSizeLimit As Integer = ConfigurationSettings.AppSettings("intFileSizeLimit")

    Public Shared hifSupDoc As ArrayList = New ArrayList()
    Dim strRightCode As String = ConfigurationSettings.AppSettings("RightCode")
    Public Shared dsUseList As DataTable = New DataTable()

    Dim strLansweeperConnectionString As String = ConfigurationSettings.AppSettings("LansweeperConnectionString")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Me.Page.MaintainScrollPositionOnPostBack = True

        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If IsPostBack() = False Then

            ddlStation.Enabled = False
            ddlTransferToStation.Visible = False

            lblSpecialRequest.Visible = False
            lblSpecialReqCategory.Visible = False

           
            hdfConfirmValue.Value = ""
            Session("MappedAssetList") = ""
            Session("DA_CategoryDepreciation_Period") = ""
            Session("DA_ParentCategory") = ""
            Session("DA_ParentCategoryID") = ""
            Session("DA_CategoryID") = ""
            Session("DA_Category") = ""

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            DisableButton()

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "A0002")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If

            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Asset Detail page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnSave.Enabled = True
                        gvSpec.Columns(0).Visible = True
                        gvSoftware.Columns(0).Visible = True
                        gvChildAsset.Columns(0).Visible = True
                        btnSpecification.Enabled = True
                        btnSoftware.Enabled = True
                        btnMaintenance.Enabled = True
                        btnChildAsset.Enabled = True
                    Else
                        Session("blnWrite") = False
                        btnSave.Enabled = False
                        gvSpec.Columns(0).Visible = False
                        gvSoftware.Columns(0).Visible = False
                        gvChildAsset.Columns(0).Visible = False
                        btnSpecification.Enabled = False
                        btnSoftware.Enabled = False
                        btnMaintenance.Enabled = False
                        btnChildAsset.Enabled = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            dsUseList.Clear()
            dsUseList.Dispose()
            dsUseList.Columns.Clear()
            dsUseList.Columns.Add("OwnerID")
            dsUseList.Columns.Add("FullName")
            dsUseList.Columns.Add("JobGrade")
            dsUseList.Columns.Add("Internal_Audit")
            dsUseList.Columns.Add("MIS_Seed_Member")
            dsUseList.Columns.Add("Job_Function")

            VisibleMantadary_Send_eReq(False)
            BindDropDownList()

            'Dim strAssetID_QS As String = ""

            'If Request.QueryString("AssetID") <> Nothing Then
            '    strAssetID_QS = Request.QueryString("AssetID").ToString
            'End If

            If Session("AssetID") <> Nothing Then
                If Session("AssetID").ToString <> "" Then
                    txtAssetID.Text = Session("AssetID").ToString
                    GetAssetDetail()
                    Session("AssetID") = ""
                End If
                'ElseIf strAssetID_QS <> "" Then
                '    txtAssetID.Text = strAssetID_QS
                '    GetAssetDetail()
            End If

            Session("DA_USERLIST") = ""
            Session("DA_USERLIST_TYPE") = ""

            Me.btnSave.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnSave, "", "btnPosteFMS", "btnSendeReq", "", "", ""))
            Me.btnPosteFMS.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnPosteFMS, "", "btnSave", "btnSendeReq", "", "", ""))
            Me.btnSendeReq.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnSendeReq, "", "btnSave", "btnPosteFMS", "", "", ""))

            'Me.btnSave.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnSave, "", "btnPosteFMS", "btnSendeReq", "btnCanceleReq", "", ""))
            'Me.btnPosteFMS.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnPosteFMS, "", "btnSave", "btnSendeReq", "btnCanceleReq", "", ""))
            'Me.btnSendeReq.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnSendeReq, "", "btnSave", "btnPosteFMS", "btnCanceleReq", "", ""))
            'Me.btnCanceleReq.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnCanceleReq, "", "btnSave", "btnPosteFMS", "btnSendeReq", "", ""))
        Else
            If Session("MappedAssetList").ToString <> hdfMappedAssetID.Value.ToString Then
                'hdfMappedAssetID.Value = Session("MappedAssetList").ToString
                ''Session("MappedAssetList") = ""
                'GetMappedAssetList()
                GetAssetDetail()
            End If

            If Session("DA_USERLIST") <> "" Then
                If Session("DA_USERLIST_TYPE") = "Owner" Then
                    'If Session("DA_USERLIST").ToString <> "" Then
                    '    hdfOwnerUserID.Value = Session("DA_USERLIST").ToString
                    '    If hdfOwnerUserID.Value.ToString <> "" Then
                    '        Dim dsOwnerInfo As DataSet = clsCF.GetOwnerInformation(hdfOwnerUserID.Value.ToString)
                    '        If dsOwnerInfo.Tables.Count > 0 Then
                    '            If dsOwnerInfo.Tables(0).Rows.Count > 0 Then
                    '                txtJobGrade.Text = dsOwnerInfo.Tables(0).Rows(0)("JobGrade").ToString

                    '                If dsOwnerInfo.Tables(0).Rows(0)("IsInternalAudit").ToString = "Y" Then
                    '                    chkInternalAudit.Checked = True
                    '                Else
                    '                    chkInternalAudit.Checked = False
                    '                End If

                    '                If dsOwnerInfo.Tables(0).Rows(0)("IsSeedMember").ToString = "Y" Then
                    '                    chkMIS_Seed_Member.Checked = True
                    '                Else
                    '                    chkMIS_Seed_Member.Checked = False
                    '                End If

                    '            End If
                    '        End If
                    '    End If
                    'End If
                    'ShowApplyTo(Session("DA_USERLIST").ToString, "Owner")
                    'ChildAssetMaintainRelation()
                ElseIf Session("DA_USERLIST_TYPE") = "LocationApprovedBy" Then
                    If Session("DA_USERLIST").ToString <> "" Then
                        hdfLocationApprovedBy.Value = Session("DA_USERLIST").ToString
                    End If
                    ShowApplyTo(Session("DA_USERLIST").ToString, "LocationApprovedBy")
                ElseIf Session("DA_USERLIST_TYPE") = "CCParty" Then
                    If Session("DA_USERLIST").ToString <> "" Then
                        hdfCCParty.Value = Session("DA_USERLIST").ToString
                    End If
                    ShowApplyTo(Session("DA_USERLIST").ToString, "CCParty")
                ElseIf Session("DA_USERLIST_TYPE") = "UserList" Then
                    If Session("DA_USERLIST").ToString <> "" Then
                        hdfUserList.Value = Session("DA_USERLIST").ToString
                    End If
                    ShowApplyTo(Session("DA_USERLIST").ToString, "UserList")
                End If
                Session("DA_USERLIST_TYPE") = ""
                Session("DA_USERLIST") = ""
            Else
                If Session("DA_USERLIST_TYPE") = "Owner" Then
                    If Session("DA_USERLIST").ToString <> "" Then
                        hdfOwnerUserID.Value = Session("DA_USERLIST").ToString
                    End If
                    ShowApplyTo(hdfOwnerUserID.Value.ToString, "Owner")
                    ChildAssetMaintainRelation()
                ElseIf Session("DA_USERLIST_TYPE") = "LocationApprovedBy" Then
                    If Session("DA_USERLIST").ToString <> "" Then
                        hdfLocationApprovedBy.Value = Session("DA_USERLIST").ToString
                    End If
                    ShowApplyTo(hdfLocationApprovedBy.Value, "LocationApprovedBy")
                ElseIf Session("DA_USERLIST_TYPE") = "CCParty" Then
                    If Session("DA_USERLIST").ToString <> "" Then
                        hdfCCParty.Value = Session("DA_USERLIST").ToString
                    End If
                    ShowApplyTo(hdfCCParty.Value, "CCParty")
                End If
                Session("DA_USERLIST_TYPE") = ""
                Session("DA_USERLIST") = ""
            End If

            If Session("DA_PayeeID") <> Nothing Then
                hdfVendorPayeeID.Value = Session("DA_PayeeID").ToString
            End If
            If Session("DA_PayeeName") <> Nothing Then
                lblVendorPayee.Text = Session("DA_PayeeName").ToString
            End If

            If Session("DA_PayeeType") <> Nothing Then
                If Session("DA_PayeeType").ToString = "Vendor" Then
                    txtShopName.Visible = False
                Else
                    txtShopName.Visible = True
                End If
                hdfVendorPayeeType.Value = Session("DA_PayeeType").ToString
            End If

            If Session("DA_CategoryID").ToString <> "" Then
                hdfCategoryID.Value = Session("DA_CategoryID").ToString
                Session("DA_CategoryID") = ""

                hdfParentCategoryID.Value = ""
                hdfParentCategory.Value = ""

                lblParentCategory.Text = ""
                lblParentCategory.Visible = False
            End If

            If Session("DA_Category").ToString <> "" Then
                lblCategory.Text = Session("DA_Category").ToString
                Session("DA_Category") = ""
            End If

            If Session("DA_ParentCategoryID").ToString <> "" Then
                hdfParentCategoryID.Value = Session("DA_ParentCategoryID").ToString
                BindBrand()
                Session("DA_ParentCategoryID") = ""
            End If

            If Session("DA_ParentCategory").ToString <> "" Then
                hdfParentCategory.Value = Session("DA_ParentCategory").ToString
                Session("DA_ParentCategory") = ""
                If hdfParentCategory.Value.ToString <> "" Then
                    lblParentCategory.Text = hdfParentCategory.Value.ToString + " - "
                    lblParentCategory.Visible = True
                End If
            End If



            If Session("DA_CategoryDepreciation_Period").ToString <> "" Then
                txtDepreciationPeriod.Text = Session("DA_CategoryDepreciation_Period").ToString
                Session("DA_CategoryDepreciation_Period") = ""
            End If


            If txtAssetID.Text.ToString <> "" Then
                hdfUserAssetMsg.Value = ""

                GetAssetSubData()

                'If ddlUserType.SelectedValue.ToString = "User" Then
                '    If gvUserList.Rows.Count = 0 Then
                '        hdfOwnerUserID.Value = ""
                '        hdfUserList.Value = ""
                '    Else
                '        If ddlUserType.SelectedValue.ToString = "User" And hdfOwnerUserID.Value.ToString <> "" And hdfCategoryID.Value.ToString <> "" Then
                '            Dim strMsg As String = CheckUserAsset()
                '            If strMsg <> "" Then
                '                Page.ClientScript.RegisterStartupScript(Me.GetType(), "PopupScript", "<script type=text/javascript>alert('" + strMsg + "');</script>")

                '                'hdfUserAssetMsg.Value = strMsg

                '                '    If hdfConfirmValue.Value.ToString <> "No" Then
                '                '        hdfConfirmValue.Value = "Yes"
                '                '        Page.ClientScript.RegisterStartupScript(Me.GetType(), "PopupScript", "<script type=text/javascript>confirm();</script>")
                '                '    End If
                '                'Else
                '                '    hdfConfirmValue.Value = "No"
                '            End If
                '        End If
                '    End If
                'End If
            End If

        End If

        If txtAssetID.Text.ToString = "" Then
            btnSpecification.Enabled = False
            btnSoftware.Enabled = False
            btnMaintenance.Enabled = False
            btnChildAsset.Enabled = False
            fuBrowse.Enabled = False
            btnAddQuoReceipt.Enabled = False
            btnPosteFMS.Enabled = False
            btnSendeReq.Enabled = False
        End If
    End Sub

    

    Private Sub DisableButton()
        btnSave.Enabled = False
        btnPosteFMS.Enabled = False
        btnSendeReq.Enabled = False
        btnCanceleReq.Enabled = False

        gvQuotationReceipt.Columns(0).Visible = False
        gvSpec.Columns(0).Visible = False
        gvSoftware.Columns(0).Visible = False
        gvChildAsset.Columns(0).Visible = False

        btnSpecification.Enabled = False
        btnSoftware.Enabled = False
        btnMaintenance.Enabled = False
        btnChildAsset.Enabled = False

        fuBrowse.Enabled = False
        btnAddQuoReceipt.Enabled = False

    End Sub


    Private Sub ShowApplyTo(ByVal strApplyList As String, ByVal strType As String)
        Try
            Dim MyData As New DataSet
            Dim i As Integer

            If strType = "Owner" Then
                txtOwner.Text = ""
                MyData = DIMERCO.SDK.Utilities.LSDK.getUserDataByList(strApplyList)
                txtOwner.Text = MyData.Tables(0).Rows(0)("Fullname").ToString
            End If

            If strType = "LocationApprovedBy" Then
                lblLocationApprovedBy.Text = ""
                MyData = DIMERCO.SDK.Utilities.LSDK.getUserDataByList(strApplyList)
                For i = 0 To MyData.Tables(0).Rows.Count - 1
                    If lblLocationApprovedBy.Text.ToString = "" Then
                        lblLocationApprovedBy.Text = MyData.Tables(0).Rows(i)("Fullname").ToString()
                    Else
                        lblLocationApprovedBy.Text = lblLocationApprovedBy.Text.ToString + ", " + MyData.Tables(0).Rows(i)("Fullname").ToString
                    End If
                Next
            End If

            'End If
            If strType = "CCParty" Then
                lblCCParty.Text = ""
                MyData = DIMERCO.SDK.Utilities.LSDK.getUserDataByList(strApplyList)

                hdfCCParty.Value = strApplyList
                For i = 0 To MyData.Tables(0).Rows.Count - 1
                    If lblCCParty.Text.ToString = "" Then
                        lblCCParty.Text = MyData.Tables(0).Rows(i)("Fullname").ToString()
                    Else
                        lblCCParty.Text = lblCCParty.Text.ToString + ", " + MyData.Tables(0).Rows(i)("Fullname").ToString
                    End If
                Next
            End If

            If strType = "UserList" Then
                MyData = DIMERCO.SDK.Utilities.LSDK.getUserDataByList(strApplyList)

                hdfUserList.Value = strApplyList

                For i = 0 To MyData.Tables(0).Rows.Count - 1

                    If dsUseList.Select("OwnerID='" + MyData.Tables(0).Rows(i)("UserID").ToString() + "'").Length <= 0 Then

                        Dim dsOwnerInfo As DataSet = clsCF.GetOwnerInformation(MyData.Tables(0).Rows(i)("UserID").ToString())
                        If dsOwnerInfo.Tables.Count > 0 Then
                            If dsOwnerInfo.Tables(0).Rows.Count > 0 Then
                                Dim newUserRow As DataRow = dsUseList.NewRow()

                                newUserRow("OwnerID") = MyData.Tables(0).Rows(i)("UserID").ToString()
                                newUserRow("FullName") = MyData.Tables(0).Rows(i)("FULLNAME").ToString()
                                newUserRow("JobGrade") = dsOwnerInfo.Tables(0).Rows(0)("JobGrade").ToString
                                If dsOwnerInfo.Tables(0).Rows(0)("IsInternalAudit").ToString = "Y" Then
                                    newUserRow("Internal_Audit") = "Y"
                                Else
                                    newUserRow("Internal_Audit") = "N"
                                End If
                                If dsOwnerInfo.Tables(0).Rows(0)("IsSeedMember").ToString = "Y" Then
                                    newUserRow("MIS_Seed_Member") = "Y"
                                Else
                                    newUserRow("MIS_Seed_Member") = "N"
                                End If
                                newUserRow("Job_Function") = ""

                                dsUseList.Rows.Add(newUserRow)

                            End If
                        End If
                    End If
                Next

                gvUserList.DataSource = dsUseList
                gvUserList.DataMember = dsUseList.TableName
                gvUserList.DataBind()
            End If
        Catch ex As Exception
            Me.lblMsg.Text = "ShowApplyTo: " & ex.Message.ToString
        End Try
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[AccessStation],[CurrencyCode],[AssetStatus], [WarrantyType],[StationList]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"
                MyData.Tables(1).TableName = "CurrencyCode"

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)

                If Session("DA_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("DA_StationID").ToString
                    GetStationLocation(ddlStation.SelectedValue.ToString)
                End If

                Dim dtViewCurrency As DataView = clsCF.GetCurrency()

                ddlCurrencyCode.DataSource = dtViewCurrency
                ddlCurrencyCode.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlCurrencyCode.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlCurrencyCode.DataBind()

                ddlOthCurrencyCode.DataSource = dtViewCurrency.Table
                ddlOthCurrencyCode.DataValueField = dtViewCurrency.Table.Columns("HQID").ToString
                ddlOthCurrencyCode.DataTextField = dtViewCurrency.Table.Columns("CurrencyCode").ToString
                ddlOthCurrencyCode.DataBind()

                StationCurrency()

                ddlStatus.DataSource = MyData.Tables(2)
                ddlStatus.DataValueField = MyData.Tables(2).Columns("StatusID").ToString
                ddlStatus.DataTextField = MyData.Tables(2).Columns("Status").ToString
                ddlStatus.DataBind()

                ddlWarranty.DataSource = MyData.Tables(3)
                ddlWarranty.DataValueField = MyData.Tables(3).Columns("WarrantyID").ToString
                ddlWarranty.DataTextField = MyData.Tables(3).Columns("WarrantyType").ToString
                ddlWarranty.DataBind()

                Dim dtAssetClassCOA As New DataView
                dtAssetClassCOA = clsCF.GetCOAList("1")
                ddlAssetClassCOA.DataSource = dtAssetClassCOA
                ddlAssetClassCOA.DataValueField = dtAssetClassCOA.Table.Columns("COA").ToString
                ddlAssetClassCOA.DataTextField = dtAssetClassCOA.Table.Columns("COADesc").ToString
                ddlAssetClassCOA.DataBind()

                Dim dtDepreciationCOA As New DataView
                dtDepreciationCOA = clsCF.GetCOAList("2")
                ddlAccDepreciationCOA.DataSource = dtDepreciationCOA
                ddlAccDepreciationCOA.DataValueField = dtDepreciationCOA.Table.Columns("COA").ToString
                ddlAccDepreciationCOA.DataTextField = dtDepreciationCOA.Table.Columns("COADesc").ToString
                ddlAccDepreciationCOA.DataBind()


                ddlTransferToStation.DataSource = MyData.Tables(4)
                ddlTransferToStation.DataValueField = MyData.Tables(4).Columns("StationID").ToString
                ddlTransferToStation.DataTextField = MyData.Tables(4).Columns("StationCode").ToString
                ddlTransferToStation.DataBind()

                'Dim dtViewJobFunction As DataView = clsCF.GeteRequistionJobFunction()
                'ddlJobFunction_ignore.DataSource = dtViewJobFunction
                'ddlJobFunction_ignore.DataValueField = dtViewJobFunction.Table.Columns("Job_Function").ToString
                'ddlJobFunction_ignore.DataTextField = dtViewJobFunction.Table.Columns("Job_Function").ToString
                'ddlJobFunction_ignore.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub BindBrand()
        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByID")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[BrandCategory]"
                    .Parameters.Add("@ID", SqlDbType.Int)
                    If hdfParentCategoryID.Value.ToString = "" Then
                        .Parameters("@ID").Value = 0
                    Else
                        .Parameters("@ID").Value = hdfParentCategoryID.Value.ToString
                    End If

                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "Brand"

                ddlBrand.DataSource = MyData
                ddlBrand.DataValueField = MyData.Tables(0).Columns("BrandID").ToString()
                ddlBrand.DataTextField = MyData.Tables(0).Columns("BrandName").ToString
                ddlBrand.DataBind()

                If ddlBrand.Items.Count = 1 Then
                    txtBrand.Enabled = True
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "BindBrand: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub GetAssetDetail()
        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetAssetDetail")

                With MyCommand

                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(1).TableName = "AssetDetail"

                ddlLocation.DataSource = MyData.Tables(0)
                ddlLocation.DataValueField = MyData.Tables(0).Columns("LocationID").ToString
                ddlLocation.DataTextField = MyData.Tables(0).Columns("Location").ToString
                ddlLocation.DataBind()

                If MyData.Tables(1).Rows(0)("ApplyNewID").ToString = "" Then
                    btnAddQuoReceipt.Visible = True
                    fuBrowse.Visible = True
                    btnAddQuoReceipt.Enabled = True
                    fuBrowse.Enabled = True
                Else
                    btnAddQuoReceipt.Visible = False
                    fuBrowse.Visible = False
                End If

                hdfMappedAssetID.Value = MyData.Tables(1).Rows(0)("MappedAssetList").ToString
                Session("MappedAssetList") = hdfMappedAssetID.Value.ToString()

                ddlStation.SelectedValue = MyData.Tables(1).Rows(0)("StationID").ToString
                hdfOriginalStationID.Value = MyData.Tables(1).Rows(0)("StationID").ToString

                ddlLocation.SelectedValue = MyData.Tables(1).Rows(0)("LocationID").ToString
                hdfOriginalLocationID.Value = MyData.Tables(1).Rows(0)("LocationID").ToString

                txtOwner.Text = MyData.Tables(1).Rows(0)("OwnerName").ToString
                hdfOwnerUserID.Value = MyData.Tables(1).Rows(0)("OwnerID").ToString
                'If hdfOwnerUserID.Value.ToString <> "" Then
                '    Dim dsOwnerInfo As DataSet = clsCF.GetOwnerInformation(hdfOwnerUserID.Value.ToString)
                '    If dsOwnerInfo.Tables.Count > 0 Then
                '        If dsOwnerInfo.Tables(0).Rows.Count > 0 Then
                '            txtJobGrade.Text = dsOwnerInfo.Tables(0).Rows(0)("JobGrade").ToString
                '        End If
                '    End If
                'End If

                'If MyData.Tables(1).Rows(0)("Internal_Audit").ToString = 0 Then
                '    chkInternalAudit.Checked = False
                'Else
                '    chkInternalAudit.Checked = True
                'End If
                'If MyData.Tables(1).Rows(0)("MIS_Seed_Member").ToString = 0 Then
                '    chkMIS_Seed_Member.Checked = False
                'Else
                '    chkMIS_Seed_Member.Checked = True
                'End If

                'ddlJobFunction.SelectedValue = MyData.Tables(1).Rows(0)("Job_Function").ToString

                ddlUserType.SelectedValue = MyData.Tables(1).Rows(0)("OwnerType").ToString
                'If ddlUserType.SelectedValue = "User" Then
                '    btnOwner.Enabled = True
                '    txtOwner.Enabled = False
                '    hdfOriginalOwner.Value = hdfOwnerUserID.Value.ToString
                'ElseIf ddlUserType.SelectedValue = "Office" Then
                '    btnOwner.Enabled = False
                '    txtOwner.Enabled = True
                '    hdfOriginalOwner.Value = txtOwner.Text.ToString
                'Else
                '    btnOwner.Enabled = False
                '    txtOwner.Enabled = False
                '    hdfOriginalOwner.Value = ""
                'End If

                txtOwner.Visible = False
                lbtnAddOwner.Visible = False
                gvUserList.Visible = False
                hdfUserList.Value = ""

                If ddlUserType.SelectedValue = "User" Then
                    lbtnAddOwner.Visible = True
                    hdfOriginalOwner.Value = hdfOwnerUserID.Value.ToString
                    gvUserList.Visible = True
                ElseIf ddlUserType.SelectedValue = "Office" Then
                    txtOwner.Visible = True
                    txtOwner.Enabled = True
                    hdfOriginalOwner.Value = txtOwner.Text.ToString
                End If

                Dim j As Integer
                For j = 0 To MyData.Tables(12).Rows.Count - 1
                    If dsUseList.Select("OwnerID='" + MyData.Tables(12).Rows(j)("OwnerID").ToString() + "'").Length <= 0 Then
                        Dim newUserRow As DataRow = dsUseList.NewRow()
                        newUserRow("OwnerID") = MyData.Tables(12).Rows(j)("OwnerID").ToString()
                        newUserRow("FullName") = MyData.Tables(12).Rows(j)("FULLNAME").ToString()
                        newUserRow("JobGrade") = MyData.Tables(12).Rows(j)("JobGrade").ToString
                        newUserRow("Internal_Audit") = MyData.Tables(12).Rows(j)("Internal_Audit").ToString
                        newUserRow("MIS_Seed_Member") = MyData.Tables(12).Rows(j)("MIS_Seed_Member").ToString
                        newUserRow("Job_Function") = MyData.Tables(12).Rows(j)("Job_Function").ToString
                        dsUseList.Rows.Add(newUserRow)
                    End If

                    If hdfUserList.Value.ToString = "" Then
                        hdfUserList.Value = MyData.Tables(12).Rows(j)("OwnerID").ToString()
                    Else
                        hdfUserList.Value = hdfUserList.Value.ToString + "," + MyData.Tables(12).Rows(j)("OwnerID").ToString()
                    End If
                Next

                gvUserList.DataSource = MyData.Tables(12)
                gvUserList.DataMember = MyData.Tables(12).TableName
                gvUserList.DataBind()





                lblCategory.Text = MyData.Tables(1).Rows(0)("Category").ToString
                hdfCategoryID.Value = MyData.Tables(1).Rows(0)("CategoryID").ToString
                hdfParentCategory.Value = MyData.Tables(1).Rows(0)("ParentCategory").ToString
                If hdfParentCategory.Value.ToString <> "" Then
                    lblParentCategory.Text = MyData.Tables(1).Rows(0)("ParentCategory").ToString + " - "
                    lblParentCategory.Visible = True
                Else
                    lblParentCategory.Text = ""
                    lblParentCategory.Visible = False
                End If


                hdfParentCategoryID.Value = MyData.Tables(1).Rows(0)("ParentCategoryID").ToString

                lblSpecialReqCategory.Visible = False
                lblSpecialRequest.Visible = False
                lblSpecialReqCategory.Text = MyData.Tables(1).Rows(0)("SpecialReqCategory").ToString
                hdfActualCategoryID.Value = MyData.Tables(1).Rows(0)("ActualCategoryID").ToString

                If MyData.Tables(1).Rows(0)("SpecialReqCategory").ToString <> "" Then
                    lblSpecialReqCategory.Visible = True
                    lblSpecialRequest.Visible = True
                End If

                lblUseOthBudget.Visible = False
                If MyData.Tables(1).Rows(0)("UseOthBudget").ToString <> "" Then
                    lblUseOthBudget.Visible = True
                    lblUseOthBudget.Text = MyData.Tables(1).Rows(0)("UseOthBudget").ToString
                End If



                BindBrand()

                If IsNumeric(MyData.Tables(1).Rows(0)("Brand").ToString) = True Then

                    If ddlBrand.Items.Count > 0 Then
                        If ddlBrand.Items.Contains(New ListItem(MyData.Tables(1).Rows(0)("Brandname").ToString, MyData.Tables(1).Rows(0)("Brand").ToString)) = True Then
                            ddlBrand.SelectedValue = MyData.Tables(1).Rows(0)("Brand").ToString
                            txtBrand.Text = ""
                            txtBrand.Enabled = False
                        Else
                            txtBrand.Text = ""
                            txtBrand.Enabled = True
                            txtBrand.Text = MyData.Tables(1).Rows(0)("Brandname").ToString
                        End If
                    Else
                        txtBrand.Text = ""
                        txtBrand.Enabled = True
                        txtBrand.Text = MyData.Tables(1).Rows(0)("Brandname").ToString
                    End If
                Else
                    ddlBrand.SelectedValue = "0"
                    txtBrand.Text = ""
                    txtBrand.Enabled = True
                    txtBrand.Text = MyData.Tables(1).Rows(0)("Brand").ToString
                End If

                'txtBrand.Text = MyData.Tables(1).Rows(0)("Brand").ToString
                txtModel.Text = MyData.Tables(1).Rows(0)("Model").ToString
                txtDescription.Text = MyData.Tables(1).Rows(0)("Description").ToString

                txtAssetNo.Text = MyData.Tables(1).Rows(0)("AssetNo").ToString
                txtIPAddress.Text = MyData.Tables(1).Rows(0)("IP_Address").ToString
                lbleRequisitionNo.Text = MyData.Tables(1).Rows(0)("eRequisitionNo").ToString

                'If MyData.Tables(1).Rows(0)("eReq_Status").ToString = "P" Or MyData.Tables(1).Rows(0)("eReq_Status").ToString = "W" Then
                '    Button1.Visible = True
                '    Button2.Visible = True
                '    Button3.Visible = True
                'Else
                '    Button1.Visible = False
                '    Button2.Visible = False
                '    Button3.Visible = False
                'End If

                txtSerial.Text = MyData.Tables(1).Rows(0)("SerialNo").ToString
                ddlCondition.SelectedValue = MyData.Tables(1).Rows(0)("Condition").ToString
                txtWarranty.Text = MyData.Tables(1).Rows(0)("Warranty").ToString
                ddlWarranty.SelectedValue = MyData.Tables(1).Rows(0)("WarrantyType").ToString
                txtVendorRefNo.Text = MyData.Tables(1).Rows(0)("Vendor_RefNo").ToString

                ddlStatus.SelectedValue = MyData.Tables(1).Rows(0)("AssetStatusID").ToString

                

                If MyData.Tables(1).Rows(0)("Disable_AssetStatus").ToString() = 1 Then
                    ddlStatus.Enabled = True
                Else
                    ddlStatus.Enabled = False
                End If


                lblVendorPayee.Text = MyData.Tables(1).Rows(0)("VendorPayeeName").ToString
                hdfVendorPayeeID.Value = MyData.Tables(1).Rows(0)("VendorPayeeID").ToString
                hdfVendorPayeeType.Value = MyData.Tables(1).Rows(0)("VendorPayeeType").ToString
                hdfVendorDetail.Value = MyData.Tables(1).Rows(0)("VendorPayeeDetail").ToString

                If hdfVendorPayeeType.Value.ToString = "User" Then
                    txtShopName.Visible = True
                    txtShopName.Text = MyData.Tables(1).Rows(0)("ShopName").ToString
                    btnVendorPayee.Enabled = False
                ElseIf hdfVendorPayeeType.Value.ToString = "Vendor" Then
                    txtShopName.Visible = False
                    txtShopName.Text = ""
                    btnVendorPayee.Enabled = True
                Else
                    txtShopName.Text = ""
                    txtShopName.Visible = False
                    btnVendorPayee.Enabled = True
                End If


                txtWarrantyPeriod.Text = MyData.Tables(1).Rows(0)("Warranty_Period").ToString
                txtAvailInInventory.Text = MyData.Tables(1).Rows(0)("Availability_In_Inventory").ToString
                If MyData.Tables(1).Rows(0)("RequiredDate").ToString <> "" Then
                    cldRequiredDate.SelectedDate = Format(Convert.ToDateTime(MyData.Tables(1).Rows(0)("RequiredDate").ToString), "dd MMM yyyy")
                Else
                    cldRequiredDate.Clear()
                End If

                txtRemarks.Text = MyData.Tables(1).Rows(0)("Remarks").ToString
                If MyData.Tables(1).Rows(0)("PurchaseCurrency").ToString <> "" Then
                    ddlCurrencyCode.SelectedValue = MyData.Tables(1).Rows(0)("PurchaseCurrency").ToString
                End If

                txtPurchaseCost.Text = MyData.Tables(1).Rows(0)("PurchaseCost").ToString
                txtPurchaseBudgeted.Text = MyData.Tables(1).Rows(0)("BudgetAmount").ToString
                txtCurrencyRate.Text = MyData.Tables(1).Rows(0)("PurchaseCurrencyRate").ToString

                If txtPurchaseCost.Text.ToString <> "" And txtCurrencyRate.Text.ToString <> "" Then
                    txtCRQuoted.Text = Format(CDec(txtPurchaseCost.Text.ToString) / CDec(txtCurrencyRate.Text.ToString), "0.0000")
                End If
                If txtPurchaseBudgeted.Text.ToString <> "" And txtCurrencyRate.Text.ToString <> "" Then
                    txtCRBudgeted.Text = Format(CDec(txtPurchaseBudgeted.Text.ToString) / CDec(txtCurrencyRate.Text.ToString), "0.0000")
                End If

                hdfLocationApprovedBy.Value = MyData.Tables(1).Rows(0)("LocationApprovedBy").ToString

                ShowApplyTo(hdfLocationApprovedBy.Value, "LocationApprovedBy")
                If hdfLocationApprovedBy.Value.ToString <> "" Then
                    btnLocApprovedBy.Enabled = False
                Else
                    btnLocApprovedBy.Enabled = True
                End If

                hdfCCParty.Value = MyData.Tables(1).Rows(0)("eMailCCParty").ToString
                ShowApplyTo(hdfCCParty.Value.ToString, "CCParty")


                If MyData.Tables(1).Rows(0)("PurchaseDate").ToString <> "" Then
                    cldPurchaseDate.SelectedDate = Format(Convert.ToDateTime(MyData.Tables(1).Rows(0)("PurchaseDate").ToString), "dd MMM yyyy")
                Else
                    cldPurchaseDate.Clear()
                End If

                txtDepreciationPeriod.Text = MyData.Tables(1).Rows(0)("Depreciation_Period").ToString
                txtQuantity.Text = MyData.Tables(1).Rows(0)("No_Of_Units").ToString
                txtUnit.Text = MyData.Tables(1).Rows(0)("Units").ToString


                If MyData.Tables(1).Rows(0)("OthChargesCurrency").ToString <> "" Then
                    ddlOthCurrencyCode.SelectedValue = MyData.Tables(1).Rows(0)("OthChargesCurrency").ToString
                End If

                txtOthCharges.Text = MyData.Tables(1).Rows(0)("OthersCharges").ToString


                If MyData.Tables(1).Rows(0)("AssetClass_COA").ToString <> "" Then
                    ddlAssetClassCOA.SelectedValue = MyData.Tables(1).Rows(0)("AssetClass_COA").ToString
                Else
                    ddlAssetClassCOA.SelectedValue = ""
                End If

                If MyData.Tables(1).Rows(0)("Depreciation_Effective").ToString <> "" Then
                    cldDepreciationEffective.SelectedDate = Format(Convert.ToDateTime(MyData.Tables(1).Rows(0)("Depreciation_Effective").ToString), "dd MMM yyyy")
                Else
                    cldDepreciationEffective.Clear()
                End If

                txtDepreciationMonth.Text = MyData.Tables(1).Rows(0)("Depreciation_Month").ToString
                txtAccDepreciation.Text = MyData.Tables(1).Rows(0)("Accumulated_Depreciation").ToString()
                If MyData.Tables(1).Rows(0)("Depreciation_Expense_COA").ToString() <> "" Then
                    ddlDepExpCOA.SelectedValue = MyData.Tables(1).Rows(0)("Depreciation_Expense_COA").ToString()
                Else
                    ddlDepExpCOA.SelectedValue = ""
                End If


                If MyData.Tables(1).Rows(0)("Accumulated_Depreciation_COA").ToString() <> "" Then
                    ddlAccDepreciationCOA.SelectedValue = MyData.Tables(1).Rows(0)("Accumulated_Depreciation_COA").ToString()
                Else
                    ddlAccDepreciationCOA.SelectedValue = ""
                End If

                txtResidualValue.Text = MyData.Tables(1).Rows(0)("ResidualValue").ToString()
                txtNetBookValue.Text = MyData.Tables(1).Rows(0)("NetBookValue").ToString()
                If MyData.Tables(1).Rows(0)("Obsolate_Date").ToString <> "" Then
                    cldObsoleteDate.SelectedDate = Format(Convert.ToDateTime(MyData.Tables(1).Rows(0)("Obsolate_Date").ToString), "dd MMM yyyy")
                Else
                    cldObsoleteDate.Clear()
                End If
                If MyData.Tables(1).Rows(0)("Disposal_Date").ToString <> "" Then
                    cldDisposalDate.SelectedDate = Format(Convert.ToDateTime(MyData.Tables(1).Rows(0)("Disposal_Date").ToString), "dd MMM yyyy")
                Else
                    cldDisposalDate.Clear()
                End If

                If MyData.Tables(1).Rows(0)("WriteOff_Date").ToString <> "" Then
                    cldWriteoffDate.SelectedDate = Format(Convert.ToDateTime(MyData.Tables(1).Rows(0)("WriteOff_Date").ToString), "dd MMM yyyy")
                Else
                    cldWriteoffDate.Clear()
                End If

                hdfCreatedBy.Value = MyData.Tables(1).Rows(0)("CreatedBy").ToString
                lblCreatedBy.Text = MyData.Tables(1).Rows(0)("CreatedByName").ToString
                lblCreatedOn.Text = MyData.Tables(1).Rows(0)("CreatedOn").ToString
                lbleReqApprovedBy.Text = MyData.Tables(1).Rows(0)("eReq_ApprovedBy").ToString
                lbleReqApprovedOn.Text = MyData.Tables(1).Rows(0)("eReq_ApprovedOn").ToString
                lbleReqStatus.Text = MyData.Tables(1).Rows(0)("eReqStatus").ToString

                txtAppToBeUsed.Text = MyData.Tables(1).Rows(0)("Application_ToBeUsed").ToString

                lblPosteFMSBy.Text = MyData.Tables(1).Rows(0)("eFMS_By").ToString
                lblPosteFMSOn.Text = MyData.Tables(1).Rows(0)("eFMS_On").ToString
                lblPosteFMSStatus.Text = MyData.Tables(1).Rows(0)("eFMSStatus").ToString
                lblPosteFMSACK_Reason.Text = MyData.Tables(1).Rows(0)("eFMS_ACK_Reason").ToString

                

                txtOthersInfo.Text = MyData.Tables(1).Rows(0)("OthersInfo").ToString

                txtVAT.Text = MyData.Tables(1).Rows(0)("VAT").ToString
                txtDiscount.Text = MyData.Tables(1).Rows(0)("Discount").ToString

                chkTransferStation.Checked = False
                ddlTransferToStation.Visible = False
                lblTransferRemark.Visible = False
                If MyData.Tables(1).Rows(0)("TransferTo_StationID").ToString <> "" Then
                    chkTransferStation.Checked = True
                    ddlTransferToStation.Visible = True
                    lblTransferRemark.Visible = True
                    ddlTransferToStation.SelectedValue = MyData.Tables(1).Rows(0)("TransferTo_StationID").ToString
                End If



                ''---------------------------------------------------------------
                ''Start - Add Post eFMS Exceptional - 29 jan 2014
                ''---------------------------------------------------------------
                'chkPosteFMS_Exception.Visible = False
                'chkPosteFMS_Exception.Checked = False
                'If MyData.Tables(1).Rows(0)("PosteFMS_Exception").ToString = "1" Then
                '    chkPosteFMS_Exception.Visible = True
                '    chkPosteFMS_Exception.Checked = True
                '    PosteFMS_Exception()
                'End If
                ''---------------------------------------------------------------
                ''End - Add Post eFMS Exceptional - 29 jan 2014
                ''---------------------------------------------------------------
                'chkPosteFMS_Exception.Checked = False
                'ddlExceptionReason.Visible = False
                'If MyData.Tables(1).Rows(0)("eFMS_ACK_Reason").ToString = "1" Then
                '    chkPosteFMS_Exception.Checked = True
                '    ddlExceptionReason.Visible = True
                '    ddlExceptionReason.SelectedValue = MyData.Tables(1).Rows(0)("eFMS_ACK_Reason").ToString()
                'End If

                gvAllocation.DataSource = MyData
                gvAllocation.DataMember = MyData.Tables(2).TableName
                gvAllocation.DataBind()

                gvChildAsset.DataSource = MyData
                gvChildAsset.DataMember = MyData.Tables(3).TableName
                gvChildAsset.DataBind()
                If gvChildAsset.Rows.Count > 0 Then
                    gvChildAsset.Columns(1).Visible = False
                    ChildAssetMaintainRelation()
                End If

                gvMaintenance.DataSource = MyData
                gvMaintenance.DataMember = MyData.Tables(4).TableName
                gvMaintenance.DataBind()

                gvSoftware.DataSource = MyData
                gvSoftware.DataMember = MyData.Tables(5).TableName
                gvSoftware.DataBind()

                gvSpec.DataSource = MyData
                gvSpec.DataMember = MyData.Tables(6).TableName
                gvSpec.DataBind()


                gvQuotationReceipt.DataSource = MyData
                gvQuotationReceipt.DataMember = MyData.Tables(7).TableName
                gvQuotationReceipt.DataBind()

                gvParentAsset.DataSource = MyData
                gvParentAsset.DataMember = MyData.Tables(11).TableName
                gvParentAsset.DataBind()

                gvSubAsset.DataSource = MyData
                gvSubAsset.DataMember = MyData.Tables(13).TableName
                gvSubAsset.DataBind()

                gvMainAsset.DataSource = MyData
                gvMainAsset.DataMember = MyData.Tables(14).TableName
                gvMainAsset.DataBind()


                VisibleMantadary_Send_eReq(False)
                VisibleMantadary_Post_eFMS(False)

                If MyData.Tables(9).Rows(0)("eReqParty") <> "0" Then
                    If MyData.Tables(1).Rows(0)("Need_Send_eReq").ToString() = "1" Then
                        btnSendeReq.Enabled = True
                        'ddlStatus.Enabled = False
                        VisibleMantadary_Send_eReq(True)

                        If lbleReqStatus.Text.ToString = "Approved" Then
                            btnCanceleReq.Enabled = False
                            btnSendeReq.Enabled = False
                        ElseIf lbleReqStatus.Text.ToString = "Rejected" Then
                            btnCanceleReq.Enabled = False
                            btnSendeReq.Enabled = False
                        ElseIf lbleReqStatus.Text.ToString = "Pending" Then
                            btnCanceleReq.Enabled = True
                            btnSendeReq.Enabled = False

                        ElseIf lbleReqStatus.Text.ToString = "Canceled" Then
                            btnCanceleReq.Enabled = False
                            btnSendeReq.Enabled = True
                        ElseIf lbleReqStatus.Text.ToString = "Modification Request" Then
                            btnCanceleReq.Enabled = True
                            btnSendeReq.Enabled = True
                        Else
                            btnCanceleReq.Enabled = False
                            btnSendeReq.Enabled = False
                        End If
                    Else
                        btnSendeReq.Enabled = False
                        btnCanceleReq.Enabled = False
                        'ddlStatus.Enabled = True

                    End If
                Else
                    btnSendeReq.Enabled = False
                    btnCanceleReq.Enabled = False
                End If

                hdfeFMS_Status.Value = MyData.Tables(1).Rows(0)("eFMS_Status").ToString
                hdfeFMS_ACK.Value = MyData.Tables(1).Rows(0)("eFMS_ACK").ToString
                btnSplitRecord.Visible = False

                lblLansweeperMappingMsg.Text = ""
                If MyData.Tables(1).Rows(0)("eFMS_Status").ToString = "D" Or MyData.Tables(1).Rows(0)("eFMS_Status").ToString = "S" Then

                    If MyData.Tables(1).Rows(0)("eFMS_Status").ToString = "D" Then
                        If txtQuantity.Text.ToString <> "" Then
                            If CInt(txtQuantity.Text.ToString) > 1 Then
                                btnSplitRecord.Visible = True
                            End If
                        End If
                    End If

                    If MyData.Tables(9).Rows(0)("eFMS_Party") <> "0" Then

                        ''---------------------------------------------------------------
                        ''Start - Add Post eFMS Exceptional - 29 jan 2014
                        ''---------------------------------------------------------------
                        'If MyData.Tables(1).Rows(0)("PosteFMS_Exception").ToString = "1" Then
                        '    chkPosteFMS_Exception.Visible = True
                        '    PosteFMS_Exception()
                        'End If
                        ''---------------------------------------------------------------
                        ''End - Add Post eFMS Exceptional - 29 jan 2014
                        ''---------------------------------------------------------------

                        If MyData.Tables(1).Rows(0)("Need_Post_eFMS").ToString() = "1" Then

                            
                            If MyData.Tables(1).Rows(0)("eFMS_Status").ToString = "D" Then
                                lblLansweeperMappingMsg.Text = MyData.Tables(1).Rows(0)("LansweeperMappingMsg").ToString

                                If lblLansweeperMappingMsg.Text.ToString() <> "" Then
                                    btnPosteFMS.Enabled = False
                                Else
                                    btnPosteFMS.Enabled = True
                                End If
                                VisibleMantadary_Post_eFMS(True)
                            ElseIf MyData.Tables(1).Rows(0)("eFMS_Status").ToString = "S" Then
                                If hdfeFMS_ACK.Value.ToString = "0" Then
                                    lblLansweeperMappingMsg.Text = MyData.Tables(1).Rows(0)("LansweeperMappingMsg").ToString

                                    btnPosteFMS.Text = "Re-Post eFMS"
                                    If lblLansweeperMappingMsg.Text.ToString() <> "" Then
                                        btnPosteFMS.Enabled = False
                                    Else
                                        btnPosteFMS.Enabled = True
                                    End If


                                    VisibleMantadary_Post_eFMS(True)
                                Else
                                    btnPosteFMS.Enabled = False
                                End If
                            Else
                                btnPosteFMS.Enabled = False
                            End If
                        Else
                            btnPosteFMS.Enabled = False
                        End If


                        Else
                            btnPosteFMS.Enabled = False
                        End If

                    ElseIf MyData.Tables(1).Rows(0)("eFMS_Status").ToString = "N" Then 'NO NEED POST eFMS
                        ''---------------------------------------------------------------
                        ''Start - Add Post eFMS Exceptional - 29 jan 2014
                        ''---------------------------------------------------------------
                        'chkPosteFMS_Exception.Visible = True
                        ''---------------------------------------------------------------
                        ''End - Add Post eFMS Exceptional - 29 jan 2014
                        ''---------------------------------------------------------------
                        btnPosteFMS.Enabled = False
                        ddlAssetClassCOA.Enabled = False
                        cldDepreciationEffective.Enabled = False
                        ddlDepExpCOA.Enabled = False
                        ddlAccDepreciationCOA.Enabled = False
                        txtResidualValue.Enabled = False
                        txtNetBookValue.Enabled = False
                        txtDepreciationPeriod.Enabled = False
                        cldPurchaseDate.Enabled = False
                        If txtQuantity.Text.ToString <> "" Then
                            If CInt(txtQuantity.Text.ToString) > 1 Then
                                btnSplitRecord.Visible = True
                            End If
                        End If
                    ElseIf MyData.Tables(1).Rows(0)("eFMS_Status").ToString = "P" Then 'Purchased at eFMS
                        btnPosteFMS.Text = "Post eFMS"
                        btnPosteFMS.Enabled = False
                        ddlAssetClassCOA.Enabled = False
                        cldDepreciationEffective.Enabled = False
                        ddlDepExpCOA.Enabled = False
                        ddlAccDepreciationCOA.Enabled = False
                        txtResidualValue.Enabled = False
                        txtNetBookValue.Enabled = False
                        txtDepreciationPeriod.Enabled = False
                        txtQuantity.Enabled = False
                        cldPurchaseDate.Enabled = False
                        txtPurchaseCost.Enabled = False
                        txtPurchaseBudgeted.Enabled = False
                        txtOthCharges.Enabled = False
                        txtVAT.Enabled = False
                        txtDiscount.Enabled = False
                        'ddlBrand.Enabled = False
                        'txtModel.Enabled = False
                    End If

                    If MyData.Tables(1).Rows(0)("CreatedBy").ToString <> Session("DA_UserID").ToString Then
                        If Session("blnModifyOthers") = False Then
                            btnSave.Enabled = False
                            btnSave.Enabled = False
                            gvSpec.Columns(0).Visible = False
                            gvSoftware.Columns(0).Visible = False
                            gvChildAsset.Columns(0).Visible = False
                            btnSpecification.Enabled = False
                            btnSoftware.Enabled = False
                            btnMaintenance.Enabled = False
                            btnChildAsset.Enabled = False
                        Else
                            btnSave.Enabled = True
                            btnSave.Enabled = True
                            gvSpec.Columns(0).Visible = True
                            gvSoftware.Columns(0).Visible = True
                            gvChildAsset.Columns(0).Visible = True
                            btnSpecification.Enabled = True
                            btnSoftware.Enabled = True
                            btnMaintenance.Enabled = True
                            btnChildAsset.Enabled = True
                        End If

                        If Session("blnDeleteOthers") = True Then
                            gvSpec.Columns(0).Visible = True
                            gvSoftware.Columns(0).Visible = True
                            gvChildAsset.Columns(0).Visible = True
                        Else
                            gvSpec.Columns(0).Visible = False
                            gvSoftware.Columns(0).Visible = False
                            gvChildAsset.Columns(0).Visible = False
                        End If
                    End If

                    'Disable the field 
                    If MyData.Tables(1).Rows(0)("ApplyNewID").ToString <> "" Then
                        txtQuantity.Enabled = False
                        btnCategory.Enabled = False
                        btnVendorPayee.Enabled = False
                        txtShopName.Enabled = False
                        cldPurchaseDate.Enabled = False
                        ddlCurrencyCode.Enabled = False
                        txtPurchaseCost.Enabled = False
                        txtPurchaseBudgeted.Enabled = False
                        txtCurrencyRate.Enabled = False
                        btnLocApprovedBy.Enabled = False
                        txtCRQuoted.Enabled = False
                        txtCRBudgeted.Enabled = False
                        ddlOthCurrencyCode.Enabled = False
                        txtOthCharges.Enabled = False
                        btnSpecification.Enabled = False
                        'btnSoftware.Enabled = False
                    Else
                        If txtQuantity.Text.ToString <> "" Then
                            If CInt(txtQuantity.Text.ToString) > 1 Then
                                btnSplitRecord.Visible = True
                            End If
                        End If
                    End If

                    If ddlStatus.SelectedValue.ToString = "18" Or ddlStatus.SelectedValue.ToString = "5" Or ddlStatus.SelectedValue.ToString = "3" Or ddlStatus.SelectedValue.ToString = "4" Or ddlStatus.SelectedValue.ToString = "11" Then
                        btnSave.Enabled = False
                        btnPosteFMS.Enabled = False
                        btnSendeReq.Enabled = False
                        btnCanceleReq.Enabled = False
                        btnSplitRecord.Enabled = False
                        btnSpecification.Enabled = False
                        btnSoftware.Enabled = False
                        btnMaintenance.Enabled = False
                        btnChildAsset.Enabled = False
                        btnAddSubAsset.Enabled = False
                        'If ddlStatus.SelectedValue.ToString = "5" Then
                        '    chkTransferStation.Enabled = False
                        '    ddlTransferToStation.Enabled = False
                        'End If
                    End If

                    GetMappedAssetList()


            Catch ex As Exception
                Me.lblMsg.Text = "GetAssetDetail: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using

    End Sub

    Private Sub GetMappedAssetList()
        Using MyConnection As New SqlConnection(strLansweeperConnectionString)

            Try

                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand


                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetMappedAssetList")

                With MyCommand

                    .Parameters.Add("@MappedAssetList", SqlDbType.NVarChar)
                    .Parameters("@MappedAssetList").Value = hdfMappedAssetID.Value.ToString()
                    .Parameters.Add("@eAssetAssetID", SqlDbType.NVarChar, 50)
                    .Parameters("@eAssetAssetID").Value = Session("AssetID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "MappedAssetList"

                gvLansweeperMap.DataSource = MyData
                gvLansweeperMap.DataMember = MyData.Tables(0).TableName
                gvLansweeperMap.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "GetMappedAssetList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using

    End Sub

    Private Sub VisibleMantadary_Send_eReq(ByVal blnVisible As Boolean)

        Label60.Visible = blnVisible
        Label61.Visible = blnVisible
        Label62.Visible = blnVisible
        'Label63.Visible = blnVisible
        'Label64.Visible = blnVisible
        Label65.Visible = blnVisible
        Label66.Visible = blnVisible
        Label67.Visible = blnVisible
        Label68.Visible = blnVisible
        Label69.Visible = blnVisible
        Label70.Visible = blnVisible
        Label71.Visible = blnVisible
        Label72.Visible = blnVisible
        Label73.Visible = blnVisible
        Label74.Visible = blnVisible
        Label75.Visible = blnVisible
        Label76.Visible = blnVisible
        'Label77.Visible = blnVisible
        Label78.Visible = blnVisible

    End Sub

    Private Sub VisibleMantadary_Post_eFMS(ByVal blnVisible As Boolean)

        Label60.Visible = blnVisible
        Label61.Visible = blnVisible
        Label62.Visible = blnVisible
        'Label63.Visible = blnVisible
        'Label64.Visible = blnVisible
        Label65.Visible = blnVisible
        Label66.Visible = blnVisible
        Label67.Visible = blnVisible
        Label68.Visible = blnVisible
        Label69.Visible = blnVisible
        Label70.Visible = blnVisible
        Label71.Visible = blnVisible
        Label72.Visible = blnVisible
        Label73.Visible = blnVisible
        Label74.Visible = blnVisible
        Label75.Visible = blnVisible
        Label76.Visible = blnVisible
        'Label77.Visible = blnVisible
        Label78.Visible = blnVisible
        Label79.Visible = blnVisible
        Label80.Visible = blnVisible
        Label81.Visible = blnVisible
        Label82.Visible = blnVisible
        Label83.Visible = blnVisible
        Label84.Visible = blnVisible
        Label85.Visible = blnVisible
        Label86.Visible = blnVisible

    End Sub

    Private Sub GetAssetSubData()
        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetAssetDetail")

                With MyCommand

                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(1).TableName = "AssetDetail"


                gvAllocation.DataSource = MyData
                gvAllocation.DataMember = MyData.Tables(2).TableName
                gvAllocation.DataBind()

                gvChildAsset.DataSource = MyData
                gvChildAsset.DataMember = MyData.Tables(3).TableName
                gvChildAsset.DataBind()

                If gvChildAsset.Rows.Count > 0 Then
                    gvChildAsset.Columns(1).Visible = False
                    ChildAssetMaintainRelation()
                End If


                gvMaintenance.DataSource = MyData
                gvMaintenance.DataMember = MyData.Tables(4).TableName
                gvMaintenance.DataBind()

                gvSoftware.DataSource = MyData
                gvSoftware.DataMember = MyData.Tables(5).TableName
                gvSoftware.DataBind()

                gvSpec.DataSource = MyData
                gvSpec.DataMember = MyData.Tables(6).TableName
                gvSpec.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "GetAssetDetail: " & ex.Message.ToString
            Finally
                 clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnSpecification_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSpecification.Click
        Dim strScript As String

        Session("AssetID") = ""
        Session("AssetID") = txtAssetID.Text.ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('frmAssetSpecification.aspx?ParentForm=frmAssetDetail','AddSpecification','height=500, width=700,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub btnSoftware_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSoftware.Click
        Dim strScript As String

        Session("AssetID") = ""
        Session("AssetID") = txtAssetID.Text.ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('frmAssetSoftware.aspx?ParentForm=frmAssetDetail','AddSoftware','height=500, width=700,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub btnMaintenance_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMaintenance.Click
        Session("DA_AssetID") = txtAssetID.Text.ToString
        Session("ApplyNewType") = "Maintenance"
        Response.Redirect("~/Purchase/frmApplyNew.aspx?ANT=M&ID=0")
    End Sub

    Protected Sub btnChildAsset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChildAsset.Click
        Dim strScript As String

        Session("AssetID") = ""
        Session("AssetStationID") = ""
        Session("AssetID") = txtAssetID.Text.ToString
        Session("AssetStationID") = ddlStation.SelectedValue.ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('frmAssetChild.aspx?ParentForm=frmAssetDetail','AddChildAsset','height=750, width=900,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Private Function DataValidation() As Boolean
        DataValidation = True

        If ddlStation.SelectedValue.ToString = "" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Station.')</script>")
            ddlStation.Focus()
            Exit Function
        End If
        If ddlLocation.SelectedValue.ToString = "" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Location.')</script>")
            ddlLocation.Focus()
            Exit Function
        End If
        If ddlUserType.SelectedValue.ToString <> "Select One" Then

            If ddlUserType.SelectedValue.ToString = "User" Then
                If gvUserList.Rows.Count <= 0 Then
                    hdfOwnerUserID.Value = ""
                    hdfUserList.Value = ""
                    'DataValidation = False
                    'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please Select Owner.')</script>")
                    'lbtnAddOwner.Focus()
                    'Exit Function
                Else
                    Dim ddlJobFunction_gv As New DropDownList
                    Dim j As Integer
                    For j = 0 To gvUserList.Rows.Count - 1
                        ddlJobFunction_gv = gvUserList.Rows(j).FindControl("ddlJobFunction")

                        If ddlJobFunction_gv.SelectedValue.ToString = "" Then
                            DataValidation = False
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please Select Job Function.')</script>")
                            Exit Function
                        End If
                    Next

                    Dim strMsg As String = CheckUserAsset()
                    If strMsg <> "" Then
                        'DataValidation = False
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "PopupScript", "<script type=text/javascript>alert('" + strMsg + "');</script>")
                        Exit Function
                    End If
                End If
            Else
                If txtOwner.Text.ToString = "" Then
                    DataValidation = False
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Owner.')</script>")
                    txtOwner.Focus()
                    Exit Function
                End If
            End If


        End If

        If lblCategory.Text.ToString = "" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please Select Category.')</script>")
            btnCategory.Focus()
            Exit Function
        End If

        If ddlCondition.SelectedValue.ToString = "" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Condition.')</script>")
            ddlCondition.Focus()
            Exit Function
        End If

        If txtAvailInInventory.Text.ToString = "" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Available In Inventory.')</script>")
            txtAvailInInventory.Focus()
            Exit Function
        End If

        If txtQuantity.Text.ToString = "" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Quantity.')</script>")
            txtQuantity.Focus()
            Exit Function
        End If

        If txtDepreciationPeriod.Text.ToString = "" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Depreciation Period.')</script>")
            txtDepreciationPeriod.Focus()
            Exit Function
        End If


        If ddlCurrencyCode.SelectedValue.ToString = "0" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Purchase Cost Currency.')</script>")
            ddlCurrencyCode.Focus()
            Exit Function
        End If

        If txtPurchaseCost.Text.ToString = "" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Purchase Cost.')</script>")
            txtPurchaseCost.Focus()
            Exit Function
        End If

        If txtResidualValue.Text.ToString = "" Then
            txtResidualValue.Text = "0.00"
            'DataValidation = False
            'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Residual Value.')</script>")
            'txtResidualValue.Focus()
            'Exit Function
        End If

        If txtNetBookValue.Text.ToString = "" Then
            DataValidation = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter NetBook Value.')</script>")
            txtNetBookValue.Focus()
            Exit Function
        End If



        Return DataValidation

    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        lblMsg.Text = ""
        If DataValidation() = False Then
            Exit Sub
        Else
            Dim strReturnMsg As String = ""
            strReturnMsg = SaveAssetData()
            If strReturnMsg <> "" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strReturnMsg + "')</script>")
            Else
                GetAssetDetail()
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
            End If
        End If
    End Sub

    Private Function SaveAssetData() As String
        Using sqlConn As New SqlConnection(strConnectionString)

            Dim strReturnMsg As String = ""
            Dim strMsg As String = ""

            Dim strPurchaseDate As String = ""
            Dim strDepreciationEffective As String = ""
            Dim strObsoleteDate As String = ""
            Dim strDisposalDate As String = ""
            Dim strWriteoffDate As String = ""
            Dim strRequiredDate As String = ""
            Dim StrNewAssetID As String = ""
            Dim strChildAssetList As String = ""
            Dim strUserList As String = ""

            strPurchaseDate = Format(cldPurchaseDate.SelectedDate, "dd MMM yyyy").ToString
            If strPurchaseDate.Contains("01 Jan 0001") = True Then
                strPurchaseDate = ""
            End If

            strDepreciationEffective = Format(cldDepreciationEffective.SelectedDate, "dd MMM yyyy").ToString
            If strDepreciationEffective.Contains("01 Jan 0001") = True Then
                strDepreciationEffective = ""
            End If

            strObsoleteDate = Format(cldObsoleteDate.SelectedDate, "dd MMM yyyy").ToString
            If strObsoleteDate.Contains("01 Jan 0001") = True Then
                strObsoleteDate = ""
            End If


            strDisposalDate = Format(cldDisposalDate.SelectedDate, "dd MMM yyyy").ToString
            If strDisposalDate.Contains("01 Jan 0001") = True Then
                strDisposalDate = ""
            End If

            strWriteoffDate = Format(cldWriteoffDate.SelectedDate, "dd MMM yyyy").ToString
            If strWriteoffDate.Contains("01 Jan 0001") = True Then
                strWriteoffDate = ""
            End If


            strRequiredDate = Format(cldRequiredDate.SelectedDate, "dd MMM yyyy").ToString
            If strRequiredDate.Contains("01 Jan 0001") = True Then
                strRequiredDate = ""
            End If

            If txtVAT.Text.ToString = "" Then
                txtVAT.Text = "0.00"
            End If

            If txtDiscount.Text.ToString = "" Then
                txtDiscount.Text = "0.00"
            End If

            If txtNetBookValue.Text.ToString = "" Then
                If ddlStatus.SelectedValue.ToString = 0 Then
                    txtNetBookValue.Text = ((CDec(txtPurchaseCost.Text.ToString) - CDec(txtVAT.Text.ToString)) * CInt(txtQuantity.Text.ToString)) - CDec(txtDiscount.Text.ToString)
                End If
            End If
            

            Dim i As Integer

            If gvChildAsset.Rows.Count > 0 Then
                Dim lbtnRemove As New LinkButton
                Dim chkMaintainRelation As New CheckBox
                Dim strID As String = ""
                For i = 0 To gvChildAsset.Rows.Count - 1
                    lbtnRemove = gvChildAsset.Rows(i).FindControl("lbtnRemove")
                    chkMaintainRelation = gvChildAsset.Rows(i).FindControl("chkMaintainRelation")
                    If chkMaintainRelation.Checked = True Then
                        strID = lbtnRemove.CommandArgument.ToString
                        If strChildAssetList = "" Then
                            strChildAssetList = strID
                        Else
                            strChildAssetList = strChildAssetList + "<" + strID
                        End If
                    End If
                Next
            End If


            Dim strUser As String = ""
            Dim lbtnUserID_gv As New LinkButton
            Dim lblJobGrade_gv As New Label
            Dim lblInternalAudit_gv As New Label
            Dim lblMISSeedMember_gv As New Label
            Dim ddlJobFunction_gv As New DropDownList
            Dim iInternalAudit As Integer = 0
            Dim iMISSeedMember As Integer = 0

            strUserList = ""

            For i = 0 To gvUserList.Rows.Count - 1
                strUser = ""
                lbtnUserID_gv = gvUserList.Rows(i).Cells(0).FindControl("lbtnRemove")
                lblJobGrade_gv = gvUserList.Rows(i).Cells(2).FindControl("lblJobGrade")
                lblInternalAudit_gv = gvUserList.Rows(i).Cells(3).FindControl("lblInternalAudit")
                lblMISSeedMember_gv = gvUserList.Rows(i).Cells(4).FindControl("lblMISSeedMember")
                ddlJobFunction_gv = gvUserList.Rows(i).Cells(5).FindControl("ddlJobFunction")

                If lblInternalAudit_gv.Text.ToString = "Y" Then
                    iInternalAudit = 1
                End If
                If lblMISSeedMember_gv.Text.ToString = "Y" Then
                    iMISSeedMember = 1
                End If

                strUser = lbtnUserID_gv.CommandArgument.ToString() + "<" + lblJobGrade_gv.Text.ToString + ">" + iInternalAudit.ToString + "^" + iMISSeedMember.ToString + "~" + ddlJobFunction_gv.SelectedValue.ToString()
                If strUserList = "" Then
                    strUserList = strUser + "|"
                Else
                    strUserList = strUserList + strUser + "|"
                End If
            Next

            Try
                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveAssetDetail", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@LocationID", SqlDbType.BigInt)
                    .Parameters("@LocationID").Value = ddlLocation.SelectedValue.ToString
                    .Parameters.Add("@OwnerType", SqlDbType.VarChar, 50)
                    .Parameters("@OwnerType").Value = ddlUserType.SelectedValue.ToString
                    .Parameters.Add("@OwnerID", SqlDbType.NVarChar, 100)
                    If ddlUserType.SelectedValue.ToString = "User" Then
                        .Parameters("@OwnerID").Value = hdfOwnerUserID.Value.ToString
                    Else
                        .Parameters("@OwnerID").Value = txtOwner.Text.ToString
                    End If
                    '.Parameters.Add("@Internal_Audit", SqlDbType.TinyInt)
                    'If chkInternalAudit.Checked = True Then
                    '    .Parameters("@Internal_Audit").Value = 1
                    'Else
                    '    .Parameters("@Internal_Audit").Value = 0
                    'End If

                    '.Parameters.Add("@MIS_Seed_Member", SqlDbType.TinyInt)
                    'If chkMIS_Seed_Member.Checked = True Then
                    '    .Parameters("@MIS_Seed_Member").Value = 1
                    'Else
                    '    .Parameters("@MIS_Seed_Member").Value = 0
                    'End If
                    '.Parameters.Add("@Job_Function", SqlDbType.NVarChar, 100)
                    '.Parameters("@Job_Function").Value = ddlJobFunction.SelectedValue.ToString


                    .Parameters.Add("@CategoryID", SqlDbType.BigInt)
                    .Parameters("@CategoryID").Value = CInt(hdfCategoryID.Value.ToString)

                    .Parameters.Add("@Brand", SqlDbType.NVarChar, 50)
                    If ddlBrand.SelectedValue.ToString <> "0" Then
                        .Parameters("@Brand").Value = ddlBrand.SelectedValue.ToString
                    Else
                        .Parameters("@Brand").Value = txtBrand.Text.ToString
                    End If


                    .Parameters.Add("@Model", SqlDbType.NVarChar, 100)
                    .Parameters("@Model").Value = txtModel.Text.ToString
                    .Parameters.Add("@Application_ToBeUsed", SqlDbType.NVarChar)
                    .Parameters("@Application_ToBeUsed").Value = txtAppToBeUsed.Text.ToString
                    .Parameters.Add("@Description", SqlDbType.NVarChar)
                    .Parameters("@Description").Value = txtDescription.Text.ToString
                    .Parameters.Add("@AssetNo", SqlDbType.NVarChar, 50)
                    .Parameters("@AssetNo").Value = txtAssetNo.Text.ToString
                    .Parameters.Add("@IP_Address", SqlDbType.NVarChar, 15)
                    .Parameters("@IP_Address").Value = txtIPAddress.Text.ToString
                    .Parameters.Add("@SerialNo", SqlDbType.NVarChar, 50)
                    .Parameters("@SerialNo").Value = txtSerial.Text.ToString
                    .Parameters.Add("@Condition", SqlDbType.VarChar, 5)
                    .Parameters("@Condition").Value = ddlCondition.SelectedValue.ToString
                    .Parameters.Add("@Warranty", SqlDbType.Int)
                    If txtWarranty.Text.ToString = "" Then
                        .Parameters("@Warranty").Value = DBNull.Value
                    Else
                        .Parameters("@Warranty").Value = CInt(txtWarranty.Text.ToString)
                    End If
                    .Parameters.Add("@WarrantyType", SqlDbType.Int)
                    If CInt(ddlWarranty.SelectedValue.ToString) = 0 Then
                        .Parameters("@WarrantyType").Value = DBNull.Value
                    Else
                        .Parameters("@WarrantyType").Value = CInt(ddlWarranty.SelectedValue.ToString)
                    End If
                    .Parameters.Add("@Vendor_RefNo", SqlDbType.NVarChar, 100)
                    .Parameters("@Vendor_RefNo").Value = txtVendorRefNo.Text.ToString

                    .Parameters.Add("@AssetStatusID", SqlDbType.Int)
                    If CInt(ddlStatus.SelectedValue.ToString) = 0 Then
                        .Parameters("@AssetStatusID").Value = DBNull.Value
                    Else
                        .Parameters("@AssetStatusID").Value = CInt(ddlStatus.SelectedValue.ToString)
                    End If

                    .Parameters.Add("@VendorPayeeID", SqlDbType.NVarChar, 50)
                    .Parameters("@VendorPayeeID").Value = hdfVendorPayeeID.Value.ToString
                    .Parameters.Add("@VendorPayeeType", SqlDbType.NVarChar, 50)
                    .Parameters("@VendorPayeeType").Value = hdfVendorPayeeType.Value.ToString
                    .Parameters.Add("@ShopName", SqlDbType.NVarChar, 500)
                    .Parameters("@ShopName").Value = txtShopName.Text.ToString

                    .Parameters.Add("@Warranty_Period", SqlDbType.Decimal, 18, 2)
                    If txtWarrantyPeriod.Text.ToString = "" Then
                        .Parameters("@Warranty_Period").Value = DBNull.Value
                    Else
                        .Parameters("@Warranty_Period").Value = CDec(txtWarrantyPeriod.Text.ToString)
                    End If

                    .Parameters.Add("@Availability_In_Inventory", SqlDbType.Int)
                    If txtAvailInInventory.Text.ToString = "" Then
                        .Parameters("@Availability_In_Inventory").Value = DBNull.Value
                    Else
                        .Parameters("@Availability_In_Inventory").Value = CInt(txtAvailInInventory.Text.ToString)
                    End If

                    .Parameters.Add("@Remarks", SqlDbType.NVarChar)
                    .Parameters("@Remarks").Value = txtRemarks.Text.ToString


                    .Parameters.Add("@PurchaseCurrency", SqlDbType.Int)
                    If CInt(ddlCurrencyCode.SelectedValue.ToString) = 0 Then
                        .Parameters("@PurchaseCurrency").Value = DBNull.Value
                    Else
                        .Parameters("@PurchaseCurrency").Value = CInt(ddlCurrencyCode.SelectedValue.ToString)
                    End If

                    .Parameters.Add("@PurchaseCost", SqlDbType.Decimal, 18, 2)
                    If txtPurchaseCost.Text.ToString = "" Then
                        .Parameters("@PurchaseCost").Value = DBNull.Value
                    Else
                        .Parameters("@PurchaseCost").Value = CDec(txtPurchaseCost.Text.ToString)
                    End If

                    .Parameters.Add("@BudgetAmount", SqlDbType.Decimal, 18, 2)
                    If txtPurchaseBudgeted.Text.ToString = "" Then
                        .Parameters("@BudgetAmount").Value = DBNull.Value
                    Else
                        .Parameters("@BudgetAmount").Value = CDec(txtPurchaseBudgeted.Text.ToString)
                    End If

                    .Parameters.Add("@PurchaseCurrencyRate", SqlDbType.Decimal, 18, 2)
                    If txtCurrencyRate.Text.ToString = "" Then
                        .Parameters("@PurchaseCurrencyRate").Value = DBNull.Value
                    Else
                        .Parameters("@PurchaseCurrencyRate").Value = CDec(txtCurrencyRate.Text.ToString)
                    End If

                    .Parameters.Add("@LocationApprovedBy", SqlDbType.NVarChar)
                    .Parameters("@LocationApprovedBy").Value = hdfLocationApprovedBy.Value.ToString.Replace(",,", ",")

                    .Parameters.Add("@eMailCCParty", SqlDbType.NVarChar)
                    .Parameters("@eMailCCParty").Value = hdfCCParty.Value.ToString.Replace(",,", ",")


                    .Parameters.Add("@PurchaseDate", SqlDbType.NVarChar, 20)
                    .Parameters("@PurchaseDate").Value = strPurchaseDate

                    .Parameters.Add("@Depreciation_Period", SqlDbType.Decimal, 18, 2)
                    If txtDepreciationPeriod.Text.ToString = "" Then
                        .Parameters("@Depreciation_Period").Value = DBNull.Value
                    Else
                        .Parameters("@Depreciation_Period").Value = CDec(txtDepreciationPeriod.Text.ToString)
                    End If

                    .Parameters.Add("@No_Of_Units", SqlDbType.Int)
                    If txtQuantity.Text.ToString = "" Then
                        .Parameters("@No_Of_Units").Value = DBNull.Value
                    Else
                        .Parameters("@No_Of_Units").Value = CInt(txtQuantity.Text.ToString)
                    End If
                    .Parameters.Add("@Units", SqlDbType.NVarChar, 50)
                    .Parameters("@Units").Value = txtUnit.Text.ToString

                    .Parameters.Add("@OthChargesCurrency", SqlDbType.Int)
                    If CInt(ddlOthCurrencyCode.SelectedValue.ToString) = 0 Then
                        .Parameters("@OthChargesCurrency").Value = DBNull.Value
                    Else
                        If txtOthCharges.Text.ToString = "" Then
                            .Parameters("@OthChargesCurrency").Value = DBNull.Value
                        Else
                            .Parameters("@OthChargesCurrency").Value = CInt(ddlOthCurrencyCode.SelectedValue.ToString)
                        End If

                    End If

                    .Parameters.Add("@OthersCharges", SqlDbType.Decimal, 18, 2)
                    If txtOthCharges.Text.ToString = "" Then
                        .Parameters("@OthersCharges").Value = DBNull.Value
                    Else
                        .Parameters("@OthersCharges").Value = CDec(txtOthCharges.Text.ToString)
                    End If


                    .Parameters.Add("@AssetClass_COA", SqlDbType.BigInt)
                    If ddlAssetClassCOA.SelectedValue.ToString = "" Then
                        .Parameters("@AssetClass_COA").Value = DBNull.Value
                    Else
                        .Parameters("@AssetClass_COA").Value = CInt(ddlAssetClassCOA.SelectedValue.ToString)
                    End If

                    .Parameters.Add("@Depreciation_Effective", SqlDbType.NVarChar, 20)
                    .Parameters("@Depreciation_Effective").Value = strDepreciationEffective

                    .Parameters.Add("@Depreciation_Month", SqlDbType.Decimal, 18, 2)
                    If txtDepreciationMonth.Text.ToString = "" Then
                        .Parameters("@Depreciation_Month").Value = DBNull.Value
                    Else
                        .Parameters("@Depreciation_Month").Value = CDec(txtDepreciationMonth.Text.ToString)
                    End If

                    .Parameters.Add("@Accumulated_Depreciation", SqlDbType.Decimal, 18, 2)
                    If txtAccDepreciation.Text.ToString = "" Then
                        .Parameters("@Accumulated_Depreciation").Value = DBNull.Value
                    Else
                        .Parameters("@Accumulated_Depreciation").Value = CDec(txtAccDepreciation.Text.ToString)
                    End If

                    .Parameters.Add("@Depreciation_Expense_COA", SqlDbType.BigInt)
                    If ddlDepExpCOA.SelectedValue.ToString = "" Then
                        .Parameters("@Depreciation_Expense_COA").Value = DBNull.Value
                    Else
                        .Parameters("@Depreciation_Expense_COA").Value = CInt(ddlDepExpCOA.SelectedValue.ToString)
                    End If

                    .Parameters.Add("@Accumulated_Depreciation_COA", SqlDbType.BigInt)
                    If ddlAccDepreciationCOA.SelectedValue.ToString = "" Then
                        .Parameters("@Accumulated_Depreciation_COA").Value = DBNull.Value
                    Else
                        .Parameters("@Accumulated_Depreciation_COA").Value = CInt(ddlAccDepreciationCOA.SelectedValue.ToString)
                    End If

                    .Parameters.Add("@ResidualValue", SqlDbType.Decimal, 18, 2)
                    If txtResidualValue.Text.ToString = "" Then
                        .Parameters("@ResidualValue").Value = DBNull.Value
                    Else
                        .Parameters("@ResidualValue").Value = CDec(txtResidualValue.Text.ToString)
                    End If

                    .Parameters.Add("@NetBookValue", SqlDbType.Decimal, 18, 2)
                    If txtNetBookValue.Text.ToString = "" Then
                        .Parameters("@NetBookValue").Value = DBNull.Value
                    Else
                        .Parameters("@NetBookValue").Value = CDec(txtNetBookValue.Text.ToString)
                    End If

                    .Parameters.Add("@Obsolate_Date", SqlDbType.NVarChar, 20)
                    .Parameters("@Obsolate_Date").Value = strObsoleteDate

                    .Parameters.Add("@WriteOff_Date", SqlDbType.NVarChar, 20)
                    .Parameters("@WriteOff_Date").Value = strWriteoffDate

                    .Parameters.Add("@Disposal_Date", SqlDbType.NVarChar, 20)
                    .Parameters("@Disposal_Date").Value = strDisposalDate


                    .Parameters.Add("@Required_Date", SqlDbType.NVarChar, 20)
                    .Parameters("@Required_Date").Value = strRequiredDate
                    .Parameters.Add("@ChildAssetList", SqlDbType.NVarChar)
                    .Parameters("@ChildAssetList").Value = strChildAssetList
                    .Parameters.Add("@OthersInfo", SqlDbType.NVarChar)
                    .Parameters("@OthersInfo").Value = txtOthersInfo.Text.ToString

                    .Parameters.Add("@VAT", SqlDbType.Decimal, 18, 2)
                    .Parameters("@VAT").Value = txtVAT.Text.ToString

                    .Parameters.Add("@Discount", SqlDbType.Decimal, 18, 2)
                    .Parameters("@Discount").Value = txtDiscount.Text.ToString

                    .Parameters.Add("@UserList", SqlDbType.NVarChar)
                    .Parameters("@UserList").Value = strUserList

                    .Parameters.Add("@TransferStation", SqlDbType.VarChar, 3)
                    If chkTransferStation.Checked = True Then
                        .Parameters("@TransferStation").Value = ddlTransferToStation.SelectedValue.ToString()
                    Else
                        .Parameters("@TransferStation").Value = ""
                    End If

                    '.Parameters.Add("@PosteFMSException", SqlDbType.TinyInt)
                    'If chkPosteFMS_Exception.Checked = True Then
                    '    .Parameters("@PosteFMSException").Value = 1
                    'Else
                    '    .Parameters("@PosteFMSException").Value = DBNull.Value
                    'End If

                    '.Parameters.Add("@PosteFMSException_Reason", SqlDbType.Int)
                    'If chkPosteFMS_Exception.Checked = True Then
                    '    .Parameters("@PosteFMSException_Reason").Value = Convert.ToInt32(ddlExceptionReason.SelectedValue.ToString())
                    'Else
                    '    .Parameters("@PosteFMSException_Reason").Value = DBNull.Value
                    'End If

                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .Parameters.Add("@NewAssetID", SqlDbType.VarChar, 20)
                    .Parameters("@NewAssetID").Direction = ParameterDirection.Output

                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                    StrNewAssetID = .Parameters("@NewAssetID").Value.ToString()
                End With

                If strMsg <> "" Then
                    strReturnMsg = strMsg
                    'lblMsg.Text = strMsg
                    'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    If StrNewAssetID.ToString <> "" Then
                        txtAssetID.Text = StrNewAssetID.ToString
                    End If

                    'strReturnMsg = "Save Successful"
                    'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                End If

                Return strReturnMsg
            Catch ex As Exception
                Return ex.Message.ToString
                'Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using

    End Function

    Protected Sub gvSpec_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvSpec.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim intID As Integer = 0
                Dim lbtnRemove As New LinkButton

                lblMsg.Text = ""

                lbtnRemove = gvSpec.Rows(e.RowIndex).FindControl("lbtnRemove")
                intID = CInt(lbtnRemove.CommandArgument.ToString)

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteAssetSpecification", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                    .Parameters.Add("@SpecificationID", SqlDbType.Int)
                    .Parameters("@SpecificationID").Value = intID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else

                    GetAssetSpecification()
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                    'lblMsg.Text = "Delete Successful"
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvSpec_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Private Sub GetAssetSpecification()
        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetAssetSpecification")

                With MyCommand
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AssetSpecification"

                gvSpec.DataSource = MyData
                gvSpec.DataMember = MyData.Tables(0).TableName
                gvSpec.DataBind()


            Catch ex As Exception
                Me.lblMsg.Text = "GetAssetSpecification: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub gvSoftware_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvSoftware.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim intSofwareID As Integer = 0
                Dim intID As Integer = 0
                Dim lbtnRemove As New LinkButton
                Dim hdfID As New HiddenField

                lblMsg.Text = ""

                lbtnRemove = gvSoftware.Rows(e.RowIndex).FindControl("lbtnRemove")
                intSofwareID = CInt(lbtnRemove.CommandArgument.ToString)

                hdfID = gvSoftware.Rows(e.RowIndex).FindControl("hdfID")
                intID = CInt(hdfID.Value.ToString)

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteAssetSoftware", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ID", SqlDbType.Int)
                    .Parameters("@ID").Value = intID
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                    .Parameters.Add("@SoftwareID", SqlDbType.BigInt)
                    .Parameters("@SoftwareID").Value = intSofwareID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    GetAssetSoftware()
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                    'lblMsg.Text = "Delete Successful"
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvSoftware_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Private Sub GetAssetSoftware()
        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetAssetSoftware")

                With MyCommand
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AssetSoftware"

                gvSoftware.DataSource = MyData
                gvSoftware.DataMember = MyData.Tables(0).TableName
                gvSoftware.DataBind()


            Catch ex As Exception
                Me.lblMsg.Text = "GetAssetSoftware: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub gvChildAsset_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvChildAsset.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim strID As String = 0
                Dim lbtnRemove As New LinkButton

                lblMsg.Text = ""

                lbtnRemove = gvChildAsset.Rows(e.RowIndex).FindControl("lbtnRemove")
                strID = lbtnRemove.CommandArgument.ToString

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteAssetChild", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                    .Parameters.Add("@Child_AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@Child_AssetID").Value = strID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    GetAssetChild()
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")

                    'lblMsg.Text = "Delete Successful"
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvChildAsset_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Private Sub GetAssetChild()
        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetAssetChild")

                With MyCommand

                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AssetChild"

                gvChildAsset.DataSource = MyData
                gvChildAsset.DataMember = MyData.Tables(0).TableName
                gvChildAsset.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "GetAssetChild: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub gvMaintenance_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvMaintenance.SelectedIndexChanged
        Dim strScript As String

        Session("AssetID") = ""
        Session("AssetStationID") = ""
        Session("AssetMaintenanceID") = ""
        Session("AssetID") = txtAssetID.Text.ToString
        Session("AssetStationID") = ddlStation.SelectedValue.ToString

        Dim lbtnView As New LinkButton
        lbtnView = gvMaintenance.SelectedRow.FindControl("lbtnView")
        Session("AssetMaintenanceID") = lbtnView.CommandArgument.ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('frmAssetMaintenanceDetail.aspx?ParentForm=frmAssetDetail','AssetMaintenanceDetail','height=750, width=900,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    'Protected Sub btnOwner_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOwner.Click
    '    Dim strScript As String

    '    If hdfOwnerUserID.Value.ToString = "" Then
    '        Session("DA_USERLIST") = ""
    '        Session("DA_New_USERLIST") = ""
    '        Session("DA_USERLIST_Name") = ""
    '        Session("DA_USERLIST_TYPE") = "Owner"
    '    Else
    '        Session("DA_USERLIST_TYPE") = "Owner"
    '        Session("DA_USERLIST") = hdfOwnerUserID.Value.ToString
    '        Session("DA_USERLIST_Name") = txtOwner.Text.ToString
    '    End If

    '    Session("DA_AddUserStationID") = ""
    '    If ddlStation.SelectedValue.ToString <> "" Then
    '        Session("DA_AddUserStationID") = ddlStation.SelectedValue.ToString
    '    Else
    '        Session("DA_AddUserStationID") = Session("DA_StationID").ToString
    '    End If


    '    strScript = "<script language=javascript>"
    '    strScript += "theChild = window.open('../frmAddUser2.aspx?ParentForm=frmAssetDetail','AddOwner','height=550, width=550,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
    '    strScript += "</script>"
    '    Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)

    'End Sub

    Protected Sub ddlUserType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUserType.SelectedIndexChanged
        lbtnAddOwner.Visible = False
        txtOwner.Visible = False
        If ddlUserType.SelectedValue = "User" Then
            lbtnAddOwner.Visible = True
            txtOwner.Text = ""
            txtOwner.Visible = False
            gvUserList.Visible = True
            'If txtAssetID.Text.ToString = "" Then
            '    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter owner at User  List.')</script>")
            '    btnOwner.Enabled = False
            '    txtOwner.Enabled = False
            '    txtOwner.Text = ""
            '    hdfOwnerUserID.Value = ""
            'Else
            '    btnOwner.Enabled = True
            '    txtOwner.Enabled = False
            '    txtOwner.Text = ""
            '    hdfOwnerUserID.Value = ""
            'End If
        ElseIf ddlUserType.SelectedValue = "Office" Then
            txtOwner.Visible = True
            txtOwner.Enabled = True
            'btnOwner.Enabled = False
            txtOwner.Text = ""
            hdfOwnerUserID.Value = ""
            hdfUserList.Value = ""

            dsUseList.Clear()
            gvUserList.DataSource = dsUseList
            gvUserList.DataMember = dsUseList.TableName
            gvUserList.DataBind()
            gvUserList.Visible = False
            'Else
            '    txtOwner.Enabled = False
            '    btnOwner.Enabled = False
            '    txtOwner.Text = ""
            '    hdfOwnerUserID.Value = ""

            '    'lblMsg.Text = "Please select User Type."
            '    Exit Sub
        End If

        'If ddlUserType.SelectedValue = "User" Then
        '    btnOwner.Enabled = True
        '    txtOwner.Enabled = False
        '    txtOwner.Text = ""
        '    hdfOwnerUserID.Value = ""
        'ElseIf ddlUserType.SelectedValue = "Office" Then
        '    txtOwner.Enabled = True
        '    btnOwner.Enabled = False
        '    txtOwner.Text = ""
        '    hdfOwnerUserID.Value = ""
        'Else
        '    txtOwner.Enabled = False
        '    btnOwner.Enabled = False
        '    txtOwner.Text = ""
        '    hdfOwnerUserID.Value = ""

        '    'lblMsg.Text = "Please select User Type."
        '    Exit Sub
        'End If
    End Sub

    Protected Sub btnCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCategory.Click


        Dim strScript As String

        Session("DA_CategoryDepreciation_Period") = ""
        Session("DA_ParentCategoryID") = ""
        Session("DA_CategoryID") = ""
        Session("DA_Category") = ""

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmItem.aspx?ParentForm=frmAssetDetail','AddCategory','height=550, width=550,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub btnVendorPayee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVendorPayee.Click
        Dim strScript As String

        If hdfVendorPayeeID.Value.ToString = "" Then
            Session("DA_USERLIST") = ""
            Session("DA_New_USERLIST") = ""
            Session("DA_USERLIST_Name") = ""
            Session("DA_USERLIST_TYPE") = "VendorPayee"
        Else
            Session("DA_USERLIST_TYPE") = "VendorPayee"
            Session("DA_USERLIST") = hdfVendorPayeeID.Value.ToString
            Session("DA_USERLIST_Name") = lblVendorPayee.Text.ToString
        End If

        Session("DA_AddUserStationID") = ""
        Session("DA_AddUserStationID") = Session("DA_StationID").ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmPayee.aspx?ParentForm=frmAssetDetail','AddVendorPayee','height=550, width=550,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)



    End Sub

    Protected Sub btnLocApprovedBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLocApprovedBy.Click
        Dim strScript As String

        If hdfLocationApprovedBy.Value.ToString = "" Then
            Session("DA_USERLIST") = ""
            Session("DA_New_USERLIST") = ""
            Session("DA_USERLIST_Name") = ""
            Session("DA_USERLIST_TYPE") = "LocationApprovedBy"
        Else
            Session("DA_USERLIST_TYPE") = "LocationApprovedBy"
            Session("DA_USERLIST") = hdfLocationApprovedBy.Value.ToString
            Session("DA_USERLIST_Name") = lblLocationApprovedBy.Text.ToString
        End If

        Session("DA_AddUserStationID") = ""
        Session("DA_AddUserStationID") = Session("DA_StationID").ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmAddUser2.aspx?ParentForm=frmAssetDetail','AddLocationApprovedBy','height=550, width=550,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)

    End Sub

    Protected Sub btnCCParty_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCCParty.Click
        Dim strScript As String

        If hdfCCParty.Value.ToString = "" Then
            Session("DA_USERLIST") = ""
            Session("DA_New_USERLIST") = ""
            Session("DA_USERLIST_Name") = ""
            Session("DA_USERLIST_TYPE") = "CCParty"
        Else
            Session("DA_USERLIST_TYPE") = "CCParty"
            Session("DA_USERLIST") = hdfCCParty.Value.ToString
            Session("DA_USERLIST_Name") = lblCCParty.Text.ToString
        End If

        Session("DA_AddUserStationID") = ""
        Session("DA_AddUserStationID") = Session("DA_StationID").ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmAddUser.aspx?ParentForm=frmAssetDetail','AddCCParty','height=550, width=550,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)

    End Sub

    Protected Sub ddlStation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStation.SelectedIndexChanged
        GetStationLocation(ddlStation.SelectedValue.ToString)
        ChildAssetMaintainRelation()
        StationCurrency()
    End Sub

    Private Sub StationCurrency()
        clsCF.StationCurrencyID(ddlStation.SelectedValue.ToString, ddlCurrencyCode)
        clsCF.StationCurrencyID(ddlStation.SelectedValue.ToString, ddlOthCurrencyCode)
        'ddlCurrencyCode.SelectedValue = clsCF.StationCurrencyID(ddlStation.SelectedValue.ToString)
        'ddlOthCurrencyCode.SelectedValue = clsCF.StationCurrencyID(ddlStation.SelectedValue.ToString)
    End Sub

    Private Sub ChildAssetMaintainRelation()
        If gvChildAsset.Rows.Count > 0 Then
            If ddlStation.SelectedValue.ToString <> hdfOriginalStationID.Value.ToString Then
                gvChildAsset.Columns(1).Visible = True
                Exit Sub
            Else
                gvChildAsset.Columns(1).Visible = False
            End If
            If ddlLocation.SelectedValue.ToString <> hdfOriginalLocationID.Value.ToString Then
                gvChildAsset.Columns(1).Visible = True
                Exit Sub
            Else
                gvChildAsset.Columns(1).Visible = False
            End If
            If hdfOwnerUserID.Value.ToString <> hdfOriginalOwner.Value.ToString Then
                gvChildAsset.Columns(1).Visible = True
                Exit Sub
            Else
                gvChildAsset.Columns(1).Visible = False
            End If
        End If
    End Sub
        
    Private Sub GetStationLocation(ByVal strStation As String)
        Try
            If strStation = "" Then
                lblMsg.Text = "Please select Station"
                Exit Sub
            End If

            Dim dsLocation As DataSet = clsCF.GetStationLocation(strStation)
            ddlLocation.DataSource = dsLocation.Tables(0)
            ddlLocation.DataValueField = dsLocation.Tables(0).Columns("LocationID").ToString
            ddlLocation.DataTextField = dsLocation.Tables(0).Columns("Location").ToString
            ddlLocation.DataBind()

            'Dim dsStation As New DataSet
            'Dim MyData As New DataSet
            'Dim MyAdapter As SqlDataAdapter
            'Dim MyCommand As SqlCommand
            'Dim MyConnection As New SqlConnection(strConnectionString)

            'MyCommand = New SqlCommand("SP_GetReferenceData_ByStation")

            'With MyCommand
            '    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
            '    .Parameters("@ReSM").Value = strReSM
            '    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
            '    .Parameters("@ReferenceType").Value = "[Location]"
            '    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
            '    .Parameters("@StationID").Value = strStation
            '    .CommandType = CommandType.StoredProcedure
            '    .Connection = MyConnection
            'End With

            'MyAdapter = New SqlDataAdapter(MyCommand)
            'MyAdapter.Fill(MyData)
            'MyData.Tables(0).TableName = "Location"
            'ddlLocation.DataSource = MyData.Tables(0)
            'ddlLocation.DataValueField = MyData.Tables(0).Columns("LocationID").ToString
            'ddlLocation.DataTextField = MyData.Tables(0).Columns("Location").ToString
            'ddlLocation.DataBind()
           
        Catch ex As Exception
            Me.lblMsg.Text = "GetStationLocation: " & ex.Message.ToString
        Finally
            'sqlConn.Close()
        End Try
    End Sub

    Protected Sub btnSendeReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendeReq.Click
       
        Dim strEReqNo As String = ""

        Try
            lblMsg.Text = ""

            If Validate_eReqData() = False Then
                Exit Sub
            End If

            'SAVE ASSET DETAIL
            Dim strReturnMsg As String = ""

            strReturnMsg = SaveAssetData()
            If strReturnMsg <> "" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strReturnMsg + "')</script>")
                Exit Sub
            End If

            'CALL SEND EREQ 
            Dim strAction As String = ""
            If lbleRequisitionNo.Text.ToString = "" Then
                strAction = "Add"
            Else
                strAction = "Modify"
            End If

            'strReturnMsg = SaveeReq(txtAssetID.Text.ToString, strAction)
            'If strReturnMsg <> "" Then
            '    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strReturnMsg + "')</script>")
            '    Exit Sub
            'End If

            strReturnMsg = ""
            strReturnMsg = clsCF.SendeRequisition(txtAssetID.Text.ToString, strAction, Session("DA_UserID").ToString, hdfCurrentDateTime.Value.ToString())


            'Dim strBrand As String = ""
            'If txtBrand.Text.ToString <> "" Then
            '    strBrand = txtBrand.Text.ToString
            'Else
            '    strBrand = ddlBrand.SelectedItem.Text.ToString
            'End If

            'strReturnMsg = clsCF.SendeRequisition(strAction, lbleRequisitionNo.Text.ToString, ddlCondition.SelectedValue.ToString, _
            'hdfOwnerUserID.Value.ToString(), chkInternalAudit.Checked, ddlJobFunction.SelectedValue.ToString, _
            'chkMIS_Seed_Member.Checked, hdfParentCategory.Value.ToString, lblCategory.Text.ToString, _
            'txtAppToBeUsed.Text.ToString, hdfVendorDetail.Value.ToString, _
            'strBrand, txtModel.Text.ToString, CInt(txtAvailInInventory.Text.ToString), Convert.ToDateTime(cldRequiredDate.SelectedDate).ToString, _
            'CInt(txtQuantity.Text.ToString), ddlCurrencyCode.SelectedItem.Text.ToString, txtCurrencyRate.Text.ToString, _
            'txtPurchaseBudgeted.Text.ToString, txtPurchaseCost.Text.ToString, txtRemarks.Text.ToString, lblLocationApprovedBy.Text.ToString, _
            'hdfCCParty.Value.ToString, hdfCreatedBy.Value.ToString, gvSpec, Session("DA_UserID").ToString, _
            'hdfCurrentDateTime.Value.ToString(), txtAssetID.Text.ToString)

            If strReturnMsg = "" Then
                GetAssetDetail()
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Send eRequisition Successful')</script>")
            Else
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strReturnMsg + "')</script>")
            End If

        Catch ex As Exception
            Me.lblMsg.Text = "btnSendeReq_Click: " & ex.Message.ToString
        Finally
            'sqlConn.Close()
        End Try

    End Sub

    Private Function Validate_eReqData() As Boolean
        Dim blnValidate As Boolean = True

        If ddlStation.SelectedValue.ToString = "" Then
            blnValidate = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Station')</script>")
            ddlStation.Focus()
            Exit Function
        End If
        If ddlCondition.SelectedValue.ToString = "" Then
            blnValidate = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Condition')</script>")
            ddlCondition.Focus()
            Exit Function
        End If
        If ddlLocation.SelectedValue.ToString = "0" Then
            blnValidate = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Location')</script>")
            ddlLocation.Focus()
            Exit Function
        End If

        If ddlUserType.SelectedValue.ToString <> "Select One" Then
            If ddlUserType.SelectedValue.ToString = "Office" Then
                blnValidate = False
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('eRequisition Item must provide User Name.')</script>")
                txtOwner.Focus()
                Exit Function
            End If

            If ddlUserType.SelectedValue.ToString = "User" Then
                If gvUserList.Rows.Count <= 0 Then
                    blnValidate = False
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please Select Owner.')</script>")
                    lbtnAddOwner.Focus()
                    Exit Function
                End If

                'blnValidate = False
                'ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Owner.')</script>")
                'btnOwner.Focus()
                'Exit Function

                Dim ddlJobFunction_gv As New DropDownList
                ddlJobFunction_gv = gvUserList.Rows(0).FindControl("ddlJobFunction")

                If ddlJobFunction_gv.SelectedValue.ToString = "" Then
                    blnValidate = False
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please Select Job Function.')</script>")
                    Exit Function
                End If
            End If
        Else
            blnValidate = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Owner Type')</script>")
            ddlUserType.Focus()
            Exit Function
        End If

        If ddlBrand.Items.Count = 1 Then
            If txtBrand.Text.ToString = "" Then
                blnValidate = False
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Brand')</script>")
                txtBrand.Focus()
                Exit Function
            End If
        Else
            If ddlBrand.SelectedValue = "0" And txtBrand.Text.ToString = "" Then
                blnValidate = False
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter/select Brand')</script>")
                txtBrand.Focus()
                Exit Function
            End If
        End If

        If txtModel.Text.ToString = "" Then
            blnValidate = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Model')</script>")
            txtModel.Focus()
            Exit Function
        End If

        If hdfCategoryID.Value.ToString = "" Then
            blnValidate = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Category')</script>")
            btnCategory.Focus()
            Exit Function
        End If

        Dim strRequiredDate As String = Format(cldRequiredDate.SelectedDate, "dd MMM yyyy").ToString
        If strRequiredDate.Contains("01 Jan 0001") = True Then
            blnValidate = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Required Date')</script>")
            cldRequiredDate.Focus()
            Exit Function
        End If

        If hdfVendorPayeeID.Value.ToString = "" Then
            blnValidate = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Vendor/Payee')</script>")
            btnVendorPayee.Focus()
            Exit Function
        End If

        If txtAvailInInventory.Text.ToString = "" Then
            blnValidate = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Availability In Inventory')</script>")
            txtAvailInInventory.Focus()
            Exit Function
        End If

        If txtPurchaseCost.Text.ToString = "" Then
            blnValidate = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Purchase Cost (Quoted Amount)')</script>")
            txtPurchaseCost.Focus()
            Exit Function
        End If

        If txtPurchaseBudgeted.Text.ToString = "" Then
            blnValidate = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Purchase Cost (Budgeted Amount)')</script>")
            txtPurchaseBudgeted.Focus()
            Exit Function
        End If

        If txtCurrencyRate.Text.ToString = "" Then
            blnValidate = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Currency Rate')</script>")
            txtCurrencyRate.Focus()
            Exit Function
        End If

        If hdfLocationApprovedBy.Value.ToString = "" Then
            blnValidate = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please Approved By')</script>")
            btnLocApprovedBy.Focus()
            Exit Function
        End If

        'If hdfCCParty.Value.ToString = "" Then
        '    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter eMail CC Party')</script>")
        '    blnValidate = False
        '    Exit Function
        'End If

        If txtQuantity.Text.ToString = "" Then
            blnValidate = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Quantity')</script>")
            txtQuantity.Focus()
            Exit Function
        End If

        If txtRemarks.Text.ToString = "" Then
            blnValidate = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Remarks')</script>")
            txtRemarks.Focus()
            Exit Function
        End If

        If gvSpec.Rows.Count < 1 Then
            blnValidate = False
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Specification')</script>")
            btnSpecification.Focus()
            Exit Function
        End If

        Return blnValidate

    End Function

    'Private Sub SendeReq(ByVal strSendeReqStatus As String)
    '    Dim sqlConn As New SqlConnection(strConnectionString)
    '    Dim strMsg As String = ""

    '    Try
    '        sqlConn.Open()
    '        Dim cmd As New SqlCommand("SP_SendeReqStatus", sqlConn)
    '        With cmd
    '            .CommandType = CommandType.StoredProcedure
    '            .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
    '            .Parameters("@UserID").Value = Session("DA_UserID").ToString
    '            .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
    '            .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

    '            .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
    '            .Parameters("@AssetID").Value = txtAssetID.Text.ToString
    '            .Parameters.Add("@eReq_Status", SqlDbType.VarChar, 1)
    '            .Parameters("@eReq_Status").Value = strSendeReqStatus
    '            .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
    '            .Parameters("@Msg").Direction = ParameterDirection.Output

    '            .CommandTimeout = 0
    '            .ExecuteNonQuery()

    '            strMsg = .Parameters("@Msg").Value.ToString()
    '        End With

    '        If strMsg <> "" Then
    '            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
    '        Else
    '            If strAction = "Delete" Then
    '                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete eRequisition Successful')</script>")
    '            Else
    '                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Send eRequisition Successful')</script>")
    '            End If
    '        End If

    '    Catch ex As Exception
    '        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + ex.Message.ToString + "')</script>")
    '        Exit Sub
    '    Finally
    '        sqlConn.Close()
    '    End Try
    'End Sub

    'Private Sub SendeRequisition(ByVal strAction As String)
    '    Dim strEReqNo As String = ""
    '    Dim strEReq_ErrMessage As String = ""
    '    Dim sqlConn As New SqlConnection(strConnectionString)
    '    Dim strMsg As String = ""


    '    Try
    '        'eRequisition Header
    '        Dim entity As New eAsset.EntityRequisition
    '        entity = New eAsset.EntityRequisition()

    '        entity.ControlID = lbleRequisitionNo.Text.ToString

    '        entity.Action = strAction
    '        entity.Condition = ddlCondition.SelectedValue.ToString

    '        entity.UserID = hdfOwnerUserID.Value.ToString
    '        If chkInternalAudit.Checked = True Then
    '            entity.Internal_Audit = True
    '        Else
    '            entity.Internal_Audit = False
    '        End If

    '        entity.JobFunction = ddlJobFunction.SelectedValue.ToString

    '        If chkMIS_Seed_Member.Checked = True Then
    '            entity.MIS_Seed_Member = True
    '        Else
    '            entity.MIS_Seed_Member = False
    '        End If

    '        entity.Category = hdfParentCategory.Value.ToString
    '        entity.Type = lblCategory.Text.ToString

    '        entity.Applications_to_be_Used = txtAppToBeUsed.Text.ToString
    '        entity.Vendor = hdfVendorDetail.Value.ToString

    '        If txtBrand.Text.ToString <> "" Then
    '            entity.Brand_Name = txtBrand.Text.ToString
    '        Else
    '            entity.Brand_Name = ddlBrand.SelectedItem.Text.ToString
    '        End If

    '        entity.Module = txtModel.Text.ToString
    '        entity.Availability_in_Inventory = CInt(txtAvailInInventory.Text.ToString)
    '        entity.Require_Date = Convert.ToDateTime(cldRequiredDate.SelectedDate)
    '        entity.Number_of_Units = CInt(txtQuantity.Text.ToString)
    '        entity.Currency = ddlCurrencyCode.SelectedItem.Text.ToString
    '        entity.Rate = CDec(txtCurrencyRate.Text.ToString)
    '        entity.Budget = CDec(txtPurchaseBudgeted.Text.ToString)
    '        entity.Quote = CDec(txtPurchaseCost.Text.ToString)
    '        entity.Remark = txtRemarks.Text.ToString
    '        entity.LocalApprovedBy = lblLocationApprovedBy.Text.ToString
    '        entity.eMailCCParty = Replace(hdfCCParty.Value.ToString, ",", ";")
    '        entity.CreatedBy = hdfCreatedBy.Value.ToString


    '        '' ''eRequisition Item Detail (Specification)
    '        Dim entityItem As New eAsset.EntityItem
    '        Dim items As New System.Collections.Generic.List(Of eAsset.EntityItem)
    '        Dim i As Integer
    '        Dim lblSpec As New Label
    '        Dim lblQty As New Label

    '        For i = 0 To gvSpec.Rows.Count - 1
    '            lblSpec = gvSpec.Rows(i).FindControl("lblSpec")
    '            lblQty = gvSpec.Rows(i).FindControl("lblQty")

    '            entityItem.CategoryCode = lblCategory.Text.ToString
    '            entityItem.ItemDesc = lblSpec.Text.ToString
    '            entityItem.ItemQty = CInt(lblQty.Text.ToString)
    '            items.Add(entityItem)
    '        Next

    '        'eRequisition Return
    '        Dim c As New eAsset.LeaveApplication
    '        Dim ret As New eAsset.eRequisitionReturn
    '        ret = c.Send2eRequisition(strRightCode, entity, items.ToArray())

    '        '// ret.Result is a token of return value, and need saved in your DB, so that you can do the delete function before eRequisition Approved. 
    '        If ret.Result.ToString <> "" Then
    '            strEReqNo = ret.Result.ToString
    '            'SendeRequisition = True
    '        End If

    '        If ret.Message.ToString <> "" Then
    '            'SendeRequisition = False
    '            strEReqNo = ""
    '            strEReq_ErrMessage = ret.Message.ToString
    '            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strEReq_ErrMessage + "')</script>")
    '            'Return SendeRequisition
    '            Exit Sub
    '        End If

    '        'strEReqNo = "DIMPEN02700001"


    '        If strEReqNo <> "" Then
    '            lbleRequisitionNo.Text = strEReqNo

    '            'UPDATE DATA AFTER SEND EREQ SUCCESSFUL
    '            sqlConn.Open()
    '            Dim cmd As New SqlCommand("SP_Update_Send_eReq", sqlConn)
    '            With cmd
    '                .CommandType = CommandType.StoredProcedure
    '                .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
    '                .Parameters("@UserID").Value = Session("DA_UserID").ToString
    '                .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
    '                .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

    '                .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
    '                .Parameters("@AssetID").Value = txtAssetID.Text.ToString
    '                .Parameters.Add("@eRequisitionNo", SqlDbType.VarChar, 20)
    '                .Parameters("@eRequisitionNo").Value = strEReqNo
    '                .Parameters.Add("@eReq_Status", SqlDbType.VarChar, 1)
    '                If strAction = "Delete" Then
    '                    .Parameters("@eReq_Status").Value = "C"
    '                Else
    '                    .Parameters("@eReq_Status").Value = "P"
    '                End If

    '                .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
    '                .Parameters("@Msg").Direction = ParameterDirection.Output

    '                .CommandTimeout = 0
    '                .ExecuteNonQuery()

    '                strMsg = .Parameters("@Msg").Value.ToString()
    '            End With

    '            If strMsg <> "" Then
    '                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
    '            Else
    '                GetAssetDetail()
    '                If strAction = "Delete" Then
    '                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete eRequisition Successful')</script>")
    '                Else
    '                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Send eRequisition Successful')</script>")
    '                End If

    '            End If
    '        End If

    '        'Return SendeRequisition

    '    Catch ex As Exception
    '        strEReqNo = ""
    '        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + ex.Message.ToString + "')</script>")
    '        Exit Sub
    '    Finally
    '        sqlConn.Close()
    '    End Try
    'End Sub

    Protected Sub btnCanceleReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCanceleReq.Click

        ''SAVE ASSET DETAIL
        'Dim strReturnMsg As String = ""
        'strReturnMsg = SaveAssetData()
        'If strReturnMsg <> "" Then
        '    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strReturnMsg + "')</script>")
        '    Exit Sub
        'End If

        ''CALL SEND EREQ 
        'SendeRequisition("Delete")



        Dim strAction As String = ""
        Dim strReturnMsg As String = ""
        strAction = "Delete"

        'strReturnMsg = SaveeReq(txtAssetID.Text.ToString, strAction)
        'If strReturnMsg <> "" Then
        '    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strReturnMsg + "')</script>")
        '    Exit Sub
        'End If

        strReturnMsg = ""

        'strReturnMsg = clsCF.CancelRequisition(txtAssetID.Text.ToString(), lbleRequisitionNo.Text.ToString, Session("DA_UserID").ToString, hdfCurrentDateTime.Value.ToString())
        strReturnMsg = clsCF.SendeRequisition(txtAssetID.Text.ToString(), strAction, Session("DA_UserID").ToString, hdfCurrentDateTime.Value.ToString())

        'Dim strBrand As String = ""
        'If txtBrand.Text.ToString <> "" Then
        '    strBrand = txtBrand.Text.ToString
        'Else
        '    strBrand = ddlBrand.SelectedItem.Text.ToString
        'End If

        'strReturnMsg = clsCF.SendeRequisition(strAction, lbleRequisitionNo.Text.ToString, ddlCondition.SelectedValue.ToString, _
        'hdfOwnerUserID.Value.ToString(), chkInternalAudit.Checked.ToString, ddlJobFunction.SelectedValue.ToString, _
        'chkMIS_Seed_Member.Checked, hdfParentCategory.Value.ToString, lblCategory.Text.ToString, _
        'txtAppToBeUsed.Text.ToString, hdfVendorDetail.Value.ToString, _
        'strBrand, txtModel.Text.ToString, CInt(txtAvailInInventory.Text.ToString), Convert.ToDateTime(cldRequiredDate.SelectedDate), _
        'CInt(txtQuantity.Text.ToString), ddlCurrencyCode.SelectedItem.Text.ToString, txtCurrencyRate.Text.ToString, _
        'txtPurchaseBudgeted.Text.ToString, txtPurchaseCost.Text.ToString, txtRemarks.Text.ToString, lblLocationApprovedBy.Text.ToString, _
        'hdfCCParty.Value.ToString, hdfCreatedBy.Value.ToString, gvSpec, Session("DA_UserID").ToString, hdfCurrentDateTime.Value.ToString(), _
        'txtAssetID.Text.ToString)

        If strReturnMsg = "" Then
            GetAssetDetail()
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Cancel eRequisition Successful')</script>")
        Else
            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strReturnMsg + "')</script>")
        End If

    End Sub

    Protected Sub btnPosteFMS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosteFMS.Click
        Dim strAction As String = ""
        Dim strAssetDescription As String = ""

        Try

            If ddlStation.SelectedValue.ToString = "" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Station.')</script>")
                ddlStation.Focus()
                Exit Sub
            End If

            If ddlAssetClassCOA.SelectedValue.ToString = "" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Asset Class COA.')</script>")
                ddlAssetClassCOA.Focus()
                Exit Sub
            End If

            If lblCategory.Text.ToString = "" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Category.')</script>")
                btnCategory.Focus()
                Exit Sub
            End If

            If txtQuantity.Text.ToString = "" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Quantity.')</script>")
                txtQuantity.Focus()
                Exit Sub
            End If

            If CInt(txtQuantity.Text.ToString) = 0 Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Quantity.')</script>")
                txtQuantity.Focus()
                Exit Sub
            End If

            If txtUnit.Text.ToString = "" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Unit.')</script>")
                txtUnit.Focus()
                Exit Sub
            End If

            Dim strPurchaseDate As String = Format(cldPurchaseDate.SelectedDate, "dd MMM yyyy").ToString
            If strPurchaseDate.Contains("01 Jan 0001") = True Then
                strPurchaseDate = ""
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Purchase Date.')</script>")
                cldPurchaseDate.Focus()
                Exit Sub
            End If

            Dim strDepreciationEffective As String = Format(cldDepreciationEffective.SelectedDate, "dd MMM yyyy").ToString
            If strDepreciationEffective.Contains("01 Jan 0001") = True Then
                strDepreciationEffective = ""
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Depreciation Effective Date.')</script>")
                cldDepreciationEffective.Focus()
                Exit Sub
            End If

            If CDate(strDepreciationEffective) < CDate(strPurchaseDate) Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Depreciation Effective.')</script>")
                cldDepreciationEffective.Focus()
                Exit Sub
            End If

            If ddlCurrencyCode.SelectedValue.ToString = "" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Purchase Cost Currency Code.')</script>")
                ddlCurrencyCode.Focus()
                Exit Sub
            End If

            If txtPurchaseCost.Text.ToString = "" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Purchase Cost.')</script>")
                txtPurchaseCost.Focus()
                Exit Sub
            End If

            If txtDepreciationPeriod.Text.ToString = "" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Depreciation Period.')</script>")
                txtDepreciationPeriod.Focus()
                Exit Sub
            End If

            If CInt(txtDepreciationPeriod.Text.ToString) = 0 Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Depreciation Period.')</script>")
                txtDepreciationPeriod.Focus()
                Exit Sub
            End If

            If txtResidualValue.Text.ToString = "" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Residual Value.')</script>")
                txtResidualValue.Focus()
                Exit Sub
            End If


            'If CDec(txtResidualValue.Text.ToString) = 0 Then
            '    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Residual Value.')</script>")
            '    txtResidualValue.Focus()
            '    Exit Sub
            'End If

            If ddlAccDepreciationCOA.SelectedValue.ToString = "" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Accumulated Depreciation COA.')</script>")
                ddlAccDepreciationCOA.Focus()
                Exit Sub
            End If

            If txtVAT.Text.ToString = "" Then
                txtVAT.Text = "0.00"
            End If

            If txtDiscount.Text.ToString = "" Then
                txtDiscount.Text = "0.00"
            End If

            Dim strReturnMsg_Save As String = ""
            strReturnMsg_Save = SaveAssetData()
            If strReturnMsg_Save <> "" Then
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strReturnMsg_Save + "')</script>")
                Exit Sub
            End If

            If hdfeFMS_Status.Value.ToString = "D" Then
                strAction = "N"
            ElseIf hdfeFMS_Status.Value.ToString = "S" Then
                If hdfeFMS_ACK.Value.ToString = "0" Then
                    strAction = "U"
                ElseIf hdfeFMS_ACK.Value.ToString = "" Then
                    strAction = "N"
                End If
            End If

            If ddlBrand.SelectedValue.ToString <> "" Then
                strAssetDescription = clsCF.TrimAll(ddlBrand.SelectedItem.Text.ToString + " " + txtModel.Text.ToString)
            Else
                strAssetDescription = clsCF.TrimAll(txtBrand.Text.ToString + " " + txtModel.Text.ToString)
            End If

            'CREATE DATATABLE AND COLUMNS
            Dim dtPosteFMS As DataTable = New DataTable("PosteFMS")
            dtPosteFMS.Columns.Add("StationID", Type.GetType("System.String"))
            dtPosteFMS.Columns.Add("AssetID", Type.GetType("System.String"))
            dtPosteFMS.Columns.Add("Action", Type.GetType("System.String"))
            dtPosteFMS.Columns.Add("AssetClass", Type.GetType("System.String"))

            dtPosteFMS.Columns.Add("AssetName", Type.GetType("System.String"))
            dtPosteFMS.Columns.Add("AssetDescription", Type.GetType("System.String"))
            dtPosteFMS.Columns.Add("Owner", Type.GetType("System.String"))
            dtPosteFMS.Columns.Add("Location", Type.GetType("System.String"))
            dtPosteFMS.Columns.Add("Quantity", Type.GetType("System.Int32"))

            dtPosteFMS.Columns.Add("Unit", Type.GetType("System.String"))
            dtPosteFMS.Columns.Add("PurchaseDate", Type.GetType("System.DateTime"))
            dtPosteFMS.Columns.Add("DepreciationEffective", Type.GetType("System.DateTime"))
            dtPosteFMS.Columns.Add("OriginalPurchaseCostCoins", Type.GetType("System.String"))
            dtPosteFMS.Columns.Add("OriginalPurchaseCost", Type.GetType("System.Decimal"))

            dtPosteFMS.Columns.Add("UsefulLifeByMonth", Type.GetType("System.Int32"))
            dtPosteFMS.Columns.Add("ResidualValue", Type.GetType("System.Decimal"))
            dtPosteFMS.Columns.Add("AccumulatedDepreciationCOA", Type.GetType("System.String"))
            dtPosteFMS.Columns.Add("CreateUser", Type.GetType("System.String"))
            dtPosteFMS.Columns.Add("GroupID", Type.GetType("System.String"))


            'ADD DATA INTO DATATABLE
            dtPosteFMS.Rows.Add()
            dtPosteFMS.Rows(0)("StationID") = ddlStation.SelectedValue.ToString
            dtPosteFMS.Rows(0)("AssetID") = txtAssetID.Text.ToString
            dtPosteFMS.Rows(0)("Action") = strAction
            dtPosteFMS.Rows(0)("AssetClass") = ddlAssetClassCOA.SelectedValue.ToString

            If lblSpecialReqCategory.Visible = False Then
                dtPosteFMS.Rows(0)("AssetName") = lblCategory.Text.ToString
            Else
                dtPosteFMS.Rows(0)("AssetName") = lblSpecialReqCategory.Text.ToString
            End If


            If CInt(txtQuantity.Text.ToString) > 1 Then
                dtPosteFMS.Rows(0)("AssetDescription") = strAssetDescription + " (" + txtQuantity.Text.ToString + " " + txtUnit.Text.ToString + ")"
            Else
                dtPosteFMS.Rows(0)("AssetDescription") = strAssetDescription
            End If

            Dim strOwner As String = ""

            If gvUserList.Rows.Count > 0 Then
                Dim lblName_gv As New Label
                lblName_gv = gvUserList.Rows(0).FindControl("lblName")
                strOwner = lblName_gv.Text.ToString
            Else
                strOwner = txtOwner.Text.ToString
            End If

            dtPosteFMS.Rows(0)("Owner") = strOwner
            dtPosteFMS.Rows(0)("Location") = Left(ddlLocation.SelectedItem.Text.ToString, 10)
            dtPosteFMS.Rows(0)("Quantity") = 1 'CInt(txtQuantity.Text.ToString)

            'If CInt(txtQuantity.Text.ToString) > 1 Then
            '    dtPosteFMS.Rows(0)("Unit") = txtUnit.Text.ToString + " (" + txtQuantity.Text.ToString + ")"
            'Else
            dtPosteFMS.Rows(0)("Unit") = txtUnit.Text.ToString
            'End If

            dtPosteFMS.Rows(0)("PurchaseDate") = strPurchaseDate
            dtPosteFMS.Rows(0)("DepreciationEffective") = strDepreciationEffective
            dtPosteFMS.Rows(0)("OriginalPurchaseCostCoins") = ddlCurrencyCode.SelectedItem.Text.ToString
            'dtPosteFMS.Rows(0)("OriginalPurchaseCost") = CDec(txtPurchaseCost.Text.ToString) * CInt(txtQuantity.Text.ToString)
            dtPosteFMS.Rows(0)("OriginalPurchaseCost") = (CDec(txtPurchaseCost.Text.ToString) - CDec(txtVAT.Text.ToString) - CDec(txtDiscount.Text.ToString)) * CInt(txtQuantity.Text.ToString)

            dtPosteFMS.Rows(0)("UsefulLifeByMonth") = CInt(txtDepreciationPeriod.Text.ToString)
            dtPosteFMS.Rows(0)("ResidualValue") = CDec(txtResidualValue.Text.ToString)
            dtPosteFMS.Rows(0)("AccumulatedDepreciationCOA") = ddlAccDepreciationCOA.SelectedValue.ToString
            dtPosteFMS.Rows(0)("CreateUser") = Session("DA_UserID").ToString
            dtPosteFMS.Rows(0)("GroupID") = txtAssetNo.Text.ToString



            Dim dsPosteFMS As DataSet = New DataSet("PosteFMS")
            dsPosteFMS.Tables.Add(dtPosteFMS)
            Dim strPostMsg As String = ""


            strPostMsg = clsNewAsset.SendNewAssetstoStation(Session("DA_UserID").ToString, ddlStation.SelectedValue.ToString, dsPosteFMS)

            'strPostMsg = "eAsset Send to Station Success"
            If strPostMsg.Contains("eAsset Send to Station Success") = True Then
                Dim strReturnMsg As String = UpdatePosteFMSDetail()
                If strReturnMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "UpdatePosteFMSDetail - Error: ", "<script>alert('" + strReturnMsg + "')</script>")
                Else
                    ClientScript.RegisterStartupScript(Me.GetType(), "PostMsg", "<script>alert('Asset Send to Station Success')</script>")
                    GetAssetDetail()
                End If
            Else
                ClientScript.RegisterStartupScript(Me.GetType(), "PostErrorMsg", "<script>alert('" + strPostMsg + "')</script>")
            End If

        Catch ex As Exception
            ClientScript.RegisterStartupScript(Me.GetType(), "PostErrorMsg", "<script>alert('" + ex.Message.ToString + "')</script>")
        End Try

    End Sub

    Private Function UpdatePosteFMSDetail() As String
        Using sqlConn As New SqlConnection(strConnectionString)

            Dim strMsg As String = ""
            Dim strReturnMsg As String = ""

            Try
                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_UpdatePosteFMSDetail", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 50)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                    .Parameters.Add("@PostBy", SqlDbType.VarChar, 6)
                    .Parameters("@PostBy").Value = Session("DA_UserID").ToString

                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output

                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    strReturnMsg = strMsg
                Else
                    GetAssetDetail()
                    strReturnMsg = ""
                End If

                Return strReturnMsg
            Catch ex As Exception
                strReturnMsg = ex.Message.ToString
                Return strReturnMsg
                'ClientScript.RegisterStartupScript(Me.GetType(), "UpdatePosteFMSDetail", "<script>alert('" + ex.Message.ToString + "')</script>")
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Function

    Private Function SaveeReq(ByVal strAssetID As String, ByVal strAction As String) As String

        Using sqlConn As New SqlConnection(strConnectionString)

            Dim strMsg As String = ""

            Try
                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_Save_eReq", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = strAssetID
                    .Parameters.Add("@Action", SqlDbType.VarChar, 20)
                    .Parameters("@Action").Value = strAction

                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString

                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString

                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output

                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    Return strMsg
                End If

                Return strMsg

            Catch ex As Exception
                Return "Update_eReqACK: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using

    End Function

    'Private Sub Update_eReqACK()
    '    Dim strResult As String = ""
    '    Dim strErrMessage As String = ""
    '    Dim strApprovedBy As String = ""
    '    Dim strApprovedOn As String = ""
    '    Dim strApprovalRemarks As String = ""

    '    Dim strMsg As String = ""
    '    Dim sqlConn As New SqlConnection(strConnectionString)

    '    Try
    '        'eRequisition Return
    '        Dim c As New eAsset.LeaveApplication
    '        Dim ret As New eAsset.eRequisitionReturn
    '        ret = c.CheckeRequisitionStatus(strRightCode, lbleRequisitionNo.Text.ToString)


    '        If ret.Result.ToString <> "" Then
    '            strResult = ret.Result.ToString
    '        End If

    '        If ret.Message.ToString <> "" Then
    '            strErrMessage = ret.Message.ToString
    '            strApprovedBy = clsCF.SplitString(strErrMessage, "<By>", "</By>")
    '            strApprovedOn = clsCF.SplitString(strErrMessage, "<On>", "</On>")
    '            strApprovalRemarks = clsCF.SplitString(strErrMessage, "<Remark>", "</Remark>")
    '        End If

    '        Dim streReq_Status As String = ""
    '        If strResult = "Approved" Then
    '            streReq_Status = "A"
    '        ElseIf strResult = "Reject" Then
    '            streReq_Status = "R"
    '        ElseIf strResult = "Modify" Then
    '            streReq_Status = "W"
    '        ElseIf strResult = "Pending" Then
    '            streReq_Status = "P"
    '        End If




    '        sqlConn.Open()
    '        Dim cmd As New SqlCommand("SP_Update_eReqACK", sqlConn)
    '        With cmd
    '            .CommandType = CommandType.StoredProcedure
    '            .Parameters.Add("@eRequisitionNo", SqlDbType.VarChar, 50)
    '            .Parameters("@eRequisitionNo").Value = lbleRequisitionNo.Text.ToString
    '            .Parameters.Add("@eReq_Status", SqlDbType.VarChar, 1)
    '            .Parameters("@eReq_Status").Value = streReq_Status

    '            .Parameters.Add("@eReq_ApprovedBy", SqlDbType.VarChar, 6)
    '            .Parameters("@eReq_ApprovedBy").Value = strApprovedBy

    '            .Parameters.Add("@eReq_ApprovedOn", SqlDbType.NVarChar, 50)
    '            .Parameters("@eReq_ApprovedOn").Value = strApprovedOn

    '            .Parameters.Add("@eReq_Comments", SqlDbType.NVarChar)
    '            .Parameters("@eReq_Comments").Value = strApprovalRemarks


    '            .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
    '            .Parameters("@Msg").Direction = ParameterDirection.Output

    '            .CommandTimeout = 0
    '            .ExecuteNonQuery()

    '            strMsg = .Parameters("@Msg").Value.ToString()
    '        End With

    '        If strMsg <> "" Then
    '            lblMsg.Text = strMsg
    '        Else
    '            'GetAssetDetail()
    '            'lblMsg.Text = "Update Successful"
    '        End If
    '    Catch ex As Exception
    '        Me.lblMsg.Text = "Update_eReqACK: " & ex.Message.ToString
    '    Finally
    '        sqlConn.Close()
    '    End Try
    'End Sub

    'Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
    '    Update_eReqACK("A")
    'End Sub

    'Private Sub Update_eReqACK(ByVal strStatus As String)
    '    Dim strMsg As String = ""
    '    Dim sqlConn As New SqlConnection(strConnectionString)
    '    Try
    '        sqlConn.Open()
    '        Dim cmd As New SqlCommand("SP_Update_eReqACK", sqlConn)
    '        With cmd
    '            .CommandType = CommandType.StoredProcedure
    '            .Parameters.Add("@eRequisitionNo", SqlDbType.VarChar, 50)
    '            .Parameters("@eRequisitionNo").Value = lbleRequisitionNo.Text.ToString
    '            .Parameters.Add("@eReq_Status", SqlDbType.VarChar, 1)
    '            .Parameters("@eReq_Status").Value = strStatus

    '            .Parameters.Add("@eReq_ApprovedBy", SqlDbType.VarChar, 6)
    '            .Parameters("@eReq_ApprovedBy").Value = "A0282"

    '            .Parameters.Add("@eReq_ApprovedOn", SqlDbType.NVarChar, 50)
    '            .Parameters("@eReq_ApprovedOn").Value = hdfCurrentDateTime.Value.ToString()

    '            .Parameters.Add("@eReq_Comments", SqlDbType.NVarChar)
    '            .Parameters("@eReq_Comments").Value = "Comment"


    '            .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
    '            .Parameters("@Msg").Direction = ParameterDirection.Output

    '            .CommandTimeout = 0
    '            .ExecuteNonQuery()

    '            strMsg = .Parameters("@Msg").Value.ToString()
    '        End With

    '        If strMsg <> "" Then
    '            lblMsg.Text = strMsg
    '        Else
    '            GetAssetDetail()
    '            lblMsg.Text = "Update Successful"
    '        End If
    '    Catch ex As Exception
    '        Me.lblMsg.Text = "Update_eReqACK: " & ex.Message.ToString
    '    Finally
    '        sqlConn.Close()
    '    End Try
    'End Sub

    'Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
    '    Update_eReqACK("R")
    'End Sub

    'Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
    '    Update_eReqACK("W")
    'End Sub

    Protected Sub btnAddQuoReceipt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddQuoReceipt.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                'Save Quotation File into db
                Dim i As Integer = 0
                Dim strFileName As String = ""
                Dim strFilePath As String = ""
                Dim strMsg As String = ""
                Dim blnContains As Boolean = False

                If fuBrowse.FileName <> "" Then
                    If (Regex.IsMatch(fuBrowse.FileName, strFileExtension, RegexOptions.IgnoreCase)) = False Then
                        lblMsg.Text = "Invalid file format."
                        Exit Sub
                    End If

                    ' Get the size in bytes of the file to upload.
                    Dim fileSize As Integer = fuBrowse.PostedFile.ContentLength

                    ' Allow only files less than 2,100,000 bytes (approximately 2 MB) to be uploaded.
                    'If (fileSize < 2100000) Then
                    If (fileSize < intFileSizeLimitKB) Then

                        clsCF.CheckConnectionState(sqlConn)

                        Dim cmd As New SqlCommand("SP_SaveAssetQuoReceiptFile", sqlConn)
                        With cmd
                            .CommandType = CommandType.StoredProcedure
                            .Parameters.Add("@AssetID", SqlDbType.NVarChar, 50)
                            .Parameters("@AssetID").Value = txtAssetID.Text.ToString

                            .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                            .Parameters("@UserID").Value = Session("DA_UserID").ToString
                            .Parameters.Add("@FileName", SqlDbType.NVarChar, 500)
                            .Parameters("@FileName").Value = clsCF.RenameFileName(fuBrowse.FileName.ToString)
                            .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                            .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                            .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                            .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString

                            .Parameters.Add("@FilePath", SqlDbType.NVarChar, 500)
                            .Parameters("@FilePath").Direction = ParameterDirection.Output
                            .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                            .Parameters("@Msg").Direction = ParameterDirection.Output
                            .CommandTimeout = 0
                            .ExecuteNonQuery()

                            strMsg = .Parameters("@Msg").Value.ToString()
                            strFilePath = .Parameters("@FilePath").Value.ToString()
                        End With

                        If strMsg <> "" Then
                            'lblMsg.Text = strMsg
                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                        Else
                            If strFilePath <> "" Then

                                'strAlbumPath = hfServerPath.Value
                                Dim fn As String = ""
                                fn = System.IO.Path.GetFileName(fuBrowse.PostedFile.FileName)

                                If fn <> "" Then
                                    If FileIO.FileSystem.DirectoryExists(strFilePath) = False Then
                                        FileIO.FileSystem.CreateDirectory(strFilePath)
                                    End If

                                    If FileIO.FileSystem.FileExists(strFilePath + "/" + fn) = False Then
                                        fuBrowse.PostedFile.SaveAs(strFilePath + "/" + fn)
                                    Else
                                        FileIO.FileSystem.DeleteFile(strFilePath + "/" + fn)
                                        fuBrowse.PostedFile.SaveAs(strFilePath + "/" + fn)
                                    End If
                                End If
                            End If

                            ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Add Successful')</script>")
                            GetAssetQuoReceiptFile()
                        End If
                    Else
                        lblMsg.Text = "Your file was not uploaded because it exceeds the " & intFileSizeLimit & " MB size limit."
                    End If
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnAddQuoReceipt_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        ChildAssetMaintainRelation()
    End Sub

    Private Function CheckUserAsset() As String

        Dim lbtnRemove_gv As New LinkButton
        Dim strOwnerUserID As String = ""
        Dim strUser As String
        Dim i As Integer
        For i = 0 To gvUserList.Rows.Count - 1
            lbtnRemove_gv = gvUserList.Rows(i).FindControl("lbtnRemove")
            strUser = lbtnRemove_gv.CommandArgument.ToString()
            If strOwnerUserID = "" Then
                strOwnerUserID = strUser
            Else
                strOwnerUserID = strOwnerUserID + "," + strUser
            End If
        Next
        Using sqlConn As New SqlConnection(strConnectionString)

            Dim strMsg As String = ""

            CheckUserAsset = ""

            Try
                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_CheckUserAsset", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString

                    .Parameters.Add("@OwnerType", SqlDbType.NVarChar, 50)
                    .Parameters("@OwnerType").Value = ddlUserType.SelectedValue.ToString
                    .Parameters.Add("@Owner", SqlDbType.NVarChar, 100)
                    .Parameters("@Owner").Value = strOwnerUserID
                    .Parameters.Add("@CategoryID", SqlDbType.BigInt)
                    .Parameters("@CategoryID").Value = CInt(hdfCategoryID.Value.ToString)
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output

                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With
                CheckUserAsset = strMsg

                'If strMsg <> "" Then
                '    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                'End If
                Return CheckUserAsset
            Catch ex As Exception
                Me.lblMsg.Text = "CheckUserAsset: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try

        End Using
    End Function

    Protected Sub ddlBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBrand.SelectedIndexChanged
        If ddlBrand.SelectedValue.ToString = "0" Then
            txtBrand.Enabled = True
            txtBrand.Focus()
        Else
            txtBrand.Text = ""
            txtBrand.Enabled = False
        End If
    End Sub

    Protected Sub gvQuotationReceipt_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvQuotationReceipt.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim intID As Integer = 0
                Dim lbtnDelete As New LinkButton

                lblMsg.Text = ""

                lbtnDelete = gvQuotationReceipt.Rows(e.RowIndex).FindControl("lbtnDelete")
                intID = CInt(lbtnDelete.CommandArgument.ToString)

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteAssetQuoReceiptFile", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                    .Parameters.Add("@DocPicID", SqlDbType.Int)
                    .Parameters("@DocPicID").Value = intID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    GetAssetQuoReceiptFile()
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvQuotationReceipt_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Private Sub GetAssetQuoReceiptFile()
        Using MyConnection As New SqlConnection(strConnectionString)

            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetAssetQuoReceiptFile")

                With MyCommand

                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AssetQuoReceiptFile"


                gvQuotationReceipt.DataSource = MyData
                gvQuotationReceipt.DataMember = MyData.Tables(0).TableName
                gvQuotationReceipt.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "GetAssetQuoReceiptFile: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnSplitRecord_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSplitRecord.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String = ""
                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SplitRecord", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 50)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output

                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                    Exit Sub
                Else
                    GetAssetDetail()
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Split Record Successful')</script>")
                    'Response.Redirect("~/Asset/frmAssetList.aspx")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnSplitRecord_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub gvChildAsset_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvChildAsset.SelectedIndexChanged
        Session("AssetID") = ""
        Session("AssetID") = gvChildAsset.SelectedDataKey.Value.ToString
        Response.Redirect("~/Asset/frmAssetDetail.aspx")
    End Sub

    Protected Sub gvParentAsset_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvParentAsset.SelectedIndexChanged
        Session("AssetID") = ""
        Session("AssetID") = gvParentAsset.SelectedDataKey.Value.ToString
        Response.Redirect("~/Asset/frmAssetDetail.aspx")
    End Sub

    Protected Sub lbtnAddOwner_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnAddOwner.Click
        Dim strScript As String

        If gvUserList.Rows.Count = 0 Then
            Session("DA_USERLIST") = ""
            Session("DA_New_USERLIST") = ""
            Session("DA_USERLIST_TYPE") = "UserList"
        Else
            Dim i As Integer
            Dim strUser As String = ""
            Dim lbtnUserID_gv As New LinkButton
            Dim lblJobGrade_gv As New Label
            Dim lblInternalAudit_gv As New Label
            Dim lblMISSeedMember_gv As New Label
            Dim ddlJobFunction_gv As New DropDownList

            hdfUserList.Value = ""

            For i = 0 To gvUserList.Rows.Count - 1
                strUser = ""
                lbtnUserID_gv = gvUserList.Rows(i).Cells(0).FindControl("lbtnRemove")
                lblJobGrade_gv = gvUserList.Rows(i).Cells(2).FindControl("lblJobGrade")
                lblInternalAudit_gv = gvUserList.Rows(i).Cells(3).FindControl("lblInternalAudit")
                lblMISSeedMember_gv = gvUserList.Rows(i).Cells(4).FindControl("lblMISSeedMember")
                ddlJobFunction_gv = gvUserList.Rows(i).Cells(5).FindControl("ddlJobFunction")


                If dsUseList.Select("OwnerID='" + lbtnUserID_gv.CommandArgument.ToString() + "'").Length > 0 Then
                    Dim myRow() As Data.DataRow
                    myRow = dsUseList.Select("OwnerID='" + lbtnUserID_gv.CommandArgument.ToString() + "'")
                    myRow(0)("Job_Function") = ddlJobFunction_gv.SelectedValue.ToString()
                End If

                If hdfUserList.Value.ToString = "" Then
                    hdfUserList.Value = lbtnUserID_gv.CommandArgument.ToString()
                Else
                    hdfUserList.Value = hdfUserList.Value.ToString + "," + lbtnUserID_gv.CommandArgument.ToString()
                End If
            Next


            Session("DA_USERLIST_TYPE") = "UserList"
            Session("DA_USERLIST") = hdfUserList.Value.ToString
        End If

        Session("DA_AddUserStationID") = ""
        Session("DA_AddUserStationID") = Session("DA_StationID").ToString

        strScript = "<script language=javascript>"
        strScript += "theChild = window.open('../frmAddUser.aspx?ParentForm=frmAssetDetail','AddUser','height=550, width=550,status= no, resizable= no, scrollbars=yes, toolbar=no,location=center,menubar=no, top=100, left=300');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)
    End Sub

    Protected Sub btnAddSubAsset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddSubAsset.Click
        Response.Redirect("frmSubAssetDetail.aspx?MainAssetID=" & txtAssetID.Text.ToString())
    End Sub

    Public Function GeteRequistionJobFunction() As DataView
        Dim dtViewJobFunction As DataView = clsCF.GeteRequistionJobFunction()
        Return dtViewJobFunction
    End Function

    Protected Sub gvUserList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvUserList.SelectedIndexChanged
        Dim strRemove As String = ""

        strRemove = gvUserList.SelectedValue.ToString

        If dsUseList.Select("OwnerID='" + strRemove + "'").Length > 0 Then
            Dim myRow() As Data.DataRow
            myRow = dsUseList.Select("OwnerID='" + strRemove + "'")
            dsUseList.Rows.Remove(myRow(0))
        End If

        Dim i As Integer
        Dim strUser As String = ""
        Dim lbtnUserID_gv As New LinkButton
        Dim lblJobGrade_gv As New Label
        Dim lblInternalAudit_gv As New Label
        Dim lblMISSeedMember_gv As New Label
        Dim ddlJobFunction_gv As New DropDownList

        For i = 0 To gvUserList.Rows.Count - 1
            strUser = ""
            lbtnUserID_gv = gvUserList.Rows(i).Cells(0).FindControl("lbtnRemove")
            lblJobGrade_gv = gvUserList.Rows(i).Cells(2).FindControl("lblJobGrade")
            lblInternalAudit_gv = gvUserList.Rows(i).Cells(3).FindControl("lblInternalAudit")
            lblMISSeedMember_gv = gvUserList.Rows(i).Cells(4).FindControl("lblMISSeedMember")
            ddlJobFunction_gv = gvUserList.Rows(i).Cells(5).FindControl("ddlJobFunction")

            If dsUseList.Select("OwnerID='" + lbtnUserID_gv.CommandArgument.ToString() + "'").Length > 0 Then
                Dim myRow() As Data.DataRow
                myRow = dsUseList.Select("OwnerID='" + lbtnUserID_gv.CommandArgument.ToString() + "'")
                myRow(0)("Job_Function") = ddlJobFunction_gv.SelectedValue.ToString()
            End If
        Next

        gvUserList.DataSource = dsUseList
        gvUserList.DataMember = dsUseList.TableName
        gvUserList.DataBind()

        If gvUserList.Rows.Count = 0 Then
            hdfUserAssetMsg.Value = ""
            hdfOwnerUserID.Value = ""
            hdfUserList.Value = ""
        End If
    End Sub

    Protected Sub gvSubAsset_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSubAsset.SelectedIndexChanged
        Session("AssetID") = ""
        Session("AssetID") = gvSubAsset.SelectedDataKey.Value.ToString
        Response.Redirect("~/Asset/frmAssetDetail.aspx")
    End Sub

    Protected Sub gvMainAsset_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvMainAsset.SelectedIndexChanged
        Session("AssetID") = ""
        Session("AssetID") = gvMainAsset.SelectedDataKey.Value.ToString
        Response.Redirect("~/Asset/frmAssetDetail.aspx")
    End Sub

    Protected Sub chkTransferStation_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkTransferStation.CheckedChanged
        ddlStation.Enabled = False
        ddlTransferToStation.Visible = False
        lblTransferRemark.Visible = False

        If chkTransferStation.Checked = True Then
            ddlTransferToStation.Visible = True
            lblTransferRemark.Visible = True
        End If
    End Sub

    Protected Sub chkPosteFMS_Exception_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPosteFMS_Exception.CheckedChanged
        PosteFMS_Exception()
    End Sub

    Private Sub PosteFMS_Exception()
        ddlExceptionReason.Visible = False
        ddlAssetClassCOA.Enabled = False
        cldDepreciationEffective.Enabled = False
        ddlDepExpCOA.Enabled = False
        ddlAccDepreciationCOA.Enabled = False
        txtNetBookValue.Enabled = False
        btnPosteFMS.Enabled = False
        VisibleMantadary_Post_eFMS(False)

        If chkPosteFMS_Exception.Checked = True Then
            VisibleMantadary_Post_eFMS(True)
            ddlExceptionReason.Visible = True
            ddlAssetClassCOA.Enabled = True
            cldDepreciationEffective.Enabled = True
            ddlDepExpCOA.Enabled = True
            ddlAccDepreciationCOA.Enabled = True
            txtNetBookValue.Enabled = True
            hdfeFMS_Status.Value = "D"
            If lblLansweeperMappingMsg.Text.ToString() <> "" Then
                btnPosteFMS.Enabled = False
            Else
                btnPosteFMS.Enabled = True
            End If

        End If
    End Sub


    Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click
        eRequisitionApproval("M")
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        eRequisitionApproval("A")
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        eRequisitionApproval("R")
    End Sub

    Private Function eRequisitionApproval(ByVal strStatus As String)
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String = ""
                Dim intMailID As String = 0
                Dim strApplyNewID As String = ""
                Dim strStatusValue As String = ""

                lblMsg.Text = ""

                If strStatus = "A" Then
                    strStatusValue = "Approve"
                ElseIf strStatus = "M" Then
                    strStatusValue = "Request Modify"
                ElseIf strStatus = "R" Then
                    strStatusValue = "Reject"
                End If

                If strStatus <> "A" Then
                    If txtComment.Text.ToString = "" Then
                        txtComment.Focus()
                        Return "Please enter Comment"
                        Exit Function
                    End If
                End If

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_UpdateeReqACK", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@eRequisitionNo", SqlDbType.NVarChar, 50)
                    .Parameters("@eRequisitionNo").Value = lbleRequisitionNo.Text.ToString()

                    .Parameters.Add("@eReq_Status", SqlDbType.VarChar, 1)
                    .Parameters("@eReq_Status").Value = strStatus

                    .Parameters.Add("@eReq_ApprovedBy", SqlDbType.VarChar, 6)
                    .Parameters("@eReq_ApprovedBy").Value = Session("DA_UserID").ToString()

                    .Parameters.Add("@eReq_ApprovedOn", SqlDbType.NVarChar, 50)
                    .Parameters("@eReq_ApprovedOn").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@eReq_Comments", SqlDbType.NVarChar)
                    .Parameters("@eReq_Comments").Value = txtComment.Text.ToString()

                    .Parameters.Add("@biMailID", SqlDbType.NVarChar, 500)
                    .Parameters("@biMailID").Direction = ParameterDirection.Output
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                    intMailID = .Parameters("@biMailID").Value.ToString()
                End With

                If strMsg <> "" Then
                    Return strMsg
                Else
                    Dim strResult As String = ""
                    strResult = clsCF.SendMail_ApplyNew(intMailID, strApplyNewID)

                    If strResult = "" Then

                        clsCF.UpdateMailQueue(intMailID, 1, strStatusValue + " Successful", hdfCurrentDateTime.Value.ToString)
                        Return strStatusValue + " Successful"
                    Else
                        clsCF.UpdateMailQueue(intMailID, 2, "Send Fail - " & strResult, hdfCurrentDateTime.Value.ToString)
                        Return "Approval Fail - " & strResult
                    End If
                End If
            Catch ex As Exception
                Return ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Function

    Protected Sub btnLanSweeper_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLanSweeper.Click
        Dim strScript As String

        Session("AssetNo") = ""
        Session("eRequisitionNo") = ""
        Session("Category") = ""
        Session("Currency") = ""
        Session("PurchaseAmt") = ""
        Session("BudgetAmt") = ""
        Session("MappedAssetList") = ""
        Session("PurchaseDate") = ""

        Dim strCategory As String

        If lblSpecialReqCategory.Text.ToString <> "" Then
            strCategory = lblSpecialReqCategory.Text.ToString
        ElseIf lblCategory.Text.ToString <> "" Then
            strCategory = lblCategory.Text.ToString
        Else
            strCategory = lblParentCategory.Text.ToString
        End If

        Session("AssetID") = txtAssetID.Text.ToString
        Session("AssetNo") = txtAssetNo.Text.ToString
        Session("eRequisitionNo") = lbleRequisitionNo.Text.ToString
        Session("Category") = strCategory
        Session("Currency") = ddlCurrencyCode.SelectedItem.Text.ToString
        Session("PurchaseAmt") = txtPurchaseCost.Text.ToString
        Session("BudgetAmt") = txtPurchaseBudgeted.Text.ToString
        Session("MappedAssetList") = hdfMappedAssetID.Value.ToString

        Dim strPurchaseDate As String = Format(cldPurchaseDate.SelectedDate, "dd MMM yyyy").ToString
        If strPurchaseDate.Contains("01 Jan 0001") = True Then
            strPurchaseDate = ""
        End If

        Session("PurchaseDate") = strPurchaseDate

        strScript = "<script language=javascript>"
        'strScript += "theChild = window.open('frmLanSweeperMapping.aspx?ParentForm=frmAssetDetail','LanSweeper Mapping','height=800, width=900,status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no,fullscreen=yes,titlebar=no, top=100, left=300');"
        strScript += "theChild = window.open('frmLanSweeperMapping.aspx?ParentForm=frmAssetDetail','LanSweeper Mapping','status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no,fullscreen=yes,titlebar=no,border-style=none');"
        strScript += "</script>"
        Page.ClientScript.RegisterStartupScript(GetType(Page), "OpenWindow", strScript)

    End Sub

    Protected Sub gvLansweeperMap_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvLansweeperMap.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim strReturnMsg As String
                Dim strMappedAssetList As String
                Dim LanSweeperAssetID As Integer = 0
                Dim lbtnRemove As New LinkButton
                Dim hdfID As New HiddenField

                lblMsg.Text = ""

                lbtnRemove = gvLansweeperMap.Rows(e.RowIndex).FindControl("lbtnRemove")
                LanSweeperAssetID = CInt(lbtnRemove.CommandArgument.ToString)

                strReturnMsg = UnMappingLansweeper(LanSweeperAssetID)

                If strReturnMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strReturnMsg + "')</script>")
                Else
                    clsCF.CheckConnectionState(sqlConn)

                    Dim cmd As New SqlCommand("SP_DeleteLanSweeperMapping", sqlConn)
                    With cmd
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                        .Parameters("@AssetID").Value = txtAssetID.Text.ToString
                        .Parameters.Add("@LanSweeperAssetID", SqlDbType.BigInt)
                        .Parameters("@LanSweeperAssetID").Value = LanSweeperAssetID
                        .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                        .Parameters("@UserID").Value = Session("DA_UserID").ToString
                        .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                        .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                        .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                        .Parameters("@Msg").Direction = ParameterDirection.Output
                        .Parameters.Add("@MappedAssetList", SqlDbType.NVarChar, 4000)
                        .Parameters("@MappedAssetList").Direction = ParameterDirection.Output
                        .CommandTimeout = 0
                        .ExecuteNonQuery()

                        strMsg = .Parameters("@Msg").Value.ToString()
                        strMappedAssetList = .Parameters("@MappedAssetList").Value.ToString()
                    End With

                    If strMsg <> "" Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                    Else
                        'hdfMappedAssetID.Value = strMappedAssetList
                        'Session("MappedAssetList") = hdfMappedAssetID.Value
                        ''GetMappedAssetList()
                        GetAssetDetail()
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                    End If
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "gvLansweeperMap_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Private Function UnMappingLansweeper(ByVal intID As Integer) As String
        Dim strMsg As String = ""
        Dim strReturnMsg As String = ""

        Using sqlConn As New SqlConnection(strLansweeperConnectionString)
            Try
                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_UnMapping", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@UnMapAssetID", SqlDbType.BigInt)
                    .Parameters("@UnMapAssetID").Value = intID
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output

                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()

                End With

                If strMsg <> "" Then
                    strReturnMsg = strMsg
                Else
                    strReturnMsg = ""
                End If

                Return strReturnMsg
            Catch ex As Exception
                strReturnMsg = ex.Message.ToString
                Return strReturnMsg
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Function

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        If Session("MappedAssetList").ToString <> hdfMappedAssetID.Value.ToString Then
            GetAssetDetail()
        Else
            hdfMappedAssetID.Value = Session("MappedAssetList").ToString
            GetMappedAssetList()
        End If
    End Sub
End Class
