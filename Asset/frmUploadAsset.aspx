<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmUploadAsset.aspx.vb" Inherits="Asset_frmUploadAsset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Upload Asset</title>
    <script type="text/javascript">
         function showDate() 
        {
            var month_names = new Array("Jan", "Feb", "Mar", 
            "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
            "Oct", "Nov", "Dec");

            var d = new Date();
            var curr_day = d.getDay();
            var curr_date = d.getDate();
            var hours = d.getHours();
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            
            var curr_hours;
            var curr_minutes;
            var curr_seconds;

            if(hours<10)
            {
                curr_hours = "0" + hours;
            }
            else
            {
                curr_hours = hours;
            }
            
            if(minutes < 10)
            {
                curr_minutes = "0" + minutes;
            }
             else
            {
                curr_minutes = minutes;
            }
            
           if(seconds < 10)
            {
                curr_seconds = "0" + seconds;
            }
             else
            {
                curr_seconds = seconds;
            }

            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
 
            document.getElementById("hdfCurrentDateTime").value = curr_date + " " +  month_names[curr_month] +  " " + curr_year + " " + curr_hours + ":" + curr_minutes + ":" + curr_seconds   
         }
     </script>
</head>
<body>
    <form id="frmUploadAsset" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 96px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td style="width: 96px">
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Upload Asset"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 96px">
                </td>
                <td colspan="7">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 96px">
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 96px">
                </td>
                <td colspan="7">
                    <asp:Label ID="lblID" runat="server" Font-Names="Verdana" Font-Size="X-Small" Visible="False"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td style="width: 96px">
                </td>
                <td colspan="7">
                    <table style="width: 650px">
                        <tr>
                            <td style="width: 150px">
                                <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Station"></asp:Label></td>
                            <td style="width: 5px">
                                :</td>
                            <td>
                                <asp:DropDownList ID="ddlStation" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 150px">
                                <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="File"></asp:Label></td>
                            <td style="width: 5px">
                                :</td>
                            <td>
                                <asp:FileUpload ID="fuFile" runat="server" Width="335px" />
                                <asp:HiddenField ID="hdfFilePath" runat="server" />
                                <asp:HiddenField ID="hdfCurrentDateTime" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <table style="width: 450px">
                                    <tr>
                                        <td style="width: 150px">
                                            <asp:Button ID="btnProcess" runat="server" Font-Names="Verdana" Font-Size="Small"
                                                Text="Process" Width="85px" OnClientClick="showDate();" /></td>
                                        <td style="width: 150px">
                                        </td>
                                        <td style="width: 150px">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td style="width: 96px">
                </td>
                <td colspan="8">
                    <asp:GridView ID="gvImport" runat="server" BorderColor="Silver" BorderWidth="1px"
                        EmptyDataText="Record Not Found" Font-Names="Verdana" Font-Size="X-Small">
                    </asp:GridView>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td style="width: 96px">
                </td>
                <td colspan="7">
                    <table style="width: 650px">
                        <tr>
                            <td style="width: 150px">
                            </td>
                            <td style="width: 5px">
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td style="width: 96px">
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
