<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmSubAssetDetail.aspx.vb" Inherits="Asset_frmSubAssetDetail" %>

<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>

<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Sub Asset Detail</title>
     <script type="text/javascript">
         function showDate() 
        {
            var month_names = new Array("Jan", "Feb", "Mar", 
            "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
            "Oct", "Nov", "Dec");

            var d = new Date();
            var curr_day = d.getDay();
            var curr_date = d.getDate();
            var hours = d.getHours();
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            
            var curr_hours;
            var curr_minutes;
            var curr_seconds;

            if(hours<10)
            {
                curr_hours = "0" + hours;
            }
            else
            {
                curr_hours = hours;
            }
            
            if(minutes < 10)
            {
                curr_minutes = "0" + minutes;
            }
             else
            {
                curr_minutes = minutes;
            }
            
           if(seconds < 10)
            {
                curr_seconds = "0" + seconds;
            }
             else
            {
                curr_seconds = seconds;
            }

            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
 
            document.getElementById("hdfCurrentDateTime").value = curr_date + " " +  month_names[curr_month] +  " " + curr_year + " " + curr_hours + ":" + curr_minutes + ":" + curr_seconds
         }
         
//         function checkVerify() 
//        {  
//            bool continue = true; 
//            var msg = document.getElementById("hdfUserAssetMsg").value
//            if (msg != "") 
//            { 
//                continue = confirm(msg); 
//            }  
//            return continue; 
//        } 


//        function promptMessage() 
//        {
////           var msg = document.getElementById("hdfUserAssetMsg").value
////           if (msg != "")
////            {
////                var theAnswer = confirm(msg);
////                 if (theAnswer)
////                 {
//////                    alert("Javascript is cool.");
////                  }
////                 else
////                 {
//////                    alert("Here is a message anyway.");
////                  }
////            }
//            


//          var msg = document.getElementById("hdfUserAssetMsg").value
//          if (msg != "")
//          {
//            alert(msg);
//          }
//        }
//        
//        function ConfirmOnDuplicateAsset() 
//        { 
//          alert("here");
//          var msg = document.getElementById("hdfUserAssetMsg").value
//          alert(msg);
//          if (msg != "")
//          {
//            if (confirm(msg & ". Do you want to continue?") == true) 
//                return true; 
//            else 
//                return false; 
//          }  
//         } 
//         
//         function ConfirmApproval(objMsg) 
//        { 
//            if(confirm(objMsg)) 
//            { 
//                return true; 
//            }     
//            else 
//                return false;     
//        } 

        function confirmAsset() 
        {
        
//            var msg = document.getElementById("hdfUserAssetMsg")
            if (msg.value != "")
            {
//                alert(msg.value);
                var answer = confirm("Owner already assigned same category assets! Do you want to Continue?")
                if (answer)
                {
	                document.getElementById("hdfConfirmValue").value = "Yes";
                }
                else
                {
	               document.getElementById("hdfConfirmValue").value = "No";
                }
            }
            else
            {
                document.getElementById("hdfConfirmValue").value = "Yes";
            }
        }
        
        
        function confirm() 
        {
        
//            var answer = confirm("Leave tizag.com?")
//	        if (answer){
//		        alert("Bye bye!")
//	        }
//	        else{
//		        alert("Thanks for sticking around!")
//	        }


//            document.getElementById("hdfConfirmValue").value = "No";
            
            var msg = document.getElementById("hdfUserAssetMsg")
            if (msg.value != "")
            {
                alert(msg.value);
//                var answer = confirm("Owner already assigned same category assets! Do you want to Continue?")
//                if (answer)
//                {
//	                document.getElementById("hdfConfirmValue").value = "Yes";
//                }
//                else
//                {
//	               document.getElementById("hdfConfirmValue").value = "No";
//                }
            }
//            else
//            {
//                document.getElementById("hdfConfirmValue").value = "Yes";
//            }
        }



 
     </script>
</head>
<body>
    <form id="frmSubAssetDetail" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 7px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Sub Asset Detail"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:HiddenField ID="hdfCurrentDateTime" runat="server" />
                    <asp:HiddenField ID="hdfUserAssetMsg" runat="server" />
                    <asp:HiddenField ID="hdfConfirmValue" runat="server" />
                    <asp:HiddenField ID="hdfMainAssetID" runat="server" />
                    &nbsp;&nbsp;
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label96" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                        Font-Underline="True" ForeColor="Blue" Text="Main Asset :-"></asp:Label></td>
                <td>
                </td>
                <td colspan="5">
                    <asp:GridView ID="gvParentAsset" runat="server" AutoGenerateColumns="False" Font-Names="Verdana"
                        Font-Size="X-Small" DataKeyNames="ASSETID">
                        <Columns>
                            <asp:TemplateField HeaderText="AssetID">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("AssetNo") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label42" runat="server" Text='<%# Bind("AssetID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="AssetNo" HeaderText="AssetNo" />
                            <asp:BoundField DataField="Category" HeaderText="Item" />
                            <asp:BoundField DataField="OwnerType" HeaderText="Owner Type" />
                            <asp:BoundField DataField="OwnerName" HeaderText="Owner" />
                            <asp:BoundField DataField="Brand" HeaderText="Brand" />
                            <asp:BoundField DataField="Model" HeaderText="Model" />
                        </Columns>
                    </asp:GridView>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <table>
                        <tr>
                            <td style="width: 150px">
                                <asp:Button ID="btnSave" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Save"
                                    Width="101px" OnClientClick="showDate();confirm();" Visible="False" /></td>
                            <td style="width: 150px"></td>
                            <td style="width: 150px"></td>
                            <td style="width: 150px"></td>
                            <td style="width: 150px">
                    </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Asset Information :-" Font-Bold="True" ForeColor="Blue" BackColor="GradientActiveCaption" Width="100%"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Asset ID"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:TextBox ID="txtAssetID" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="150px" Enabled="False"></asp:TextBox></td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label85" runat="server" ForeColor="Red" Text="*"></asp:Label><asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Asset No"></asp:Label><br />
                    <asp:Label ID="Label38" runat="server" Font-Names="Verdana" Font-Size="XX-Small"
                        ForeColor="#C00000" Text=" ** split by comma (,)"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td style="font-size: x-small; font-family: Verdana">
                    <asp:TextBox ID="txtAssetNo" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="175px" MaxLength="500"></asp:TextBox></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label60" runat="server" ForeColor="Red" Text="*"></asp:Label><asp:Label ID="Label10" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Station"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:DropDownList ID="ddlStation" runat="server" Font-Names="Verdana" Font-Size="X-Small" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label6" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="IP Address"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td>
                    <asp:TextBox ID="txtIPAddress" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        MaxLength="15" Width="175px"></asp:TextBox></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label61" runat="server" ForeColor="Red" Text="*"></asp:Label><asp:Label ID="Label11" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Location"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:DropDownList ID="ddlLocation" runat="server" Font-Names="Verdana" Font-Size="X-Small" AutoPostBack="True">
                    </asp:DropDownList></td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label8" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Serial #"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td>
                    <asp:TextBox ID="txtSerial" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="175px"></asp:TextBox></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label68" runat="server" ForeColor="Red" Text="*"></asp:Label><asp:Label ID="Label23" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Condition"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:DropDownList ID="ddlCondition" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                        <asp:ListItem></asp:ListItem>
                        <asp:ListItem>A</asp:ListItem>
                        <asp:ListItem>R</asp:ListItem>
                    </asp:DropDownList></td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label97" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Others Info"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td rowspan="2">
                    <asp:TextBox ID="txtOthersInfo" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="179px" Height="38px" TextMode="MultiLine"></asp:TextBox>&nbsp;</td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label15" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Warranty"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <ew:numericbox id="txtWarranty" runat="server" font-names="Verdana" font-size="X-Small"
                        width="65px"></ew:numericbox>
                    <asp:DropDownList ID="ddlWarranty" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                    </asp:DropDownList></td>
                <td>
                </td>
                <td>
                </td>
                <td style="width: 7px">
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td rowspan="2">
                    <asp:Label ID="Label62" runat="server" ForeColor="Red" Text="*"></asp:Label><asp:Label ID="Label9" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Owner"></asp:Label></td>
                <td rowspan="2">
                    :</td>
                <td rowspan="4" colspan="5">
                    <asp:DropDownList ID="ddlUserType" runat="server" AutoPostBack="True" Font-Names="Verdana"
                        Font-Size="X-Small">
                        <asp:ListItem>Select One</asp:ListItem>
                        <asp:ListItem>User</asp:ListItem>
                        <asp:ListItem>Office</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Button ID="btnAddUser" runat="server" Text="Add User" Visible="False" /><br />
                    <asp:TextBox ID="txtOwner" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="200px" MaxLength="100" Visible="False"></asp:TextBox>
                    <asp:GridView ID="gvUserList" runat="server" AutoGenerateColumns="False" Font-Names="Verdana"
                        Font-Size="X-Small" DataKeyNames="OwnerID">
                        <Columns>
                            <asp:TemplateField HeaderText="Remove">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnRemove" runat="server" CommandArgument='<%# Bind("OwnerID") %>' CommandName="Select">Remove</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FullName" HeaderText="Name" />
                            <asp:TemplateField HeaderText="Job Grade">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("JobGrade") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblJobGrade" runat="server" Text='<%# Bind("JobGrade") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Internal Audit">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("InternalAudit") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblInternalAudit" runat="server" Text='<%# Bind("Internal_Audit") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="MIS Seed Member">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("MISSeedMember") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblMISSeedMember" runat="server" Text='<%# Bind("MIS_Seed_Member") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Job Function">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("JobFunction") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlJobFunction" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                        SelectedValue='<%# Bind("Job_Function") %>' DataSource = '<%# GeteRequistionJobFunction() %>' DataTextField="Job_Function">
                                    </asp:DropDownList>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:HiddenField ID="hdfUserList" runat="server" />
                    <asp:HiddenField ID="hdfOwnerUserID" runat="server" Visible="False" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label65" runat="server" ForeColor="Red" Text="*"></asp:Label><asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Category"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:Label ID="lblParentCategory" runat="server" BorderColor="Silver" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Verdana" Font-Size="X-Small"></asp:Label><asp:Label
                            ID="lblCategory" runat="server" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                            Font-Names="Verdana" Font-Size="X-Small"></asp:Label>
                    <asp:Button ID="btnCategory" runat="server" Font-Size="X-Small" Text="...." />
                    <asp:HiddenField ID="hdfCategoryID" runat="server" />
                    <asp:HiddenField ID="hdfParentCategory" runat="server" />
                    <asp:HiddenField ID="hdfParentCategoryID" runat="server" />
                </td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label16" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Vendor Ref #"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td>
                    <asp:TextBox ID="txtVendorRefNo" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="150px"></asp:TextBox></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td style="height: 6px">
                </td>
                <td style="height: 6px">
                    <asp:Label ID="Label13" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Brand"></asp:Label></td>
                <td style="height: 6px">
                    :</td>
                <td style="height: 6px">
                    <asp:DropDownList ID="ddlBrand" runat="server" AutoPostBack="True" Font-Names="Verdana"
                        Font-Size="X-Small">
                    </asp:DropDownList>
                    <asp:TextBox ID="txtBrand" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="130px" MaxLength="50"></asp:TextBox></td>
                <td style="height: 6px">
                </td>
                <td style="height: 6px">
                    <asp:Label ID="Label27" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Status"></asp:Label></td>
                <td style="height: 6px; width: 7px;">
                    :</td>
                <td style="height: 6px">
                    <asp:DropDownList ID="ddlStatus" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                    </asp:DropDownList></td>
                <td style="height: 6px">
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label14" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Model"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:TextBox ID="txtModel" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="200px" MaxLength="100"></asp:TextBox></td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label69" runat="server" ForeColor="Red" Text="*"></asp:Label><asp:Label ID="Label54" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Date Required"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td><ew:calendarpopup id="cldRequiredDate" runat="server" width="120px" Font-Names="Verdana" Font-Size="X-Small">
                    <selecteddatestyle backcolor="Yellow" font-names="Verdana,Helvetica,Tahoma,Arial"
                        font-size="XX-Small" forecolor="Black"></selecteddatestyle>
                    <holidaystyle backcolor="White" font-names="Verdana,Helvetica,Tahoma,Arial" font-size="XX-Small"
                        forecolor="Black"></holidaystyle>
                    <offmonthstyle backcolor="AntiqueWhite" font-names="Verdana,Helvetica,Tahoma,Arial"
                        font-size="XX-Small" forecolor="Gray"></offmonthstyle>
                    <monthheaderstyle backcolor="Yellow" font-names="Verdana,Helvetica,Tahoma,Arial"
                        font-size="XX-Small" forecolor="Black"></monthheaderstyle>
                    <weekdaystyle backcolor="White" font-names="Verdana,Helvetica,Tahoma,Arial" font-size="XX-Small"
                        forecolor="Black"></weekdaystyle>
                    <gototodaystyle backcolor="White" font-names="Verdana,Helvetica,Tahoma,Arial" font-size="XX-Small"
                        forecolor="Black"></gototodaystyle>
                    <cleardatestyle backcolor="White" font-names="Verdana,Helvetica,Tahoma,Arial" font-size="XX-Small"
                        forecolor="Black"></cleardatestyle>
                    <weekendstyle backcolor="LightGray" font-names="Verdana,Helvetica,Tahoma,Arial" font-size="XX-Small"
                        forecolor="Black"></weekendstyle>
                    <dayheaderstyle backcolor="Orange" font-names="Verdana,Helvetica,Tahoma,Arial" font-size="XX-Small"
                        forecolor="Black" />
                    <todaydaystyle backcolor="LightGoldenrodYellow" font-names="Verdana,Helvetica,Tahoma,Arial"
                        font-size="XX-Small" forecolor="Black" />
                </ew:calendarpopup></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label87" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Application To Be Used"></asp:Label></td>
                <td>
                    :</td>
                <td colspan="5">
                    <asp:TextBox ID="txtAppToBeUsed" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="500px"></asp:TextBox></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label7" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Description"></asp:Label></td>
                <td>
                    :</td>
                <td colspan="5">
                    <asp:TextBox ID="txtDescription" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="500px"></asp:TextBox></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label40" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Specification"></asp:Label></td>
                <td>
                    :</td>
                <td colspan="5">
                    <asp:TextBox ID="txtSpecification" runat="server" TextMode="MultiLine" Width="578px"></asp:TextBox></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label12" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                        ForeColor="Blue" Text="Purchasing Information :-" BackColor="GradientActiveCaption" Width="100%"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label70" runat="server" ForeColor="Red" Text="*"></asp:Label><asp:Label ID="Label17" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Vendor"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:Label ID="lblVendorPayee" runat="server" BorderColor="Silver" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Verdana" Font-Size="X-Small"></asp:Label>
                    <asp:Button ID="btnVendorPayee" runat="server" Font-Size="X-Small" Text="...." /><br />
                    <asp:TextBox ID="txtShopName" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="200px"></asp:TextBox>
                    <asp:HiddenField ID="hdfVendorPayeeID" runat="server" />
                    <asp:HiddenField ID="hdfVendorPayeeType" runat="server" />
                    <asp:HiddenField ID="hdfVendorDetail" runat="server" />
                </td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label84" runat="server" ForeColor="Red" Text="*"></asp:Label><asp:Label ID="Label18" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Date Of Purchase"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td>
                    <ew:calendarpopup id="cldPurchaseDate" runat="server" width="120px" Font-Names="Verdana" Font-Size="X-Small">
<SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></SelectedDateStyle>

<HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></HolidayStyle>

<OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Gray"></OffMonthStyle>

<MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></MonthHeaderStyle>

<WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></WeekdayStyle>

<GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></GoToTodayStyle>

<ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></ClearDateStyle>

<WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></WeekendStyle>

<DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></DayHeaderStyle>

<TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" ForeColor="Black"></TodayDayStyle>
</ew:calendarpopup>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label19" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Warranty Period"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <ew:numericbox id="txtWarrantyPeriod" runat="server" font-names="Verdana" font-size="X-Small"
                        width="65px"></ew:numericbox>
                    <asp:Label ID="Label20" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Text="Month"></asp:Label></td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label79" runat="server" ForeColor="Red" Text="*"></asp:Label><asp:Label ID="Label21" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Depreciation Period"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td>
                    <ew:numericbox id="txtDepreciationPeriod" runat="server" font-names="Verdana" font-size="X-Small"
                        width="65px" MaxLength="4" PositiveNumber="True"></ew:numericbox>
                    &nbsp;<asp:Label ID="Label22" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Text="Month"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label71" runat="server" ForeColor="Red" Text="*"></asp:Label><asp:Label ID="Label24" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Availability In Inventory"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <ew:numericbox id="txtAvailInInventory" runat="server" font-names="Verdana" font-size="X-Small"
                        width="150px"></ew:numericbox>
                </td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label72" runat="server" ForeColor="Red" Text="*"></asp:Label><asp:Label ID="Label25" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Quantity"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td>
                    <ew:numericbox id="txtQuantity" runat="server" font-names="Verdana" font-size="X-Small"
                        width="50px" MaxLength="4" PositiveNumber="True"></ew:numericbox>
                    <asp:TextBox ID="txtUnit" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="55px" MaxLength="5"></asp:TextBox></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label26" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Remarks"></asp:Label></td>
                <td>
                    :</td>
                <td rowspan="2">
                    <asp:TextBox ID="txtRemarks" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Height="76px" TextMode="MultiLine" Width="250px"></asp:TextBox></td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label31" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Quotation/Receipt"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td align="left" rowspan="2" valign="middle">
                    <asp:FileUpload ID="fuBrowse" runat="server" Width="150px" /><asp:Button ID="btnAddQuoReceipt" runat="server" Text="Add" Width="40px" /><asp:GridView ID="gvQuotationReceipt" runat="server" AutoGenerateColumns="False"
                        BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px"
                        CellPadding="3" Font-Names="Verdana" Font-Size="X-Small">
                        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                        <Columns>
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandArgument='<%# Bind("DocPicID") %>'
                                        CommandName="Delete" Enabled='<%# Bind("AllowDelete") %>' OnClientClick="return confirm('Are you sure you want to delete this Quotation\Receipt File?');">Delete</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quotation/Receipt">
                                <ItemTemplate>
                                    <asp:Label ID="lblQuotationReceipt" runat="server" Text='<%# Bind("DocPicLink") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                        <AlternatingRowStyle BackColor="#F7F7F7" />
                    </asp:GridView>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td style="width: 7px">
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label74" runat="server" ForeColor="Red" Text="*"></asp:Label><asp:Label ID="Label28" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Purchase Cost Per Unit (Quoted/Budgeted)"></asp:Label></td>
                <td>
                    :</td>
                <td rowspan="1">
                    <asp:DropDownList ID="ddlCurrencyCode" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="54px" Enabled="False">
                    </asp:DropDownList><ew:numericbox id="txtPurchaseCost" runat="server" font-names="Verdana" font-size="X-Small"
                        width="83px" MaxLength="21" PositiveNumber="True"></ew:numericbox><asp:Label ID="Label29" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                        Text=" /"></asp:Label><ew:numericbox id="txtPurchaseBudgeted" runat="server" font-names="Verdana" font-size="X-Small"
                        width="90px" MaxLength="21" PositiveNumber="True"></ew:numericbox></td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label30" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Others Charges"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td>
                    <asp:DropDownList ID="ddlOthCurrencyCode" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="54px" Enabled="False">
                    </asp:DropDownList>
                    <ew:numericbox id="txtOthCharges" runat="server" font-names="Verdana" font-size="X-Small"
                        width="120px"></ew:numericbox></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label98" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="VAT Per Unit"></asp:Label></td>
                <td>
                    :</td>
                <td rowspan="1">
                    <ew:numericbox id="txtVAT" runat="server" font-names="Verdana" font-size="X-Small"
                        width="120px" MaxLength="21" PositiveNumber="True">
                    </ew:numericbox></td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label99" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Discount Amount"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td>
                    <ew:numericbox id="txtDiscount" runat="server" font-names="Verdana" font-size="X-Small"
                        width="120px" MaxLength="21" PositiveNumber="True">
                    </ew:numericbox></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label75" runat="server" ForeColor="Red" Text="*"></asp:Label><asp:Label ID="Label32" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Currency Rate"></asp:Label></td>
                <td>
                    :</td>
                <td colspan="5" rowspan="1">
                    <asp:Label ID="Label33" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="1&nbsp;USD = "></asp:Label>
                    <ew:numericbox id="txtCurrencyRate" runat="server" font-names="Verdana" font-size="X-Small"
                        width="65px"></ew:numericbox>
                    <asp:Label ID="Label34" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Local Currency"></asp:Label>
                    &nbsp;
                    <asp:Label ID="Label36" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="USD $ :"></asp:Label>
                    <ew:numericbox id="txtCRQuoted" runat="server" font-names="Verdana" font-size="X-Small"
                        width="80px"></ew:numericbox>
                    <asp:Label ID="Label35" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                        Text=" /"></asp:Label>
                    <ew:numericbox id="txtCRBudgeted" runat="server" font-names="Verdana" font-size="X-Small"
                        width="80px"></ew:numericbox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label37" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Approved By"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:Label ID="lblLocationApprovedBy" runat="server" BorderColor="Silver" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Verdana" Font-Size="X-Small"></asp:Label><asp:Button ID="btnLocApprovedBy" runat="server" Font-Size="X-Small" Text="...." /><asp:HiddenField ID="hdfLocationApprovedBy" runat="server" />
                </td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label91" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Created By"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td>
                    <asp:Label ID="lblCreatedBy" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label><asp:HiddenField ID="hdfCreatedBy" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label47" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="eMail CC Party"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:Label ID="lblCCParty" runat="server" Font-Names="Verdana" Font-Size="X-Small" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"></asp:Label><asp:Button ID="btnCCParty" runat="server" Font-Size="X-Small" Text="...." /><asp:HiddenField ID="hdfCCParty" runat="server" />
                </td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label92" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Created On"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td>
                    <asp:Label ID="lblCreatedOn" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td colspan="5">
                    </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label45" runat="server" BackColor="GradientActiveCaption" Font-Bold="True"
                        Font-Names="Verdana" Font-Size="X-Small" ForeColor="Blue" Text="Others Information :-"
                        Width="100%"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td style="height: 33px">
                </td>
                <td style="height: 33px">
                    <asp:Label ID="Label46" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Asset Class (COA)"></asp:Label></td>
                <td style="height: 33px">
                    :</td>
                <td style="height: 33px">
                    <asp:DropDownList ID="ddlAssetClassCOA" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                    </asp:DropDownList></td>
                <td style="height: 33px">
                </td>
                <td style="height: 33px">
                </td>
                <td style="width: 7px; height: 33px;">
                </td>
                <td style="height: 33px">
                </td>
                <td style="height: 33px">
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label49" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Depreciation Effective"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <ew:CalendarPopup ID="cldDepreciationEffective" runat="server"
                        Font-Size="X-Small" Nullable="True" Width="120px">
                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Gray" />
                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                    </ew:CalendarPopup>
                </td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label55" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Residual Value"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td>
                    <ew:NumericBox ID="txtResidualValue" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="150px" MaxLength="21" PositiveNumber="True" Enabled="False"></ew:NumericBox></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label50" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Depreciation / Month"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <ew:NumericBox ID="txtDepreciationMonth" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="150px" Enabled="False"></ew:NumericBox></td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label86" runat="server" ForeColor="Red" Text="*"></asp:Label><asp:Label ID="Label56" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Net Book Value"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td>
                    <ew:NumericBox ID="txtNetBookValue" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="150px" MaxLength="21"></ew:NumericBox></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label51" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Accumulated Depreciation"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <ew:NumericBox ID="txtAccDepreciation" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="150px" Enabled="False"></ew:NumericBox></td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label57" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Obsolete Date"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td>
                    <ew:CalendarPopup ID="cldObsoleteDate" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Nullable="True" Width="120px" Enabled="False">
                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Gray" />
                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                    </ew:CalendarPopup>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label52" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Depreciation Expense COA"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:DropDownList ID="ddlDepExpCOA" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                        <asp:ListItem>62020400</asp:ListItem>
                    </asp:DropDownList></td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label58" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Disposal Date"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td>
                    <ew:CalendarPopup ID="cldDisposalDate" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Nullable="True" Width="120px" Enabled="False">
                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Gray" />
                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                    </ew:CalendarPopup>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label53" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Accumulated Depreciation COA"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:DropDownList ID="ddlAccDepreciationCOA" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                    </asp:DropDownList></td>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label59" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Writeoff Date"></asp:Label></td>
                <td style="width: 7px">
                    :</td>
                <td>
                    <ew:CalendarPopup ID="cldWriteoffDate" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Nullable="True" Width="120px" Enabled="False">
                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Gray" />
                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                    </ew:CalendarPopup>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                </td>
                <td colspan="5">
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    </td>
                <td>
                </td>
                <td colspan="5">
                    &nbsp;</td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="5">
                    <asp:CheckBox ID="chkSplit" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Text="Split Into Single Asset Record" />
                    &nbsp; &nbsp;
                    <asp:Button ID="btnGenerate" runat="server" Text="Generate" OnClientClick="showDate(); return confirm('Are you sure you want to delete this Grade Setting?');" /></td>
                <td style="width: 7px">
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td colspan="7">
                    <asp:GridView ID="gvSubAssetTemp" runat="server" AutoGenerateColumns="False" Font-Names="Verdana"
                        Font-Size="X-Small" DataKeyNames="AssetID">
                        <Columns>
                            <asp:TemplateField HeaderText="Remove">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandArgument='<%# Bind("AssetID") %>' CommandName="Delete" OnClientClick="showDate(); return confirm('Are you sure you want to delete this Sub Asset?');" >Delete</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TempAssetID">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("TempAssetID") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnTempAssetID" runat="server" CommandArgument='<%# Bind("AssetID") %>'
                                        Text='<%# Bind("AssetID") %>' CommandName="Select"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="AssetNo" HeaderText="Asset No" />
                            <asp:BoundField DataField="Category" HeaderText="Category" />
                            <asp:BoundField DataField="BrandModel" HeaderText="Brand &amp; Model" />
                            <asp:BoundField DataField="No_Of_Units" HeaderText="Quantity" />
                            <asp:BoundField DataField="OwnerName" HeaderText="Owner" />
                            <asp:BoundField DataField="PurchaseCost" HeaderText="Purchase Cost" />
                        </Columns>
                    </asp:GridView>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td style="width: 7px">
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Button ID="btnConfirm" runat="server" Text="Confirm" /></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td style="width: 7px">
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td style="width: 7px">
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
