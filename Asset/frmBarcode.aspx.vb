Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO

Partial Class Asset_frmBarcode
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click

        Dim barCode As String = txtAssetID.Text

        If ddlType.SelectedValue = "Barcode" Then

            Dim imgBarCode As New System.Web.UI.WebControls.Image()

            Using bitMap As New Bitmap(barCode.Length * 40, 80)

                Using graphics__1 As Graphics = Graphics.FromImage(bitMap)

                    Dim oFont As New Font("IDAutomationHC39M", 16)

                    Dim point As New PointF(2.0F, 2.0F)

                    Dim blackBrush As New SolidBrush(Color.Black)

                    Dim whiteBrush As New SolidBrush(Color.White)

                    graphics__1.FillRectangle(whiteBrush, 0, 0, bitMap.Width, bitMap.Height)

                    graphics__1.DrawString("*" & barCode & "*", oFont, blackBrush, point)

                End Using

                Using ms As New MemoryStream()

                    bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png)

                    Dim byteImage As Byte() = ms.ToArray()

                    Convert.ToBase64String(byteImage)

                    imgBarCode.ImageUrl = "data:image/png;base64," & Convert.ToBase64String(byteImage)

                End Using

                plBarCode.Controls.Add(imgBarCode)

            End Using

        ElseIf ddlType.SelectedValue = "Asset Tag" Then
            AssetTag.InnerText = txtAssetID.Text.ToString

        End If
    End Sub
End Class
