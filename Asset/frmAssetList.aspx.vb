Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Asset_frmAssetList
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")
    Dim blnAssetDetailPagePopUP As String = ConfigurationSettings.AppSettings("AssetDetailPagePopUP")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If IsPostBack() = False Then
            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            'gvAssetList.Columns(1).Visible = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "A0001")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Asset List page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        'btnQuery.Visible = True
                        'gvAssetList.Columns(1).Visible = True
                    Else
                        Session("blnWrite") = False
                        'btnQuery.Visible = False
                        'gvAssetList.Columns(1).Visible = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                    'btnQuery.Visible = True
                    'gvAssetList.Columns(1).Visible = True
                Else
                    Session("blnModifyOthers") = False
                    'btnQuery.Visible = False
                    'gvAssetList.Columns(1).Visible = False
                End If

                'If Mid(strAccessRight, 5, 1) = 1 Then
                '    Session("blnDeleteOthers") = True
                '    btnQuery.Visible = True
                '    gvAssetList.Columns(1).Visible = True
                'Else
                '    Session("blnDeleteOthers") = False
                '    btnQuery.Visible = False
                '    gvAssetList.Columns(1).Visible = False
                'End If
            End If

            Dim strApplyNewID_QS As String = ""

            If Request.QueryString("ApplyNewID") <> Nothing Then
                strApplyNewID_QS = Request.QueryString("ApplyNewID").ToString
            End If

            If Session("ApplyNewID") <> Nothing Then
                txtPurchaseNo.Text = Session("ApplyNewID").ToString
                Session("ApplyNewID") = ""
            ElseIf strApplyNewID_QS <> "" Then
                txtPurchaseNo.Text = strApplyNewID_QS
            End If

            BindDropDownList()
            GetAssetList()

        Else

        End If
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[AccessStation],[Year],[Group_Category],[AssetStatus]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"
                MyData.Tables(1).TableName = "AssetStatus"

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)


                'If MyData.Tables(0).Rows(0)("AccessStation").ToString <> "" Then
                '    dsStation = DIMERCO.SDK.Utilities.LSDK.getStationDataByWithList(MyData.Tables(0).Rows(0)("AccessStation").ToString)
                'Else
                '    dsStation = DIMERCO.SDK.Utilities.ReSM.GetStationList()
                'End If
                'Dim dtView As DataView = dsStation.Tables(0).DefaultView
                'dtView.Sort = "StationCode ASC"

                'ddlStation.DataSource = dtView
                'ddlStation.DataValueField = dtView.Table.Columns("StationID").ToString
                'ddlStation.DataTextField = dtView.Table.Columns("StationCode").ToString
                'ddlStation.DataBind()



                If Session("DA_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("DA_StationID").ToString
                End If


                ddlYear.DataSource = MyData.Tables(1)
                ddlYear.DataValueField = MyData.Tables(1).Columns("Year").ToString
                ddlYear.DataTextField = MyData.Tables(1).Columns("Year").ToString
                ddlYear.DataBind()
                ddlYear.SelectedValue = Now.Year



                ddlItem.DataSource = MyData.Tables(2)
                ddlItem.DataValueField = MyData.Tables(2).Columns("CategoryID").ToString
                ddlItem.DataTextField = MyData.Tables(2).Columns("Category").ToString
                ddlItem.DataBind()

                ddlStatus.DataSource = MyData.Tables(3)
                ddlStatus.DataValueField = MyData.Tables(3).Columns("StatusID").ToString
                ddlStatus.DataTextField = MyData.Tables(3).Columns("Status").ToString
                ddlStatus.DataBind()



            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub GetAssetList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetAssetList")

                With MyCommand

                    .Parameters.Add("@StationID", SqlDbType.VarChar, 3)
                    .Parameters("@StationID").Value = ddlStation.SelectedValue.ToString
                    .Parameters.Add("@Status", SqlDbType.Int)
                    .Parameters("@Status").Value = CInt(ddlStatus.SelectedValue.ToString)
                    .Parameters.Add("@Year", SqlDbType.Int)
                    If ddlYear.SelectedValue.ToString = "" Then
                        .Parameters("@Year").Value = DBNull.Value
                    Else
                        .Parameters("@Year").Value = CInt(ddlYear.SelectedValue.ToString)
                    End If

                    .Parameters.Add("@Item", SqlDbType.BigInt)
                    .Parameters("@Item").Value = CInt(ddlItem.SelectedValue.ToString)
                    .Parameters.Add("@SubCategoryID", SqlDbType.BigInt)
                    If ddlSubCategory.Items.Count > 0 Then
                        .Parameters("@SubCategoryID").Value = CInt(ddlSubCategory.SelectedValue.ToString)
                    Else
                        .Parameters("@SubCategoryID").Value = 0
                    End If
                    .Parameters.Add("@AssetNo", SqlDbType.NVarChar, 50)
                    .Parameters("@AssetNo").Value = txtAssetNo.Text.ToString
                    .Parameters.Add("@CategoryDesc", SqlDbType.NVarChar, 100)
                    .Parameters("@CategoryDesc").Value = txtCategoryDesc.Text.ToString
                    .Parameters.Add("@ApplyNewID", SqlDbType.VarChar, 20)
                    .Parameters("@ApplyNewID").Value = txtPurchaseNo.Text.ToString
                    .Parameters.Add("@AssetID", SqlDbType.NVarChar, 50)
                    .Parameters("@AssetID").Value = txtAssetID.Text.ToString


                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AssetList"

                gvAssetList.DataSource = MyData
                gvAssetList.DataMember = MyData.Tables(0).TableName
                gvAssetList.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "GetAssetList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnQuery_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQuery.Click
        Session("DA_StationID") = ddlStation.SelectedValue.ToString
        GetAssetList()
    End Sub

    Protected Sub gvAssetList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvAssetList.SelectedIndexChanged
        Dim lbtnAssetID As New LinkButton
        lbtnAssetID = gvAssetList.SelectedRow.FindControl("lbtnAssetID")

        Session("AssetID") = ""
        Session("AssetID") = lbtnAssetID.CommandArgument.ToString

        ' verify whether AssetDetailPage show in a popup window or not.
        If blnAssetDetailPagePopUP Then
            'Dim url As String = Replace(Request.Url.ToString(), "frmAssetList", "frmAssetDetail")

            Dim strScript As String
            strScript = "<script language=javascript>"
            'strScript += "theChild = window.open('" + url + "');"
            strScript += "theChild = window.open('frmAssetDetail.aspx?ParentForm=frmAssetList','Asset Detail','status= no, resizable=no, scrollbars=yes, toolbar=no,location=center,menubar=no,fullscreen=yes,titlebar=no,border-style=none');"
            strScript += "</script>"

            Page.ClientScript.RegisterStartupScript(GetType(Page), "_blank", strScript)
        Else
            Response.Redirect("~/Asset/frmAssetDetail.aspx")
        End If

    End Sub

    Protected Sub ddlItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlItem.SelectedIndexChanged
        GetSubCategory()
    End Sub

    Private Sub GetSubCategory()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByID")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[SubCategory]"
                    .Parameters.Add("@ID", SqlDbType.BigInt)
                    .Parameters("@ID").Value = CInt(ddlItem.SelectedValue.ToString)
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "SubCategory"


                ddlSubCategory.DataSource = MyData.Tables(0)
                ddlSubCategory.DataValueField = MyData.Tables(0).Columns("CategoryID").ToString
                ddlSubCategory.DataTextField = MyData.Tables(0).Columns("Category").ToString
                ddlSubCategory.DataBind()


            Catch ex As Exception
                Me.lblMsg.Text = "GetSubCategory: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub
End Class
