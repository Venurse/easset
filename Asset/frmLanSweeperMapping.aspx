﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmLanSweeperMapping.aspx.vb" Inherits="Asset_frmLanSweeperMapping" %>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" Namespace="eWorld.UI" TagPrefix="ew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LanSweeper Mapping</title>
  <script type="text/javascript">
         function showDate() 
        {
            var month_names = new Array("Jan", "Feb", "Mar", 
            "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
            "Oct", "Nov", "Dec");

            var d = new Date();
            var curr_day = d.getDay();
            var curr_date = d.getDate();
            var hours = d.getHours();
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            
            var curr_hours;
            var curr_minutes;
            var curr_seconds;

            if(hours<10)
            {
                curr_hours = "0" + hours;
            }
            else
            {
                curr_hours = hours;
            }
            
            if(minutes < 10)
            {
                curr_minutes = "0" + minutes;
            }
             else
            {
                curr_minutes = minutes;
            }
            
           if(seconds < 10)
            {
                curr_seconds = "0" + seconds;
            }
             else
            {
                curr_seconds = seconds;
            }

            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
 
            document.getElementById("hdfCurrentDateTime").value = curr_date + " " +  month_names[curr_month] +  " " + curr_year + " " + curr_hours + ":" + curr_minutes + ":" + curr_seconds
         }
      </script>
</head>
<body>
    <form id="frmLanSweeperMapping" runat="server">
    <div>
        <table style="width:100%;">
            <tr>
                <td colspan="8">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="100">
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" 
                        Text="Asset Name"></asp:Label>
                </td>
                <td width="5">
                    :</td>
                <td width="150">
                    <asp:TextBox ID="txtAssetName" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small" Width="196px"></asp:TextBox>
                </td>
                <td width="100">
                    &nbsp;</td>
                <td width="100">
                    <asp:Label ID="Label3" runat="server" Font-Names="Verdana" Font-Size="X-Small" 
                        Text="First Seen Date"></asp:Label>
                </td>
                <td width="5">
                    :</td>
                <td colspan="2">
                    <ew:calendarpopup id="cldSeenDateFrom" runat="server" width="90px" 
                        Font-Names="Verdana" Font-Size="X-Small" Nullable="True">
                    <selecteddatestyle backcolor="Yellow" font-names="Verdana,Helvetica,Tahoma,Arial"
                        font-size="XX-Small" forecolor="Black"></selecteddatestyle>
                    <holidaystyle backcolor="White" font-names="Verdana,Helvetica,Tahoma,Arial" font-size="XX-Small"
                        forecolor="Black"></holidaystyle>
                    <offmonthstyle backcolor="AntiqueWhite" font-names="Verdana,Helvetica,Tahoma,Arial"
                        font-size="XX-Small" forecolor="Gray"></offmonthstyle>
                    <monthheaderstyle backcolor="Yellow" font-names="Verdana,Helvetica,Tahoma,Arial"
                        font-size="XX-Small" forecolor="Black"></monthheaderstyle>
                    <weekdaystyle backcolor="White" font-names="Verdana,Helvetica,Tahoma,Arial" font-size="XX-Small"
                        forecolor="Black"></weekdaystyle>
                    <gototodaystyle backcolor="White" font-names="Verdana,Helvetica,Tahoma,Arial" font-size="XX-Small"
                        forecolor="Black"></gototodaystyle>
                    <cleardatestyle backcolor="White" font-names="Verdana,Helvetica,Tahoma,Arial" font-size="XX-Small"
                        forecolor="Black"></cleardatestyle>
                    <weekendstyle backcolor="LightGray" font-names="Verdana,Helvetica,Tahoma,Arial" font-size="XX-Small"
                        forecolor="Black"></weekendstyle>
                    <dayheaderstyle backcolor="Orange" font-names="Verdana,Helvetica,Tahoma,Arial" font-size="XX-Small"
                        forecolor="Black" />
                    <todaydaystyle backcolor="LightGoldenrodYellow" font-names="Verdana,Helvetica,Tahoma,Arial"
                        font-size="XX-Small" forecolor="Black" />
                </ew:calendarpopup>&nbsp;<asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small" 
                        Text=" To "></asp:Label>
                    <ew:calendarpopup id="cldSeenDateTo" runat="server" width="90px" 
                        Font-Names="Verdana" Font-Size="X-Small" Nullable="True">
                    <selecteddatestyle backcolor="Yellow" font-names="Verdana,Helvetica,Tahoma,Arial"
                        font-size="XX-Small" forecolor="Black"></selecteddatestyle>
                    <holidaystyle backcolor="White" font-names="Verdana,Helvetica,Tahoma,Arial" font-size="XX-Small"
                        forecolor="Black"></holidaystyle>
                    <offmonthstyle backcolor="AntiqueWhite" font-names="Verdana,Helvetica,Tahoma,Arial"
                        font-size="XX-Small" forecolor="Gray"></offmonthstyle>
                    <monthheaderstyle backcolor="Yellow" font-names="Verdana,Helvetica,Tahoma,Arial"
                        font-size="XX-Small" forecolor="Black"></monthheaderstyle>
                    <weekdaystyle backcolor="White" font-names="Verdana,Helvetica,Tahoma,Arial" font-size="XX-Small"
                        forecolor="Black"></weekdaystyle>
                    <gototodaystyle backcolor="White" font-names="Verdana,Helvetica,Tahoma,Arial" font-size="XX-Small"
                        forecolor="Black"></gototodaystyle>
                    <cleardatestyle backcolor="White" font-names="Verdana,Helvetica,Tahoma,Arial" font-size="XX-Small"
                        forecolor="Black"></cleardatestyle>
                    <weekendstyle backcolor="LightGray" font-names="Verdana,Helvetica,Tahoma,Arial" font-size="XX-Small"
                        forecolor="Black"></weekendstyle>
                    <dayheaderstyle backcolor="Orange" font-names="Verdana,Helvetica,Tahoma,Arial" font-size="XX-Small"
                        forecolor="Black" />
                    <todaydaystyle backcolor="LightGoldenrodYellow" font-names="Verdana,Helvetica,Tahoma,Arial"
                        font-size="XX-Small" forecolor="Black" />
                </ew:calendarpopup></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label6" runat="server" Font-Names="Verdana" Font-Size="X-Small" 
                        Text="Asset Type"></asp:Label>
                </td>
                <td>
                    :</td>
                <td>
                    <asp:DropDownList ID="ddlAssetType" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small" 
                        Text="Lansweeper Asset ID"></asp:Label>
                </td>
                <td>
                    :</td>
                <td>
                    <ew:numericbox id="txtAssetID" runat="server" font-names="Verdana" font-size="X-Small"
                        width="80px"></ew:numericbox>
                    </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label7" runat="server" Font-Names="Verdana" Font-Size="X-Small" 
                        Text="IP Location"></asp:Label>
                </td>
                <td>
                    :</td>
                <td>
                    <asp:DropDownList ID="ddlIPLocation" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Label ID="Label8" runat="server" Font-Names="Verdana" Font-Size="X-Small" 
                        Text="IP Address"></asp:Label>
                </td>
                <td>
                    :</td>
                <td>
                    <asp:TextBox ID="txtIPAddress" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small" Width="141px"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hdfSearchType" runat="server" />
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnSearch" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small" Text="Search" />
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:HiddenField ID="hdfCurrentDateTime" runat="server" />
                    </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" 
                        Text="Station" Visible="False"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;&nbsp;<asp:DropDownList ID="ddlStation" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small" Visible="False">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="7">
        <asp:GridView ID="gvLanSweeperAssetList" runat="server" AutoGenerateColumns="False" 
                        Font-Names="Verdana" Font-Size="X-Small" Caption="Lansweeper Asset List" 
                        EmptyDataText="Record not found" Width="98%">
            <Columns>
                <asp:TemplateField HeaderText="Select">
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" 
                            Checked='<%# Bind("LanSweeperAssetID") %>' />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSelect" runat="server" 
                            Enabled='<%# bind("EnableSelect") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset ID">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" 
                            Text='<%# Bind("LanSweeperAssetID") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblAssetID" runat="server" 
                            Text='<%# Bind("LanSweeperAssetID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="AssetTypename" HeaderText="Asset Type" />
                <asp:BoundField DataField="AssetUnique" HeaderText="Asset Unique" />
                <asp:BoundField DataField="AssetName" HeaderText="Asset Name" />
                <asp:BoundField DataField="Model" HeaderText="Model" />
                <asp:BoundField DataField="Description" HeaderText="Description" />
                <asp:BoundField DataField="IPLocation" HeaderText="IP Location" />
                <asp:BoundField DataField="IPAddress" HeaderText="IP Address" />
                <asp:BoundField DataField="Serialnumber" HeaderText="Serial Number" />
                <asp:BoundField DataField="Username" HeaderText="User Name" />
                <asp:BoundField DataField="Firstseen" HeaderText="First Seen" />
                <asp:BoundField DataField="Lastseen" HeaderText="Last Seen" />
                <asp:BoundField DataField="eAsset_AssetID" HeaderText="Mapped AssetID" />
            </Columns>
        </asp:GridView>
    
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td height="40" valign="middle">
                    <asp:Button ID="btnMapping" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small" Text="Mapping" onclientclick="showDate();" />
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="7">
        <asp:GridView ID="gvMapped" runat="server" AutoGenerateColumns="False" 
                        Font-Names="Verdana" Font-Size="X-Small" Caption="Mapped Asset List" 
                        EmptyDataText="Record not found" Width="98%">
            <Columns>
                <asp:TemplateField HeaderText="Remove" ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="lbtnRemove" runat="server" 
                            CommandArgument='<%# bind("LanSweeperAssetID") %>' CommandName="Delete" 
                            Text="Remove"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="LanSweeperAssetID" HeaderText="Asset ID" />
                <asp:BoundField DataField="AssetTypename" HeaderText="Asset Type" />
                <asp:BoundField DataField="AssetUnique" HeaderText="Asset Unique" />
                <asp:BoundField DataField="AssetName" HeaderText="Asset Name" />
                <asp:BoundField DataField="Model" HeaderText="Model" />
                <asp:BoundField DataField="Description" HeaderText="Description" />
                <asp:BoundField DataField="IPLocation" HeaderText="IP Location" />
                <asp:BoundField DataField="IPAddress" HeaderText="IP Address" />
                <asp:BoundField DataField="Serialnumber" HeaderText="Serial Number" />
                <asp:BoundField DataField="Username" HeaderText="User Name" />
                <asp:BoundField DataField="Firstseen" HeaderText="First Seen" />
                <asp:BoundField DataField="Lastseen" HeaderText="Last Seen" />
            </Columns>
        </asp:GridView>
    
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td height="40" valign="middle">
                    <asp:Button ID="btnClose" runat="server" Font-Names="Verdana" 
                        Font-Size="X-Small" Text="Close" />
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
