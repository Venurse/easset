Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Asset_frmAssetSpecification
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If Session("AssetID").ToString() = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If IsPostBack() = False Then
            hdfAssetID.Value = ""

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            btnSave.Enabled = False
            gvSpec.Columns(0).Visible = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "A0005")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Asset Specification Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnSave.Enabled = True
                        gvSpec.Columns(0).Visible = True
                    Else
                        Session("blnWrite") = False
                        btnSave.Enabled = False
                        gvSpec.Columns(0).Visible = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            If Session("AssetID") <> Nothing Then
                If Session("AssetID").ToString <> "" Then
                    hdfAssetID.Value = Session("AssetID").ToString
                End If
            End If
            'Session("AssetID") = ""
            GetAssetSpecification()
        Else

        End If
    End Sub

    Private Sub GetAssetSpecification()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetAssetSpecification")

                With MyCommand
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = hdfAssetID.Value.ToString
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AssetSpecification"

                gvSpec.DataSource = MyData
                gvSpec.DataMember = MyData.Tables(0).TableName
                gvSpec.DataBind()

                If MyData.Tables(1).Rows(0)("CreatedBy").ToString <> Session("DA_UserID").ToString Then
                    If Session("blnModifyOthers") = False Then
                        btnSave.Enabled = False
                        gvSpec.Columns(0).Visible = False
                    Else
                        btnSave.Enabled = True
                        gvSpec.Columns(0).Visible = True
                    End If

                    If Session("blnDeleteOthers") = True Then
                        gvSpec.Columns(0).Visible = True
                    Else
                        gvSpec.Columns(0).Visible = False
                    End If
                End If


            Catch ex As Exception
                Me.lblMsg.Text = "GetAssetSpecification: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim intID As Integer = 0
                Dim strMsg As String = ""
                Dim strSpecification As String = ""

                lblMsg.Text = ""

                If lblID.Text.Equals("") Then
                    intID = 0
                Else
                    intID = CInt(lblID.Text)
                End If

                strSpecification = txtSpecification.Text.ToString

                If strSpecification = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please key in Specification.')</script>")
                    Exit Sub
                End If

                If Len(strSpecification) > 1000 Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Specification over field size limit.')</script>")
                    Exit Sub
                End If

                If txtQty.Text.ToString = "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please key in Quantity.')</script>")
                    Exit Sub
                End If

                If CInt(txtQty.Text.ToString) <= 0 Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Quantity.')</script>")
                    'lblMsg.Text = "Invalid Quantity."
                    Exit Sub
                End If

                If txtQty.Text.ToString.Contains(".") = True Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Invalid Quantity.')</script>")
                    'lblMsg.Text = "Invalid Quantity."
                    Exit Sub
                End If

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveAssetSpecification", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = hdfAssetID.Value.ToString
                    .Parameters.Add("@SpecificationID", SqlDbType.Int)
                    .Parameters("@SpecificationID").Value = intID
                    .Parameters.Add("@Specification", SqlDbType.NVarChar)
                    .Parameters("@Specification").Value = strSpecification
                    .Parameters.Add("@Qty", SqlDbType.Int)
                    .Parameters("@Qty").Value = CInt(txtQty.Text.ToString)
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    'lblMsg.Text = ""
                    btnClear_Click(sender, e)
                    GetAssetSpecification()

                    'lblMsg.Text = "Save Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        lblID.Text = ""
        txtSpecification.Text = ""
        txtQty.Text = ""
        txtSpecification.Focus()
    End Sub

    Protected Sub gvSpec_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSpec.SelectedIndexChanged
        Dim strID As String = 0
        strID = gvSpec.SelectedDataKey.Value.ToString
        lblID.Text = strID

        Dim lbtnSpec As New LinkButton
        lbtnSpec = gvSpec.SelectedRow.FindControl("lbtnSpec")
        txtSpecification.Text = lbtnSpec.Text.ToString

        Dim lblQuantity As New Label
        lblQuantity = gvSpec.SelectedRow.FindControl("lblQuantity")
        txtQty.Text = lblQuantity.Text.ToString


    End Sub

    Protected Sub gvSpec_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvSpec.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim intID As Integer = 0
                Dim lbtnRemove As New LinkButton

                lblMsg.Text = ""

                lbtnRemove = gvSpec.Rows(e.RowIndex).FindControl("lbtnRemove")
                intID = CInt(lbtnRemove.CommandArgument.ToString)

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteAssetSpecification", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = hdfAssetID.Value.ToString
                    .Parameters.Add("@SpecificationID", SqlDbType.Int)
                    .Parameters("@SpecificationID").Value = intID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    lblID.Text = ""

                    btnClear_Click(sender, e)
                    GetAssetSpecification()
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                    'lblMsg.Text = "Delete Successful"
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvSpec_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        Dim ParentFormID As String = ""
        ParentFormID = Request.QueryString("ParentForm").ToString
        Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener =self;window.close();</script>")
    End Sub
End Class
