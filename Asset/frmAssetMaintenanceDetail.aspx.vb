Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Asset_frmAssetMaintenanceDetail
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        'If Session("AssetID") = Nothing Then
        '    Response.Redirect("~/frmLogin.aspx")
        '    Exit Sub
        'End If

        If IsPostBack() = False Then
            hdfAssetID.Value = ""
            hdfAssetMaintenanceID.Value = ""

            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "A0003")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Asset Maintenance Detail Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                    Else
                        Session("blnWrite") = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            If Session("AssetID") <> Nothing Then
                If Session("AssetID").ToString <> "" Then
                    hdfAssetID.Value = Session("AssetID").ToString
                End If
            End If

            If Session("AssetMaintenanceID") <> Nothing Then
                If Session("AssetMaintenanceID").ToString <> "" Then
                    hdfAssetMaintenanceID.Value = Session("AssetMaintenanceID").ToString
                End If
            End If

            Session("AssetID") = ""
            Session("AssetMaintenanceID") = ""
            GetAssetMaintenanceDetail()

        End If
    End Sub

    Private Sub GetAssetMaintenanceDetail()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetAssetMaintenanceDetail")

                With MyCommand

                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = hdfAssetID.Value.ToString
                    .Parameters.Add("@MaintenanceID", SqlDbType.VarChar, 20)
                    .Parameters("@MaintenanceID").Value = hdfAssetMaintenanceID.Value.ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AssetMaintenanceDetail"

                lblDate.Text = MyData.Tables(0).Rows(0)("CreatedOn").ToString
                lblIssue.Text = MyData.Tables(0).Rows(0)("Issue").ToString
                lblDescription.Text = MyData.Tables(0).Rows(0)("Description").ToString
                btnView.Text = MyData.Tables(0).Rows(0)("ApplyNewID").ToString

                gvQuoReceipt.DataSource = MyData
                gvQuoReceipt.DataMember = MyData.Tables(2).TableName
                gvQuoReceipt.DataBind()

                gvApprovalHistory.DataSource = MyData
                gvApprovalHistory.DataMember = MyData.Tables(3).TableName
                gvApprovalHistory.DataBind()


            Catch ex As Exception
                Me.lblMsg.Text = "GetAssetMaintenanceDetail: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Session("ApplyNewID") = btnView.Text.ToString
        Response.Redirect("~/Purchase/frmPurchaseDetail.aspx")
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Response.Write("<script language=""Javascript"">window.close();</script>")
        'Dim ParentFormID As String = ""
        'ParentFormID = Request.QueryString("ParentForm").ToString
        'Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener =self;window.close();</script>")
    End Sub
End Class
