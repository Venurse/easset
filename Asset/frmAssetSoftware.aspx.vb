Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Asset_frmAssetSoftware
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")
    Dim dsSoftware As New DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        'If Session("AssetID").ToString() = Nothing Then
        '    Response.Redirect("~/frmLogin.aspx")
        '    Exit Sub
        'End If


        If IsPostBack() = False Then
            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            btnSave.Enabled = False
            gvSoftware.Columns(0).Visible = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "A0004")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Asset Software Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnSave.Enabled = True
                        gvSoftware.Columns(0).Visible = True
                    Else
                        Session("blnWrite") = False
                        btnSave.Enabled = False
                        gvSoftware.Columns(0).Visible = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            BindDropDownList()
            If Session("AssetID") <> Nothing Then
                If Session("AssetID").ToString <> "" Then
                    hdfAssetID.Value = Session("AssetID").ToString
                End If
            End If
            Session("AssetID") = ""
            GetAssetSoftware()
        Else

        End If
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByID")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[Software],[SoftwareCategory]"
                    .Parameters.Add("@ID", SqlDbType.BigInt)
                    .Parameters("@ID").Value = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "Software"
                MyData.Tables(1).TableName = "SoftwareCategory"

                ddlSoftwareCategory.DataSource = MyData.Tables(1)
                ddlSoftwareCategory.DataValueField = MyData.Tables(1).Columns("SoftwareCategoryID").ToString
                ddlSoftwareCategory.DataTextField = MyData.Tables(1).Columns("SoftwareCategory").ToString
                ddlSoftwareCategory.DataBind()


                dsSoftware.Tables.Add(MyData.Tables(0).Copy)
                dsSoftware.Tables(0).TableName = "Software"
                ViewState("dsSoftware") = dsSoftware

                BindSoftware(CInt(ddlSoftwareCategory.SelectedValue.ToString))

            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Sub BindSoftware(ByVal intSoftwareCategory As Integer)
        dsSoftware = ViewState("dsSoftware")

        Dim objDataSet2 As New DataSet
        objDataSet2.Merge(dsSoftware.Tables(0).Select("SoftwareCategoryID=" & intSoftwareCategory & " OR SoftwareCategoryID=0"))

        If objDataSet2.Tables.Count = 0 Then
            objDataSet2 = dsSoftware.Clone()
        End If

        ddlSoftware.DataSource = objDataSet2.Tables(0)
        ddlSoftware.DataValueField = objDataSet2.Tables(0).Columns("SoftwareID").ToString
        ddlSoftware.DataTextField = objDataSet2.Tables(0).Columns("Software").ToString
        ddlSoftware.DataBind()

    End Sub

    Private Sub GetAssetSoftware()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetAssetSoftware")

                With MyCommand
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = hdfAssetID.Value.ToString
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AssetSoftware"

                gvSoftware.DataSource = MyData
                gvSoftware.DataMember = MyData.Tables(0).TableName
                gvSoftware.DataBind()

                If MyData.Tables(1).Rows(0)("CreatedBy").ToString <> Session("DA_UserID").ToString Then
                    If Session("blnModifyOthers") = False Then
                        btnSave.Enabled = False
                        gvSoftware.Columns(0).Visible = False
                    Else
                        btnSave.Enabled = True
                        gvSoftware.Columns(0).Visible = True
                    End If

                    If Session("blnDeleteOthers") = True Then
                        gvSoftware.Columns(0).Visible = True
                    Else
                        gvSoftware.Columns(0).Visible = False
                    End If
                End If

            Catch ex As Exception
                Me.lblMsg.Text = "GetAssetSoftware: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim intID As Integer = 0
                Dim strMsg As String = ""
                Dim intSoftware As Integer = 0

                lblMsg.Text = ""

                If lblID.Text.Equals("") Then
                    intID = 0
                Else
                    intID = CInt(lblID.Text)
                End If

                intSoftware = CInt(ddlSoftware.SelectedValue.ToString)

                If intSoftware = 0 Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please select Software.')</script>")
                    'lblMsg.Text = "Please select Software."
                    Exit Sub
                End If

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_SaveAssetSoftware", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ID", SqlDbType.Int)
                    .Parameters("@ID").Value = intID
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = hdfAssetID.Value.ToString
                    .Parameters.Add("@SoftwareID", SqlDbType.BigInt)
                    .Parameters("@SoftwareID").Value = intSoftware
                    .Parameters.Add("@Version", SqlDbType.NVarChar, 50)
                    .Parameters("@Version").Value = txtVersion.Text.ToString
                    .Parameters.Add("@Language", SqlDbType.NVarChar, 50)
                    .Parameters("@Language").Value = txtLanguage.Text.ToString
                    .Parameters.Add("@Reference", SqlDbType.NVarChar)
                    .Parameters("@Reference").Value = txtReference.Text.ToString
                    .Parameters.Add("@License", SqlDbType.VarChar, 1)
                    .Parameters("@License").Value = ddlLicense.SelectedValue.ToString
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    'lblMsg.Text = ""
                    btnClear_Click(sender, e)
                    GetAssetSoftware()

                    'lblMsg.Text = "Save Successful"
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Save Successful')</script>")
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "btnSave_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        lblMsg.Text = ""
        ClearData()
    End Sub

    Private Sub ClearData()
        ddlSoftware.Enabled = True
        ddlSoftware.SelectedValue = 0
        txtVersion.Text = ""
        txtLanguage.Text = ""
        ddlLicense.SelectedValue = "Y"
        txtReference.Text = ""

    End Sub

    Protected Sub gvSoftware_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSoftware.SelectedIndexChanged
        Dim strID As String = 0

        Dim hdfSoftwareCategoryID As New HiddenField
        hdfSoftwareCategoryID = gvSoftware.SelectedRow.FindControl("hdfSoftwareCategoryID")
        ddlSoftwareCategory.SelectedValue = hdfSoftwareCategoryID.Value.ToString
        BindSoftware(CInt(hdfSoftwareCategoryID.Value.ToString))

        Dim lbtnSoftware As New LinkButton
        lbtnSoftware = gvSoftware.SelectedRow.FindControl("lbtnSoftware")
        ddlSoftware.SelectedValue = CInt(lbtnSoftware.CommandArgument.ToString)
        ddlSoftware.Enabled = False

        Dim hdfID As New HiddenField
        hdfID = gvSoftware.SelectedRow.FindControl("hdfID")
        lblID.Text = hdfID.Value.ToString

        Dim lblVersion As New Label
        lblVersion = gvSoftware.SelectedRow.FindControl("lblVersion")
        txtVersion.Text = lblVersion.Text.ToString

        Dim lblLanguage As New Label
        lblLanguage = gvSoftware.SelectedRow.FindControl("lblLanguage")
        txtLanguage.Text = lblLanguage.Text.ToString

        Dim lblLicense As New Label
        lblLicense = gvSoftware.SelectedRow.FindControl("lblLicense")
        ddlLicense.SelectedValue = lblLicense.Text.ToString

        Dim lblReference As New Label
        lblReference = gvSoftware.SelectedRow.FindControl("lblReference")
        txtReference.Text = lblReference.Text.ToString



    End Sub

    Protected Sub gvSoftware_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvSoftware.RowDeleting
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String
                Dim intSofwareID As Integer = 0
                Dim intID As Integer = 0
                Dim lbtnRemove As New LinkButton
                Dim hdfID As New HiddenField

                lblMsg.Text = ""

                lbtnRemove = gvSoftware.Rows(e.RowIndex).FindControl("lbtnRemove")
                intSofwareID = CInt(lbtnRemove.CommandArgument.ToString)

                hdfID = gvSoftware.Rows(e.RowIndex).FindControl("hdfID")
                intID = CInt(hdfID.Value.ToString)

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_DeleteAssetSoftware", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@ID", SqlDbType.Int)
                    .Parameters("@ID").Value = intID
                    .Parameters.Add("@AssetID", SqlDbType.VarChar, 20)
                    .Parameters("@AssetID").Value = hdfAssetID.Value.ToString
                    .Parameters.Add("@SoftwareID", SqlDbType.BigInt)
                    .Parameters("@SoftwareID").Value = intSofwareID
                    .Parameters.Add("@Msg", SqlDbType.VarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                End With

                If strMsg <> "" Then
                    'lblMsg.Text = strMsg
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    lblID.Text = ""

                    btnClear_Click(sender, e)
                    GetAssetSoftware()
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Delete Successful')</script>")
                    'lblMsg.Text = "Delete Successful"
                End If
            Catch ex As Exception
                Me.lblMsg.Text = "gvSoftware_RowDeleting: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReturn.Click
        Dim ParentFormID As String = ""
        ParentFormID = Request.QueryString("ParentForm").ToString
        Response.Write("<script language=""Javascript"">window.opener.document." + ParentFormID + ".submit();window.opener =self;window.close();</script>")
    End Sub

    Protected Sub ddlSoftwareCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSoftwareCategory.SelectedIndexChanged
        BindSoftware(CInt(ddlSoftwareCategory.SelectedValue.ToString))
    End Sub
End Class
