<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAssetSpecification.aspx.vb" Inherits="Asset_frmAssetSpecification" ValidateRequest="false"%>

<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Asset Specification</title>
</head>
<body>
    <form id="frmAssetSpecification" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 96px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td style="width: 96px">
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Asset Specification"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 96px">
                </td>
                <td colspan="7">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 96px">
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 96px">
                </td>
                <td colspan="7">
                    <asp:Label ID="lblID" runat="server" Font-Names="Verdana" Font-Size="X-Small" Visible="False"></asp:Label>
                    <asp:HiddenField ID="hdfAssetID" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td style="width: 96px">
                </td>
                <td colspan="7">
                    <table style="width: 650px">
                        <tr>
                            <td style="width: 150px">
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Specification"></asp:Label></td>
                            <td style="width: 5px">
                                :</td>
                            <td>
                    <asp:TextBox ID="txtSpecification" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        Width="450px" MaxLength="1000"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Quantity"></asp:Label></td>
                            <td>
                                :</td>
                            <td>
                    <ew:numericbox id="txtQty" runat="server" font-names="Verdana" font-size="X-Small" PositiveNumber="True"></ew:numericbox></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                    <table style="width: 450px">
                        <tr>
                            <td style="width: 150px">
                                <asp:Button ID="btnSave" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Save"
                                    Width="85px" /></td>
                            <td style="width: 150px">
                                <asp:Button ID="btnClear" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Clear"
                                    Width="85px" /></td>
                            <td style="width: 150px">
                            </td>
                        </tr>
                    </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td style="width: 96px">
                </td>
                <td colspan="8">
                    <asp:GridView ID="gvSpec" runat="server" AutoGenerateColumns="False" Font-Names="Verdana"
                        Font-Size="X-Small" DataKeyNames="SpecificationID" BorderColor="Silver" BorderWidth="1px" EmptyDataText="Record Not Found">
                        <Columns>
                            <asp:TemplateField HeaderText="Remove">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnRemove" runat="server" CommandArgument='<%# Bind("SpecificationID") %>' CommandName="Delete" OnClientClick="return confirm('Are you sure you want to remove this Specification?');">Remove</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Specification">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Specification") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnSpec" runat="server" CommandArgument='<%# Bind("SpecificationID") %>'
                                        Text='<%# Bind("Specification") %>' CommandName="Select"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quantity">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Quantity") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblQuantity" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td style="width: 96px">
                </td>
                <td colspan="7"><table style="width: 650px">
                    <tr>
                        <td style="width: 150px">
                        </td>
                        <td style="width: 5px">
                        </td>
                        <td>
                            <table style="width: 335px">
                                <tr>
                                    <td style="width: 150px">
                                        <asp:Button ID="btnReturn" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Return"
                                    Width="85px" /></td>
                                    <td style="width: 150px">
                                    </td>
                                    <td style="width: 150px">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td style="width: 96px">
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
