Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Asset_frmAsset_eReq
    Inherits System.Web.UI.Page
    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        Me.Page.MaintainScrollPositionOnPostBack = True

        If IsPostBack() = False Then
            Clear()
            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            btnApprove.Enabled = False
            btnReject.Enabled = False
            btnModify.Enabled = False


            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "A0009")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the eRequisition Approval Page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                        btnApprove.Enabled = True
                        btnReject.Enabled = True
                        btnModify.Enabled = True

                    Else
                        Session("blnWrite") = False
                        btnApprove.Enabled = False
                        btnReject.Enabled = False
                        btnModify.Enabled = False
                    End If
                End If

                If Mid(strAccessRight, 4, 1) = 1 Then
                    Session("blnModifyOthers") = True
                Else
                    Session("blnModifyOthers") = False
                End If

                If Mid(strAccessRight, 5, 1) = 1 Then
                    Session("blnDeleteOthers") = True
                Else
                    Session("blnDeleteOthers") = False
                End If
            End If

            GeteRequisitionApprovalList()

            Me.btnApprove.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnApprove, "", "btnReject", "btnModify", "", "", ""))
            Me.btnReject.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnReject, "", "btnApprove", "btnModify", "", "", ""))
            Me.btnModify.Attributes.Add("onclick", clsCF.DisableTheButton(Me.Page, Me.btnModify, "", "btnApprove", "btnReject", "", "", ""))

        End If
    End Sub

    Private Sub GeteRequisitionApprovalList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GeteRequisitionApprovalList")

                With MyCommand
                    .Parameters.Add("@ApprovalParty", SqlDbType.VarChar, 6)
                    .Parameters("@ApprovalParty").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "eRequisitionApprovalList"

                gveReqList.DataSource = MyData
                gveReqList.DataMember = MyData.Tables(0).TableName
                gveReqList.DataBind()

            Catch ex As Exception
                Me.lblMsg.Text = "GeteRequisitionApprovalList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub gveReqList_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles gveReqList.SelectedIndexChanged
        Clear()
        Dim streRequisitionNo As String = gveReqList.SelectedValue.ToString()
        GeteRequisitionDetail(streRequisitionNo)
    End Sub

    Private Sub GeteRequisitionDetail(ByVal streRequisitionNo As String)
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GeteRequisitionDetail")

                With MyCommand
                    .Parameters.Add("@eRequisitionNo", SqlDbType.VarChar, 50)
                    .Parameters("@eRequisitionNo").Value = streRequisitionNo
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "eRequisitionDetail"

                lbleRequisitionNo.Text = MyData.Tables(0).Rows(0)("eRequisitionNo").ToString()
                lblDateRequired.Text = MyData.Tables(0).Rows(0)("DateRequired").ToString
                lblStation.Text = MyData.Tables(0).Rows(0)("StationCode").ToString
                lblCondition.Text = MyData.Tables(0).Rows(0)("Condition").ToString
                lblApplyBy.Text = MyData.Tables(0).Rows(0)("ApplyBy").ToString
                lblLocation.Text = MyData.Tables(0).Rows(0)("Location").ToString
                lblParentCategory.Text = MyData.Tables(0).Rows(0)("ParentCategory").ToString
                lblCategory.Text = MyData.Tables(0).Rows(0)("Category").ToString

                lblSpecialReqCategory.Visible = False
                lblSpecialRequest.Visible = False
                lblSpecialReqCategory.Text = MyData.Tables(0).Rows(0)("SpecialReqCategory").ToString

                If MyData.Tables(0).Rows(0)("SpecialReqCategory").ToString <> "" Then
                    lblSpecialReqCategory.Visible = True
                    lblSpecialRequest.Visible = True
                End If


                lblBrandModel.Text = MyData.Tables(0).Rows(0)("BrandModel").ToString
                lblSpecification.Text = MyData.Tables(0).Rows(0)("Specification").ToString
                lblAppToBeUsed.Text = MyData.Tables(0).Rows(0)("ApplicationToBeUsed").ToString
                lblDescription.Text = MyData.Tables(0).Rows(0)("Description").ToString
                lblRemarks.Text = MyData.Tables(0).Rows(0)("Remarks").ToString
                lblVendor.Text = MyData.Tables(0).Rows(0)("Vendor").ToString
                lblWarrantyPeriod.Text = MyData.Tables(0).Rows(0)("WarrantyPeriod").ToString
                lblQuantity.Text = MyData.Tables(0).Rows(0)("Quantity").ToString
                lblPurchaseCost.Text = MyData.Tables(0).Rows(0)("PurchaseCost").ToString
                lblCurrencyRate.Text = MyData.Tables(0).Rows(0)("CurrencyRate").ToString
                lblLocalApprovedBy.Text = MyData.Tables(0).Rows(0)("LocalApprovedBy").ToString

                gvUserList.Visible = True
                gvUserList.DataSource = MyData
                gvUserList.DataMember = MyData.Tables(1).TableName
                gvUserList.DataBind()

                lbtnQuotation.Text = MyData.Tables(2).Rows(0)("DocPicLink").ToString

                btnApprove.Enabled = True
                btnModify.Enabled = True
                btnReject.Enabled = True

            Catch ex As Exception
                Me.lblMsg.Text = "GeteRequisitionApprovalList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        eRequisitionApproval("A")
    End Sub

    Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click
        eRequisitionApproval("W")
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        eRequisitionApproval("R")
    End Sub

    Private Sub eRequisitionApproval(ByVal strStatus As String)
        Using sqlConn As New SqlConnection(strConnectionString)
            Try
                Dim strMsg As String = ""
                Dim strMailID As String = ""
                Dim strApplyNewID As String = ""
                Dim strStatusValue As String = ""

                lblMsg.Text = ""

                If strStatus = "A" Then
                    strStatusValue = "Approve"
                ElseIf strStatus = "W" Then
                    strStatusValue = "Request Modify"
                ElseIf strStatus = "R" Then
                    strStatusValue = "Reject"
                End If

                If strStatus <> "A" Then
                    If txtComment.Text.ToString = "" Then
                        txtComment.Focus()
                        ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Please enter Comment')</script>")
                        Exit Sub
                    End If
                End If

                clsCF.CheckConnectionState(sqlConn)

                Dim cmd As New SqlCommand("SP_UpdateeReqACK", sqlConn)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@eRequisitionNo", SqlDbType.NVarChar, 50)
                    .Parameters("@eRequisitionNo").Value = lbleRequisitionNo.Text.ToString()

                    .Parameters.Add("@eReq_Status", SqlDbType.VarChar, 1)
                    .Parameters("@eReq_Status").Value = strStatus

                    .Parameters.Add("@eReq_ApprovedBy", SqlDbType.VarChar, 6)
                    .Parameters("@eReq_ApprovedBy").Value = Session("DA_UserID").ToString()

                    .Parameters.Add("@eReq_ApprovedOn", SqlDbType.NVarChar, 50)
                    .Parameters("@eReq_ApprovedOn").Value = hdfCurrentDateTime.Value.ToString()

                    .Parameters.Add("@eReq_Comments", SqlDbType.NVarChar)
                    .Parameters("@eReq_Comments").Value = txtComment.Text.ToString()

                    .Parameters.Add("@biMailID", SqlDbType.NVarChar, 500)
                    .Parameters("@biMailID").Direction = ParameterDirection.Output
                    .Parameters.Add("@Msg", SqlDbType.NVarChar, 255)
                    .Parameters("@Msg").Direction = ParameterDirection.Output
                    .CommandTimeout = 0
                    .ExecuteNonQuery()

                    strMsg = .Parameters("@Msg").Value.ToString()
                    strMailID = .Parameters("@biMailID").Value.ToString()
                End With

                If strMsg <> "" Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strMsg + "')</script>")
                Else
                    Dim intCnt As Integer = 0
                    Dim intMailID As Integer
                    Dim arrMailID() As String = strMailID.Split(New Char() {","c})

                    For Each intMailID In arrMailID
                        intCnt = intCnt + 1
                        If intMailID <> 0 Then
                            Dim strResult As String = ""
                            strResult = clsCF.SendMail_ApplyNew(intMailID, strApplyNewID)

                            If strResult = "" Then
                                clsCF.UpdateMailQueue(intMailID, 1, strStatusValue + " Successful", hdfCurrentDateTime.Value.ToString)
                                If intCnt = 1 Then
                                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('" + strStatusValue + " Successful')</script>")
                                End If
                            Else
                                clsCF.UpdateMailQueue(intMailID, 2, "Send Fail - " & strResult, hdfCurrentDateTime.Value.ToString)
                                If intCnt = 1 Then
                                    ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Approval Fail - " + strResult + "')</script>")
                                End If
                            End If
                        End If
                    Next
                    GeteRequisitionApprovalList()
                    Clear()
                End If
            Catch ex As Exception
                ClientScript.RegisterStartupScript(Me.GetType(), "key", "<script>alert('Error: " + ex.Message.ToString + "')</script>")
                'Return ex.Message.ToString
            Finally
                clsCF.CloseConnection(sqlConn)
            End Try
        End Using
    End Sub

    Private Sub Clear()
        lbleRequisitionNo.Text = ""
        lblDateRequired.Text = ""
        lblStation.Text = ""
        lblApplyBy.Text = ""
        lblLocation.Text = ""
        lblCondition.Text = ""
        lblParentCategory.Text = ""
        lblCategory.Text = ""
        lblSpecialRequest.Visible = False
        lblSpecialReqCategory.Text = ""
        lblBrandModel.Text = ""
        lblSpecification.Text = ""
        lblAppToBeUsed.Text = ""
        lblDescription.Text = ""
        lblRemarks.Text = ""
        lblVendor.Text = ""
        lblWarrantyPeriod.Text = ""
        lblQuantity.Text = ""
        lbtnQuotation.Text = ""
        lblPurchaseCost.Text = ""
        lblCurrencyRate.Text = ""
        lblLocalApprovedBy.Text = ""
        txtComment.Text = ""
        gvUserList.Dispose()
        gvUserList.Visible = False
        btnApprove.Enabled = False
        btnModify.Enabled = False
        btnReject.Enabled = False
    End Sub

End Class
