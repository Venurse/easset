<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAssetMaintenanceDetail.aspx.vb" Inherits="Asset_frmAssetMaintenanceDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Maintenance Detail</title>
</head>
<body>
    <form id="frmAssetMaintenanceDetail" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td style="width: 10%">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 50px">
                </td>
                <td style="width: 150px">
                </td>
                <td style="width: 5px">
                </td>
                <td>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                        Text="Maintenance Detail"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="7">
                    <asp:Label ID="lblMsg" runat="server" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Red"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:HiddenField ID="hdfAssetID" runat="server" />
                </td>
                <td>
                </td>
                <td>
                    <asp:HiddenField ID="hdfAssetMaintenanceID" runat="server" />
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label2" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Date"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:Label ID="lblDate" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="Verdana" Font-Size="X-Small" Text="Issue"></asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:Label ID="lblIssue" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="X-Small">Description</asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:Label ID="lblDescription" runat="server" Font-Names="Verdana" Font-Size="X-Small"></asp:Label></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label5" runat="server" Font-Names="Verdana" Font-Size="X-Small">Quotation/Receipt</asp:Label></td>
                <td>
                    :</td>
                <td colspan="5">
                    <asp:GridView ID="gvQuoReceipt" runat="server" AutoGenerateColumns="False" Font-Names="Verdana"
                        Font-Size="X-Small">
                        <Columns>
                            <asp:BoundField DataField="QuotationReceiptNo" HeaderText="Quotation / Receipt No" />
                            <asp:TemplateField HeaderText="File">
                                <ItemTemplate>
                                    <asp:Label ID="lblQuoReceipt" runat="server" Text='<%# Bind("DocPicLink") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label7" runat="server" Font-Names="Verdana" Font-Size="X-Small">Detail</asp:Label></td>
                <td>
                    :</td>
                <td>
                    <asp:Button ID="btnView" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        ForeColor="Blue" /></td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                    <asp:Label ID="Label8" runat="server" Font-Names="Verdana" Font-Size="X-Small">Approval History</asp:Label></td>
                <td>
                    :</td>
                <td colspan="5">
                    <asp:GridView ID="gvApprovalHistory" runat="server" BackColor="White" BorderColor="#336666"
                        BorderStyle="Double" BorderWidth="3px" CellPadding="4" Font-Names="Verdana" Font-Size="X-Small"
                        GridLines="Horizontal">
                        <RowStyle BackColor="White" ForeColor="#333333" />
                        <FooterStyle BackColor="White" ForeColor="#333333" />
                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                    </asp:GridView>
                </td>
                <td>
                </td>
            </tr>
            <tr style="font-size: 12pt; color: #000000; font-family: Times New Roman">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                    <table style="width: 335px">
                        <tr>
                            <td style="width: 150px">
                                <asp:Button ID="btnClose" runat="server" Font-Names="Verdana" Font-Size="Small" Text="Close"
                                    Width="100px" /></td>
                            <td style="width: 150px">
                            </td>
                            <td style="width: 150px">
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
