Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings

Partial Class Asset_frmUploadAsset
    Inherits System.Web.UI.Page

    Dim clsCF As New clsComFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")
    Dim strReSM As String = ConfigurationSettings.AppSettings("ReSM")
    Dim strExcelExtension As String = ConfigurationSettings.AppSettings("ExcelExtension")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("DA_UserID") = Nothing Then
            Response.Redirect("~/frmLogin.aspx")
            Exit Sub
        End If

        If IsPostBack() = False Then
            Session("blnAccess") = False
            Session("blnReadOnly") = False
            Session("blnWrite") = False
            Session("blnModifyOthers") = False
            Session("blnDeleteOthers") = False

            Dim strAccessRight As String = clsCF.CheckUserAccessRight(Session("DA_UserID"), "A0007")

            If Len(strAccessRight) >= 7 Then
                Session("AccessStation") = Mid(strAccessRight, 7, Len(strAccessRight)).ToString
            Else
                Session("AccessStation") = ""
            End If


            If IsNumeric(Left(strAccessRight, 6)) = True Then
                If Mid(strAccessRight, 1, 1) = 1 Then
                    Session("blnAccess") = True
                Else
                    Session("blnAccess") = False
                    Session("AccessRightMsg") = "You are no right to access the Upload Asset page."
                    Response.Redirect("~/ListHome.aspx")
                    Exit Sub
                End If

                If Mid(strAccessRight, 2, 1) = 1 Then
                    Session("blnReadOnly") = True
                Else
                    Session("blnReadOnly") = False

                    If Mid(strAccessRight, 3, 1) = 1 Then
                        Session("blnWrite") = True
                    Else
                        Session("blnWrite") = False
                    End If
                End If
            End If

            BindDropDownList()

        End If
    End Sub

    Private Sub BindDropDownList()
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_GetReferenceData_ByUser")

                With MyCommand

                    .Parameters.Add("@ReSM", SqlDbType.VarChar, 50)
                    .Parameters("@ReSM").Value = strReSM
                    .Parameters.Add("@ReferenceType", SqlDbType.VarChar, 100)
                    .Parameters("@ReferenceType").Value = "[AccessStation],[ImageDocPath_Temp]"
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "AccessStation"

                Dim strIncludeStationList As String
                strIncludeStationList = MyData.Tables(0).Rows(0)("AccessStation").ToString
                clsCF.Bind_dllStation(strIncludeStationList, ddlStation)


                If Session("DA_StationID").ToString <> "" Then
                    ddlStation.SelectedValue = Session("DA_StationID").ToString
                End If

                hdfFilePath.Value = MyData.Tables(1).Rows(0)("ImageDocPath_Temp").ToString


            Catch ex As Exception
                Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Using MyConnection As New SqlConnection(strConnectionString)
            Try
                lblMsg.Text = ""

                If fuFile.FileName.ToString = "" Then
                    lblMsg.Text = "Please select a file."
                    Exit Sub
                End If

                'Dim fn As String = "E:\EASSET_UPLOAD\DIMPEN_AA.xls"

                'If SqlBulkCopy(fn, "DIMPEN_AA.xls") = False Then
                '    Exit Sub
                'End If

                If (Regex.IsMatch(fuFile.FileName, strExcelExtension, RegexOptions.IgnoreCase)) = False Then
                    lblMsg.Text = "Invalid file format."
                    Exit Sub
                End If

                If FileIO.FileSystem.DirectoryExists(hdfFilePath.Value.ToString + "/") = False Then
                    FileIO.FileSystem.CreateDirectory(hdfFilePath.Value.ToString + "/")
                End If

                Dim strFileName As String = ""
                Dim strFileExt As String = ""
                strFileName = System.IO.Path.GetFileName(fuFile.FileName)
                If strFileName.EndsWith(".xls") Then
                    strFileName = strFileName.Replace(".xls", "")
                    strFileExt = ".xls"
                ElseIf strFileName.EndsWith(".xlsx") Then
                    strFileName = strFileName.Replace(".xlsx", "")
                    strFileExt = ".xlsx"
                End If
                strFileName = strFileName + "_" + Format(DateTime.Now, "dd MMM yyyy hh:mm:ss").ToString.Replace(" ", "").Replace(":", "") + strFileExt


                Dim fn As String = hdfFilePath.Value.ToString + "/" + strFileName

                fuFile.SaveAs(hdfFilePath.Value.ToString + "/" + strFileName)

                'Dim fn As String = hdfFilePath.Value.ToString + "/" + System.IO.Path.GetFileName(fuFile.FileName)
                'fuFile.SaveAs(hdfFilePath.Value.ToString + "/" + System.IO.Path.GetFileName(fuFile.FileName))

                If FileIO.FileSystem.FileExists(fn) = False Then
                    fn = ""
                    lblMsg.Text = "Unable to upload. Please contact System Admin."
                    Exit Sub
                End If


                If SqlBulkCopy(fn, strFileName) = False Then
                    Exit Sub
                End If

                'If SqlBulkCopy(fn, fuFile.FileName.ToString) = False Then
                '    Exit Sub
                'End If

                'Process Data and get Import Data
                Dim dsStation As New DataSet
                Dim MyData As New DataSet
                Dim MyAdapter As SqlDataAdapter
                Dim MyCommand As SqlCommand

                clsCF.CheckConnectionState(MyConnection)

                MyCommand = New SqlCommand("SP_Upload_OldAssetDetail")

                With MyCommand

                    .Parameters.Add("@StationCode", SqlDbType.VarChar, 6)
                    .Parameters("@StationCode").Value = ddlStation.SelectedItem.Text.ToString
                    .Parameters.Add("@UserID", SqlDbType.VarChar, 6)
                    .Parameters("@UserID").Value = Session("DA_UserID").ToString()
                    .Parameters.Add("@CurrentDate", SqlDbType.NVarChar, 50)
                    .Parameters("@CurrentDate").Value = hdfCurrentDateTime.Value.ToString()
                    .CommandType = CommandType.StoredProcedure
                    .Connection = MyConnection
                End With

                MyAdapter = New SqlDataAdapter(MyCommand)
                MyAdapter.Fill(MyData)
                MyData.Tables(0).TableName = "Data"

                gvImport.Dispose()

                gvImport.DataSource = MyData.Tables(0)
                gvImport.DataMember = MyData.Tables(0).TableName
                gvImport.DataBind()
                gvImport.Visible = True

            Catch ex As Exception
                Me.lblMsg.Text = "btnImport_Click: " & ex.Message.ToString
            Finally
                clsCF.CloseConnection(MyConnection)
            End Try
        End Using
    End Sub

    Private Function SqlBulkCopy(ByVal strPath As String, ByVal strFile As String) As Boolean

        Try

            ' Connection String to Excel Workbook
            Dim MyConnection As System.Data.OleDb.OleDbConnection
            If strFile.Contains(".xls") = True Then
                MyConnection = New System.Data.OleDb.OleDbConnection( _
                "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & strPath & "; Extended Properties='Excel 8.0;HDR=yes;IMEX=1'")
                '"provider=Microsoft.Jet.OLEDB.4.0; " & _
                '"data source=" & strPath & "; " & _
                '"Extended Properties=""Excel 8.0;HDR=NO;IMEX=1""")
                '"Extended Properties='Excel 8.0;HDR=NO;IMEX=1'")



                'Dim excelConnectionString As String = ""
                'excelConnectionString = ("Provider=Microsoft.Jet.OLEDB.4.0;DataSource=" & strPath & ";Extended Properties='Excel 8.0;HDR=NO;IMEX=1'")

            ElseIf strFile.Contains(".xlsx") = True Then
                MyConnection = New System.Data.OleDb.OleDbConnection( _
                "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & strPath & "; Extended Properties='Excel 12.0;HDR=yes;IMEX=1'")
                '"provider=Microsoft.ACE.OLEDB.12.0; " & _
                '"data source=" & strPath & "; " & _
                '"Extended Properties=""Excel 12.0;HDR=NO;IMEX=1""")
                '"Extended Properties='Excel 12.0;HDR=NO;IMEX=1'")

            End If

            Dim sheetTable As New DataTable
            Dim rowSheetName As DataRow
            Dim sheetName As String

            MyConnection.Open()

            sheetTable = MyConnection.GetSchema("Tables")
            rowSheetName = sheetTable.Rows(0)
            sheetName = rowSheetName(2).ToString()


            ' Create Connection to Excel Workbook
            Dim command As New System.Data.OleDb.OleDbCommand("Select * FROM [" & sheetName & "]", MyConnection)
            'MyConnection.Open()

            ' Create DbDataReader to Data Worksheet
            Using dr As Common.DbDataReader = command.ExecuteReader()

                Dim dt As DataTable = New DataTable()
                dt.Columns.Add("UserID")
                dt.Columns(0).DefaultValue = Session("DA_UserID").ToString() + "-" + hdfCurrentDateTime.Value.ToString

                Dim i As Integer = dr.FieldCount
                Dim j As Integer
                For j = 0 To i - 1
                    dt.Columns.Add(dr.GetName(j).ToString)
                Next
                dt.Load(dr)

                Dim drNew As Common.DbDataReader
                drNew = dt.CreateDataReader

                ' SQL Server Connection String. Bulk Copy to SQL Server
                Using bulkCopy As New SqlBulkCopy(strConnectionString)
                    bulkCopy.DestinationTableName = "tbUploadOldAsset"
                    bulkCopy.WriteToServer(drNew)
                End Using
            End Using
            Return True
        Catch ex As Exception
            Me.lblMsg.Text = "SqlBulkCopy: " & ex.Message.ToString
            Return False
            Exit Function
        End Try
    End Function
End Class
