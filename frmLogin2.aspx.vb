Imports System.Configuration.ConfigurationSettings
Imports System.Data
Imports System.Data.SqlClient


Partial Class frmLogin2
    Inherits System.Web.UI.Page

    Dim strExtLink As String
    'Dim clsComF As New clsCommonFunction
    Dim strConnectionString As String = ConfigurationSettings.AppSettings("ConnectionString")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack() = False Then
            Session("LastContentForm") = ""
            Session("DA_UserName") = ""
            Session("DA_UserID") = ""
            Session("DA_StationID") = ""
            Session("DA_StationCode") = ""
            Session("DA_DepartmentCode") = ""

            If Request.QueryString("ExtLink") <> Nothing Then
                If Request.QueryString("ExtLink").ToString().Equals("") = False Then
                    strExtLink = Request.QueryString("ExtLink").ToString()
                    Session("LastContentForm") = strExtLink
                End If
            End If

            BindDropDownList()

            Dim strUserName As String = Request.ServerVariables("LOGON_USER").ToString
            Dim hasDomain As Integer = strUserName.IndexOf("\")
            If hasDomain > 0 Then
                txtUserID.Text = strUserName.Remove(0, hasDomain + 1).ToUpper()
            Else
                txtUserID.Text = strUserName.ToUpper()
            End If

            If Session("DA_LOG_IN_OUT") = Nothing Then
                Dim dsUserDetail As New DataSet
                dsUserDetail = DIMERCO.SDK.Utilities.LSDK.getUserProfilebyUserList(txtUserID.Text.ToString)
                If dsUserDetail.Tables(0).Rows.Count > 0 Then
                    Session("DA_UserID") = dsUserDetail.Tables(0).Rows(0)("UserID").ToString
                    Session("DA_UserName") = dsUserDetail.Tables(0).Rows(0)("FullName").ToString
                    Session("DA_StationID") = dsUserDetail.Tables(0).Rows(0)("StationID").ToString
                    Session("DA_StationCode") = dsUserDetail.Tables(0).Rows(0)("StationCode").ToString
                    Session("DA_DepartmentCode") = dsUserDetail.Tables(0).Rows(0)("DepartmentID").ToString
                    Session("DA_LOG_IN_OUT") = "IN"
                    Response.Redirect("~/Default.aspx")
                End If
            End If
        End If
    End Sub

    Private Sub BindDropDownList()
        Try

            Dim dsStation As New DataSet
            dsStation = DIMERCO.SDK.Utilities.ReSM.GetStationList()
            Dim dtView As DataView = dsStation.Tables(0).DefaultView
            dtView.Sort = "StationCode ASC"

            ddlStation.DataSource = dtView
            ddlStation.DataValueField = dtView.Table.Columns("StationID").ToString
            ddlStation.DataTextField = dtView.Table.Columns("StationCode").ToString
            ddlStation.DataBind()


        Catch ex As Exception
            Me.lblMsg.Text = "BindDropDownList: " & ex.Message.ToString
        End Try

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        txtUserID.Text = ""
        txtPwd.Text = ""
        txtUserID.Focus()
    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim strLoginID As String = ""
        Dim strPassword As String = ""
        Dim strStation As String = ""
        Dim strMsg As String = ""

        lblMsg.Text = ""

        If hdfCurrentDateTime.Value.ToString <> "" Then
            Session("CurrentDateTime") = hdfCurrentDateTime.Value.ToString
        Else
            Session("CurrentDateTime") = Format(DateTime.Now, "dd MMM yyyy hh:mm:ss").ToString
        End If


        'Get Login ID and Password
        strLoginID = Trim(txtUserID.Text.ToString())
        strPassword = Trim(txtPwd.Text.ToString())
        If Len(strLoginID) = 0 Then
            lblMsg.Text = "Please enter User ID."
            Exit Sub
        End If

        If Len(strPassword) = 0 Then
            lblMsg.Text = "Please enter Password."
            Exit Sub
        End If

        'Check User Login ID And Password
        strMsg = CheckLoginUser(strLoginID, strPassword, strStation)
        If strMsg = "" Then
            Session("AccessRightMsg") = ""
            Response.Redirect("~/Default.aspx")
        Else
            lblMsg.Text = strMsg.ToString()
            Exit Sub
        End If
    End Sub

    Private Function CheckLoginUser(ByVal strUserID As String, ByVal strPassword As String, ByVal strStation As String) As String
        Dim strMsg As String = ""
        Dim strUserStation As String = ""

        Session("DA_UserID") = ""
        Session("DA_StationID") = ""
        Session("DA_StationCode") = ""

        Try
            Dim lds As New DataSet()
            If DIMERCO.SDK.Utilities.ReSM.CheckUserInfo(strUserID, strPassword, lds) = True Then

                lds.Tables(0).Rows(0)(0).ToString()
                Session("DA_UserName") = lds.Tables(0).Rows(0)("Fullname").ToString
                Session("DA_UserID") = strUserID.ToUpper.ToString
                Session("DA_StationID") = lds.Tables(0).Rows(0)("StationID").ToString()
                ddlStation.SelectedValue = Session("DA_StationID").ToString
                Session("DA_StationCode") = ddlStation.SelectedItem.Text.ToUpper.ToString
                Session("DA_DepartmentCode") = lds.Tables(0).Rows(0)("DepartmentID").ToString
            End If

            Return strMsg
        Catch ex As Exception
            Me.lblMsg.Text = "CheckLoginUser: " & ex.Message.ToString
            strMsg = Me.lblMsg.Text
            Return strMsg
        End Try

    End Function
End Class
